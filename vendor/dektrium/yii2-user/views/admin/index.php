<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;
use yii\db\Query;
use app\models\Empresa;
use app\models\Taller;
use app\models\AuthAssignment;

/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \dektrium\user\models\UserSearch $searchModel
 */

$this->title = Yii::t('user', 'Manage users');
//$this->params['breadcrumbs'][] = $this->title;
?>


<?= $this->render('/admin/_menu') ?>

<?php Pjax::begin() ?>
<div class="table-responsive">
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel'  => $searchModel,
    'layout'       => "{items}\n{pager}",
    'tableOptions' => ['class' => 'table table-responsive'],
    'summary' => '',
    'options' =>[
        'class' => 'grid',
    ],
    'columns' => [
        [
            'attribute' => 'username',
            'enableSorting' => false,
        ],
        [
            'attribute' => 'email',
            'enableSorting' => false,
        ],
        /*[
            'attribute' => 'Empresa',
            'value' => function ($model) {

                        $emp=Empresa::findOne($model->id_empresa);
                
                         if ($emp) {
                    return $emp->nombre;
                }
                    if (!$emp) {
                    return '--';
                }
                        
                    },
            'filter' => Html::input('text', 'UserSearch[id_empresa]', $searchModel->id_empresa, ['class' => 'form-control',]),
            'enableSorting' => false,
        ],*/
                [
            'attribute' => 'Taller',
            'value' => function ($model) {

                        
                        $taller=Taller::findOne($model->id_taller);
                         if (!$taller) {
                    return '--';
                }
                    if ($taller) {
                    return $taller->nombre;
                }
                        
                    },
             'filter' => Html::input('text', 'UserSearch[id_taller]', $searchModel->id_taller, ['class' => 'form-control',]),
            'enableSorting' => false,
        ],




 


        [
            'label' => 'Rol Asignado',
            'value' =>  function ($model) {
 
                             $rol=AuthAssignment::findOne(['user_id'=>$model->id]);



                         
                                // if($rol->item_name == 'admin'){
                                //     $roles='Administrador';
                                // }
                                // if($rol->item_name == 'gerente'){
                                  
                                //     $roles='Gerente';

                                // }
                                // if($rol->item_name == 'supervisor'){
                                     
                                //     $roles='Supervisor';
                                // }
                                // if($rol->item_name  == 'coord-flota'){
                                    
                                //     $roles='Coordinador de Flota';
                                // }
                                // if($rol->item_name == 'control'){
                                   
                                //     $roles ='Control';
                                // }
                                // if($rol->item_name == 'taller' || $rol->item_name == 'Taller'){
                                    
                                //     $roles='Taller';
                                // }
                                // if($rol->item_name == 'analista'){
                                    
                                //     $roles='Analista de mantenimiento';
                                // }
                                // if($rol->item_name == 'gerente-compras'){
                                    
                                //     $roles='Gerente de Compras';
                                // }
                                // if($rol->item_name == 'proveedor'){
                               
                                //     $roles='Proveedor';
                                // }
                                // if($rol->item_name == 'agencia'){
                                 
                                //     $roles='Cliente';
                                // }
                                //  if($rol->item_name == 'mensajero'){
                              
                                //     $roles='Mensajero';
                                // }
                        
                             if ($rol['item_name']) { 
                                 # code...
                        return $rol['item_name'];
                             }
                    },
                'filter' => Html::input('text', 'UserSearch[id_rol]', $searchModel->id_rol, ['class' => 'form-control',]),
        ],
        /*[
            'attribute' => 'cargo',
            'value' => function ($model) {
                if ($model->cargo==1) {
                    return 'Supervisor';
                } else if ($model->cargo==2){
                    return 'Coordinador de la flota';
                } else if ($model->cargo==3){
                    return 'Gerente de la marca';
                } else {
                    return '-';
                }
            },
        ],*/
        [
            'header' => Yii::t('user', 'Block status'),
            'value' => function ($model) {
                if ($model->isBlocked) {
                    return Html::a(Yii::t('user', 'Unblock'), ['block', 'id' => $model->id], [
                        'class' => 'btn btn-xs btn-success btn-block',
                        'data-method' => 'post',
                        'data-confirm' => Yii::t('user', 'Are you sure you want to unblock this user?'),
                    ]);
                } else {
                    return Html::a(Yii::t('user', 'Block'), ['block', 'id' => $model->id], [
                        'class' => 'btn btn-xs btn-danger btn-block',
                        'data-method' => 'post',
                        'data-confirm' => Yii::t('user', 'Are you sure you want to block this user?'),
                    ]);
                }
            },
            'format' => 'raw',
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
            'buttons' => [
                'resend_password' => function ($url, $model, $key) {
                    if (!$model->isAdmin) {
                        return '
                    <a data-method="POST" data-confirm="' . Yii::t('user', 'Are you sure?') . '" href="' . Url::to(['resend-password', 'id' => $model->id]) . '">
                    <span title="' . Yii::t('user', 'Generate and send new password to user') . '" class="glyphicon glyphicon-envelope">
                    </span> </a>';
                    }
                },
                'switch' => function ($url, $model) {
                    if($model->id != Yii::$app->user->id && Yii::$app->getModule('user')->enableImpersonateUser) {
                        return Html::a('<span class="glyphicon glyphicon-user"></span>', ['/user/admin/switch', 'id' => $model->id], [
                            'title' => Yii::t('user', 'Become this user'),
                            'data-confirm' => Yii::t('user', 'Are you sure you want to switch to this user for the rest of this Session?'),
                            'data-method' => 'POST',
                        ]);
                    }
                }
            ]
        ],
    ],
]); ?>
</div>
<?php Pjax::end() ?>
