 

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use app\models\modelo;
use app\models\marca;
use app\models\TipoEmpresa;
use kartik\form\ActiveForm;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $model app\models\Caso */

$this->title = 'Detalle de '.$elproveedor->nombre;
?>
<div class="proveedorpieza-view">


 <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>
         <div class="row">
               <div class="row">
    <div class="col-md-1 col-xs-3">
      <h4 class="titulo"><img src="<?= Yii::$app->request->baseUrl ?>/img/prov.png" alt="" width="40" height="40" ></h4>
    </div>
    <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
      <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
    </div>
  </div>
      </div>
 
  

     <!-- --DATOS DE LA EMPRESA-- -->
    <br>

 
<div class="row" style="border-bottom: 1px solid #222d32;">
        <div class="col-md-6">
            <h4>DATOS DEL PROVEEDOR</h4>
        </div>  
    <div class="col-md-6">
        <div class="pull-right">
                <?= Html::a('Listado de Proveedores',['proveedor/index'], ['class' => 'btn btn-small-gris']) ?>           
        </div> 
    </div>
</div>
<br>
        <!-- fin de cabeza -->

        <div class="row">
            <div class="col-md-4">
                 <?= $form->field($elproveedor, 'tipo_persona')->textInput(['maxlength' => true, 'disable','readonly'=>'true'])?>
           </div>
           <div class="col-md-4">
                 <?= $form->field($elproveedor, 'identificacion')->textInput(['maxlength' => true,'readonly'=>'true']) ?>
           </div>
           <div class="col-md-4">
                 <?= $form->field($empresatipo, 'nombre')->textInput(['maxlength' => true,'readonly'=>'true']) ?>
           </div>
        </div>


        <div class="row">
            <div class="col-md-8">
                 <?= $form->field($elproveedor, 'nombre')->textInput(['maxlength' => true,'readonly'=>'true'])?>
            </div>
           <div class="col-md-4">
                 <?= $form->field($elproveedor, 'telefono')->textInput(['maxlength' => true,'readonly'=>'true']) ?>
           </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                 <?= $form->field($elproveedor, 'direccion')->textInput(['maxlength' => true, 'readonly'=>'true'])?>
           </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                 <?= $form->field($elproveedor, 'descripcion')->textInput(['maxlength' => true, 'readonly'=>'true'])?>
           </div>
        </div>

    <br>
    <div class="row" style="border-bottom: 1px solid #222d32;">
        <h4>DATOS DEL REPRESENTANTE LEGAL</h4>
    </div>
    <br>

         <div class="row">
            <div class="col-md-4">
                 <?= $form->field($elproveedor, 'nombre_representante')->textInput(['maxlength' => true,'readonly'=>'true'])?>
           </div>
           <div class="col-md-4">
                 <?= $form->field($elproveedor, 'identificacion_representante')->textInput(['maxlength' => true,'readonly'=>'true']) ?>
           </div>
           <div class="col-md-4">
                 <?= $form->field($elproveedor, 'puesto_representante')->textInput(['maxlength' => true,'readonly'=>'true']) ?>
           </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                 <?= $form->field($elproveedor, 'correo_representante')->textInput(['maxlength' => true, 'disable','readonly'=>'true'])?>
           </div>
           <div class="col-md-4">
                 <?= $form->field($elproveedor, 'celular_representante')->textInput(['maxlength' => true,'readonly'=>'true']) ?>
<!--  --> 

           </div>
           <div class="col-md-4">
                 <?= $form->field($elproveedor, 'telefono_representante')->textInput(['maxlength' => true,'readonly'=>'true']) ?>
           </div>

        </div>

         <br>
    <div class="row" style="border-bottom: 1px solid #222d32;">
        <h4>DEFINICIÓN DE PAGOS</h4>
    </div>
    <br>

      <div class="row">
            <div class="col-md-4">
                 <?= $form->field($elproveedor, 'Tipo_pago')->textInput(['maxlength' => true,'readonly'=>'true'])?>
           </div>
           <div class="col-md-4">
                 <?= $form->field($elproveedor, 'credito')->textInput(['maxlength' => true,'readonly'=>'true']) ?>
           </div>
           <div class="col-md-4">
                 <?= $form->field($elproveedor, 'plazo_pago')->textInput(['maxlength' => true,'readonly'=>'true']) ?>
           </div>
    </div>

          <br>
    <div class="row" style="border-bottom: 1px solid #222d32;">
        <h4>DEFINICIÓN DE SERVICIOS</h4>
    </div>
    <br>

      <div class="row">
            <div class="col-md-4">
                 <?= $form->field($elproveedor, 'garantia')->textInput(['maxlength' => true,'readonly'=>'true'])?>
           </div>
           <div class="col-md-4">
                 <?= $form->field($elproveedor, 'domicilio')->textInput(['maxlength' => true,'readonly'=>'true']) ?>
           </div>
           <div class="col-md-4">
                 <?= $form->field($elproveedor, 'porcentaje')->textInput(['maxlength' => true,'readonly'=>'true']) ?>
           </div>
    </div>

           <br>
    <div class="row" style="border-bottom: 1px solid #222d32;">
        <h4>ASOCIACIÓN DE VEHÍCULOS Y PIEZAS</h4>
    </div>
    <br>
 
 

 
 
 <table class ="tablaproveedor" border="1">
            <td     style="border: 0px;"></td>
            <td style="font-weight:bold; text-align:center; background:#DAE3F3;">Piezas</td>
            <td style="font-weight:bold; text-align:center;background:#DAE3F3;">Marcas</td>
            <td style="font-weight:bold; text-align:center;background:#DAE3F3;">Modelos</td>
            <td style="font-weight:bold; text-align:center;background:#DAE3F3;">Años</td>
            <tr><th style="width: 30%;font-weight:bold; text-align:center; background:#DAE3F3; ">Piezas Carrocería</th> 

            <td style="width: 30%;  text-align:center;">
                
                <?php 
                $piezas="";
                 foreach ($elproveedor->proveedorPiezas as $value) {
                              if($value->pieza!=null){

                                    if($value->pieza->tipoPieza->id == 5){ 
                                        

                                        $piezas = $piezas."<br>". $value->pieza->nombre;

                                    }

                              } 
                              }
                              if ($piezas) { 
                                  echo $piezas;
                              }

                           

                 ?>


            </td>  
            <td>

                 <?php 
                $piezas="";
                  foreach ($elproveedor->proveedorPiezas as $value) {
 
                                 if($value->pieza->modelo_id_modelo!=null){

                                    if($value->pieza->tipoPieza->id == 5){ 

                                           $piezas = $piezas."<br>". $value->pieza->modeloIdModelo->idMarca->nombre;

                                    }

                              } 
                
                              }
 
                            echo $piezas;

                 ?>

            </td> 
             <td>

                 <?php 
                $piezas="";
                  foreach ($elproveedor->proveedorPiezas as $value) {
 
                                 if($value->pieza->modeloIdModelo!=null){

                                    if($value->pieza->tipoPieza->id == 5){ 

                                           $piezas = $piezas."<br>". $value->pieza->modeloIdModelo->nombre;

                                    }

                              } 
                     
                              }
                            echo $piezas;

                 ?>

            </td>
            <td>

                 <?php 
                $piezas="";
                  foreach ($elproveedor->proveedorPiezas as $value) {

                          
                                 if($value->pieza->modeloIdModelo!=null){

                                    if($value->pieza->tipoPieza->id == 5){ 

                                           $piezas = $piezas."<br>". $value->pieza->anho;

                                    }

                              } 
                         
                              }
                            echo $piezas;

                 ?>

            </td>

        </tr>
        <tr><th style="width: 30%; font-weight:bold; text-align:center; background:#DAE3F3;">Piezas Mecánicas</th> 
            <td style="width: 30%;  text-align:center;">
                
                <?php 
                $piezas="";
                 foreach ($elproveedor->proveedorPiezas as $value) {
                              if($value->pieza!=null){

                                    if($value->pieza->tipoPieza->id == 4){ 
                                        

                                        $piezas = $piezas."<br>". $value->pieza->nombre;

                                    }

                              } 
                              }
                              if ($piezas) { 
                                  echo $piezas;
                              }

                           

                 ?>


            </td>  
            <td>

                 <?php 
                $piezas="";
                  foreach ($elproveedor->proveedorPiezas as $value) {
 
                                 if($value->pieza->modelo_id_modelo!=null){

                                    if($value->pieza->tipoPieza->id == 4){ 

                                           $piezas = $piezas."<br>". $value->pieza->modeloIdModelo->idMarca->nombre;

                                    }

                              } 
                
                              }
 
                            echo $piezas;

                 ?>

            </td> 
             <td>

                 <?php 
                $piezas="";
                  foreach ($elproveedor->proveedorPiezas as $value) {
 
                                 if($value->pieza->modeloIdModelo!=null){

                                    if($value->pieza->tipoPieza->id == 4){ 

                                           $piezas = $piezas."<br>". $value->pieza->modeloIdModelo->nombre;

                                    }

                              } 
                     
                              }
                            echo $piezas;

                 ?>

            </td>
            <td>

                 <?php 
                $piezas="";
                  foreach ($elproveedor->proveedorPiezas as $value) {

                          
                                 if($value->pieza->modeloIdModelo!=null){

                                    if($value->pieza->tipoPieza->id == 4){ 

                                           $piezas = $piezas."<br>". $value->pieza->anho;

                                    }

                              } 
                         
                              }
                            echo $piezas;

                 ?>

            </td>
                        <td>

                 <?php 
                $piezas="";
                foreach ($elproveedor->proveedorPiezas as $value) {

                          
                                 if($value->pieza->modeloIdModelo!=null){

                                    if($value->pieza->tipoPieza->id == 4){ 

                                           $piezas = $piezas."<br>". $value->pieza->anho;

                                    }

                              } 
                         
                              }
                            echo $piezas;

                 ?>

            </td>
        </tr>
                 <tr><th style="width: 30%; font-weight:bold; text-align:center; background:#DAE3F3;">Insumos </th>
                  <td style="width: 30%;  text-align:center;">

                     <?php 
                $piezas="";
                 foreach ($elproveedor->proveedorPiezas as $value) {

                          
                                 if($value->pieza->modeloIdModelo!=null){

                                    if($value->pieza->tipoPieza->id == 1){ 

                                           $piezas = $piezas."<br>". $value->pieza->anho;

                                    }

                              } 
                         
                              }
                               
                                 echo $piezas;
                              
                           
                             ?>

                 </td>
                 <td></td>
                  <td></td>
                   <td></td>
             </tr>
 

 
    </table><br>





</div>
