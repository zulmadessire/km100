<?php

use yii\helpers\Html;
use yii\grid\GridView;


use app\models\Empresa;
use app\models\Pieza;
use app\models\Marca;
use app\models\TipoEmpresa;
use app\models\Proveedor;
use app\models\TipoPieza;
use app\models\MarcaInsumo;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProveedorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Listado de Proveedores';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proveedor-index">
    
      <div class="row">
    <div class="col-md-1 col-xs-3">
      <h4 class="titulo"><img src="<?= Yii::$app->request->baseUrl ?>/img/prov.png" alt="" width="40" height="40" ></h4>
    </div>
    <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
      <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
    </div>
  </div>
       <div class="col-md-12">
            <div class="pull-right">
                <?= Html::a('Registrar Proveedor',['proveedorpieza/create'], ['class' => 'btn btn-reportes']) ?>
                
            </div>
        </div>
          </br>
        </br>
         <div class="col-md-12">
     
</div>
</div>



