<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Proveedorpieza */

$this->title = 'Actualizar Proveedor  ';
 
?>
<div class="proveedorpieza-update">

   
       <div class="row">
    <div class="col-md-1 col-xs-3">
      <h4 class="titulo"><img src="<?= Yii::$app->request->baseUrl ?>/img/prov.png" alt="" width="40" height="40" ></h4>
    </div>
    <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
      <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
    </div>
  </div>
    <br>
            <div class="row" style="border-bottom: 1px solid #222d32;">
              <div class="col-xs-12 col-md-6">
                  <h4>DATOS DEL PROVEEDOR</h4>
              </div>
              <div class="col-xs-12 col-md-6">
                <div align="right">
                      <?php
                      echo Html::a('Listado de proveedores', ['proveedor/index'], ['class' => 'btn btn-accion']);
                      ?>
                </div>
              </div>
          </div>
         

    <?= $this->render('_form', [
         
       'modelprov' => $modelprov,
                'modelsPiezas' => (empty($modelsPiezas)) ? [new ProveedorPieza] : $modelsPiezas,
    ]) ?>


</div>
