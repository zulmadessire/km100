<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProveedorpiezaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proveedorpieza-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    

    <?= $form->field($model, 'modelo_id_modelo') ?>

    <?= $form->field($model, 'pieza_id_pieza') ?>

    <?= $form->field($model, 'proveedor_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
