<?php

use app\models\AuthAssignment;
use app\models\Empresa;
use app\models\Estacion;
use app\models\Cliente;
use app\models\User;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

/**
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\User $user
 */
?>

<?=$form->field($user, 'email')->textInput(['maxlength' => 255])?>
<?=$form->field($user, 'username')->textInput(['maxlength' => 255])?>

<?php if (Yii::$app->user->identity->id == $user->id): ?>
    <?php

        $preguntas = ['Nombre de mi mascota' => 'Nombre de mi mascota', 'Color preferido' => 'Color preferido', 'Comida favorita' => 'Comida favorita', 'Nombre de mi padre' => 'Nombre de mi padre', 'Nombre de la madre' => 'Nombre de la madre', 'Película favorita' => 'Película favorita'];
        echo $form->field($user, "pregunta")->dropDownList(
            $preguntas,
            [
                'prompt' => 'Seleccione...',
                'class'  => 'form-control',

            ]
        );

    ?>
<?php endif?>

<?php

if (Yii::$app->user->identity->id == $user->id): ?>
    <?=$form->field($user, 'respuesta')->textInput(['maxlength' => 255])?>
    <?=$form->field($user, 'password')->passwordInput()?>
<?php endif?>


<?php

    $roll  = AuthAssignment::findOne(['user_id' => Yii::$app->user->identity->id]);
    $admin = $roll->item_name;

?>

 <?php

if ($admin == 'Administrador' || $admin == 'admin') {
    ?>
    <?php

        $tipos_pago = [0 => 'Si', 1 => 'No'];
        echo $form->field($user, "bandera")->dropDownList(
            $tipos_pago,
            [
                'prompt'   => 'Seleccione...',
                'class'    => 'form-control',
                'onchange' => ' if(($(this).val() == 0)){

                                            document.getElementById("band").style.display="none";
                                  
                                        }else if(($(this).val() == 1)){

                                            document.getElementById("band").style.display="block";
                                           
                                        } ',
                'id'       => 'field_tipo_pago',
            ]
        );

    ?>



    <?php

    if ($user->id != null && $user->bandera==1) {
        ?>
 
        <?php
            $query = new Query();
            $query->select(['estacion.id_estacion as id_est',
                'concat(empresa.nombre, concat(" - ",estacion.nombre)) as est'])
                ->from('empresa')
                ->join('INNER JOIN', 'estacion', 'empresa.id_empresa = estacion.id_empresa')
                ->join('INNER JOIN', 'estacion_usuario', 'estacion_usuario.estacion_id = estacion.id_estacion')
                ->where(['estacion_usuario.user_id' => $user->id])
                ->orderBy(['empresa.nombre' => SORT_ASC, 'estacion.nombre' => SORT_ASC]);
            $rows = $query->all();

              $est1 = Estacion::find()->all();
        $est = ArrayHelper::map($est1, 'id_estacion', 'nombre');
         $empr = Empresa::find()->all();
 

            $value = array($empr);
            if ($rows) {
                # code...
            $est = ArrayHelper::map($rows, 'id_est', 'est');

            $est_emp = Estacion::findOne(['id_estacion' => $rows[0]['id_est']]);

            $empr = Empresa::find()->all();

            $empr = ArrayHelper::map($empr, 'id_empresa', 'nombre');

            $empresa = $est_emp->idEmpresa->cliente_id;

            $value = array($empresa);

            $est2 = Estacion::find()->all();

            $est2 = ArrayHelper::map($est2, 'id_estacion', 'nombre');

            $est2=[];
            foreach ($est as $key => $value2) {
                $est2[] = $key;
            }

            $esta_usu->estacion_id= $est2;
            } 
               



             }
        ?>

 <?php
 

  if ((\Yii::$app->user->can('admin') && ($user->id != null && $user->bandera==0))||(\Yii::$app->user->can('admin') && ($user->id == null)))
   { ?>
 
   <?php 
        $est1 = Estacion::find()->all();
        $est = ArrayHelper::map($est1, 'id_estacion', 'nombre');

    
        $empr = Empresa::find()->all();
        


            $value = array($empr);
    ?>

  <?php }?>
 
 
<div id="band" style="display: none;">

    <!-- _____________________________________________________________________ -->
  
        <div class="col-xs-12 col-md-offset-3 col-md-9" style ="padding-left: 6px;padding-right: 1px;">
            
               <?php
                echo Select2::widget([
                    'name'    => 'user[id_cliente]',
                    'value'   => $value,
                    'data'    =>  ArrayHelper::map(Cliente::find()->where('id != 6')->orderBy('nombre_comercial')->all(), 'id', 'nombre_comercial'),
                    'options' => ['placeholder' => 'Selecciona el cliente', 'id' => 'id_cliente'],
                    ]);

    ?>
       </div>
       <br><br><br>
         <div class="col-xs-12  col-md-12" style ="padding-left: 6px;padding-right: 1px;">
<?php

                echo $form->field($esta_usu, 'estacion_id')->widget(DepDrop::classname(), [

                    'data'           => $est,
                   // 'value' => [[22],[24],[26]],
                    'type'           => DepDrop::TYPE_SELECT2,
                    'options'        => ['id' => 'estacion_id', 'multiple' => true, ],
                    'select2Options' => ['pluginOptions' => ['allowClear' => true,], ],

                    'pluginOptions'  => [

                        'depends'     => ['id_cliente'],
                        'placeholder' => 'Estación',
                        'url'         => Url::to(['/user/admin/getestaciones']),
                         

                    ],
                    ])->label(false);?>

              </div>
 
 
<!-- _____________________________________________________________________ -->
 
 </div>


<?php } ?>












<script type="text/javascript">
var tipo_pago  = "<?php echo $user->bandera; ?>";

if(tipo_pago == 0){
 document.getElementById("band").style.display="none";
                                        // document.getElementById("band2").style.display="block";
}else{
       
                                        document.getElementById("band").style.display="block";
                                        // document.getElementById("band2").style.display="none";

}

</script>