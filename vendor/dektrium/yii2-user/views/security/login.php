<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use dektrium\user\widgets\Connect;
use dektrium\user\models\LoginForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;


/**
 * @var yii\web\View $this
 * @var dektrium\user\models\LoginForm $model
 * @var dektrium\user\Module $module
 */

$this->title = Yii::t('user', 'Sign in');
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>

    <div class="row login-contenedor">
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 img-contenedor">
            <img src="<?= Yii::$app->request->baseUrl ?>/img/portada-log.png" width="100%" class="fondo-img"/>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 form-contenedor"  align="center">
            <img src="<?= Yii::$app->request->baseUrl ?>/img/logo.png" width="50%" /><br><br>
            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'enableAjaxValidation' => true,
                'enableClientValidation' => false,
                'validateOnBlur' => false,
                'validateOnType' => false,
                'validateOnChange' => false,
            ]) ?>

            <?php if ($module->debug): ?>
                <?= $form->field($model, 'login', [
                    'inputOptions' => [
                        'autofocus' => 'autofocus',
                        'class' => 'form-control',
                        'tabindex' => '1']])->dropDownList(LoginForm::loginList());
                ?>

            <?php else: ?>

                <?= $form->field($model, 'login',
                    ['inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'tabindex' => '1']]
                );
                ?>

            <?php endif ?>

            <?php if ($module->debug): ?>
                <div class="alert alert-warning">
                    <?= Yii::t('user', 'Password is not necessary because the module is in DEBUG mode.'); ?>
                </div>
            <?php else: ?>
                <?= $form->field(
                    $model,
                    'password',
                    ['inputOptions' => ['class' => 'form-control', 'tabindex' => '2']])
                    ->passwordInput()
                    ->label(Yii::t('user', 'Password')) ?>
            <?php endif ?>

          

            <?= Html::submitButton('Login',
                ['class' => 'btn btn-danger btn-block', 'tabindex' => '4']
            ) ?>

            <?php ActiveForm::end(); ?>

              <?= Html::a('¿Olvidaste la contraseña?', '#', ['data-toggle' => 'modal','data-target' => '#modal_olvido', 'class'=>'login-contrasena']) ?>

              <br>

               <?= Html::a('Registrarse como proveedor', '#', ['data-toggle' => 'modal','data-target' => '#proveedor', 'class'=>'login-contrasena']) ?>

        </div>
<?= Connect::widget([
            'baseAuthUrl' => ['/user/security/auth'],
        ]) ?>
    </div>
<!-- Ventana modal para recuperar contraseña  -->
<?php
Modal::begin([
    'header' => '<h2 class="titulomodal">Recuperar Contraseña</h2>',
    'id' => 'modal_olvido',
    'size' => 'modal-md',
]);
?>

    <div class="row">
        <?php $form = ActiveForm::begin(['id' => 'recuperar-form', 'method' => 'post']); ?>
            <div class="col-md-offset-2 col-md-10 col-md-offset-1">
                <div class="row">
                   <?= $form->field($recuperar, 'username')->textInput() 
                             ->label(false)
                             -> textInput(['placeholder' => 'Ingrese nombre de usuario']) ?>  
                </div>
                <div class="row">
            <?php 
             
          $preguntas = ['Nombre de mi mascota' => 'Nombre de mi mascota', 'Color preferido' => 'Color preferido', 'Comida favorita' =>'Comida favorita', 'Nombre de mi padre' =>'Nombre de mi padre', 'Nombre de la madre' =>'Nombre de la madre', 'Película favorita' =>'Película favorita'];
                echo $form->field($recuperar, "pregunta")->dropDownList(
                    $preguntas,
                    [
                         'prompt'=>'Seleccione...',
                        'class' => 'form-control',
                      
                    ]
                    )->label(false); 
// echo $form->field($recuperar, 'pregunta')->widget(Select2::classname(), [
//     'data' => $preguntas,
//     'options' => ['placeholder' => 'Select a state ...'],
//     'pluginOptions' => [
//         'allowClear' => true
//     ],
// ]);
 

?> 
 </div>
                <div class="row">
                   <?= $form->field($recuperar, 'respuesta')->textInput() 
                             ->label(false)
                             -> textInput(['placeholder' => 'Ingrese Respuesta']) ?>  
                </div>
                <div class="col-md-8 col-md-offset-2">
                    <?= Html::submitButton('Enviar', ['class' => 'btn btn-danger btn-block', 'name' => 'login-button']) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>

<?php Modal::end(); ?>

<!-- Ventana modal para agregar proveedor desde afuera -->

<?php
Modal::begin([
    'header' => '<h2 class="titulomodal">Registro de Proveedor</h2>',
    'id' => 'proveedor',
    'size' => 'modal-md',
]);
?>

    <div class="row">
        <?php $form = ActiveForm::begin(['id' => 'proveedor', 'method' => 'post']); ?>
            <div class="col-md-offset-2 col-md-10 col-md-offset-1">
                <div class="row">
                   <?= $form->field($proveedor, 'email')->textInput() 
                             ->label(false)
                             -> textInput(['placeholder' => 'Ingrese correo electrónico']) ?>  
                </div>
                <div class="row">
                   <?= $form->field($proveedor, 'username')->textInput() 
                             ->label(false)
                             -> textInput(['placeholder' => 'Ingrese nombre de usuario']) ?>  
                </div>
                <div class="row">
 
                      <?= $form->field($proveedor,'password',
                    ['inputOptions' => ['class' => 'form-control', 'tabindex' => '2','type'=>'password']])
                    ->passwordInput()
                    ->label(false)
                    -> textInput(['placeholder' => 'Ingrese contraseña']) ?>  
                    
 
                </div>

                    <div class="row">
 
                      <?= $form->field($proveedor,'password2',
                    ['inputOptions' => ['class' => 'form-control', 'tabindex' => '2','type'=>'password']])
                    ->passwordInput()
                    ->label(false)
                    -> textInput(['placeholder' => 'Repita contraseña']) ?>  
                  
 
                </div>
          
                <div class="col-md-8 col-md-offset-2">
                    <?= Html::submitButton('Registrar', ['class' => 'btn btn-danger btn-block', 'name' => 'registrar']) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>

<?php Modal::end(); ?>