<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use dektrium\user\widgets\Connect;
use dektrium\user\models\LoginForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var dektrium\user\models\LoginForm $model
 * @var dektrium\user\Module $module
 */

$this->title = Yii::t('user', 'Sign in');
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>

    <div class="row login-contenedor">
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 img-contenedor">
            <img src="<?= Yii::$app->request->baseUrl ?>/img/portada-log.png" width="100%" class="fondo-img"/>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 form-contenedor"  align="center">
            <img src="<?= Yii::$app->request->baseUrl ?>/img/logo.png" width="50%" /><br><br>
            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'enableAjaxValidation' => true,
                'enableClientValidation' => false,
                'validateOnBlur' => false,
                'validateOnType' => false,
                'validateOnChange' => false,
            ]) ?>

            <?php if ($module->debug): ?>
         

            <?php else: ?>
 
            <?php ActiveForm::end(); ?>
        </div>
<?= Connect::widget([
            'baseAuthUrl' => ['/user/security/auth'],
        ]) ?>
    </div>
