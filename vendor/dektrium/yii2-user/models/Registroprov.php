<?php
namespace dektrium\user\models;

use dektrium\user\Finder;
use dektrium\user\helpers\Password;
use dektrium\user\traits\ModuleTrait;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use Yii;
use yii\base\Model;
 

class Registroprov extends model
{
   public $username;
   public $email;
   public $registration_ip;
   public $unconfirmed_email;
   public $password;
   public $password2;
   public $created_at;
   public $last_login_at;
   public $confirmed_at;
   public $id_empresa;
   public $bandera;
   public $id_mensajero;
   public $pregunta;
   public $respuesta;
 


   public function rules()
   {
       return [
           [['username'], 'required', 'message'=>'Debe ingresar el usuario'],
           [['email'], 'required', 'message'=>'Debe ingresar el correo electrónico'],
           [['email'], 'email'],
           [['password'], 'required', 'message'=>'Debe ingresar la pregunta'],
           [['password2'], 'required', 'message'=>'Debe ingresar la respuesta'],
           [['registration_ip','unconfirmed_email','created_at','last_login_at','confirmed_at','id_empresa','bandera','id_mensajero'], 'required'],
       ];
   }
   public function attributeLabels()
   {
       return [
          'username' => 'Usuario',
          'pregunta' => 'Pregunta',
          'password' => 'Password',
          'respuesta' => 'Respuesta',
          'email' => 'email',

       ];
   }
}