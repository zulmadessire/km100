<?php
namespace dektrium\user\models;

use dektrium\user\Finder;
use dektrium\user\helpers\Password;
use dektrium\user\traits\ModuleTrait;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use Yii;
use yii\base\Model;
 

class Recuperarpass extends model
{
   public $username;
   public $pregunta;
   public $respuesta;

   public function rules()
   {
       return [
           [['username'], 'required', 'message'=>'Debe ingresar el usuario'],
           [['pregunta'], 'required', 'message'=>'Debe ingresar la pregunta'],
           [['respuesta'], 'required', 'message'=>'Debe ingresar la respuesta'],
       ];
   }

   public function attributeLabels()
   {
       return [
          'username' => 'Usuario',
          'pregunta' => 'Pregunta',
          'respuesta' => 'Respuesta',
       ];
   }
}