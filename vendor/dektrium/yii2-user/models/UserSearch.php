<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace dektrium\user\models;

use dektrium\user\Finder;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UserSearch represents the model behind the search form about User.
 */
class UserSearch extends Model
{
    /** @var string */
    public $username;

    /** @var string */
    public $email;

    /** @var int */
    public $created_at;

    /** @var int */
    public $last_login_at;

    /** @var string */
    public $registration_ip;

    /** @var Finder */
    protected $finder;

    public $empresa;

    public $id_empresa;

    public $id_taller;

    public $id_rol;

    /**
     * @param Finder $finder
     * @param array  $config
     */
    public function __construct(Finder $finder, $config = [])
    {
        $this->finder = $finder;
        parent::__construct($config);
    }

    /** @inheritdoc */
    public function rules()
    {
        return [
            'fieldsSafe' => [['username', 'email','id_rol', 'registration_ip','id_empresa','id_taller', 'created_at', 'last_login_at','id_mensajero','pregunta','respuesta'], 'safe'],
            'createdDefault' => ['created_at', 'default', 'value' => null],
            'lastloginDefault' => ['last_login_at', 'default', 'value' => null],
        ];
    }

    /** @inheritdoc */
    public function attributeLabels()
    {
        return [
            'username'        => Yii::t('user', 'Username'),
            'email'           => Yii::t('user', 'Email'),
            'created_at'      => Yii::t('user', 'Registration time'),
            'last_login_at'   => Yii::t('user', 'Last login'),
            'registration_ip' => Yii::t('user', 'Registration ip'),
            'id_empresa' => Yii::t('user', 'id empresa'),
            'id_taller' => Yii::t('user', 'id taller'),
            'id_mensajero' => Yii::t('user', 'id mensajero'),
            'pregunta' => Yii::t('user', 'Pregunta Secreta'),
            'Respuesta' => Yii::t('user', 'Respuesta Secreta'),
        ];
    }

    /**
     * @param $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->finder->getUserQuery();

        if ( isset($params['UserSearch']['id_empresa']) AND $params['UserSearch']['id_empresa'] != '') {
               $query->andwhere( '(SELECT nombre FROM Empresa WHERE id_empresa = user.id_empresa LIMIT 1) = '."'".$params['UserSearch']['id_empresa']."'" );
           }
          if ( isset($params['UserSearch']['id_taller']) AND $params['UserSearch']['id_taller'] != '') {
               $query->andwhere( '(SELECT nombre FROM Taller WHERE id_taller = user.id_taller LIMIT 1) = '."'".$params['UserSearch']['id_taller']."'" );
           }
           if ( isset($params['UserSearch']['id_rol']) AND $params['UserSearch']['id_rol'] != '') {
               $query->andwhere( '(SELECT item_name FROM auth_assignment WHERE user_id = user.id LIMIT 1) = '."'".$params['UserSearch']['id_rol']."'" );
           }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if ($this->created_at !== null) {
            $date = strtotime($this->created_at);
            $query->andFilterWhere(['between', 'created_at', $date, $date + 3600 * 24]);
        }

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['registration_ip' => $this->registration_ip]);

        return $dataProvider;
    }
}
