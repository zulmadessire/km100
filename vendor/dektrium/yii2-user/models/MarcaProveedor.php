<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "marca_proveedor".
 *
 * @property integer $id
 * @property integer $id_proveedor
 * @property integer $id_marca
 *
 * @property Marca $idMarca
 * @property Proveedor $idProveedor
 */
class MarcaProveedor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'marca_proveedor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_proveedor', 'id_marca'], 'integer'],
            [['id_marca'], 'exist', 'skipOnError' => true, 'targetClass' => Marca::className(), 'targetAttribute' => ['id_marca' => 'id_marca']],
            [['id_proveedor'], 'exist', 'skipOnError' => true, 'targetClass' => Proveedor::className(), 'targetAttribute' => ['id_proveedor' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_proveedor' => 'Id Proveedor',
            'id_marca' => 'Id Marca',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMarca()
    {
        return $this->hasOne(Marca::className(), ['id_marca' => 'id_marca']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProveedor()
    {
        return $this->hasOne(Proveedor::className(), ['id' => 'id_proveedor']);
    }
}
