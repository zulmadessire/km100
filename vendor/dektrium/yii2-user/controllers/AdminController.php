<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace dektrium\user\controllers;

use dektrium\user\filters\AccessRule;
use dektrium\user\Finder;
use dektrium\user\models\Profile;
use dektrium\user\models\User;
use dektrium\user\models\UserSearch;
use dektrium\user\helpers\Password;
use dektrium\user\Module;
use dektrium\user\traits\EventTrait;
use yii;
use yii\base\ExitException;
use yii\base\Model;
use yii\base\Module as Module2;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use app\models\Estacion;
use app\models\Empresa;
use app\models\EstacionUsuario;

use app\models\AuthAssignment;



/**
 * AdminController allows you to administrate users.
 *
 * @property Module $module
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com
 */
class AdminController extends Controller
{
    use EventTrait;

    /**
     * Event is triggered before creating new user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_BEFORE_CREATE = 'beforeCreate';

    /**
     * Event is triggered after creating new user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_AFTER_CREATE = 'afterCreate';

    /**
     * Event is triggered before updating existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_BEFORE_UPDATE = 'beforeUpdate';

    /**
     * Event is triggered after updating existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_AFTER_UPDATE = 'afterUpdate';

    /**
     * Event is triggered before impersonating as another user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_BEFORE_IMPERSONATE = 'beforeImpersonate';

    /**
     * Event is triggered after impersonating as another user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_AFTER_IMPERSONATE = 'afterImpersonate';

    /**
     * Event is triggered before updating existing user's profile.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_BEFORE_PROFILE_UPDATE = 'beforeProfileUpdate';

    /**
     * Event is triggered after updating existing user's profile.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_AFTER_PROFILE_UPDATE = 'afterProfileUpdate';

    /**
     * Event is triggered before confirming existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_BEFORE_CONFIRM = 'beforeConfirm';

    /**
     * Event is triggered after confirming existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_AFTER_CONFIRM = 'afterConfirm';

    /**
     * Event is triggered before deleting existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_BEFORE_DELETE = 'beforeDelete';

    /**
     * Event is triggered after deleting existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_AFTER_DELETE = 'afterDelete';

    /**
     * Event is triggered before blocking existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_BEFORE_BLOCK = 'beforeBlock';

    /**
     * Event is triggered after blocking existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_AFTER_BLOCK = 'afterBlock';

    /**
     * Event is triggered before unblocking existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_BEFORE_UNBLOCK = 'beforeUnblock';

    /**
     * Event is triggered after unblocking existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_AFTER_UNBLOCK = 'afterUnblock';

    /**
     * Name of the session key in which the original user id is saved
     * when using the impersonate user function.
     * Used inside actionSwitch().
     */
    const ORIGINAL_USER_SESSION_KEY = 'original_user';

    /** @var Finder */
    protected $finder;

    /**
     * @param string  $id
     * @param Module2 $module
     * @param Finder  $finder
     * @param array   $config
     */
    public function __construct($id, $module, Finder $finder, $config = [])
    {
        $this->finder = $finder;
        parent::__construct($id, $module, $config);
    }

    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete'  => ['post'],
                    'confirm' => ['post'],
                    'block'   => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        //'actions' => ['update','update-profile', 'info', 'index', 'create', 'assignments'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        Url::remember('', 'actions-redirect');
        $searchModel  = \Yii::createObject(UserSearch::className());
        $dataProvider = $searchModel->search(\Yii::$app->request->get());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel'  => $searchModel,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        /** @var User $user */
        $user = \Yii::createObject([
            'class'    => User::className(),
            'scenario' => 'create',
        ]);
        $event = $this->getUserEvent($user);
 
        $model = new AuthAssignment();
        $esta = new Estacion();
        $esta_usu = new EstacionUsuario();
 

        $this->performAjaxValidation($user);

        $this->trigger(self::EVENT_BEFORE_CREATE, $event);



        if ($user->load(\Yii::$app->request->post()) && $user->create()) {
             $user2 = $this->findModel($user->id);
            $user2->bandera = $_POST['User']['bandera'];
            print_r($user2->bandera);
            $user2->save(false);
            if($user2->bandera==0){

      
            \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'User has been created'));

            $this->trigger(self::EVENT_AFTER_CREATE, $event);
                        return $this->redirect(['update-profile', 'id' => $user->id]);
   
            }else{
                    $params_post = Yii::$app->getRequest()->getBodyParams();
         
            if ($params_post) {

 
            
                if(isset($params_post['EstacionUsuario'])){
            
                    foreach ($params_post['EstacionUsuario'] as $value) {

                        foreach ($value as  $value2) {
                                $esta_usu = new EstacionUsuario();

                               $esta_usu->estacion_id = $value2;
                               $esta_usu->user_id = $user->id;
                               $esta_usu->save(false);
                        }
                       
                         
                    }
                 }
            }
                 \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'User has been created'));
                 $this->trigger(self::EVENT_AFTER_CREATE, $event);
             return $this->redirect(['update-profile', 'id' => $user->id]);
            }
        } 
        $est = null;
        $est_sel = null;
        return $this->render('create', [
            'user' => $user,
            'esta' => $esta,
            'esta_usu' => $esta_usu,
            'est_sel' => $est_sel,
            'model' => $model,
        ]);
    }
/**
     * Obtener las estaciones de una agencia que tiene asociado un taller
     * @param integer $id
     * @return mixed
     */
    public function actionGetestaciones()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $id_cliente = $parents[0];

                $query = new query();
                $query->select(["est.id_estacion as id", "CONCAT(emp.nombre, ' - ', est.nombre) as name"])
                    ->from('estacion est')
                    ->join('INNER JOIN','empresa emp','emp.id_empresa = est.id_empresa')
                    ->where('emp.cliente_id = ' . "'".$id_cliente."'" );

                $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                $out = Yii::$app->db->createCommand($sql)->queryAll();

                echo Json::encode(['output'=>$out, 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }
    /**
     * Updates an existing User model.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        Url::remember('', 'actions-redirect');
        $user = $this->findModel($id);
        $si = 0;
        // $model = AuthAssignment::findOne(['item_name'=>$user->id]);
        $model = authAssignment::findOne(['user_id'=>$user->id]);
        $est_sel = 0;
        $esta = 0;
         $esta_usu = EstacionUsuario::findOne(['user_id'=>$user->id]);        
         if ($esta_usu==null) {
            $esta_usu = new EstacionUsuario();
         }

        $user->scenario = 'update';
        $event = $this->getUserEvent($user);

        $this->performAjaxValidation($user);

        $this->trigger(self::EVENT_BEFORE_UPDATE, $event);


        if ($user->load(\Yii::$app->request->post())) {

            $user2 = $this->findModel($user->id);

           if ((\Yii::$app->user->can('admin'))){
                # code...
            $user2->bandera = $_POST['User']['bandera'];
            }

 
            $user2->save(false);
      
            if ($user->save(false)) {

                if ($model) {
                 $model->user_id = $user->id;
            $model->created_at = 1504258089;
                }
             if ($user2->bandera==0 && (\Yii::$app->user->can('admin'))) {
                $estadoeliminar = EstacionUsuario::find()->where(['user_id'=>$user->id])->All(); 
                if ($estadoeliminar) {
                    foreach ($estadoeliminar as $key => $value) {
                    $value->delete();
                }
                }
            }else{

                
                 $params_post = Yii::$app->getRequest()->getBodyParams();
         
            if ($params_post  && (\Yii::$app->user->can('admin'))) {

                $estadoeliminar = EstacionUsuario::find()->where(['user_id'=>$user->id])->All();  

                foreach ($estadoeliminar as $key => $value) {
                  $value->delete();
                }
            
                if(isset($params_post['EstacionUsuario'])  && (\Yii::$app->user->can('admin'))){
            
                    foreach ($params_post['EstacionUsuario'] as $value) {

                  if ($value) {
                           foreach ($value as  $value2) {
                                $esta_usu = new EstacionUsuario();

                               $esta_usu->estacion_id = $value2;
                               $esta_usu->user_id = $user->id;
                              if ($esta_usu->save(false)) {
                                  $si = 1;
                               }  
                        }
                       
                         
                    }
                  }
                 }
            }
            }
                
                if ($model) {
                    if ($model->save(false) || $si==1 ) {
                \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'Account details have been updated'));
            $this->trigger(self::EVENT_AFTER_UPDATE, $event);
            // return $this->refresh(); 
              return $this->redirect(['update-profile', 'id' => $user->id]);
            if ($model->item_name=='proveedor') {

                   
                     return $this->redirect(URL::toRoute('//proveedorpieza/create'));
            }else{
                 return $this->redirect(['update-profile', 'id' => $user->id]);
            }
                 }   
                }


              if ($si==1 ) {
                \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'Account details have been updated'));
            $this->trigger(self::EVENT_AFTER_UPDATE, $event);
            // return $this->refresh(); 
              return $this->redirect(['update-profile', 'id' => $user->id]);
            if ($model->item_name=='proveedor') {

                   
                     return $this->redirect(URL::toRoute('//proveedorpieza/create'));
            }else{
                 return $this->redirect(['update-profile', 'id' => $user->id]);
            }
                 }  
            }
 
           
        }


        if ($user->bandera==1 ) {

        $est_sel = Estacion::find()
                    ->select('id_estacion')
                    ->where(['coordinador' => $user->id])
                    ->orderBy('nombre')
                    ->all();
        $est_sels = array();
 
        if (count($est_sels) > 0) {
            foreach ($est_sels as $aux) {
                $est_sel = array_merge($est_sel, array($aux['id_estacion']));
            }
        }
        $esta = Estacion::findOne(['coordinador'=>$user->id]);

        
      
         $query = new Query();
    $query -> select([  'estacion.id_estacion as id_est',
                        'concat(empresa.nombre, concat(" - ",estacion.nombre)) as est'])
            ->from('empresa')
            ->join('INNER JOIN','estacion','empresa.id_empresa = estacion.id_empresa')
            ->join('INNER JOIN','estacion_usuario','estacion_usuario.estacion_id = estacion.id_estacion')
            ->where(['estacion_usuario.user_id' => $user->id])
            ->orderBy(['empresa.nombre'=> SORT_ASC,'estacion.nombre'=> SORT_ASC]);
    $rows = $query->all();


    $esta_usu = ArrayHelper::map($rows, 'id_est','est');


    if ($rows) {
        # code...
    $esta_usu = EstacionUsuario::findOne(['estacion_id'=>$rows[0]['id_est']]);
    }
 
        }
        

 
        return $this->render('_account', [
            'user' => $user,
            'est_sel' => $est_sel,
             'model' => $model,
             'esta' => $esta,
             'esta_usu' => $esta_usu,
             
        ]);
    }



        /**
     * Updates an existing User model.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function actionRecuperar($id=null)
    {
      
     
        Url::remember('', 'actions-redirect');
        $user = $this->findModel($id);
        // $model = AuthAssignment::findOne(['item_name'=>$user->id]);
        $model = authAssignment::findOne(['user_id'=>$user->id]);
        $resp_ant = $user->respuesta;
  
        $user->scenario = 'update';
        $event = $this->getUserEvent($user);

        $this->performAjaxValidation($user);
 
        if ($user->load(\Yii::$app->request->post())) {

        if ($_POST['User']['respuesta'] == $resp_ant) {
           $user->resendPassword();
          \Yii::$app->getSession()->setFlash('success', '¡A su correo se envió la información de su contraseña!');
      
        }else{

            \Yii::$app->getSession()->setFlash('error', '¡Respuesta incorrecta, no se pudo recuperar su contraseña!');
        }
       
       
            return $this->refresh();
        }
        
        $est_sels = Estacion::find()
                    ->select('id_estacion')
                    ->where(['coordinador' => $user->id])
                    ->orderBy('nombre')
                    ->all();
        $est_sel = array();
        if (count($est_sels) > 0) {
            foreach ($est_sels as $aux) {
                $est_sel = array_merge($est_sel, array($aux['id_estacion']));
            }
        }
        return $this->render('_recuperar', [
            'user' => $user,
            'est_sel' => $est_sel,
             'model' => $model,
        ]);
    }

    /**
     * Updates an existing profile.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function actionUpdateProfile($id)
    {
        Url::remember('', 'actions-redirect');
        $user    = $this->findModel($id);
        $profile = $user->profile;

        if ($profile == null) {
            $profile = \Yii::createObject(Profile::className());
            $profile->link('user', $user);
        }
        $event = $this->getProfileEvent($profile);

        $this->performAjaxValidation($profile);

        $this->trigger(self::EVENT_BEFORE_PROFILE_UPDATE, $event);

        if ($profile->load(\Yii::$app->request->post()) && $profile->save()) {
            \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'Profile details have been updated'));
            $this->trigger(self::EVENT_AFTER_PROFILE_UPDATE, $event);
            // return $this->refresh();
              if ((\Yii::$app->user->can('admin'))){

                return $this->redirect(['assignments', 'id' => $user->id]);
              }else{
            return $this->redirect(['update-profile', 'id' => $user->id]);
              }
        }

        return $this->render('_profile', [
            'user'    => $user,
            'profile' => $profile,
        ]);
    }

    /**
     * Shows information about user.
     *
     * @param int $id
     *
     * @return string
     */
    public function actionInfo($id)
    {
        Url::remember('', 'actions-redirect');
        $user = $this->findModel($id);

        return $this->render('_info', [
            'user' => $user,
        ]);
    }

    /**
     * Switches to the given user for the rest of the Session.
     * When no id is given, we switch back to the original admin user
     * that started the impersonation.
     *
     * @param int $id
     *
     * @return string
     */
    public function actionSwitch($id = null)
    {
        if (!$this->module->enableImpersonateUser) {
            throw new ForbiddenHttpException(Yii::t('user', 'Impersonate user is disabled in the application configuration'));
        }

        if(!$id && Yii::$app->session->has(self::ORIGINAL_USER_SESSION_KEY)) {
            $user = $this->findModel(Yii::$app->session->get(self::ORIGINAL_USER_SESSION_KEY));

            Yii::$app->session->remove(self::ORIGINAL_USER_SESSION_KEY);
        } else {
            if (!Yii::$app->user->identity->isAdmin) {
                throw new ForbiddenHttpException;
            }

            $user = $this->findModel($id);
            Yii::$app->session->set(self::ORIGINAL_USER_SESSION_KEY, Yii::$app->user->id);
        }

        $event = $this->getUserEvent($user);

        $this->trigger(self::EVENT_BEFORE_IMPERSONATE, $event);
        
        Yii::$app->user->switchIdentity($user, 3600);
        
        $this->trigger(self::EVENT_AFTER_IMPERSONATE, $event);

        return $this->goHome();
    }

    /**
     * If "dektrium/yii2-rbac" extension is installed, this page displays form
     * where user can assign multiple auth items to user.
     *
     * @param int $id
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionAssignments($id)
    {
        if (!isset(\Yii::$app->extensions['dektrium/yii2-rbac'])) {
            throw new NotFoundHttpException();
        }
        Url::remember('', 'actions-redirect');
        $user = $this->findModel($id);
        $params_post = Yii::$app->getRequest()->getBodyParams();
        if ($params_post) {
            $estaciones = Estacion::find()
                ->where(['coordinador' => $id])
                ->all();
            if($estaciones){
                foreach ($estaciones as $estac) {
                    $estac->coordinador = null;
                    $estac->save();
                }
            }
            if(isset($params_post['id_estacion']) && in_array('coord-flota', $params_post['Assignment']['items'])){
                foreach ($params_post['id_estacion'] as $value) {
                    $estacion = Estacion::findOne($value);
                    $estacion->coordinador = $id;
                    $estacion->save();
                }
            }
        }

        return $this->render('_assignments', [
            'user' => $user,
        ]);
    }

    /**
     * Confirms the User.
     *
     * @param int $id
     *
     * @return Response
     */
    public function actionConfirm($id)
    {
        $model = $this->findModel($id);
        $event = $this->getUserEvent($model);

        $this->trigger(self::EVENT_BEFORE_CONFIRM, $event);
        $model->confirm();
        $this->trigger(self::EVENT_AFTER_CONFIRM, $event);

        \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'User has been confirmed'));

        return $this->redirect(Url::previous('actions-redirect'));
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        if ($id == \Yii::$app->user->getId()) {
            \Yii::$app->getSession()->setFlash('danger', \Yii::t('user', 'You can not remove your own account'));
        } else {
            $estaciones = Estacion::find()
                ->where(['coordinador' => $id])
                ->all();
            if($estaciones){
                foreach ($estaciones as $estac) {
                    $estac->coordinador = null;
                    $estac->save();
                }
            }
            $model = $this->findModel($id);
            $event = $this->getUserEvent($model);
            $this->trigger(self::EVENT_BEFORE_DELETE, $event);
            $model->delete();
            $this->trigger(self::EVENT_AFTER_DELETE, $event);
            \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'User has been deleted'));
        }

        return $this->redirect(['index']);
    }

    /**
     * Blocks the user.
     *
     * @param int $id
     *
     * @return Response
     */
    public function actionBlock($id)
    {
        if ($id == \Yii::$app->user->getId()) {
            \Yii::$app->getSession()->setFlash('danger', \Yii::t('user', 'You can not block your own account'));
        } else {
            $user  = $this->findModel($id);
            $event = $this->getUserEvent($user);
            if ($user->getIsBlocked()) {
                $this->trigger(self::EVENT_BEFORE_UNBLOCK, $event);
                $user->unblock();
                $this->trigger(self::EVENT_AFTER_UNBLOCK, $event);
                \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'User has been unblocked'));
            } else {
                $this->trigger(self::EVENT_BEFORE_BLOCK, $event);
                $user->block();
                $this->trigger(self::EVENT_AFTER_BLOCK, $event);
                \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'User has been blocked'));
            }
        }

        return $this->redirect(Url::previous('actions-redirect'));
    }

    /**
     * Generates a new password and sends it to the user.
     *
     * @return Response
     */
    public function actionResendPassword($id)
    {
        $user = $this->findModel($id);
        if ($user->isAdmin) {
            throw new ForbiddenHttpException(Yii::t('user', 'Password generation is not possible for admin users'));
        }

        if ($user->resendPassword()) {
            Yii::$app->session->setFlash('success', \Yii::t('user', 'New Password has been generated and sent to user'));
        } else {
            Yii::$app->session->setFlash('danger', \Yii::t('user', 'Error while trying to generate new password'));
        }

        return $this->redirect(Url::previous('actions-redirect'));
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param int $id
     *
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $user = $this->finder->findUserById($id);
        if ($user === null) {
            throw new NotFoundHttpException('The requested page does not exist');
        }

        return $user;
    }

    /**
     * Performs AJAX validation.
     *
     * @param array|Model $model
     *
     * @throws ExitException
     */
    protected function performAjaxValidation($model)
    {
        if (\Yii::$app->request->isAjax && !\Yii::$app->request->isPjax) {
            if ($model->load(\Yii::$app->request->post())) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                echo json_encode(ActiveForm::validate($model));
                \Yii::$app->end();
            }
        }
    }
}
