<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace dektrium\user\controllers;

use dektrium\user\Finder;
use dektrium\user\models\Account;
use dektrium\user\models\LoginForm;
use dektrium\user\models\User;
use app\models\UserSearch;
use app\models\AuthAssignment;
use dektrium\user\models\Recuperarpass;
use dektrium\user\models\Registroprov;
use dektrium\user\Module;
use dektrium\user\traits\AjaxValidationTrait;
use dektrium\user\traits\EventTrait;
use yii\authclient\AuthAction;
use yii\authclient\ClientInterface;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use kartik\growl\Growl;
use dektrium\user\helpers\Password;
/**
 * Controller that manages user authentication process.
 *
 * @property Module $module
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class SecurityController extends Controller
{
    use AjaxValidationTrait;
    use EventTrait;

    /**
     * Event is triggered before logging user in.
     * Triggered with \dektrium\user\events\FormEvent.
     */
    const EVENT_BEFORE_LOGIN = 'beforeLogin';

    /**
     * Event is triggered after logging user in.
     * Triggered with \dektrium\user\events\FormEvent.
     */
    const EVENT_AFTER_LOGIN = 'afterLogin';

    /**
     * Event is triggered before logging user out.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_BEFORE_LOGOUT = 'beforeLogout';

    /**
     * Event is triggered after logging user out.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_AFTER_LOGOUT = 'afterLogout';

    /**
     * Event is triggered before authenticating user via social network.
     * Triggered with \dektrium\user\events\AuthEvent.
     */
    const EVENT_BEFORE_AUTHENTICATE = 'beforeAuthenticate';

    /**
     * Event is triggered after authenticating user via social network.
     * Triggered with \dektrium\user\events\AuthEvent.
     */
    const EVENT_AFTER_AUTHENTICATE = 'afterAuthenticate';

    /**
     * Event is triggered before connecting social network account to user.
     * Triggered with \dektrium\user\events\AuthEvent.
     */
    const EVENT_BEFORE_CONNECT = 'beforeConnect';

    /**
     * Event is triggered before connecting social network account to user.
     * Triggered with \dektrium\user\events\AuthEvent.
     */
    const EVENT_AFTER_CONNECT = 'afterConnect';


    /** @var Finder */
    protected $finder;

    /**
     * @param string $id
     * @param Module $module
     * @param Finder $finder
     * @param array  $config
     */
    public function __construct($id, $module, Finder $finder, $config = [])
    {
        $this->finder = $finder;
        parent::__construct($id, $module, $config);
    }

    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    ['allow' => true, 'actions' => ['login', 'auth'], 'roles' => ['?']],
                    ['allow' => true, 'actions' => ['login', 'auth', 'logout'], 'roles' => ['@']],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /** @inheritdoc */
    public function actions()
    {
        return [
            'auth' => [
                'class' => AuthAction::className(),
                // if user is not logged in, will try to log him in, otherwise
                // will try to connect social account to user.
                'successCallback' => \Yii::$app->user->isGuest
                    ? [$this, 'authenticate']
                    : [$this, 'connect'],
            ],
        ];
    }

    /**
     * Displays the login page.
     *
     * @return string|Response
     */
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            $this->goHome();
        }

        $recuperar = new Recuperarpass();

        $proveedor = new Registroprov();

        $usuario = new UserSearch();

        $rol = new AuthAssignment();

        /** @var LoginForm $model */
        $model = \Yii::createObject(LoginForm::className());
        $event = $this->getFormEvent($model);

        $this->performAjaxValidation($model);

        $this->trigger(self::EVENT_BEFORE_LOGIN, $event);

        if( $proveedor->load(\Yii::$app->request->post() ) )
        {

                $usuario->username = $proveedor->username;
                $usuario->email=$proveedor->email;
                $usuario->password_hash = Password::hash($proveedor->password);
                $usuario->auth_key=0;
                $usuario->created_at=0;
                $usuario->updated_at=0;
                $usuario->flags=0;
                $usuario->cargo_gerencial=0;
                $usuario->cargo=0;
                // $usuario->id_proveedor = $modelprov->id;
                $usuario->confirmed_at = 1491044212;
                if ($proveedor->password == $proveedor->password2) {
                if ($usuario->save(false)) {
                  $rol->item_name = 'proveedor';
                  $rol->user_id = $usuario->id;
                  $rol->save(false);
                  $correo_envio = $proveedor->email;
                  $login =  $usuario->username;

                }
                       if($this->notificacion($proveedor,$proveedor->password, $usuario)){
                          echo Growl::widget([
                                'type' => Growl::TYPE_SUCCESS,
                                'title' => 'Proveedor Registrado con éxito!',
                                'icon' => 'glyphicon glyphicon-ok-sign',
                                'body' => 'Tu contraseña fue enviada al correo',
                                'showSeparator' => true,
                                'delay' => 1000,
                                'pluginOptions' => [
                                    'showProgressbar' => false,
                                    'placement' => [
                                        'from' => 'top',
                                        'align' => 'right',
                                    ]
                                ]
                            ]);
                    }
                }else{
                     echo Growl::widget([
                                'type' => Growl::TYPE_DANGER,
                                'title' => 'Error en contraseñas!',
                                'icon' => 'glyphicon glyphicon-ok-sign',
                                'body' => 'Las contraseñas no son iguales',
                                'showSeparator' => true,
                                'delay' => 1000,
                                'pluginOptions' => [
                                    'showProgressbar' => false,
                                    'placement' => [
                                        'from' => 'top',
                                        'align' => 'right',
                                    ]
                                ]
                            ]);
                }
                 
                

               
               return $this->render('login', [
            'model'  => $model,
            'module' => $this->module,
            'recuperar' => $recuperar,
            'proveedor' => $proveedor,
             
        ]);
                   
        }


         if( $recuperar->load(\Yii::$app->request->post() ) )
        {
            $username = $recuperar->username;
            $pregunta = $recuperar->pregunta;
            $respuesta = $recuperar->respuesta;


            $usuario = User::findOne(['username'=>$username]);
            if (!$usuario) {
                    echo Growl::widget([
                    'type' => Growl::TYPE_DANGER,
                    'title' => 'No se envió su contraseña!',
                    'icon' => 'glyphicon glyphicon-info-sign',
                    'body' => 'El usuario no se encuentra registrado.',
                    'showSeparator' => true,
                    'delay' => 1000,
                    'pluginOptions' => [
                        'showProgressbar' => false,
                        'placement' => [
                            'from' => 'top',
                            'align' => 'right',
                        ]
                    ]
                ]);


                
                        return $this->render('login', [
                        'model'  => $model,
                        'module' => $this->module,
                        'recuperar' => $recuperar,
                        'proveedor' => $proveedor,
                         
                    ]);
            }else{

               if ($usuario->pregunta == $pregunta) {

                 if ($usuario->respuesta != $respuesta) {
                              echo Growl::widget([
                                    'type' => Growl::TYPE_DANGER,
                                    'title' => 'No se envió su contraseña!',
                                    'icon' => 'glyphicon glyphicon-info-sign',
                                    'body' => 'Respuesta incorrecta.',
                                    'showSeparator' => true,
                                    'delay' => 1000,
                                    'pluginOptions' => [
                                        'showProgressbar' => false,
                                        'placement' => [
                                            'from' => 'top',
                                            'align' => 'right',
                                        ]
                                    ]
                                ]);
                     return $this->render('login', [
                            'model'  => $model,
                            'module' => $this->module,
                            'recuperar' => $recuperar,
                            'proveedor' => $proveedor,
                              ]);
                  }
                 if ($usuario->respuesta == $respuesta) {
                             echo Growl::widget([
                                'type' => Growl::TYPE_SUCCESS,
                                'title' => 'Contraseña Recuperada con éxito!',
                                'icon' => 'glyphicon glyphicon-ok-sign',
                                'body' => 'Tu contraseña fue enviada al correo',
                                'showSeparator' => true,
                                'delay' => 1000,
                                'pluginOptions' => [
                                    'showProgressbar' => false,
                                    'placement' => [
                                        'from' => 'top',
                                        'align' => 'right',
                                    ]
                                ]
                            ]);
                 
                      $usuario->resendPassword();

                       return $this->render('login', [
                            'model'  => $model,
                            'module' => $this->module,
                            'recuperar' => $recuperar,  
                            'proveedor' => $proveedor,   
                        ]);
         }
         
    } else{
                        echo Growl::widget([
                        'type' => Growl::TYPE_DANGER,
                        'title' => 'No se envió su contraseña!',
                        'icon' => 'glyphicon glyphicon-info-sign',
                        'body' => 'Pregunta incorrecta.',
                        'showSeparator' => true,
                        'delay' => 1000,
                        'pluginOptions' => [
                            'showProgressbar' => false,
                            'placement' => [
                                'from' => 'top',
                                'align' => 'right',
                            ]
                        ]
                    ]);

                        

                }


            }
     
             return $this->render('login', [
            'model'  => $model,
            'module' => $this->module,
            'recuperar' => $recuperar,
             'proveedor' => $proveedor,
        ]);
     
           
 
        }


        if ($model->load(\Yii::$app->getRequest()->post()) && $model->login()) {

            $this->trigger(self::EVENT_AFTER_LOGIN, $event);
            return $this->goBack();
        }

        return $this->render('login', [
            'model'  => $model,
            'module' => $this->module,
            'recuperar' => $recuperar,
            'proveedor' => $proveedor,
             
        ]);
    }
         
    //correo enviado a proveedor

         public function notificacion($model,$password, $usuario)
    {
      $envio = 0;
        $mensaje_sup ='  <p> Hola '.$model->username.'<br>
                        Km100 le informa que se ha realizado exitosamente el Registro de un Proveedor.
                    </p>
                    <p>
                        <strong>Usuario: '.$usuario->username.'</strong><br>
                        <strong>Contraseña: </strong>: '.$password.'<br>
                         
                    </p>
                    <br>
                    <hr>
                    <p style="color:#999;">Favor <strong>no</strong> responder este correo.</p>
        ';
        if (!\Yii::$app  ->mailer->compose()
                        ->setFrom('notificaciones.km100@gmail.com')
                        ->setTo($model->email)
                        ->setSubject('Km100 - Notificación Registro de Taller')
                        ->setHtmlBody($mensaje_sup)
                        ->send())
        {
            $envio=0;
        }
     

      if ($envio==0){
            return true;
        }else{
            return false;
        }
    }

        public function actionResendPassword($id)
    {
        $user = $this->findModel($id);
        if ($user->isAdmin) {
            throw new ForbiddenHttpException(Yii::t('user', 'Password generation is not possible for admin users'));
        }

        if ($user->resendPassword()) {
            Yii::$app->session->setFlash('success', \Yii::t('user', 'New Password has been generated and sent to user'));
        } else {
            Yii::$app->session->setFlash('danger', \Yii::t('user', 'Error while trying to generate new password'));
        }

        return $this->redirect(Url::previous('actions-redirect'));
    }
    /**
     * Logs the user out and then redirects to the homepage.
     *
     * @return Response
     */
    public function actionLogout()
    {
        $event = $this->getUserEvent(\Yii::$app->user->identity);

        $this->trigger(self::EVENT_BEFORE_LOGOUT, $event);

        \Yii::$app->getUser()->logout();

        $this->trigger(self::EVENT_AFTER_LOGOUT, $event);

        return $this->goHome();
    }

    /**
     * Tries to authenticate user via social network. If user has already used
     * this network's account, he will be logged in. Otherwise, it will try
     * to create new user account.
     *
     * @param ClientInterface $client
     */
    public function authenticate(ClientInterface $client)
    {
        $account = $this->finder->findAccount()->byClient($client)->one();

        if (!$this->module->enableRegistration && ($account === null || $account->user === null)) {
            \Yii::$app->session->setFlash('danger', \Yii::t('user', 'Registration on this website is disabled'));
            $this->action->successUrl = Url::to(['/user/security/login']);
            return;
        }

        if ($account === null) {
            /** @var Account $account */
            $accountObj = \Yii::createObject(Account::className());
            $account = $accountObj::create($client);
        }

        $event = $this->getAuthEvent($account, $client);

        $this->trigger(self::EVENT_BEFORE_AUTHENTICATE, $event);

        if ($account->user instanceof User) {
            if ($account->user->isBlocked) {
                \Yii::$app->session->setFlash('danger', \Yii::t('user', 'Your account has been blocked.'));
                $this->action->successUrl = Url::to(['/user/security/login']);
            } else {
                \Yii::$app->user->login($account->user, $this->module->rememberFor);
                $this->action->successUrl = \Yii::$app->getUser()->getReturnUrl();
            }
        } else {
            $this->action->successUrl = $account->getConnectUrl();
        }

        $this->trigger(self::EVENT_AFTER_AUTHENTICATE, $event);
    }

    /**
     * Tries to connect social account to user.
     *
     * @param ClientInterface $client
     */
    public function connect(ClientInterface $client)
    {
        /** @var Account $account */
        $account = \Yii::createObject(Account::className());
        $event   = $this->getAuthEvent($account, $client);

        $this->trigger(self::EVENT_BEFORE_CONNECT, $event);

        $account->connectWithUser($client);

        $this->trigger(self::EVENT_AFTER_CONNECT, $event);

        $this->action->successUrl = Url::to(['/user/settings/networks']);
    }
}
