<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

/**
 * @var $dataProvider array
 * @var $filterModel  dektrium\rbac\models\Search
 * @var $this         yii\web\View
 */


use yii\helpers\Html;
use kartik\select2\Select2;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

$this->title = Yii::t('rbac', 'LISTADO DE ROLES');
?>

<?php $this->beginContent('@dektrium/rbac/views/layout.php') ?>
<?php Pjax::begin() ?>

  	<div class="row">
  		<div class="col-md-1 col-xs-3">
  			<h4 class="titulo"><i class="fa fa-user-secret fa-4x" aria-hidden="true"></i></h4>
  		</div>
  		<div class="col-md-10 col-xs-9" style="padding-top: 30px;">
  			<h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
  		</div>
  	</div>
  	</br>
    <div class="row">
        <div class="col-md-12">
            <?php
                echo Html::a('Registrar Rol', ['create'], ['class' => 'btn btn-small-gris', 'style' => 'float:right;']);
            ?>
        </div>
    </div>
    <br>
    <div class="table-responsive" style="padding-bottom:80px;">

      <?= GridView::widget([
          'dataProvider' => $dataProvider,
          'filterModel'  => $filterModel,
          'layout'       => "{items}\n{pager}",
          'filterPosition'=>' ',
          'summary' => '',
          'options' =>[
              'class' => 'grid',
          ],
          'columns'      => [
              [
                  'attribute' => 'name',
                  'header'    => Yii::t('rbac', 'Nombre'),
                  'options'   => [
                      'style' => 'width: 20%'
                  ],
                  /*'filter' => Select2::widget([
                      'model'     => $filterModel,
                      'attribute' => 'name',
                      'data'      => $filterModel->getNameList(),
                      'options'   => [
                          'placeholder' => Yii::t('rbac', 'Select role'),
                      ],
                      'pluginOptions' => [
                          'allowClear' => true,
                      ],
                  ]),*/
              ],
              [
                  'attribute' => 'rule_name',
                  'header'    => Yii::t('rbac', 'Nombre de regla'),
                  'options'   => [
                      'style' => 'width: 20%'
                  ],
                  'filter' => Select2::widget([
                      'model'     => $filterModel,
                      'attribute' => 'rule_name',
                      'data'      => $filterModel->getRuleList(),
                      'options'   => [
                          'placeholder' => Yii::t('rbac', 'Select rule'),
                      ],
                      'pluginOptions' => [
                          'allowClear' => true,
                      ],
                  ]),
              ],
              [
                  'attribute' => 'description',
                  'header'    => Yii::t('rbac', 'Descripción'),
                  'options'   => [
                      'style' => 'width: 55%',
                  ],
                  'filterInputOptions' => [
                      'class'       => 'form-control',
                      'id'          => null,
                      'placeholder' => Yii::t('rbac', 'Enter the description')
                  ],
              ],
              [
                  'class' => 'yii\grid\ActionColumn',
                  'header'=>'',
                  'template' => '{menu}',
                  'buttons' => [
                      'menu' => function ($url, $model) {
                           return  '<div class="dropdown">
                                    <button class="btn btn-default dropdown-toggle btn-sm" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i>
                                    <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                      <li>'.Html::a('<span><i class="fa fa-pencil fa-sm" aria-hidden="true"></i></span> Modificar rol',[ '/rbac/role/update', 'name' => $model['name']]).' </li>
                                      <li>'.Html::a('<span><i class="fa fa-trash-o fa-sm" aria-hidden="true"></i></span> Eliminar rol', ['/rbac/role/delete', 'name' => $model['name']], [
                                               'title' => Yii::t('user', 'Eliminar rol'),
                                               'data-confirm' => Yii::t('user', 'Esta seguro de eliminar este rol?'),
                                               'data-method' => 'POST',
                                           ]).' </li>
                                    </ul>
                                  </div>';


                      },
                  ],
             ],
          ],
      ]) ?>
</div>
<?php Pjax::end() ?>

<?php $this->endContent() ?>
