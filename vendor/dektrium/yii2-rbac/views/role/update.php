<?php

use yii\helpers\Html;
/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

/**
 * @var $model dektrium\rbac\models\Role
 * @var $this  yii\web\View
 */

$this->title = Yii::t('rbac', 'MODIFICAR ROL');
?>

<?php $this->beginContent('@dektrium/rbac/views/layout.php') ?>

  	<div class="row">
  		<div class="col-md-1 col-xs-3">
  			<h4 class="titulo"><i class="fa fa-user-secret fa-4x" aria-hidden="true"></i></h4>
  		</div>
  		<div class="col-md-10 col-xs-9" style="padding-top: 30px;">
  			<h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
  		</div>
  	</div>
  	</br>
    <div class="row" style="border-bottom: 1px solid #222d32;">
          <div class="col-xs-12 col-md-6">
              <h4>DATOS DEL ROL</h4>
          </div>
          <div class="col-xs-12 col-md-6">
              <?php
                  echo Html::a('Listado de roles', ['index'], ['class' => 'btn btn-accion', 'style' => 'float:right;']);
              ?>
          </div>
    </div>
    <br>
<?= $this->render('_form', [
    'model' => $model,
]) ?>

<?php $this->endContent() ?>
