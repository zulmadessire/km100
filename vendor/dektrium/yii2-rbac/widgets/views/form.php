<?php

/*
 * This file is part of the Dektrium project
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use app\models\Estacion;
use app\models\User;
use dektrium\rbac\models\Assignment;
use kartik\select2\Select2;
use yii\bootstrap\Alert;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var $model Assignment
 */
?>

<?php if ($model->updated): ?>

    <?=Alert::widget([
        'options' => [
            'class' => 'alert-success',
        ],
        'body'    => 'Las Asignaciones han sido actualidas',
    ])?>

<?php endif?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'enableAjaxValidation'   => false,
])?>

    <?=Html::activeHiddenInput($model, 'user_id')?>

    <?=$form->field($model, 'items')->widget(Select2::className(), [
        'data'    => $model->getAvailableItems(),
        'options' => [
            'id'       => 'items',
            'multiple' => true,
        ],
    ])->label('Asignaciones')?>

    <br>
    <div class="form-group" align="center">
        <?=Html::submitButton('Actualizar Asignaciones', ['class' => 'btn btn-primario'])?>
    </div>

<?php ActiveForm::end()?>