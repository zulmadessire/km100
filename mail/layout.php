<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>

<html>
    <body>
        <div style="background: #3c3c3b; padding:30px;">
		    <div style="width: 100%; border-radius: 8px; max-width: 550px; background-color: #fff; margin: 0 auto;">
		        <div style="text-align: center; padding-top: 20px;">
			     	<a href="http://<?= Yii::$app->getRequest()->serverName.Yii::$app->homeUrl ?>" target="_blank">
			            <img src="http://54.175.105.217/desarrollokm100/img/logo.png" width="30%">
			        </a>
			    </div>
		        <div style="padding-left: 35px; padding-right: 35px; padding-bottom: 20px;">
		            <div style="padding-bottom: 20px; width: 100%; border: 2px solid #ffffff; border-radius: 8px; max-width: 550px; background-color: #fff; margin: 0 auto; display: inline-block;">
		                <div style="width: 90%; position: relative; min-height: 1px; padding-left: 15px; padding-right: 15px; font-family: 'Open Sans',Arial,sans-serif; margin: 0 auto; left: 0;">
		                    <div style="color: #3f4652; font-size: 24px; font-family: 'Calibri', 'Helvetica','Arial',sans-serif !important; text-align: center; line-height: 32px; text-transform: uppercase;">
		                        <strong>¡Hola!</strong>
		                    </div>
		                    <div style="width: 90%; position: relative; min-height: 1px; padding-left: 15px; padding-right: 15px; float: left; font-family: 'Calibri', 'Helvetica','Arial',sans-serif !important;">
		                        <div style="padding:10px; widht:80%; font-size: 16px;">
		                        	<?= $content ?>
		                            <p>Puede acceder al sistema a través del siguiente enlace: <a href="http://<?= Yii::$app->getRequest()->serverName.Yii::$app->homeUrl ?>" target="_blank"><strong><?= Yii::$app->getRequest()->serverName.Yii::$app->homeUrl ?></strong></a></p>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>
		        <div style="width: 90%; margin:0 auto; margin-top:30px; padding:10px; background: white; font-family: 'Open Sans',Arial,sans-serif; font-weight: 100; text-align: center; color: #3c3c3b; font-size: 12px;">
		            © <?= Yii::$app->name ?> <?= date('Y') ?>.
		        </div>
		    </div>
		</div>
    </body>
</html>