<?php

namespace app\controllers;

use Yii;
use app\models\Taller;
use app\models\TallerSearch;
use app\models\Marca;
use app\models\Modelo;
use app\models\UserSearch;
use app\models\AuthAssignment;
use app\models\ServicioTaller;
use app\models\VehiculoTaller;
use app\models\UploadAcuerdoTaller;
use app\models\UploadRegistroMercantil;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use dektrium\user\helpers\Password;


/**
 * TallerController implements the CRUD actions for Taller model.
 */
class TallerController extends Controller
{
    /**
     * @inheritdoc
     */
    public $basePath ='./uploads/talleres/';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Taller models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TallerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Taller model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionDescargar($dir)
    {
        return $this->redirect(Yii::$app->request->baseUrl.$dir);
    }

    /**
     * Creates a new Taller model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
    //print_r($_POST);
    //die();
        $model = new Taller();
        $modelmodelo = [new Modelo()];
        $modelservicio = [new ServicioTaller()];
        $acuerdo = new UploadAcuerdoTaller();
        $mercantil = new UploadRegistroMercantil();
        $usuario = new UserSearch();
        $rol = new AuthAssignment();

        $length = 10;
        $chars = array_merge(range(0,9), range('a','z'), range('A','Z'));
        shuffle($chars);
        $password = implode(array_slice($chars, 0, $length));




        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
          //print_r($_POST); 
          $band = 0;
            $model->user = 'rsa.' . $model->user;

          if($model->save(false))
          {
            $path = $this->basePath . $model->id_taller . '/';
            if ( !is_dir($path) ) {
                mkdir( $path, 0777, true );
            }

            // Guardo el acuerdo comercial
            $acuerdo->imageFiles = UploadedFile::getInstances($acuerdo, 'imageFiles');
            $acuerdo->upload($model->id_taller);

            $mercantil->imageFiles = UploadedFile::getInstances($mercantil, 'imageFiles');
            $mercantil->upload($model->id_taller);

            //Asociar Modelos de Vehiculos
            if (isset($_POST['Modelo'][0]['id_modelo'])) {
                foreach ($_POST['Modelo'] as $tall) {
                    $nuevo_vehi_taller = new VehiculoTaller();
                    $nuevo_vehi_taller->id_modelo = $tall['id_modelo'];
                    $nuevo_vehi_taller->id_taller = $model->id_taller;
                    if(!$nuevo_vehi_taller->save()){
                      $band = 1;
                    }
                }
            }

            if($band == 1){
                \Yii::$app->getSession()->setFlash('error', '¡Error en la creación del Taller!');
            }
            \Yii::$app->getSession()->setFlash('success', '¡El Taller ha sido creado con éxito!');
          }else{

            print_r($model->getErrors());
            die();

          }


          $usuario->username = $model->user;
          $usuario->email=$model->email_representante;
          $usuario->password_hash = Password::hash($password);
          $usuario->auth_key=0;
          $usuario->created_at=0;
          $usuario->updated_at=0;
          $usuario->flags=0;
          $usuario->cargo_gerencial=0;
          $usuario->cargo=0;
          $usuario->id_taller = $model->id_taller;
          $usuario->confirmed_at = 1491044212;
          $usuario->save(false);

          $rol->item_name = 'taller';
          $rol->user_id = $usuario->id;
          $rol->save(false);

          //enviar correo

          $correo_envio = $model->email_representante;
          $login =  $usuario->username;

              if(!$this->notificacion($model,$password, $usuario)){
                        \Yii::$app->getSession()->setFlash('error', '¡Error en el envío de notificación!');
                    }

          // print_r($password);
           return $this->redirect(['view', 'id' => $model->id_taller]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'modelmodelo' => (empty($modelmodelo)) ? [new Modelo] : $modelmodelo,
                'modelservicio' => (empty($modelservicio)) ? [new ServicioTaller] : $modelservicio,
                'acuerdo' => $acuerdo,
                'mercantil' => $mercantil
            ]);
        }
     
      
    }

    /**
     * Updates an existing Taller model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */

     public function notificacion($model,$password, $usuario)
    {
      $envio = 0;
        $mensaje_sup ='  <p> Km100 le informa que ha sido registrado exitosamente como RSA:
                    </p>
                    <p>
                        <strong>Nombre de Usuario: </strong>'.$usuario->username.'<br>
                        <strong>Clave de Acceso: </strong>'.$password.'<br>
                         
                    </p>
        ';
        if (!Yii::$app->mailer->compose(['html'=>'layout'], ['content'=> $mensaje_sup])
                        ->setFrom([Yii::$app->params['adminEmail'] => 'KM100'])
                        ->setTo($model->email_taller,$model->email_representante)
                        ->setSubject('Notificación de Registro')
                        ->send())
        {
            $envio=0;
        }
     

      if ($envio==0){
            return true;
        }else{
            return false;
        }
    }


    public function actionUpdate($id)
    {
        // print_r($_POST);
        // die();
        $model = $this->findModel($id);
        $modelmodelo = Modelo::find()
            ->joinWith('vehiculoTallers')
            ->where([ 'vehiculo_taller.id_taller' => $id])
            ->all();
        //$modelservicio = [new ServicioTaller()];
        $acuerdo = new UploadAcuerdoTaller();
        $mercantil = new UploadRegistroMercantil();

        if ($model->load(Yii::$app->request->post())  ) {
            $model->update();  

            // Guardo el acuerdo comercial
            $acuerdo->imageFiles = UploadedFile::getInstances($acuerdo, 'imageFiles');
            $acuerdo->upload($model->id_taller);

            $mercantil->imageFiles = UploadedFile::getInstances($mercantil, 'imageFiles');
            $mercantil->upload($model->id_taller);
                      
            \Yii::$app->getSession()->setFlash('success', 'La solicitud esta Finalizada');
            return $this->redirect(['view', 'id' => $model->id_taller]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelmodelo' => (empty($modelmodelo)) ? [new Modelo] : $modelmodelo,
                //'modelservicio' => (empty($modelservicio)) ? [new ServicioTaller] : $modelservicio,
                'acuerdo' => $acuerdo,
                'mercantil' => $mercantil
            ]);
        }
    }

    public function actionAsociar($id)
    {
    
       $model = $this->findModel($id);
       $modelservicio = ServicioTaller::find()
           ->joinWith('taller')
           ->where([ 'taller.id_taller' => $id])
           ->all();
        if(!$modelservicio){
          $modelservicio = [new ServicioTaller()];
        }

       if ($model->load(Yii::$app->request->post())) {
         $band = 0;
         $servicios_asig = ServicioTaller::find()->where(['id_taller'=>$model->id_taller])->all();
         foreach ($servicios_asig as $asig) {
           $asig->delete();
         }
         //Asociar Servicios de taller
         //Se elimino el registro de la linea
         if (isset($_POST['ServicioTaller'][0]['decimal'])) {
             foreach ($_POST['ServicioTaller'] as $servi) {
                 $nuevo_servicio = new ServicioTaller();
                 //$nuevo_servicio->garantia = $servi['garantia'];
                 $nuevo_servicio->garantia = 0;
                 $nuevo_servicio->decimal = $servi['decimal'];
                  $nuevo_servicio->labor = $servi['labor'];
                 $nuevo_servicio->id_tipo_servicio = $servi['id_tipo_servicio'];
                 $nuevo_servicio->id_taller = $model->id_taller;
                 if(!$nuevo_servicio->save()){
                   $band = 1;
                 }
             }
         }
         if($band == 1){
             \Yii::$app->getSession()->setFlash('error', '¡Error en la asignacion de Servicios al Taller!');
         }
         \Yii::$app->getSession()->setFlash('success', '¡Las asignaciones han sido realizadas con éxito!');
         return $this->redirect(['view', 'id' => $model->id_taller]);
       } else {
           return $this->render('_form-servicios', [
               'model' => $model,
               'modelservicio' => (empty($modelservicio)) ? [new ServicioTaller] : $modelservicio,
           ]);
       }
    }
    /**
     * Deletes an existing Taller model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Taller model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Taller the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Taller::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
