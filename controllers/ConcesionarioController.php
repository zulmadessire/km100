<?php

namespace app\controllers;

use Yii;
use app\models\Concesionario;
use app\models\ConcesionarosSearch;
use yii\web\Controller;
use yii\db\Query;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Response;

use yii\widgets\ActiveForm;
use app\models\SucursalConcesionario;
use app\models\DetalleGarantia;
use app\models\PartesGarantia;
use app\models\Marca;
use app\models\Modelo;
use app\models\Model;
use app\models\Partes;
use app\models\SucursalesSearch;
 


/**
 * ConcesionarioController implements the CRUD actions for Concesionario model.
 */
class ConcesionarioController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Concesionario models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ConcesionarosSearch();
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Concesionario model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        //Datos del concesionario
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Concesionario model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
 




    public function actionCreate()
    {
        // print_r($_POST);
        // die('fin');
        $model = new Concesionario;
        $model_partes = new Partes;
        $model_sucursal = [new SucursalConcesionario];
        $model_detalle_garantia = [new DetalleGarantia];
        $model_partes_garantia = [new PartesGarantia];


        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

  
            $model_sucursal = Model::createMultiple(SucursalConcesionario::classname());
            Model::loadMultiple($model_sucursal, Yii::$app->request->post());

             if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validateMultiple($model_sucursal);
            }
 

            //Se guarda el concesionario
            if ($model->save(false)) {
                //Se guardan las sucursales
                foreach ($model_sucursal as $key => $sucursal) {
                    $sucursal->concesionario_id = $model->id;
                    if ( !( $bandera = $sucursal->save(false) ) ) {
                        break;
                    }
                }

                if ( $bandera ) {
                    //Se registran las garantias
                    $model_detalle_garantia = Model::createMultiple(DetalleGarantia::classname());
                    Model::loadMultiple($model_detalle_garantia, Yii::$app->request->post());
                    $aux_partes = Yii::$app->request->post('PartesGarantia');
            
                    foreach ($model_detalle_garantia as $key => $detalle) {
                        $detalle->id_concesionario = $model->id;
                        if ($detalle->save(false)) {
                            $ids_partes_array = array_keys($aux_partes[$key]['tiempo']);

                            for ($i=0; $i < sizeof($ids_partes_array); $i++) {
                                $model_partes_garantia = new PartesGarantia();
                                $model_partes_garantia->kilometraje = $aux_partes[$key]['kilometraje'][$ids_partes_array[$i]];
                                $model_partes_garantia->tiempo = $aux_partes[$key]['tiempo'][$ids_partes_array[$i]];
                                $model_partes_garantia->periodo = $aux_partes[$key]['periodo'][$ids_partes_array[$i]];
                                $model_partes_garantia->partes_id = $ids_partes_array[$i];
                                $model_partes_garantia->detalle_garantia_id = $detalle->id;
                                if ( !( $bandera = $model_partes_garantia->save(false) ) ) {
                                    print_r($model_detalle_garantia->getErrors());
                                    die();
                                }
                            }

                            if ( $bandera ) {
                                \Yii::$app->getSession()->setFlash('success', '¡Se registro el concsionario con ÉXITO!');   
                            } else{
                                \Yii::$app->getSession()->setFlash('error', '¡Error al registrar las garantias!');   
                            }
                        }
                    }
                } else{
                    \Yii::$app->getSession()->setFlash('error', '¡Error al registrar las Sucursales del Concesionario!');   
                }
            } else{
                \Yii::$app->getSession()->setFlash('error', '¡Error al registrar el Concesionario!');   
            }
            
            return $this->redirect(['index']);
        }
        else{
            $marcas = ArrayHelper::map(Marca::find()->orderBy('nombre')->all(), 'id_marca', 'nombre');
            $partes = Partes::find()->where(['id_tipo_garantia' => 1])->all();

            return $this->render('create', [
                'model' => $model,
                'model_partes' => $model_partes,
                'model_sucursal' => (empty($model_sucursal)) ? [new SucursalConcesionario] : $model_sucursal,
                'model_detalle_garantia' => (empty($model_detalle_garantia)) ? [new DetalleGarantia] : $model_detalle_garantia,
                'model_partes_garantia' => (empty($model_partes_garantia)) ? [new PartesGarantia] : $model_partes_garantia,
                'marcas' => $marcas,
                'partes' => $partes,
            ]);
        }

       
    }
    /**
     * Updates an existing Concesionario model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model_partes = new Partes;
        $model_sucursal = $model->sucursalConcesionarios;        
        $model_detalle_garantia = $model->detalleGarantias;   
        $model_partes_garantia = [];
        $oldParteGarantiaIds = [];

        if (!empty($model_detalle_garantia)) 
        {
            foreach ($model_detalle_garantia as $indexDetalle => $detalle_garantia)
            {              
                $model_partes_garantia[$indexDetalle] = $detalle_garantia->partesGarantias;                 
                $oldParteGarantiaIds = ArrayHelper::merge(ArrayHelper::index($detalle_garantia->partesGarantias, 'id'), $oldParteGarantiaIds);
            }
        }
 
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $oldIDsSucursal = ArrayHelper::map($model_sucursal, 'id', 'id');
            $model_sucursal = Model::createMultiple(SucursalConcesionario::classname(), $model_sucursal);
            Model::loadMultiple($model_sucursal, Yii::$app->request->post());
            $deletedIDsSucursal = array_diff($oldIDsSucursal, array_filter(ArrayHelper::map($model_sucursal, 'id', 'id')));

            $model_partes_garantia = [];
            $oldDetalleGarantiaIDs = ArrayHelper::map($model_detalle_garantia, 'id', 'id');
            $modelsDetalleGarantia = Model::createMultiple(DetalleGarantia::classname(), $model_detalle_garantia);
            Model::loadMultiple($model_detalle_garantia, Yii::$app->request->post());

                
            $idsPartesDetalle = [];
            for($m=0; $m<sizeof(Yii::$app->request->post('DetalleGarantia')); $m++ ) {

                $detallesGarantias =Yii::$app->request->post('DetalleGarantia') ;
                    if( empty($detallesGarantias[$m]['id'] )) {
                        $nuevoDetalle = new DetalleGarantia();
                        $nuevoDetalle->id_marca = $detallesGarantias[$m]['id_marca'] ;
                        $nuevoDetalle->id_concesionario = $id ;

                        array_push($model_detalle_garantia, $nuevoDetalle );
                    }else{
                        if(isset($detallesGarantias[$m]['id'] )) {
                            array_push($idsPartesDetalle ,$detallesGarantias[$m]['id'] );
                        }
                    }
                }           
                    $deletedDetalleGarantiaIDs = array_diff($oldDetalleGarantiaIDs, array_filter($idsPartesDetalle));
                    
                    $partesGarantiaIDs = [];

                            if (isset($_POST['PartesGarantia'])) {

                                foreach ($_POST['PartesGarantia'] as $indexDetalleGarantia => $partesGarantia) {
                                      
                                    if(isset($partesGarantia['id'])){
                                        $partesGarantiaIDs = ArrayHelper::merge($partesGarantiaIDs, array_filter($partesGarantia['id']));
                                            }

                                    for($x=0 ; $x<sizeof($partesGarantiaIDs); $x++ ){

                                        if(isset($partesGarantia['tiempo'][$x])){

                                            $modelParte = (isset($partesGarantia['id'][$x]) && isset($oldParteGarantiaIds[$partesGarantia['id'][$x]])) ? $oldParteGarantiaIds[$partesGarantia['id'][$x]] : new PartesGarantia;
                                            $modelParte->tiempo = $partesGarantia['tiempo'][$x];
                                            $modelParte->periodo = $partesGarantia['periodo'][$x];
                                            $modelParte->kilometraje = $partesGarantia['kilometraje'][$x];
                                            $modelParte->partes_id =  $x;
                                            $model_partes_garantia[$indexDetalleGarantia][$x] = $modelParte;
                                            $valid = $modelParte->validate();
 
                                        }
                                    
                                    }     
                                }
                            }

                            $oldPartesGarantiaIDs = ArrayHelper::getColumn($oldParteGarantiaIds, 'id');
                            $deletedPartesGarantiaIDs = array_diff($oldPartesGarantiaIDs, $partesGarantiaIDs);
                
                            if (true) {
                                $transaction = Yii::$app->db->beginTransaction();
                                try {
                                    if ($flag = $model->save(false)) {

                                        if (!empty($deletedIDsSucursal)) {
                                            SucursalConcesionario::deleteAll(['id' => $deletedIDsSucursal]);
                                        }   

                                        foreach ($model_sucursal as $sucursal) {
                                            $sucursal->concesionario_id = $model->id;
                                            if (! ($flag = $sucursal->save(false))) {
                                                break;
                                            }
                                        }
                
                                        foreach ($model_detalle_garantia as $indexDetalleGarantia => $detalleGarantia) {
                
                                            if ($flag === false) {
                                                break;
                                            }
                
                                            $detalleGarantia->id_concesionario = $model->id;  
                                            
                                            
                                            if (!($flag = $detalleGarantia->save(false))) {
                                              
                                                break;
                                            }
                                           
                                            if (isset($model_partes_garantia[$indexDetalleGarantia]) && is_array($model_partes_garantia[$indexDetalleGarantia])) {
                                                foreach ($model_partes_garantia[$indexDetalleGarantia] as $indexParte => $parte) {

                                                    $parte->detalle_garantia_id = $detalleGarantia->id;
                                                   
                                                    if (!($flag = $parte->save(false))) {
                                                        print_r($parte->getErrors());
                                               
                                                        break;
                                                    }else{

                                                    }
                                                }
 
                                            }
                                        }
                                    }

                                    if (!empty($deletedPartesGarantiaIDs)) {  
                                        // print_r($deletedPartesGarantiaIDs);    
                                                          
                                        // foreach($deletedPartesGarantiaIDs as $index => $delete){
                                        //     echo '<br>';
                                        //     echo 'hh' . $delete;
                                        //     echo '<br>';
                                         
                                        // }
                                     }

                                     if (!empty($deletedDetalleGarantiaIDs)) {

                                        
                                        foreach($deletedDetalleGarantiaIDs as $indexDetalle => $deleteDetalle){                                             
                                            //PartesGarantia::find(['detalle_garantia_id'=>$deleteDetalle])->deleteAll();       
                                            PartesGarantia::deleteAll('detalle_garantia_id = :detalle_garantia_id', [':detalle_garantia_id' => $deleteDetalle]);                                         
                                            //$modelll = \Yii::$app->db->createCommand('DELETE FROM partes_garantia WHERE detalle_garantia_id=:deleteDetalle');
                                            //$modelll->bindParam(':deleteDetalle', $deleteDetalle);
                                            //$modelll->execute();
                                            DetalleGarantia::findOne($deleteDetalle)->delete();    
                                            //echo '+' . $deleteDetalle;                                          
                                        }                                                           
                                     }
                
                                    if ($flag) {
                                    
                                        $transaction->commit();
                                        //die(); 
                                        return $this->redirect(['update', 'id' => $model->id]);
                                    } else {
                                        $transaction->rollBack();
                                    }
                                } catch (Exception $e) {
                                    $transaction->rollBack();
                                }
                            }
                    }
                    else{
                        $marcas = ArrayHelper::map(Marca::find()->orderBy('nombre')->all(), 'id_marca', 'nombre');
                        $partes = Partes::find()->where(['id_tipo_garantia' => 1])->all();
                        
                        return $this->render('_form-update', [
                            'model' => $model,
                            'model_partes' => $model_partes,
                            'model_sucursal' => (empty($model_sucursal)) ? [new SucursalConcesionario] : $model_sucursal,
                            'model_detalle_garantia' => (empty($model_detalle_garantia)) ? [new DetalleGarantia] : $model_detalle_garantia,
                            'model_partes_garantia' => (empty($model_partes_garantia)) ? [new PartesGarantia] : $model_partes_garantia,
                            'marcas' => $marcas,
                            'partes' => $partes,
                        ]);
        }
    }

   


    public function actionListarmodelos($id)
    {

        if (\Yii::$app->request->isAjax ) {
            $query = new Query();
            $query->select('*')
            ->from('modelo')
            ->where(['=','id_marca', $id])
            ->orderBy('nombre');
            $rows = $query->all();

            if (count($rows) > 0) {
                echo "<option value=''> Seleccione... </option>";
                foreach ($rows as $aux) {
                    echo "<option value='".$aux['id_modelo']."'>" . $aux['nombre'] ."</option>";
                }
            } else {
                echo "<option> -- </option>";
            }
        }
        \Yii::$app->end();
    }


    /**
     * Deletes an existing Concesionario model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Concesionario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Concesionario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Concesionario::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
