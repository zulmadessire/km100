<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if(Yii::$app->user->isGuest) {
            return $this->redirect('user/security/login');//user/security/login');
        } else {

           $usuario  = User::findOne(['id'=>Yii::$app->user->identity->id]);
          if ($usuario->pregunta == '') {
              return $this->redirect(['user/admin/update', 'id'=> $usuario->id]);
          }
          

            if (\Yii::$app->user->can('admin') || \Yii::$app->user->can('supervisor')) {
                return $this->redirect('caso/create');
            } else if (Yii::$app->user->can('taller')){
                return $this->redirect('servicio/indextaller');
            } else if (Yii::$app->user->can('analista')){
                return $this->redirect('analista-mantenimiento/listadochecklist');
            } else if (Yii::$app->user->can('gerente-compras')){
                return $this->redirect('servicio/indexordentrabajo');
            } else if (Yii::$app->user->can('proveedor') && $usuario->id_proveedor==null){
                return $this->redirect('proveedorpieza/create');
            } else if (Yii::$app->user->can('proveedor') && $usuario->id_proveedor!=null){
                // return $this->redirect(['proveedorpieza/update','id'=>$usuario->id_proveedor]);
               return $this->redirect('proveedor/index');
            }else{
                return $this->redirect('caso/index');
            }
        }
    }

    public function actionAdmin()
    {
          return $this->redirect('../admin');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {

            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }

        $producto = Yii::$app->db2->createCommand("SELECT  * FROM caso")->queryAll();

        echo "<pre>";
        print_r($producto);
        echo "</pre>";
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
