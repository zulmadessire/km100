<?php

namespace app\controllers;

use Yii;
use app\models\Mensajero;
use app\models\MensajerosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\UserSearch;
use app\models\AuthAssignment;
use app\models\User;
use dektrium\user\helpers\Password;
use app\models\Modelo;
/**
 * MensajeroController implements the CRUD actions for Mensajero model.
 */
class MensajeroController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Mensajero models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MensajerosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Mensajero model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Mensajero model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
//print_r($_POST);
        //die();
        $model = new Mensajero();

        $usuario = new UserSearch();
        $rol = new AuthAssignment();


        $length = 10;
        $chars = array_merge(range(0,9), range('a','z'), range('A','Z'));
        shuffle($chars);
        $password = implode(array_slice($chars, 0, $length));

        if ($model->load(Yii::$app->request->post())) {
            if(!$model->save()) {
                print_r($model->getErrors());
                die();
            }
                $usuario->username = $model->user;
                $usuario->email=$model->correo;
                $usuario->password_hash = Password::hash($password);
                $usuario->auth_key=0;
                $usuario->created_at=0;
                $usuario->updated_at=0;
                $usuario->flags=0;
                $usuario->cargo_gerencial=0;
                $usuario->cargo=0;
                $usuario->id_mensajero = $model->id;
                $usuario->confirmed_at = 1491044212;
                $usuario->save(false);

                $rol->item_name = 'reparto';
                $rol->user_id = $usuario->id;
                $rol->save(false);

                $correo_envio = $model->correo;
                $login =  $usuario->username;

                    if(!$this->notificacion($model,$password, $usuario)){
                        \Yii::$app->getSession()->setFlash('error', '¡Error en el envío de notificación!');
                    }

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model
            ]);
        }
    }

         public function notificacion($model,$password, $usuario)
    {
      $envio = 0;
        $mensaje_sup ='  <p>'.$model->nombre.', <br>
                        Km100 le informa que ha sido registrado como Representante de Reparto, sus datos son:
                    </p>
                    <p>
                        <strong>Nombre de Usuario: </strong>'.$usuario->username.'<br>
                        <strong>Clave de Acceso: </strong>'.$password.'<br>
                    </p>
        ';
        if (!Yii::$app->mailer->compose(['html'=>'layout'], ['content'=> $mensaje_sup])
                        ->setFrom([Yii::$app->params['adminEmail'] => 'KM100'])
                        ->setTo($model->correo)
                        ->setSubject('Notificación de Registro')
                        ->send())
        {
            $envio=0;

        }
     
      if ($envio==0){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Updates an existing Mensajero model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Mensajero model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Mensajero model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Mensajero the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Mensajero::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
