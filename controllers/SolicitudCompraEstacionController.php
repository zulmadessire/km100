<?php

namespace app\controllers;

use Yii;
use app\models\SolicitudCompraEstacion;
use app\models\SolicitudCompraEstacionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Vehiculo;
use app\models\Pieza;
use app\models\Estacion;
use app\models\Proveedor;
use app\models\SolicitudPiezaEstacion;
use app\models\SolicitudCompraPieza;
use app\models\EstadoCompraPieza;
use app\models\PipelineCompraPieza;
use app\models\SolicitudCompraPiezaSearch;
use app\models\User;
use app\models\SolicitudCompraProveedorSearch;
use app\models\CotizacionProveedor;
use app\models\CotizacionProveedorPieza;
use app\models\CotizacionProveedorPiezaSearch;
use yii\helpers\ArrayHelper;
use app\models\OrdenCompraPieza;
use app\models\OrdenCompraPiezaSearch;
use app\models\OrdenCompraFactura;
use app\models\SolicitudCompraProveedor;
use app\models\DespachoSearch;
use app\models\Despacho;
use app\models\DespachoProveedorPieza;
use app\models\DespachoProveedorPiezaSearch;
use yii\db\Query;
use kartik\mpdf\Pdf;
use mPDF;
use yii\helpers\Json;
use app\models\Caso;
use app\models\CasoPiezaEstacion;
use app\models\EstacionUsuario;



/**
 * SolicitudCompraEstacionController implements the CRUD actions for SolicitudCompraEstacion model.
 */
class SolicitudCompraEstacionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SolicitudCompraEstacion models.
     * @return mixed
     */
    public function actionIndex()
    {

        Yii::$app->user->identity->id;
        
        $searchModel = new SolicitudCompraPiezaSearch();
        $dataProvider = $searchModel->searchEstacion(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy('id ASC');

        // Proveedor recomendado en base a la cantidad de ordenes de compras
        // $mejor_proveedor = OrdenCompraPieza::find()
        // ->select(['proveedor.nombre as pro, id_proveedor, COUNT(id_proveedor) totales'])
        // ->leftJoin('proveedor', '`proveedor`.`id` = `orden_compra_pieza`.`id_proveedor`')
        // ->groupBy(['id_proveedor'])
        // ->orderBy('totales DESC')
        // ->limit(3)
        // ->all();

        //Proveedor recomendado en base al porcentaje de descuento
        $mejor_proveedor = Proveedor::find()
        ->orderBy('porcentaje DESC')
        ->limit(5)
        ->all();
        

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'mejor_proveedor' => $mejor_proveedor,
        ]);
    }

    /**
     * Displays a single SolicitudCompraEstacion model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionListar($id)
    {        
       $countpieza = Pieza::find()
       ->where(['tipo_pieza_id' => $id])
       ->count();

       $pieza = Pieza::find()
       ->where(['tipo_pieza_id' => $id])
       ->all();

       if($countpieza>0){
            echo "<option value=''> Seleccione... </option>";
            foreach($pieza as $pieza){
                echo "<option value='".$pieza->id_pieza."'>".$pieza->nombre."</option>";
            }
        }
        else{
            echo "<option>-</option>";
        }
    }

    /**
     * Creates a new SolicitudCompraEstacion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($type = 0)
    {
        // print_r(Yii::$app->request->post());
        // die();
        $model = new SolicitudCompraEstacion();
        $model_cotizacion_estacion_pieza = [new SolicitudPiezaEstacion];

        $cliente = (\Yii::$app->user->can('estaciones todas') )?'':EstacionUsuario::find()->where(['user_id'=>Yii::$app->user->identity->ID ])->one()->estacion->idEmpresa->cliente;
        $empresa = (\Yii::$app->user->can('estaciones todas'))?'':EstacionUsuario::find()->where(['user_id'=>Yii::$app->user->identity->ID ])->one()->estacion->idEmpresa;
        $estaciones = (\Yii::$app->user->can('estaciones asociadas'))?ArrayHelper::getColumn(EstacionUsuario::find()->select('estacion_id')->where(['user_id'=>Yii::$app->user->identity->ID ])->asArray()->all(), 'estacion_id'):'';
        
        //Consultando los vehiculos a los cuales se les puede registraruna solicitud de compra
        if ( \Yii::$app->user->can('estaciones todas') ) {
            //Se muestran todos los vehiculos que no esten en transito
            $vehiculos = ArrayHelper::map(Vehiculo::find()->select(['mva'])->where(['transito' => 0])->distinct()->all(), 'mva', 'mva');
        } else if ( \Yii::$app->user->can('estaciones cliente') ){
            //Busco las estaciones asociadas al cliente del usuario
            $v_estaciones = Estacion::find()->joinWith('idEmpresa')->where([ 'empresa.cliente_id' => $cliente ])->all();
            $ids_estaciones = ArrayHelper::getColumn($v_estaciones, 'id_estacion');
            $vehiculos = ArrayHelper::map(Vehiculo::find()->select(['mva'])->join('INNER JOIN', 'flota', 'flota.id_vehiculo = vehiculo.id')->where(['vehiculo.transito' => 0, 'flota.id_estacion' => $ids_estaciones])->andWhere('flota.fecha_fin IS NULL')->distinct()->all(), 'mva', 'mva');
        } else if( \Yii::$app->user->can('estaciones asociadas') ) {
            //Busco las estaciones asociadas al usuario
            $v_estaciones = EstacionUsuario::find()->where(['user_id'=>Yii::$app->user->identity->ID ])->all();
            $ids_estaciones = ArrayHelper::getColumn($v_estaciones, 'estacion_id');
            $vehiculos = ArrayHelper::map(Vehiculo::find()->select(['mva'])->join('INNER JOIN', 'flota', 'flota.id_vehiculo = vehiculo.id')->where(['vehiculo.transito' => 0, 'flota.id_estacion' => $ids_estaciones])->andWhere('flota.fecha_fin IS NULL')->distinct()->all(), 'mva', 'mva');
        }

        if ($model->load(Yii::$app->request->post())) {
            
            $band1 = $model->tipo_solicitud;

            if ( $band1 == 1 ) {
                $model->placa = Vehiculo::findOne($model->placa)->placa;
                $model->id_modelo = $model->id_modelo2;
            } else if( $band1 == 2 ) {
                $model->id_modelo = $model->id_modelo;
            } else if( $band1 == 3 ) {
                $model->id_modelo = NULL;
            } else if( $band1 == 4 ) {               
                $model->placa = Vehiculo::findOne($model->placa)->placa;
                $model->id_modelo = $model->id_modelo2;
                if( Yii::$app->request->post('name') == 1) {
                    $model->id_caso = $model->id_caso;
                }
            }

            $model->fecha_registro = date('Y-m-d');
            if($model->save()) {
                // caso
                if($band1 == 4 && !empty($_POST['SolicitudCompraEstacion']['id_caso'])) {
                    $model_caso_pieza = new CasoPiezaEstacion();
                    $model_caso_pieza->id_solicitud_compra_estacion = $model->id;
                    $model_caso_pieza->id_caso = $_POST['SolicitudCompraEstacion']['id_caso'];
                    $model_caso_pieza->save();
                }
                //die('entra2');
                $this->estacionPiezas($model->id);

                $band = true;
                if($band) {                    
                    //Se crea la Solicitud de compra de pieza
                    $model_solicitud_compra_pieza = new SolicitudCompraPieza();
                    $model_solicitud_compra_pieza->fecha = date("Y-m-d H:i:s");
                    $model_solicitud_compra_pieza->codigo_estado = 0; //Asignado
                    $model_solicitud_compra_pieza->id_solicitud_compra_estacion = $model->id;
                    $model_solicitud_compra_pieza->origen_solicitud = 2;// para saber que la solicitud es de estacion
                    if($band1 == 4 && !empty($_POST['SolicitudCompraEstacion']['id_caso'])) {
                        $model_solicitud_compra_pieza->id_caso_estacion = $_POST['SolicitudCompraEstacion']['id_caso'];// para saber que la solicitud es de estacion
                    }


                    if ($model_solicitud_compra_pieza->save()) {
                        //Se cambia el estado de la solicitud en el pipeline de compra de pieza
                        $estado_compra_pieza = EstadoCompraPieza::findOne(['codigo' => 0])->id; //Asignado
                        $model_pipeline_compra_pieza = new PipelineCompraPieza();
                        $model_pipeline_compra_pieza->fecha = date('Y-m-d');
                        $model_pipeline_compra_pieza->id_solicitud_compra_pieza = $model_solicitud_compra_pieza->id;
                        $model_pipeline_compra_pieza->id_estado_compra_pieza = $estado_compra_pieza;
                        $model_pipeline_compra_pieza->save();
                        unset($model_pipeline_compra_pieza);

                        
                        if($band1 == 10) {
                            $model_solicitud_compra_pieza->aprobacion = 2;
                            $model_solicitud_compra_pieza->codigo_estado = 3;
                            $model_solicitud_compra_pieza->update();
                            $model_solicitud_compra_proveedor = new SolicitudCompraProveedor();
                            //$model_proveedor = Proveedor::findOne($proveedor); // buscar el id del proveedor en licitaciones
                            $model_proveedor_id = 7;
                            $model_proveedor_porcentaje = 10;
                            $costo_licitacion = 222;
                            $model_solicitud_compra_proveedor->id_solicitud_compra_pieza = $model_solicitud_compra_pieza->id;
                            $model_solicitud_compra_proveedor->id_proveedor = $model_proveedor_id;//$model_proveedor->id;
                    
                            if ($model_solicitud_compra_proveedor->save(false)) {
                                $model_cotizacion_proveedor = new CotizacionProveedor();
                                $model_cotizacion_proveedor->fecha = date("Y-m-d H:i:s");
                                $model_cotizacion_proveedor->estado = 0; //Creada la cotizacion
                                $model_cotizacion_proveedor->id_solicitud_compra_proveedor = $model_solicitud_compra_proveedor->id;
                                if ($model_cotizacion_proveedor->save()) {
                                    $model_cotizacion_pieza = SolicitudPiezaEstacion::find()->where(['id_solicitud_compra_estacion' => $model_solicitud_compra_pieza->id_solicitud_compra_estacion])->all();                    
                                    foreach ($model_cotizacion_pieza as $k => $pieza) {
                                        $model_cotizacion_proveedor_pieza = new CotizacionProveedorPieza();
                                        $model_cotizacion_proveedor_pieza->cantidad_solicitada = $pieza->cantidad;
                                        $model_cotizacion_proveedor_pieza->aprobacion = 2;
                                        $model_cotizacion_proveedor_pieza->cantidad_comprar = $pieza->cantidad;
                                        $model_cotizacion_proveedor_pieza->costo = $costo_licitacion;
                                        $model_cotizacion_proveedor_pieza->fecha_entrega = date("Y-m-d H:i:s");
                                        $model_cotizacion_proveedor_pieza->id_cotizacion_proveedor = $model_cotizacion_proveedor->id;
                
                                        $model_cotizacion_proveedor_pieza->id_solicitud_pieza_estacion = $pieza->id;
                
                                        $model_cotizacion_proveedor_pieza->ahorro = $model_proveedor_porcentaje; //$model_proveedor->porcentaje;
                                        $model_cotizacion_proveedor_pieza->estado_despacho = 0;
                                        $model_cotizacion_proveedor_pieza->save();

                                        
                                        if ($pieza) {    
                                
                                            //$model_orden_compra = OrdenCompraPieza::find()->where(['id_cotizacion_proveedor' => $pieza->id_cotizacion_proveedor])->one();
                                            //$total_pieza_descuento = $pieza->costo - ( ($pieza->costo * $pieza->ahorro) / 100 ) ;
                                            //$total_pieza_itbs = ($total_pieza_descuento * 0.18 ) + $total_pieza_descuento;
                                                
                                            //if (!$model_orden_compra) {
                                                    
                                                $model_orden_compra = new OrdenCompraPieza();
                                                $model_orden_compra->fecha = date("Y-m-d H:i:s");
                                                $model_orden_compra->estado = 0; //creado
                                                $model_orden_compra->id_solicitud_compra_pieza = $model_solicitud_compra_pieza->id;
                                                $model_orden_compra->id_cotizacion_proveedor = $model_cotizacion_proveedor->id;
                                                $model_orden_compra->total = 99; //$total_pieza_itbs;
                                                $model_orden_compra->id_proveedor = $model_proveedor_id;//$pieza->idCotizacionProveedor->idSolicitudCompraProveedor->id_proveedor;
                                                $model_orden_compra->save();
                                
                                            //}//  else{
                                            //     $total = ($model_orden_compra->total)? $model_orden_compra->total : 0;                    
                                            //     $model_orden_compra->total = $total + $total_pieza_itbs;
                                            //     $model_orden_compra->save();
                                            // }
                                
                                        }
                                        
                                        //unset($model_cotizacion_proveedor_pieza);
                                    }
                                    for ( $i = 1; $i < 4; $i++) {                                
                                        $estado_compra_pieza1 = EstadoCompraPieza::findOne(['codigo' => $i])->id; //Asignado
                                        $model_pipeline_compra_pieza1 = new PipelineCompraPieza();
                                        $model_pipeline_compra_pieza1->fecha = date('Y-m-d');
                                        $model_pipeline_compra_pieza1->id_solicitud_compra_pieza = $model_solicitud_compra_pieza->id;
                                        $model_pipeline_compra_pieza1->id_estado_compra_pieza = $estado_compra_pieza1;
                                        $model_pipeline_compra_pieza1->save();                                    
                                        unset($model_pipeline_compra_pieza1);                              
                                    }
                                } else{
                                    echo "Error en el registro";
                                }
                            }
                        }
                        \Yii::$app->getSession()->setFlash('success', '!Registro de solicitud con éxito!');
                        return $this->redirect(['index']);
                    } else{
                        \Yii::$app->getSession()->setFlash('error', '!Hubo un error generando la solicitud de compra de pieza!');
                        return $this->render('create', [
                            'model' => $model,
                        ]);
                    }

                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'model_cotizacion_estacion_pieza' => (empty($model_cotizacion_estacion_pieza)) ? [new SolicitudPiezaEstacion] : $model_cotizacion_estacion_pieza,
                'type' => $type,
                'cliente' => $cliente,
                'empresa' => $empresa,
                'estaciones' => $estaciones,
                'vehiculos' => $vehiculos,
                ]);
        }
    }

    /**
     * Updates an existing SolicitudCompraEstacion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model_cotizacion_estacion_pieza = SolicitudPiezaEstacion::find()->where(['id_solicitud_compra_estacion' => $model->id])->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'model_cotizacion_estacion_pieza' => (empty($model_cotizacion_estacion_pieza)) ? [new SolicitudPiezaEstacion] : $model_cotizacion_estacion_pieza
            ]);
        }
    }

    /**
     * Deletes an existing SolicitudCompraEstacion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function estacionPiezas($id)
    {
        $model_cotizacion_estacion_pieza = SolicitudPiezaEstacion::find()->where(['id_solicitud_compra_estacion' => $id])->all();

        if (Yii::$app->request->post('SolicitudPiezaEstacion')[0]['id_pieza']) {
            foreach ($model_cotizacion_estacion_pieza as $key => $pieza) {
                $pieza->delete();
            }
            
            foreach (Yii::$app->request->post('SolicitudPiezaEstacion') as $i => $pieza) {
                $model_cotizacion_estacion_pieza = new SolicitudPiezaEstacion;
                $model_cotizacion_estacion_pieza->id_pieza = $pieza['id_pieza'];
                $model_cotizacion_estacion_pieza->cantidad = $pieza['cantidad'];
                $model_cotizacion_estacion_pieza->id_solicitud_compra_estacion = $id;
                $model_cotizacion_estacion_pieza->save(false);
                // unset($model_cotizacion_estacion_pieza);
        
            }
        }
    }

    protected function actionLicitadas($id) {

            $model = SolicitudCompraPieza::findOne($id);
            $model_solicitud_compra_proveedor = new SolicitudCompraProveedor();
            //$model_proveedor = Proveedor::findOne($proveedor); // buscar el id del proveedor en licitaciones
            $model_proveedor_id = 7;
            $model_proveedor_porcentaje = 10;
            $model_solicitud_compra_proveedor->id_solicitud_compra_pieza = $model->id;
            $model_solicitud_compra_proveedor->id_proveedor = $model_proveedor_id;//$model_proveedor->id;
    
            if ($model_solicitud_compra_proveedor->save(false)) {
                $model_cotizacion_proveedor = new CotizacionProveedor();
                $model_cotizacion_proveedor->fecha = date("Y-m-d H:i:s");
                $model_cotizacion_proveedor->estado = 0; //Creada la cotizacion
                $model_cotizacion_proveedor->id_solicitud_compra_proveedor = $model_solicitud_compra_proveedor->id;
                if ($model_cotizacion_proveedor->save()) {
                    $model_cotizacion_taller_pieza = SolicitudPiezaEstacion::find()->where(['id_solicitud_compra_estacion' => $model->id_solicitud_compra_estacion])->all();                    
                    foreach ($model_cotizacion_taller_pieza as $k => $pieza) {
                        $model_cotizacion_proveedor_pieza = new CotizacionProveedorPieza();
                        $model_cotizacion_proveedor_pieza->cantidad_solicitada = $pieza->cantidad;
                        $model_cotizacion_proveedor_pieza->aprobacion = 2;
                        $model_cotizacion_proveedor_pieza->fecha_entrega = date("Y-m-d H:i:s");
                        $model_cotizacion_proveedor_pieza->id_cotizacion_proveedor = $model_cotizacion_proveedor->id;

                        $model_cotizacion_proveedor_pieza->id_solicitud_pieza_estacion = $pieza->id;

                        $model_cotizacion_proveedor_pieza->ahorro = $model_proveedor_porcentaje; //$model_proveedor->porcentaje;
                        $model_cotizacion_proveedor_pieza->estado_despacho = 0;
                        $model_cotizacion_proveedor_pieza->save();
                        unset($model_cotizacion_proveedor_pieza);
                    }
                } else{
                    echo "No se pudo crear la cotizacion";
                }
            } else{
                echo "No se pudo asignar el proveedor";
            }
    }

    /**
     * Index de la solicitud de cotizaciones (estación) para los proveedores
     */
    public function actionIndexsolicitudcotizacion()
    {
        $id_proveedor = User::findOne(Yii::$app->user->id)->id_proveedor;
        $searchModel = new SolicitudCompraProveedorSearch(['id_proveedor' => $id_proveedor]);
        $dataProvider = $searchModel->searchEstacion(Yii::$app->request->queryParams);

        //$id_servicio = SolicitudServicioTaller::findOne($id)->id_servicio;

        return $this->render('index-solicitud-cotizacion', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'id_proveedor' => $id_proveedor,
        ]);
    }

    /**
     * Cotización del proveedor
     */
    public function actionCotizacionproveedor($id, $origen = 0)
    {
        $model_cotizacion_proveedor = CotizacionProveedor::findOne($id);
        //$model_cotizacion_proveedor_pieza = CotizacionProveedorPieza::find()->where(['id_cotizacion_proveedor' => $model_cotizacion_proveedor->id]);

        $searchModel = new CotizacionProveedorPiezaSearch(['id_cotizacion_proveedor' => $model_cotizacion_proveedor->id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $model_cotizacion_proveedor->scenario = 'update';
        //$model_solicitud_estacion = SolicitudCompraEstacion::findOne($model->idSolicitudCompraPieza->id_solicitud_compra_estacion);
        $id_solicitud_compra_estacion = $model_cotizacion_proveedor->idSolicitudCompraProveedor->idSolicitudCompraPieza->id_solicitud_compra_estacion;
        $model_solicitud_compra_estacion = SolicitudCompraEstacion::findOne($id_solicitud_compra_estacion);
        $model_vehiculo = Vehiculo::findOne($model_solicitud_compra_estacion->id_vehiculo);

        return $this->render('cotizacion-proveedor', [
            'dataProvider' => $dataProvider,
            'model_vehiculo' => $model_vehiculo,
            'model_cotizacion_proveedor' => $model_cotizacion_proveedor,
            'origen' => $origen,
            'model_solicitud_compra_estacion' => $model_solicitud_compra_estacion
        ]);
    }

        /**
     * Guardar cotización del proveedor
     * @autor Zulma Hernández
     */
    public function actionGuardarcotizacionproveedor($id)
    {
        if (Yii::$app->request->post()) {
            $cantidad_disponible = Yii::$app->request->post('cantidad_disponible');
            $id_pieza = Yii::$app->request->post('id_pieza');
            $fecha_entrega = Yii::$app->request->post('fecha_entrega');
            $costo = Yii::$app->request->post('costo');
            $descuento = Yii::$app->request->post('descuento');
            $garantia = Yii::$app->request->post('garantia');
            $km_garantia = Yii::$app->request->post('km_garantia');

            $total_descuento = $itbs = $total = 0;

            $model_cotizacion_proveedor = CotizacionProveedor::findOne($id);
            $model_cotizacion_proveedor->load(Yii::$app->request->post());
            $model_cotizacion_proveedor->estado = 1; //Editada por el proveedor

            foreach ($cantidad_disponible as $i => $disponible) {
                $model_cotizacion_proveedor_pieza = CotizacionProveedorPieza::find()->where([ 'id' => $id_pieza[$i] ])->one();
                $model_cotizacion_proveedor_pieza->cantidad_disponible = $disponible;
                $model_cotizacion_proveedor_pieza->fecha_entrega = date("Y-m-d", strtotime($fecha_entrega[$i]));
                $model_cotizacion_proveedor_pieza->costo = $costo[$i];
                $model_cotizacion_proveedor_pieza->ahorro = $descuento[$i];
                $model_cotizacion_proveedor_pieza->aprobacion = 0;
                $model_cotizacion_proveedor_pieza->garantia = $garantia[$i];
                if ($km_garantia[$i] == 0) {
                    $km_garantia[$i] == NULL;
                }
                $model_cotizacion_proveedor_pieza->km_garantia = $km_garantia[$i];
                $model_cotizacion_proveedor_pieza->save();

                $total_descuento += ($costo[$i] * $model_cotizacion_proveedor_pieza->cantidad_solicitada) - ((($costo[$i] * $model_cotizacion_proveedor_pieza->cantidad_solicitada) * $descuento[$i])/100);
            }
            
            $itbis = ($total_descuento * 0.18);
            $total = $total_descuento + $itbis;

            $model_cotizacion_proveedor->subtotal = $total_descuento;
            $model_cotizacion_proveedor->itbs = $itbis;
            $model_cotizacion_proveedor->descuento = $total_descuento;

            if ( $model_cotizacion_proveedor->save() ) {
                \Yii::$app->getSession()->setFlash('success', '¡La cotización se ha guardado con éxito!');
            } else{
                \Yii::$app->getSession()->setFlash('error', '¡Error al guardar la cotización!');
            }

            $this->redirect('../indexsolicitudcotizacion');
        }
    }

    /**
     * Cotizaciones recibidas por los proveedores solicitados
     */
    public function actionCotizacionesrecibidas($id)
    {
        //die();
        $model = SolicitudCompraPieza::findOne($id);
        $flag = 0;

        if (Yii::$app->request->post()) {
            //Resetear valores de la cotizacion rechazada
            if ($model->aprobacion == 5) {
                $flag = 5;
                $id_cotizacion_proveedor_pieza = Yii::$app->request->post('id');
                
                $model->aprobacion = NULL; //Resetear estatus
                $model->observacion_gerente = NULL; 
                $model->motivo_rechazo = NULL; 
                $model->save();
                
                foreach ($id_cotizacion_proveedor_pieza as $i => $icpp) {
                    $cotizacion = CotizacionProveedorPieza::findOne($icpp);
                    $cotizacion->aprobacion = NULL; //Resetear estatus
                    $cotizacion->cantidad_comprar = NULL;
                    $cotizacion->save();
                }
            }
            if ($model->load(Yii::$app->request->post())) {
                $model->aprobacion = 1; //Para saber que guardo la seleccion de cotizaciones recibidas
                $model->save();
            }

            $compras = Yii::$app->request->post('comprar');
            $cotizaciones = Yii::$app->request->post('selected');

            foreach ($compras as $i => $compra) {
                $cotizacion = CotizacionProveedorPieza::findOne($cotizaciones[$i]);
                $cotizacion->cantidad_comprar = $compra;
                $cotizacion->aprobacion = 1; //Para saber que selecciono la cotizacion del proveedor seleccionado
                $cotizacion->save();
            }

            $model->codigo_estado = 2;
            $model->id_analista = Yii::$app->user->identity->id;
            if ($model->save()) {
                if($flag == 0) {
                    $estado_compra_pieza = EstadoCompraPieza::findOne(['codigo' => 2])->id; //Aprobacion
                    $model_pipeline_compra_pieza = new PipelineCompraPieza();
                    $model_pipeline_compra_pieza->fecha = date('Y-m-d');
                    $model_pipeline_compra_pieza->id_solicitud_compra_pieza = $model->id;
                    $model_pipeline_compra_pieza->id_estado_compra_pieza = $estado_compra_pieza;
                    $model_pipeline_compra_pieza->save();
                    unset($model_pipeline_compra_pieza); 
                }               
            } else{
                \Yii::$app->getSession()->setFlash('error', '¡Hubo un error!');
            } 

            \Yii::$app->getSession()->setFlash('success', '¡Se han guardado las cotizaciones seleccioandas con éxito!');
            return $this->redirect(['solicitud-compra-estacion/index']);

        } else{
            //$piezas = ArrayHelper::map(CotizacionTallerPieza::find()->where('id_cotizacion_taller = '. $model->id_cotizacion_taller)->all(), 'id_pieza', 'idPieza.nombre');
            $piezas = ArrayHelper::map(SolicitudPiezaEstacion::find()->where('id_solicitud_compra_estacion = '. $model->id_solicitud_compra_estacion)->all(), 'id_pieza', 'idPieza.nombre');
            $cotizaciones_piezas = CotizacionProveedorPieza::find()->joinWith('idCotizacionProveedor')->joinWith('idCotizacionProveedor.idSolicitudCompraProveedor')->where('solicitud_compra_proveedor.id_solicitud_compra_pieza ='.$model->id )->all();
            $cotizaciones_piezas_rechazadas = CotizacionProveedorPieza::find()->joinWith('idCotizacionProveedor')->joinWith('idCotizacionProveedor.idSolicitudCompraProveedor')->where('solicitud_compra_proveedor.id_solicitud_compra_pieza ='.$model->id .' AND cotizacion_proveedor_pieza.aprobacion = 5')->all();

            return $this->render('cotizaciones-recibidas', [
                'model' => $model,
                'cotizaciones_piezas' => $cotizaciones_piezas,
                'cotizaciones_piezas_rechazadas' => $cotizaciones_piezas_rechazadas,
                'piezas' => $piezas,
            ]);
        }
    }

    /**
     * Index de la solicitud de aprobaciones de compra para los gerentes de compra
     */
    public function actionIndexsolicitudaprobacion()
    {
        if(\Yii::$app->user->can('director-comercial')) {
            $searchModel = new SolicitudCompraPiezaSearch(['codigo_estado' => 2, 'aprobacion' => 2]);
        }else {
            $searchModel = new SolicitudCompraPiezaSearch(['codigo_estado' => 2]);
        }
        $dataProvider = $searchModel->searchEstacion(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy('id DESC');

        return $this->render('index-solicitud-aprobacion', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Cotización del proveedor
     */
    public function actionGestionaprobacion($id)
    {
        $model = SolicitudCompraPieza::findOne($id);

        $cotizaciones_piezas = CotizacionProveedorPieza::find()->joinWith('idCotizacionProveedor')->joinWith('idCotizacionProveedor.idSolicitudCompraProveedor')->where('solicitud_compra_proveedor.id_solicitud_compra_pieza ='.$model->id .' AND cotizacion_proveedor_pieza.aprobacion = 1')->all();
        $model_solicitud_compra_estacion = $this->findModel($model->id_solicitud_compra_estacion);
        $model_vehiculo = Vehiculo::findOne($model_solicitud_compra_estacion->id_vehiculo);

        return $this->render('gestion-aprobacion', [
            'model' => $model,
            'cotizaciones_piezas'                   => $cotizaciones_piezas,
            'model_solicitud_compra_estacion'       => $model_solicitud_compra_estacion,
            'model_vehiculo'                        => $model_vehiculo,
        ]);
        
    }

    /*
    * Aprobar una solicitud de compra, genrente de compras
    * @autor Zulma Hernández
    */
    public function actionAprobarsolicitud(){
        // print_r($_POST);
        // die();
        if (Yii::$app->request->post()) {
            //$id = Yii::$app->request->post('id');
            $solicitud = Yii::$app->request->post('SolicitudCompraPieza');            
            $id = $solicitud['id']; 
            $mlp = $solicitud['mlp']; 
            $estado =  $solicitud['estado']; 

            $model = SolicitudCompraPieza::findOne($id);
            if($estado == 1) {
                if ($mlp == 2 && $estado == 1 && \Yii::$app->user->can('gerente-compras')) {
                    if ($model->load(Yii::$app->request->post())) {
                        $model->aprobacion = 2; //Indica que el monto supera, al establecido en configuración y tiene que ser aprobado por director-comercial
                        $model->save();
                    }
                    \Yii::$app->getSession()->setFlash('success', 'La solicitud de aprobacion fue enviada al director comercial');
                    return $this->redirect(['solicitud-compra-estacion/indexsolicitudaprobacion/']);
                }elseif ($mlp == 1 && $estado == 1 && \Yii::$app->user->can('gerente-compras') || $mlp == 2 && $estado == 1 && \Yii::$app->user->can('director-comercial')) {
                    
                    $this->ordenCompra($model->id); //Generar orden de compra

                    //Estatus de cotizacion por proveedor
                    $solicitudes_proveedor = SolicitudCompraProveedor::find()->where(['id_solicitud_compra_pieza' => $model->id])->all();
                    foreach($solicitudes_proveedor as $sp) {
                        $cotizacion_proveedor = CotizacionProveedor::find()->where(['id_solicitud_compra_proveedor' => $sp->id])->one();
                        if( $cotizacion_proveedor->estado == 0 ) {
                            $cotizacion_proveedor->estado = 3;// Notificar que el proveedor no envio cotizacion y perdio el chance 
                            //$cotizacion_proveedor->save();  
                        }elseif( $cotizacion_proveedor->estado == 1) {
                            $cotizacion_proveedor->estado = 2; // Ya aprobaron cotizacion y no tienen chance de editarlas
                        }                                                  
                        $cotizacion_proveedor->save();
                    }
                    
                    $model->codigo_estado = 3; //Por recibir
                    if ($model->save()) {
                        $estado_compra_pieza = EstadoCompraPieza::findOne(['codigo' => 3])->id; //Por recibir
                        $model_pipeline_compra_pieza = new PipelineCompraPieza();
                        $model_pipeline_compra_pieza->fecha = date('Y-m-d');
                        $model_pipeline_compra_pieza->id_solicitud_compra_pieza = $model->id;
                        $model_pipeline_compra_pieza->id_estado_compra_pieza = $estado_compra_pieza;
                        $model_pipeline_compra_pieza->save();
                        unset($model_pipeline_compra_pieza);
                        \Yii::$app->getSession()->setFlash('success', '¡La orden de compra se ha creado con éxito!');
                        return $this->redirect(['solicitud-compra-estacion/indexsolicitudaprobacion/']);
                    } else{
                        \Yii::$app->getSession()->setFlash('error', '¡Error al generar la orden de compra!');
                        return $this->redirect(['solicitud-compra-estacion/gestionaprobacion/'. $id]);
                    }
                }
            }elseif($estado == 2) {
                // print_r($mlp = $solicitud['observacion_gerente']);
                // die();
                if ($mlp == 1 && $estado == 2 && \Yii::$app->user->can('gerente-compras') || $mlp == 2 && $estado == 2 && \Yii::$app->user->can('gerente-compras') || $mlp == 2 && $estado == 2 && \Yii::$app->user->can('director-comercial')) {
                    
                    $id_cotizacion_proveedor_pieza = Yii::$app->request->post('id');
                    if ($model->load(Yii::$app->request->post())) {
                        $model->aprobacion = 5; //Para saber que rechazo la coticacion enviada por el analista
                        $model->save();
                    }
                    
                    foreach ($id_cotizacion_proveedor_pieza as $i => $icpp) {
                        $cotizacion = CotizacionProveedorPieza::findOne($icpp);
                        $cotizacion->aprobacion = 5; //Para saber que rechazo la coticacion enviada por el analista
                        $cotizacion->save();
                    }
                    \Yii::$app->getSession()->setFlash('success', 'La solicitud ha sido rechazada con éxito');
                    return $this->redirect(['solicitud-compra-estacion/indexsolicitudaprobacion/']);
                }
            }
        }
    }

    public function ordenCompra($id) {
        $cotizaciones_piezas = CotizacionProveedorPieza::find()->joinWith('idCotizacionProveedor')->joinWith('idCotizacionProveedor.idSolicitudCompraProveedor')->where('solicitud_compra_proveedor.id_solicitud_compra_pieza ='.$id .' AND cotizacion_proveedor_pieza.aprobacion = 1')->all();
        foreach ($cotizaciones_piezas as $i => $pieza) {
            if ($pieza) {    

                $model_orden_compra = OrdenCompraPieza::find()->where(['id_cotizacion_proveedor' => $pieza->id_cotizacion_proveedor])->one();
                $total_pieza_descuento = $pieza->costo - ( ($pieza->costo * $pieza->ahorro) / 100 ) ;
                $total_pieza_itbs = ($total_pieza_descuento * 0.18 ) + $total_pieza_descuento;
                
                if (!$model_orden_compra) {
                    
                    $model_orden_compra = new OrdenCompraPieza();
                    $model_orden_compra->fecha = date("Y-m-d H:i:s");
                    $model_orden_compra->estado = 0; //creado
                    $model_orden_compra->id_solicitud_compra_pieza = $id;
                    $model_orden_compra->id_cotizacion_proveedor = $pieza->id_cotizacion_proveedor;
                    $model_orden_compra->total = $total_pieza_itbs;
                    $model_orden_compra->id_proveedor = $pieza->idCotizacionProveedor->idSolicitudCompraProveedor->id_proveedor;
                    $model_orden_compra->save();

                } else{
                    $total = ($model_orden_compra->total)? $model_orden_compra->total : 0;                    
                    $model_orden_compra->total = $total + $total_pieza_itbs;
                    $model_orden_compra->save();
                }

                $pieza->aprobacion = 2; //Aprobado por el gerente de compras
                $pieza->save();
            }
        }
    }

    /**
     * Checklist de recepcion
     */
    public function actionChecklistrecepcion($id){

        $model_cotizacion_proveedor = CotizacionProveedor::findOne($id);
        $model_orden_compra = OrdenCompraPieza::find()->where([ 'id_cotizacion_proveedor' => $model_cotizacion_proveedor->id ])->one();

        if (Yii::$app->request->post()) {
            $cantidades_recibidas = Yii::$app->request->post('recibido');

            $serial = Yii::$app->request->post('serial');
            $piezas = Yii::$app->request->post('pieza_id');
            $j = 0;
            foreach ($cantidades_recibidas as $i => $recibido) {
                $model_cotizacion_proveedor_pieza = CotizacionProveedorPieza::findOne($piezas[$i]);
                $model_cotizacion_proveedor_pieza->inventario = 1;
                $model_cotizacion_proveedor_pieza->cantidad_recibida = $recibido;
                $model_cotizacion_proveedor_pieza->serial = $serial[$i];
                $model_cotizacion_proveedor_pieza->save();
                unset($model_cotizacion_proveedor_pieza);
            }
    

            if ($model_orden_compra->load(Yii::$app->request->post())) {
                $model_orden_compra->save();
            }

            \Yii::$app->getSession()->setFlash('success', '¡La recepción se ha guardado con éxito para el proveedor!');
            return $this->redirect(['solicitud-compra-estacion/index']);
        } else{
            $model_solicitud_compra_pieza = SolicitudCompraPieza::findOne( $model_orden_compra->id_solicitud_compra_pieza );
            $model_solicitud_compra_estacion = $this->findModel($model_solicitud_compra_pieza->id_solicitud_compra_estacion);
            $model_vehiculo = Vehiculo::findOne($model_solicitud_compra_estacion->id_vehiculo);
            $model_estacion = Estacion::findOne($model_solicitud_compra_estacion->id_estacion);
            $model_proveedor = Proveedor::findOne($model_orden_compra->id_proveedor);

            $searchModel = new CotizacionProveedorPiezaSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->where('id_cotizacion_proveedor = '. $model_cotizacion_proveedor->id .' AND aprobacion = 2');

            return $this->render('checklist-recepcion', [
                'model_orden_compra' => $model_orden_compra,
                'model_vehiculo' => $model_vehiculo,
                'dataProvider' => $dataProvider,
                'model_proveedor' => $model_proveedor,
                'model_estacion' => $model_estacion,
                'model_solicitud_compra_pieza' => $model_solicitud_compra_pieza,
                'model_solicitud_compra_estacion' => $model_solicitud_compra_estacion
            ]);
        }
        
    }

    /**
     * Finalizar recepciones por parte de km
     */
    public function actionFinalizarrecepcion(){
        if (Yii::$app->request->post()) {
            $id = Yii::$app->request->post('id');
            $observacion_recepcion = Yii::$app->request->post('observacion_recepcion');
            $model = SolicitudCompraPieza::findOne($id);            
            $model->codigo_estado = 4;  //Asignacion de ruta
            if ($observacion_recepcion) {
                $model->observacion_recepcion = $observacion_recepcion;
            }
            if ($model->save()) {
                $estado_compra_pieza = EstadoCompraPieza::findOne(['codigo' => 4])->id; //Asignacion de ruta
                $model_pipeline_compra_pieza = new PipelineCompraPieza();
                $model_pipeline_compra_pieza->fecha = date('Y-m-d');
                $model_pipeline_compra_pieza->id_solicitud_compra_pieza = $model->id;
                $model_pipeline_compra_pieza->id_estado_compra_pieza = $estado_compra_pieza;
                $model_pipeline_compra_pieza->save();
                unset($model_pipeline_compra_pieza);
                \Yii::$app->getSession()->setFlash('success', '¡La recepción se ha finalizado con éxito!');
            } else{
                \Yii::$app->getSession()->setFlash('error', '¡Hubo un error!');
            }
           
            return;
        }
    }

     /**
     * Despacho
     */
    public function actionDespacho( $id ){
        $model = SolicitudCompraPieza::findOne($id);   

        $searchModel = new DespachoSearch([ 'id_solicitud_compra_pieza' => $id ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);     
        $dataProvider->query->orderBy('id DESC');

        $searchModelPieza = new CotizacionProveedorPiezaSearch();
        $dataProviderPieza = $searchModelPieza->search(Yii::$app->request->queryParams);
        $dataProviderPieza->query->joinWith('idCotizacionProveedor')->joinWith('idCotizacionProveedor.idSolicitudCompraProveedor')->where('solicitud_compra_proveedor.id_solicitud_compra_pieza = '. $model->id .' AND aprobacion = 2');
        $cantidad_piezas = $dataProviderPieza->getTotalCount();

        //$searchModel = new CotizacionProveedorPiezaSearch();
        $dataProviderDespacho = $searchModelPieza->search(Yii::$app->request->queryParams);
        $dataProviderDespacho->query->joinWith('idCotizacionProveedor')->joinWith('idCotizacionProveedor.idSolicitudCompraProveedor')->where('solicitud_compra_proveedor.id_solicitud_compra_pieza = '. $model->id .' AND aprobacion = 2 AND estado_despacho = 1');
        $cantidad_piezas_despacho = $dataProviderDespacho->getTotalCount();

        return $this->render('despacho', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'cantidad_piezas' => $cantidad_piezas,
            'cantidad_piezas_despacho' => $cantidad_piezas_despacho,

        ]);
    }

    /**
     * Creando envio
     */
    public function actionCrearenvio( $id ){

        $model = SolicitudCompraPieza::findOne($id);  
        $model_despacho = Despacho::find()->where( ['id_solicitud_compra_pieza' => $model->id] )->select('numero')->max('numero');
        $model_solicitud_compra_estacion = $this->findModel($model->id_solicitud_compra_estacion);

        $numero = 1;
        if ( count($model_despacho) != 0 ) {
            $numero = $model_despacho + 1;
        }

        $model_despacho = new Despacho();
        $model_despacho->isNewRecord = true;
        $model_despacho->numero = $numero;
        $model_despacho->estado = 0; //En proceso
        $model_despacho->id_solicitud_compra_pieza = $model->id;
        $model_despacho->estado_recepcion_taller = 0;
        $model_despacho->id_estacion = $model_solicitud_compra_estacion->id_estacion;
        $model_despacho->save();


        if ($model_despacho->save()) {
            return $this->redirect(['despachopiezas',
                'id' => $model_despacho->id,
            ]);
        } else{
            \Yii::$app->getSession()->setFlash('error', 'Error al crear el envío.');
            return $this->redirect(['despacho',
                'id' => $model->id,
            ]);
        }
    }

    public function actionDespachopiezas( $id ){
        $model_despacho = Despacho::findOne($id);
        $model_despacho->scenario = 'update';

        if ( Yii::$app->request->post() ) {
            if ( $model_despacho->load(Yii::$app->request->post()) && $model_despacho->validate() ) {
                $piezas = Yii::$app->request->post('selection');
                if ($piezas) {
                    $cantidades = explode(",", Yii::$app->request->post('cantidades_enviar'));
                
                    //Aqui creo que falta validar la cantidad para quitarla de pendiente por despacho
                    foreach ($piezas as $key => $pieza) {
                        $model_despacho_proveedor_pieza = new DespachoProveedorPieza();
                        $model_despacho_proveedor_pieza->isNewRecord = true;
                        $model_despacho_proveedor_pieza->cantidad_enviada_taller = $cantidades[$key];
                        $model_despacho_proveedor_pieza->id_despacho = $model_despacho->id;
                        $model_despacho_proveedor_pieza->id_cotizacion_proveedor_pieza = $pieza;
                        $model_despacho_proveedor_pieza->save();

                        $model_cotizacion_proveedor_pieza = CotizacionProveedorPieza::findOne($pieza);
                        $model_cotizacion_proveedor_pieza->estado_despacho = 1; //ENVIADO A MENSAJERO
                        $model_cotizacion_proveedor_pieza->save();
                    }

                    $model_despacho->fecha = date('Y-m-d');
                    $model_despacho->estado = 1; // ENVIADO A MENSAJERO
                    $model_despacho->traslado = 'TKM100-'.$model_despacho->id;

                    if ($model_despacho->save()) {
                        \Yii::$app->getSession()->setFlash('success', 'Envío guardado con éxito.');
                        return $this->redirect([ 'despacho', 'id' => $model_despacho->id_solicitud_compra_pieza ]);
                    } else{
                        \Yii::$app->getSession()->setFlash('error', 'Ocurrió un error guardando el envío, comuniquese con el administrador.');
                        return; 
                    }
                } else{
                    \Yii::$app->getSession()->setFlash('error', 'Debe seleccionar al menos una pieza a enviar');
                    return; 
                }
            } else{
                \Yii::$app->getSession()->setFlash('error', ($model_despacho->getErrors('id_mensajero'))?$model_despacho->getErrors('id_mensajero')[0]: 'Ocurrió un error guardando el envío, comuniquese con el administrador.');
                return;
            }
        } else {
            if ( $model_despacho->estado != 1 ) {

                $model = SolicitudCompraPieza::findOne($model_despacho->id_solicitud_compra_pieza);

                $searchModel = new CotizacionProveedorPiezaSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                $dataProvider->query->joinWith('idCotizacionProveedor')->joinWith('idCotizacionProveedor.idSolicitudCompraProveedor')->where('solicitud_compra_proveedor.id_solicitud_compra_pieza = '. $model->id .' AND aprobacion = 2 AND estado_despacho = 0');
                
                return $this->render('despacho-piezas', [
                    'model' => $model,
                    'model_despacho' => $model_despacho,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]);
            } else{
                \Yii::$app->getSession()->setFlash('error', 'El envío ya se ha realizado.');
                return $this->redirect([ 'despacho', 'id' => $model_despacho->id ]); 
            }
        }
    }

    //Representantes de reparto
    public function actionListado(){
        $searchModel = new DespachoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where('id_mensajero ='.Yii::$app->user->identity->id_mensajero.' AND estado != 3 AND id_estacion is not null');
        $dataProvider->query->orderBy('id DESC');
        return $this->render('listado-despacho', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);        
    }

    //representantes de reparto
    public function actionAsignadas($id){

        $searchModel = new DespachoProveedorPiezaSearch();
        $dataProvider = $searchModel->searchEstacion(Yii::$app->request->queryParams,$id);
        
        if ( Yii::$app->request->post() ) {
            $piezas = Yii::$app->request->post('selection');
            $despacho = Despacho::findOne($id);
            $despacho->estado = 2;// Estado 2 para saber que fue enviada a la Estacion 
            $despacho->update();
            foreach ($piezas as $key => $pieza) {                
                $model_despacho_proveedor_pieza = DespachoProveedorPieza::findOne($pieza);
                $model_despacho_proveedor_pieza->recibido = 1;
    
                $model_cotizacion_proveedor_pieza = CotizacionProveedorPieza::findOne($model_despacho_proveedor_pieza->id_cotizacion_proveedor_pieza);
                $model_cotizacion_proveedor_pieza->estado_despacho = 2; //RECIBIDO POR EL MENSAJERO
                $model_cotizacion_proveedor_pieza->save();
    
                if ($model_despacho_proveedor_pieza->update()) {
                    $band = 1;                   
                }
            } 
            if($band == 1) {
                return $this->redirect([ 'listado']); 
            }
    
        }
            
        return $this->render('piezas-asignadas', [
            // 'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    //Checklist piezas recibidas estación
    public function actionPiezasRecibidas($id) {
        $searchModel = new DespachoProveedorPiezaSearch();
        $dataProvider = $searchModel->searchPiezasEstacion(Yii::$app->request->queryParams,$id);
        $dataProviderRecibidas = $searchModel->searchPiezasEstacionRecibidas(Yii::$app->request->queryParams,$id);
    
        if ( Yii::$app->request->post() ) {
            $piezas = Yii::$app->request->post('selection');
            $despacho = Despacho::findOne($id);
    
            foreach ($piezas as $key => $pieza) {               
                $model_despacho_proveedor_pieza = DespachoProveedorPieza::findOne($pieza);
                $model_despacho_proveedor_pieza->recibido = 2;//Entregada a estación
    
                $model_cotizacion_proveedor_pieza = CotizacionProveedorPieza::findOne($model_despacho_proveedor_pieza->id_cotizacion_proveedor_pieza);
                $model_cotizacion_proveedor_pieza->estado_despacho = 3; //Entregado a estación
                $model_cotizacion_proveedor_pieza->inventario = 3; //Entregado a estación
                $model_cotizacion_proveedor_pieza->save();
    
                if ($model_despacho_proveedor_pieza->update()) {
                    $tr = 0;        
    
                }
            }
    
            //Piezas recibidas de estación por despacho
            $piezas_enviadas = DespachoProveedorPieza::find()->where(['id_despacho' => $id])->count();
            $piezas_recibidas_estacion_mensajero = DespachoProveedorPieza::find()->where(['id_despacho' => $id])->andWhere(['recibido' => 2])->count();
            if($piezas_enviadas == $piezas_recibidas_estacion_mensajero) {
                $despacho->estado = 3;// Entregada estación 
                $despacho->estado_recepcion_taller = 1;// Entregada estación 
                $despacho->update();
            }
    
            //Piezas Aprobadas por el analista y/o gerente de compras 
            $cotizaciones_piezas = CotizacionProveedorPieza::find()
            ->joinWith('idCotizacionProveedor')
            ->joinWith('idCotizacionProveedor.idSolicitudCompraProveedor')
            ->where('solicitud_compra_proveedor.id_solicitud_compra_pieza ='.$despacho->id_solicitud_compra_pieza .' AND cotizacion_proveedor_pieza.aprobacion = 2')
            ->count();
    
            //Cantidad de piezas recibidas por el estación
            $piezas_recibidas_estacion = DespachoProveedorPieza::find()
            ->select('`despacho_proveedor_pieza`.`id`')
            ->leftJoin('despacho', '`despacho`.`id` = `despacho_proveedor_pieza`.`id_despacho`')
            ->where(['despacho.id_solicitud_compra_pieza' => $despacho->id_solicitud_compra_pieza])
            ->andWhere(['`despacho_proveedor_pieza`.`recibido`' => 2])
            ->count();
    
            if($cotizaciones_piezas == $piezas_recibidas_estacion) {
    
                if ($tr == 0) {
                    $estado_compra_pieza = EstadoCompraPieza::findOne(['codigo' => 5])->id; //Entregada
                    $model_pipeline_compra_pieza = new PipelineCompraPieza();
                    $model_pipeline_compra_pieza->fecha = date('Y-m-d');
                    $model_pipeline_compra_pieza->id_solicitud_compra_pieza = $despacho->id_solicitud_compra_pieza;
                    $model_pipeline_compra_pieza->id_estado_compra_pieza = $estado_compra_pieza;
                    $model_pipeline_compra_pieza->save();
                    unset($model_pipeline_compra_pieza);
                        
                    $model_solicitud_compra_pieza = SolicitudCompraPieza::find()->where(['id' => $despacho->id_solicitud_compra_pieza])->one();
                    $model_solicitud_compra_pieza->codigo_estado = 5;//Entregada
                    $model_solicitud_compra_pieza->update();                                     
        
                    \Yii::$app->getSession()->setFlash('success', '¡Piezas recibidas!');
                    return $this->redirect(['solicitud-compra-estacion/listado/']);
                } else{
                    \Yii::$app->getSession()->setFlash('error', '¡Error !');
                    return $this->redirect(['solicitud-compra-estacion/piezas-recibidas/'. $id]);
                }
            }else {                
                \Yii::$app->getSession()->setFlash('success', '¡Piezas recibidas, aun quedan pendiente por recibir!');
                return $this->redirect(['solicitud-compra-estacion/piezas-recibidas/'. $id]);
            }
        } 
            
        return $this->render('recibidas-estacion', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataProviderRecibidas' => $dataProviderRecibidas,
        ]);    
    }
    
    /**
     * Index de la solicitud de ordenes de compra para el proveedor
     * @autor Zulma Hernández / @ajustes Albert
     */
    public function actionIndexordencompraproveedor()
    {
        $model_orden_compra_factura = new OrdenCompraFactura();

        $id_proveedor = User::findOne(Yii::$app->user->id)->id_proveedor;
        $searchModel = new OrdenCompraPiezaSearch(['id_proveedor' => $id_proveedor ]);
        $dataProvider = $searchModel->searchEstacion(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy('id DESC');
        
        return $this->render('index-orden-compra-proveedor', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'model_orden_compra_factura' => $model_orden_compra_factura,
        ]);
    }

    /**
     * Asociar factura a una orden de compra vista del proveedor
     * @autor Zulma Hernández / @ajustes Albert
     */
    public function actionAsociarfactura()
    {

        if (Yii::$app->request->post()) {
            $id = Yii::$app->request->post('id');
            $monto = Yii::$app->request->post('monto');

            $model_orden_compra_factura = new OrdenCompraFactura();
            $model_orden_compra = OrdenCompraPieza::findOne($id);

            if ($model_orden_compra_factura->load(Yii::$app->request->post())) {

                if ($monto == $model_orden_compra->total) {
                    $model_orden_compra->estado = 1;//Con factura asociada
                    $model_orden_compra->update();
                    $model_orden_compra_factura->fecha = date("Y-m-d H:i:s");
                    $model_orden_compra_factura->monto = $monto; 
                    $model_orden_compra_factura->estado = 0; //creado
                    $model_orden_compra_factura->id_orden_compra_pieza = $model_orden_compra->id;
                    if ($model_orden_compra_factura->save()) {
                        \Yii::$app->getSession()->setFlash('success', '¡La factura se ha asociado con éxito!');
                    } else{
                        \Yii::$app->getSession()->setFlash('error', 'Error, la factura no se pudo asociar');
                    }
                } else{
                    \Yii::$app->getSession()->setFlash('error', 'Error, El monto de la factura no coincide con el monto de la orden de compra.');
                }
            } else{
                \Yii::$app->getSession()->setFlash('error', 'Error al cargar la información de la factura');
            }
            return $this->redirect('indexordencompraproveedor');
        }
    }

    public function actionListarmodelos($id)
    {
        if (\Yii::$app->request->isAjax ) {
            $query = new Query();
            $query->select('*')
                ->from('modelo')
                ->where(['=','id_marca', $id])
                ->orderBy('nombre');
            $rows = $query->all();

            if (count($rows) > 0) {
                echo "<option value=''> Seleccione... </option>";
                foreach ($rows as $aux) {
                    echo "<option value='".$aux['id_modelo']."'>" . $aux['nombre'] ."</option>";
                }
            } else {
                echo "<option> -- </option>";
            }
        }
        \Yii::$app->end();
    }

    /*
    * PDF Translado de piezas - Logistica
    * @author Albert Vargas
    * @return mixed
    */
    public function actionImprimirtranslado($id) {

        $model_despacho = Despacho::findOne($id);
        $searchModel = new DespachoProveedorPiezaSearch();
        $dataProvider = $searchModel->searchEstacion(Yii::$app->request->queryParams,$id);
        $model_solicitud_compra_pieza = SolicitudCompraPieza::findOne($model_despacho->id_solicitud_compra_pieza);        
        $model = $this->findModel($model_solicitud_compra_pieza->id_solicitud_compra_estacion);
        $model_vehiculo = Vehiculo::findOne( $model->id_vehiculo );
        $model_orden_compra = OrdenCompraPieza::find()->where( ['id_solicitud_compra_pieza' => $model_solicitud_compra_pieza->id] )->one();

        $pdf = new Pdf([
            'content' => $this->renderPartial('_transladopiezaspdf', ['dataProvider' => $dataProvider, 'model_solicitud_compra_pieza' => $model_solicitud_compra_pieza, 'model_vehiculo' => $model_vehiculo, 'model_orden_compra' => $model_orden_compra, 'model' => $model]),
            'destination' => Pdf::DEST_BROWSER,
            'mode' => Pdf::MODE_CORE,
            'format' => Pdf::FORMAT_A4,
            'filename' => 'TRANSLADO_'.$id.'.pdf',
            'options' => [
                'title' => 'TRANSLADO DE PIEZAS',
                'subject' => 'TRANSLADO_'.$id,
            ],
        ]);

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');

        return $pdf->render(); 
        exit;

    }

    /**
     * Visualizar orden compra en PDF
     * @autor Zulma Hernández / @ajustes Albert Vargas
     */
    public function actionImprimirorden($id)
    {
        //Obtengo la orden de compra solicitada
        $model_orden_compra = OrdenCompraPieza::findOne( $id );
        //Obtengo la solicitud compra pieza asociada a la orden de compra
        $model_solicitud_compra_pieza = SolicitudCompraPieza::findOne($model_orden_compra->id_solicitud_compra_pieza);
        $model = SolicitudCompraEstacion::findOne( $model_solicitud_compra_pieza->id_solicitud_compra_estacion );
        $model_vehiculo = Vehiculo::findOne( $model->id_vehiculo );
        //Obtengo el proveedor asociado a la orden de compra
        $model_proveedor = Proveedor::findOne( $model_orden_compra->id_proveedor );
        //Obtengo las piezas aprobadas para la orden de compra
        $piezas = CotizacionProveedorPieza::find()->where(['id_cotizacion_proveedor' => $model_orden_compra->id_cotizacion_proveedor, 'aprobacion' => 2 ])->all();
 
        $pdf = new Pdf([
            'content' => $this->renderPartial('_ordencomprapieza',[ 'model_orden_compra' => $model_orden_compra, 'model_proveedor' => $model_proveedor, 'piezas' => $piezas, 'model' => $model, 'model_vehiculo' => $model_vehiculo ]),
            'destination' => Pdf::DEST_BROWSER,
            'mode' => Pdf::MODE_CORE,
            'format' => Pdf::FORMAT_A4,
            'filename' => 'OC_'.$id.'.pdf',
            'options' => [
                'title' => 'ORDEN DE COMPRA',
                'subject' => 'OC_'.$id,
            ],
        ]);
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');

        return $pdf->render(); 
        exit;
    }

    /**
     * Index de la solicitud de ordenes de compra de taller
     * @autor Zulma Hernández / @ajustes Albert Vargas 
     */
    public function actionIndexordencompra()
    {
        $model_orden_compra_factura = new OrdenCompraFactura();

        $searchModel = new OrdenCompraPiezaSearch();
        $dataProvider = $searchModel->searchEstacion(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy('id DESC');
        
        return $this->render('index-orden-compra', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'model_orden_compra_factura' => $model_orden_compra_factura,
        ]);
    }

    /**
     * Guardar Imagen Piezas Analista
     * @autor Albert Vargas
     */
    public function actionGuardarimagenpieza()
    {
        // print_r($_FILES);
        // die();
        if (Yii::$app->request->post()) {
            $count = count(Yii::$app->request->post('id'));
            $id = Yii::$app->request->post('id');
            $observacion = Yii::$app->request->post('observacion_pieza');

            for ($i = 0; $i < $count; $i++) {
                $rutabd = '';
                if($_FILES['SolicitudPiezaEstacion']['name']['imageFiles'][$i] != NULL) {
                    $nom_img  = str_replace(' ', '', $_FILES['SolicitudPiezaEstacion']['name']['imageFiles'][$i]);
                    $ruta = './uploads/imagen-piezas/pieza-'.$id[$i].'-'.$nom_img;
                    $rutabd = '/uploads/imagen-piezas/pieza-'.$id[$i].'-'.$nom_img;
                }

                $model_cotizacion_taller_pieza = SolicitudPiezaEstacion::find()->where([ 'id' => $id[$i] ])->one();
                $model_cotizacion_taller_pieza->observacion_pieza = $observacion[$i];

                if($_FILES['SolicitudPiezaEstacion']['name']['imageFiles'][$i] != NULL) {
                    $model_cotizacion_taller_pieza->imagen_pieza = $rutabd;
                }

                if($model_cotizacion_taller_pieza->save()) {
                    if($_FILES['SolicitudPiezaEstacion']['name']['imageFiles'][$i] != NULL) {
                        move_uploaded_file($_FILES['SolicitudPiezaEstacion']['tmp_name']['imageFiles'][$i], $ruta);
                    }
                }   
               

            }

            if ( $model_cotizacion_taller_pieza->save() ) {
                \Yii::$app->getSession()->setFlash('success', '¡La información se ha guardado con éxito!');
            } else{
                \Yii::$app->getSession()->setFlash('error', '¡Error al guardar la información!');
            }

            $this->redirect('index');
        }
    }

    /**
     * Guardar Imagen Piezas Proveeedor
     * @autor Albert Vargas
     */
    public function actionGuardarimagenpiezaproveedor()
    {
        if (Yii::$app->request->post()) {
            $count = count(Yii::$app->request->post('id'));
            $id = Yii::$app->request->post('id');
            $observacion = Yii::$app->request->post('observacion_pieza');

            for ($i = 0; $i < $count; $i++) {
                $rutabd = '';
                if($_FILES['CotizacionProveedorPieza']['name']['imageFiles'][$i] != NULL) {
                    $nom_img  = str_replace(' ', '', $_FILES['CotizacionProveedorPieza']['name']['imageFiles'][$i]);
                    $ruta = './uploads/imagen-piezas-proveedor/pieza-'.$id[$i].'-'.$nom_img;
                    $rutabd = '/uploads/imagen-piezas-proveedor/pieza-'.$id[$i].'-'.$nom_img;
                }

                $model_cotizacion_taller_pieza = CotizacionProveedorPieza::find()->where([ 'id' => $id[$i] ])->one();
                $model_cotizacion_taller_pieza->observacion_pieza = $observacion[$i];

                if($_FILES['CotizacionProveedorPieza']['name']['imageFiles'][$i] != NULL) {
                    $model_cotizacion_taller_pieza->imagen_pieza = $rutabd;
                }

                if($model_cotizacion_taller_pieza->save()) {
                    if($_FILES['CotizacionProveedorPieza']['name']['imageFiles'][$i] != NULL) {
                        move_uploaded_file($_FILES['CotizacionProveedorPieza']['tmp_name']['imageFiles'][$i], $ruta);
                    }
                }   
               

            }

            if ( $model_cotizacion_taller_pieza->save() ) {
                \Yii::$app->getSession()->setFlash('success', '¡La información se ha guardado con éxito!');
            } else{
                \Yii::$app->getSession()->setFlash('error', '¡Error al guardar la información!');
            }

            $this->redirect(Yii::$app->request->referrer);
        }
    }

    public function actionIndexcotizacionesrecibidas($id)
    {
        $model = SolicitudCompraPieza::findOne($id);
        $piezas = ArrayHelper::map(SolicitudPiezaEstacion::find()->where('id_solicitud_compra_estacion = '. $model->id_solicitud_compra_estacion)->all(), 'id_pieza', 'idPieza.nombre');
        $cotizaciones_piezas = CotizacionProveedorPieza::find()->joinWith('idCotizacionProveedor')->joinWith('idCotizacionProveedor.idSolicitudCompraProveedor')->where('solicitud_compra_proveedor.id_solicitud_compra_pieza ='.$model->id )->all();

        return $this->render('index-cotizaciones-recibidas', [
            'model' => $model,
            'cotizaciones_piezas' => $cotizaciones_piezas,
            'piezas' => $piezas,
        ]);
    }

    public function actionGetcasos()
    {
        $out = [];
        if (isset($_POST['id'])) {
             $id_vehiculo = Yii::$app->request->post('id',0);
            if ($id_vehiculo > 0) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $tipo_caso = 0;
                $model_vehiculo = Vehiculo::find()->where(['mva'=>$id_vehiculo])->one();
                $out = Caso::find()->where(['placa' => $model_vehiculo->placa, 'tipo_caso' => $tipo_caso ])->orderBy('id_caso')->all();
                echo Json::encode(['output'=>$out, 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }

    public function actionCheckCaso(){

        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            
            $id = Yii::$app->request->post('id',0);
            $caso = Caso::findOne($id);

            if($caso->id_tipo_accidente ==14){
            
            echo Json::encode(['verificado'=>1]);
            
            }else{
            echo Json::encode(['verificado'=>0]);
            }

        }


    }

    /**
     * Finds the SolicitudCompraEstacion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SolicitudCompraEstacion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SolicitudCompraEstacion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
