<?php

namespace app\controllers;

use Yii;
use app\models\MantenimientoPreventivo;
use app\models\MantenimientoPreventivoSearch;
use app\models\Vehiculo;
use app\models\ActividadMntoSearch;
use app\models\Consumo;
use app\models\ProyeccionMantenimiento;
use app\models\EstadoVehiculo;
use app\models\Estado;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\UploadMantenimiento;
use yii\web\UploadedFile;
use yii\db\Query;
/**
 * MantenimientoPreventivoController implements the CRUD actions for MantenimientoPreventivo model.
 */
class MantenimientoPreventivoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MantenimientoPreventivo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MantenimientoPreventivoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MantenimientoPreventivo model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MantenimientoPreventivo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @autor Zulma Hernández
     */
    public function actionCreate($id)
    {
        $model = new MantenimientoPreventivo();
         $acuerdo = new UploadMantenimiento();

        if ( $id != 0 ) {
            $model_vehiculo = Vehiculo::findOne($id);
        } else {
            $model_vehiculo = new Vehiculo();
        }

        $ultimo_mantenimiento = ProyeccionMantenimiento::find( )->where(['id_vehiculo' => $id])->orderby('id DESC')->one();
        if ($model->load(Yii::$app->request->post()) ) {
       
            // 1) Guardando el mantenimiento
            $model->fecha_realizacion = date('Y-m-d');
            $model->id_tipo_mantenimiento = $ultimo_mantenimiento->id_proximo_tipo_mantenimiento;
            $model->id_proyeccion_mantenimiento = $ultimo_mantenimiento->id;
            $model->id_usuario_registra = Yii::$app->user->identity->id;
            $model->id_vehiculo = $model_vehiculo->id;

            if ($model->save()) {
 

                $acuerdo->imageFiles = UploadedFile::getInstances($acuerdo, 'imageFiles');
                $acuerdo->upload($model->id);

                // 2) Actualizando la proyección
                $ultimo_mantenimiento->realizado = 1;
                $ultimo_mantenimiento->save();

                // 3) Se calcula la diferencia del ultimo mantenimiento proyectado con el km actual para saber cuantos mantenimientos se deben registrar
                $diferencia = abs(ceil((floatval($model->km) - floatval($ultimo_mantenimiento->km_proyectado)) / 5000));

                $diferencia = ( $diferencia == 0 ) ? 1 : $diferencia; //Si el mantenimiento se realiza antes del proyectado

                // 4) Creando la nueva proyeccion
                for ( $i = 0;  $i < $diferencia;  $i ++) { 
                    $model_proyeccion = new ProyeccionMantenimiento();
                    $model_proyeccion->isNewRecord = true;
                    $model_proyeccion->km_proyectado = strval( $ultimo_mantenimiento->km_proyectado + (5000 * ($i+1)) );
                    $model_proyeccion->realizado = 0;
                    $model_proyeccion->serie = ($ultimo_mantenimiento->serie < 6)?( $ultimo_mantenimiento->serie + ($i+1) ): 1;
                    $model_proyeccion->id_vehiculo = $model_vehiculo->id;

                    if ( $model_proyeccion->serie == 1 || $model_proyeccion->serie == 2 || $model_proyeccion->serie == 4 || $model_proyeccion->serie == 5) {
                        $model_proyeccion->id_proximo_tipo_mantenimiento = 1;
                    } elseif ( $model_proyeccion->serie == 3 ) {
                        $model_proyeccion->id_proximo_tipo_mantenimiento = 2;
                    } elseif ( $model_proyeccion->serie == 6 ) {
                        $model_proyeccion->id_proximo_tipo_mantenimiento = 3;
                    }

                    $model_proyeccion->save();
                    //unset($model_proyeccion);
                }

                // 5) Se actualiza el estado del vehiculo con respecto al mantenimiento 

                $estado_vehiculo = EstadoVehiculo::find()->joinWith('idEstado')->where('estado.id_tipo_estado = 4 AND estado_vehiculo.id_vehiculo = ' . $model_vehiculo->id )->orderBy('estado_vehiculo.id DESC')->one();

                if ( floatval($model_vehiculo->km) <= floatval($model_proyeccion->km_proyectado) && floatval($model_vehiculo->km) < (floatval($model_proyeccion->km_proyectado) - 600 ) && $model_proyeccion->realizado == 0 )  {
                    //El estatus del mantenimiento es Vigente
                    if ( count($estado_vehiculo) == 0 || (count($estado_vehiculo) > 0 && $estado_vehiculo->idEstado->nombre != 'Vigente') ) {
                        $this->actualizarEstado($model_vehiculo->id, 'Vigente');
                    }
                //Si el km del vehiculo es mayor que el km proyectado - 500 pero menor al km proyecto el vehiculo se encuentra en estado Atrasado
                } else if ( floatval($model_vehiculo->km) >= floatval($model_proyeccion->km_proyectado - 500) && floatval($model_vehiculo->km) < floatval($model_proyeccion->km_proyectado)) {
                    //El estatus del mantenimiento es Atrasado
                    if ( count($estado_vehiculo) == 0 || (count($estado_vehiculo) > 0 && $estado_vehiculo->idEstado->nombre != 'Atrasado') ) {
                        $this->actualizarEstado($model_vehiculo->id, 'Atrasado');
                    }
                //Si el km del vehiculo se encutra entre 600 y 500 km antes de cumplirse el mantenimeinto proyectado el vehiculo se encuentra en estado Asignar
                } else if ( floatval($model_vehiculo->km) >= floatval($model_proyeccion->km_proyectado - 600) && floatval($model_vehiculo->km) <= floatval($model_proyeccion->km_proyectado - 500) ) {
                    //El estatus del mantenimiento es Asignar
                    if ( count($estado_vehiculo) == 0 || (count($estado_vehiculo) > 0 && $estado_vehiculo->idEstado->nombre != 'Asignar') ) {
                        $this->actualizarEstado($model_vehiculo->id, 'Asignar');
                    }
                }

                // 6) Guardando las cantidades consumidas de las piezas
                $cantidades = Yii::$app->request->post('cantidad');
                $cantidades_ids = Yii::$app->request->post('cantidad_id');

                foreach ($cantidades as $key => $cantidad) {
                    $model_consumo = new Consumo();
                    $model_consumo->isNewRecord = true;
                    $model_consumo->cantidad = $cantidad;
                    $model_consumo->id_actividad_pieza = $cantidades_ids[$key];
                    $model_consumo->id_mantenimiento_preventivo = $model->id;
                    $model_consumo->save();
                    unset($model_consumo);
                }
                \Yii::$app->getSession()->setFlash('success', 'Se registro el mantenimiento con éxito!');
                return $this->redirect(['/vehiculo/ficha/'.$id.'?type=3',]);

            } else{
                
                $searchModel = new ActividadMntoSearch(['id_tipo_mnto' => (($ultimo_mantenimiento)?$ultimo_mantenimiento->id_proximo_tipo_mantenimiento:1) ]);
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                
                \Yii::$app->getSession()->setFlash('error', 'Error al guardar el mantenimiento!');
                return $this->render('create', [
                    'model' => $model,
                    'id' => $id,
                    'model_vehiculo' => $model_vehiculo,
                    'ultimo_mantenimiento' => $ultimo_mantenimiento,
                    'dataProvider' => $dataProvider,
                    'acuerdo' => $acuerdo,
                ]);
            }

            /*echo "<pre>";
            print_r(Yii::$app->request->post());
            echo "</pre>";*/
            
        } else {
            
            $searchModel = new ActividadMntoSearch(['id_tipo_mnto' => (($ultimo_mantenimiento)?$ultimo_mantenimiento->id_proximo_tipo_mantenimiento:1) ]);
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            
            return $this->render('create', [
                'model' => $model,
                'id' => $id,
                'model_vehiculo' => $model_vehiculo,
                'ultimo_mantenimiento' => $ultimo_mantenimiento,
                'dataProvider' => $dataProvider,
                'acuerdo' => $acuerdo
            ]);
        }
    }


    /**
     * Actualizando el estado de mantenimiento del vehiculo
     * @autor Zulma Hernández
     * @param integer $id_vehiculo
     * @param string $estado
     * @return mixed
     */
    public function actualizarEstado( $id_vehiculo, $estado ){

        $model_estado_vehiculo = new EstadoVehiculo();
        $model_estado_vehiculo->isNewRecord = true;
        $model_estado_vehiculo->id_estado = Estado::findOne(['id_tipo_estado' => '4', 'nombre' => $estado ])->id;
        $model_estado_vehiculo->id_vehiculo = $id_vehiculo;
        $model_estado_vehiculo->save();
        unset($model_estado_vehiculo);
    }

    /**
     * Updates an existing MantenimientoPreventivo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MantenimientoPreventivo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MantenimientoPreventivo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MantenimientoPreventivo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MantenimientoPreventivo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}