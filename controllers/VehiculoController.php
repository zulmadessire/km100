<?php

namespace app\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use app\models\Vehiculo;
use app\models\Pieza;
use app\models\transito;
use app\models\Estacion;
use app\models\Flota;
use app\models\Notificaciones;
use app\models\Bitacora;
use app\models\Empresa;
use app\models\taller;
use app\models\PipelineServicio;
use app\models\TallerSearch;
use app\models\VehiculoSearch;
use app\models\WizardSearch;
use app\models\Concesionario;
use app\models\FlotaSearch;
use app\models\GarantiaVehiculoSearch;
use app\models\ResumenSearch;
use app\models\GarantiaVehiculo;
use app\models\Modelo;
use app\models\Marca;
use app\models\Consumo;
use app\models\ProyeccionMantenimiento;
use app\models\MantenimientoPreventivoSearch;
use app\models\ServicioSearch;
use app\models\UploadFileVehiculo;
use app\models\EstadoVehiculo;
use app\models\Estado;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;
use dosamigos\qrcode\QrCode;
use yii\web\UploadedFile;
use yii\bootstrap\Html;



/**
 * VehiculoController implements the CRUD actions for Vehiculo model.
 */
class VehiculoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Vehiculo models.
     * @return mixed
     */
    public function actionIndex( $type = 0 )
    {
        $searchModel = new VehiculoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere('transito = 0');
 
        $model_search = new VehiculoSearch();
        $model = new Vehiculo();
   

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model_search' => $model_search,
            'type' => $type
        ]);
    }

       /**
     * Lists all Vehiculo models en mantenimiento.
     * @return mixed
     */
    public function actionIndexmantenimiento( $type = 0 )
    {
        $searchModel = new VehiculoSearch();
        $dataProvider = $searchModel->mantenimientosearch(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere('transito = 0');
 
        $model_search = new VehiculoSearch();
        $model = new Vehiculo();
         if ($model->load(Yii::$app->request->post())) {
             echo " entro al boton buscar ";
            die();
         }

        return $this->render('index_mantenimiento', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model_search' => $model_search,
            'type' => $type
        ]);
    }

     /**
     * Lists all Vehiculos proximos a vencer.
     * @return mixed
     */
    public function actionIndexproximosvencer()
    {
        $searchModel = new VehiculoSearch();
        $dataProvider = $searchModel->proximosearch(Yii::$app->request->queryParams);
        // $dataProvider->query->andWhere('transito = 0');
 
        $model_search = new VehiculoSearch();
        $model = new Vehiculo();
     

        return $this->render('indexvencer', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model_search' => $model_search,
 
        ]);
    }

    public function actionExcelvehiculos($id_marca, $id_modelo, $mva, $id_cliente, $id_empresa, $id_estacion, $id_estado, $id_concesionario){
        $searchModel = new VehiculoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere('transito = 0');
 
            if ($id_marca != 0) {
                  $dataProvider->query->andwhere( '(SELECT id_marca FROM modelo WHERE id_modelo = vehiculo.id_modelo LIMIT 1) = '.$id_marca );
            }
            if($id_modelo != 0){
                 $dataProvider->query->andwhere( '(SELECT id_modelo FROM modelo WHERE id_modelo = vehiculo.id_modelo LIMIT 1) = '.$id_modelo);
            }
             if($id_cliente != 0){
                 $dataProvider->query->andwhere( '(SELECT cli.id FROM flota f INNER JOIN estacion es ON es.id_estacion = f.id_estacion INNER JOIN empresa emp ON es.id_empresa = emp.id_empresa INNER JOIN cliente cli ON cli.id=emp.cliente_id WHERE f.id_vehiculo = vehiculo.id ORDER BY f.id DESC LIMIT 1) = '.$id_cliente);
            }
            if($mva != 0){
                $dataProvider->query->andwhere(['mva'=>$mva]);
            }
            if($id_empresa != 0){
                 $dataProvider->query->andwhere( '(SELECT emp.id_empresa FROM flota f INNER JOIN estacion es ON es.id_estacion = f.id_estacion INNER JOIN empresa emp ON es.id_empresa = emp.id_empresa WHERE f.id_vehiculo = vehiculo.id ORDER BY f.id DESC LIMIT 1) = '.$id_empresa);
            }
            if($id_estacion != 0){
                 $dataProvider->query->andwhere( '(SELECT id_estacion FROM flota WHERE id_vehiculo = vehiculo.id ORDER BY id DESC LIMIT 1) = '.$id_estacion);
            }
             if($id_estado != 0){
                 $dataProvider->query->andwhere( '(SELECT ev.id_estado FROM estado_vehiculo ev INNER JOIN estado es ON es.id = ev.id_estado AND es.id_tipo_estado = 1  WHERE ev.id_vehiculo = vehiculo.id ORDER BY ev.id DESC LIMIT 1) = '.$id_estado);
            }
            if($id_concesionario != 0){
                 $dataProvider->query->andwhere( '(SELECT id FROM concesionario WHERE id = vehiculo.id_concesionario LIMIT 1) = '.$id_concesionario);
            }
        $model = $dataProvider->getModels();
 
        try{
            // Create new PHPExcel object
            $objPHPExcel = new \PHPExcel();
            
            // Fill worksheet from values in array
            $objWorkSheet = $objPHPExcel->createSheet(0);

            $array_fondo = array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'f4f4f4'));
            $array_letra = array( 'bold'  => true,'color' => array('rgb' => '000000'));

            /*$objWorkSheet
            ->setCellValue('A2', 'Marca: ')
            ->setCellValue('B2', Marca::findOne($model->idModelo->id_marca)->nombre)
            ->setCellValue('A3', 'Modelo: ')
            ->setCellValue('B3', $model->idModelo->nombre)
            ->setCellValue('A4', 'Color: ')
            ->setCellValue('B4', $model->color)
            ->setCellValue('A5', 'Transmisión: ')
            ->setCellValue('B5', $model->transmision)
            ->setCellValue('A6', 'Chasis: ')
            ->setCellValue('B6', $model->chasis)
            ->setCellValue('A7', 'Placa: ')
            ->setCellValue('B7', $model->placa)
            ->setCellValue('A8', 'Ficaha o MVA: ')
            ->setCellValue('B8', $model->mva );

            $objWorkSheet->getStyle('A2')->applyFromArray( array('font' => $array_letra ));
            $objWorkSheet->getStyle('A3')->applyFromArray( array('font' => $array_letra ));
            $objWorkSheet->getStyle('A4')->applyFromArray( array('font' => $array_letra ));
            $objWorkSheet->getStyle('A5')->applyFromArray( array('font' => $array_letra ));
            $objWorkSheet->getStyle('A6')->applyFromArray( array('font' => $array_letra ));
            $objWorkSheet->getStyle('A7')->applyFromArray( array('font' => $array_letra ));
            $objWorkSheet->getStyle('A8')->applyFromArray( array('font' => $array_letra ));*/


            /// ------------------------------------------------------------------------------
            $objWorkSheet
            ->setCellValue('A4', 'MVA')
            ->setCellValue('B4', 'Placa')
            ->setCellValue('C4', 'Chasis')
            ->setCellValue('D4', 'Marca')
            ->setCellValue('E4', 'Modelo')
            ->setCellValue('F4', 'Año')
            ->setCellValue('G4', 'Kilometraje')
            ->setCellValue('H4', 'Empresa')
            ->setCellValue('I4', 'Estación')
            ->setCellValue('J4', 'Concesionario')
            ->setCellValue('K4', 'Estatus');

            $objWorkSheet->getStyle('A4')->applyFromArray( array( 'fill' => $array_fondo,'font' => $array_letra ));
            $objWorkSheet->getStyle('B4')->applyFromArray( array( 'fill' => $array_fondo,'font' => $array_letra ));
            $objWorkSheet->getStyle('C4')->applyFromArray( array( 'fill' => $array_fondo,'font' => $array_letra ));
            $objWorkSheet->getStyle('D4')->applyFromArray( array( 'fill' => $array_fondo,'font' => $array_letra ));
            $objWorkSheet->getStyle('E4')->applyFromArray( array( 'fill' => $array_fondo,'font' => $array_letra ));
            $objWorkSheet->getStyle('F4')->applyFromArray( array( 'fill' => $array_fondo,'font' => $array_letra ));
            $objWorkSheet->getStyle('G4')->applyFromArray( array( 'fill' => $array_fondo,'font' => $array_letra ));
            $objWorkSheet->getStyle('H4')->applyFromArray( array( 'fill' => $array_fondo,'font' => $array_letra ));
            $objWorkSheet->getStyle('I4')->applyFromArray( array( 'fill' => $array_fondo,'font' => $array_letra ));
            $objWorkSheet->getStyle('J4')->applyFromArray( array( 'fill' => $array_fondo,'font' => $array_letra ));
            $objWorkSheet->getStyle('K4')->applyFromArray( array( 'fill' => $array_fondo,'font' => $array_letra ));

            $array_letra = array( 'bold'  => true,'color' => array('rgb' => 'ffffff'));

            for($j = 5; $j <= count($model)+4;$j++){

                // Estacion  Actual
                $query = new query();
                $query->select('em.nombre, es.nombre nombre_estacion')
                    ->from('flota f')
                    ->join('INNER JOIN','estacion es','es.id_estacion = f.id_estacion')
                    ->join('INNER JOIN','empresa em','es.id_empresa = em.id_empresa')
                    ->where('f.id_vehiculo = '. $model[$j-5]->id )
                    ->orderBy('f.id desc');

                $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                $agencia = Yii::$app->db->createCommand($sql)->queryOne();

                // Estado Actual
                $query = new query();
                $query->select('es.nombre, es.color')
                    ->from('estado_vehiculo ev')
                    ->join('INNER JOIN','estado es','es.id = ev.id_estado AND es.id_tipo_estado = 1')
                    ->where('ev.id_vehiculo = '. $model[$j-5]->id )
                    ->orderBy('ev.id desc');

                $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                $estado = Yii::$app->db->createCommand($sql)->queryOne();

                $array_fondo = array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => str_replace("#","",$estado['color'],$i)));

                $objWorkSheet->setCellValue('A'.$j, $model[$j-5]->mva); //MVA
                $objWorkSheet->setCellValue('B'.$j, $model[$j-5]->placa); //Placa
                $objWorkSheet->setCellValue('C'.$j, $model[$j-5]->chasis); //Chasis
                $objWorkSheet->setCellValue('D'.$j, $model[$j-5]->idModelo->idMarca->nombre); //Marca
                $objWorkSheet->setCellValue('E'.$j, $model[$j-5]->idModelo->nombre); //Modelo
                $objWorkSheet->setCellValue('F'.$j, '-'); //Año
                $objWorkSheet->setCellValue('G'.$j, $model[$j-5]->km); //Kilometraje
                $objWorkSheet->setCellValue('H'.$j, $agencia['nombre']); //Empresa
                $objWorkSheet->setCellValue('I'.$j, $agencia['nombre_estacion']); //Estacion
                $objWorkSheet->setCellValue('J'.$j, $model[$j-5]->idConcesionario->nombre); //Concesionario
                $objWorkSheet->setCellValue('K'.$j, $estado['nombre']); //Estatus

                $objWorkSheet->getStyle('K'.$j)->applyFromArray( array( 'fill' => $array_fondo,'font' => $array_letra ));
            }

            $objWorkSheet->setTitle('VEHICULOS');


            // Save Excel 2007 file
            $file = 'files/'.'REPORTE_VEHICULOS.xlsx';
            
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save($file);

            header('Content-Description: File Transfer');
            header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            header("Content-Disposition: attachment; filename=\"".basename($file)."\"");
            header("Content-Transfer-Encoding: binary");
            header("Expires: 0");
            header("Pragma: public");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header('Content-Length: ' . filesize($file)); //Remove

            ob_clean();
            flush();

            readfile($file);
            exit();
        }
        catch(Exception $e){
            die('Error');
        }  

    }

    /**
     * Displays a single Vehiculo model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Vehiculo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Vehiculo();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Vehiculo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Vehiculo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Vehiculo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Vehiculo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Vehiculo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Ficha del Vehiculo
     * @param integer $id
     * @return mixed
     */
    public function actionFicha($id, $type = 0)
    {
        $model = $this->findModel($id);

        if ( $type == 0 ) {
            $searchModel_flota = new FlotaSearch([ 'id_vehiculo' => $model->id ]);
            $dataProvider_flota = $searchModel_flota->search(Yii::$app->request->queryParams);
            $dataProvider_flota->query->orderBy('id DESC');

            return $this->render('ficha', [
                'model' => $model,
                'dataProvider_flota' => $dataProvider_flota,
                'type' => $type,
            ]);
        } else if ( $type == 1 ) {
            $model_gv = new \app\models\GarantiaVehiculo;

            $searchModel_gv = new GarantiaVehiculoSearch([ 'id_vehiculo' => $model->id ]);
            $dataProvider_gv = $searchModel_gv->search(Yii::$app->request->queryParams);
            $dataProvider_gv->query->orderBy('id');

            return $this->render('ficha', [
                'model' => $model,
                'id' => $id,
                'type' => $type,
                'dataProvider_gv' => $dataProvider_gv,
                'model_gv' => $model_gv
            ]);
        } else if ( $type == 2 ) {
            return $this->render('ficha', [
                'model' => $model,
                'id' => $id,
                'type' => $type,
            ]);
        } else if ( $type == 3 ) {
            $proyecciones = ProyeccionMantenimiento::find()->where([ 'id_vehiculo' => $id ])->orderby('id DESC')->limit(6)->all();
            $searchModel_mp = new MantenimientoPreventivoSearch();
            $dataProvider_mp = $searchModel_mp->search_realizados(Yii::$app->request->queryParams);
            $dataProvider_mp->query->orderBy('id DESC');
            
            return $this->render('ficha', [
                'model' => $model,
                'type' => $type,
                  'id' => $id,
                'proyecciones' => $proyecciones,
                'dataProvider_mp' => $dataProvider_mp,
                'searchModel_mp' => $searchModel_mp,
            ]);
        } else if ( $type == 4 ){
            $searchModel_servicio = new ServicioSearch([ 'id_vehiculo' => $id ]);
            $dataProvider_servicio = $searchModel_servicio->search(Yii::$app->request->queryParams);
            $dataProvider_servicio->query->orderBy('id DESC');

            return $this->render('ficha', [
                'id' => $id,
                'model' => $model,
                'type' => $type,
                'dataProvider_servicio' => $dataProvider_servicio,
                'searchModel_servicio' => $searchModel_servicio,
            ]);
        }
        
    }

    /**
     * Obtener modelos de una marca de vehiculo
     * @param integer $id
     * @return mixed
     */
    public function actionGetmodelo()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $id_marca = $parents[0];
                $out = Modelo::find()->select(['id' => 'id_modelo','name' =>'nombre' ])->where(['id_marca' => $id_marca])->orderBy('nombre')->asArray()->all();
                echo Json::encode(['output'=>$out, 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }

    /**
     * Obtener las empresas dependientes de un cliente
     * @param integer $id
     * @return mixed
     * @author Zulma Hernández
     */
    public function actionGetempresas()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $id_cliente = $parents[0];
                $out = Empresa::find()->select(['id' => 'id_empresa','name' =>'nombre' ])->where(['cliente_id' => $id_cliente])->orderBy('nombre')->asArray()->all();
                echo Json::encode(['output'=>$out, 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }

    /**
     * Obtener las estaciones de una agencia
     * @param integer $id
     * @return mixed
     */
    public function actionGetestaciones()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null && $parents ) {
                $id_empresa = $parents[0];
                $out = Estacion::find()->select(['id' => 'id_estacion','name' =>'nombre' ])->where(['id_empresa' => $id_empresa])->orderBy('nombre')->asArray()->all();
                echo Json::encode(['output'=>$out, 'selected'=>'']);
                return;
            }
 
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }
    
    /**
     * Obtener las estaciones de una agencia que tiene asociado un taller
     * @param integer $id
     * @return mixed
     */
    public function actionGetestacionestaller()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $id_agencia = $parents[0];
                $tall = 1;
                    $query = new query();
                    $query->select(["est.id_estacion as id", "est.nombre as name"])
                        ->from('estacion est')
                        ->join('INNER JOIN','empresa emp','emp.id_empresa = est.id_empresa')
                        ->where('emp.nombre = ' . "'".$id_agencia."'" )
                        ->andWhere('est.asoc_taller = ' . "'".$tall."'" );

                    $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                    $out = Yii::$app->db->createCommand($sql)->queryAll();
                }
                echo Json::encode(['output'=>$out, 'selected'=>'']);
                return;
            // }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }

    /**
     * Anular una garantía
     * @param integer $id
     * @return mixed
     */
    public function actionAnulargarantia()
    {
        if (Yii::$app->request->post()) {

            $id = Yii::$app->request->post('id');
            $motivo = Yii::$app->request->post('GarantiaVehiculo');
            $model_gv = GarantiaVehiculo::findOne($id);
            $model_gv->anular = '1';
            $model_gv->motivo = $motivo['motivo'];
            $model_gv->id_usuario_anula = Yii::$app->user->identity->id;
            $model_gv->fecha_anula = date('Y-m-d');
            
            if ($model_gv->save()) {
                \Yii::$app->getSession()->setFlash('success', 'Garantía anulada con éxito!');
            } else{
                \Yii::$app->getSession()->setFlash('error', 'Error anulando la Garantía!');
            }

            return;
        }
    }

    //lee los archivos wizard y los guarda en la base de datos (tabla vehiculo, flota y bitacora)
    public function actionWizard(){
        set_time_limit(3000);
        
        
        $modelwizard = new Estacion();

        //construccion del nombre de archivo
        $fecha = date('Y-m-j');
        $nuevafecha = strtotime ( '-1 day' , strtotime ( $fecha ) ) ;
        $nuevafecha = date ( 'Ymj' , $nuevafecha );
        $narchivo  = "FDO".$nuevafecha;
        //cuando se tenga la tarea programada este es el nombre del archivo

     
           $file = fopen("./files/FDO20170815.txt", "r") or die("Error");
           $salto = 0;
           // $vector[];
           $i=0;
             while(!feof($file)) {
                $linea = fgets($file);
                $salto = nl2br($linea);
                $vector[$i] = $salto;
                $i++;

            }
      //       for ($i=0; $i <sizeof($vector) ; $i++) { 
      //               $estacion = substr($vector[$i], 1194, 5); 
      //           $estaciones = intval($estacion);
      //       $est =  Estacion::find()->where([ 'id_estacion' => $estaciones ])->all();

      //       if(!$est){
      //           print_r($estaciones.'<br>');
      //       }
      // }die();
            for ($i=0; $i <sizeof($vector) ; $i++) { 

                

               $chasis = substr($vector[$i], 9, 20);
               $color = substr($vector[$i], 184, 3);
               $transmision =  substr($vector[$i], 212, 1);
               $placa =  substr($vector[$i], 363, 10);
               $mva =  substr($vector[$i], 0, 9);
               $categoria =  substr($vector[$i], 623, 1);
               $clase =  substr($vector[$i], 169, 1);
               $combustible =  substr($vector[$i], 166, 3);
               
               $anho = substr($vector[$i], 164, 2); 
               $frenovacion = substr($vector[$i], 338, 6); 
               $km =  substr($vector[$i], 38, 9);
               $modelo = 1;               
               $estacion = substr($vector[$i], 1194, 5); 
               // $estacion = substr($vector[$i], 1159, 5); 
               $estaciones = intval($estacion);
      
                             
               


               if($modelo=="KIA"){
                $marca = substr($vector[$i], 173, 4);
               }
               else{
                $marca = substr($vector[$i], 174, 4);
               }
                if($marca=="RIO"){
                 $marca = 31;
               }
               if($marca=="SPOR"){
                 $marca = 34;
               }
                if($marca==" SPO"){
                 $marca = 34;
               }
                 if($marca==" RIO"){
                 $marca = 31;
               }
               if($marca=="318I"){
                 $marca = 76;
               }
               if($marca=="COLO"){
                 $marca = 5;
               }
               if($marca=="SORE"){
                 $marca = 32;
               }
                 if($marca=="OUTL"){
                 $marca = 14;
               }
                if($marca==" FRO"){
                 $marca = 46;
               } 
               if($marca=="FRO "){
                 $marca = 46;
               } 
                if($marca==" URB"){
                 $marca = 49;
               }
                if($marca=="URB "){
                 $marca = 49;
               }
               if($marca=="KANG"){
                 $marca = 55;
               }
                 if($marca=="H1  "){
                 $marca = 23;
               }
                 if($marca=="  H1"){
                 $marca = 23;
               }
                if($marca=="EXPL"){
                 $marca = 12;
               }
                if($marca=="F150"){
                 $marca = 68;
               }
                if($marca=="YTAH"){
                 $marca = 7;
               }
                if($marca=="AVEO"){
                 $marca = 69;
               }
                if($marca=="RTRA"){
                 $marca = 8;
               }
                if($marca=="ACRV"){
                 $marca = 19;
               }
               if($marca==" CRV"){
                 $marca = 19;
               }
               if($marca=="CRV "){
                 $marca = 19;
               }
                if($marca=="FRON"){
                 $marca = 46;
               }
                if($marca=="GOL "){
                 $marca = 66;
               }
                if($marca=="L300"){
                 $marca = 70;
               }
                if($marca=="XTRA"){
                 $marca = 51;
               }
                if($marca=="URVA"){
                 $marca = 49;
               }
                if($marca=="RANG"){
                 $marca = 14;
               }
                if($marca=="ACCE"){
                 $marca = 21;
               }
                if($marca=="ACC "){
                 $marca = 21;
               }
               if($marca==" ACC"){
                 $marca = 21;
               }
                if($marca=="TUCS"){
                 $marca = 28;
               }
                if($marca=="DAH1"){
                 $marca = 23;
               }
                if($marca==" PIC"){
                 $marca = 30;
               }
                if($marca=="PIC "){
                 $marca = 30;
               }
                if($marca=="PICA"){
                 $marca = 30;
               }
                if($marca=="RIO "){
                 $marca = 31;
               }
                if($marca=="WWAY"){
                 $marca = 71;
               }
                if($marca=="SPOR"){
                 $marca = 32;
               }
                if($marca=="I-10"){
                 $marca = 26;
               }
                if($marca=="WWAY"){
                 $marca = 71;
               }
                if($marca=="N300"){
                 $marca = 6;
               }
                if($marca=="GRAN"){
                 $marca = 22;
               }
                if($marca=="I-10"){
                 $marca = 72;
               }
                if($marca=="BT50"){
                 $marca = 36;
               }
                if($marca=="L200"){
                 $marca = 43;
               }
                if($marca=="HILU"){
                 $marca = 63;
               }
                if($marca=="EXPE"){
                 $marca = 11;
               }
                if($marca=="VITA"){
                 $marca = 61;
               }
                if($marca=="UDOK"){
                 $marca = 15;
               }
                if($marca=="DUST"){
                 $marca = 54;
               }
                if($marca=="A3  "){
                 $marca = 2;
               }
                if($marca=="A6  "){
                 $marca = 74;
               }
                if($marca=="Q3AD"){
                 $marca = 3;
               }
                if($marca=="COOP"){
                 $marca = 40;
               }
                if($color=="BLK"){
                 $color = "NEGRO";
               }
                if($color=="SIL"){
                 $color = "PLATEADO";
               }
                if($color=="GRY"){
                 $color = "GRIS";
               }
                if($color=="WHI"){
                 $color = "BLANCO";
               }
                if($color=="RED"){
                 $color = "ROJO";
               }
                if($color=="BLU"){
                 $color = "AZUL";
               }
                if($color=="GLD"){
                 $color = "DORADO";
               }
                if($color=="BRO"){
                 $color = "MARRÓN";
               }
               $existe =  Vehiculo::find()->where([ 'chasis' => $chasis, 'wizard'=>1])->all();
  
            if($chasis){
               $modelbitacora = new Bitacora();
               $modelbitacora->color = $color;
               $modelbitacora->transmision = $transmision;
               $modelbitacora->placa = $placa;
               $modelbitacora->mva = $mva;
               $modelbitacora->chasis = $chasis;
               $modelbitacora->km = $km;
               $modelbitacora->wizard = 1;
               // $modelbitacora->id_categoria = $categoria;
               $modelbitacora->id_categoria = 1;
               $modelbitacora->clase = $clase;
               $modelbitacora->combustible = $combustible;
 
               $modelbitacora->anho = $anho;
  
               $modelbitacora->fecha_renovacion_placa = $frenovacion;
               if($marca!=0){

               $modelbitacora->id_modelo = $marca;
               }
                $modelbitacora->save(false);
               if($existe){
                   $existe[0]->color = $color;
                   $existe[0]->transmision = $transmision;
                   $existe[0]->placa = $placa;
                   $existe[0]->mva = $mva;
                   $existe[0]->chasis = $chasis;
                   // $existe[0]->id_categoria = $categoria;
                   $existe[0]->id_categoria = 1;
                   $existe[0]->wizard = 1;
                   $existe[0]->clase = $clase;
                   $existe[0]->combustible = $combustible;
   
                   $existe[0]->km = $km;
                   $existe[0]->anho = $anho;
                      if($marca!=0){
                
               $existe[0]->id_modelo = $marca;
               }
                   $existe[0]->fecha_renovacion_placa = $frenovacion;

                   $existe[0]->update(false);
               }else{
               $modelwizard = new Vehiculo();
               $modelwizard->color = $color;
               $modelwizard->transmision = $transmision;
               $modelwizard->placa = $placa;
               $modelwizard->mva = $mva;
               $modelwizard->chasis = $chasis;
               $modelwizard->km = $km;
               $modelwizard->wizard = 1;
                $modelwizard->id_categoria = $categoria;
               $modelwizard->id_categoria = 1;
               $modelwizard->clase = $clase;
               $modelwizard->combustible = $combustible;
 
               $modelwizard->anho = $anho;
               $modelwizard->fecha_renovacion_placa = $frenovacion;
               if($marca!=0){

               $modelwizard->id_modelo = $marca;
               }
                $modelwizard->save(false);
               }
              }
              // echo $km.'<br>';

            $km =  substr($vector[$i], 38, 9);
            //ojo esto es por lo que no se tiene definido bien la estacion
            $estaciones = intval($estacion);
            $est =  Estacion::find()->where([ 'id_estacion' => $estaciones ])->all();
 
            $id_carro =  Vehiculo::find()->where([ 'chasis' => $chasis,'wizard'=>1 ])->all();
            
             
            $fech = substr($nuevafecha, 0, 4).'-'.substr($nuevafecha, 4, 2).'-'.substr($nuevafecha, 6, 2);
     
            if($id_carro){
            $id = $id_carro[0]->id;
            //este me dice si el vehoiculo ya esta en flota
            $id_f = Flota::find()->where([ 'id_vehiculo' => $id ])->all();
            
               if(!$id_f){
                 $modelflota = new Flota();
                 $modelflota->fecha_inicio = $fech;
                 $modelflota->id_vehiculo = $id_carro[0]->id;
                 if ($est) {
                     # code...
                 $modelflota->id_estacion = $est[0]->id_estacion;
                 }
                 $modelflota->kilometraje=$km;
                 // $modelflota->fecha = date('Y-m-j');
                 $modelflota->save(false);

               }else{
  
        
  
      
                $modelflota = new Flota();
         
                 $modelflota->id_vehiculo = $id_carro[0]->id;
                 if ($est) {
                     # code...
                 $modelflota->id_estacion = $est[0]->id_estacion;
                 }
                 $modelflota->kilometraje=$km;
            
                 $modelflota->save(false);
     
       
                 
               }
                
            }
    
 
        }
         

    
        fclose($file);

        return $this->render('wizard');

           
    }
//vehiculos en transito
    public function actionTransito(){
        $consulta = Vehiculo::find()
            ->where(['transito' => '1'])
            ->groupBy(['id_modelo']);
             
    
        $searchModel_mp = new WizardSearch();
        $dataProvider = $searchModel_mp->search(Yii::$app->request->queryParams);
        $dataProvider_mp = $searchModel_mp->search_wizard(Yii::$app->request->queryParams);
            
        return $this->render('transito', [
            'searchModel_mp' => $searchModel_mp,
            'dataProvider' => $dataProvider,
            'consulta' => $consulta,
            'dataProvider_mp' => $dataProvider_mp,
        ]);
    }
//guarda los vehiculos en transito
    public function actionGuardartransito(){
        set_time_limit(300);

        $inputFiles = 'files/FlotaOperativa.xlsx';



      try{
             $inputFileType = \PHPExcel_IOFactory::identify($inputFiles);
             $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
             $objPHPExcel = $objReader->load($inputFiles);
             
         } catch (Exception $ex) {             
             die('Error');
         }
                  
         $sheet = $objPHPExcel->getSheet(0);
         $highestRow = $sheet->getHighestRow();
         $highestColumn = $sheet->getHighestColumn();
   

         //$row is start 2 because first row assigned for heading.         
         for($row=2; $row<=$highestRow; ++$row)
         {                  
             
             $rowData = $sheet->rangeToArray('A'.$row.':'.$highestColumn.$row,NULL,TRUE,FALSE);
             $color = $rowData[0][7];
             $chasis = $rowData[0][8];
             $concesionario = $rowData[0][13];
             $placa = $rowData[0][6];
             $id_categoria = $rowData[0][15];
             $clase = $rowData[0][14];
             $fecha_compra = $rowData[0][16];
             $id_modelo = $rowData[0][9];
    
             $anho = $rowData[0][4];
             $fecha_compra = substr($fecha_compra, 6, 4).'-'.substr($fecha_compra, 0, 2).'-'.substr($fecha_compra, 3, 2);

              $id_modelo =  Modelo::find()->where([ 'nombre' => $id_modelo ])->all();
              

              $existe =  Vehiculo::find()->where([ 'chasis' => $chasis, 'transito'=>1 ])->all();
              $id_concesionario = Concesionario::find()->where([ 'nombre' => $concesionario ])->all();
 
             
                if ($existe) {
               $existe[0]->color = $color;
               $existe[0]->chasis = $chasis;
               $existe[0]->placa = $placa;
               $existe[0]->id_categoria = 3;
               $existe[0]->clase = $clase;
               $existe[0]->fecha_compra = $fecha_compra;
               $existe[0]->anho = $anho;
                if ($id_concesionario) {
                $existe[0]->id_concesionario = $id_concesionario[0]->id;
                }   
               $existe[0]->transito = 1;
                   if ($id_modelo!=null) {
                   $existe[0]->id_modelo = $id_modelo[0]->id_modelo;
               }
                  $existe[0]->update(false);
              }else{
                $modelwizard = new Vehiculo();

               $modelwizard->color = $color;
               $modelwizard->chasis = $chasis;
               $modelwizard->placa = $placa;
               if ($id_concesionario) {
               $modelwizard->id_concesionario =$id_concesionario[0]->id;
                }
               $modelwizard->id_categoria = 3;
               $modelwizard->clase = $clase;
               $modelwizard->fecha_compra = $fecha_compra;
               $modelwizard->anho = $anho;
               $modelwizard->transito = 1;
                   if ($id_modelo!=null) {
                   $modelwizard->id_modelo = $id_modelo[0]->id_modelo;
               }
                $modelwizard->save(false);

              }
              

        }
    }
//registra los nuevos vehiculos que el usuario manda a cargar
    public function actionRegistratransito(){
        set_time_limit(300);

        $model = new transito();


        if (Yii::$app->request->isPost) {
            $model->file = UploadedFile::getInstance($model, 'file');
            $model->upload();
        }else{
            return $this->render('registratransito',[
               'model'=>$model,
            ]);
        }

        $inputFiles = 'files/Flotaoperativa.xlsx';



        try{
           $inputFileType = \PHPExcel_IOFactory::identify($inputFiles);
           $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
           $objPHPExcel = $objReader->load($inputFiles);
           
        } catch (Exception $ex) {             
           die('Error');
        }
               
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
               
            
        //$row is start 2 because first row assigned for heading.         
        for($row=2; $row<=$highestRow; ++$row)
        {           
                   
            $rowData = $sheet->rangeToArray('A'.$row.':'.$highestColumn.$row,NULL,TRUE,FALSE);
            $color = $rowData[0][7];
            $chasis = $rowData[0][8];
            $concesionario = $rowData[0][13];
            $placa = $rowData[0][6];
            $id_categoria = $rowData[0][15];
            $clase = $rowData[0][14];
            $fecha_compra = $rowData[0][16];
            $id_modelo = $rowData[0][9];
                       
            $anho = $rowData[0][4];
            $fecha_compra = substr($fecha_compra, 6, 4).'-'.substr($fecha_compra, 0, 2).'-'.substr($fecha_compra, 3, 2);

            $id_modelo =  Modelo::find()->where([ 'nombre' => $id_modelo ])->all();
           

            $existe =  Vehiculo::find()->where([ 'chasis' => $chasis, 'transito'=>1 ])->all();
            $id_concesionario = Concesionario::find()->where([ 'nombre' => $concesionario ])->all();
                       
                       
            if ($existe) {

                $existe[0]->color = $color;
                $existe[0]->chasis = $chasis;
                $existe[0]->placa = $placa;
                $existe[0]->id_categoria = 3;
                $existe[0]->clase = $clase;
                $existe[0]->fecha_compra = $fecha_compra;
                $existe[0]->anho = $anho;
                        
                if ($id_concesionario) {
                    $existe[0]->id_concesionario = $id_concesionario[0]->id;
                }   
                        
                $existe[0]->transito = 1;
                
                if ($id_modelo!=null) {
                    $existe[0]->id_modelo = $id_modelo[0]->id_modelo;
                }
                     
                $existe[0]->update(false);
            } else{
                $modelwizard = new Vehiculo();

                $modelwizard->color = $color;
                $modelwizard->chasis = $chasis;
                $modelwizard->placa = $placa;
                if ($id_concesionario) {
                    $modelwizard->id_concesionario =$id_concesionario[0]->id;
                }
                $modelwizard->id_categoria = 3;
                $modelwizard->clase = $clase;
                $modelwizard->fecha_compra = $fecha_compra;
                $modelwizard->anho = $anho;
                $modelwizard->transito = 1;
                if ($id_modelo!=null) {
                    $modelwizard->id_modelo = $id_modelo[0]->id_modelo;
                }
                $modelwizard->save(false);

            }
        }
     
     
        $consulta = Vehiculo::find()
        ->where(['transito' => '1'])
        ->groupBy(['id_modelo']);
     
     
        $searchModel_mp = new WizardSearch();
        $dataProvider = $searchModel_mp->search(Yii::$app->request->queryParams);
        $dataProvider_mp = $searchModel_mp->search_wizard(Yii::$app->request->queryParams);

        return $this->render('transito', [
            'searchModel_mp' => $searchModel_mp,
            'dataProvider' => $dataProvider,
            'consulta' => $consulta,    
            'dataProvider_mp' => $dataProvider_mp,
        ]);
    }

    /**
     * Lectura de archivos tsd
     */
    public function actionTsd(){

        set_time_limit(300);

        $inputFiles = 'files/FullInventory.xlsx';



        try{
            $inputFileType = \PHPExcel_IOFactory::identify($inputFiles);
            $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFiles);
             
        } catch (Exception $ex) {             
            die('Error');
        }
                  
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
   

        //$row is start 2 because first row assigned for heading.         
        for($row=2; $row<=$highestRow; ++$row)
        {                  
             
            $rowData = $sheet->rangeToArray('A'.$row.':'.$highestColumn.$row,NULL,TRUE,FALSE);
            $color = $rowData[0][6];
            $chasis = $rowData[0][10];
            $placa = $rowData[0][7];
            $mva = $rowData[0][1];
            $clase = $rowData[0][2];
            $km = $rowData[0][9];
            $id_modelo = $rowData[0][4];
            $anho = $rowData[0][3];
    
            /*echo 'color: '.$color.'<br>';
            echo 'Chasis '.$chasis.'<br>';
            echo 'placa '.$placa.'<br>';
            echo 'mva '.$mva.'<br>';
            echo 'clase '.$clase.'<br>';
            echo 'km '.$km.'<br>';
            echo 'modelo '.$id_modelo.'<br>';
            echo 'anho '.$anho.'<br>';
            echo '<hr>';*/
            

        }
    }

    /**
     * Lectura de archivos tsd
     */
    public function actionPiezas(){

        set_time_limit(300);

        $inputFiles = 'files/piezas.xlsx';



        try{
            $inputFileType = \PHPExcel_IOFactory::identify($inputFiles);
            $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFiles);
             
        } catch (Exception $ex) {             
            die('Error');
        }
                  
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
   

        //$row is start 2 because first row assigned for heading.         
        for($row=2; $row<=$highestRow; ++$row)
        {                  
             
            $rowData = $sheet->rangeToArray('A'.$row.':'.$highestColumn.$row,NULL,TRUE,FALSE);
            $codigo = $rowData[0][0];
            $nombre = $rowData[0][1];
            $tamano = $rowData[0][2];
            $unidad = $rowData[0][3];
            $anho = $rowData[0][4];
            $modelo = $rowData[0][5];
            $marca = $rowData[0][6];
            $id_tipo_pieza = $rowData[0][7];
      
            echo 'codigo: '.$codigo.'<br>';
            echo 'nombre: '.$nombre.'<br>';
            echo 'tamaño '.$tamano.'<br>';
            echo 'unidad '.$unidad.'<br>';
            echo 'año '.$anho.'<br>';
            echo 'modelo '.$modelo.'<br>';
            echo 'marca '.$marca.'<br>';
            echo 'tipo pieza '.$id_tipo_pieza.'<br>';
            
            echo '<hr>';
            
            $piezas = new Pieza();
            $piezas->nombre = $nombre;
            $piezas->tamano = $tamano;
            $piezas->unidad = $unidad;
            $piezas->tipo_pieza_id = $id_tipo_pieza;
            $piezas->marca_insumo = $marca;
            $piezas->codigo = $codigo;
            $piezas->anho = $anho;
            $piezas->modelo_id_modelo = $modelo;

            $piezas->save(false);
        }
    }

    /**
     * Historico de piezas
     * @param integer $id
     * @return mixed
     */
    public function actionHistoricopiezas($id)
    {
        $model = $this->findModel($id);

        return $this->render('historico-piezas', [
            'model' => $model,
        ]);
    }



    /**
     * Visualizaciín del QR del vehículo
     * @param integer $chasis
     * @return mixed
     */
    public function actionQr($chasis)
    {
        return QrCode::png($chasis);
    }

    /**
     * Descargar documento Excel con las garantias del vehículo
     * @param integer $id
     * @return mixed
     */
    public function actionExcelgarantia($id)
    {
        $model = $this->findModel($id);

        $model_gv = GarantiaVehiculo::find()->where([ 'id_vehiculo' => $id ])->orderby('id')->all();

        try{

            // Create new PHPExcel object
            $objPHPExcel = new \PHPExcel();
            
            // Fill worksheet from values in array
            $objWorkSheet = $objPHPExcel->createSheet(0);

            $array_fondo = array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'dde5f4'));
            $array_letra = array( 'bold'  => true,'color' => array('rgb' => '000000'));

            $objWorkSheet
            ->setCellValue('A2', 'Marca: ')
            ->setCellValue('B2', Marca::findOne($model->idModelo->id_marca)->nombre)
            ->setCellValue('A3', 'Modelo: ')
            ->setCellValue('B3', $model->idModelo->nombre)
            ->setCellValue('A4', 'Color: ')
            ->setCellValue('B4', $model->color)
            ->setCellValue('A5', 'Transmisión: ')
            ->setCellValue('B5', $model->transmision)
            ->setCellValue('A6', 'Chasis: ')
            ->setCellValue('B6', $model->chasis)
            ->setCellValue('A7', 'Placa: ')
            ->setCellValue('B7', $model->placa)
            ->setCellValue('A8', 'Ficaha o MVA: ')
            ->setCellValue('B8', $model->mva );

            $objWorkSheet->getStyle('A2')->applyFromArray( array('font' => $array_letra ));
            $objWorkSheet->getStyle('A3')->applyFromArray( array('font' => $array_letra ));
            $objWorkSheet->getStyle('A4')->applyFromArray( array('font' => $array_letra ));
            $objWorkSheet->getStyle('A5')->applyFromArray( array('font' => $array_letra ));
            $objWorkSheet->getStyle('A6')->applyFromArray( array('font' => $array_letra ));
            $objWorkSheet->getStyle('A7')->applyFromArray( array('font' => $array_letra ));
            $objWorkSheet->getStyle('A8')->applyFromArray( array('font' => $array_letra ));

            $objWorkSheet
            ->setCellValue('A10', 'Descripción')
            ->setCellValue('B10', 'Tiempo')
            ->setCellValue('C10', 'Kilometraje')
            ->setCellValue('D10', 'Anulado por')
            ->setCellValue('E10', 'Fecha Anulado');

            $objWorkSheet->getStyle('A10')->applyFromArray( array( 'fill' => $array_fondo,'font' => $array_letra ));
            $objWorkSheet->getStyle('B10')->applyFromArray( array( 'fill' => $array_fondo,'font' => $array_letra ));
            $objWorkSheet->getStyle('C10')->applyFromArray( array( 'fill' => $array_fondo,'font' => $array_letra ));
            $objWorkSheet->getStyle('D10')->applyFromArray( array( 'fill' => $array_fondo,'font' => $array_letra ));
            $objWorkSheet->getStyle('E10')->applyFromArray( array( 'fill' => $array_fondo,'font' => $array_letra ));

            for($j = 11; $j <= count($model_gv)+10;$j++){

                $objWorkSheet->setCellValue('A'.$j, $model_gv[$j-11]->idGarantia->nombre);
                $objWorkSheet->setCellValue('B'.$j, ($model_gv[$j-11]->tiempo)?$model_gv[$j-11]->tiempo:'-');
                $objWorkSheet->setCellValue('C'.$j, ($model_gv[$j-11]->km)?$model_gv[$j-11]->km:'-');
                $objWorkSheet->setCellValue('D'.$j, ($model_gv[$j-11]->anular == 1)?(($model_gv[$j-11]->motivo == 1 )?'Accidente':'Cambio de piezas'):'-');
                $objWorkSheet->setCellValue('E'.$j, ($model_gv[$j-11]->fecha_anula)?$model_gv[$j-11]->fecha_anula:'-');

            }

            $objWorkSheet->setTitle('GARANTIAS');


            // Save Excel 2007 file
            $file = 'files/'.'GARANTIAS_VEHICULO_#'.$model->placa.'.xlsx';
            
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save($file);

            header('Content-Description: File Transfer');
            header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            header("Content-Disposition: attachment; filename=\"".basename($file)."\"");
            header("Content-Transfer-Encoding: binary");
            header("Expires: 0");
            header("Pragma: public");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header('Content-Length: ' . filesize($file)); //Remove

            ob_clean();
            flush();

            readfile($file);
            exit();
        }
        catch(Exception $e){
            die('Error');
        }    
    }
    public function actionExportarpreventivo($id){

        $vehiculo = $this->findModel($id);

        $proyecciones = ProyeccionMantenimiento::find()->where([ 'id_vehiculo' => $id ])->orderby('id DESC')->limit(6)->all();
        $searchModel_mp = new MantenimientoPreventivoSearch();
        $dataProvider_mp = $searchModel_mp->search_realizados(Yii::$app->request->queryParams);

        $model = $dataProvider_mp->getModels();
        try{

            // Create new PHPExcel object
            $objPHPExcel = new \PHPExcel();
            
            // Fill worksheet from values in array
            $objWorkSheet = $objPHPExcel->createSheet(0);

            $array_fondo = array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'dde5f4'));
            $array_letra = array( 'bold'  => true,'color' => array('rgb' => '000000'));

            $objWorkSheet
            ->setCellValue('A2', 'Marca: ')
            ->setCellValue('B2', Marca::findOne($vehiculo->idModelo->id_marca)->nombre)
            ->setCellValue('A3', 'Modelo: ')
            ->setCellValue('B3', $vehiculo->idModelo->nombre)
            ->setCellValue('A4', 'Color: ')
            ->setCellValue('B4', $vehiculo->color)
            ->setCellValue('A5', 'Transmisión: ')
            ->setCellValue('B5', $vehiculo->transmision)
            ->setCellValue('A6', 'Chasis: ')
            ->setCellValue('B6', $vehiculo->chasis)
            ->setCellValue('A7', 'Placa: ')
            ->setCellValue('B7', $vehiculo->placa)
            ->setCellValue('A8', 'Ficaha o MVA: ')
            ->setCellValue('B8', $vehiculo->mva );

            $objWorkSheet->getStyle('A2')->applyFromArray( array('font' => $array_letra ));
            $objWorkSheet->getStyle('A3')->applyFromArray( array('font' => $array_letra ));
            $objWorkSheet->getStyle('A4')->applyFromArray( array('font' => $array_letra ));
            $objWorkSheet->getStyle('A5')->applyFromArray( array('font' => $array_letra ));
            $objWorkSheet->getStyle('A6')->applyFromArray( array('font' => $array_letra ));
            $objWorkSheet->getStyle('A7')->applyFromArray( array('font' => $array_letra ));
            $objWorkSheet->getStyle('A8')->applyFromArray( array('font' => $array_letra ));

            $array_fondo = array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'dde5f4'));
            $array_letra = array( 'bold'  => true,'color' => array('rgb' => '000000'));

            /// ------------------------------------------------------------------------------
            $objWorkSheet
            ->setCellValue('A10', 'Fecha Mtto.                              ')
            ->setCellValue('B10', 'Km de Mtto.                              ')
            ->setCellValue('C10', 'Tipo                                     ')
            ->setCellValue('D10', 'Cliete                                   ')
            ->setCellValue('E10', 'Estación                                 ')
            ->setCellValue('F10', 'Piezas                                 ');

            $objWorkSheet->getStyle('A10')->applyFromArray( array( 'fill' => $array_fondo,'font' => $array_letra ));
            $objWorkSheet->getStyle('B10')->applyFromArray( array( 'fill' => $array_fondo,'font' => $array_letra ));
            $objWorkSheet->getStyle('C10')->applyFromArray( array( 'fill' => $array_fondo,'font' => $array_letra ));
            $objWorkSheet->getStyle('D10')->applyFromArray( array( 'fill' => $array_fondo,'font' => $array_letra ));
            $objWorkSheet->getStyle('E10')->applyFromArray( array( 'fill' => $array_fondo,'font' => $array_letra ));
            $objWorkSheet->getStyle('F10')->applyFromArray( array( 'fill' => $array_fondo,'font' => $array_letra ));
            
            $array_letra = array( 'bold'  => true,'color' => array('rgb' => 'ffffff'));
            for($j = 11; $j <= count($model)+10;$j++){

                $model_consumo = Consumo::find()->where([ 'id_mantenimiento_preventivo' => $model[$j-11]->id ])->all();

                if ( $model[$j-11]->fecha_realizacion ) {
                        
                    $fechas = date('d/m/Y', strtotime($model[$j-11]->fecha_realizacion));
                } else{
                    $fechas = '-';
                }
                
                $objWorkSheet->setCellValue('A'.$j, $fechas); //fecha
                

                if ($model[$j-11]->km) {
                    
                    $kilometros = number_format(floatval($model[$j-11]->km), 0, ',', '.') . ' km';
                } else{
                
                    $kilometros = '-';
                }

                
                $objWorkSheet->setCellValue('B'.$j, $kilometros); //kilometraje


                if ($model[$j-11]->idTipoMantenimiento->nombre) {                    
                    $tipos = ucfirst(strtolower($model[$j-11]->idTipoMantenimiento->nombre));
                } else{
                    $tipos = '-';
                }

                $objWorkSheet->setCellValue('C'.$j, $tipos); //tipo
              

                if ( $model[$j-11]->id_estacion ) {
                    $clientes = $model[$j-11]->idEstacion->idEmpresa->cliente->nombre_comercial;
                } else{
                    $clientes ='-';
                }
                
                $objWorkSheet->setCellValue('D'.$j, $clientes); //cliente


                if ( $model[$j-11]->id_estacion ) {
                    $estaciones =   $model[$j-11]->idEstacion->idEmpresa->nombre .' - '. $model[$j-11]->idEstacion->nombre;
                } else{
                    $estaciones =  '-';
                }

                $objWorkSheet->setCellValue('E'.$j, $estaciones); //Estaciones

                $consumo_piezas = '';
                foreach ($model_consumo as $key => $consumo) {
                    if ( isset($consumo[$key + 1]) ) {
                        $consumo_piezas .= $consumo->idActividadPieza->idPieza->nombre . ' / ';
                    } else{
                        $consumo_piezas .= $consumo->idActividadPieza->idPieza->nombre;
                    }
                }

                $objWorkSheet->setCellValue('F'.$j, $consumo_piezas); //Estaciones



            }
            // Save Excel 2007 file
            $file = 'files/'.'MANTENIMIENTO_PREVENTIVO_'.$vehiculo->placa.'.xlsx';
            
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save($file);

            header('Content-Description: File Transfer');
            header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            header("Content-Disposition: attachment; filename=\"".basename($file)."\"");
            header("Content-Transfer-Encoding: binary");
            header("Expires: 0");
            header("Pragma: public");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header('Content-Length: ' . filesize($file)); //Remove

            ob_clean();
            flush();

            readfile($file);
            exit();
        }
        catch(Exception $e){
            die('Error');
        }    
        
    }
    public function actionExportar($id){
        
        
        $searchModel_servicio = new ServicioSearch([ 'id_vehiculo' => $id ]);
        $dataProvider_servicio = $searchModel_servicio->search(Yii::$app->request->queryParams);

        $model = $dataProvider_servicio->getModels();


        try{

            // Create new PHPExcel object
            $objPHPExcel = new \PHPExcel();
            
            // Fill worksheet from values in array
            $objWorkSheet = $objPHPExcel->createSheet(0);

            $array_fondo = array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'dde5f4'));
            $array_letra = array( 'bold'  => true,'color' => array('rgb' => '000000'));

            /// ------------------------------------------------------------------------------
            $objWorkSheet
            ->setCellValue('A4', 'Numero de Solicitud                     ')
            ->setCellValue('B4', 'Tipo de solicitud                       ')
            ->setCellValue('C4', 'Cliente                                 ')
            ->setCellValue('D4', 'Estación                                ')
            ->setCellValue('E4', 'Estatus                                 ');

            $objWorkSheet->getStyle('A4')->applyFromArray( array( 'fill' => $array_fondo,'font' => $array_letra ));
            $objWorkSheet->getStyle('B4')->applyFromArray( array( 'fill' => $array_fondo,'font' => $array_letra ));
            $objWorkSheet->getStyle('C4')->applyFromArray( array( 'fill' => $array_fondo,'font' => $array_letra ));
            $objWorkSheet->getStyle('D4')->applyFromArray( array( 'fill' => $array_fondo,'font' => $array_letra ));
            $objWorkSheet->getStyle('E4')->applyFromArray( array( 'fill' => $array_fondo,'font' => $array_letra ));
            
            $array_letra = array( 'bold'  => true,'color' => array('rgb' => 'ffffff'));
            for($j = 5; $j <= count($model)+4;$j++){

                $objWorkSheet->setCellValue('A'.$j, $model[$j-5]->id); //MVA
                if ($model[$j-5]->servicio ) {
                    $servicios = explode("*", $model[$j-5]->servicio);

                    foreach ($servicios as $key => $servicio) {
                        switch ($servicio) {
                            case '0':
                            $vector_servicios[] = 'Mecánica Ligera';
                            break;

                            case '1':
                            $vector_servicios[] = 'Mecánica Especializada';
                            break;

                            case '2':
                            $vector_servicios[] = 'Aire Acondicionado';
                            break;

                            case '3':
                            $vector_servicios[] = 'Desabolladura y pintura';
                            break;

                            case '4':
                            $vector_servicios[] = 'Accesorios';
                            break;
                            
                            default:
                                            # code...
                            break;
                        }
                        
                    }

                    $respuesta_servicios = implode('\ ', $vector_servicios);
                    
                } 
                $objWorkSheet->setCellValue('B'.$j, $respuesta_servicios); //tipo de solicitud
                if ( $model[$j-5]->id_estacion_actual ) {
                   $cliente = Empresa::findOne($model[$j-5]->idEstacionActual->id_empresa)->nombre;
               } else{
                $cliente = '-';
            }
                $objWorkSheet->setCellValue('C'.$j, $cliente); //cliente

                $objWorkSheet->setCellValue('D'.$j, $model[$j-5]->idEstacionActual->nombre); //estacion



                $query = new query();
                $query->select('es.nombre, es.color')
                ->from('pipeline_servicio p')
                ->join('INNER JOIN','estado_servicio es','es.id = p.id_estado_servicio')
                ->where('p.id_servicio = '. $model[$j-5]->id )
                ->orderBy('p.id desc');

                $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                $estado = Yii::$app->db->createCommand($sql)->queryOne();

                
                
                
                $array_fondo = array('type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => str_replace("#","",$estado['color'],$i)));
                $array_letra = array( 'bold'  => true,'color' => array('rgb' => '000000'));
                $objWorkSheet->setCellValue('E'.$j, $estado['nombre']); 
                $objWorkSheet->getStyle('E'.$j)->applyFromArray( array( 'fill' => $array_fondo,'font' => $array_letra ));



            }
            // Save Excel 2007 file
            $file = 'files/'.'GARANTIAS_VEHICULO_#.xlsx';
            
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save($file);

            header('Content-Description: File Transfer');
            header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            header("Content-Disposition: attachment; filename=\"".basename($file)."\"");
            header("Content-Transfer-Encoding: binary");
            header("Expires: 0");
            header("Pragma: public");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header('Content-Length: ' . filesize($file)); //Remove

            ob_clean();
            flush();

            readfile($file);
            exit();
        }
        catch(Exception $e){
            die('Error');
        }    
    }

    public function actionResumen()
    {
        $searchResumen = new ResumenSearch();
        $dataProvider = $searchResumen->search(Yii::$app->request->queryParams);
        $dataProviderResumen = $searchResumen->resumen(Yii::$app->request->queryParams);
        return $this->render('resumen', [
            'searchResumen' => $searchResumen,
            'dataProvider' => $dataProvider,
            'dataProviderResumen' => $dataProviderResumen,
           
        ]);
    }

    private function downloadFile($dir, $file, $extensions=[])
    {
        //Si el directorio existe
        if (is_dir($dir))
        {
            //Ruta absoluta del archivo
            $path = $dir.$file;
   
            //Si el archivo existe
            if (is_file($path))
            {
                //Obtener información del archivo
                $file_info = pathinfo($path);
                //Obtener la extensión del archivo
                $extension = $file_info["extension"];
    
                if (is_array($extensions))
                {
                    //Si el argumento $extensions es un array
                    //Comprobar las extensiones permitidas
                    foreach($extensions as $e)
                    {
                        //Si la extension es correcta
                        if ($e === $extension)
                        {
                           //Procedemos a descargar el archivo
                           // Definir headers
                           $size = filesize($path);
                           header("Content-Type: application/force-download");
                           header("Content-Disposition: attachment; filename=$file");
                           header("Content-Transfer-Encoding: binary");
                           header("Content-Length: " . $size);
                           // Descargar archivo
                           readfile($path);
                           //Correcto
                           return true;
                        }
                    }
                }
    
            }
        }
        //Ha ocurrido un error al descargar el archivo
        return false;
    }
 
    public function actionDownload()
    {
        if (Yii::$app->request->get("file"))
        {
            //Si el archivo no se ha podido descargar
            //downloadFile($dir, $file, $extensions=[])
            if (!$this->downloadFile("files/", Html::encode($_GET["file"]), ["xlsx"]))
            {
                //Mensaje flash para mostrar el error
                Yii::$app->session->setFlash("errordownload");
            }
        }
  
        return $this->render("errordownload");
    }

//Estos son los action para las tareas programadas

    public function actionPlaca()
    {


        $vehiculos =  Vehiculo::find()->all();
        $fecha = date('Y-m-j');
        $nuevafecha = strtotime ( '+30 day' , strtotime ( $fecha ) ) ;
        $fecha_placas = date ( 'Y-m-j' , $nuevafecha );

        $placas_vencidas = strtotime ( '-1 day' , strtotime ( $fecha ) ) ;
        $placas_vencidas = date ( 'Y-m-j' , $placas_vencidas );
        foreach ($vehiculos as $key => $value) {
            if($value['fecha_renovacion_placa']){

                if ($fecha_placas == $value['fecha_renovacion_placa'] ) {

                    $existe = Notificaciones::find()->where(['id_vehiculo'=>$value->id,'tipo'=>0, 'estado'=>1,])->all();

                    if (!$existe) {
                        
                    $notif = new Notificaciones();
                    $notif->tiempo = 4;
                    $notif->tipo = 0;
                    $notif->estado = 0;
                    $notif->id_vehiculo = $value->id;
                    $notif->save();
                    // echo "entro";
                    }
                
                }
                    if ($placas_vencidas == $value['fecha_renovacion_placa'] ) {

                    $existe = Notificaciones::find()->where(['id_vehiculo'=>$value->id,'tipo'=>0, 'estado'=>1,])->all();

                    if (!$existe) {
                        
                    $notif = new Notificaciones();
                    $notif->tiempo = 0;
                    $notif->tipo = 0;
                    $notif->estado = 1;
                    $notif->id_vehiculo = $value->id;
                    $notif->save();
                    echo "entro 1";
                    }
                
                }
                if ($fecha > $value['fecha_renovacion_placa'] ) {

                    $existe = Notificaciones::find()->where(['id_vehiculo'=>$value->id,'tipo'=>0, 'estado'=>1,])->all();

                    if (!$existe) {
                        
                    $notif = new Notificaciones();
                    $notif->tiempo = 0;
                    $notif->tipo = 0;
                    $notif->estado = 1;
                    $notif->id_vehiculo = $value->id;
                    $notif->save();
                    echo "entro 2";
                    }
                
                }
         
            }
           
        }

    }

       public function actionSeguro()
    {


        $vehiculos =  Vehiculo::find()->all();
        $fecha = date('Y-m-j');
        $nuevafecha = strtotime ( '+30 day' , strtotime ( $fecha ) ) ;
        $fecha_seguros = date ( 'Y-m-j' , $nuevafecha );
        $seguro_vencido = strtotime ( '-1 day' , strtotime ( $fecha ) ) ;
        $seguro_vencido = date ( 'Y-m-j' , $seguro_vencido );
 
        foreach ($vehiculos as $key => $value) {
            if($value['fecha_venc_seguro']){
                if ($fecha_seguros == $value['fecha_venc_seguro'] ) {


                    $existe = Notificaciones::find()->where(['id_vehiculo'=>$value->id,'tipo'=>1, 'estado'=>1])->all();

                    if (!$existe) {
                        
                    $notif = new Notificaciones();
                    $notif->tiempo = 4;
                    $notif->tipo = 1;
                    $notif->estado = 0;
                    $notif->id_vehiculo = $value->id;
                    $notif->save(false);
                    echo "entro";
                    }
                
                }
                 if ($seguro_vencido == $value['fecha_venc_seguro'] ) {

                    $existe = Notificaciones::find()->where(['id_vehiculo'=>$value->id,'tipo'=>1, 'estado'=>1,])->all();
                    
                    if (!$existe) {
                        
                    $notif = new Notificaciones();
                    $notif->tiempo = 0;
                    $notif->tipo = 1;
                    $notif->estado = 1;
                    $notif->id_vehiculo = $value->id;
                    $notif->save(false);
                    echo "entro 1";
                    }
                
                }
                if ($fecha > $value['fecha_venc_seguro'] ) {

                    $existe = Notificaciones::find()->where(['id_vehiculo'=>$value->id,'tipo'=>1, 'estado'=>1,])->all();

                    if (!$existe) {

                    $notif = new Notificaciones();
                    $notif->tiempo = 0;
                    $notif->tipo = 1;
                    $notif->estado = 1;
                    $notif->id_vehiculo = $value->id;
                    $notif->save(false);
                    echo "entro 2";
                    }
                
                }
         
            }
           
        }

    }

           public function actionMarbetes()
    {


        $vehiculos =  Vehiculo::find()->all();
        $fecha = date('Y-m-j');
        $nuevafecha = strtotime ( '+60 day' , strtotime ( $fecha ) ) ;
        $fecha_marbetes = date ( 'Y-m-j' , $nuevafecha );

        $marbetes_vencidos = strtotime ( '-1 day' , strtotime ( $fecha ) ) ;
        $marbetes_vencidos = date ( 'Y-m-j' , $marbetes_vencidos );
        // print_r($marbetes_vencidos);
        foreach ($vehiculos as $key => $value) {
            if($value['fecha_venc_marbete']){

                if ($fecha_marbetes == $value['fecha_venc_marbete'] ) {

                    $existe = Notificaciones::find()->where(['id_vehiculo'=>$value->id,'tipo'=>2, 'estado'=>1])->all();

                    if (!$existe) {
                        
                    $notif = new Notificaciones();
                    $notif->tiempo = 5;
                    $notif->tipo = 2;
                    $notif->estado = 0;
                    $notif->id_vehiculo = $value->id;
                    $notif->save(false);
                    echo "entro";
                    }
                
                }
                if ($marbetes_vencidos == $value['fecha_venc_marbete'] ) {

                    $existe = Notificaciones::find()->where(['id_vehiculo'=>$value->id,'tipo'=>2, 'estado'=>1,])->all();
                    
                    if (!$existe) {
                        
                    $notif = new Notificaciones();
                    $notif->tiempo = 0;
                    $notif->tipo = 2;
                    $notif->estado = 1;
                    $notif->id_vehiculo = $value->id;
                    $notif->save(false);
                    echo "entro 1";
                    }
                
                }
                if ($fecha > $value['fecha_venc_marbete'] ) {

                    $existe = Notificaciones::find()->where(['id_vehiculo'=>$value->id,'tipo'=>2, 'estado'=>1,])->all();

                    if (!$existe) {

                    $notif = new Notificaciones();
                    $notif->tiempo = 0;
                    $notif->tipo = 2;
                    $notif->estado = 1;
                    $notif->id_vehiculo = $value->id;
                    $notif->save(false);
                    echo "entro 2";
                    }
                
                }
         
            }
           
        }

    }
           public function actionTaller1()
    {


        $talleres =  Taller::find()->all();
        $fecha = date('Y-m-j');
        $nuevafecha = strtotime ( '+10 day' , strtotime ( $fecha ) ) ;
        $fecha_taller1 = date ( 'Y-m-j' , $nuevafecha );

        $rm_vencidos = strtotime ( '-1 day' , strtotime ( $fecha ) ) ;
        $rm_vencidos = date ( 'Y-m-j' , $rm_vencidos );

        foreach ($talleres as $key => $value) {
            if($value['vencimiento_rm']){

                if ($fecha_taller1 == $value['vencimiento_rm'] ) {

                    $existe = Notificaciones::find()->where(['id_taller'=>$value->id_taller,'tipo'=>3, 'estado'=>0])->all();
                    

                    if (!$existe) {
                        
                    $notif = new Notificaciones();
                    $notif->tiempo = 3;
                    $notif->tipo = 3;
                    $notif->estado = 0;
                    $notif->id_taller = $value->id_taller;
                    $notif->save(false);
                    echo "entro";
                    }
                
                }

                    if ($rm_vencidos == $value['vencimiento_rm'] ) {

                    $existe = Notificaciones::find()->where(['id_taller'=>$value->id_taller,'tipo'=>3, 'estado'=>0,])->all();
                    
                    if (!$existe) {
                        
                    $notif = new Notificaciones();
                    $notif->tiempo = 0;
                    $notif->tipo = 3;
                    $notif->estado = 1;
                    $notif->id_vehiculo = $value->id;
                    $notif->save(false);
                    echo "entro 1";
                    }
                
                }
                if ($fecha > $value['vencimiento_rm'] ) {

                    $existe = Notificaciones::find()->where(['id_taller'=>$value->id_taller,'tipo'=>3, 'estado'=>1,])->all();

                    if (!$existe) {

                    $notif = new Notificaciones();
                    $notif->tiempo = 0;
                    $notif->tipo = 3;
                    $notif->estado = 1;
                    $notif->id_vehiculo = $value->id;
                    $notif->save(false);
                    echo "entro 2";
                    }
                
                }
         
            }
           
        }

    }

               public function actionTaller2()
    {


        $talleres =  Taller::find()->all();
        $fecha = date('Y-m-j');
        $nuevafecha = strtotime ( '+10 day' , strtotime ( $fecha ) ) ;
        $fecha_taller2 = date ( 'Y-m-j' , $nuevafecha );

        $acuerdos_vencidos = strtotime ( '-1 day' , strtotime ( $fecha ) ) ;
        $acuerdos_vencidos = date ( 'Y-m-j' , $acuerdos_vencidos );
        foreach ($talleres as $key => $value) {
            if($value['vencimiento_acuerdo']){

                if ($fecha_taller2 == $value['vencimiento_acuerdo'] ) {

                    $existe = Notificaciones::find()->where(['id_taller'=>$value->id_taller,'tipo'=>4, 'estado'=>1])->all();

                    if (!$existe) {
                        
                    $notif = new Notificaciones();
                    $notif->tiempo = 3;
                    $notif->tipo = 4;
                    $notif->estado = 0;
                    $notif->id_taller = $value->id_taller;
                    $notif->save();
                    echo "entro";
                    }
                
                }


                    if ($acuerdos_vencidos == $value['vencimiento_acuerdo'] ) {

                    $existe = Notificaciones::find()->where(['id_taller'=>$value->id_taller,'tipo'=>4, 'estado'=>1,])->all();
                    
                    if (!$existe) {
                        
                    $notif = new Notificaciones();
                    $notif->tiempo = 0;
                    $notif->tipo = 4;
                    $notif->estado = 1;
                    $notif->id_vehiculo = $value->id;
                    $notif->save(false);
                    echo "entro 1";
                    }
                
                }
                if ($fecha > $value['vencimiento_acuerdo'] ) {

                    $existe = Notificaciones::find()->where(['id_taller'=>$value->id_taller,'tipo'=>4, 'estado'=>1,])->all();

                    if (!$existe) {

                    $notif = new Notificaciones();
                    $notif->tiempo = 0;
                    $notif->tipo = 4;
                    $notif->estado = 1;
                    $notif->id_vehiculo = $value->id;
                    $notif->save(false);
                    echo "entro 2";
                    }
                
                }
         
            }
           
        }

    }

    /**
     * Cargar Vehiculos a la flota.
     * @author Zulma Hernández
     * @return mixed
     */
    public function actionCargarvehiculos(){

        set_time_limit(3000);

        $upload_vehiculos = new UploadFileVehiculo();

        if ( $upload_vehiculos->load(Yii::$app->request->post()) ) {
            $upload_vehiculos->file = UploadedFile::getInstance($upload_vehiculos, 'file');

            if ($upload_vehiculos->upload()) {
                $inputFiles = 'files/vehiculos/file-vehiculo-registro_vehiculos.xlsx';

                try {
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFiles);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFiles);
                } catch (Exception $e) {
                    \Yii::$app->getSession()->setFlash('error', '¡Error al cargar el archivo!');
                    return;
                }

                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();

                for ( $row = 2; $row <= $highestRow; $row++ ) { 
                    
                    $rowData = $sheet->rangeToArray('A'.$row.':'.$highestColumn.$row,NULL,TRUE,FALSE);

                    //Obtengo el chasis del vehiculo
                    $chasis = trim($rowData[0][2]);
                    
                    if ($chasis  != '') {
                        //Busco el vehiculo con ese chasis
                        $vehiculo = Vehiculo::find()->where(['chasis' => $chasis])->one();

                        if ( !empty($vehiculo) ) {
                            $vehiculo->km = $rowData[0][11];
                        } else{

                            $vehiculo = new Vehiculo();

                            $vehiculo->color = $rowData[0][0];
                            $vehiculo->transmision = $rowData[0][1];
                            $vehiculo->chasis = $chasis;
                            $vehiculo->placa = $rowData[0][3];
                            $vehiculo->mva = $rowData[0][4];
                            $vehiculo->id_categoria = $rowData[0][5];
                            $vehiculo->clase = $rowData[0][6];
                            $vehiculo->combustible = $rowData[0][7];
                            $f = $rowData[0][8];
                            if ( $f != '' ) {
                                $dateTime = new \DateTime("1899-12-30 + $f days");
                                $vehiculo->fecha_compra = $dateTime->format('Y-m-d');
                            }
                            $f = $rowData[0][9];
                            if ( $f != '' ) {
                                $dateTime = new \DateTime("1899-12-30 + $f days");
                                $vehiculo->fecha_inicial = $dateTime->format('Y-m-d');
                            }
                            $vehiculo->tiempo_vida = $rowData[0][10];
                            $vehiculo->km = $rowData[0][11];
                            $f = $rowData[0][12];
                            if ( $f != '' ) {
                                $dateTime = new \DateTime("1899-12-30 + $f days");
                                $vehiculo->fecha_renovacion_placa = $dateTime->format('Y-m-d');
                            }
                            $f = $rowData[0][13];
                            if ( $f != '' ) {
                                $dateTime = new \DateTime("1899-12-30 + $f days");
                                $vehiculo->fecha_emision_placa = $dateTime->format('Y-m-d');
                            }
                            $vehiculo->wizard = $rowData[0][14];
                            $vehiculo->exactus = $rowData[0][15];
                            $vehiculo->id_modelo = $rowData[0][16];
                            $vehiculo->id_concesionario = $rowData[0][17];
                            $vehiculo->transito = 0;
                            $vehiculo->anho = $rowData[0][18];
                            $vehiculo->id_propietario = $rowData[0][19];
                            $f = $rowData[0][20];
                            if ( $f != '' ) {
                                $dateTime = new \DateTime("1899-12-30 + $f days");
                                $vehiculo->fecha_emision_seguro = $dateTime->format('Y-m-d');
                            }
                            $f = $rowData[0][21];
                            if ( $f != '' ) {
                                $dateTime = new \DateTime("1899-12-30 + $f days");
                                $vehiculo->fecha_venc_seguro = $dateTime->format('Y-m-d');
                            }
                            $f = $rowData[0][22];
                            if ( $f != '' ) {
                                $dateTime = new \DateTime("1899-12-30 + $f days");
                                $vehiculo->fecha_emision_marbete = $dateTime->format('Y-m-d');
                            }
                            $f = $rowData[0][23];
                            if ( $f != '' ) {
                                $dateTime = new \DateTime("1899-12-30 + $f days");
                                $vehiculo->fecha_venc_marbete = $dateTime->format('Y-m-d');
                            }
                        }

                        if ( $vehiculo->save(false) ) {
                            //Se tiene la ubicacion del vehiculo
                            if ( $rowData[0][24] != '' ) {
                                $model_flota = new Flota();
                                $model_flota->fecha_inicio = date('Y-m-d');
                                $model_flota->id_estacion = trim($rowData[0][24]);
                                $model_flota->id_vehiculo = $vehiculo->id;
                                $model_flota->kilometraje = $vehiculo->km;
                                $model_flota->save();
                                unset($model_flota);
                            }

                            //Se conoce el estatus del vehiculo
                            if ( $rowData[0][25] != '' ) {
                                //Se actualiza el estado actual del vehiculo
                                $model_estado_vehiculo = new EstadoVehiculo();
                                $model_estado_vehiculo->isNewRecord = true;
                                $model_estado_vehiculo->id_estado = Estado::findOne(['id' => trim($rowData[0][25]),])->id;
                                $model_estado_vehiculo->id_vehiculo = $vehiculo->id;
                                $model_estado_vehiculo->save();
                                unset($model_estado_vehiculo);
                            }
                        }
                    }
                    
                }
                \Yii::$app->getSession()->setFlash('success', '¡Vehículos cargados con éxito!');
                return $this->redirect(['index']);
            } else{
                \Yii::$app->getSession()->setFlash('error', '¡Error al cargar el archivo!');
                return;
            }

        } else {
            return $this->render('cargar-vehiculos', [
                'upload_vehiculos' => $upload_vehiculos,
            ]);
        }
        return $this->render('cargar-vehiculos');
    }

}
