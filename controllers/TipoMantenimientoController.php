<?php

namespace app\controllers;

use Yii;
use app\models\TipoMantenimiento;
use app\models\TipoMantenimientoSearch;
use app\models\ActividadMnto;
use app\models\Pieza;
use app\models\ActividadPieza;
use app\models\Model;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * TipoMantenimientoController implements the CRUD actions for TipoMantenimiento model.
 */
class TipoMantenimientoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TipoMantenimiento models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TipoMantenimientoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TipoMantenimiento model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TipoMantenimiento model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TipoMantenimiento();
        $modelpiezas = new Pieza();
        $modelact = [new ActividadMnto()];
        $modelpiezas = [[new ActividadPieza()]];
        if ($model->load(Yii::$app->request->post())) {
            $valid = 0;
            $model->nombre = $model->nombre;
            $model->estado = 1;
            if($model->save()){
                if (isset($_POST['ActividadMnto'][0]['nombre'])) {
                    foreach ($_POST['ActividadMnto'] as $indexHouse => $rooms) {
                        $nueva_activ = new ActividadMnto();
                        $nueva_activ->nombre = $rooms['nombre'];
                        $nueva_activ->tipo_actividad = $rooms['tipo_actividad'];
                        $nueva_activ->id_tipo_mnto = $model->id_tipo_mnto;
                        if($nueva_activ->save()){
                            foreach ($_POST['ActividadPieza'][$indexHouse] as $key => $pieza) {

                                $nueva_pieza = new ActividadPieza();
                                $nueva_pieza->id_actividad_mnto = $nueva_activ->id_actividad_mnto;
                                $nueva_pieza->id_pieza = $pieza['id_pieza'];
                                if(!$nueva_pieza->save()){
                                  print_r($nueva_pieza->getErrors()); die();
                                    $valid = 1;
                                }
                            }
                        }
                    }
                }
            }

            if($valid == 0){
                \Yii::$app->getSession()->setFlash('success', 'El Registro del Mantenimiento ha sido registrado con éxito!');
                return $this->redirect(['index']);
            }else{
                \Yii::$app->getSession()->setFlash('success', 'Error al registrar el Nuevo Tipo de Mantenimiento');
            }
        }
        return $this->render('create', [
            'modelact' => (empty($modelact)) ? [new TipoMantenimiento] : $modelact,
            'model' => $model,
            'modelpiezas' => (empty($modelpiezas)) ? [[new PaymentLoads]] : $modelpiezas,
            
        ]);
    }

    /**
     * Updates an existing TipoMantenimiento model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
   public function actionUpdate($id)
    {
        
        $model = $this->findModel($id);
        // $modelact = ActividadMnto::find()->where(['id_tipo_mnto'=>$model->id_tipo_mnto])->All(); 
        $modelact = $model->actividadMantenimientos;

       $modelpiezas[][]=0;
         if (!empty($modelact)) {
            foreach ($modelact as $indexHouse => $modelHouse) {
                $rooms = $modelHouse->actividadPiezas;
                $modelpiezas[$indexHouse] = $rooms; 

            }
        }
 
        if (!$modelpiezas[0]) {
        
 
 
        $modelact = [new ActividadMnto()];
        $modelpiezas = [[new ActividadPieza()]];

        }
 

  
     $nombre = $model->nombre;
     $model2 = new TipoMantenimiento();


        if ($model->load(Yii::$app->request->post())) {
            $model2->nombre = $model->nombre;
            $model2->estado = 1;
            $valid = 0;
            $model->nombre = $nombre;
            $model->estado = 0;
            $model->update(false);
            if($model2->save()){
                       if (isset($_POST['ActividadMnto'][0]['nombre'])) {
                    foreach ($_POST['ActividadMnto'] as $indexHouse => $rooms) {
                        $nueva_activ = new ActividadMnto();
                        $nueva_activ->nombre = $rooms['nombre'];
                        $nueva_activ->tipo_actividad = $rooms['tipo_actividad'];
                        $nueva_activ->id_tipo_mnto = $model2->id_tipo_mnto;
                        if($nueva_activ->save()){
                            $contador=0;
                            foreach ($_POST['ActividadPieza'][$indexHouse] as $i => $value) {

                                $contador++;
                            }echo $contador;
                            for ($ii=0; $ii < ($contador) ; $ii++) { 
                                $nueva_pieza = new ActividadPieza();
                                $nueva_pieza->id_actividad_mnto = $nueva_activ->id_actividad_mnto;
                                $nueva_pieza->id_pieza = $_POST['ActividadPieza'][$indexHouse][$ii]['id_pieza'];
                 

                              if(!$nueva_pieza->save()){
                                  print_r($nueva_pieza->getErrors()); die();
                                    $valid = 1;
                                }
                            }

                        }
                    }
                }
            }

            if($valid == 0){
                \Yii::$app->getSession()->setFlash('success', 'El Registro del Mantenimiento ha sido registrado con éxito!');
                return $this->redirect(['index']);
            }else{
                \Yii::$app->getSession()->setFlash('success', 'Error al registrar el Nuevo Tipo de Mantenimiento');
            }
        }

        return $this->render('update', [
            'modelact' => (empty($modelact)) ? [new TipoMantenimiento] : $modelact,
            'model' => $model,
            'modelpiezas' => (empty($modelpiezas)) ? [[new PaymentLoads]] : $modelpiezas,
           
        ]);


    }
    /**
     * Deletes an existing TipoMantenimiento model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TipoMantenimiento model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TipoMantenimiento the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TipoMantenimiento::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
