<?php

namespace app\controllers;

use Yii;
use app\models\PiezaServicioTaller;
use app\models\PiezaServicioTallerSearch;
use app\models\ServicioTaller;
use app\models\Model as Model;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/**
 * PiezaServicioTallerController implements the CRUD actions for PiezaServicioTaller model.
 */
class PiezaServicioTallerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PiezaServicioTaller models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->request->post('hasEditable')) {
            // instantiate your book model for saving
            $id_pieza = Yii::$app->request->post('editableKey');

            $model = $this->findModel($id_pieza);

            // store a default json response as desired by editable
            $out = Json::encode(['output' => '', 'message' => '']);

            // fetch the first entry in posted data (there should only be one entry
            // anyway in this array for an editable submission)
            // - $posted is the posted data for Book without any indexes
            // - $post is the converted array for single model validation
            
            $posted = current($_POST['PiezaServicioTaller']);
            $post['PiezaServicioTaller'] = $posted;

            // load model like any single model validation
            if ($model->load($post)) {

                // can save model or do something before saving model
                $model->save();

                // custom output to return to be displayed as the editable grid cell
                // data. Normally this is empty - whereby whatever value is edited by
                // in the input by user is updated automatically.
                $output = '';

                // specific use case where you need to validate a specific
                // editable column posted when you have more than one
                // EditableColumn in the grid view. We evaluate here a
                // check to see if buy_amount was posted for the Book model

                if (isset($posted['carro'])) {
                    $output = Yii::$app->formatter->asDecimal($model->carro,1);
                } elseif (isset($posted['jeepeta'])) {
                    $output = Yii::$app->formatter->asDecimal($model->jeepeta,1);
                } elseif (isset($posted['camioneta'])) {
                    $output = Yii::$app->formatter->asDecimal($model->camioneta,1);
                } elseif (isset($posted['van'])) {
                    $output = Yii::$app->formatter->asDecimal($model->van,1);
                } /*else if (isset($posted['uman_modificada'])) {
                    $output = Yii::$app->formatter->asInteger($model->uman_modificada);
                }*/
                

                // similarly you can check if the name attribute was posted as well
                // if (isset($posted['name'])) {
                // $output = ''; // process as you need
                // }
                $out = Json::encode(['output' => $output, 'message' => '']);
            }
            // return ajax json encoded response and exit
            echo $out;
            return;
        }

        $searchModel = new PiezaServicioTallerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $servicio_taller = ArrayHelper::map(ServicioTaller::find()->orderBy('nombre')->all(), 'id', 'nombre');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'servicio_taller' => $servicio_taller,
        ]);
    }

    /**
     * Displays a single PiezaServicioTaller model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PiezaServicioTaller model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = [new PiezaServicioTaller()];
        //$model_pieza = new PiezaServicioTaller();

        $servicio_taller = ArrayHelper::map(ServicioTaller::find()->orderBy('nombre')->all(), 'id', 'nombre');

        if (Yii::$app->request->post()) {
            $model = Model::createMultiple(PiezaServicioTaller::classname());
            Model::loadMultiple($model, Yii::$app->request->post());

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validateMultiple($model);
            }

            $valido = Model::validateMultiple($model);

            if ($valido) {
                foreach ($model as $key => $pieza) {
                    $pieza->estado = 1;
                    $bandera = $pieza->save();
                }
            }

            if ($bandera) {
                \Yii::$app->getSession()->setFlash('success', '¡La pieza ha sido creada con éxito!');
            } else{
                \Yii::$app->getSession()->setFlash('error', '¡Error al guardar la pieza!');   
            }
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
            //'model_pieza' => $model_pieza,
            'servicio_taller' => $servicio_taller,
        ]);
    }

    /**
     * Updates an existing PiezaServicioTaller model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PiezaServicioTaller model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PiezaServicioTaller model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PiezaServicioTaller the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PiezaServicioTaller::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
