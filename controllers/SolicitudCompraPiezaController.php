<?php

namespace app\controllers;

use Yii;
use app\models\SolicitudCompraPieza;
use app\models\SolicitudCompraPiezaSearch;
use app\models\Proveedor;
use app\models\SolicitudCompraProveedor;
use app\models\SolicitudCompraProveedorSearch;
use app\models\CotizacionProveedor;
use app\models\CotizacionProveedorPieza;
use app\models\CotizacionProveedorPiezaSearch;
use app\models\CotizacionTallerPieza;
use app\models\CotizacionTaller;
use app\models\OrdenCompraPieza;
use app\models\OrdenCompraPiezaSearch;
use app\models\OrdenCompraFactura;
use app\models\User;
use app\models\Flota;
use app\models\Servicio;
use app\models\Estacion;
use app\models\Vehiculo;
use app\models\EstadoCompraPieza;
use app\models\PipelineCompraPieza;
use app\models\DespachoSearch;
use app\models\Despacho;
use app\models\DespachoProveedorPiezaSearch;
use app\models\DespachoProveedorPieza;
use app\models\SolicitudServicioTaller;
use app\models\Taller;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use kartik\mpdf\Pdf;
use mPDF;
use app\models\EstadoServicio;
use app\models\PipelineServicio;
use app\models\MotivoDevolucion;
use app\models\SolicitudPiezaEstacion;
use app\models\UploadImagenPieza;
use yii\web\UploadedFile;
use app\models\Pieza;


/**
 * SolicitudCompraPiezaController implements the CRUD actions for SolicitudCompraPieza model.
 */

class SolicitudCompraPiezaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SolicitudCompraPieza models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SolicitudCompraPiezaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy('id DESC');

        //Proveedor recomendado en base a la cantidad de ordenes de compras
        // $mejor_proveedor = OrdenCompraPieza::find()
        // ->select(['proveedor.nombre as pro, id_proveedor, COUNT(id_proveedor) totales'])
        // ->leftJoin('proveedor', '`proveedor`.`id` = `orden_compra_pieza`.`id_proveedor`')
        // ->groupBy(['id_proveedor'])
        // ->orderBy('totales DESC')
        // ->limit(3)
        // ->all();

        //Proveedor recomendado en base al porcentaje de descuento
        $mejor_proveedor = Proveedor::find()
        ->orderBy('porcentaje DESC')
        ->limit(3)
        ->all();


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'mejor_proveedor' => $mejor_proveedor
        ]);
    }

    /**
     * Displays a single SolicitudCompraPieza model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SolicitudCompraPieza model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SolicitudCompraPieza();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SolicitudCompraPieza model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SolicitudCompraPieza model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SolicitudCompraPieza model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SolicitudCompraPieza the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SolicitudCompraPieza::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

        /**
     * Solicitar cotizaciones a los talleres
     * @autor Zulma Hernández
     */
    public function actionSolicitudcotizacioninvitacion()
    {
        // print_r('entre');
        // die();
        if (Yii::$app->request->post()) {
            $id = Yii::$app->request->post('id');
            $model = $this->findModel($id);
            $proveedores = Yii::$app->request->post('proveedor');
            $enviadas = SolicitudCompraProveedor::find()->where(['id_solicitud_compra_pieza' => $model->id])->all();
            $cantidad_enviadas = count($enviadas);

            foreach ($proveedores as $i => $proveedor) {
                if ( $cantidad_enviadas > 0 ) {
                    foreach($enviadas as $env) {
                        if($env->id_proveedor == $proveedor) {
                            $band = 0;
                        }else {
                            $band = 1;
                        }
                    }
                } else{
                    $band = 1;
                }
                
                if($band == 1) {
                    $this->solicitudCompraProveedor($proveedor, $id);
                }else {
                    \Yii::$app->getSession()->setFlash('success', '¡La solicitud ha sido enviada con éxito!');
                }
            }
            if($cantidad_enviadas == 0) {               
                $model->codigo_estado = 1;
                if ($model->save()) {
                    $estado_compra_pieza = EstadoCompraPieza::findOne(['codigo' => 1])->id; //Espera de cotización
                    $model_pipeline_compra_pieza = new PipelineCompraPieza();
                    $model_pipeline_compra_pieza->fecha = date('Y-m-d');
                    $model_pipeline_compra_pieza->id_solicitud_compra_pieza = $model->id;
                    $model_pipeline_compra_pieza->id_estado_compra_pieza = $estado_compra_pieza;
                    $model_pipeline_compra_pieza->save();
                    unset($model_pipeline_compra_pieza);
                    \Yii::$app->getSession()->setFlash('success', '¡La solicitud se ha creado con éxito!');
                } else{
                    echo "No se pudo cambiar el estado de la solicitud";
                } 
            }else {
                \Yii::$app->getSession()->setFlash('success', '¡La solicitud ha sido enviada con éxito!');                
            }
        }
    }

    /**
     * Solicitar cotizaciones a los talleres
     * @autor Zulma Hernández
     */
    public function actionSolicitudcotizacion()
    {
        // print_r('entre');
        // die();
        if (Yii::$app->request->post()) {
            $id = Yii::$app->request->post('id');
            $model = $this->findModel($id);
            $proveedores = Yii::$app->request->post('proveedor');
            $enviadas = SolicitudCompraProveedor::find()->where(['id_solicitud_compra_pieza' => $model->id])->all();
            $cantidad_enviadas = count($enviadas);

            foreach ($proveedores as $i => $proveedor) {
                $this->solicitudCompraProveedor($proveedor, $id);
            }
            if($cantidad_enviadas == 0) {               
                $model->codigo_estado = 1;
                if ($model->save()) {
                    $estado_compra_pieza = EstadoCompraPieza::findOne(['codigo' => 1])->id; //Espera de cotización
                    $model_pipeline_compra_pieza = new PipelineCompraPieza();
                    $model_pipeline_compra_pieza->fecha = date('Y-m-d');
                    $model_pipeline_compra_pieza->id_solicitud_compra_pieza = $model->id;
                    $model_pipeline_compra_pieza->id_estado_compra_pieza = $estado_compra_pieza;
                    $model_pipeline_compra_pieza->save();
                    unset($model_pipeline_compra_pieza);
                    \Yii::$app->getSession()->setFlash('success', '¡La solicitud se ha creado con éxito!');
                } else{
                    echo "No se pudo cambiar el estado de la solicitud";
                } 
            }else {
                \Yii::$app->getSession()->setFlash('success', '¡La solicitud ha sido enviada con éxito!');                
            }
        }
    }

    protected function solicitudCompraProveedor($proveedor, $id)
    {
        $model = $this->findModel($id);
        $model_solicitud_compra_proveedor = new SolicitudCompraProveedor();
        $model_proveedor = Proveedor::findOne($proveedor);
        $model_solicitud_compra_proveedor->id_solicitud_compra_pieza = $model->id;
        $model_solicitud_compra_proveedor->id_proveedor = $model_proveedor->id;

        if ($model_solicitud_compra_proveedor->save(false)) {
            $model_cotizacion_proveedor = new CotizacionProveedor();
            $model_cotizacion_proveedor->fecha = date("Y-m-d H:i:s");
            $model_cotizacion_proveedor->estado = 0; //Creada la cotizacion
            $model_cotizacion_proveedor->id_solicitud_compra_proveedor = $model_solicitud_compra_proveedor->id;
            if ($model_cotizacion_proveedor->save()) {
                if($model->origen_solicitud == 2) {
                    $model_cotizacion_taller_pieza = SolicitudPiezaEstacion::find()->where(['id_solicitud_compra_estacion' => $model->id_solicitud_compra_estacion])->all();
                }else {
                    $model_cotizacion_taller_pieza = CotizacionTallerPieza::find()->where(['id_cotizacion_taller' => $model->id_cotizacion_taller])->all();
                }
                foreach ($model_cotizacion_taller_pieza as $k => $pieza) {
                    $model_cotizacion_proveedor_pieza = new CotizacionProveedorPieza();
                    $model_cotizacion_proveedor_pieza->id_pieza = $pieza->id_pieza;
                    $model_cotizacion_proveedor_pieza->cantidad_solicitada = $pieza->cantidad;
                    $model_cotizacion_proveedor_pieza->id_cotizacion_proveedor = $model_cotizacion_proveedor->id;
                    if($model->origen_solicitud == 2) {
                        $model_cotizacion_proveedor_pieza->id_solicitud_pieza_estacion = $pieza->id;
                    }else {
                        $model_cotizacion_proveedor_pieza->id_cotizacion_taller_pieza = $pieza->id;
                    }
                    $model_cotizacion_proveedor_pieza->ahorro = $model_proveedor->porcentaje;
                    $model_cotizacion_proveedor_pieza->estado_despacho = 0;
                    $model_cotizacion_proveedor_pieza->save();
                    unset($model_cotizacion_proveedor_pieza);
                }
            } else{
                echo "No se pudo crear la cotizacion";
            }
        } else{
            echo "No se pudo asignar el proveedor";
        }
    }

    /**
     * Obtener los proveedores para una solicitud de compra de piezas
     * @autor Zulma Hernández
     */
    public function actionGetproveedores()
    {
        if (Yii::$app->request->post()) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $model_proveedores = Proveedor::find()->all();

            $proveedores = ArrayHelper::toArray($model_proveedores, [
                'app\models\Proveedor' => [
                    'id',
                    'nombre',
                ],
            ]);

            return $proveedores;
        }
    }

    /**
     * Index de la solicitud de cotizaciones para los proveedores
     * @autor Zulma Hernández
     */
    public function actionIndexsolicitudcotizacion()
    {
        $id_proveedor = User::findOne(Yii::$app->user->id)->id_proveedor;
        $searchModel = new SolicitudCompraProveedorSearch(['id_proveedor' => $id_proveedor]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        //$id_servicio = SolicitudServicioTaller::findOne($id)->id_servicio;

        return $this->render('index-solicitud-cotizacion', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'id_proveedor' => $id_proveedor,
        ]);
    }

    /**
     * Cotización del proveedor
     * @autor Zulma Hernández
     */
    public function actionCotizacionproveedor($id, $origen = 0)
    {
        $model_cotizacion_proveedor = CotizacionProveedor::findOne($id);
        //$model_cotizacion_proveedor_pieza = CotizacionProveedorPieza::find()->where(['id_cotizacion_proveedor' => $model_cotizacion_proveedor->id]);

        $searchModel = new CotizacionProveedorPiezaSearch(['id_cotizacion_proveedor' => $model_cotizacion_proveedor->id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $model_cotizacion_proveedor->scenario = 'update';

        $id_servicio = $model_cotizacion_proveedor->idSolicitudCompraProveedor->idSolicitudCompraPieza->id_servicio;
        $model_servicio = Servicio::findOne($id_servicio);
        $model_vehiculo = Vehiculo::findOne($model_servicio->id_vehiculo);

        return $this->render('cotizacion-proveedor', [
            'dataProvider' => $dataProvider,
            'model_vehiculo' => $model_vehiculo,
            'model_cotizacion_proveedor' => $model_cotizacion_proveedor,
            'origen' => $origen
        ]);
    }

    /**
     * Guardar cotización del proveedor
     * @autor Zulma Hernández
     */
    public function actionGuardarcotizacionproveedor($id)
    {
        if (Yii::$app->request->post()) {
            $cantidad_disponible = Yii::$app->request->post('cantidad_disponible');
            $id_pieza = Yii::$app->request->post('id_pieza');
            $fecha_entrega = Yii::$app->request->post('fecha_entrega');
            $costo = Yii::$app->request->post('costo');
            $descuento = Yii::$app->request->post('descuento');
            $garantia = Yii::$app->request->post('garantia');

            $total_descuento = $itbs = $total = 0;

            $model_cotizacion_proveedor = CotizacionProveedor::findOne($id);
            $model_cotizacion_proveedor->load(Yii::$app->request->post());
            $model_cotizacion_proveedor->estado = 1; //Editada por el proveedor

            foreach ($cantidad_disponible as $i => $disponible) {
                $model_cotizacion_proveedor_pieza = CotizacionProveedorPieza::find()->where([ 'id' => $id_pieza[$i] ])->one();
                $model_cotizacion_proveedor_pieza->cantidad_disponible = $disponible;
                $model_cotizacion_proveedor_pieza->fecha_entrega = date("Y-m-d", strtotime($fecha_entrega[$i]));
                $model_cotizacion_proveedor_pieza->costo = $costo[$i];
                $model_cotizacion_proveedor_pieza->ahorro = $descuento[$i];
                $model_cotizacion_proveedor_pieza->aprobacion = 0;
                $model_cotizacion_proveedor_pieza->garantia = $garantia[$i];
                $model_cotizacion_proveedor_pieza->save();

                $total_descuento += ($costo[$i] * $model_cotizacion_proveedor_pieza->cantidad_solicitada) - ((($costo[$i] * $model_cotizacion_proveedor_pieza->cantidad_solicitada) * $descuento[$i])/100);
            }
            
            $itbis = ($total_descuento * 0.18);
            $total = $total_descuento + $itbis;

            $model_cotizacion_proveedor->subtotal = $total_descuento;
            $model_cotizacion_proveedor->itbs = $itbis;
            $model_cotizacion_proveedor->descuento = $total_descuento;

            if ( $model_cotizacion_proveedor->save() ) {
                \Yii::$app->getSession()->setFlash('success', '¡La cotización se ha guardado con éxito!');
            } else{
                \Yii::$app->getSession()->setFlash('error', '¡Error al guardar la cotización!');
            }

            $this->redirect('../indexsolicitudcotizacion');
        }
    }

    /**
     * Cotizaciones recibidas por los proveedores solicitados
     * @autor Zulma Hernández
     */
    public function actionCotizacionesrecibidas($id)
    {
        $model = $this->findModel($id);
        $flag = 0;

        if (Yii::$app->request->post()) {
            //Resetear valores de la cotizacion rechazada
            if ($model->aprobacion == 5) {
                $flag = 5;
                $id_cotizacion_proveedor_pieza = Yii::$app->request->post('id');
                
                $model->aprobacion = NULL; //Resetear estatus
                $model->observacion_gerente = NULL; 
                $model->motivo_rechazo = NULL; 
                $model->save();
                
                foreach ($id_cotizacion_proveedor_pieza as $i => $icpp) {
                    $cotizacion = CotizacionProveedorPieza::findOne($icpp);
                    $cotizacion->aprobacion = NULL; //Resetear estatus
                    $cotizacion->cantidad_comprar = NULL;
                    $cotizacion->save();
                }
            }
            if ($model->load(Yii::$app->request->post())) {
                $model->aprobacion = 1; //Para saber que guardo la seleccion de cotizaciones recibidas
                $model->save();
            }

            $compras = Yii::$app->request->post('comprar');
            $cotizaciones = Yii::$app->request->post('selected');

            foreach ($compras as $i => $compra) {
                $cotizacion = CotizacionProveedorPieza::findOne($cotizaciones[$i]);
                $cotizacion->cantidad_comprar = $compra;
                $cotizacion->aprobacion = 1; //Para saber que selecciono la cotizacion del proveedor seleccionado
                $cotizacion->save();
            }

            $model->codigo_estado = 2;
            $model->id_analista = Yii::$app->user->identity->id;
            if ($model->save()) {
                if($flag == 0) {
                    $estado_compra_pieza = EstadoCompraPieza::findOne(['codigo' => 2])->id; //Aprobacion
                    $model_pipeline_compra_pieza = new PipelineCompraPieza();
                    $model_pipeline_compra_pieza->fecha = date('Y-m-d');
                    $model_pipeline_compra_pieza->id_solicitud_compra_pieza = $model->id;
                    $model_pipeline_compra_pieza->id_estado_compra_pieza = $estado_compra_pieza;
                    $model_pipeline_compra_pieza->save();
                    unset($model_pipeline_compra_pieza); 
                }               
            } else{
                \Yii::$app->getSession()->setFlash('error', '¡Hubo un error!');
            } 

            \Yii::$app->getSession()->setFlash('success', '¡Se han guardado las cotizaciones seleccioandas con éxito!');
            return $this->redirect(['solicitud-compra-pieza/index']);

        } else{
            if($model->origen_solicitud == 2) {
                $piezas = ArrayHelper::map(SolicitudPiezaEstacion::find()->where('id_solicitud_compra_estacion = '. $model->id_solicitud_compra_estacion)->all(), 'id_pieza', 'idPieza.nombre');
            }else {
                $piezas = ArrayHelper::map(CotizacionTallerPieza::find()->where('id_cotizacion_taller = '. $model->id_cotizacion_taller)->all(), 'id_pieza', 'idPieza.nombre');
            }
            $cotizaciones_piezas = CotizacionProveedorPieza::find()->joinWith('idCotizacionProveedor')->joinWith('idCotizacionProveedor.idSolicitudCompraProveedor')->where('solicitud_compra_proveedor.id_solicitud_compra_pieza ='.$model->id )->all();
            $cotizaciones_piezas_rechazadas = CotizacionProveedorPieza::find()->joinWith('idCotizacionProveedor')->joinWith('idCotizacionProveedor.idSolicitudCompraProveedor')->where('solicitud_compra_proveedor.id_solicitud_compra_pieza ='.$model->id .' AND cotizacion_proveedor_pieza.aprobacion = 5')->all();

            return $this->render('cotizaciones-recibidas', [
                'model' => $model,
                'cotizaciones_piezas' => $cotizaciones_piezas,
                'cotizaciones_piezas_rechazadas' => $cotizaciones_piezas_rechazadas,
                'piezas' => $piezas,
            ]);
        }
    }

    public function actionIndexcotizacionesrecibidas($id)
    {
        $model = $this->findModel($id);
        $piezas = ArrayHelper::map(CotizacionTallerPieza::find()->where('id_cotizacion_taller = '. $model->id_cotizacion_taller)->all(), 'id_pieza', 'idPieza.nombre');
        $cotizaciones_piezas = CotizacionProveedorPieza::find()->joinWith('idCotizacionProveedor')->joinWith('idCotizacionProveedor.idSolicitudCompraProveedor')->where('solicitud_compra_proveedor.id_solicitud_compra_pieza ='.$model->id )->all();

        return $this->render('index-cotizaciones-recibidas', [
            'model' => $model,
            'cotizaciones_piezas' => $cotizaciones_piezas,
            'piezas' => $piezas,
        ]);
    }

    /**
     * Cotización del proveedor
     * @autor Zulma Hernández
     */
    public function actionSolicitudprobacion($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->post()) {
            $model->codigo_estado = 2;
            if ($model->save()) {
                $estado_compra_pieza = EstadoCompraPieza::findOne(['codigo' => 2])->id; //Aprobacion
                $model_pipeline_compra_pieza = new PipelineCompraPieza();
                $model_pipeline_compra_pieza->fecha = date('Y-m-d');
                $model_pipeline_compra_pieza->id_solicitud_compra_pieza = $model->id;
                $model_pipeline_compra_pieza->id_estado_compra_pieza = $estado_compra_pieza;
                $model_pipeline_compra_pieza->save();
                unset($model_pipeline_compra_pieza);
                \Yii::$app->getSession()->setFlash('success', '¡La solicitud de aprobación se ha creado con éxito!');
                
            } else{
                \Yii::$app->getSession()->setFlash('error', '¡Hubo un error!');
            }
            return $this->redirect(['solicitud-compra-pieza/index']);
        } else {
            $cotizaciones_piezas = CotizacionProveedorPieza::find()->joinWith('idCotizacionProveedor')->joinWith('idCotizacionProveedor.idSolicitudCompraProveedor')->where('solicitud_compra_proveedor.id_solicitud_compra_pieza ='.$model->id .' AND cotizacion_proveedor_pieza.aprobacion = 1')->all();
            $piezas = ArrayHelper::map(CotizacionTallerPieza::find()->where('id_cotizacion_taller = '. $model->id_cotizacion_taller)->all(), 'id_pieza', 'idPieza.nombre');
            return $this->render('solicitar-probacion', [
                'model' => $model,
                'cotizaciones_piezas' => $cotizaciones_piezas,
                'piezas' => $piezas,
            ]);
        }
        
    }

        /**
     * Cotización del proveedor
     * @autor Zulma Hernández
     */
    public function actionGestionaprobacion($id)
    {
        // print_r($_POST);
        // die();
        $model = $this->findModel($id);

        $cotizaciones_piezas = CotizacionProveedorPieza::find()->joinWith('idCotizacionProveedor')->joinWith('idCotizacionProveedor.idSolicitudCompraProveedor')->where('solicitud_compra_proveedor.id_solicitud_compra_pieza ='.$model->id .' AND cotizacion_proveedor_pieza.aprobacion = 1')->all();
        $model_servicio = Servicio::findOne($model->id_servicio);
        $model_vehiculo = Vehiculo::findOne($model_servicio->id_vehiculo);
        $cotizacion = CotizacionTaller::findOne($model->id_cotizacion_taller);
        $solicitud_taller = SolicitudServicioTaller::findOne($cotizacion->id_solicitud_servicio_taller);
        $taller = Taller::findOne($solicitud_taller->id_taller);

        return $this->render('gestion-aprobacion', [
            'model' => $model,
            'cotizaciones_piezas'   => $cotizaciones_piezas,
            'model_servicio'        => $model_servicio,
            'model_vehiculo'        => $model_vehiculo,
            'model_taller'          => $taller,      
        ]);
        
    }

    /**
     * Index de la solicitud de aprobaciones de compra para los gerentes de compra
     * @autor Zulma Hernández
     */
    public function actionIndexsolicitudaprobacion()
    {
        if(\Yii::$app->user->can('director-comercial')) {
            $searchModel = new SolicitudCompraPiezaSearch(['codigo_estado' => 2, 'aprobacion' => 2]);
        }else {
            $searchModel = new SolicitudCompraPiezaSearch(['codigo_estado' => 2]);
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy('id DESC');

        return $this->render('index-solicitud-aprobacion', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /*
    * Aprobar una solicitud de compra, genrente de compras
    * @autor Zulma Hernández
    */
    public function actionAprobarsolicitud(){
        // print_r($_POST);
        // die();
        if (Yii::$app->request->post()) {
            //$id = Yii::$app->request->post('id');
            $solicitud = Yii::$app->request->post('SolicitudCompraPieza');            
            $id = $solicitud['id']; 
            $mlp = $solicitud['mlp']; 
            $estado =  $solicitud['estado']; 

            $model = $this->findModel($id);
            if($estado == 1) {
                if ($mlp == 2 && $estado == 1 && \Yii::$app->user->can('gerente-compras')) {
                    if ($model->load(Yii::$app->request->post())) {
                        $model->aprobacion = 2; //Indica que el monto supera, al establecido en configuración y tiene que ser aprobado por director-comercial
                        $model->save();
                    }
                    \Yii::$app->getSession()->setFlash('success', 'La solicitud de aprobacion fue enviada al director comercial');
                    return $this->redirect(['solicitud-compra-pieza/indexsolicitudaprobacion/']);
                }elseif ($mlp == 1 && $estado == 1 && \Yii::$app->user->can('gerente-compras') || $mlp == 2 && $estado == 1 && \Yii::$app->user->can('director-comercial')) {
                    
                    $this->ordenCompra($model->id); //Generar orden de compra

                    //Estatus de cotizacion por proveedor
                    $solicitudes_proveedor = SolicitudCompraProveedor::find()->where(['id_solicitud_compra_pieza' => $model->id])->all();
                    foreach($solicitudes_proveedor as $sp) {
                        $cotizacion_proveedor = CotizacionProveedor::find()->where(['id_solicitud_compra_proveedor' => $sp->id])->one();
                        if( $cotizacion_proveedor->estado == 0 ) {
                            $cotizacion_proveedor->estado = 3;// Notificar que el proveedor no envio cotizacion y perdio el chance 
                            //$cotizacion_proveedor->save();  
                        }elseif( $cotizacion_proveedor->estado == 1) {
                            $cotizacion_proveedor->estado = 2; // Ya aprobaron cotizacion y no tienen chance de editarlas
                        }                                                  
                        $cotizacion_proveedor->save();
                    }
                    
                    $model->codigo_estado = 3; //Por recibir
                    if ($model->save()) {
                        $estado_compra_pieza = EstadoCompraPieza::findOne(['codigo' => 3])->id; //Por recibir
                        $model_pipeline_compra_pieza = new PipelineCompraPieza();
                        $model_pipeline_compra_pieza->fecha = date('Y-m-d');
                        $model_pipeline_compra_pieza->id_solicitud_compra_pieza = $model->id;
                        $model_pipeline_compra_pieza->id_estado_compra_pieza = $estado_compra_pieza;
                        $model_pipeline_compra_pieza->save();
                        unset($model_pipeline_compra_pieza);
                        \Yii::$app->getSession()->setFlash('success', '¡La orden de compra se ha creado con éxito!');
                        return $this->redirect(['solicitud-compra-pieza/indexsolicitudaprobacion/']);
                    } else{
                        \Yii::$app->getSession()->setFlash('error', '¡Error al generar la orden de compra!');
                        return $this->redirect(['solicitud-compra-pieza/gestionaprobacion/'. $id]);
                    }
                }
            }elseif($estado == 2) {
                // print_r($mlp = $solicitud['observacion_gerente']);
                // die();
                if ($mlp == 1 && $estado == 2 && \Yii::$app->user->can('gerente-compras') || $mlp == 2 && $estado == 2 && \Yii::$app->user->can('gerente-compras') || $mlp == 2 && $estado == 2 && \Yii::$app->user->can('director-comercial')) {
                    
                    $id_cotizacion_proveedor_pieza = Yii::$app->request->post('id');
                    if ($model->load(Yii::$app->request->post())) {
                        $model->aprobacion = 5; //Para saber que rechazo la coticacion enviada por el analista
                        $model->save();
                    }
                    
                    foreach ($id_cotizacion_proveedor_pieza as $i => $icpp) {
                        $cotizacion = CotizacionProveedorPieza::findOne($icpp);
                        $cotizacion->aprobacion = 5; //Para saber que rechazo la coticacion enviada por el analista
                        $cotizacion->save();
                    }
                    \Yii::$app->getSession()->setFlash('success', 'La solicitud ha sido rechazada con éxito');
                    return $this->redirect(['solicitud-compra-pieza/indexsolicitudaprobacion/']);
                }
            }elseif($estado == 3 && \Yii::$app->user->can('director-comercial')) {//Cancelar caso y compra de piezas

                //Cancelar compra piezas
                $model->codigo_estado = 7; //Cancelado
                if ($model->save()) {
                    $estado_compra_pieza = EstadoCompraPieza::findOne(['codigo' => 7])->id; //Cancelado
                    $model_pipeline_compra_pieza = new PipelineCompraPieza();
                    $model_pipeline_compra_pieza->fecha = date('Y-m-d');
                    $model_pipeline_compra_pieza->id_solicitud_compra_pieza = $model->id;
                    $model_pipeline_compra_pieza->id_estado_compra_pieza = $estado_compra_pieza;
                    $model_pipeline_compra_pieza->save();
                    unset($model_pipeline_compra_pieza);
                } 

                $id_cotizacion_proveedor_pieza = Yii::$app->request->post('id');
                if ($model->load(Yii::$app->request->post())) {
                    $model->aprobacion = 7; //Para saber que cancelo el caso
                    $model->save();
                }
                    
                foreach ($id_cotizacion_proveedor_pieza as $i => $icpp) {
                    $cotizacion = CotizacionProveedorPieza::findOne($icpp);
                    $cotizacion->aprobacion = 7; //Para saber que cancelo el caso
                    $cotizacion->save();
                }

                //Cancelar Caso
                //Motivo de cancelación
                $motivo = Yii::$app->request->post('Servicio');
                $model_servicio = Servicio::findOne( $model->id_servicio );
                $model_servicio->observacion_cancelado = $motivo['observacion_cancelado'];
                $model_servicio->usuario_cancela = Yii::$app->user->identity->id;
                $model->update();

                /* Pipeline -> Estado Cancelado */
                $estado_0 = EstadoServicio::findOne(['codigo' => 12])->id;
                $model_pipeline = new PipelineServicio();
                $model_pipeline->fecha =  date('Y-m-d');
                $model_pipeline->id_servicio = $model->id_servicio;
                $model_pipeline->id_estado_servicio = $estado_0;
                $model_pipeline->save();
                unset($model_pipeline);
                /*Mail*/
                try {
                    $mensaje_sup ='  <p> Km100 Le informa que la solicitud de servicio fue cancelada:
                                    </p>
                                    <p>
                                        <strong>Servicio:</strong> Nº'.$model->id_servicio.'<br>
                                        <strong>MVA Ficha: </strong>'.$model_servicio->ficha.'<br>
                                        <strong>Nº Placa: </strong>'.$model_servicio->placa.'<br>
                                        <strong>Motivo de cancelación: </strong>'.$motivo['observacion_cancelado'].'
                                    </p>
                    ';

                    Yii::$app->mailer->compose(['html'=>'layout'], ['content'=> $mensaje_sup])
                                    ->setFrom([Yii::$app->params['adminEmail'] => 'KM100'])
                                    ->setTo('zulmadessire@gmail.com')
                                    ->setSubject('Solicitud de Servicio Cancelada')
                                    ->send();
                } catch (Exception $e) {
                    \Yii::$app->getSession()->setFlash('error', 'Error al enviar el correo de notificación');
                }
                /*Fin Mail*/
                    
                \Yii::$app->getSession()->setFlash('success', 'La solicitud ha sido rechazada con éxito');
                return $this->redirect(['solicitud-compra-pieza/indexsolicitudaprobacion/']);
            }
        }
    }

    public function ordenCompra($id) {
        $cotizaciones_piezas = CotizacionProveedorPieza::find()->joinWith('idCotizacionProveedor')->joinWith('idCotizacionProveedor.idSolicitudCompraProveedor')->where('solicitud_compra_proveedor.id_solicitud_compra_pieza ='.$id .' AND cotizacion_proveedor_pieza.aprobacion = 1')->all();
        foreach ($cotizaciones_piezas as $i => $pieza) {
            if ($pieza) {    

                $model_orden_compra = OrdenCompraPieza::find()->where(['id_cotizacion_proveedor' => $pieza->id_cotizacion_proveedor])->one();
                $total_pieza_descuento = $pieza->costo - ( ($pieza->costo * $pieza->ahorro) / 100 ) ;
                $total_pieza_itbs = ($total_pieza_descuento * 0.18 ) + $total_pieza_descuento;
                
                if (!$model_orden_compra) {
                    
                    $model_orden_compra = new OrdenCompraPieza();
                    $model_orden_compra->fecha = date("Y-m-d H:i:s");
                    $model_orden_compra->estado = 0; //creado
                    $model_orden_compra->id_solicitud_compra_pieza = $id;
                    $model_orden_compra->id_cotizacion_proveedor = $pieza->id_cotizacion_proveedor;
                    $model_orden_compra->total = $total_pieza_itbs;
                    $model_orden_compra->id_proveedor = $pieza->idCotizacionProveedor->idSolicitudCompraProveedor->id_proveedor;
                    $model_orden_compra->save();

                } else{
                    $total = ($model_orden_compra->total)? $model_orden_compra->total : 0;                    
                    $model_orden_compra->total = $total + $total_pieza_itbs;
                    $model_orden_compra->save();
                }

                $pieza->aprobacion = 2; //Aprobado por el gerente de compras
                $pieza->save();
            }
        }
    }

    /**
     * Index de la solicitud de ordenes de compra para el proveedor
     * @autor Zulma Hernández
     */
    public function actionIndexordencompraproveedor()
    {
        $model_orden_compra_factura = new OrdenCompraFactura();

        $id_proveedor = User::findOne(Yii::$app->user->id)->id_proveedor;
        $searchModel = new OrdenCompraPiezaSearch(['id_proveedor' => $id_proveedor ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy('id DESC');
        
        return $this->render('index-orden-compra-proveedor', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'model_orden_compra_factura' => $model_orden_compra_factura,
        ]);
    }

    /**
     * Asociar factura a una orden de compra vista del proveedor
     * @autor Zulma Hernández
     */
    public function actionAsociarfactura()
    {

        if (Yii::$app->request->post()) {
            $id = Yii::$app->request->post('id');
            $monto = Yii::$app->request->post('monto');

            $model_orden_compra_factura = new OrdenCompraFactura();
            $model_orden_compra = OrdenCompraPieza::findOne($id);

            if ($model_orden_compra_factura->load(Yii::$app->request->post())) {

                if ($monto == $model_orden_compra->total) {
                    $model_orden_compra->estado = 1;//Con factura asociada
                    $model_orden_compra->update();
                    $model_orden_compra_factura->fecha = date("Y-m-d H:i:s");
                    $model_orden_compra_factura->monto = $monto; 
                    $model_orden_compra_factura->estado = 0; //creado
                    $model_orden_compra_factura->id_orden_compra_pieza = $model_orden_compra->id;
                    if ($model_orden_compra_factura->save()) {
                        \Yii::$app->getSession()->setFlash('success', '¡La factura se ha asociado con éxito!');
                    } else{
                        \Yii::$app->getSession()->setFlash('error', 'Error, la factura no se pudo asociar');
                    }
                } else{
                    \Yii::$app->getSession()->setFlash('error', 'Error, El monto de la factura no coincide con el monto de la orden de compra.');
                }
            } else{
                \Yii::$app->getSession()->setFlash('error', 'Error al cargar la información de la factura');
            }
            return $this->redirect('indexordencompraproveedor');
        }
    }

    /**
     * Obtener los proveedores que tienen orden de trabajo
     * @autor Zulma Hernández
     */
    public function actionGetproveedoresrecepcion()
    {
        if (Yii::$app->request->post()) {

            $proveedores = ArrayHelper::map(Proveedor::find()->select(['id', 'nombre'])->distinct()->orderBy('nombre_comercial')->all(), 'nombre_comercial', 'nombre_comercial');
            /*\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $model_proveedores = Proveedor::find()->all();

            $proveedores = ArrayHelper::toArray($model_proveedores, [
                'app\models\Proveedor' => [
                    'id',
                    'nombre',
                ],
            ]);*/

            return $proveedores;
        }
    }

    /**
     * Visualizar orden compra en PDF
     * @autor Zulma Hernández
     */
    public function actionImprimirorden($id)
    {
        //Obtengo la orden de compra solicitada
        $model_orden_compra = OrdenCompraPieza::findOne( $id );
        //Obtengo la solicitud compra pieza asociada a la orden de compra
        $model = $this->findModel( $model_orden_compra->id_solicitud_compra_pieza );
        $model_cotizacion_taller = CotizacionTaller::findOne( $model->id_cotizacion_taller );
        $model_servicio = Servicio::findOne( $model->id_servicio );
        $model_vehiculo = Vehiculo::findOne( $model_servicio->id_vehiculo );
        //Obtengo el proveedor asociado a la orden de compra
        $model_proveedor = Proveedor::findOne( $model_orden_compra->id_proveedor );
        //Obtengo las piezas aprobadas para la orden de compra
        $piezas = CotizacionProveedorPieza::find()->where(['id_cotizacion_proveedor' => $model_orden_compra->id_cotizacion_proveedor, 'aprobacion' => 2 ])->all();
        //$cotizaciones_piezas = CotizacionProveedorPieza::find()->joinWith('idCotizacionProveedor')->joinWith('idCotizacionProveedor.idSolicitudCompraProveedor')->where('solicitud_compra_proveedor.id_solicitud_compra_pieza ='.$model->id .' AND cotizacion_proveedor_pieza.aprobacion = 1')->all();
    

        /*echo "<pre>";
        print_r( $piezas );
        echo "</pre>";*/
        $pdf = new Pdf([
            'content' => $this->renderPartial('_ordencomprapieza',[ 'model_orden_compra' => $model_orden_compra, 'model_proveedor' => $model_proveedor, 'piezas' => $piezas, 'model_cotizacion_taller' => $model_cotizacion_taller, 'model_vehiculo' => $model_vehiculo ]),
            'destination' => Pdf::DEST_BROWSER,
            'mode' => Pdf::MODE_CORE,
            'format' => Pdf::FORMAT_A4,
            'filename' => 'OC_'.$id.'.pdf',
            'options' => [
                'title' => 'ORDEN DE COMPRA',
                'subject' => 'OC_'.$id,
            ],
        ]);
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');

        return $pdf->render(); 
        exit;
    }

    /**
     * Checklist de recepcion
     * @autor Zulma Hernández
     * @param integer $id cotizacion proveedor
     * @return mixed
     */
    public function actionChecklistrecepcion($id){

        $model_cotizacion_proveedor = CotizacionProveedor::findOne($id);
        $model_orden_compra = OrdenCompraPieza::find()->where([ 'id_cotizacion_proveedor' => $model_cotizacion_proveedor->id ])->one();

        if (Yii::$app->request->post()) {
            $cantidades_recibidas = Yii::$app->request->post('recibido');

            $serial = Yii::$app->request->post('serial');
            $piezas = Yii::$app->request->post('pieza_id');
            $j = 0;
            foreach ($cantidades_recibidas as $i => $recibido) {
                $model_cotizacion_proveedor_pieza = CotizacionProveedorPieza::findOne($piezas[$i]);
                $model_cotizacion_proveedor_pieza->inventario = 1;
                $model_cotizacion_proveedor_pieza->cantidad_recibida = $recibido;
                $model_cotizacion_proveedor_pieza->serial = $serial[$i];
                $model_cotizacion_proveedor_pieza->save();
                unset($model_cotizacion_proveedor_pieza);
            }
    

            if ($model_orden_compra->load(Yii::$app->request->post())) {
                $model_orden_compra->save();
            }

            \Yii::$app->getSession()->setFlash('success', '¡La recepción se ha guardado con éxito para el proveedor!');
            return $this->redirect(['solicitud-compra-pieza/index']);
        } else{
            $model_solicitud_compra_pieza = SolicitudCompraPieza::findOne( $model_orden_compra->id_solicitud_compra_pieza );
            $model_servicio = Servicio::findOne($model_solicitud_compra_pieza->id_servicio);
            $model_vehiculo = Vehiculo::findOne($model_servicio->id_vehiculo);
             $model_flota = Flota::findOne(['id_vehiculo'=>$model_vehiculo->id]); //Esto esta mal, hay que obtener el ultimo registro de esta tabla
            $model_estacion = Estacion::findOne($model_flota->id_estacion);
            $model_estacion2 = Estacion::findOne($model_servicio->id_estacion_actual);
            //$piezas = CotizacionProveedorPieza::find()->where(['id_cotizacion_proveedor' => $model_cotizacion_proveedor->id, 'aprobacion' => 2 ])->all();
            $model_proveedor = Proveedor::findOne($model_orden_compra->id_proveedor);

            $searchModel = new CotizacionProveedorPiezaSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->where('id_cotizacion_proveedor = '. $model_cotizacion_proveedor->id .' AND aprobacion = 2');

            return $this->render('checklist-recepcion', [
                'model_orden_compra' => $model_orden_compra,
                'model_servicio' => $model_servicio,
                'model_vehiculo' => $model_vehiculo,
                'dataProvider' => $dataProvider,
                'model_proveedor' => $model_proveedor,
                'model_estacion' => $model_estacion,
                'model_estacion2' => $model_estacion2,
                'model_solicitud_compra_pieza' => $model_solicitud_compra_pieza,
                            ]);
        }
        
    }
    /**
     * Index de la solicitud de ordenes de compra recibidas para el proveedor
     * @autor Zulma Hernández
     */
    public function actionRecibidas()
    {
        $searchModel = new CotizacionProveedorPiezaSearch(['inventario' => 1]);
        $dataProvider = $searchModel->search2(Yii::$app->request->queryParams);

        return $this->render('index-piezas-recibidas', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

      /**
     * Index de la solicitud de ordenes de compra cerradas para el proveedor
     * @autor Zulma Hernández
     */
    public function actionCerradas()
    {
        $searchModel = new SolicitudCompraPiezaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        

        return $this->render('index-cerradas', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }
    /**
     * Finalizar recepciones por parte de km
     * @autor Zulma Hernández
     * @param integer $id
     * @return mixed
     */
    public function actionFinalizarrecepcion(){
        if (Yii::$app->request->post()) {
            $id = Yii::$app->request->post('id');
            $observacion_recepcion = Yii::$app->request->post('observacion_recepcion');
            $model = $this->findModel($id);             
            $model->codigo_estado = 4;  //Asignacion de ruta
            if ($observacion_recepcion) {
                $model->observacion_recepcion = $observacion_recepcion;
            }
            if ($model->save()) {
                $estado_compra_pieza = EstadoCompraPieza::findOne(['codigo' => 4])->id; //Asignacion de ruta
                $model_pipeline_compra_pieza = new PipelineCompraPieza();
                $model_pipeline_compra_pieza->fecha = date('Y-m-d');
                $model_pipeline_compra_pieza->id_solicitud_compra_pieza = $model->id;
                $model_pipeline_compra_pieza->id_estado_compra_pieza = $estado_compra_pieza;
                $model_pipeline_compra_pieza->save();
                unset($model_pipeline_compra_pieza);
                \Yii::$app->getSession()->setFlash('success', '¡La recepción se ha finalizado con éxito!');
            } else{
                \Yii::$app->getSession()->setFlash('error', '¡Hubo un error!');
            }
           
            return;
        }
    }

    /**
     * Checklist de recepcion
     * @autor Zulma Hernández
     * @param integer $id
     * @return mixed
     */
    public function actionDespacho( $id ){

        $model = $this->findModel($id);

        $searchModel = new DespachoSearch([ 'id_solicitud_compra_pieza' => $id ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);        
        $dataProvider->query->orderBy('id DESC');

        $searchModelPieza = new CotizacionProveedorPiezaSearch();
        $dataProviderPieza = $searchModelPieza->search(Yii::$app->request->queryParams);
        $dataProviderPieza->query->joinWith('idCotizacionProveedor')->joinWith('idCotizacionProveedor.idSolicitudCompraProveedor')->where('solicitud_compra_proveedor.id_solicitud_compra_pieza = '. $model->id .' AND aprobacion = 2');
        $cantidad_piezas = $dataProviderPieza->getTotalCount();

        //$searchModel = new CotizacionProveedorPiezaSearch();
        $dataProviderDespacho = $searchModelPieza->search(Yii::$app->request->queryParams);
        $dataProviderDespacho->query->joinWith('idCotizacionProveedor')->joinWith('idCotizacionProveedor.idSolicitudCompraProveedor')->where('solicitud_compra_proveedor.id_solicitud_compra_pieza = '. $model->id .' AND aprobacion = 2 AND estado_despacho = 1');
        $cantidad_piezas_despacho = $dataProviderDespacho->getTotalCount();

        return $this->render('despacho', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'cantidad_piezas' => $cantidad_piezas,
            'cantidad_piezas_despacho' => $cantidad_piezas_despacho,

        ]);

    }
    //representantes de reparto

    public function actionListado(){
 
    // $model = $this->findModel($id);
    //die();
        $searchModel = new DespachoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where('id_mensajero ='.Yii::$app->user->identity->id_mensajero.' AND estado != 3 AND id_taller is not null');
        $dataProvider->query->orderBy('id DESC');
        return $this->render('listado-despacho', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    //representantes de reparto
    public function actionAsignadas($id){

        $searchModel = new DespachoProveedorPiezaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$id);
      
        // $dataProvider->query->where('id_mensajero ='.Yii::$app->user->identity->id_mensajero);
        // $dataProvider->query->where('id_mensajero ='.Yii::$app->user->identity->id_mensajero);

        if ( Yii::$app->request->post() ) {
            $piezas = Yii::$app->request->post('selection');
            $despacho = Despacho::findOne($id);
            $despacho->estado = 2;// Estado 2 para saber que fue enviada al taller 
            $despacho->update();
            foreach ($piezas as $key => $pieza) {                
                $model_despacho_proveedor_pieza = DespachoProveedorPieza::findOne($pieza);
                $model_despacho_proveedor_pieza->recibido = 1;

                $model_cotizacion_proveedor_pieza = CotizacionProveedorPieza::findOne($model_despacho_proveedor_pieza->id_cotizacion_proveedor_pieza);
                $model_cotizacion_proveedor_pieza->estado_despacho = 2; //RECIBIDO POR EL MENSAJERO
                $model_cotizacion_proveedor_pieza->save();

                if ($model_despacho_proveedor_pieza->update()) {
                    $band = 1;                   
                }
            } 
            if($band == 1) {
                return $this->redirect([ 'listado']); 
            }

        }
        
        return $this->render('piezas-asignadas', [
            // 'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    //Checklist piezas recibidas taller
    public function actionPiezasRecibidas($id) {
        $searchModel = new DespachoProveedorPiezaSearch();
        $dataProvider = $searchModel->searchPiezasTaller(Yii::$app->request->queryParams,$id);
        $dataProviderRecibidas = $searchModel->searchPiezasTallerRecibidas(Yii::$app->request->queryParams,$id);

        if ( Yii::$app->request->post() ) {
            $piezas = Yii::$app->request->post('selection');
            $despacho = Despacho::findOne($id);

            foreach ($piezas as $key => $pieza) {               
                $model_despacho_proveedor_pieza = DespachoProveedorPieza::findOne($pieza);
                $model_despacho_proveedor_pieza->recibido = 2;//Entregada a taller

                $model_cotizacion_proveedor_pieza = CotizacionProveedorPieza::findOne($model_despacho_proveedor_pieza->id_cotizacion_proveedor_pieza);
                $model_cotizacion_proveedor_pieza->estado_despacho = 3; //Entregado a Taller
                $model_cotizacion_proveedor_pieza->inventario = 3; //Entregado a Taller
                $model_cotizacion_proveedor_pieza->save();

                if ($model_despacho_proveedor_pieza->update()) {
                    $tr = 0;        

                }
            }

            //Piezas recibidas de taller por despacho
            $piezas_enviadas = DespachoProveedorPieza::find()->where(['id_despacho' => $id])->count();
            $piezas_recibidas_taller_mensajero = DespachoProveedorPieza::find()->where(['id_despacho' => $id])->andWhere(['recibido' => 2])->count();
            if($piezas_enviadas == $piezas_recibidas_taller_mensajero) {
                $despacho->estado = 3;// Entregada taller 
                $despacho->estado_recepcion_taller = 1;// Entregada taller 
                $despacho->update();
            }

            //Piezas Aprobadas por el analista y/o gerente de compras 
            $cotizaciones_piezas = CotizacionProveedorPieza::find()
            ->joinWith('idCotizacionProveedor')
            ->joinWith('idCotizacionProveedor.idSolicitudCompraProveedor')
            ->where('solicitud_compra_proveedor.id_solicitud_compra_pieza ='.$despacho->id_solicitud_compra_pieza .' AND cotizacion_proveedor_pieza.aprobacion = 2')
            ->count();

            //Cantidad de piezas recibidas por el taller
            $piezas_recibidas_taller = DespachoProveedorPieza::find()
            ->select('`despacho_proveedor_pieza`.`id`')
            ->leftJoin('despacho', '`despacho`.`id` = `despacho_proveedor_pieza`.`id_despacho`')
            ->where(['despacho.id_solicitud_compra_pieza' => $despacho->id_solicitud_compra_pieza])
            ->andWhere(['`despacho_proveedor_pieza`.`recibido`' => 2])
            ->count();

            if($cotizaciones_piezas == $piezas_recibidas_taller) {

                if ($tr == 0) {
                    $estado_compra_pieza = EstadoCompraPieza::findOne(['codigo' => 5])->id; //Entregada
                    $model_pipeline_compra_pieza = new PipelineCompraPieza();
                    $model_pipeline_compra_pieza->fecha = date('Y-m-d');
                    $model_pipeline_compra_pieza->id_solicitud_compra_pieza = $despacho->id_solicitud_compra_pieza;
                    $model_pipeline_compra_pieza->id_estado_compra_pieza = $estado_compra_pieza;
                    $model_pipeline_compra_pieza->save();
                    unset($model_pipeline_compra_pieza);
                    
                    $model_solicitud_compra_pieza = SolicitudCompraPieza::find()->where(['id' => $despacho->id_solicitud_compra_pieza])->one();
                    $model_solicitud_compra_pieza->codigo_estado = 5;//Entregada
                    $model_solicitud_compra_pieza->update();
                    
                    $estado_0 = EstadoServicio::findOne(['codigo' => 4])->id;//En proceso
                    $model_pipeline = new PipelineServicio();
                    $model_pipeline->fecha =  date('Y-m-d');
                    $model_pipeline->id_servicio = $model_solicitud_compra_pieza->id_servicio;
                    $model_pipeline->id_estado_servicio = $estado_0;
                    $model_pipeline->save();                                        
    
                    \Yii::$app->getSession()->setFlash('success', '¡Piezas recibidas!');
                    return $this->redirect(['solicitud-compra-pieza/listado/']);
                } else{
                    \Yii::$app->getSession()->setFlash('error', '¡Error al generar la orden de compra!');
                    return $this->redirect(['solicitud-compra-pieza/piezas-recibidas/'. $id]);
                }
            }else {                
                \Yii::$app->getSession()->setFlash('success', '¡Piezas recibidas, aun quedan pendiente por recibir!');
                return $this->redirect(['solicitud-compra-pieza/piezas-recibidas/'. $id]);
            }
        } 
        
        return $this->render('recibidas-taller', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataProviderRecibidas' => $dataProviderRecibidas,
        ]);




    }

    /**
     * Creando envio
     * @autor Zulma Hernández
     * @param integer $id
     * @return mixed
     */
    public function actionCrearenvio( $id ){

        $model = $this->findModel($id);
        $model_despacho = Despacho::find()->where( ['id_solicitud_compra_pieza' => $model->id] )->select('numero')->max('numero');

        $numero = 1;
        if ( count($model_despacho) != 0 ) {
            $numero = $model_despacho + 1;
        }

        $model_despacho = new Despacho();
        $model_despacho->isNewRecord = true;
        $model_despacho->numero = $numero;
        $model_despacho->estado = 0; //En proceso
        $model_despacho->id_solicitud_compra_pieza = $model->id;
        $model_despacho->estado_recepcion_taller = 0;
        $model_despacho->id_taller = $model->idOrdenTrabajo->idCotizacionTaller->idSolicitudServicioTaller->id_taller;

        if ($model_despacho->save()) {
            return $this->redirect(['despachopiezas',
                'id' => $model_despacho->id,
            ]);
        } else{
            \Yii::$app->getSession()->setFlash('error', 'Error al crear el envío.');
            return $this->redirect(['despacho',
                'id' => $model->id,
            ]);
        }
    }


    public function actionDespachopiezas( $id ){
        $model_despacho = Despacho::findOne($id);
        $model_despacho->scenario = 'update';

        if ( Yii::$app->request->post() ) {
            if ( $model_despacho->load(Yii::$app->request->post()) && $model_despacho->validate() ) {
                $piezas = Yii::$app->request->post('selection');
                if ($piezas) {
                    $cantidades = explode(",", Yii::$app->request->post('cantidades_enviar'));
                
                    //Aqui creo que falta validar la cantidad para quitarla de pendiente por despacho
                    foreach ($piezas as $key => $pieza) {
                        $model_despacho_proveedor_pieza = new DespachoProveedorPieza();
                        $model_despacho_proveedor_pieza->isNewRecord = true;
                        $model_despacho_proveedor_pieza->cantidad_enviada_taller = $cantidades[$key];
                        $model_despacho_proveedor_pieza->id_despacho = $model_despacho->id;
                        $model_despacho_proveedor_pieza->id_cotizacion_proveedor_pieza = $pieza;
                        $model_despacho_proveedor_pieza->save();

                        $model_cotizacion_proveedor_pieza = CotizacionProveedorPieza::findOne($pieza);
                        $model_cotizacion_proveedor_pieza->estado_despacho = 1; //ENVIADO A MENSAJERO
                        $model_cotizacion_proveedor_pieza->save();
                    }

                    $model_despacho->fecha = date('Y-m-d');
                    $model_despacho->estado = 1; // ENVIADO A MENSAJERO
                    $model_despacho->traslado = 'TKM100-'.$model_despacho->id;

                    if ($model_despacho->save()) {
                        \Yii::$app->getSession()->setFlash('success', 'Envío guardado con éxito.');
                        return $this->redirect([ 'despacho', 'id' => $model_despacho->id_solicitud_compra_pieza ]);
                    } else{
                        \Yii::$app->getSession()->setFlash('error', 'Ocurrió un error guardando el envío, comuniquese con el administrador.');
                        return; 
                    }
                } else{
                    \Yii::$app->getSession()->setFlash('error', 'Debe seleccionar al menos una pieza a enviar');
                    return; 
                }
            } else{
                \Yii::$app->getSession()->setFlash('error', ($model_despacho->getErrors('id_mensajero'))?$model_despacho->getErrors('id_mensajero')[0]: 'Ocurrió un error guardando el envío, comuniquese con el administrador.');
                return;
            }
        } else {
            if ( $model_despacho->estado != 1 ) {

                $model = $this->findModel($model_despacho->id_solicitud_compra_pieza);

                $searchModel = new CotizacionProveedorPiezaSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                $dataProvider->query->joinWith('idCotizacionProveedor')->joinWith('idCotizacionProveedor.idSolicitudCompraProveedor')->where('solicitud_compra_proveedor.id_solicitud_compra_pieza = '. $model->id .' AND aprobacion = 2 AND estado_despacho = 0');
                
                return $this->render('despacho-piezas', [
                    'model' => $model,
                    'model_despacho' => $model_despacho,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]);
            } else{
                \Yii::$app->getSession()->setFlash('error', 'El envío ya se ha realizado.');
                return $this->redirect([ 'despacho', 'id' => $model_despacho->id ]); 
            }
        }
    }


    public function actionSolicitudpiezaestacion() {
        $model = new SolicitudCompraPieza();

        return $this->render('solicitud-pieza-estacion', [
            'model' => $model,
        ]);
    }

    /*
    * Devolución de Piezas a Proveedor
    * @author Zulma Hernández
    * @param integer $id cotizacion proveedor
    * @return mixed
    */
    public function actionDevolucion($id){

        if ( Yii::$app->request->post() ) {
            //Recibo los ids de piezas seleccionados
            $piezas = Yii::$app->request->post('selection');
            if ($piezas) {
                //Recibo las cantidades y los motivos de devolucion
                $cantidades = explode(",", Yii::$app->request->post('cantidades_devolucion'));
                $motivo_devolucion = explode(",", Yii::$app->request->post('motivo_devolucion'));

                foreach ($piezas as $key => $pieza) {
                    //Verifico si las piezas devueltas les fueron asignado un motivo
                    if ( isset($motivo_devolucion[$key]) && $motivo_devolucion[$key] != '' ) {

                        //Guardo la actualizacion de la pieza devuelta
                        $model_cotizacion_proveedor_pieza = CotizacionProveedorPieza::findOne($pieza);
                        $model_cotizacion_proveedor_pieza->cantidad_devolucion = $cantidades[$key];
                        $model_cotizacion_proveedor_pieza->id_usuario_devuelve = Yii::$app->user->identity->id;
                        $model_cotizacion_proveedor_pieza->fecha_devolucion = date('Y-m-d');
                        $model_cotizacion_proveedor_pieza->id_motivo_devolucion = $motivo_devolucion[$key];
                        $model_cotizacion_proveedor_pieza->save();
                    } else{
                        \Yii::$app->getSession()->setFlash('error', 'Debe indicar el motivo de dovolución de las piezas seleccionadas.');
                        return; 
                    }
                }

                \Yii::$app->getSession()->setFlash('success', 'Se guardó la información de las piezas devueltas con Éxito!');
                return $this->redirect(['index']);
            } else{
                \Yii::$app->getSession()->setFlash('error', 'Debe seleccionar al menos una pieza a enviar');
                return; 
            }

        } else{
            $model_cotizacion_proveedor = CotizacionProveedor::findOne($id);
            $model_orden_compra = OrdenCompraPieza::find()->where([ 'id_cotizacion_proveedor' => $model_cotizacion_proveedor->id ])->one();

            $model = $this->findModel($model_orden_compra->id_solicitud_compra_pieza);
            $model_servicio = Servicio::findOne($model->id_servicio);
            $model_vehiculo = Vehiculo::findOne($model_servicio->id_vehiculo);
            $model_proveedor = Proveedor::findOne($model_orden_compra->id_proveedor);

            $searchModel = new CotizacionProveedorPiezaSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->where('id_cotizacion_proveedor = '. $model_cotizacion_proveedor->id .' AND aprobacion = 2');

            $motivo_devolucion = ArrayHelper::map(MotivoDevolucion::find()->all(), 'id', 'nombre');

            return $this->render('devolucion',[
                'model_cotizacion_proveedor' => $model_cotizacion_proveedor,
                'model_orden_compra' => $model_orden_compra,
                'model_servicio' => $model_servicio,
                'model_vehiculo' => $model_vehiculo,
                'model_proveedor' => $model_proveedor,
                'dataProvider' => $dataProvider,
                'motivo_devolucion' => $motivo_devolucion,
            ]);
        }
 
    }

    /*
    * Index Devolución de Piezas a Proveedor - Logistica
    * @author Zulma Hernández
    * @return mixed
    */
    public function actionIndexdevolucion(){
        $searchModel = new OrdenCompraPiezaSearch();
        $dataProvider = $searchModel->searchDevoluciones(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy('id DESC');

        $proveedores = ArrayHelper::map(Proveedor::find()->orderBy('nombre')->all(), 'id', 'nombre');
        //$dataProvider->query->where('id_cotizacion_proveedor = '. $model_cotizacion_proveedor->id .' AND aprobacion = 2');
        /*echo "<pre>";
        print_r($dataProvider->getModels());
        echo "</pre>";*/
        return $this->render('index-devolucion',[
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'proveedores' => $proveedores,
        ]);
    }

    /*
    * Index Devolución de Piezas a Proveedor - Vista del proveedor
    * @author Zulma Hernández
    * @return mixed
    */
    public function actionIndexdevolucionproveedor(){

        if (\Yii::$app->user->can('proveedor')) {

            $id_proveedor = User::findOne(Yii::$app->user->identity->id)->id_proveedor;
            $searchModel = new OrdenCompraPiezaSearch(['id_proveedor' => $id_proveedor]);
            $dataProvider = $searchModel->searchDevoluciones(Yii::$app->request->queryParams);

            $proveedores = ArrayHelper::map(Proveedor::find()->orderBy('nombre')->all(), 'id', 'nombre');
            
            return $this->render('index-devolucion-proveedor',[
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'proveedores' => $proveedores,
            ]);
        } else{
            return;
        }
    }

    /*
    * View Detalle Devolución de Piezas a Proveedor - Vista del proveedor
    * @author Zulma Hernández
    * @return mixed
    */
    public function actionViewdevolucionproveedor($id){

        if (\Yii::$app->user->can('proveedor')) {

            $model_orden_compra = OrdenCompraPieza::findOne($id);

            $model = $this->findModel($model_orden_compra->id_solicitud_compra_pieza);
            $model_servicio = Servicio::findOne($model->id_servicio);
            $model_vehiculo = Vehiculo::findOne($model_servicio->id_vehiculo);

            $searchModel = new CotizacionProveedorPiezaSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->where('id_cotizacion_proveedor = '. $model_orden_compra->id_cotizacion_proveedor .' AND aprobacion = 2 AND cantidad_devolucion IS NOT NULL');

            $motivo_devolucion = ArrayHelper::map(MotivoDevolucion::find()->all(), 'id', 'nombre');

            return $this->render('view-devolucion-proveedor',[
                'model_orden_compra' => $model_orden_compra,
                'model_servicio' => $model_servicio,
                'model_vehiculo' => $model_vehiculo,
                'dataProvider' => $dataProvider,
                'motivo_devolucion' => $motivo_devolucion,
            ]);

        } else{
            return;
        }

    }

    /*
    * PDF Translado de piezas - Logistica
    * @author Albert Vargas
    */
    public function actionImprimirtranslado($id) {

        $model_despacho = Despacho::findOne($id);
        $searchModel = new DespachoProveedorPiezaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$id);
        $model = $this->findModel($model_despacho->id_solicitud_compra_pieza);
        $model_servicio = Servicio::findOne( $model->id_servicio );
        $model_vehiculo = Vehiculo::findOne( $model_servicio->id_vehiculo );
        $model_orden_compra = OrdenCompraPieza::findOne($model_despacho->id_solicitud_compra_pieza);

        $pdf = new Pdf([
            'content' => $this->renderPartial('_transladopiezaspdf', ['dataProvider' => $dataProvider, 'model' => $model, 'model_vehiculo' => $model_vehiculo, 'model_orden_compra' => $model_orden_compra]),
            'destination' => Pdf::DEST_BROWSER,
            'mode' => Pdf::MODE_CORE,
            'format' => Pdf::FORMAT_A4,
            'filename' => 'TRANSLADO_'.$id.'.pdf',
            'options' => [
                'title' => 'TRANSLADO DE PIEZAS',
                'subject' => 'TRANSLADO_'.$id,
            ],
        ]);

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');

        return $pdf->render(); 
        exit;

    }

    /**
     * Index de la solicitud de ordenes de compra de taller
     * @autor Zulma Hernández / @ajustes Albert Vargas 
     */
    public function actionIndexordencompra()
    {
        $model_orden_compra_factura = new OrdenCompraFactura();

        $searchModel = new OrdenCompraPiezaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy('id DESC');
        
        return $this->render('index-orden-compra', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'model_orden_compra_factura' => $model_orden_compra_factura,
        ]);
    }

    /**
     * Guardar Imagen Piezas Analista
     * @autor Albert Vargas
     */
    public function actionGuardarimagenpieza()
    {
        if (Yii::$app->request->post()) {
            $count = count(Yii::$app->request->post('id'));
            $id = Yii::$app->request->post('id');
            $observacion = Yii::$app->request->post('observacion_pieza');

            for ($i = 0; $i < $count; $i++) {
                $rutabd = '';
                if($_FILES['CotizacionTallerPieza']['name']['imageFiles'][$i] != NULL) {
                    $nom_img  = str_replace(' ', '', $_FILES['CotizacionTallerPieza']['name']['imageFiles'][$i]);
                    $ruta = './uploads/imagen-piezas/pieza-'.$id[$i].'-'.$nom_img;
                    $rutabd = '/uploads/imagen-piezas/pieza-'.$id[$i].'-'.$nom_img;
                }

                $model_cotizacion_taller_pieza = CotizacionTallerPieza::find()->where([ 'id' => $id[$i] ])->one();
                $model_cotizacion_taller_pieza->observacion_pieza = $observacion[$i];

                if($_FILES['CotizacionTallerPieza']['name']['imageFiles'][$i] != NULL) {
                    $model_cotizacion_taller_pieza->imagen_pieza = $rutabd;
                }

                if($model_cotizacion_taller_pieza->save()) {
                    if($_FILES['CotizacionTallerPieza']['name']['imageFiles'][$i] != NULL) {
                        move_uploaded_file($_FILES['CotizacionTallerPieza']['tmp_name']['imageFiles'][$i], $ruta);
                    }
                }   
               

            }

            if ( $model_cotizacion_taller_pieza->save() ) {
                \Yii::$app->getSession()->setFlash('success', '¡La información se ha guardado con éxito!');
            } else{
                \Yii::$app->getSession()->setFlash('error', '¡Error al guardar la información!');
            }

            $this->redirect('index');
        }
    }

    /**
     * Guardar Imagen Piezas Proveeedor
     * @autor Albert Vargas
     */
    public function actionGuardarimagenpiezaproveedor()
    {
        if (Yii::$app->request->post()) {
            $count = count(Yii::$app->request->post('id'));
            $id = Yii::$app->request->post('id');
            $observacion = Yii::$app->request->post('observacion_pieza');

            for ($i = 0; $i < $count; $i++) {
                $rutabd = '';
                if($_FILES['CotizacionProveedorPieza']['name']['imageFiles'][$i] != NULL) {
                    $nom_img  = str_replace(' ', '', $_FILES['CotizacionProveedorPieza']['name']['imageFiles'][$i]);
                    $ruta = './uploads/imagen-piezas-proveedor/pieza-'.$id[$i].'-'.$nom_img;
                    $rutabd = '/uploads/imagen-piezas-proveedor/pieza-'.$id[$i].'-'.$nom_img;
                }

                $model_cotizacion_taller_pieza = CotizacionProveedorPieza::find()->where([ 'id' => $id[$i] ])->one();
                $model_cotizacion_taller_pieza->observacion_pieza = $observacion[$i];

                if($_FILES['CotizacionProveedorPieza']['name']['imageFiles'][$i] != NULL) {
                    $model_cotizacion_taller_pieza->imagen_pieza = $rutabd;
                }

                if($model_cotizacion_taller_pieza->save()) {
                    if($_FILES['CotizacionProveedorPieza']['name']['imageFiles'][$i] != NULL) {
                        move_uploaded_file($_FILES['CotizacionProveedorPieza']['tmp_name']['imageFiles'][$i], $ruta);
                    }
                }   
               

            }

            if ( $model_cotizacion_taller_pieza->save() ) {
                \Yii::$app->getSession()->setFlash('success', '¡La información se ha guardado con éxito!');
            } else{
                \Yii::$app->getSession()->setFlash('error', '¡Error al guardar la información!');
            }

            $this->redirect(Yii::$app->request->referrer);
        }
    }
        
    /**
     * Historico pieza
     * @autor Albert Vargas
     */

    public function actionHistoricopieza($id) {
        $searchModel = new CotizacionProveedorPiezaSearch(['id_pieza' => $id]);
        $dataProvider = $searchModel->searchHistoricoPieza(Yii::$app->request->queryParams);
        $pieza = Pieza::findOne($id);
        
        return $this->render('historico-pieza', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'pieza' => $pieza,
        ]);
    }

    /*
    * Descarga de archivos del servidor
    * @autor Zulma Hernández
    */
    /*public function actionConexion(){
        //set_time_limit(300);


        $ftp_server = "186.150.207.206";
        $ftp_user = "avisrd";
        $ftp_pass = "123456A@";

        // set up a connection or die
        $conn_id = @ftp_ssl_connect($ftp_server, 22) or die("Couldn't connect to $ftp_server"); 

        // try to login
        if (@ftp_login($conn_id, $ftp_user, $ftp_pass)) {
            ftp_pasv($conn_id, true);
            $local_file = Yii::$app->basePath .'/files/FDO20170824.TXT';

            if (ftp_chdir($conn_id, "/AVIS")) {
                     
                $server_file =  './FDO20170824.TXT';

                $arch_file = fopen($local_file, 'w');


                if (ftp_get($conn_id, $local_file, $server_file, FTP_ASCII, 0)) {
                    echo "Se ha guardado satisfactoriamente en $local_file\n";
                } else {
                    echo "Ha habido un problema\n";
                }
            } else{
                echo "No entro en AVIS";
            }
        } else {
            echo "Couldn't connect as $ftp_user\n";
        }

        // close the connection
        ftp_close($conn_id);  
        return $this->render('conexion');
    }*/
}
