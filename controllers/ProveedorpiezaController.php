<?php

namespace app\controllers;

use Yii;
use app\models\proveedorpiezaSearch;
use app\models\MarcaProveedorsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;

use app\models\Model;
use app\models\Pieza;
use app\models\TipoActividad;
use app\models\Marca;
use app\models\Modelo;

use app\models\ProveedorPieza;
use app\models\ModeloPieza;
use app\models\Proveedor;
use app\models\TipoEmpresa;
use app\models\TipoPieza;
use app\models\MarcaInsumo;
use app\models\UserSearch;
use app\models\AuthAssignment;
use app\models\User;
use dektrium\user\helpers\Password;
use app\models\MarcaProveedor;
/**
 * ProveedorpiezaController implements the CRUD actions for Proveedorpieza model.
 */
class ProveedorpiezaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
        'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
        'delete' => ['POST'],
        ],
        ],
        ];
    }

    /**
     * Lists all Proveedorpieza models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProveedorpiezaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            ]);
    }

    /**
     * Displays a single Proveedorpieza model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)

    { 


      $elproveedor = Proveedor::findOne($id);
      $empresatipo = TipoEmpresa::findOne($elproveedor->tipo_empresa_id);
      $modelopiezas = new ModeloPieza();
      $searchModel = new MarcaProveedorsSearch([ 'id_proveedor' => $id ]);
      $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
      $dataProvider->query->orderBy('id DESC');


      return $this->render('view', [
        'model' => $elproveedor,
        'elproveedor' => $elproveedor,
        'empresatipo' => $empresatipo,
        'modelopiezas'=>$modelopiezas,
        'dataProvider'=>$dataProvider,
        'searchModel'=>$searchModel,



        ]);



  }


  
  public function actionFicha($id)

  { 
      $elproveedor = Proveedor::findOne($id);
      $empresatipo = TipoEmpresa::findOne($elproveedor->tipo_empresa_id);

      return $this->render('Ficha', [
        'model' => $elproveedor,
        'elproveedor' => $elproveedor,
        'empresatipo' => $empresatipo,
        ]);
  }

    /**
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     *Creacion de provveedor
     */
    public function actionCreate()
    {

        $modelprov = new Proveedor();
        // $modelsPiezas= [new Proveedorpieza];
        $modelsPiezas= [new MarcaProveedor];
        

        $usuario = new UserSearch();
        $rol = new AuthAssignment();
        $length = 10;
        $chars = array_merge(range(0,9), range('a','z'), range('A','Z'));
        shuffle($chars);
        $password = implode(array_slice($chars, 0, $length));


        if ($modelprov->load(Yii::$app->request->post()) && $modelprov->validate() ) {
 
            $modelsPiezas = Model::createMultiple(MarcaProveedor::classname());
            Model::loadMultiple($modelsPiezas, Yii::$app->request->post());
 
 $usu  = AuthAssignment::findOne(['user_id'=>Yii::$app->user->identity->id]);
            if ( $modelprov->save(false)) {
                //Guardar codigo proveedor
                $model2 = Proveedor::findOne($modelprov->id);
                $codigo_proveedor = 'SUP-0' . $modelprov->id;           
                $model2->codigo_proveedor = $codigo_proveedor;
                $model2->update(false);
                
              if ($usu->item_name!='proveedor') {
                 $usuario->username = $modelprov->user;
                $usuario->email=$modelprov->correo_representante;
                $usuario->password_hash = Password::hash($password);
                $usuario->auth_key=0;
                $usuario->created_at=0;
                $usuario->updated_at=0;
                $usuario->flags=0;
                $usuario->cargo_gerencial=0;
                $usuario->cargo=0;
                $usuario->id_proveedor = $modelprov->id;
                $usuario->confirmed_at = 1491044212;
                $usuario->save(false);

                $rol->item_name = 'proveedor';
                $rol->user_id = $usuario->id;
                $rol->save(false);

                $correo_envio = $modelprov->correo_representante;
                $login =  $usuario->username;
                if(!$this->notificacion($modelprov,$password, $usuario)){
                        \Yii::$app->getSession()->setFlash('error', '¡Error en el envío de notificación!');
                    }
              }
              if ($usu->item_name=='proveedor') {
                $usus  = UserSearch::findOne(['id'=>Yii::$app->user->identity->id]);
                $usus->id_proveedor = $modelprov->id;
                $usus->save(false);
              }
               

       
             //Asociar Marcas de Vehiculos
            if (isset($_POST['MarcaProveedor'][0]['id_marca'])) {
                foreach ($_POST['MarcaProveedor'] as $tall) {
                    $modelmarcaprov = new MarcaProveedor();
                    $modelmarcaprov->id_marca = $tall['id_marca'];
                    $modelmarcaprov->id_proveedor = $modelprov->id;
                    if(!$modelmarcaprov->save(false)){
                      $band = 1;
                    }
                }
            }
          if ($usu->item_name!='proveedor') {
      
          return $this->redirect(['proveedor/index']);
          }else{
            return $this->redirect(['proveedorpieza/view','id'=>$modelprov->id]);
          }

            }

        } else {
            return $this->render('create', [
                'modelprov' => $modelprov,
                'modelsPiezas' => (empty($modelsPiezas)) ? [new MarcaProveedor] : $modelsPiezas,
          


                ]);
        }
    }





       /**
     * Updates an existing MarcaProveedor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {


 

       $modelprov = Proveedor::findOne($id);
       $usu  = AuthAssignment::findOne(['user_id'=>Yii::$app->user->identity->id]);
        $modelsPiezas = MarcaProveedor::find()
            ->joinWith('idProveedor')
            ->where([ 'marca_proveedor.id_proveedor' => $id])
            ->all();

        if ($modelprov->load(Yii::$app->request->post()) && $modelprov->save(false)) {
            
             $band = 0;
  

          //Asociar Modelos de Vehiculos
          $provpieza = MarcaProveedor::find()->where(['id_proveedor'=>$id])->all();
       
         
          foreach ($provpieza as $asig) {
            $asig->delete(false);
          }
 

              if (isset($_POST['MarcaProveedor'][0]['id_marca'])) {
           
              $i = 0;
                foreach ($_POST['MarcaProveedor'] as $piezas) {
                    $ppieza = new MarcaProveedor();
                    $ppieza->id_marca = $piezas['id_marca'];; 
                    $ppieza->id_proveedor = $id;
                    if ($ppieza->id_marca!='') {
                   $ppieza->save(false);
                     $band = 1;
                       }

   
                  }
              }
              
             if ($usu->item_name!='proveedor') {
       \Yii::$app->getSession()->setFlash('success', '¡Proveedor modificado con exito!');
          return $this->redirect(['proveedor/index']);
          }else{
             \Yii::$app->getSession()->setFlash('success', '¡Proveedor modificado con exito!');
            return $this->redirect(['proveedorpieza/view','id'=>$modelprov->id]);
          }
       
         


        } else {
            return $this->render('update', [
                'modelprov' => $modelprov,
                'modelsPiezas' => (empty($modelsPiezas)) ? [new MarcaProveedor] : $modelsPiezas,
                //'modelservicio' => (empty($modelservicio)) ? [new ServicioTaller] : $modelservicio,
                
            ]);
        }










    }
    public function notificacion($model,$password, $usuario)
    {
        $envio = 0;
        $mensaje_sup ='  <p> Hola '.$model->nombre.'<br>
                        Km100 le informa que se ha realizado exitosamente el Registro de su cuenta de usuario.
                    </p>
                    <p>
                        <strong>Nombre de Usuario: </strong>'.$usuario->username.'<br>
                        <strong>Clave de Acceso: </strong>'.$password.'<br>
                    </p>
        ';
        if (!Yii::$app->mailer->compose(['html'=>'layout'], ['content'=> $mensaje_sup])
                        ->setFrom([Yii::$app->params['adminEmail'] => 'KM100'])
                        ->setTo($model->correo_representante)
                        ->setSubject('Notificación Registro de Proveedor')
                        ->send())
        {
            $envio=0;
        }
     

        if ($envio==0){
            return true;
        }else{
            return false;
        }
    }

    public function actionListarmodelos($id)
    {

        if (\Yii::$app->request->isAjax ) {
            $query = new Query();
            $query->select('*')
            ->from('modelo')
            ->where(['=','id_marca', $id])
            ->orderBy('nombre');
            $rows = $query->all();

            if (count($rows) > 0) {
                echo "<option value=''> Seleccione... </option>";
                foreach ($rows as $aux) {
                    echo "<option value='".$aux['id_modelo']."'>" . $aux['nombre'] ."</option>";
                }
            } else {
                echo "<option> -- </option>";
            }
        }
        \Yii::$app->end();
    }

 

    /**
     * Deletes an existing Proveedorpieza model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Proveedorpieza model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Proveedorpieza the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Proveedorpieza::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
