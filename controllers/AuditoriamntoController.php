<?php

namespace app\controllers;

use app\models\AuditoriaMnto;
use app\models\AuditoriamntoSearch;
use Yii;
use app\models\Auditoriafs;
use app\models\Estacion;
use app\models\SolAudMnto;
use app\models\OtrasMnto;
use app\models\Servicio;
use app\models\FlotaSearch;
use app\models\AddFs;
use app\models\AddMnto;
use app\models\Vehiculo;
use app\models\PipelineServicio;
use app\models\AuditoriaGeneral;
use app\models\PipelineServicioSearch;
use app\models\OrdenCompraPiezaSearch;
use app\models\AuditoriafsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\EstacionesSearch;
/**
 * AuditoriamntoController implements the CRUD actions for Auditoriamnto model.
 */
class AuditoriamntoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

     public function actionAuditoriamnto($id=null,$mensual)
    {


          $model = new Auditoriamnto();
          $model_est = Estacion::findOne($id);
          $modelsSolicitudes = [new SolAudMnto];
          $modelsotras = [new OtrasMnto];
          $modelAud = new AuditoriaGeneral();
 
        if ($model->load(Yii::$app->request->post())) {
  
          $model->id_estacion = $id;
         
          $model->mntos_realizados = $_POST['AuditoriaMnto']['mntos_realizados'];
          $model->mntos_smna_anterior = $_POST['AuditoriaMnto']['mntos_smna_anterior']; 
          $model->mntos_smna_anterior = $_POST['AuditoriaMnto']['mntos_smna_anterior']; 
          if ($_POST['AuditoriaMnto']['mntos_realizados']) {
             $model->desviacion = $_POST['AuditoriaMnto']['mntos_smna_anterior'] - $_POST['AuditoriaMnto']['mntos_realizados'];
          }else{
            $model->desviacion = $_POST['AuditoriaMnto']['mntos_smna_anterior'] - 0;

          }
           $model->estado =1;
            $model->mensual = $mensual;
          

          
           $model->comentario =$model->id_estacion;
   

          $model->fecha = date('Y-m-d');

           if ($model->save()) {

                $modelAud->fecha = date('Y-m-d');
                $modelAud->estado = 2;
                $modelAud->id_estacion = $id;
                $modelAud->id_aud_mnto = $model->id;
                $modelAud->save(false);

        $fecha_actual = date('Y-m-d');
        $lunes = date('Y-m-d', strtotime('last Monday'));
        $lunes_anterior = date('Y-m-d', strtotime('last Monday -7 days'));

        $query = Vehiculo::find()
                ->join('JOIN','proyeccion_mantenimiento','proyeccion_mantenimiento.id_vehiculo = vehiculo.id')
                ->where(['proyeccion_mantenimiento.id_estacion_reporte' => $model_est->id_estacion])
                ->andWhere(['proyeccion_mantenimiento.realizado' => 0])
                ->andWhere(['between', 'proyeccion_mantenimiento.fecha_reporte', $lunes_anterior, $lunes ]);
                 
                 $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                $vehiculos = Yii::$app->db->createCommand($sql)->queryOne();
                

             
              // 4) Guardando las del gridview
                  $observacion = Yii::$app->request->post('observacion');
              if ($observacion) {
                  # code...
                
                      foreach ($observacion as $key => $obs) {

                    $f_ser = new AddMnto();
                    $f_ser->isNewRecord = true;
                    $f_ser->observacion = $obs;      
          
                  
                    $f_ser->ficha = $vehiculos['mva'];
                 
                    $f_ser->id_aud_mnto = $model->id;
                    
                    $f_ser->id_modelo = $vehiculos['id_modelo']; 
                    
                    $f_ser->save(false);
                    unset($f_ser);
              }
                }

                if (isset($_POST['SolAudMnto'][0]['ficha_mva'])) {
                  foreach ($_POST['SolAudMnto'] as $numeros) {
                      $sol = new SolAudMnto();
                    
                      $sol->ficha_mva = $numeros['ficha_mva'];
                      $sol->id_modelo = $numeros['id_modelo'];
                      $sol->km_actual =$numeros['km_actual'];
                      $sol->id_tipo_mnto =$numeros['id_tipo_mnto'];
                      $sol->observacion =$numeros['observacion'];
                      $sol->id_aud_mnto = $model->id;
                      $sol->save(false);
                  }
                  // \Yii::$app->getSession()->setFlash('success', '¡El Rubro ha sido cargado con éxito!');
              }
                 if (isset($_POST['OtrasMnto'][0]['ficha_mva'])) {
                  foreach ($_POST['OtrasMnto'] as $numeros) {
                      $sol2 = new OtrasMnto();
                    
                      $sol2->ficha_mva = $numeros['ficha_mva'];
                      $sol2->km_actual =$numeros['km_actual'];
                      $sol2->km_proximo =$numeros['km_proximo'];
                      $sol2->observacion =$numeros['observacion'];
                      $sol2->id_aud_mnto = $model->id;
                      $sol2->save(false);
                  }
                  // \Yii::$app->getSession()->setFlash('success', '¡El Rubro ha sido cargado con éxito!');
              }

               // 4) Guardando las del gridview
            
           \Yii::$app->getSession()->setFlash('success', '¡La Auditoria ha sido cargado con éxito, por favor continue el proceso de auditoria!');
           }

            return $this->redirect('/km100/auditoriageneral/index'); 
        } else {

         $searchModel = new FlotaSearch();
         $dataProvider = $searchModel->searchauditoria(Yii::$app->request->queryParams, $model_est->id_estacion);
          return $this->render('auditoriamnto',[
                 'searchModel' => $searchModel,
                 'dataProvider' => $dataProvider,
                 'model' => $model,
                 'model_est' => $model_est,
                 'modelsSolicitudes' => (empty($modelsSolicitudes)) ? [new SolAudMnto] : $modelsSolicitudes,
                 'modelsotras' => (empty($modelsotras)) ? [new OtrasMnto] : $modelsotras,

                  
            ]);
        }



 

 

        
     

    }
 
    /**
     * Lists all Auditoriafs models.
     * @return mixed
     */
    public function actionIndexmnto()
    {

        $searchModel = new estacionesSearch();
        $dataProvider = $searchModel->searchmnto(Yii::$app->request->queryParams);

        return $this->render('indexmnto', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Lists all Auditoriamnto models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AuditoriamntoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Auditoriamnto model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Auditoriamnto model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Auditoriamnto();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Auditoriamnto model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Auditoriamnto model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Auditoriamnto model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Auditoriamnto the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Auditoriamnto::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
