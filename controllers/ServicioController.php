<?php

namespace app\controllers;

use Yii;
use yii\helpers\Json;
use yii\helpers\BaseFileHelper;
use yii\helpers\ArrayHelper;
use yii\db\Query;
use app\models\Servicio;
use app\models\ServicioSearch;
use app\models\Vehiculo;
use app\models\EstadoVehiculo;
use app\models\Estado;
use app\models\VehiculoSearch;
use app\models\Marca;
use app\models\Modelo;
use app\models\Caso;
use app\models\CasoServicio;
use app\models\ServicioTaller;
use app\models\PipelineServicio;
use app\models\EstadoServicio;
use app\models\Empresa;
use app\models\ServicioVehiculoEscaneadoSearch;
use app\models\ServicioVehiculoEscaneado;
use app\models\CasoServicioSearch;
use app\models\TallerSearch;
use app\models\Taller;
use app\models\User;
use app\models\Pieza;
use app\models\SolicitudServicioTaller;
use app\models\SolicitudServicioTallerSearch;
use app\models\CotizacionTaller;
use app\models\CotizacionTallerSearch;
use app\models\CotizacionTallerAnalista;
use app\models\CotizacionTallerPieza;
use app\models\CotizacionTallerPiezaSearch;
use app\models\CotizacionTallerActividad;
use app\models\CotizacionTallerActividadSearch;
use app\models\UploadCotizacionTaller;
use app\models\OrdenTrabajo;
use app\models\OrdenTrabajoSearch;
use app\models\OrdenCompraPiezaSearch;
use app\models\SolicitudCompraPieza;
use app\models\EstadoCompraPieza;
use app\models\PipelineCompraPieza;
use yii\web\UploadedFile;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\UploadActaPolicial;
use app\models\UploadReporteAccidente;
use app\models\ChecklistAvaluo;
use app\models\Variable;
use app\models\ServicioEncuesta;
use app\models\ChecklistAvaluoPieza;
use app\models\ChecklistavaluoPiezasSearch;
use app\models\PiezaServicioTaller;
use app\models\GarantiaVehiculoSearch;
use app\models\GarantiaVehiculo;
use app\models\ServicioMensaje;
use app\models\EstacionUsuario;
use kartik\mpdf\Pdf;

/**
 * ServicioController implements the CRUD actions for Servicio model.
 */
class ServicioController extends Controller
{
    /**
     * @inheritdoc
     */
    public $basePath ='./uploads/servicios/';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Servicio models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ServicioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy('id DESC');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Servicio model.
     * @param integer $id, $origen //$origen Saber de dónde vengo (Listado de solicitudes o Gestión de servicios)
     * @return mixed
     */
    public function actionView($id, $origen )
    {
        $model = $this->findModel($id);
        $model_vehiculo = Vehiculo::findOne([ 'id' => $model->id_vehiculo ]);
        
        $searchModel_caso = new CasoServicioSearch([ 'id_servicio' => $model->id ]);
        $dataProvider_caso = $searchModel_caso->search(Yii::$app->request->queryParams);
        
        return $this->render('view', [
            'model' => $model,
            'model_vehiculo' => $model_vehiculo,
            'dataProvider_caso' => $dataProvider_caso,
            'origen' => $origen
        ]);
    }

    /**
     * Creates a new Servicio model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Servicio();
        $model_caso = new Caso();
        $acta = new UploadActaPolicial();
        $reporte = new UploadReporteAccidente();

        $cliente = (\Yii::$app->user->can('registro servicio todos') )?'':EstacionUsuario::find()->where(['user_id'=>Yii::$app->user->identity->ID ])->one()->estacion->idEmpresa->cliente;
        $empresa = (\Yii::$app->user->can('registro servicio todos'))?'':EstacionUsuario::find()->where(['user_id'=>Yii::$app->user->identity->ID ])->one()->estacion->idEmpresa;
        $estaciones = (\Yii::$app->user->can('registro servicio estaciones asociadas'))?ArrayHelper::getColumn(EstacionUsuario::find()->select('estacion_id')->where(['user_id'=>Yii::$app->user->identity->ID ])->asArray()->all(), 'estacion_id'):'';

        if ( \Yii::$app->user->can('registro servicio todos') ) {
            $vehiculos = ArrayHelper::map(Caso::find()->select(['ficha'])->distinct()->all(), 'ficha', 'ficha');
        } else if ( \Yii::$app->user->can('registro servicio cliente') ) {
            $v_estaciones = Estacion::find()->joinWith('idEmpresa')->where([ 'empresa.cliente_id' => $cliente ])->all();
            $ids_estaciones = ArrayHelper::getColumn($v_estaciones, 'id_estacion');
            $vehiculos = ArrayHelper::map(Caso::find()->select(['ficha'])->where(['id_estacion' => $ids_estaciones ])->distinct()->all(), 'ficha', 'ficha');
        } else if ( \Yii::$app->user->can('registro servicio estaciones asociadas') ) {
            $v_estaciones = EstacionUsuario::find()->where(['user_id'=>Yii::$app->user->identity->ID ])->all();
            $ids_estaciones = ArrayHelper::getColumn($v_estaciones, 'estacion_id');
            $vehiculos = ArrayHelper::map(Caso::find()->select(['ficha'])->where(['id_estacion' => $ids_estaciones ])->distinct()->all(), 'ficha', 'ficha');
        }
        if ( $model->load(Yii::$app->request->post()) ) {
            $servicios ="";

            if ( Yii::$app->request->post('tipo_servicio') ) {
                $checkboxes = Yii::$app->request->post('tipo_servicio');
                foreach($checkboxes as $c)
                    $servicios = $servicios .'*'.$c;  
            } else{
                \Yii::$app->getSession()->setFlash('error', 'Debe seleccionar los Servicios a Solicitar!');
                return $this->render('create', [
                    'model' => $model,
                    'model_caso' => $model_caso,
                ]);
            }

            $model->servicio = $servicios;
            $model->fecha = date('Y-m-d');
            $model->decision = 0;
            $model->linea = 0;
            $model->img_reporte = '';
            $model->aprobacion_estacion = -1;
            $model->registrado_por = Yii::$app->user->identity->id;

            if ( $model->save() ) {
                $path = $this->basePath . $model->id . '/';
                if ( !is_dir($path) ) {
                    mkdir( $path, 0777, true );
                }

                $casos = Yii::$app->request->post('Caso');

                //Guardar reporte
                $reporte->imageFiles = UploadedFile::getInstances($reporte, 'imageFiles');
                $reporte->upload($model->id);

                //Guardar acta policial
                $acta->imageFiles = UploadedFile::getInstances($acta, 'imageFiles');
                $acta->upload($model->id);

                $id_casos = '';
                foreach ($casos['id_caso'] as $key => $caso) {
                    $model_caso_servicio = new CasoServicio();
                    $model_caso_servicio->isNewRecord = true;
                    $model_caso_servicio->id_servicio = $model->id;
                    $model_caso_servicio->id_caso = $caso;
                    $model_caso_servicio->save();
                    unset($model_caso_servicio);
                    $id_casos = $id_casos.$caso.' ';
                }

                /* Pipeline -> Estado inicial Asignación */
                $estado_0 = EstadoServicio::findOne(['codigo' => 11])->id;
                $model_pipeline = new PipelineServicio();
                $model_pipeline->fecha = $model->fecha = date('Y-m-d');
                $model_pipeline->id_servicio = $model->id;
                $model_pipeline->id_estado_servicio = $estado_0;
                $model_pipeline->save();
                unset($model_pipeline);

            }else {
                print_r($model->getErrors());
                die();
            }

            $model_vehiculo = Vehiculo::findOne($model->id_vehiculo);

            try {
                //Email a quien registro la solicitud
                $mensaje_sup ='  <p>'.Yii::$app->user->identity->username.', <br>
                        Km100 le informa que la Solicitud de Servicio ha sido creada con éxito:
                        </p>
                        <p>
                            <strong>Nº Solicitud: </strong>'.$model->id.'<br>
                            <strong>MVA Ficha: </strong>'.$model_vehiculo->mva.'<br>
                            <strong>Nº Placa: </strong>'.$model_vehiculo->placa.'<br>
                            <strong>KM: </strong>'.$model_vehiculo->km.'<br>
                            <strong>Casos asociados: </strong>'.$id_casos.'<br>
                        </p>
                ';
                Yii::$app->mailer->compose(['html'=>'layout'], ['content'=> $mensaje_sup])
                    ->setFrom([Yii::$app->params['adminEmail'] => 'KM100'])
                    ->setTo(Yii::$app->user->identity->email)
                    ->setSubject('Registro de Solicitud de Servicio')
                    ->send();

                //Email al Planner
                $planners = User::find()->joinWith('authAssignments')->andWhere('auth_assignment.item_name = "planner"')->all();
                
                foreach ($planners as $user) {
                    $correos[] = $user->email;
                }

                $mensaje_sup ='  <p> Km100 le informa que una nueva Solicitud de Servicio ha sido registrada: </p>
                        <p>
                            <strong>Nº Solicitud: </strong>'.$model->id.'<br>
                            <strong>Empresa: </strong>'.$model->idEstacionActual->idEmpresa->nombre.'<br>
                            <strong>Estación: </strong>'.$model->idEstacionActual->nombre.'<br>
                            <strong>MVA Ficha: </strong>'.$model_vehiculo->mva.'<br>
                            <strong>Nº Placa: </strong>'.$model_vehiculo->placa.'<br>
                            <strong>KM: </strong>'.$model_vehiculo->km.'<br>
                            <strong>Casos asociados: </strong>'.$id_casos.'<br>
                        </p>
                ';
                Yii::$app->mailer->compose(['html'=>'layout'], ['content'=> $mensaje_sup])
                    ->setFrom([Yii::$app->params['adminEmail'] => 'KM100'])
                    ->setTo($correos)
                    ->setSubject('Nueva Solicitud de Servicio')
                    ->send();

            } catch (Exception $e) {
                \Yii::$app->getSession()->setFlash('error', 'Error al enviar el correo de notificacion');
            }

            \Yii::$app->getSession()->setFlash('success', 'Se registro la solicitud con éxito!');

            if ( $model->cobro_cliente == 0 ) {
                return $this->redirect(['index']);
            } else{
                return $this->redirect(['cobro-caso/create']);
            }
            
        } else {
            $acta->scenario = 'acta';
            $reporte->scenario = 'acta';
            return $this->render('create', [
                'model' => $model,
                'model_caso' => $model_caso,
                'acta' => $acta,
                'reporte'   => $reporte,
                'cliente' => $cliente,
                'empresa' => $empresa,
                'estaciones' => $estaciones,
                'vehiculos' => $vehiculos,
            ]);
        }
    }

    /**
     * Updates an existing Servicio model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model_caso = new Caso();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'model_caso' => $model_caso,
            ]);
        }
    }

    /**
     * Deletes an existing Servicio model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Servicio model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Servicio the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Servicio::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Obtener las placas de una ficha
     * @param integer $id
     * @return mixed
     */
    public function actionGetplacas()
    {

        $out = [];
        if (isset($_POST['depdrop_parents'])) {

            $id = end($_POST['depdrop_parents']);
            $list = Vehiculo::find()->andWhere(['mva'=>$id])->asArray()->all();
            $selected  = null;
            if ($id != null && count($list) > 0) {
                $selected = '';
                foreach ($list as $i => $vehiculo) {
                    $out[] = ['id' => $vehiculo['id'], 'name' => $vehiculo['placa']];
                    if ($i == 0) {
                        $selected = $vehiculo['id'];
                    }
                }
                // Shows how you can preselect a value
                echo Json::encode(['output' => $out, 'selected'=>$selected]);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected'=>'']);
    }
 
    /**
     * Obtener vehiculo
     * @param integer $id
     * @return mixed
     */
    public function actionGetvehiculo()
    {
        if (Yii::$app->request->post()) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $response = [];

            $id_vehiculo = Yii::$app->request->post('id_vehiculo');

            $model_vehiculo = Vehiculo::findOne($id_vehiculo);

            $response['modelo'] = $model_vehiculo->idModelo->nombre;
            $response['marca'] = Marca::findOne($model_vehiculo->idModelo->id_marca)->nombre;

            return $response;
        }
    }

    /**
     * Obtener los casos asociados a una placa
     * @param integer $id
     * @return mixed
     */
    public function actionGetcasos()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $id_vehiculo = $parents[0];
                $tipo_caso = $parents[1];
                $model_vehiculo = Vehiculo::findOne($id_vehiculo);
                $out = Caso::find()->select(['id' => 'id_caso','name' =>'id_caso' ])->where(['placa' => $model_vehiculo->placa, 'tipo_caso' => $tipo_caso ])->orderBy('id_caso')->asArray()->all();
                echo Json::encode(['output'=>$out, 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }

    /**
     * Listado de gestion de servicios.
     * @return mixed
     */
    public function actionGestionservicio($type = 0)
    {
        if ( $type == 0 ) {
            $searchModel = new ServicioSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->orderBy('id ASC');

            $model_cotizacion_taller_analista = new CotizacionTallerAnalista();

            $talleres = ArrayHelper::map(Taller::find()->orderBy('nombre')->all(), 'id_taller', 'nombre');

            return $this->render('gestion-servicio', [  'dataProvider' => $dataProvider, 
                                                        'model_cotizacion_taller_analista' => $model_cotizacion_taller_analista, 
                                                        'searchModel' => $searchModel,
                                                        'talleres' => $talleres,
                                                    ]);
        }
        else if ( $type == 1 ) {
            $searchModel = new ServicioVehiculoEscaneadoSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->orderBy('id ASC');

            return $this->render('_gestion-recepcion-vehiculo', [ 'dataProvider' => $dataProvider ]);
        }
        
    }
   
    /**
     * Escanear vehiculos con nueva solicitud de servicio
     * @return mixed
     */
    public function actionEscanearvehiculo()
    {

        if (Yii::$app->request->post()) {
            
            $codigo = Yii::$app->request->post('codigo');

            $model_vehiculo = Vehiculo::find()->where(['placa'=>$codigo])->one();

            $model = Servicio::find()->where(['id_vehiculo' => $model_vehiculo->id])->all();

            if ( $model ) {
                foreach ($model as $key => $servicio) {
                    $query = new query();
                    $query->select('es.id, es.codigo')
                        ->from('pipeline_servicio p')
                        ->join('INNER JOIN','estado_servicio es','es.id = p.id_estado_servicio')
                        ->where('p.id_servicio = '. $servicio->id )
                        ->orderBy('p.id desc');

                    $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                    $estado = Yii::$app->db->createCommand($sql)->queryOne();
                    
                    if ( $estado['codigo'] == 10 ) {
                        $escaneado = ServicioVehiculoEscaneado::find()->where([ 'id_servicio' => $servicio->id ])->all();
                        if (!$escaneado) {
                            $model_servicio_vehiculo_escaneado = new ServicioVehiculoEscaneado();
                            $model_servicio_vehiculo_escaneado->isNewRecord = true;
                            $model_servicio_vehiculo_escaneado->ficha = $servicio->ficha;
                            $model_servicio_vehiculo_escaneado->modelo = Modelo::findOne(['id_modelo' => $servicio->idVehiculo->id_modelo])->nombre;
                            $model_servicio_vehiculo_escaneado->empresa = Empresa::findOne(['id_empresa' => $servicio->idEstacionActual->id_empresa])->nombre_comercial;
                            $model_servicio_vehiculo_escaneado->tipo_solicitud = $servicio->servicio;
                            $model_servicio_vehiculo_escaneado->id_servicio = $servicio->id;
                            $model_servicio_vehiculo_escaneado->save();
                            unset($model_servicio_vehiculo_escaneado);
                        }
                    }
                }
                return 1; //Se cargaron los vehiculos
            } else {
                return 0; //El vehiculo no tiene ninguna solicitud de servicio asociado
            }

        }

    }

    /**
     * Eliminar una solicitud escaneda
     * @param integer $id
     * @return mixed
     */
    public function actionEliminarvehiculoescaneado($id)
    {
        $model_servicio_vehiculo_escaneado = ServicioVehiculoEscaneado::findOne($id)->delete();

        return $this->redirect('../gestionservicio?type=1');
    }

    /**
     * Cargar vehiculos escaneados
     * @return mixed
     */
    public function actionCargarvehiculoescaneado()
    {

        $model_servicio_vehiculo_escaneado = ServicioVehiculoEscaneado::find()->all();


        foreach ($model_servicio_vehiculo_escaneado as $vehiculo) {
            /* Pipeline -> Estado Asignar Taller */
            $estado_0 = EstadoServicio::findOne(['codigo' => 11])->id;
            $model_pipeline = new PipelineServicio();
            $model_pipeline->fecha = date('Y-m-d');
            $model_pipeline->id_servicio = $vehiculo->id_servicio;
            $model_pipeline->id_estado_servicio = $estado_0;

            if ($model_pipeline->save()) {
                $vehiculo->delete();
            }
            unset($model_pipeline);
        }

        return $this->redirect(['servicio/gestionservicio']);
    }

    /**
     * Asignar Solicitud a Taller
     * @return mixed
     */
    public function actionAsignartaller( $id )
    {       
        //print_r($_POST);
        //die();
        $model = $this->findModel($id);
        //$model_cotizacion_taller_analista = new CotizacionTallerAnalista();
        
        
        if(!empty($_POST['enviarx']) && $_POST['enviarx'] == 4) {
            //Motivo de cancelación
            $motivo = Yii::$app->request->post('Servicio');
            $model->observacion_cancelado = $motivo['observacion_cancelado'];
            $model->usuario_cancela = Yii::$app->user->identity->id;
            $model->update();

            /* Pipeline -> Estado Cancelado */
            $estado_0 = EstadoServicio::findOne(['codigo' => 12])->id;
            $model_pipeline = new PipelineServicio();
            $model_pipeline->fecha = date('Y-m-d');
            $model_pipeline->id_servicio = $model->id;
            $model_pipeline->id_estado_servicio = $estado_0;
            $model_pipeline->save();
            unset($model_pipeline);
            /*Mail*/
            try {
                $mensaje_sup ='  <p> Km100 Le informa que la solicitud de servicio fue cancelada:
                                </p>
                                <p>
                                    <strong>Servicio:</strong> Nº'.$model->id.'<br>
                                    <strong>MVA Ficha: </strong>'.$model->ficha.'<br>
                                    <strong>Nº Placa: </strong>'.$model->placa.'<br>
                                    <strong>Motivo de cancelación: </strong>'.$motivo['observacion_cancelado'].'
                                </p>
                ';

                Yii::$app->mailer->compose(['html'=>'layout'], ['content'=> $mensaje_sup])
                                ->setFrom([Yii::$app->params['adminEmail'] => 'KM100'])
                                ->setTo('zulmadessire@gmail.com')
                                ->setSubject('Solicitud de Servicio Cancelada')
                                ->send();
            } catch (Exception $e) {
                \Yii::$app->getSession()->setFlash('error', 'Error al enviar el correo de notificación');
            }
            /*Fin Mail*/
            
            \Yii::$app->getSession()->setFlash('success', 'Servicio Cancelado con exito!');
            return $this->redirect('../gestionservicio');
        }
        if(!empty($_POST['sucursal']))
        { 
            echo "Módulo de concesionarios <br> <br> <a href='".\Yii::$app->request->referrer."'> Regresar</a> ";
            die();            
        }else {
            if (!empty($_POST['id_taller'])) {

                $model->load(Yii::$app->request->post());
                $model->aprobacion_estacion = 0;
                $model->save();
        
                if (Yii::$app->request->post()) {
                    $id_taller = Yii::$app->request->post('id_taller');
                    $model_taller = Taller::findOne($id_taller);
        
                    $model_servicio_taller = new SolicitudServicioTaller();
                    $model_servicio_taller->isNewRecord = true;
                    $model_servicio_taller->id_servicio = $model->id;
                    $model_servicio_taller->id_taller = $id_taller;
        
                    if ( $model_servicio_taller->save() ) {
                        //Enviar correo a estacion
                        \Yii::$app->getSession()->setFlash('success', 'Se asignó el taller con éxito! Información enviada a la estación');
                        return $this->redirect(['servicio/gestionservicio']);
                    } else{
                        \Yii::$app->getSession()->setFlash('error', 'Ocurrió un error al asignar el taller!');
                        return $this->redirect(['servicio/asignartaller/'.$id.'']);
                    }
                } else{
        
                    $searchModel_caso = new CasoServicioSearch([ 'id_servicio' => $id ]);
                    $dataProvider_caso = $searchModel_caso->search(Yii::$app->request->queryParams);
        
                    $searchModel_taller = new TallerSearch();
                    $dataProvider_taller = $searchModel_taller->search(Yii::$app->request->queryParams);
        
                    return $this->render('asignar-taller', [ 
                        'model' => $model,
                        'dataProvider_caso' => $dataProvider_caso,
                        'dataProvider_taller' => $dataProvider_taller,
                        'searchModel_taller'    => $searchModel_taller
                    ]);
                }
            }else {
                $tipo_taller = '';
                $searchModel_caso = new CasoServicioSearch([ 'id_servicio' => $id ]);
                $dataProvider_caso = $searchModel_caso->search(Yii::$app->request->queryParams);
                $model_vehiculo = Vehiculo::findOne($model->id_vehiculo);
                //if(isset($_POST['Servicio']['linea'])){

                if(!empty($_POST['linea']) || isset($_POST['linea']) && $_POST['linea'] == 0){                
                    $tipo_taller =  $_POST['linea'];
                    // print_r($tipo_taller);
                    // die();
                    $searchModel_taller = new TallerSearch(['tipo_taller' => $tipo_taller]);
                }else{
                    $searchModel_taller = new TallerSearch();
                }

                //Talleres
                $dataProvider_taller = $searchModel_taller->search(Yii::$app->request->queryParams);

                //Garantias
                $searchModel_gv = new GarantiaVehiculoSearch([ 'id_vehiculo' => $model_vehiculo->id, 'anular' => 0 ]);
                $dataProvider_gv = $searchModel_gv->search(Yii::$app->request->queryParams);
                $dataProvider_gv->query->orderBy('id');

                //Mantenimeintos
                $searchModel_servicio = new ServicioSearch([ 'id_vehiculo' => $model_vehiculo->id ]);
                $dataProvider_servicio = $searchModel_servicio->searchRealizados(Yii::$app->request->queryParams);
                $dataProvider_servicio->query->orderBy('id DESC');

                return $this->render('asignar-taller', [ 
                    'model' => $model,
                    'model_vehiculo' => $model_vehiculo,
                    'dataProvider_caso' => $dataProvider_caso,
                    'dataProvider_taller' => $dataProvider_taller,
                    'searchModel_taller'    => $searchModel_taller,
                    'tipo_taller'   => $tipo_taller,
                    'dataProvider_gv' => $dataProvider_gv,
                    'dataProvider_servicio' => $dataProvider_servicio,
                ]);
            }         
        }

    }


    /**
     * Index Taller
     * @return mixed
     */
    public function actionIndextaller()
    {

        $id_taller = User::findOne(Yii::$app->user->id)->id_taller;
        $searchModel = new SolicitudServicioTallerSearch(['id_taller' => $id_taller]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id_taller);
        $dataProvider->query->orderBy('id_servicio ASC');

        return $this->render('index-taller', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    /**
     * Check-In Taller - Ingreso del vehículo al taller
     * @return mixed
     */
    public function actionCheckintaller( $id )
    {
        $model = $this->findModel($id);
        $model_vehiculo = Vehiculo::findOne([ 'id' => $model->id_vehiculo ]);

        return $this->render('checkin-taller', [
            'model' => $model,
            'model_vehiculo' => $model_vehiculo,
        ]);
    }

    /**
     * Escanear Vehículo para la recepción en el taller
     * @return mixed
     */
    public function actionEscanearvehiculotaller()
    {

        if (Yii::$app->request->post()) {
            
            $codigo = Yii::$app->request->post('codigo');
            $id_servicio = Yii::$app->request->post('id_servicio');

            $model_vehiculo = Vehiculo::find()->where(['placa'=>$codigo])->one();

            if ( $model_vehiculo ) {
                $model = Servicio::find()->where(['id' => $id_servicio ])->one();

                if ( $model ) {
                    $query = new query();
                    $query->select('es.id, es.codigo')
                        ->from('pipeline_servicio p')
                        ->join('INNER JOIN','estado_servicio es','es.id = p.id_estado_servicio')
                        ->where('p.id_servicio = '. $model->id )
                        ->orderBy('p.id desc');

                    $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                    $estado = Yii::$app->db->createCommand($sql)->queryOne();

                    if ( $estado['codigo'] == 0 ) {

                        /* Pipeline -> Estado Evaluación */
                        $estado = EstadoServicio::findOne(['codigo' => 1])->id;
                        $model_pipeline = new PipelineServicio();
                        $model_pipeline->fecha = date('Y-m-d');
                        $model_pipeline->id_servicio = $model->id;
                        $model_pipeline->id_estado_servicio = $estado;
                        $model_pipeline->save();
                        unset($model_pipeline);

                        return 1;
                    } else{
                        return 0; // No se encuentra en el estado de checkin
                    }

                } else{
                    return 2; // No coinside el codigo del vehiculo con la solicitud
                }
            } else{
                return 3; // Codigo de vehiculo incorrecto
            }
        }
    }

    /**
     * Crear Cotización Taller - Registro de una cotización por parte del taller
     * @return mixed
     */
    public function actionCrearcotizaciontaller( $id )
    {

        $model_solicitud_servicio_taller = SolicitudServicioTaller::findOne($id);
        
        if ( $model_solicitud_servicio_taller ) {
            $model_cotizacion_taller = new CotizacionTaller();
            $model_cotizacion_taller->isNewRecord = true;
            $model_cotizacion_taller->fecha = date("Y-m-d H:i:s");
            $model_cotizacion_taller->estado = 0;
            $model_cotizacion_taller->activo = 0;
            $model_cotizacion_taller->id_solicitud_servicio_taller = $model_solicitud_servicio_taller->id;
            $model_cotizacion_taller->save();
            
            if ($model_cotizacion_taller->save(false)) {
                return $this->redirect([ 'cotizaciontaller', 'id' => $model_cotizacion_taller->id,]);
            } else{
                \Yii::$app->getSession()->setFlash('error', 'Ocurrió un error al crear la cotización!');
                return $this->redirect(['servicio/indextaller']);
            }
        } else{
            \Yii::$app->getSession()->setFlash('error', 'Ocurrió un error al crear la cotización!');
            return $this->redirect(['servicio/indextaller']);
        }
    }

    /**
     * Cotización Taller - Registro de una cotización por parte del taller
     * @return mixed
     */
    public function actionCotizaciontaller( $id )
    {
        $model_cotizacion_taller = CotizacionTaller::findOne($id);
        $model_servicio_mensaje = new ServicioMensaje();
        //$all_model_cotizacion_taller = CotizacionTaller::find()->where(['id_solicitud_servicio_taller' => $model_cotizacion_taller->id_solicitud_servicio_taller, 'estado' => 1 ])->all();
        $soporte_cotizacion_taller = new UploadCotizacionTaller();
        $model_cotizacion_taller_pieza = CotizacionTallerPieza::find()->where(['id_cotizacion_taller' => $model_cotizacion_taller->id])->all();
        $model_cotizacion_taller_actividad = CotizacionTallerActividad::find()->where(['id_cotizacion_taller' => $model_cotizacion_taller->id])->all();
        
        if ($model_cotizacion_taller->load(Yii::$app->request->post())) {

 
            $model_cotizacion_taller->estado = 1;
            
            if ($model_cotizacion_taller->save(false)) {

                $path = './uploads/cotizacion-taller/' . $model_cotizacion_taller->id . '/';
                if ( !is_dir($path) ) {
                    mkdir( $path, 0777, true );
                }

                // Guardo la imagen de soporte
                $soporte_cotizacion_taller->imageFiles = UploadedFile::getInstances($soporte_cotizacion_taller, 'imageFiles');
                $soporte_cotizacion_taller->upload_soporte_cotizacion_taller($model_cotizacion_taller->id);

                $soporte_cotizacion_taller->imageFiles1 = UploadedFile::getInstances($soporte_cotizacion_taller, 'imageFiles1');
                $soporte_cotizacion_taller->upload_soporte_cotizacion_taller1($model_cotizacion_taller->id);

                $soporte_cotizacion_taller->imageFiles2 = UploadedFile::getInstances($soporte_cotizacion_taller, 'imageFiles2');
                $soporte_cotizacion_taller->upload_soporte_cotizacion_taller2($model_cotizacion_taller->id);

                $soporte_cotizacion_taller->imageFiles3 = UploadedFile::getInstances($soporte_cotizacion_taller, 'imageFiles3');
                $soporte_cotizacion_taller->upload_soporte_cotizacion_taller3($model_cotizacion_taller->id);

                $soporte_cotizacion_taller->imageFiles4 = UploadedFile::getInstances($soporte_cotizacion_taller, 'imageFiles4');
                $soporte_cotizacion_taller->upload_soporte_cotizacion_taller4($model_cotizacion_taller->id);

                
                $query = new query();
                $query->select('es.id, es.codigo')
                    ->from('pipeline_servicio p')
                    ->join('INNER JOIN','estado_servicio es','es.id = p.id_estado_servicio')
                    ->where('p.id_servicio = '. $model_cotizacion_taller->idSolicitudServicioTaller->id_servicio )
                    ->orderBy('p.id desc');

                $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                $estado = Yii::$app->db->createCommand($sql)->queryOne();


                if ( $estado['codigo'] == 1 ) {

                    /* Pipeline -> Estado Aprobación */
                    $estado = EstadoServicio::findOne(['codigo' => 2])->id;
                    $model_pipeline = new PipelineServicio();
                    $model_pipeline->fecha = date('Y-m-d');
                    $model_pipeline->id_servicio = $model_cotizacion_taller->idSolicitudServicioTaller->id_servicio;
                    $model_pipeline->id_estado_servicio = $estado;
                    $model_pipeline->save();
                    unset($model_pipeline);
                }

                //Se guarda el mensaje que indique el taller
                $model_servicio_mensaje->load(Yii::$app->request->post());
                $model_servicio_mensaje->fecha = date("Y-m-d H:i:s");
                $model_servicio_mensaje->motivo = 0; //Cotización
                $model_servicio_mensaje->estado = 1; //Habilitado para ser visto por el taller
                $model_servicio_mensaje->codigo_pipeline = 2; //Aprobación
                $model_servicio_mensaje->id_servicio = $model_cotizacion_taller->idSolicitudServicioTaller->id_servicio;
                $model_servicio_mensaje->id_user = Yii::$app->user->identity->id;
                $model_servicio_mensaje->save();
                
                if (Yii::$app->request->post('CotizacionTallerPieza')[0]['id_pieza']) {
                    foreach ($model_cotizacion_taller_pieza as $key => $pieza) {
                        $pieza->delete();
                    }
                    
                    foreach (Yii::$app->request->post('CotizacionTallerPieza') as $i => $pieza) {
                        $model_cotizacion_taller_pieza = new CotizacionTallerPieza;
                        $model_cotizacion_taller_pieza->id_pieza = $pieza['id_pieza'];
                        $model_cotizacion_taller_pieza->cantidad = $pieza['cantidad'];
                        if ($pieza['cotiza']) {
                           
                        $model_cotizacion_taller_pieza->precio = $pieza['precio'];
                        $model_cotizacion_taller_pieza->cotiza = $pieza['cotiza'];
                        }
                        else{
                           $model_cotizacion_taller_pieza->precio = null;
                           $model_cotizacion_taller_pieza->cotiza = $pieza['cotiza'];
                        }
                        $model_cotizacion_taller_pieza->id_cotizacion_taller = $model_cotizacion_taller->id;
                        $model_cotizacion_taller_pieza->save(false);
                        // unset($model_cotizacion_taller_pieza);
                
                    }
                }
 
                if ( Yii::$app->request->post('CotizacionTallerActividad')[0]['tipo_servicio_taller']) {
 
                    foreach ($model_cotizacion_taller_actividad as $key => $actividad) {
                        $actividad->delete();
                    }
                    
                    foreach (Yii::$app->request->post('CotizacionTallerActividad') as $i => $actividad) {
                        $model_cotizacion_taller_actividad = new CotizacionTallerActividad;
                        $model_cotizacion_taller_actividad->id_pieza_servicio_taller = $actividad['id_pieza_servicio_taller'];
                        $model_cotizacion_taller_actividad->labor = $actividad['labor'];
                        $model_cotizacion_taller_actividad->precio = $actividad['precio'];
                        $model_cotizacion_taller_actividad->total_servicio = $actividad['total_servicio'];
                        $model_cotizacion_taller_actividad->id_cotizacion_taller = $model_cotizacion_taller->id;
                        $model_cotizacion_taller_actividad->id_vehiculo = $model_cotizacion_taller->idSolicitudServicioTaller->idServicio->id_vehiculo;
                        $model_cotizacion_taller_actividad->tipo_servicio_taller = $actividad['tipo_servicio_taller'];
                        $model_cotizacion_taller_actividad->decimal = Yii::$app->request->post('Taller')[$i]['precio_temp'];
                        $model_cotizacion_taller_actividad->save(false);
                        unset($model_cotizacion_taller_actividad);
                    }
                }
          
                \Yii::$app->getSession()->setFlash('success', '¡La cotización se creado con éxito!');
                return $this->redirect('../indextaller');
            }

        } else{
            $id_servicio = $model_cotizacion_taller->idSolicitudServicioTaller->id_servicio;
            $id_taller = User::findOne(Yii::$app->user->id)->id_taller;
            $model = $this->findModel($id_servicio);
            $model_vehiculo = Vehiculo::findOne([ 'id' => $model->id_vehiculo ]);
            $model_taller = Taller::findOne($id_taller);
            $model_pieza = new Pieza();

            $mensajes = ServicioMensaje::find()->where([ 'id_servicio' => $model->id, 'estado' => 1 ])->all();
            
            return $this->render('cotizacion-taller', [
                'model' => $model,
                'model_vehiculo' => $model_vehiculo,
                'model_taller' => $model_taller,
                'model_cotizacion_taller' => $model_cotizacion_taller,
                'soporte_cotizacion_taller' => $soporte_cotizacion_taller,
                'model_pieza' => $model_pieza,
                'model_cotizacion_taller_pieza' => (empty($model_cotizacion_taller_pieza)) ? [new CotizacionTallerPieza] : $model_cotizacion_taller_pieza,
                'model_cotizacion_taller_actividad' => (empty($model_cotizacion_taller_actividad)) ? [new CotizacionTallerActividad] : $model_cotizacion_taller_actividad,
                'model_servicio_mensaje' => $model_servicio_mensaje,
                'mensajes' => $mensajes,
            ]);
        }
        
    }

    /**
     * Index Cotizaciones de taller
     * @param integer $id
     * @return mixed
     */
    public function actionIndexcotizaciontaller($id)
    {
        $searchModel = new CotizacionTallerSearch(['id_solicitud_servicio_taller' => $id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $id_servicio = SolicitudServicioTaller::findOne($id)->id_servicio;

        return $this->render('index-cotizacion-taller', [
            'dataProvider' => $dataProvider,
            'id' => $id,
            'id_servicio' => $id_servicio,
        ]);
    }

    /**
     * Eliminar una imagen de la cotizacion
     * @return mixed
     */
    public function actionDeleteimage($id) {
        $model = CotizacionTaller::findOne($id);

        $image = Yii::$app->basePath . $model->imagen_soporte;
        if (unlink($image)) {
            $model->imagen_soporte = null;
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 
                    'Your image was removed successfully. Upload another by clicking Browse below');
            } else{
                Yii::$app->session->setFlash('error', 
                'Error removing image. Please try again later or contact the system admin.');
            }
        }

        return $this->redirect(['cotizaciontaller','id'=>$model->id]);
    }

    /**
     * Visualizacion de la cotizacion del taller
     * @return mixed
     */
    public function actionViewcotizaciontaller( $id )
    {
        $model_cotizacion_taller = CotizacionTaller::findOne($id);
        
        
        $id_servicio = $model_cotizacion_taller->idSolicitudServicioTaller->id_servicio;
        $id_taller = $model_cotizacion_taller->idSolicitudServicioTaller->id_taller;
        $model = $this->findModel($id_servicio);
        $model_vehiculo = Vehiculo::findOne([ 'id' => $model->id_vehiculo ]);
        $model_taller = Taller::findOne($id_taller);

        $searchModel_cotizacion_pieza = new CotizacionTallerPiezaSearch(['id_cotizacion_taller' => $model_cotizacion_taller->id]);
        $dataProvider_cotizacion_pieza = $searchModel_cotizacion_pieza->search(Yii::$app->request->queryParams);

        $searchModel_cotizacion_actividad = new CotizacionTallerActividadSearch(['id_cotizacion_taller' => $model_cotizacion_taller->id]);
        $dataProvider_cotizacion_actividad = $searchModel_cotizacion_actividad->search(Yii::$app->request->queryParams);

        $model_servicio_mensaje = ServicioMensaje::find()->where([ 'id_servicio' => $model->id, 'estado' => 1 ])->all();

        if (Yii::$app->user->can('admin')) {
            $model_cotizacion_taller_analista = new CotizacionTallerAnalista();
            $count_asignacion = CotizacionTallerAnalista::find()->where(['id_cotizacion_taller' => $model_cotizacion_taller->id ])->count();

            return $this->render('view-cotizacion-taller', [
                'model' => $model,
                'model_vehiculo' => $model_vehiculo,
                'model_taller' => $model_taller,
                'model_cotizacion_taller' => $model_cotizacion_taller,
                'dataProvider_cotizacion_pieza' => $dataProvider_cotizacion_pieza,
                'dataProvider_cotizacion_actividad' => $dataProvider_cotizacion_actividad,
                'model_cotizacion_taller_analista' => $model_cotizacion_taller_analista,
                'count_asignacion' => $count_asignacion,
            ]);
        } else{
            return $this->render('view-cotizacion-taller', [
                'model' => $model,
                'model_vehiculo' => $model_vehiculo,
                'model_taller' => $model_taller,
                'model_cotizacion_taller' => $model_cotizacion_taller,
                'dataProvider_cotizacion_pieza' => $dataProvider_cotizacion_pieza,
                'dataProvider_cotizacion_actividad' => $dataProvider_cotizacion_actividad,
                'model_servicio_mensaje' => $model_servicio_mensaje,
            ]);
        }
        
    }

    /**
     * Asignar analista de mantenimiento
     * @param integer $id_cotizacion
     * @return mixed
     */
    public function actionAsignaranalista()
    {
        if (Yii::$app->request->post()) {
            
            $id_analista = Yii::$app->request->post('id_analista');
            $id_servicio = Yii::$app->request->post('id');
            $add_analista_checklist_avaluo = new ChecklistAvaluo();

            if ($add_analista_checklist_avaluo->load(Yii::$app->request->post())){
                $add_analista_checklist_avaluo->id_servicio = $id_servicio;
                
                if ($add_analista_checklist_avaluo->save()) {
                    \Yii::$app->getSession()->setFlash('success', 'Se asignó el analista con éxito!');
                } else{
                    print_r($add_analista_checklist_avaluo->getErrors());  
                    die();              
                    \Yii::$app->getSession()->setFlash('error', 'Error! No se pudo asignar el analista.1');                    
                }
            } else{
                \Yii::$app->getSession()->setFlash('error', 'Error! No se pudo asignar el analista.2');
            }
        }

        // if (Yii::$app->request->post()) {
            

        //     $id_analista = Yii::$app->request->post('id_analista');
        //     $id_cotizacion = Yii::$app->request->post('id');
        //     $model_cotizacion_taller_analista = new CotizacionTallerAnalista();

        //     if ($model_cotizacion_taller_analista->load(Yii::$app->request->post())){
        //         $model_cotizacion_taller_analista->id_cotizacion_taller = $id_cotizacion;
                
        //         if ($model_cotizacion_taller_analista->save()) {
        //             \Yii::$app->getSession()->setFlash('success', 'Se asignó el analista con éxito!');
        //         } else{
        //             \Yii::$app->getSession()->setFlash('error', 'Error! No se pudo asignar el analista.');
        //         }
        //     } else{
        //         \Yii::$app->getSession()->setFlash('error', 'Error! No se pudo asignar el analista.');
        //     }
        // }

        //return $this->redirect(['servicio/viewcotizaciontaller', 'id' => $id_cotizacion]);
    }

    /**
     * Visualizacion de la cotizacion del taller
     * @return mixed
     */
    public function actionAnalisisviabilidad( $id )
    {
        $model_cotizacion_taller = CotizacionTaller::findOne($id);
        
        
        $id_servicio = $model_cotizacion_taller->idSolicitudServicioTaller->id_servicio;

        $model = $this->findModel($id_servicio);
        $model_vehiculo = Vehiculo::findOne([ 'id' => $model->id_vehiculo ]);

        $checklist_avaluo = ChecklistAvaluo::findOne(['id_servicio' => $id_servicio]);

        $searchModel_cotizacion_pieza = new CotizacionTallerPiezaSearch(['id_cotizacion_taller' => $model_cotizacion_taller->id]);
        $dataProvider_cotizacion_pieza = $searchModel_cotizacion_pieza->search(Yii::$app->request->queryParams);

        $searchModel_cotizacion_actividad = new CotizacionTallerActividadSearch(['id_cotizacion_taller' => $model_cotizacion_taller->id]);
        $dataProvider_cotizacion_actividad = $searchModel_cotizacion_actividad->search(Yii::$app->request->queryParams);

        $searchModel_caso = new CasoServicioSearch([ 'id_servicio' => $model->id ]);
        $dataProvider_caso = $searchModel_caso->search(Yii::$app->request->queryParams);

        $model_taller = Taller::findOne($model_cotizacion_taller->idSolicitudServicioTaller->id_taller);

        $add_analista_checklist_avaluo = new ChecklistAvaluo();

        $model_servicio_mensaje = new ServicioMensaje();

        $mensajes = ServicioMensaje::find()->where([ 'id_servicio' => $model->id ])->all();
        
        return $this->render('analisis-viabilidad', [
            'model' => $model,
            'model_vehiculo' => $model_vehiculo,
            'model_taller' => $model_taller,
            'model_cotizacion_taller' => $model_cotizacion_taller,
            'dataProvider_cotizacion_pieza' => $dataProvider_cotizacion_pieza,
            'dataProvider_cotizacion_actividad' => $dataProvider_cotizacion_actividad,
            'dataProvider_caso' => $dataProvider_caso,
            'checklist_avaluo' => $checklist_avaluo,
            'add_analista_checklist_avaluo' => $add_analista_checklist_avaluo,
            'model_servicio_mensaje' => $model_servicio_mensaje,
            'mensajes' => $mensajes,
        ]);
    }

    /**
     * Guardar Analisis de Viabilidad
     * @param integer $id
     * @return mixed
     */
    public function actionGuardaranalisisviabilidad()
    {
        if (Yii::$app->request->post()) {

            $id = Yii::$app->request->post('id');
            $id_cotizacion = Yii::$app->request->post('id_cotizacion');

            $model = $this->findModel($id);
            $model_cotizacion_taller = CotizacionTaller::findOne($id_cotizacion);

            $model_servicio_mensaje = new ServicioMensaje();
            $model_servicio_mensaje->load(Yii::$app->request->post());
            $model_servicio_mensaje->fecha = date("Y-m-d H:i:s");
            $model_servicio_mensaje->codigo_pipeline = 2; //Aprobación
            $model_servicio_mensaje->id_servicio = $model_cotizacion_taller->idSolicitudServicioTaller->id_servicio;
            $model_servicio_mensaje->id_user = Yii::$app->user->identity->id;

            /*echo "<pre>";
            print_r(Yii::$app->request->post());
            echo "</pre>";*/
            
            if ( $model->load(Yii::$app->request->post()) ) {
                $decision = $model->decision;

                // 1) Nivel 1: Verifica el Analista de Mantenimiento
                if ( $model_cotizacion_taller->activo == 0 && $decision == 1 ) { //Analista - Reparar

                    //Se asigna a la solicitud de servicio el id del Analista que aprobo la reparación
                    $model->id_analista = Yii::$app->user->identity->id;

                    //Activo = 1 -> solicitud de mantenimeinto aprobada por el analista
                    $model_cotizacion_taller->estado = 2;
                    $model_cotizacion_taller->activo = 1;

                    //Se busca la variable de mantenimiento para consultar el monto máximo que pude aprobar el analista
                    $model_variables = Variable::find()->where(['key' => 'mlma'])->one();

                    //Se obtienen los precios de las actividades aprobadas
                    $model_cotizacion_taller_actividad = CotizacionTallerActividad::find()->where([ 'id_cotizacion_taller' => $model_cotizacion_taller->id ])->all();

                    $subtotal = $total = $itbis = 0;

                    //Se calcula el subtotal
                    foreach ($model_cotizacion_taller_actividad as $actividad) {
                        $subtotal += $actividad->total_servicio;
                    }

                    //Se calcula el itbis
                    $itbis = $subtotal * 0.18;

                    //Se calcula el total
                    $total = $subtotal + $itbis;

                    if ( $model_cotizacion_taller->save(false) && $model->save() ) {

                        //Se guarda el mensaje
                        $model_servicio_mensaje->motivo = 2; //Analista
                        $model_servicio_mensaje->estado = 0; //Deshabilitado para ser visto por el taller
                        $model_servicio_mensaje->save();

                        //Si el total es menor al monto máximo de aprobacion de mantenimiento se crea la orden de trabajo 
                        //En caso contrario debe aprobar el Gerente de Mantenimiento
                        if ( $model_variables->value > $total ) {
                            $this->crearOrdentrabajo( $model_cotizacion_taller->id );
                        } else{
                            \Yii::$app->getSession()->setFlash('success', '¡La solicitud se aprobó con éxito!, Será analizada por el Gerente de Mantenimiento!');
                            return $this->redirect('../indexanalista');
                        }
                    }

                
                } elseif ( $model_cotizacion_taller->activo == 0 && $decision == 2 ){ //Analista - Renegociar con el taller
                    if ( $model_servicio_mensaje->mensaje != '' ) {
                        $model_cotizacion_taller->observacion_cancela = $model_servicio_mensaje->mensaje;
                        $model_cotizacion_taller->estado = 3;

                        $email_taller =  $model_cotizacion_taller->idSolicitudServicioTaller->idTaller->email_representante;
                        $motivo = $model_servicio_mensaje->mensaje ;
                        /*Mail*/
                        try {
                            $mensaje_sup ='<p>Apreciado RSA '.$model_cotizacion_taller->idSolicitudServicioTaller->idTaller->nombre.' <br>
                                            La cotización de la Solictud No.'.$model_cotizacion_taller->idSolicitudServicioTaller->id_servicio.' fue rechazada, Km100 quiere renegociar
                                        </p>
                                        <p> 
                                            Motivo: '.$motivo.'<br>
                                            Ingrese al sistema para editar la cotización.
                                        </p>
                                    ';

                            Yii::$app->mailer->compose(['html'=>'layout'], ['content'=> $mensaje_sup])
                                ->setFrom([Yii::$app->params['adminEmail'] => 'KM100'])
                                ->setTo($email_taller)
                                ->setSubject('Notificación de Negociación')
                                ->send();

                        } catch (Exception $e) {
                            \Yii::$app->getSession()->setFlash('error', 'Error al enviar el correo de notificación');
                        }
                        
                        if ($model_cotizacion_taller->save(false) && $model->save() ) {

                            //Se guarda el mensaje
                            $model_servicio_mensaje->motivo = 2; //Analista
                            $model_servicio_mensaje->estado = 1; //Habilitado para ser visto por el taller
                            $model_servicio_mensaje->save();

                            \Yii::$app->getSession()->setFlash('success', 'Solicitud enviada a Negociación con el Taller');
                            return $this->redirect('../indexanalista');

                        } else{
                            \Yii::$app->getSession()->setFlash('error', '¡Hubo un error!');
                            return $this->redirect('../indexanalista');
                        }
                    } else{
                        \Yii::$app->getSession()->setFlash('error', '¡Debe indicar una observación!');
                        return;
                    }
                } elseif ( ($model_cotizacion_taller->activo == 1 || $model_cotizacion_taller->activo == 2 ) && $decision == 2 ){ //Gerente, Director Comercial - Renegociar con el taller
                    if ( $model_servicio_mensaje->mensaje != '' ) {
                        
                        $model_cotizacion_taller->estado = 1;
                        $model_cotizacion_taller->activo = 0;

                        if ($model_cotizacion_taller->save(false) && $model->save() ) {

                            //Se guarda el mensaje
                            $model_servicio_mensaje->motivo = 3; 
                            $model_servicio_mensaje->estado = 0; //Deshabilitado para ser visto por el taller
                            $model_servicio_mensaje->save();

                            \Yii::$app->getSession()->setFlash('success', 'Solicitud enviada a Negociación con el Taller');
                            return $this->redirect('../indexgerente');
                            
                        } else{
                            \Yii::$app->getSession()->setFlash('error', '¡Hubo un error!');
                            return $this->redirect('../indexgerente');
                        }
                    } else{
                        \Yii::$app->getSession()->setFlash('error', '¡Debe indicar una observación!');
                        return;
                    }
                // 2) Nivel 2: Verifica el Gerente de Mantenimeinto
                } elseif ( $model_cotizacion_taller->activo == 1 && $decision == 1 ) {
                    //Activo = 2 -> solicitud de mantenimeinto aprobada por el gerente
                    $model_cotizacion_taller->activo = 2;

                    //Se busca la variable de mantenimiento para consultar el monto maximo que pude aprobar el gerente
                    $model_variables = Variable::find()->where(['key' => 'mlm'])->one();

                    //Se obtienen los precios de las actividades aprobadas
                    $model_cotizacion_taller_actividad = CotizacionTallerActividad::find()->where([ 'id_cotizacion_taller' => $model_cotizacion_taller->id ])->all();

                    $subtotal = $total = $itbis = 0;

                    //Se calcula el subtotal
                    foreach ($model_cotizacion_taller_actividad as $actividad) {
                        $subtotal += $actividad->total_servicio;
                    }

                    //Se calcula el itbis
                    $itbis = $subtotal * 0.18;

                    //Se calcula el total
                    $total = $subtotal + $itbis;

                    if ($model_cotizacion_taller->save(false) && $model->save() ) {

                        //Se guarda el mensaje
                        $model_servicio_mensaje->motivo = 3; //Analista
                        $model_servicio_mensaje->estado = 0; //Deshabilitado para ser visto por el taller
                        $model_servicio_mensaje->save();

                        //Si el total es menor al monto máximo de mantenimiento se crea la orden de trabajo 
                        //En caso contrario debe aprobar el Director de Operaciones
                        if ( $model_variables->value > $total ) {
                            $this->crearOrdentrabajo( $model_cotizacion_taller->id );
                        } else{
                            \Yii::$app->getSession()->setFlash('success', '¡La solicitud se aprobó con éxito!, Será analizada por el Director de Operaciones!');
                            return $this->redirect('../indexgerente');
                        }

                    }

                //Salvamento gerente
                } elseif ( $model_cotizacion_taller->activo == 1 && $decision == 4 ) {
                    //Se guarda el mensaje
                    $model_servicio_mensaje->motivo = 5; //Analista
                    $model_servicio_mensaje->estado = 0; //Deshabilitado para ser visto por el taller
                    $model_servicio_mensaje->save();

                    //La observacion del gerente
                    $model->observacion_gerente = $model_servicio_mensaje->mensaje;

                    //Activo = 2 -> solicitud de mantenimeinto aprobada por el gerente
                    $model_cotizacion_taller->activo = 2;
                    if ($model_cotizacion_taller->save(false) && $model->save() ) {
                        \Yii::$app->getSession()->setFlash('success', 'La solicitud se registro como Salvamento, Será analizada por el Director de Operaciones!');
                        return $this->redirect('../indexgerente');
                    }
                //Salvamento Director Comercial
                } elseif ( $model_cotizacion_taller->activo == 2 && $decision == 4 ) {

                     //Se guarda el mensaje
                    $model_servicio_mensaje->motivo = 5; //Analista
                    $model_servicio_mensaje->estado = 0; //Deshabilitado para ser visto por el taller
                    $model_servicio_mensaje->save();

                    //La observacion del gerente
                    $model->observacion_director = $model_servicio_mensaje->mensaje;

                    //Se cancela la solicitud
                    //Motivo de cancelación
                    $model->observacion_cancelado = $model->observacion_director;
                    $model->usuario_cancela = Yii::$app->user->identity->id;
                    $model->update();

                    /* Pipeline -> Estado Cancelado */
                    $estado_0 = EstadoServicio::findOne(['codigo' => 12])->id;
                    $model_pipeline = new PipelineServicio();
                    $model_pipeline->fecha = date('Y-m-d');
                    $model_pipeline->id_servicio = $model->id;
                    $model_pipeline->id_estado_servicio = $estado_0;
                    $model_pipeline->save();
                    unset($model_pipeline);
                    /*Mail*/
                    try {
                        $mensaje_sup ='  <p> Km100 Le informa que la solicitud de servicio fue cancelada y el vehículo será enviado a Salvamento:
                                        </p>
                                        <p>
                                            <strong>Servicio:</strong> Nº'.$model->id.'<br>
                                            <strong>MVA Ficha: </strong>'.$model->ficha.'<br>
                                            <strong>Placa>: </strong '.$model->placa.'<br>
                                            <strong>Motivo de cancelación: </strong>'.$model->observacion_director.'
                                        </p>
                        ';

                        Yii::$app->mailer->compose(['html'=>'layout'], ['content'=> $mensaje_sup])
                                        ->setFrom([Yii::$app->params['adminEmail'] => 'KM100'])
                                        ->setTo('zulmadessire@gmail.com')
                                        ->setSubject('Vehículo enviado a Salvamento')
                                        ->send();
                    } catch (Exception $e) {
                        \Yii::$app->getSession()->setFlash('error', 'Error al enviar el correo de notificación');
                    }
                    /*Fin Mail*/

                    //Se actualiza el estado actual del vehiculo
                    $model_estado_vehiculo = new EstadoVehiculo();
                    $model_estado_vehiculo->isNewRecord = true;
                    $model_estado_vehiculo->id_estado = Estado::findOne(['id_tipo_estado' => '1', 'nombre' => 'Salvamento' ])->id;
                    $model_estado_vehiculo->id_vehiculo = $model_cotizacion_taller->idSolicitudServicioTaller->idServicio->id_vehiculo;
                    $model_estado_vehiculo->save();
                    unset($model_estado_vehiculo);
                    
                    \Yii::$app->getSession()->setFlash('success', 'Vehículo enviado a Salvamento!');
                    return $this->redirect('../indexgerente');
                // 3) Nivel 3: Verifica el Director de Operaciones
                } elseif ( $model_cotizacion_taller->activo == 2 && $decision == 1 ) {
                     //Se guarda el mensaje
                    $model_servicio_mensaje->motivo = 4; //Analista
                    $model_servicio_mensaje->estado = 0; //Deshabilitado para ser visto por el taller
                    $model_servicio_mensaje->save();

                    //La observacion del gerente
                    $model->observacion_gerente = $model_servicio_mensaje->mensaje;

                    $model->save();
                    $this->crearOrdentrabajo( $model_cotizacion_taller->id );
                }
            }
        }
    }

    public function crearOrdentrabajo( $id_cotizacion_taller ){

        $model_orden_trabajo = new OrdenTrabajo();
        $model_orden_trabajo->fecha = date("Y-m-d H:i:s");
        $model_orden_trabajo->estado = 0; //Creado
        $model_orden_trabajo->id_cotizacion_taller = $id_cotizacion_taller;

        if ( $model_orden_trabajo->save() ) {
            //Aqui va el correo al taller
            //Aqui va el sms al taller
            \Yii::$app->getSession()->setFlash('success', '¡La orden de trabajo se ha creado con éxito!');
            return $this->redirect([ 'servicio/ordentrabajo', 'id' => $model_orden_trabajo->id ]);
        } else{
            \Yii::$app->getSession()->setFlash('error', '¡Hubo un error!');
            return $this->redirect([ 'servicio/analisisviabilidad', 'id' => $id_cotizacion_taller ]);
        }

    }

    /**
     * Orden de Trabajo
     * @param integer $id
     * @return mixed
     */
    public function actionOrdentrabajo( $id )
    {
        $model_orden_trabajo = OrdenTrabajo::findOne($id);
        $model_cotizacion_taller = CotizacionTaller::findOne($model_orden_trabajo->id_cotizacion_taller);
        $model = $this->findModel($model_orden_trabajo->idCotizacionTaller->idSolicitudServicioTaller->id_servicio);
        $model_vehiculo = Vehiculo::findOne([ 'id' => $model->id_vehiculo ]);

        $searchModel_cotizacion_actividad = new CotizacionTallerActividadSearch(['id_cotizacion_taller' => $model_cotizacion_taller->id]);
        $dataProvider_cotizacion_actividad = $searchModel_cotizacion_actividad->search(Yii::$app->request->queryParams);

        $searchModel_cotizacion_pieza = new CotizacionTallerPiezaSearch(['id_cotizacion_taller' => $model_cotizacion_taller->id]);
        $dataProvider_cotizacion_pieza = $searchModel_cotizacion_pieza->search(Yii::$app->request->queryParams);

        return $this->render('orden-trabajo', [
            'model' => $model,
            'model_orden_trabajo' => $model_orden_trabajo,
            'model_vehiculo' => $model_vehiculo,
            'model_cotizacion_taller' => $model_cotizacion_taller,
            'dataProvider_cotizacion_actividad' => $dataProvider_cotizacion_actividad,
            'dataProvider_cotizacion_pieza' => $dataProvider_cotizacion_pieza,
        ]);
    }

    /**
     * Guardar Orden de Trabajo
     * @param integer $id
     * @return mixed
     */
    public function actionGuardarordentrabajo( $id )
    {
        $model_orden_trabajo = OrdenTrabajo::findOne($id);

        $searchModel_cotizacion_pieza = new CotizacionTallerPiezaSearch(['id_cotizacion_taller' => $model_orden_trabajo->id_cotizacion_taller]);
        $dataProvider_cotizacion_pieza = $searchModel_cotizacion_pieza->search(Yii::$app->request->queryParams);

        if ($model_orden_trabajo->load(Yii::$app->request->post())) {
            $model_orden_trabajo->estado = 1; //Activo
            if ($model_orden_trabajo->save()) {
                //Cambio el estado de la solicitud

                //Si se registraron piezas
                if ( $dataProvider_cotizacion_pieza->getTotalCount() > 0 ) {
                    /* Pipeline -> Estado Por pieza */
                    $estado = EstadoServicio::findOne(['codigo' => 3])->id;
                    $model_pipeline = new PipelineServicio();
                    $model_pipeline->fecha = date('Y-m-d');
                    $model_pipeline->id_servicio = $model_orden_trabajo->idCotizacionTaller->idSolicitudServicioTaller->id_servicio;
                    $model_pipeline->id_estado_servicio = $estado;
                    $model_pipeline->save();
                    unset($model_pipeline);


                    //Se crea la Solicitud de compra de pieza
                    $model_solicitud_compra_pieza = new SolicitudCompraPieza();
                    $model_solicitud_compra_pieza->fecha = date("Y-m-d H:i:s");
                    $model_solicitud_compra_pieza->codigo_estado = 0; //Asignado
                    $model_solicitud_compra_pieza->origen_solicitud = 1;// Solicitud de pieza por parte del taller
                    $model_solicitud_compra_pieza->id_servicio = $model_orden_trabajo->idCotizacionTaller->idSolicitudServicioTaller->id_servicio;
                    $model_solicitud_compra_pieza->id_cotizacion_taller = $model_orden_trabajo->id_cotizacion_taller;
                    $model_solicitud_compra_pieza->id_orden_trabajo = $model_orden_trabajo->id;

                    if ($model_solicitud_compra_pieza->save()) {
                        //Se cambia el estado de la solicitud en el pipeline de compra de pieza
                        $estado_compra_pieza = EstadoCompraPieza::findOne(['codigo' => 0])->id; //Asignado
                        $model_pipeline_compra_pieza = new PipelineCompraPieza();
                        $model_pipeline_compra_pieza->fecha = date('Y-m-d');
                        $model_pipeline_compra_pieza->id_solicitud_compra_pieza = $model_solicitud_compra_pieza->id;
                        $model_pipeline_compra_pieza->id_estado_compra_pieza = $estado_compra_pieza;
                        $model_pipeline_compra_pieza->save();
                        unset($model_pipeline_compra_pieza);

                        \Yii::$app->getSession()->setFlash('success', '!Se actualizó la Orden de Trabajo con éxito!');
                        return $this->redirect(['servicio/indexordentrabajo']);
                    } else{
                        \Yii::$app->getSession()->setFlash('error', '!Hubo un error generando la solicitud de compra de pieza!');
                        return $this->render([ 'servicio/ordentrabajo', 'id' => $id ]);
                    }
                } else{
                    //Secambia al estado en proceso, saltando la compra de pieza
                    /* Pipeline -> Estado Por pieza */
                    $estado = EstadoServicio::findOne(['codigo' => 3])->id;
                    $model_pipeline = new PipelineServicio();
                    $model_pipeline->fecha = date('Y-m-d');
                    $model_pipeline->id_servicio = $model_orden_trabajo->idCotizacionTaller->idSolicitudServicioTaller->id_servicio;
                    $model_pipeline->id_estado_servicio = $estado;
                    $model_pipeline->save();
                    unset($model_pipeline);

                    /* Pipeline -> Estado en proceso */
                    $estado = EstadoServicio::findOne(['codigo' => 4])->id;
                    $model_pipeline = new PipelineServicio();
                    $model_pipeline->fecha = date('Y-m-d');
                    $model_pipeline->id_servicio = $model_orden_trabajo->idCotizacionTaller->idSolicitudServicioTaller->id_servicio;
                    $model_pipeline->id_estado_servicio = $estado;
                    $model_pipeline->save();
                    unset($model_pipeline);

                    \Yii::$app->getSession()->setFlash('success', '!Se actualizó la Orden de Trabajo con éxito!');
                    return $this->redirect(['servicio/indexordentrabajo']);
                }
                

            } else{
                \Yii::$app->getSession()->setFlash('error', '!Hubo un error!');
                return $this->render([ 'servicio/ordentrabajo', 'id' => $id ]);
            }
        } else{
            \Yii::$app->getSession()->setFlash('error', '!Hubo un error!');
            return $this->render([ 'servicio/ordentrabajo', 'id' => $id ]);
        }
    }

    /**
     * Index Orden Trabajo
     * @return mixed
     */
    public function actionIndexordentrabajo()
    {
        $searchModel = new OrdenTrabajoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy('id DESC');

        $model_orden_trabajo = new OrdenTrabajo();

        return $this->render('index-orden-trabajo', [
            'dataProvider' => $dataProvider,
            'model_orden_trabajo' => $model_orden_trabajo,
        ]);
    }

    /**
     * View de la Orden de Trabajo
     * @param integer $id
     * @return mixed
     */
    public function actionViewordentrabajo( $id )
    {
        $model_orden_trabajo = OrdenTrabajo::findOne($id);
        $model_cotizacion_taller = CotizacionTaller::findOne($model_orden_trabajo->id_cotizacion_taller);
        $model = $this->findModel($model_orden_trabajo->idCotizacionTaller->idSolicitudServicioTaller->id_servicio);
        $model_vehiculo = Vehiculo::findOne([ 'id' => $model->id_vehiculo ]);

        $searchModel_cotizacion_actividad = new CotizacionTallerActividadSearch(['id_cotizacion_taller' => $model_cotizacion_taller->id]);
        $dataProvider_cotizacion_actividad = $searchModel_cotizacion_actividad->search(Yii::$app->request->queryParams);

        $searchModel_cotizacion_pieza = new CotizacionTallerPiezaSearch(['id_cotizacion_taller' => $model_cotizacion_taller->id]);
        $dataProvider_cotizacion_pieza = $searchModel_cotizacion_pieza->search(Yii::$app->request->queryParams);

        return $this->render('view-orden-trabajo', [
            'model' => $model,
            'model_orden_trabajo' => $model_orden_trabajo,
            'model_vehiculo' => $model_vehiculo,
            'model_cotizacion_taller' => $model_cotizacion_taller,
            'dataProvider_cotizacion_actividad' => $dataProvider_cotizacion_actividad,
            'dataProvider_cotizacion_pieza' => $dataProvider_cotizacion_pieza,
        ]);
    }

    /**
     * Cancelacion de la Orden de Trabajo
     * @param integer $id
     * @return mixed
     */
    public function actionCancelarordentrabajo( )
    {
        if (Yii::$app->request->post()) {

            $id = Yii::$app->request->post('id');
            $model_orden_trabajo = OrdenTrabajo::findOne($id);
            $motivo = Yii::$app->request->post('OrdenTrabajo');

            if ($model_orden_trabajo->load(Yii::$app->request->post())) {
                
                $model_orden_trabajo->estado = 2; //Orden de trabajo cancelada
                $model_orden_trabajo->aprobacion_cancelacion = 0;
                $model_orden_trabajo->motivo_cancelacion = $motivo['motivo_cancelacion'];
                $model_orden_trabajo->save();
            }

            return;
        }
    }

    /**
     * Obtener el Motivo de Cancelacion de la Orden de Trabajo
     * @param integer $id
     * @return mixed
     */
    public function actionGetmotivocancelacion( )
    {
        if (Yii::$app->request->post()) {

            $id = Yii::$app->request->post('id');
            $model_orden_trabajo = OrdenTrabajo::findOne($id);

            if ( $model_orden_trabajo->motivo_cancelacion == 1 ) {
                return 'Elevado costo de mano de pieza cotizada';
            }
        }
    }
  /**
     * Obtener el Motivo de Cancelacion de la Orden de Trabajo
     * @param integer $id
     * @return mixed
     */
    public function actionGetprecio( )
    {
        if (Yii::$app->request->post()) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            //$servicio = Yii::$app->request->post('servicio');
            $pieza = Yii::$app->request->post('pieza');
            /*$labor = Yii::$app->request->post('labor');
            $taller = Yii::$app->request->post('taller');
            $categoria = Yii::$app->request->post('categoria');*/
            $consulta = PiezaServicioTaller::findOne($pieza);
            return $consulta;
        }
     
    }
    /**
     * Checklist de recepcion del vehiculo
     * @param integer $id id de la solicitud
     * @return mixed
     */
    public function actionChecklistrecepcion($id)
    {
        $model = $this->findModel($id);
        $model_solicitud_servicio_taller = SolicitudServicioTaller::find()->where([ 'id_servicio' => $model->id ])->one();
        $model_taller = Taller::find()->where(['id_taller'=>$model_solicitud_servicio_taller->id_taller])->one();
        $model_serv_encuesta = new ServicioEncuesta();
        $model_cotizacion_taller = CotizacionTaller::find()->where([ 'id_solicitud_servicio_taller' => $model_solicitud_servicio_taller->id, ])->one();
        
        if ($model->load(Yii::$app->request->post())) {

            $conformes = Yii::$app->request->post('conforme');
            $inconformes = Yii::$app->request->post('inconforme');
            
            $serv_inconforme[] = '';   
            $cont = 0;       

            if ($model->save(false)) {
                $model_cotizacion_taller_actividad = CotizacionTallerActividad::find()->where([ 'id_cotizacion_taller' => $model_cotizacion_taller->id ])->all();

                foreach ($model_cotizacion_taller_actividad as $i => $actividad) {
                    if ($conformes) {
                         
                    foreach ($conformes as $k => $conforme) {

                        // print_r($conformes);
                        // echo $actividad->id;
                        if ( $actividad->id == $conforme ) {
                            $actividad->conforme_recepcion = 1;
                            
                        }
                    }
                    }
                    if ($inconformes) {
                        
                    foreach ($inconformes as $kk => $inconforme) {
  
                        if ( $actividad->id == $inconforme ) {
                       $serv_inconforme[$cont] = $actividad->idTipoServicio->nombre;
                        $cont++;
                            
                            $actividad->conforme_recepcion = 0;
                             
                        }
                    }
                    }

                     $actividad->save(false);
                }
                $servicios_negativos = '';
                foreach ($serv_inconforme as $key => $value) {
                     $servicios_negativos = '* '.$value.'</br>'.$servicios_negativos;

                }
                // echo $servicios_negativos;
                // echo $model_taller->email_representante;
                // echo $model_solicitud_servicio_taller->id;
         
        if ($servicios_negativos) {
                 
                if(!$this->notificacion2($servicios_negativos,$model_taller->email_representante, $model_solicitud_servicio_taller->id,$model_taller->nombre_representante)){
                        \Yii::$app->getSession()->setFlash('error', '¡Error en el envío de notificación!');
                }
             }     

                /* Pipeline -> Estado entregado */
                $estado_0 = EstadoServicio::findOne(['codigo' => 6])->id;
                $model_pipeline = new PipelineServicio();
                $model_pipeline->fecha = $model->fecha = date('Y-m-d');
                $model_pipeline->id_servicio = $model->id;
                $model_pipeline->id_estado_servicio = $estado_0;
                $model_pipeline->save();
                unset($model_pipeline);
 
                if ( $_POST['uno']) {
                    # code...
                $uno = new ServicioEncuesta();
                $uno->id_servicio = $model->id;
                $uno->id_pregunta = 1;
                $uno->respuesta = $_POST['uno'];
                $uno->save();
                }

                if ($_POST['dos']) {
                    # code...
                $dos = new ServicioEncuesta();
                $dos->id_servicio = $model->id;
                $dos->id_pregunta = 2;
                $dos->respuesta = $_POST['dos'];
                $dos->save();
                }

                if ($_POST['tres']) {
                    # code...
                $tres = new ServicioEncuesta();
                $tres->id_servicio = $model->id;
                $tres->id_pregunta = 3;
                $tres->respuesta = $_POST['tres'];
                $tres->save();
                }

                if ($_POST['cuatro']) {
                    # code...
                $cuatro = new ServicioEncuesta();
                $cuatro->id_servicio = $model->id;
                $cuatro->id_pregunta = 4;
                $cuatro->respuesta = $_POST['cuatro'];
                $cuatro->save();
                }

                if ($_POST['cinco']) {
                    # code...
                $cinco = new ServicioEncuesta();
                $cinco->id_servicio = $model->id;
                $cinco->id_pregunta = 5;
                $cinco->respuesta = $_POST['cinco'];
                $cinco->save();
                }


                \Yii::$app->getSession()->setFlash('success', 'Check-list de recepción realizado con éxito!');
                return $this->redirect('../gestionservicio');
            }

        } else{

            $query = new query();
            $query->select('es.id, es.codigo')
                ->from('pipeline_servicio p')
                ->join('INNER JOIN','estado_servicio es','es.id = p.id_estado_servicio')
                ->where('p.id_servicio = '. $model->id )
                ->orderBy('p.id desc');

            $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
            $estado = Yii::$app->db->createCommand($sql)->queryOne();

            if ($estado['codigo'] == 5) {
                $model_vehiculo = Vehiculo::findOne($model->id_vehiculo);
                $model_orden_trabajo = OrdenTrabajo::find()->where([ 'id_cotizacion_taller' => $model_cotizacion_taller->id ])->one();

                $searchModel_cotizacion_actividad = new CotizacionTallerActividadSearch(['id_cotizacion_taller' => $model_cotizacion_taller->id]);
                $dataProvider_cotizacion_actividad = $searchModel_cotizacion_actividad->search(Yii::$app->request->queryParams);

                return $this->render( 'checklist-recepcion', [
                    'model' => $model,
                    'model_vehiculo' => $model_vehiculo,
                    'model_solicitud_servicio_taller' => $model_solicitud_servicio_taller,
                    'model_cotizacion_taller' => $model_cotizacion_taller,
                    'model_orden_trabajo' => $model_orden_trabajo,
                    'dataProvider_cotizacion_actividad' => $dataProvider_cotizacion_actividad,
                    'model_serv_encuesta' => $model_serv_encuesta,
                ]);
            } else{
                \Yii::$app->getSession()->setFlash('error', 'Error! La solicitud no esta Finalizada');
                return $this->redirect('../gestionservicio');
            }
        }
    }


    /**
     * Enviar correo al taller.
     * @return mixed
     */
    public function notificacion2($servicios_negativos, $email,$solicitud,$nombre_rep)
    {
        $envio = 0;
        $mensaje_sup ='  <p> Hola '.$nombre_rep.'<br>
                        Le informamos que la solicitud de servicio Número '.$solicitud.' se califico con inconformidad en los siguientes servicios:
                    </p>
                    <p>
                         
                        <strong>Servicios: </strong>: '.$servicios_negativos.'<br>
                         
                    </p>
                    <br>
                    <hr>
                    <p style="color:#999;">Favor <strong>no</strong> responder este correo.</p>
        ';
        if (!\Yii::$app  ->mailer->compose()
                        ->setFrom('notificaciones.km100@gmail.com')
                        ->setTo($email)
                        ->setSubject('Km100 - Notificación de inconformidad de servicios')
                        ->setHtmlBody($mensaje_sup)
                        ->send())
        {
            $envio=0;
        }
     

        if ($envio==0){
            return true;
        }else{
            return false;
        }
    }
 
    /**
     * Lists servicios cerrados.
     * @return mixed
     */
    public function actionIndexRecord()
    {
        $searchModel = new ServicioSearch();
        $dataProvider = $searchModel->searchClose(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy('id ASC');

        return $this->render('index-record', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Solicitud de cotizacion de piezas para una cotización de taller
     * @param $id //Id de la solicitud de servicio
     * @return mixed
     * @author Zulma Hernández
     */
    public function actionSolicitudcotizacionpiezas($id)
    {
        if (Yii::$app->request->post()) {

            //$id_solicitud = Yii::$app->request->post('id');

            $model = $this->findModel($id);
            $model->solicita_cotizacion = 1; //Se marca activa la solicitud de cotización de pieza
            $model->save();

            $model_cotizacion = CotizacionTaller::find()->joinWith('idSolicitudServicioTaller')->where('solicitud_servicio_taller.id_servicio = '.$model->id )->one();

            \Yii::$app->getSession()->setFlash('success', 'Se creó la solicitud de cotización de piezas con éxito!');
            return $this->redirect(['analisisviabilidad', 'id' => $model_cotizacion->id ]);
        }
    }
    
    /**
     * Displays a single Servicio model.
     * @param integer $id, $origen //$origen Saber de dónde vengo (Listado de solicitudes o Gestión de servicios)
     * @return mixed
     */
    public function actionFinalizarServicio($id)
    {
        $model = $this->findModel($id);
        if($model->load(Yii::$app->request->post())) {
            //update obervacion_finalizado
            $model->update();
            /* Pipeline -> Estado Cancelado */
            $estado_0 = EstadoServicio::findOne(['codigo' => 5])->id;
            $model_pipeline = new PipelineServicio();
            $model_pipeline->fecha = $model->fecha = date('Y-m-d');
            $model_pipeline->id_servicio = $model->id;
            $model_pipeline->id_estado_servicio = $estado_0;
            $model_pipeline->save();
            unset($model_pipeline);
            \Yii::$app->getSession()->setFlash('success', 'La solicitud esta Finalizada');
            return $this->redirect('../indextaller');
        }

        $model_cotizacion_taller = CotizacionTaller::findOne($id);
        $model_vehiculo = Vehiculo::findOne([ 'id' => $model->id_vehiculo ]);
        
        $searchModel_caso = new CasoServicioSearch([ 'id_servicio' => $model->id ]);
        $dataProvider_caso = $searchModel_caso->search(Yii::$app->request->queryParams);

        $model->scenario = 'finaliza_taller';
        
        return $this->render('finalizar-servicio', [
            'model' => $model,
            'model_vehiculo' => $model_vehiculo,
            'model_cotizacion_taller' => $model_cotizacion_taller,
            'dataProvider_caso' => $dataProvider_caso
        ]);
    }

    /**
     * Analista
     * @return mixed
     */
    public function actionIndexanalista()
    {
        
        $id_analista = User::findOne(Yii::$app->user->id);
        $searchModel = new SolicitudServicioTallerSearch();
        $dataProvider = $searchModel->searchT(Yii::$app->request->queryParams, $id_analista);
        $dataProvider->query->orderBy('id ASC');

        return $this->render('index-analista', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    public function actionViewcotizacionanalista( $id )
    {

        //$model_cotizacion_taller = CotizacionTaller::findOne($id);
        $model_cotizacion_taller = CotizacionTaller::findOne(['id_solicitud_servicio_taller' => $id]);
        if($model_cotizacion_taller->load(Yii::$app->request->post())) {
            if($_POST['CotizacionTaller']['estado'] == 3) {
                $email_taller =  $model_cotizacion_taller->idSolicitudServicioTaller->idTaller->email_representante;
                $motivo = $_POST['CotizacionTaller']['observacion_cancela'];

                //Se guarda el mensaje que indique el analista
                $model_servicio_mensaje = new ServicioMensaje();
                $model_servicio_mensaje->mensaje = $motivo;
                $model_servicio_mensaje->fecha = date("Y-m-d H:i:s");
                $model_servicio_mensaje->motivo = 0; //Cotización
                $model_servicio_mensaje->estado = 1; //Habilitado para ser visto por el taller
                $model_servicio_mensaje->codigo_pipeline = 2; //Aprobación
                $model_servicio_mensaje->id_servicio = $model_cotizacion_taller->idSolicitudServicioTaller->id_servicio;
                $model_servicio_mensaje->id_user = Yii::$app->user->identity->id;
                $model_servicio_mensaje->save();

                /*Mail*/
                try {
                    $mensaje_sup ='<p>Apreciado RSA '.$model_cotizacion_taller->idSolicitudServicioTaller->idTaller->nombre.' <br>
                                    La cotización de la Solictud No.'.$model_cotizacion_taller->idSolicitudServicioTaller->id_servicio.' fue rechazada, Km100 quiere renegociar
                                </p>
                                <p> 
                                    Motivo: '.$motivo.'<br>
                                    Ingrese al sistema para editar la cotización.
                                </p>
                            ';

                    Yii::$app->mailer->compose(['html'=>'layout'], ['content'=> $mensaje_sup])
                        ->setFrom([Yii::$app->params['adminEmail'] => 'KM100'])
                        ->setTo($email_taller)
                        ->setSubject('Notificación de Negociación')
                        ->send();
                } catch (Exception $e) {
                    \Yii::$app->getSession()->setFlash('error', 'Error al enviar el correo de notificacion');
                }

                if ($model_cotizacion_taller->save(false)) {
                    \Yii::$app->getSession()->setFlash('success', 'Cotización rechazada!');
                    return $this->redirect('../indexanalista');
                } else{
                    \Yii::$app->getSession()->setFlash('error', 'Error al rechazar la cotización');
                    return $this->redirect('../indexanalista');
                }
            } else{
                if ($model_cotizacion_taller->save(false)) {
                    \Yii::$app->getSession()->setFlash('success', 'Cotización lista para ser usada en el Análisis de Viabilidad');
                    return $this->redirect('../indexanalista');
                } else{
                    \Yii::$app->getSession()->setFlash('error', 'Error al guardar la información de la cotización');
                    return $this->redirect('../indexanalista');
                }
            }
        }
        
        
        $id_servicio = $model_cotizacion_taller->idSolicitudServicioTaller->id_servicio;
        $id_taller = $model_cotizacion_taller->idSolicitudServicioTaller->id_taller;
        $model = $this->findModel($id_servicio);
        $model_vehiculo = Vehiculo::findOne([ 'id' => $model->id_vehiculo ]);
        $model_taller = Taller::findOne($id_taller);

        $searchModel_cotizacion_pieza = new CotizacionTallerPiezaSearch(['id_cotizacion_taller' => $model_cotizacion_taller->id]);
        $dataProvider_cotizacion_pieza = $searchModel_cotizacion_pieza->search(Yii::$app->request->queryParams);

        $searchModel_cotizacion_actividad = new CotizacionTallerActividadSearch(['id_cotizacion_taller' => $model_cotizacion_taller->id]);
        $dataProvider_cotizacion_actividad = $searchModel_cotizacion_actividad->search(Yii::$app->request->queryParams);

        $model_servicio_mensaje = ServicioMensaje::find()->where([ 'id_servicio' => $model->id ])->all();

        return $this->render('view-cotizacion-analista', [
            'model' => $model,
            'model_vehiculo' => $model_vehiculo,
            'model_taller' => $model_taller,
            'model_cotizacion_taller' => $model_cotizacion_taller,
            'dataProvider_cotizacion_pieza' => $dataProvider_cotizacion_pieza,
            'dataProvider_cotizacion_actividad' => $dataProvider_cotizacion_actividad,
            'model_servicio_mensaje' => $model_servicio_mensaje,
        ]);
        
    }

    /**
     * Index del Gerente Tecnico del Mantenimiento Correctivo
     * @return mixed
     * @author Zulma Hernández
     */
    public function actionIndexgerente()
    {
        $searchModel = new ServicioSearch();
        $dataProvider = $searchModel->searchGerente(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy('id DESC');

        return $this->render('index-gerente', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    public function actionVerchecklist($id,$id_servicio)
    {
        //die('hola');
        $cavaluo = ChecklistAvaluo::findOne(['id_analista' => $id]);
        //die($id);
        $searchModel = new ChecklistavaluoPiezasSearch(['id_checklist_avaluo' => $cavaluo->id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $modelvehiculo = Servicio::findOne([ 'id' => $id_servicio ])->id_vehiculo;
        $n_sol =  Servicio::findOne([ 'id' => $id_servicio ]);
        $model_vehiculo = Vehiculo::findOne([ 'id' => $modelvehiculo ]);
        //$clavaluo = ChecklistAvaluo::findOne(['id_analista' => $cotanalista->id]);
 
        return $this->render('viewchecklist', [
            'model' => $searchModel,
            'dataProvider' => $dataProvider,
            'model_vehiculo' => $model_vehiculo,
            'n_sol' => $n_sol,
            'clavaluo' => $cavaluo,
        ]);
    }

    /**
     * Metedo que obtiene las piezas asociadas a un servicio de taller especifico
     * @return mixed
     * @author Zulma Hernández
     */
    public function actionGetpiezas($id)
    {

        if (\Yii::$app->request->isAjax ) {

            $piezas = PiezaServicioTaller::find()->where([ 'id_servicio_taller' => $id, 'estado' => 1 ])->all();

            if (count($piezas) > 0) {
                echo "<option value=''> Selecciona </option>";
                foreach ($piezas as $aux) {
                    echo "<option value='".$aux->id."'>" . $aux->nombre ."</option>";
                }
            } else {
                echo "<option> -- </option>";
            }
        }
        \Yii::$app->end();
    }

    /**
     * Index de vehculos pendietes de enviar de la estacion al taller
     * @return mixed
     * @author Zulma Hernández
     */
    public function actionIndexenvioestacion()
    {

        $searchModel = new ServicioSearch();
        $dataProvider = $searchModel->searchEnvioEstacion(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy('id ASC');

        $talleres = ArrayHelper::map(Taller::find()->orderBy('nombre')->all(), 'id_taller', 'nombre');

        return $this->render('index-envio-estacion', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'talleres' => $talleres,
        ]);
        
    }

    /**
     * Envio de vehiculo a taller
     * @return mixed
     * @author Zulma Hernández
     */
    public function actionEnviotaller($id)
    {

        $model = $this->findModel($id);
        $model_taller = Taller::findOne(SolicitudServicioTaller::find()->where(['id_servicio' => $id])->one()->id_taller);

        if( Yii::$app->request->post() ) {

            $servicio = Yii::$app->request->post('Servicio');

            if ( $servicio['accion'] == 0 ) {
                $model->aprobacion_estacion = 1;
                $model->km_estacion = $servicio['km_estacion'];
                $model->combustible_estacion = $servicio['combustible_estacion'];
                $model->fecha_envio_estacion = date('Y-m-d');

                if ( $model->update() ) {
                    /* Pipeline -> Transito */
                    $estado = EstadoServicio::findOne(['codigo' => 0])->id;
                    $model_pipeline = new PipelineServicio();
                    $model_pipeline->fecha = date('Y-m-d');
                    $model_pipeline->id_servicio = $model->id;
                    $model_pipeline->id_estado_servicio = $estado;

                    //Se actualiza el estado actual del vehiculo a Taller
                    $model_estado_vehiculo = new EstadoVehiculo();
                    $model_estado_vehiculo->isNewRecord = true;
                    $model_estado_vehiculo->id_estado = Estado::findOne(['id_tipo_estado' => '1', 'nombre' => 'Taller' ])->id;
                    $model_estado_vehiculo->id_vehiculo = $model->id_vehiculo;
                    $model_estado_vehiculo->save();
                    unset($model_estado_vehiculo);

                    try {
                        //Email al taller
                        $mensaje_sup ='<p>Apreciado RSA '.$model_taller->nombre.', <br>
                                Km100 le informa que se le ha asignado una nueva solicitud de reparación:
                                </p>
                                <p>
                                    <strong>Nº Solicitud: </strong>'.$model->id.'<br>
                                    <strong>Nº Placa: </strong>'.$model->idVehiculo->placa.'<br>
                                </p>
                        ';
                        Yii::$app->mailer->compose(['html'=>'layout'], ['content'=> $mensaje_sup])
                            ->setFrom([Yii::$app->params['adminEmail'] => 'KM100'])
                            ->setTo($model_taller->email_taller,$model_taller->email_representante)
                            ->setSubject('Nueva Solicitud de Reparación Recibida')
                            ->send();

                    } catch (Exception $e) {
                        \Yii::$app->getSession()->setFlash('error', 'Error al enviar el correo de notificacion');
                    }

                    if ( $model_pipeline->save() ) {
                        \Yii::$app->getSession()->setFlash('success', 'Envío a taller aprobado!');
                        return $this->redirect(['indexenvioestacion']);
                    } else{
                        \Yii::$app->getSession()->setFlash('error', 'Ocurrió un error!');
                        return;
                    }
                } else{
                    \Yii::$app->getSession()->setFlash('error', 'Ocurrió un error!');
                    return;
                }
            } elseif ( $servicio['accion'] == 1 ) {
                $model->observacion_cancelado = $servicio['observacion_cancelado'];
                $model->usuario_cancela = Yii::$app->user->identity->id;

                if ( $model->update()) {
                    /* Pipeline -> Estado Cancelado */
                    $estado_0 = EstadoServicio::findOne(['codigo' => 12])->id;
                    $model_pipeline = new PipelineServicio();
                    $model_pipeline->fecha = date('Y-m-d');
                    $model_pipeline->id_servicio = $model->id;
                    $model_pipeline->id_estado_servicio = $estado_0;
                    $model_pipeline->save();
                    unset($model_pipeline);

                    /*Mail*/
                    //Email al Planner
                    $planners = User::find()->joinWith('authAssignments')->andWhere('auth_assignment.item_name = "planner"')->all();
                    
                    foreach ($planners as $user) {
                        $correos[] = $user->email;
                    }
                    try {
                        $mensaje_sup ='  <p> Km100 Le informa que la solicitud de servicio fue cancelada por la estación:
                                        </p>
                                        <p>
                                            <strong>Servicio:</strong> Nº'.$model->id.'<br>
                                            <strong>MVA Ficha: </strong>'.$model->ficha.'<br>
                                            <strong>Nº Placa: </strong>'.$model->placa.'<br>
                                            <strong>Motivo de cancelación: </strong>'.$servicio['observacion_cancelado'].'
                                        </p>
                        ';

                        Yii::$app->mailer->compose(['html'=>'layout'], ['content'=> $mensaje_sup])
                                        ->setFrom([Yii::$app->params['adminEmail'] => 'KM100'])
                                        ->setTo($correos)
                                        ->setSubject('Solicitud de Servicio Cancelada')
                                        ->send();
                    } catch (Exception $e) {
                        \Yii::$app->getSession()->setFlash('error', 'Error al enviar el correo de notificación');
                    }
                    /*Fin Mail*/
                    
                    \Yii::$app->getSession()->setFlash('success', 'Servicio Cancelado con exito!');
                    return $this->redirect(['indexenvioestacion']);
                } else{
                    \Yii::$app->getSession()->setFlash('error', 'Ocurrió un error!');
                    return;
                }
            }
        } else{
            $model->scenario = 'accion';
            //, Servicio::SCENARIO_ENVIAR_ESTACION];
            $model_vehiculo = Vehiculo::findOne($model->id_vehiculo);
            

            return $this->render('envio-taller', [
                'model' => $model,
                'model_vehiculo' => $model_vehiculo,
                'model_taller' => $model_taller,
            ]);
        }
    }

    /**
     * Index de solicitudes pendientes de asignar taller (rol plenner)
     * @author Zulma Hernández
     * @return mixed
     */
    public function actionIndexasignacion()
    {
        $searchModel = new ServicioSearch();
        $dataProvider = $searchModel->searchAsignacion(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy('id DESC');

        return $this->render('index-asignacion', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    /**
     * Visualizar orden de trabajo en PDF
     * @author Zulma Hernández
     */
    public function actionImprimirorden($id)
    {
        ini_set("memory_limit","128M");

        //Obtengo la orden de trabajo solicitada
        $model_orden_trabajo = OrdenTrabajo::findOne( $id );

        //Obtengo la solicitud compra pieza asociada a la orden de compra
       /* $model = $this->findModel( $model_orden_compra->id_solicitud_compra_pieza );
        $model_cotizacion_taller = CotizacionTaller::findOne( $model->id_cotizacion_taller );
        $model_servicio = Servicio::findOne( $model->id_servicio );
        $model_vehiculo = Vehiculo::findOne( $model_servicio->id_vehiculo );
        //Obtengo el proveedor asociado a la orden de compra
        $model_proveedor = Proveedor::findOne( $model_orden_compra->id_proveedor );
        //Obtengo las piezas aprobadas para la orden de compra
        $piezas = CotizacionProveedorPieza::find()->where(['id_cotizacion_proveedor' => $model_orden_compra->id_cotizacion_proveedor, 'aprobacion' => 2 ])->all();
        //$cotizaciones_piezas = CotizacionProveedorPieza::find()->joinWith('idCotizacionProveedor')->joinWith('idCotizacionProveedor.idSolicitudCompraProveedor')->where('solicitud_compra_proveedor.id_solicitud_compra_pieza ='.$model->id .' AND cotizacion_proveedor_pieza.aprobacion = 1')->all();
*/
        $pdf = new Pdf([
            'content' => $this->renderPartial('_pdf-orden-trabajo',[ /*'model_orden_compra' => $model_orden_compra, 'model_proveedor' => $model_proveedor, 'piezas' => $piezas, 'model_cotizacion_taller' => $model_cotizacion_taller, 'model_vehiculo' => $model_vehiculo */]),
            'destination' => Pdf::DEST_BROWSER,
            'mode' => Pdf::MODE_CORE,
            'format' => Pdf::FORMAT_A4,
            'filename' => 'OC_'.$id.'.pdf',
            'options' => [
                'title' => 'ORDEN DE TRABAJO',
                'subject' => 'OT_'.$id,
            ],
        ]);
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');

        return $pdf->render(); 
        exit;
    }

}
