<?php

namespace app\controllers;

use Yii;
use app\models\AnalistaMantenimiento;
use app\models\UserSearch;
use app\models\AuthAssignment;
use app\models\AnalistaMantenimientosSearch;
use app\models\CotizacionTallerAnalista;
use app\models\ChecklistAvaluoPiezasSearch;
use app\models\CotizacionTallerAnalistaSearch;
use app\models\CotizacionTaller;
use yii\web\Controller;
use yii\helpers\Json;
use yii\db\Query;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use dektrium\user\helpers\Password;
use app\models\TallerSearch;
use app\models\Taller;
use app\models\Pieza;
use app\models\TipoPieza;
use app\models\ChecklistAvaluoPieza;
use app\models\ChecklistAvaluo;
use app\models\ChecklistAvaluoSearch;
use app\models\Servicio;
use app\models\Vehiculo;
use app\models\User;
use app\models\SolicitudServicioTaller;
use app\models\SolicitudServicioTallerSearch;

/**
 * AnalistaMantenimientoController implements the CRUD actions for AnalistaMantenimiento model.
 */
class AnalistaMantenimientoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AnalistaMantenimiento models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AnalistaMantenimientosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AnalistaMantenimiento model.
     * @param integer $id
     * @return mixed
     */

     public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Displays a single AnalistaMantenimiento model.
     * @param integer $id
     * @return mixed
     */

     public function actionViewchecklist($id)
    {
        return $this->render('view', [
            'searchModel' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AnalistaMantenimiento model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AnalistaMantenimiento();
        $usuario = new UserSearch();
        $rol = new AuthAssignment();
        $length = 10;
        $chars = array_merge(range(0,9), range('a','z'), range('A','Z'));
        shuffle($chars);
          $password = implode(array_slice($chars, 0, $length));
         

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

          $usuario->username = $model->user;
          $usuario->email=$model->correo;
          $usuario->password_hash = Password::hash($password);
          $usuario->auth_key=0;
          $usuario->created_at=0;
          $usuario->updated_at=0;
          $usuario->flags=0;
          $usuario->cargo_gerencial=0;
          $usuario->cargo=0;
          $usuario->id_analista = $model->id;
          $usuario->confirmed_at = 1491044212;
          $usuario->save(false);

          $rol->item_name = 'analista';
          $rol->user_id = $usuario->id;
          $rol->save(false);

          $correo_envio = $model->correo;
          $login =  $usuario->username;

              if(!$this->notificacion($model,$password, $usuario)){
                        \Yii::$app->getSession()->setFlash('error', '¡Error en el envío de notificación!');
                    }



            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }



    public function notificacion($model,$password, $usuario)
    {
      $envio = 0;
        $mensaje_sup =' <p>'.$model->nombre.', <br>
                        Km100 le informa que ha sido registrado como Analista de Mantenimiento, sus datos son:
                    </p>
                    <p>
                        <strong>Nombre de Usuario: </strong>'.$usuario->username.'<br>
                        <strong>Clave de Acceso: </strong>'.$password.'<br>
                    </p>
        ';
        if (!Yii::$app->mailer->compose(['html'=>'layout'], ['content'=> $mensaje_sup])
                        ->setFrom([Yii::$app->params['adminEmail'] => 'KM100'])
                        ->setTo($model->correo)
                        ->setSubject('Notificación de Registro')
                        ->send())
        {
            $envio=0;
        }
     

      if ($envio==0){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Updates an existing AnalistaMantenimiento model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * Listado de Solicitudes
     */
    public function actionListadochecklist()
    {
        /*$id_analista = User::findOne(Yii::$app->user->id)->id_analista;

        $searchModel = new CotizacionTallerAnalistaSearch(['id_analista' => $id_analista]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        */

        $id_analista = Yii::$app->user->id;

        $searchModel = new ChecklistAvaluoSearch(['id_analista' => $id_analista]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('listadochecklist', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionVerchecklist($id)
    {
        $cavaluo = ChecklistAvaluo::findOne(['id' => $id]);
        $searchModel = new ChecklistAvaluoPiezasSearch(['id_checklist_avaluo' => $id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $n_sol =  Servicio::findOne([ 'id' => $cavaluo->id_servicio ]);
        $model_vehiculo = Vehiculo::findOne([ 'id' => $n_sol->id_vehiculo ]);

        return $this->render('veravaluo', [
            'model' => $searchModel,
            'dataProvider' => $dataProvider,
            'model_vehiculo' => $model_vehiculo,
            'n_sol' => $n_sol,
            'clavaluo' => $cavaluo,
        ]);
    }



        public function actionVer($id)
    {
        
        $searchModel = new CotizacionTallerAnalistaSearch(['id_analista' => $id]);

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('ver',[
            'dataProvider' => $dataProvider,
          ]);
    }
        public function actionVeravaluo($id, $idcotizaciontaller, $idanalista)
    {
        
        $cotanalista = CotizacionTallerAnalista::findOne(['id' => $id]);
        
        $cavaluo = ChecklistAvaluo::findOne(['id_cotizacion_taller_analista' => $cotanalista->id]);
        $searchModel = new ChecklistavaluoPiezasSearch(['id_checklist_avaluo' => $cavaluo->id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);



        $ctaller = CotizacionTaller::findOne([ 'id' => $cotanalista->id_cotizacion_taller]);
        $solsertaller = SolicitudServicioTaller::findOne([ 'id' => $ctaller->id_solicitud_servicio_taller]);
        $modelservicio = Servicio::findOne([ 'id' => $solsertaller->id_servicio ])->id_vehiculo;
        $n_sol =  Servicio::findOne([ 'id' => $solsertaller->id_servicio ]);
        $model_vehiculo = Vehiculo::findOne([ 'id' => $modelservicio ]);
        $clavaluo = ChecklistAvaluo::findOne(['id_cotizacion_taller_analista' => $cotanalista->id]);
           return $this->render('veravaluo', [
            'model' => $searchModel,
            'dataProvider' => $dataProvider,
            'model_vehiculo' => $model_vehiculo,
            'n_sol' => $n_sol,
            'clavaluo' => $clavaluo,
        ]);
    }

        /**
     * checklist del avalúo
     * @param integer $id
     * @return mixed
     */
  
    public function actionChecklist($id)
    {
        $clavaluo = ChecklistAvaluo::findOne($id);
        /*$analista = new AnalistaMantenimiento();
        $analista1 = new user();
        $clavaluo = new ChecklistAvaluo();*/
        $modelpieza = [new Pieza()];
        $modelcheckavap = [new ChecklistAvaluoPieza()];

        $n_sol =  Servicio::findOne([ 'id' => $clavaluo->id_servicio ]);
        
        $model_vehiculo = Vehiculo::findOne([ 'id' => $n_sol->id_vehiculo ]);

        if ($clavaluo->load(Yii::$app->request->post())) {
             $checks ="";

            if ( Yii::$app->request->post('pie') ) {
                $checkboxes = Yii::$app->request->post('pie');
                
                foreach($checkboxes as $c)
                    $checks = $checks .'*'.$c;  

            }
            /*foreach ($n_sol->solicitudServicioTallers as $value) {

                foreach ($value->cotizacionTallers as $value2) {

                  foreach ($value2->cotizacionTallerAnalistas as $value3) {
                  $id_analista = User::findOne(Yii::$app->user->id)->id_analista;
                  
                    if ($value3->idAnalista->id == $id_analista) {//mientras
                        $idctanalista = $value3->id;
                    }
                  }
                }

            }*/
            $clavaluo->danos = $checks;
            $clavaluo->diagnostico = $clavaluo->diagnostico;
            //$clavaluo->id_cotizacion_taller_analista = $idctanalista;
            $clavaluo->save(false);
               if (isset($_POST['ChecklistAvaluoPieza'][0]['pieza_id'])) {
                foreach ($_POST['ChecklistAvaluoPieza'] as $key => $tall) {
                    $nuevo_vehi_taller = new ChecklistAvaluoPieza();
            

                    $nuevo_vehi_taller->accion = $_POST['ChecklistAvaluoPieza'][$key]['accion'];
                    $nuevo_vehi_taller->id_checklist_avaluo = $clavaluo->id;
                    $nuevo_vehi_taller->pieza_id = $_POST['ChecklistAvaluoPieza'][$key]['pieza_id'];
                    $nuevo_vehi_taller->save(false);
           
                }
            }
                    
 
            return $this->redirect(['listadochecklist']);
        }else{

        return $this->render('checklist', [
            'n_sol' => $n_sol,
            'clavaluo' => $clavaluo,
            'model_vehiculo' => $model_vehiculo,
            'modelcheckavap' => (empty($modelcheckavap)) ? [new ChecklistAvaluoPieza] : $modelcheckavap,
            'modelpieza' => (empty($modelpieza)) ? [new Pieza] : $modelpieza,
        ]);}
    }
     public function actionListar($id)
    {
        
       $countpieza = Pieza::find()
       ->where(['tipo_pieza_id' => $id])
       ->count();

       $pieza = Pieza::find()
       ->where(['tipo_pieza_id' => $id])
       ->all();

       if($countpieza>0){
        echo "<option value=''> Seleccione... </option>";
        foreach($pieza as $pieza){
            echo "<option value='".$pieza->id_pieza."'>".$pieza->nombre."</option>";
        }
    }
    else{
        echo "<option>-</option>";
    }



    }
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AnalistaMantenimiento model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AnalistaMantenimiento model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AnalistaMantenimiento the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AnalistaMantenimiento::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
