<?php

namespace app\controllers;

use Yii;
use app\models\Caso;
use app\models\CasoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\data\Pagination;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\models\Empresa;
use app\models\Estacion;
use app\models\Marca;
use app\models\Modelo;
use app\models\Tipoaccidente;
use app\models\Ubicacionunidad;
use app\models\Soporte;
use app\models\EstacionUsuario;
use app\models\UploadForm;
use app\models\Vehiculo;
use app\models\CobroCaso;

use app\models\UploadFrontal;
use app\models\UploadTrasero;
use app\models\UploadIzquierdo;
use app\models\UploadDerecho;
use app\models\UploadDanio;
use app\models\UploadPlaca;
use yii\helpers\Vardumper;
/**
 * CasoController implements the CRUD actions for Caso model.
 */
class CasoController extends Controller
{
    /**
     * @inheritdoc
     */

    public $basePath ='./uploads/casos/';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'view'],
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Caso models.
     * CASOS CLIENTES: Casos asocIados al cliente registrado del usuario
     * CASOS TODOS: Todos los casos registrados
     * CASOS ESTACIONES ASOCIADAS: Casos asocados a las estaciones del usuario
     * CASOS PROPIOS: Casos registrados por el usuario
     * @return mixed
     */
    public function actionIndex()
    {
        $info = '';

        // FILTRO DE ESTACIÓN
        if(\Yii::$app->user->can('casos cliente')){
            //Se consulta el cliente asocado a las estaciones registradas al usuario
            $cliente = EstacionUsuario::find()->where(['user_id'=>Yii::$app->user->identity->ID ])->one()->estacion->idEmpresa->cliente;
            
            //Se obtienen las estaciones asociados al cliente consultado
            $filtro_estaciones = ArrayHelper::map(Estacion::find()->joinWith('idEmpresa')->where('empresa.cliente_id = '.$cliente->id)->asArray()->all(), 'id_estacion', function($model, $defaultValue) {
                                                                                            $emp = Empresa::findOne($model['id_empresa']);
                                                                                            return $model['nombre'].' ('.$emp->nombre.')';
                                                                                        });
            
            //Mensaje que se muestra en el index para indicar a que CLIENTE pertenece el usuario
            $info = '</br><h4>Nombre Comercial: <strong>'.$cliente->nombre_comercial.'</strong></h4></br>';
        } else if ( \Yii::$app->user->can('casos todos') ) {
            //Se consultan todas las estaciones
            $filtro_estaciones = ArrayHelper::map(Estacion::find()->asArray()->all(), 'id_estacion', function($model, $defaultValue) {
                                                                                            $emp = Empresa::findOne($model['id_empresa']);
                                                                                            return $model['nombre'].' ('.$emp->nombre.')';
                                                                                        });
        } else if ( \Yii::$app->user->can('casos estaciones asociadas') ){
            //Se obtienen todas las estaciones asociadas al usuario en el registro
            $estaciones_asociadas = ArrayHelper::getColumn(EstacionUsuario::find()->select('estacion_id')->where(['user_id'=>Yii::$app->user->identity->ID ])->asArray()->all(), 'estacion_id');
            
            //Se obtienen todas las estaciones en base al resultado anterior
            $filtro_estaciones = ArrayHelper::map(Estacion::find()->where(['id_estacion'=>$estaciones_asociadas])->asArray()->all(), 'id_estacion', function($model, $defaultValue) {
                                                                                            $emp = Empresa::findOne($model['id_empresa']);
                                                                                            return $model['nombre'].' ('.$emp->nombre.')';
                                                                                        });
        } else if ( \Yii::$app->user->can('casos propios') ){
            //Se consultan los casos registrados por el usuario y se obtienen los ids de las estaciones (sin repeticiones)
            $estaciones_casos_registrados = ArrayHelper::getColumn(Caso::find()->select('id_estacion')->distinct()->where(['registrado_por'=>Yii::$app->user->identity->ID ])->asArray()->all(), 'id_estacion');

            //Se obtienen las estaciones relacionadas con la busqueda anterior
            $filtro_estaciones = ArrayHelper::map(Estacion::find()->where(['id_estacion'=>$estaciones_casos_registrados])->asArray()->all(), 'id_estacion', function($model, $defaultValue) {
                                                                                            $emp = Empresa::findOne($model['id_empresa']);
                                                                                            return $model['nombre'].' ('.$emp->nombre.')';
                                                                                        });
        }

        //Mensaje se error si no es alguno de estos roles
        if( !(  \Yii::$app->user->can('admin') ||
                \Yii::$app->user->can('Casos Cliente') ||
                \Yii::$app->user->can('Casos Todos') ||
                \Yii::$app->user->can('Casos Estaciones Asociadas') ||
                \Yii::$app->user->can('Casos Propios') ) )
        {
            \Yii::$app->getSession()->setFlash('error', '¡Error! Usted no tiene Permiso para consultar Casos Registrados...');
            return $this->render('/site/index');
        }

        $searchModel = new CasoSearch();
        $dataProvider = $searchModel->searchCasos(Yii::$app->request->queryParams);
        
        //AV - Numero de casos sin cobrar.
        $cobros_pendientes = Caso::find()
        ->select('`caso`.`id_caso`')
        ->leftJoin('cobro_caso', '`cobro_caso`.`id_caso` = `caso`.`id_caso`')
        ->where(['cobro_caso.id_caso' => NULL])
        ->count();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'info' => $info,
            'searchModel' => $searchModel,
            'filtro_estaciones' => $filtro_estaciones,
            'cobros_pendientes'=> $cobros_pendientes
        ]);
    }

    /**
     * Displays a single Caso model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $caso = Caso::findOne($id);
        $estacion = Estacion::findOne($caso->id_estacion);
        //$empresa = Empresa::findOne($estacion->id_empresa);
        $modelo = Modelo::findOne($caso->id_modelo);
        $marca = Marca::findOne($modelo->id_marca);
        $tipo_acc = Tipoaccidente::findOne($caso->id_tipo_accidente);
        $ubicacion = Ubicacionunidad::findOne($caso->id_ubicacion);
        $reg_por = User::findOne($caso->registrado_por);
        $cobro = CobroCaso::find()
        ->where([ 'id_caso' => $id])->one();
        

        return $this->render('view', [
            'model' => $caso,
            'estacion' => $estacion,
            //'empresa' => $empresa,
            'modelo' => $modelo,
            'marca' => $marca,
            'tipo_acc' => $tipo_acc,
            'ubicacion' => $ubicacion,
            'reg_por' => $reg_por,
            'cobro' => $cobro
        ]);
    }

    /**
     * Creates a new Caso model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionCreate()
    {
        set_time_limit(50000);
        $model = new Caso();
        $cobro = new CobroCaso();
        /* Soportes */
        $frontal = new UploadFrontal(['scenario' => UploadFrontal::SCENARIO_CREAR]);
        $trasero = new UploadTrasero(['scenario' => UploadTrasero::SCENARIO_CREAR]);
        $izquierdo = new UploadIzquierdo(['scenario' => UploadIzquierdo::SCENARIO_CREAR]);
        $derecho = new UploadDerecho(['scenario' => UploadDerecho::SCENARIO_CREAR]);
        $danio = new UploadDanio(['scenario' => UploadDanio::SCENARIO_CREAR]);
        $placa = new UploadPlaca(['scenario' => UploadPlaca::SCENARIO_CREAR]);
        $soportes = new UploadForm();
        $cliente = (\Yii::$app->user->can('registro casos todos') )?'':EstacionUsuario::find()->where(['user_id'=>Yii::$app->user->identity->ID ])->one()->estacion->idEmpresa->cliente;
        $empresa = (\Yii::$app->user->can('registro casos todos'))?'':EstacionUsuario::find()->where(['user_id'=>Yii::$app->user->identity->ID ])->one()->estacion->idEmpresa;
        $estaciones = (\Yii::$app->user->can('registro casos estaciones asociadas'))?ArrayHelper::getColumn(EstacionUsuario::find()->select('estacion_id')->where(['user_id'=>Yii::$app->user->identity->ID ])->asArray()->all(), 'estacion_id'):'';

        //Consultando los vehiculos a los cuales se les puede registrar un caso dependoendo del permiso de registro de caso
        if ( \Yii::$app->user->can('registro casos todos') ) {
            //Se muestran todos los vehiculos que no esten en transito
            $vehiculos = ArrayHelper::map(Vehiculo::find()->select(['mva'])->where(['transito' => 0])->distinct()->all(), 'mva', 'mva');
        } else if ( \Yii::$app->user->can('registro casos cliente') ){
            //Busco las estaciones asociadas al cliente del usuario
            $v_estaciones = Estacion::find()->joinWith('idEmpresa')->where([ 'empresa.cliente_id' => $cliente ])->all();
            $ids_estaciones = ArrayHelper::getColumn($v_estaciones, 'id_estacion');
            $vehiculos = ArrayHelper::map(Vehiculo::find()->select(['mva'])->join('INNER JOIN', 'flota', 'flota.id_vehiculo = vehiculo.id')->where(['vehiculo.transito' => 0, 'flota.id_estacion' => $ids_estaciones])->andWhere('flota.fecha_fin IS NULL')->distinct()->all(), 'mva', 'mva');
        } else if( \Yii::$app->user->can('registro casos estaciones asociadas') ) {
            //Busco las estaciones asociadas al usuario
            $v_estaciones = EstacionUsuario::find()->where(['user_id'=>Yii::$app->user->identity->ID ])->all();
            $ids_estaciones = ArrayHelper::getColumn($v_estaciones, 'estacion_id');
            $vehiculos = ArrayHelper::map(Vehiculo::find()->select(['mva'])->join('INNER JOIN', 'flota', 'flota.id_vehiculo = vehiculo.id')->where(['vehiculo.transito' => 0, 'flota.id_estacion' => $ids_estaciones])->andWhere('flota.fecha_fin IS NULL')->distinct()->all(), 'mva', 'mva');
        }

        
        /* -------- */
        /*$empresa = Empresa::find()->where([ 'id_empresa' => Yii::$app->user->identity->id_empresa])->One();
        if(!$empresa){
            \Yii::$app->getSession()->setFlash('error', '¡Error! Usted no tiene Empresa asociada...');
            return $this->render('/site/index');
        }

        $estaciones = Estacion::find()->where([ 'id_empresa' => $empresa->id_empresa])->orderBy('nombre')->All();*/

        if ($model->load(Yii::$app->request->post()))
        {
            $valido=1;
            if($model->id_estacion && $model->id_modelo && $model->ficha && $model->placa && isset($model->tipo_caso)){
                $model->id_estacion = $model->id_estacion;
                $model->id_modelo = $model->id_modelo;
                $model->ficha = $model->ficha;
                $model->placa = Vehiculo::findOne($model->placa)->placa;
                $model->tipo_caso = $model->tipo_caso;
                $model->descripcion = $model->descripcion;

                if($model->tipo_caso == '0' || $model->tipo_caso == '1') // Si es ACCIDENTE o INCIDENTE
                {
                    if($model->fecha_acc_inc && $model->id_tipo_accidente/* && $model->id_ubicacion*/){
                        $model->amet = $model->amet;
                        $model->fecha_acc_inc = $model->fecha_acc_inc;
                        $model->id_tipo_accidente = $model->id_tipo_accidente;
                        if ($model->id_ubicacion) {
                            $model->id_ubicacion = $model->id_ubicacion;
                        }else{
                            $model->id_ubicacion = 6; //Caso de "No aplica"
                        }

                        if($model->tipo_caso == '0') // Si es ACCIDENTE
                        {
                            if(isset($model->fuente_accidente)){
                                $model->fuente_accidente = $model->fuente_accidente;
                                if($model->fuente_accidente == '0')  // Si es CLIENTE FINAL
                                {
                                    if($model->num_contrato && isset($model->tipo_cliente) && isset($model->cobertura_cdw) && isset($model->deducible) && isset($model->req_cotizacion)){
                                        $model->num_contrato = $model->num_contrato;
                                        $model->tipo_cliente = $model->tipo_cliente;
                                        $model->cobertura_cdw = $model->cobertura_cdw;
                                        $model->claim = $model->claim;
                                        $model->deducible = $model->deducible;
                                        $model->monto_deducible = $model->monto_deducible;
                                        $model->req_cotizacion = $model->req_cotizacion;
                                        if($model->req_cotizacion == '1')
                                            $model->tarifa_base = $model->tarifa_base;
                                            $model->piezas = $model->piezas;
                                    }else{ $valido=5; }
                                }else  // Si es Caso interno o empleado
                                {
                                    if($model->nombre_empleado){
                                        $model->nombre_empleado = $model->nombre_empleado;
                                    }else{ $valido=4; }
                                }
                            }else{ $valido=3; }
                        }
                    }else{ $valido=2; }
                }
                $model->registrado_por = Yii::$app->user->identity->id;
                $model->fecha_registro = date('Y-m-d');
            }else{ $valido=0; }

            if($valido==1){

                if($model->save()){

                    $path = $this->basePath . $model->id_caso . '/';
                    if ( !is_dir($path) ) {
                        mkdir( $path, 0777, true );
                    }

                    if (isset($_POST['CobroCaso']['monto'])) {
                        $cobro->id_caso = $model->id_caso;
                        $cobro->monto = $_POST['CobroCaso']['monto'];
                        $cobro->forma_pago = $_POST['CobroCaso']['forma_pago'];
                        $cobro->motivo_cobro = $_POST['CobroCaso']['motivo_cobro'];
                        $cobro->fecha = $_POST['CobroCaso']['fecha'];
                        $cobro->numero_factura = $_POST['CobroCaso']['numero_factura'];

                        $cobro->save();
                    }
                    $guardo_soporte = 1;

                    try {
                        
                        $frontal->imageFiles = UploadedFile::getInstances($frontal, 'imageFiles');      $frontal->upload($model->id_caso);
                        $izquierdo->imageFiles = UploadedFile::getInstances($izquierdo, 'imageFiles');  $izquierdo->upload($model->id_caso);
                        $derecho->imageFiles = UploadedFile::getInstances($derecho, 'imageFiles');      $derecho->upload($model->id_caso);
                        $trasero->imageFiles = UploadedFile::getInstances($trasero, 'imageFiles');      $trasero->upload($model->id_caso);
                        $danio->imageFiles = UploadedFile::getInstances($danio, 'imageFiles');          $danio->upload($model->id_caso);
                        $placa->imageFiles = UploadedFile::getInstances($placa, 'imageFiles');          $placa->upload($model->id_caso);
                        $soportes->imageFiles = UploadedFile::getInstances($soportes, 'imageFiles');    $soportes->upload($model->id_caso);
                    
                    } catch (Exception $e) {
                        die($e);
                    }

                    \Yii::$app->getSession()->setFlash('success', '¡El Registro del Caso y sus Soportes han sido cargados con éxito!');

                    if(!$this->notificacion($model, $empresa)){
                        \Yii::$app->getSession()->setFlash('error', '¡Error en el envío de notificación!');
                    }
                    return $this->render('create', [
                        'model' => new Caso(),
                        'estaciones' => $estaciones,
                        'soportes' => $soportes,
                        'frontal' => $frontal,
                        'trasero' => $trasero,
                        'izquierdo' => $izquierdo,
                        'derecho' => $derecho,
                        'danio' => $danio,
                        'placa' => $placa,
                        'cliente' => $cliente,
                        'empresa' => $empresa,
                        'cobro' => $cobro,
                        'vehiculos' => $vehiculos,
                    ]);

                }else{
                    /* Soportes */
                    $frontal = new UploadFrontal();
                    $trasero = new UploadTrasero();
                    $izquierdo = new UploadIzquierdo();
                    $derecho = new UploadDerecho();
                    $danio = new UploadDanio();
                    $placa = new UploadPlaca();
                    $soportes = new UploadForm();
                    
                    /* -------- */
                    \Yii::$app->getSession()->setFlash('error', '¡Error al guardar el Registro de Caso!');
                    return $this->render('create', [
                        'model' => $model,
                        'estaciones' => $estaciones,
                        'soportes' => $soportes,
                        'frontal' => $frontal,
                        'trasero' => $trasero,
                        'izquierdo' => $izquierdo,
                        'derecho' => $derecho,
                        'danio' => $danio,
                        'placa' => $placa,
                        'cliente' => $cliente,
                        'empresa' => $empresa,
                        'cobro' => $cobro,
                        'vehiculos' => $vehiculos,
                    ]);
                }
            }else{
                \Yii::$app->getSession()->setFlash('error', 'Faltan campos por llenar... '.$valido);
                return $this->render('create', [
                    'model' => $model,
                    'estaciones' => $estaciones,
                    'soportes' => $soportes,
                    'frontal' => $frontal,
                    'trasero' => $trasero,
                    'izquierdo' => $izquierdo,
                    'derecho' => $derecho,
                    'danio' => $danio,
                    'placa' => $placa,
                    'cliente' => $cliente,
                    'empresa' => $empresa,
                    'cobro' => $cobro,
                    'vehiculos' => $vehiculos,
                ]);
            }


        } else {
            return $this->render('create', [
                'model' => $model,
                'estaciones' => $estaciones,
                'soportes' => $soportes,
                'frontal' => $frontal,
                'trasero' => $trasero,
                'izquierdo' => $izquierdo,
                'derecho' => $derecho,
                'danio' => $danio,
                'placa' => $placa,
                'cliente' => $cliente,
                'empresa' => $empresa,
                'cobro' => $cobro,
                'vehiculos' => $vehiculos,
            ]);
        }
    }

    public function actionListarmodelos($id)
    {
        if (\Yii::$app->request->isAjax ) {
            $query = new Query();
            $query->select('*')
                ->from('modelo')
                ->where(['=','id_marca', $id])
                ->orderBy('nombre');
            $rows = $query->all();

            if (count($rows) > 0) {
                echo "<option value=''> Seleccione... </option>";
                foreach ($rows as $aux) {
                    echo "<option value='".$aux['id_modelo']."'>" . $aux['nombre'] ."</option>";
                }
            } else {
                echo "<option> -- </option>";
            }
        }
        \Yii::$app->end();
    }
    /*public function actionListarestaciones($id)
    {
        if (\Yii::$app->request->isAjax ) {
            $empresa = Empresa::findOne($id);
            $query = new Query();
            $query  ->select([  'estacion.id_estacion as id_est',
                                'empresa.nombre as emp',
                                'estacion.nombre as est'])
                    ->from('empresa')
                    ->join('INNER JOIN','estacion','empresa.id_empresa = estacion.id_empresa')
                    ->where('empresa.nombre_comercial=:nom and estacion.coordinador is null',[':nom' => $empresa->nombre_comercial])
                    ->orderBy(['empresa.nombre'=> SORT_ASC,'estacion.nombre'=> SORT_ASC]);
            $rows = $query->all();
            if (count($rows) > 0) {
                echo "<option value=''> Seleccione... </option>";
                foreach ($rows as $aux) {
                    echo "<option value='".$aux['id_est']."'>".$aux['emp']." - ".$aux['est']."</option>";
                }
            } else {
                echo "<option> No hay estaciones disponibles por asignar... </option>";
            }
        }
        \Yii::$app->end();
    }*/

        /**
     * Obtener las empresas dependientes de un cliente
     * @param integer $id
     * @return mixed
     * @author Zulma Hernández
     */
    public function actionGetempresas()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            $id_cliente = end($parents);
            $list = Empresa::find()->where(['cliente_id' => $id_cliente])->all();
            $selected = null;
            if ( $parents != null ) {
                $id_cliente = $parents[0];
                $out = Empresa::find()->select(['id' => 'id_empresa','name' =>'nombre' ])->where(['cliente_id' => $id_cliente])->orderBy('nombre')->asArray()->all();  
                
                
                echo Json::encode(['output'=>$out, 'selected'=>$selected]);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }

    /**
     * Obtener las estaciones de una agencia
     * @param integer $id
     * @return mixed
     */
    public function actionGetestaciones()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $id_empresa = $parents[0];
                $out = Estacion::find()->select(['id' => 'id_estacion','name' =>'nombre' ])->where(['id_empresa' => $id_empresa])->orderBy('nombre')->asArray()->all();
                echo Json::encode(['output'=>$out, 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }

    public function notificacion($model)
    {
        $envio_s=$envio_g=1;

        /*Mail*/
        $tipo_caso = [0 => 'Accidente / Incidente', 3 => 'Aire acondicionado', 2 => 'Falla mecánica'];
        if ($model->req_cotizacion == '0') {
            $cotiza = 'No';
        }else if ($model->req_cotizacion == '1') {
            $cotiza = 'Si';
        }else{
            $cotiza = '<span style="color:#999;">No aplica<span>';
        }
        $mensaje_sup ='  <p>'.Yii::$app->user->identity->username.',<br>
                        Km100 le informa que usted ha realizado exitosamente el Registro de un Caso.
                    </p>
                    <p>
                        <strong>Caso: </strong>Nº'.$model->id_caso.'<br>
                        <strong>MVA Ficha: </strong>'.$model->ficha.'<br>
                        <strong>Nº Placa: </strong>'.$model->placa.'<br>
                        <strong>Tipo de Caso: </strong>'.$tipo_caso[$model->tipo_caso].'<br>
                        <strong>Solitud de cotización: </strong>'.$cotiza.'
                    </p>
        ';
        if (!Yii::$app->mailer->compose(['html'=>'layout'], ['content'=> $mensaje_sup])
                        ->setFrom([Yii::$app->params['adminEmail'] => 'KM100'])
                        ->setTo(Yii::$app->user->identity->email)
                        ->setSubject('Notificación Registro de Caso')
                        //->setHtmlBody($mensaje_sup)
                        ->send())
        {
            $envio_s=0;
        }
        /*Fin Mail*/

       $hay_coordinador=0;

        $usuarios = EstacionUsuario::find()->joinWith('user')->joinWith('user.authAssignments')->where([ 'estacion_id' => $model->id_estacion ])->andWhere('auth_assignment.item_name = "Nivel 2"')->all();

        if($usuarios){
            $hay_coordinador=1;
            foreach ($usuarios as $usuario) {
            
                //Correo COORDINADOR
                if($model->descripcion){ $desc = '<br><strong>Descripción del accidente</strong>: '.$model->descripcion; }
                else{ $desc = ''; }
                $Link = Yii::$app->urlManager->createAbsoluteUrl(['/caso/view', 'id' => $model->id_caso ]);
                $mensaje_coordinador ='  <p>'.$usuario->user->username.'<br>
                                Km100 le informa que se ha registrado un nuevo Caso.
                            </p>
                            <p>
                                <strong>Estación: </strong>'.$model->idEstacion->nombre.'<br>
                                <strong>Caso</strong>: Nº'.$model->id_caso.'<br>
                                <strong>MVA Ficha: </strong>'.$model->ficha.'<br>
                                <strong>Nº Placa: </strong>'.$model->placa.'<br>
                                <strong>Tipo de Caso: </strong>'.$tipo_caso[$model->tipo_caso].$desc.'
                            </p>
                            <p>Para visualizar los detalles del Caso puede
                                <a href="158.69.32.216/km100/web/index.php?r=caso%2Fview&id='.$model->id_caso.'">
                                    consultar aquí.
                                </a>
                            </p>
                ';
                if (!Yii::$app->mailer->compose(['html'=>'layout'], ['content'=> $mensaje_coordinador])
                                ->setFrom([Yii::$app->params['adminEmail'] => 'KM100'])
                                ->setTo($usuario->user->email)
                                ->setSubject('Notificación Registro de Caso')
                                ->send())
                {
                    $envio_g=0;
                }

            }
            
        }

        if($hay_coordinador==0) {
            if ($envio_s==1) {
                \Yii::$app->getSession()->setFlash('error', '!Atención! No se encontró Coordinador asociado con permiso para ser notificado...');
                return true;
            }else{
                return false;
            }
        }else if ($envio_s==0 || $envio_g==0){
            return false;
        }else{
            return true;
        }
    }
    /**
     * Obtener las placas de una ficha
     * @param integer $id
     * @return mixed
     */
    public function actionGetplacas()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {

            $id = end($_POST['depdrop_parents']);
            $list = Vehiculo::find()->andWhere(['mva'=>$id])->asArray()->all();
            $selected  = null;
            if ($id != null && count($list) > 0) {
                $selected = '';
                foreach ($list as $i => $vehiculo) {
                    $out[] = ['id' => $vehiculo['id'], 'name' => $vehiculo['placa']];
                    if ($i == 0) {
                        $selected = $vehiculo['id'];
                    }
                }
                // Shows how you can preselect a value
                echo Json::encode(['output' => $out, 'selected'=>$selected]);
                //echo Json::encode(['output' => $out, 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected'=>'']);
    }
    /**
     * Obtener vehiculo
     * @param integer $id
     * @return mixed
     */
    public function actionGetvehiculo()
    {
        if (Yii::$app->request->post()) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $response = [];

            $id_vehiculo = Yii::$app->request->post('id_vehiculo');

            $model_vehiculo = Vehiculo::findOne($id_vehiculo);

            $response['modelo'] = $model_vehiculo->idModelo->nombre;
            $response['id_modelo'] = $model_vehiculo->idModelo->id_modelo;
            $response['marca'] = Marca::findOne($model_vehiculo->idModelo->id_marca)->nombre;

            return $response;
        }
    }

    /**
     * Updates an existing Caso model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        $model = Caso::findOne($id);
        $cobro = CobroCaso::find()->andWhere(['id_caso'=>$id])->one();
        if ($cobro == NULL) {
            $cobro = new CobroCaso();
        }

        $frontal = new UploadFrontal(['scenario' => UploadFrontal::SCENARIO_MODIF]);
        $trasero = new UploadTrasero(['scenario' => UploadTrasero::SCENARIO_MODIF]);
        $izquierdo = new UploadIzquierdo(['scenario' => UploadIzquierdo::SCENARIO_MODIF]);
        $derecho = new UploadDerecho(['scenario' => UploadDerecho::SCENARIO_MODIF]);
        $danio = new UploadDanio(['scenario' => UploadDanio::SCENARIO_MODIF]);
        $placa = new UploadPlaca(['scenario' => UploadPlaca::SCENARIO_MODIF]);
        $soportes = new UploadForm();

        $cliente = (\Yii::$app->user->can('registro casos todos') || \Yii::$app->user->can('Modificar Caso') )?'':EstacionUsuario::find()->where(['user_id'=>Yii::$app->user->identity->ID ])->one()->estacion->idEmpresa->cliente;
        $empresa = (\Yii::$app->user->can('registro casos todos') || \Yii::$app->user->can('Modificar Caso') )?'':EstacionUsuario::find()->where(['user_id'=>Yii::$app->user->identity->ID ])->one()->estacion->idEmpresa;
        $estaciones = (\Yii::$app->user->can('registro casos estaciones asociadas'))?ArrayHelper::getColumn(EstacionUsuario::find()->select('estacion_id')->where(['user_id'=>Yii::$app->user->identity->ID ])->asArray()->all(), 'estacion_id'):'';

        /*$empresa = Empresa::find()->where([ 'id_empresa' => Yii::$app->user->identity->id_empresa])->One();
        if(!$empresa){
            \Yii::$app->getSession()->setFlash('error', '¡Error! Usted no tiene Empresa asociada...');
            return $this->render('/site/index');
        }

        $estaciones = Estacion::find()->where([ 'id_empresa' => $empresa->id_empresa])->orderBy('nombre')->All();*/

        if ($model->load(Yii::$app->request->post()))
        {
            $valido=1;
            if($model->id_estacion && $model->id_modelo && $model->ficha && $model->placa && isset($model->tipo_caso)){
                $model->id_estacion = $model->id_estacion;
                $model->id_modelo = $model->id_modelo;
                $model->ficha = $model->ficha;
                $model->placa = $model->placa;
                $model->tipo_caso = $model->tipo_caso;
                $model->descripcion = $model->descripcion;

                if($model->tipo_caso == '0' || $model->tipo_caso == '1') // Si es ACCIDENTE o INCIDENTE
                {
                    if($model->fecha_acc_inc && $model->id_tipo_accidente/* && $model->id_ubicacion*/){
                        $model->amet = $model->amet;
                        $model->fecha_acc_inc = $model->fecha_acc_inc;
                        $model->id_tipo_accidente = $model->id_tipo_accidente;
                        if ($model->id_ubicacion) {
                            $model->id_ubicacion = $model->id_ubicacion;
                        }else{
                            $model->id_ubicacion = 6; //Caso de "No aplica"
                        }

                        if($model->tipo_caso == '0') // Si es ACCIDENTE
                        {
                            if(isset($model->fuente_accidente)){
                                $model->fuente_accidente = $model->fuente_accidente;
                                if($model->fuente_accidente == '0')  // Si es CLIENTE FINAL
                                {
                                    if($model->num_contrato && isset($model->tipo_cliente) && isset($model->cobertura_cdw) && isset($model->deducible) && isset($model->req_cotizacion)){
                                        $model->num_contrato = $model->num_contrato;
                                        $model->tipo_cliente = $model->tipo_cliente;
                                        $model->cobertura_cdw = $model->cobertura_cdw;
                                        $model->claim = $model->claim;
                                        $model->deducible = $model->deducible;
                                        $model->monto_deducible = $model->monto_deducible;
                                        $model->req_cotizacion = $model->req_cotizacion;
                                        if($model->req_cotizacion == '1')
                                            $model->tarifa_base = $model->tarifa_base;
                                            $model->piezas = $model->piezas;
                                    }else{ $valido=5; }
                                }else  // Si es Caso interno o empleado
                                {
                                    if($model->nombre_empleado){
                                        $model->nombre_empleado = $model->nombre_empleado;
                                    }else{ $valido=4; }
                                }
                            }else{ $valido=3; }
                        }
                    }else{ $valido=2; }
                }
                //$model->registrado_por = Yii::$app->user->identity->id;
                //$model->fecha_registro = date('Y-m-d');
            }else{ $valido=0; }

            if($valido==1){
                if($model->save()){

                    if (isset($_POST['CobroCaso']['monto'])) {
                        $cobro->id_caso = $model->id_caso;
                        $cobro->monto = $_POST['CobroCaso']['monto'];
                        $cobro->forma_pago = $_POST['CobroCaso']['forma_pago'];
                        $cobro->motivo_cobro = $_POST['CobroCaso']['motivo_cobro'];
                        $cobro->fecha = $_POST['CobroCaso']['fecha'];
                        $cobro->numero_factura = $_POST['CobroCaso']['numero_factura'];

                        $cobro->save();
                    }

                    $frontal->imageFiles = UploadedFile::getInstances($frontal, 'imageFiles');      $frontal->upload($model->id_caso);
                    $izquierdo->imageFiles = UploadedFile::getInstances($izquierdo, 'imageFiles');  $izquierdo->upload($model->id_caso);
                    $derecho->imageFiles = UploadedFile::getInstances($derecho, 'imageFiles');      $derecho->upload($model->id_caso);
                    $trasero->imageFiles = UploadedFile::getInstances($trasero, 'imageFiles');      $trasero->upload($model->id_caso);
                    $danio->imageFiles = UploadedFile::getInstances($danio, 'imageFiles');          $danio->upload($model->id_caso);
                    $placa->imageFiles = UploadedFile::getInstances($placa, 'imageFiles');          $placa->upload($model->id_caso);
                    $soportes->imageFiles = UploadedFile::getInstances($soportes, 'imageFiles');    $soportes->upload($model->id_caso);

                    \Yii::$app->getSession()->setFlash('success', '¡El Caso ha sido modificado con éxito!');
                    return $this->redirect(['view', 'id' => $model->id_caso]);
                }else{
                    \Yii::$app->getSession()->setFlash('error', 'Error al guardar cambios... '/*.$valido*/);
                }
            }else{
                \Yii::$app->getSession()->setFlash('error', 'Faltan campos por llenar... ');
            }
        }
        return $this->render('update', [
                'model' => $model,
                'cliente' => $cliente,
                'empresa' => $empresa,
                'estaciones' => $estaciones,
                'soportes' => $soportes,
                'frontal' => $frontal,
                'trasero' => $trasero,
                'izquierdo' => $izquierdo,
                'derecho' => $derecho,
                'danio' => $danio,
                'placa' => $placa,
                'cobro' => $cobro,
            ]);
    }

    /**
     * Deletes an existing Caso model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
      $sop_ante = Soporte::find()->where(['id_caso' => $id])->All();
      foreach ($sop_ante as $key => $value) {
        if ($value) {
            unlink('.'.$value->ubicacion);
            $value->delete();
        }
      }

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionCobrosPendientes()
    {
        $searchModel = new CasoSearch();
        $dataProvider = $searchModel->searchCasosCobrosPendientes(Yii::$app->request->queryParams);

        return $this->render('cobros-pendientes', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    /**
     * Genera un documento Excel con los Casos de Tipo Accidnete / Incidente
     * @return Archivo Excel con los Casos de Tipo Accidente / Incidente
     * @author Zulma Hernández
     */
    public function actionExcelAccidentes()
    {
        ini_set('memory_limit', '1024M');
        set_time_limit(300);
        $searchModel = new CasoSearch([ 'tipo_caso' => 0 ]); // Tipo de Caso Accidente/Incidnete
        $dataProvider = $searchModel->searchCasos(Yii::$app->request->queryParams);
        $dataProvider->pagination = false;

        $model = $dataProvider->getModels(); //Obtiene el modelo de la busqueda

        $tipo_caso = [0 => 'Accidente', 1 => 'Incidente', 2 => 'Falla mecánica', 3 => 'Aire acondicionado'];
        $fuentes_acc = [1 => 'Caso interno o empleado', 0 => 'Cliente final'];
        $tip_clientes = [2=>'Asegurado', 1=>'Corporativo', 3=>'Leasing', 0=>'Retail'];

       
        try{

            // Create new PHPExcel object
            $objPHPExcel = new \PHPExcel();
            
            // Fill worksheet from values in array
            $objWorkSheet = $objPHPExcel->createSheet(0);
            $objWorkSheet->setCellValue('A1', 'Casos de Tipo Accidente/Incidente');
            $objWorkSheet->setCellValue('A2', '#');
            $objWorkSheet->setCellValue('B2', 'Nº de Caso');
            $objWorkSheet->setCellValue('C2', 'Fecha');
            $objWorkSheet->setCellValue('D2', 'Cliente');
            $objWorkSheet->setCellValue('E2', 'Empresa');
            $objWorkSheet->setCellValue('F2', 'Estación');
            $objWorkSheet->setCellValue('G2', 'Marca');
            $objWorkSheet->setCellValue('H2', 'Modelo');
            $objWorkSheet->setCellValue('I2', 'Ficha o MVA');
            $objWorkSheet->setCellValue('J2', 'Placa');
            $objWorkSheet->setCellValue('K2', 'Tipo de Caso');
            $objWorkSheet->setCellValue('L2', 'Descripción');
            $objWorkSheet->setCellValue('M2', 'AMET');
            $objWorkSheet->setCellValue('N2', 'Fecha Accidente/Incidente');
            $objWorkSheet->setCellValue('O2', 'Tipo de Accidente');
            $objWorkSheet->setCellValue('P2', 'Ubicación Unidad');
            $objWorkSheet->setCellValue('Q2', 'Fuente del Accidente');
            $objWorkSheet->setCellValue('R2', 'Contrato');
            $objWorkSheet->setCellValue('S2', 'Tipo Cliente');
            $objWorkSheet->setCellValue('T2', 'CDW');
            $objWorkSheet->setCellValue('U2', 'CLAIM');
            $objWorkSheet->setCellValue('V2', 'Deducible');
            $objWorkSheet->setCellValue('W2', 'Monto Deducible');
            $objWorkSheet->setCellValue('X2', 'Nombre Empleado');
            
           
           

            $count = count($model);

            for($j = 3; $j <= $count+2;$j++){

                $objWorkSheet->setCellValue('A'.$j, $j-2);
                $objWorkSheet->setCellValue('B'.$j, $model[$j-3]->id_caso);
                $objWorkSheet->setCellValue('C'.$j, $model[$j-3]->fecha_registro);
                $objWorkSheet->setCellValue('D'.$j, $model[$j-3]->idEstacion->idEmpresa->cliente->nombre_comercial);
                $objWorkSheet->setCellValue('E'.$j, $model[$j-3]->idEstacion->idEmpresa->nombre);
                $objWorkSheet->setCellValue('F'.$j, $model[$j-3]->idEstacion->nombre);
                $objWorkSheet->setCellValue('G'.$j, $model[$j-3]->idModelo->idMarca->nombre);
                $objWorkSheet->setCellValue('H'.$j, $model[$j-3]->idModelo->nombre);
                $objWorkSheet->setCellValue('I'.$j, $model[$j-3]->ficha);
                $objWorkSheet->setCellValue('J'.$j, $model[$j-3]->placa);
                $objWorkSheet->setCellValue('K'.$j, $tipo_caso[$model[$j-3]->tipo_caso]);
                $objWorkSheet->setCellValue('L'.$j, $model[$j-3]->descripcion);
                $objWorkSheet->setCellValue('M'.$j, $model[$j-3]->amet);
                $objWorkSheet->setCellValue('N'.$j, $model[$j-3]->fecha_acc_inc);
                $objWorkSheet->setCellValue('O'.$j, $model[$j-3]->idTipoAccidente->nombre);
                $objWorkSheet->setCellValue('P'.$j, $model[$j-3]->idUbicacion->nombre);
                $objWorkSheet->setCellValue('Q'.$j, $fuentes_acc[$model[$j-3]->fuente_accidente]);

                if ( $model[$j-3]->fuente_accidente == 0 ) {
                    $objWorkSheet->setCellValue('R'.$j, $model[$j-3]->num_contrato);
                    $objWorkSheet->setCellValue('S'.$j, $tip_clientes[$model[$j-3]->tipo_cliente]);
                    $objWorkSheet->setCellValue('T'.$j, ($model[$j-3]->cobertura_cdw == 1)?'SI':'NO');  
                    $objWorkSheet->setCellValue('U'.$j, $model[$j-3]->claim);
                    $objWorkSheet->setCellValue('V'.$j, ($model[$j-3]->deducible == 1)?'SI':'NO');
                    $objWorkSheet->setCellValue('W'.$j, $model[$j-3]->monto_deducible); 
                    $objWorkSheet->setCellValue('X'.$j, '-'); 
                } else{
                    $objWorkSheet->setCellValue('R'.$j, '-');
                    $objWorkSheet->setCellValue('S'.$j, '-');
                    $objWorkSheet->setCellValue('T'.$j, '-');  
                    $objWorkSheet->setCellValue('U'.$j, '-');
                    $objWorkSheet->setCellValue('V'.$j, '-');
                    $objWorkSheet->setCellValue('W'.$j, '-'); 
                    $objWorkSheet->setCellValue('X'.$j, $model[$j-3]->nombre_empleado); 
                }

            }

            $objWorkSheet->setTitle("Casos Accidentes Incidentes");

            // Save Excel 2007 file
            $file = 'uploads/CasosAccidentesIncidentes.xlsx';
            
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save($file);

            header('Content-Description: File Transfer');
            header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            header("Content-Disposition: attachment; filename=\"".basename($file)."\"");
            header("Content-Transfer-Encoding: binary");
            header("Expires: 0");
            header("Pragma: public");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header('Content-Length: ' . filesize($file)); //Remove

            ob_clean();
            flush();

            readfile($file);
            exit();
        }
        catch(Exception $e){
            die('Error');
        } 
    }

    /**
     * Genera un documento Excel con los Casos asociados al nivel del usuario
     * @return Archivo Excel con los Casos asociados al nivel del usuario
     * @author Zulma Hernández
     */
    public function actionExcelCasos()
    {
        ini_set('memory_limit', '1024M');
        set_time_limit(300);
        $searchModel = new CasoSearch();
        $dataProvider = $searchModel->searchCasos(Yii::$app->request->queryParams);
        $dataProvider->pagination = false;
        $fecha = date('Y-m-d');

        $model = $dataProvider->getModels(); //Obtiene el modelo de la busqueda

        $tipo_caso = [0 => 'Accidente / Incidente', 1 => 'Accidente / Incidente', 3 => 'Aire acondicionado', 2 => 'Falla mecánica'];
        $fuentes_acc = [1 => 'Caso interno o empleado', 0 => 'Cliente final'];
        $tip_clientes = [2=>'Asegurado', 1=>'Corporativo', 3=>'Leasing', 0=>'Retail'];

       
        try{

            // Create new PHPExcel object
            $objPHPExcel = new \PHPExcel();
            
            // Fill worksheet from values in array
            $objWorkSheet = $objPHPExcel->createSheet(0);
            $objWorkSheet->setCellValue('A1', 'Listado de Casos');
            $objWorkSheet->setCellValue('A2', '#');
            $objWorkSheet->setCellValue('B2', 'Nº de Caso');
            $objWorkSheet->setCellValue('C2', 'Fecha');
            $objWorkSheet->setCellValue('D2', 'Cliente');
            $objWorkSheet->setCellValue('E2', 'Empresa');
            $objWorkSheet->setCellValue('F2', 'Estación');
            $objWorkSheet->setCellValue('G2', 'Marca');
            $objWorkSheet->setCellValue('H2', 'Modelo');
            $objWorkSheet->setCellValue('I2', 'Ficha o MVA');
            $objWorkSheet->setCellValue('J2', 'Placa');
            $objWorkSheet->setCellValue('K2', 'Tipo de Caso');
            $objWorkSheet->setCellValue('L2', 'Descripción');
            $objWorkSheet->setCellValue('M2', 'AMET');
            $objWorkSheet->setCellValue('N2', 'Fecha Accidente/Incidente');
            $objWorkSheet->setCellValue('O2', 'Tipo de Accidente');
            $objWorkSheet->setCellValue('P2', 'Ubicación Unidad');
            $objWorkSheet->setCellValue('Q2', 'Fuente del Accidente');
            $objWorkSheet->setCellValue('R2', 'Contrato');
            $objWorkSheet->setCellValue('S2', 'Tipo Cliente');
            $objWorkSheet->setCellValue('T2', 'CDW');
            $objWorkSheet->setCellValue('U2', 'CLAIM');
            $objWorkSheet->setCellValue('V2', 'Deducible');
            $objWorkSheet->setCellValue('W2', 'Monto Deducible');
            $objWorkSheet->setCellValue('X2', 'Nombre Empleado');
            $objWorkSheet->setCellValue('Y2', 'Registrado por');
            
           
           

            $count = count($model);

            for($j = 3; $j <= $count+2;$j++){

                $objWorkSheet->setCellValue('A'.$j, $j-2);
                $objWorkSheet->setCellValue('B'.$j, $model[$j-3]->id_caso);
                $objWorkSheet->setCellValue('C'.$j, $model[$j-3]->fecha_registro);
                $objWorkSheet->setCellValue('D'.$j, $model[$j-3]->idEstacion->idEmpresa->cliente->nombre_comercial);
                $objWorkSheet->setCellValue('E'.$j, $model[$j-3]->idEstacion->idEmpresa->nombre);
                $objWorkSheet->setCellValue('F'.$j, $model[$j-3]->idEstacion->nombre);
                $objWorkSheet->setCellValue('G'.$j, $model[$j-3]->idModelo->idMarca->nombre);
                $objWorkSheet->setCellValue('H'.$j, $model[$j-3]->idModelo->nombre);
                $objWorkSheet->setCellValue('I'.$j, $model[$j-3]->ficha);
                $objWorkSheet->setCellValue('J'.$j, $model[$j-3]->placa);
                $objWorkSheet->setCellValue('K'.$j, $tipo_caso[$model[$j-3]->tipo_caso]);
                $objWorkSheet->setCellValue('L'.$j, $model[$j-3]->descripcion);

                if ( $model[$j-3]->tipo_caso == 0 || $model[$j-3]->tipo_caso == 1 ) {
                    $objWorkSheet->setCellValue('M'.$j, $model[$j-3]->amet);
                    $objWorkSheet->setCellValue('N'.$j, $model[$j-3]->fecha_acc_inc);
                    $objWorkSheet->setCellValue('O'.$j, $model[$j-3]->idTipoAccidente->nombre);
                    $objWorkSheet->setCellValue('P'.$j, $model[$j-3]->idUbicacion->nombre);
                    $objWorkSheet->setCellValue('Q'.$j, $fuentes_acc[$model[$j-3]->fuente_accidente]);

                    if ( $model[$j-3]->fuente_accidente == 0 ) {
                        $objWorkSheet->setCellValue('R'.$j, $model[$j-3]->num_contrato);
                        $objWorkSheet->setCellValue('S'.$j, $tip_clientes[$model[$j-3]->tipo_cliente]);
                        $objWorkSheet->setCellValue('T'.$j, ($model[$j-3]->cobertura_cdw == 1)?'SI':'NO');  
                        $objWorkSheet->setCellValue('U'.$j, $model[$j-3]->claim);
                        $objWorkSheet->setCellValue('V'.$j, ($model[$j-3]->deducible == 1)?'SI':'NO');
                        $objWorkSheet->setCellValue('W'.$j, $model[$j-3]->monto_deducible); 
                        $objWorkSheet->setCellValue('X'.$j, '-'); 
                    } else{
                        $objWorkSheet->setCellValue('R'.$j, '-');
                        $objWorkSheet->setCellValue('S'.$j, '-');
                        $objWorkSheet->setCellValue('T'.$j, '-');  
                        $objWorkSheet->setCellValue('U'.$j, '-');
                        $objWorkSheet->setCellValue('V'.$j, '-');
                        $objWorkSheet->setCellValue('W'.$j, '-'); 
                        $objWorkSheet->setCellValue('X'.$j, $model[$j-3]->nombre_empleado); 
                    }
                } else{
                    $objWorkSheet->setCellValue('M'.$j, '-');
                    $objWorkSheet->setCellValue('N'.$j, '-');
                    $objWorkSheet->setCellValue('O'.$j, '-');
                    $objWorkSheet->setCellValue('P'.$j, '-');
                    $objWorkSheet->setCellValue('Q'.$j, '-');
                    $objWorkSheet->setCellValue('R'.$j, '-');
                    $objWorkSheet->setCellValue('S'.$j, '-');
                    $objWorkSheet->setCellValue('T'.$j, '-');  
                    $objWorkSheet->setCellValue('U'.$j, '-');
                    $objWorkSheet->setCellValue('V'.$j, '-');
                    $objWorkSheet->setCellValue('W'.$j, '-'); 
                    $objWorkSheet->setCellValue('X'.$j, '-'); 
                }
                //$reg_por = User::findOne($model[$j-3]->registrado_por);
                $objWorkSheet->setCellValue('Y'.$j, $model[$j-3]->registradoPor->username.' ('.$model[$j-3]->registradoPor->email.')');
            }

            $objWorkSheet->setTitle("Casos");

            // Save Excel 2007 file
            $file = 'uploads/Casos_'.$fecha.'.xlsx';
            
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save($file);

            header('Content-Description: File Transfer');
            header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            header("Content-Disposition: attachment; filename=\"".basename($file)."\"");
            header("Content-Transfer-Encoding: binary");
            header("Expires: 0");
            header("Pragma: public");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header('Content-Length: ' . filesize($file)); //Remove

            ob_clean();
            flush();

            readfile($file);
            exit();
        }
        catch(Exception $e){
            die('Error');
        } 
    }

    /**
     * Finds the Caso model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Caso the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Caso::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
