<?php

namespace app\controllers;

use Yii;
use app\models\AuditoriaEjecMant;
use app\models\AuditoriaejecmantSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Auditoriafs;
use app\models\Estacion;
use app\models\SolAudFs;
use app\models\Servicio;
use app\models\Aspectos;
use app\models\AuditoriaGeneral;
use app\models\AspectosSearch;
use app\models\AddFs;
use app\models\AspectosEvaluar;
use app\models\PipelineServicio;
use app\models\PipelineServicioSearch;
use app\models\OrdenCompraPiezaSearch;
use app\models\AuditoriafsSearch;
use yii\web\Controller;
use app\models\EstacionesSearch;
/**
 * AuditoriaejecmantController implements the CRUD actions for Auditoriaejecmant model.
 */
class AuditoriaejecmantController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Auditoriaejecmant models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AuditoriaejecmantSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

     /**
     * Lists all Auditoriamnto models.
     * @return mixed
     */
    public function actionIndexeject()
    {

        $searchModel = new estacionesSearch();
        $dataProvider = $searchModel->searchmensualejec(Yii::$app->request->queryParams);

        return $this->render('indexeject', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAuditoriaejec($id=null,$mensual)
    {
        $model = new AuditoriaEjecMant();
        $model_est = Estacion::findOne($id);
        $searchModel = new AspectosSearch();
        $aspecto = new Aspectos();
        $modelAud = new AuditoriaGeneral();
        $var1 = 0;
        $var2 = 0;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

         if ($model->load(Yii::$app->request->post())) {
        
              $model->tecnico = $_POST['AuditoriaEjecMant']['tecnico'];
            $model->mva_vehiculo = $_POST['AuditoriaEjecMant']['mva_vehiculo'];
            $model->id_tipo_mnto = $_POST['AuditoriaEjecMant']['id_tipo_mnto'];
            $model->acuerdo = $_POST['AuditoriaEjecMant']['acuerdo'];
            $model->compromiso = $_POST['AuditoriaEjecMant']['compromiso'];
            $model->fecha = date('Y-m-d');
            $model->estado = 1;
            $model->id_estacion = $id;
            $model->mensual = $mensual;

            if ($model->save()) {

                $modelAud->fecha = date('Y-m-d');
                $modelAud->estado = 3;
                $modelAud->id_estacion = $id;
                $modelAud->id_aud_ejec = $model->id;
                $modelAud->save(false);

                 $sis = Yii::$app->request->post('si');
                 $nos = Yii::$app->request->post('no');
 
                if ($sis) {
                 foreach ($sis as $key => $value) { 
                    $evaluacionSi = new AspectosEvaluar();
                    $evaluacionSi->id_auditoria_ejec_mant = $model->id;
                    $evaluacionSi->id_aspecto = $value;
                    $evaluacionSi->respuesta = 1;
                    $var1 = 1;
                    $evaluacionSi->save();
                }
                 }
                 if ($nos) {
                    
                 foreach ($nos as $key => $value) {
                    $evaluacionNo = new AspectosEvaluar();
                    $evaluacionNo->id_auditoria_ejec_mant = $model->id;
                    $evaluacionNo->id_aspecto = $value;
                    $evaluacionNo->respuesta = 0;
                    $var2 = 1;
                    $evaluacionNo->save();
                 }
                 }

                 if ($var1==1 || $var2==1) {
                     \Yii::$app->getSession()->setFlash('success', '¡La Auditoria ha sido cargada con éxito!');
                    return $this->redirect('/km100/auditoriageneral/index'); 
                 }
            }
            
 
                
         }
      return $this->render('auditoriaejec', [
            'model' => $model,
            'model_est' => $model_est,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    /**
     * Displays a single Auditoriaejecmant model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Auditoriaejecmant model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Auditoriaejecmant();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Auditoriaejecmant model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Auditoriaejecmant model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Auditoriaejecmant model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Auditoriaejecmant the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Auditoriaejecmant::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
