<?php

namespace app\controllers;

use Yii;
use app\models\Pieza;
use app\models\ModeloPieza;
use app\models\MarcaInsumo;
use app\models\TipoPieza;
use app\models\PiezasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\Model as Model;
use yii\helpers\ArrayHelper;
/**
 * PiezaController implements the CRUD actions for Pieza model.
 */
class PiezaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pieza models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PiezasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionListarpiezas($id)
    {

        if (\Yii::$app->request->isAjax ) {
            $query = new Query();
            $query->select('*')
            ->from('modelo_pieza')
            ->where(['=','pieza_id', $id])
            ->orderBy('modelo_id_modelo');
            $rows = $query->all();

            if (count($rows) > 0) {
                echo "<option value=''> Seleccione... </option>";
                foreach ($rows as $aux) {
                    echo "<option value='".$aux['modelo_id_modelo']."'>" . $aux['modelo_id_modelo'] ."</option>";
                }
            } else {
                echo "<option> -- </option>";
            }
        }
        \Yii::$app->end();
    }
    /**
     * Displays a single Pieza model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pieza model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * Creacion de una pieza
     * @return mixed
     */
    public function actionCreate()
    {
    
        $model = new Pieza();
 
        $model_tipo = new TipoPieza();

        $model_pieza = [new Pieza];

        if ($model->load(Yii::$app->request->post())) {

            $model_pieza = Model::createMultiple(Pieza::classname());
            Model::loadMultiple($model_pieza, Yii::$app->request->post());

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validateMultiple($model_pieza);
            }

            $valido = Model::validateMultiple($model_pieza);

            if ($valido) {
                foreach ($model_pieza as $key => $pieza) {
                    if ($pieza->save()) {
                        if ( $pieza->tipo_pieza_id == 1 ) {
                            $pieza->codigo = 'IN-0'.$pieza->id_pieza; 
                        } elseif( $pieza->tipo_pieza_id == 6 ) {
                            $pieza->codigo = 'MSC-0'.$pieza->id_pieza; 
                        } elseif( $pieza->tipo_pieza_id == 7 ) {
                            $pieza->codigo = 'NEU-0'.$pieza->id_pieza; 
                        } elseif( $pieza->tipo_pieza_id == 8 ) {
                            $pieza->codigo = 'BAT-0'.$pieza->id_pieza; 
                        } else{
                            $pieza->codigo = 'PZ-0'.$pieza->id_pieza; 
                        }
                        $bandera = $pieza->save();
                    }
                }
            }

            if ($bandera) {
                \Yii::$app->getSession()->setFlash('success', '¡La pieza ha sido creada con éxito!');
            } else{
                \Yii::$app->getSession()->setFlash('error', '¡Error al guardar la pieza!');   
            }

            return $this->redirect('index');
        } else {
            return $this->render('create', [
                'model' => $model,
                'model_pieza' => $model_pieza,
                'model_tipo' => $model_tipo,
 
            ]);
        }
    }

    /**
     * Updates an existing Pieza model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
      
        $model_tipo = ArrayHelper::map(TipoPieza::find()->all(), 'id', 'nombre');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
           return $this->redirect(['index',]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'model_tipo' => $model_tipo,
            ]);
        }
    }

    /**
     * Deletes an existing Pieza model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pieza model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pieza the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pieza::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
