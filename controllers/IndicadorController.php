<?php

namespace app\controllers;

use Yii;
use app\models\FueraServicio;
use app\models\FueraServiciosSearch;
use app\models\Indicador;
use app\models\Vehiculo;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;


/**
 * FueraServicioController implements the CRUD actions for FueraServicio model.
 */
class IndicadorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionFueradeservicioest()
    {
        $modelIndicador = new Indicador();
        $estaciones = [];
        $total = [];
        $estaciones3 = [];
        $total3 = [];
        $estaciones4 = [];
        $total4 = [];
        $vehiculo = [];
        $total2 = [];
        $total5 = [];
 

        if ($modelIndicador->load(Yii::$app->request->post())) {

//verifico si hay estacion seleccionada

            if ($modelIndicador->estacion) {
            // Consulto los vehiculos con orden de compra abierta en la estación seleccionada
            $query = new Query(); 

            $query  ->select(['estacion.nombre AS estaciones', 'COUNT(*) AS total'])
                ->from('vehiculo')
                ->join('JOIN','solicitud_compra_estacion','solicitud_compra_estacion.id_vehiculo = vehiculo.id')
                ->join('JOIN','solicitud_compra_pieza','solicitud_compra_pieza.id_solicitud_compra_estacion = solicitud_compra_estacion.id')
                ->join('INNER JOIN', 'estacion', 'solicitud_compra_estacion.id_estacion = estacion.id_estacion')
                ->where('solicitud_compra_pieza.codigo_estado<6')
                ->andWhere('solicitud_compra_pieza.origen_solicitud=2')
                ->andWhere('solicitud_compra_estacion.id_estacion='.$modelIndicador->estacion)
                ->groupBy('estacion.nombre');
            
            $command = $query->createCommand();

            $results = $command->queryAll();   


 
            //consulto los vehiculos en flota de esa estación
            $query2 = new Query(); 

            $query2->select(['estacion.id_estacion AS estacion', 'COUNT(*) AS total'])
                ->from('vehiculo')
                ->join('INNER JOIN', 'flota', 'flota.id_vehiculo = vehiculo.id')
                ->join('INNER JOIN', 'estacion', 'flota.id_estacion = estacion.id_estacion')
                 ->where(['estacion.id_estacion' => $modelIndicador->estacion])
                ->groupBy('estacion.id_estacion');

            $command2 = $query2->createCommand();

            $results2 = $command2->queryAll();     

         
    //Si no hay vehiculos en esa estación el total es 1 para efectos de la división
        if (!$results2) {

            $results2[0]['total'] = 1;

        }
                //divido la cantidad de vehiculos con orden de compra abierta/ total de la flota
                  foreach ($results as $result ) {

                    $resultado = $result['total']/$results2[0]['total'];

                    array_push($estaciones, $result['estaciones']);
                    
                    array_push($total, intval($resultado));
            }

            //Verifico si hay mes seleccionado 
            if ($modelIndicador->mes && !$modelIndicador->ano) {
                  $query3 = new Query(); 
             $query3  ->select(['estacion.nombre AS estaciones','vehiculo.id AS vehiculos', 'COUNT(*) AS total'])
                ->from('vehiculo')
                ->join('JOIN','solicitud_compra_estacion','solicitud_compra_estacion.id_vehiculo = vehiculo.id')
                ->join('JOIN','solicitud_compra_pieza','solicitud_compra_pieza.id_solicitud_compra_estacion = solicitud_compra_estacion.id')
                ->join('INNER JOIN', 'estacion', 'solicitud_compra_estacion.id_estacion = estacion.id_estacion')
                ->where('solicitud_compra_pieza.codigo_estado<6')
                ->andWhere('solicitud_compra_pieza.origen_solicitud=2')
                ->andWhere([
                    'MONTH(solicitud_compra_pieza.fecha)' => $modelIndicador->mes])
                ->andWhere('solicitud_compra_estacion.id_estacion='.$modelIndicador->estacion)
                ->groupBy('estacion.nombre');
            
            $command3 = $query3->createCommand();

            $results3 = $command3->queryAll(); 

            

                  foreach ($results3 as $result ) {

                        $resultado = $result['total'];

                        array_push($estaciones, $result['estaciones']);

                        array_push($total3, intval($resultado));
                    }
            }
             if ($modelIndicador->ano && !$modelIndicador->mes) {

                $query5 = new Query(); 

                $query5  ->select(['estacion.nombre AS estaciones','vehiculo.id AS vehiculo', 'COUNT(*) AS total'])
                ->from('vehiculo')
                ->join('JOIN','solicitud_compra_estacion','solicitud_compra_estacion.id_vehiculo = vehiculo.id')
                ->join('JOIN','solicitud_compra_pieza','solicitud_compra_pieza.id_solicitud_compra_estacion = solicitud_compra_estacion.id')
                ->join('INNER JOIN', 'estacion', 'solicitud_compra_estacion.id_estacion = estacion.id_estacion')
                ->where('solicitud_compra_pieza.codigo_estado<6')
                ->andWhere('solicitud_compra_pieza.origen_solicitud=2')
                ->andWhere([
                    'YEAR(solicitud_compra_pieza.fecha)' => $modelIndicador->ano])
                ->andWhere('solicitud_compra_estacion.id_estacion='.$modelIndicador->estacion)
                ->groupBy('estacion.nombre');
            
            $command5 = $query5->createCommand();

            $results5 = $command5->queryAll();   
 
        
          foreach ($results5 as $result ) {
                
                $resultado = $result['total'];
 
                array_push($estaciones, $result['estaciones']);

                array_push($total5, intval($resultado));
            }
        }
             if ($modelIndicador->ano && $modelIndicador->mes) {

                $query5 = new Query(); 

                $query5  ->select(['estacion.nombre AS estaciones','vehiculo.id AS vehiculo', 'COUNT(*) AS total'])
                ->from('vehiculo')
                ->join('JOIN','solicitud_compra_estacion','solicitud_compra_estacion.id_vehiculo = vehiculo.id')
                ->join('JOIN','solicitud_compra_pieza','solicitud_compra_pieza.id_solicitud_compra_estacion = solicitud_compra_estacion.id')
                ->join('INNER JOIN', 'estacion', 'solicitud_compra_estacion.id_estacion = estacion.id_estacion')
                ->where('solicitud_compra_pieza.codigo_estado<6')
                ->andWhere('solicitud_compra_pieza.origen_solicitud=2')
                ->andWhere([
                    'YEAR(solicitud_compra_pieza.fecha)' => $modelIndicador->ano])
                ->andWhere([
                    'MONTH(solicitud_compra_pieza.fecha)' => $modelIndicador->mes])
                ->andWhere('solicitud_compra_estacion.id_estacion='.$modelIndicador->estacion)
                ->groupBy('estacion.nombre');
            
            $command5 = $query5->createCommand();

            $results5 = $command5->queryAll();   
 
        
          foreach ($results5 as $result ) {
                
                $resultado = $result['total'];
 
                array_push($estaciones, $result['estaciones']);

                array_push($total5, intval($resultado));
            }
        }
            
              
 
        }
            if (!$modelIndicador->estacion) {

                if ($modelIndicador->mes) {

                $query4 = new Query(); 

                $query4  ->select(['estacion.nombre AS estaciones','vehiculo.id AS vehiculo', 'COUNT(*) AS total'])
                ->from('vehiculo')
                ->join('JOIN','solicitud_compra_estacion','solicitud_compra_estacion.id_vehiculo = vehiculo.id')
                ->join('JOIN','solicitud_compra_pieza','solicitud_compra_pieza.id_solicitud_compra_estacion = solicitud_compra_estacion.id')
                ->join('INNER JOIN', 'estacion', 'solicitud_compra_estacion.id_estacion = estacion.id_estacion')
                ->where('solicitud_compra_pieza.codigo_estado<6')
                ->andWhere('solicitud_compra_pieza.origen_solicitud=2')
                ->andWhere([
                    'MONTH(solicitud_compra_pieza.fecha)' => $modelIndicador->mes])
                ->groupBy('estacion.nombre');
            
            $command4 = $query4->createCommand();

            $results4 = $command4->queryAll();   
 
        
          foreach ($results4 as $result ) {
                
                $resultado = $result['total'];
 
                array_push($estaciones, $result['estaciones']);

                array_push($total4, intval($resultado));
            }
        }else{


                $query4 = new Query(); 

                $query4  ->select(['estacion.nombre AS estaciones','vehiculo.id AS vehiculo', 'COUNT(*) AS total'])
                ->from('vehiculo')
                ->join('JOIN','solicitud_compra_estacion','solicitud_compra_estacion.id_vehiculo = vehiculo.id')
                ->join('JOIN','solicitud_compra_pieza','solicitud_compra_pieza.id_solicitud_compra_estacion = solicitud_compra_estacion.id')
                ->join('INNER JOIN', 'estacion', 'solicitud_compra_estacion.id_estacion = estacion.id_estacion')
                ->where('solicitud_compra_pieza.codigo_estado<6')
                ->andWhere('solicitud_compra_pieza.origen_solicitud=2')
                ->groupBy('estacion.nombre');
            
            $command4 = $query4->createCommand();

            $results4 = $command4->queryAll();   

        
          foreach ($results4 as $result ) {
                
                $resultado = $result['total'];
 
                array_push($estaciones, $result['estaciones']);

                array_push($total4, intval($resultado));
            }

        }

            if ($modelIndicador->ano) {

                $query5 = new Query(); 

                $query5  ->select(['estacion.nombre AS estaciones','vehiculo.id AS vehiculo', 'COUNT(*) AS total'])
                ->from('vehiculo')
                ->join('JOIN','solicitud_compra_estacion','solicitud_compra_estacion.id_vehiculo = vehiculo.id')
                ->join('JOIN','solicitud_compra_pieza','solicitud_compra_pieza.id_solicitud_compra_estacion = solicitud_compra_estacion.id')
                ->join('INNER JOIN', 'estacion', 'solicitud_compra_estacion.id_estacion = estacion.id_estacion')
                ->where('solicitud_compra_pieza.codigo_estado<6')
                ->andWhere('solicitud_compra_pieza.origen_solicitud=2')
                ->andWhere([
                    'YEAR(solicitud_compra_pieza.fecha)' => $modelIndicador->ano])
                ->groupBy('estacion.nombre');
            
            $command5 = $query5->createCommand();

            $results5 = $command5->queryAll();   
 
        
          foreach ($results5 as $result ) {
                
                $resultado = $result['total'];
 
                array_push($estaciones, $result['estaciones']);

                array_push($total5, intval($resultado));
            }
        }else{


                $query5 = new Query(); 

                $query5  ->select(['estacion.nombre AS estaciones','vehiculo.id AS vehiculo', 'COUNT(*) AS total'])
                ->from('vehiculo')
                ->join('JOIN','solicitud_compra_estacion','solicitud_compra_estacion.id_vehiculo = vehiculo.id')
                ->join('JOIN','solicitud_compra_pieza','solicitud_compra_pieza.id_solicitud_compra_estacion = solicitud_compra_estacion.id')
                ->join('INNER JOIN', 'estacion', 'solicitud_compra_estacion.id_estacion = estacion.id_estacion')
                ->where('solicitud_compra_pieza.codigo_estado<6')
                ->andWhere('solicitud_compra_pieza.origen_solicitud=2')
                ->groupBy('estacion.nombre');
            
            $command5 = $query5->createCommand();

            $results5 = $command5->queryAll();   

        
          foreach ($results5 as $result ) {
                
                $resultado = $result['total'];
 
                array_push($estaciones, $result['estaciones']);

                array_push($total5, intval($resultado));
            }

        }
                 

        }


        }
   
        return $this->render('fueradeservicioest', [
            'modelIndicador' => $modelIndicador,
            'estaciones' => $estaciones,
            'total' => $total,
            'estaciones4' => $estaciones4,
            'total5' => $total5,
            'total4' => $total4,
            'estaciones3' => $estaciones,
            'total3' => $total,
            'vehiculo' => $vehiculo,
            'total2' => $total2
        ]);
    

  
    }

 
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

 
}
