<?php

namespace app\controllers;

use Yii;
use app\models\AuditoriaFs;
use app\models\OtrasFs;
use app\models\ImagenesFs;
use app\models\Estacion;
use app\models\Vehiculo;
use app\models\SolicitudCompraEstacion;
use app\models\EstadoCompraPieza;
use app\models\SolicitudCompraPieza;
use app\models\VehiculoSearch;
use app\models\SolAudFs;
use app\models\Anexos;
use yii\web\Controller;
use app\models\Model;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\modules\yii2extensions\models\CatalogOption;
use app\modules\yii2extensions\models\Image;
use app\modules\yii2extensions\models\OptionValue;
use app\modules\yii2extensions\models\query\CatalogOptionQuery;

use app\models\Servicio;
use app\models\AddFs;
use app\models\PipelineServicio;
use app\models\PipelineServicioSearch;
use app\models\OrdenCompraPiezaSearch;
use app\models\AuditoriafsSearch;
use app\models\AuditoriaGeneral;
use app\models\AuditoriageneralSearch;
use app\models\EstacionesSearch;

/**
 * AuditoriafsController implements the CRUD actions for Auditoriafs model.
 */
class AuditoriafsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
   public function actionAuditoriafs($id=null,$mensual)
    {
       $model_est = Estacion::findOne($id);

        $searchModel = new OrdenCompraPiezaSearch();
        $modelsfs = [new SolAudFs];
        $modelsotras = [new OtrasFs];
        $modelsadd = [new AddFs];
        $model = new AuditoriaFs();
        $modelAud = new AuditoriaGeneral();
        $searchModel = new VehiculoSearch();
        $models = new SolAudFs();
        $modelanexos = [new SolAudFs()];
        $modelimagenes = [new ImagenesFs()];
         $dataProvider = $searchModel->fuerasearch(Yii::$app->request->queryParams,$model_est->id_estacion);
           
        if ($model_est->id_estacion) {
      
        $model_serv = Servicio::findOne(['id_estacion_actual' => $model_est->id_estacion]);
        if ($model_serv['id']) {
            
        $model_pipe = PipelineServicio::findOne(['id_servicio' => $model_serv['id']]);
        }
        }
        $variable ='';
// ['id_servicio' => $model_serv->id]


$query = Vehiculo::find()
                ->join('JOIN','solicitud_compra_estacion','solicitud_compra_estacion.id_vehiculo = vehiculo.id')
                ->join('JOIN','solicitud_compra_pieza','solicitud_compra_pieza.id_solicitud_compra_estacion = solicitud_compra_estacion.id')
                ->where('solicitud_compra_pieza.codigo_estado<6')
                ->andWhere('solicitud_compra_pieza.origen_solicitud=2')
                ->andWhere('solicitud_compra_estacion.id_estacion='.$model_est['id_estacion'])
                ->orderBy('vehiculo.id ASC');


                    $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                    $carros = Yii::$app->db->createCommand($sql)->queryAll();
        $vector_estados=[];
        $vector_ordenes=[];
        $i=0;
        foreach ($carros as $key => $value) {
            $solicitudes = SolicitudCompraEstacion::find()->where([ 'id_vehiculo' => $value['id'] ])->all();

              
        foreach ($solicitudes as $key => $value2) {
            $estados = SolicitudCompraPieza::find()->where([ 'id_solicitud_compra_estacion' => $value2['id'] ])->orderBy('codigo_estado DESC')->one();
            $edo = $estados['codigo_estado'];
            $estado = EstadoCompraPieza::find()->where( ['id' => $edo] )->one();
            $vector_estados[$i] = $estado['id'];
            $vector_ordenes[$i] =$estados['id'];
            $i++;
        
                }
        }


         if ($model->load(Yii::$app->request->post())){
       
            $model->id_estacion = $id;
            $model->fecha = date('Y-m-d');
            $model->estado = 1;
            $model->mensual = $mensual;
         
                # code...
            $model->veh_fuera_ser_est = $_POST['AuditoriaFs']['veh_fuera_ser_est'];
              $model->plan_accion = $_POST['AuditoriaFs']['planaccion'];
              if (!$_POST['AuditoriaFs']['veh_fuera_serv_reg'] && !$_POST['AuditoriaFs']['veh_fuera_ser_est']) {
                 $model->desviacion = 0;
              }else{
              $model->desviacion = $_POST['AuditoriaFs']['veh_fuera_serv_reg']-$_POST['AuditoriaFs']['veh_fuera_ser_est'];
            }
         
           if ($model->save(false)) {
                $modelAud->fecha = date('Y-m-d');
                $modelAud->estado = 1;
                $modelAud->id_estacion = $id;
                $modelAud->id_aud_fs = $model->id;
                $modelAud->save(false);


                // imagenes

                 $modelanexos = Model::createMultiple(SolAudFs::classname());
                Model::loadMultiple($modelanexos, Yii::$app->request->post());

                foreach ($modelanexos as $index => $modelanexo) {
                $modelanexo->imagefile2 = \yii\web\UploadedFile::getInstance($modelanexo, "[{$index}]imagefile2");
                // $modelanexo->imagefile3 = \yii\web\UploadedFile::getInstance($modelanexo, "[{$index}]imagefile3");
                // $modelanexo->imagefile4 = \yii\web\UploadedFile::getInstance($modelanexo, "[{$index}]imagefile4");
                $modelanexo->imageFile = \yii\web\UploadedFile::getInstance($modelanexo, "[{$index}]imageFile");
                FileHelper::createDirectory(\Yii::$app->basePath.'/files/auditorias/'.$model->id);
                  
                if(!empty($modelanexo->imageFile->extension) || !empty($modelanexo->imageFile2->extension)){

                $modelanexo->id_aud_fs = $model->id;

                $modelanexo->id_modelo = $model->id;

               if(!empty($modelanexo->imageFile->extension)){
                $ruta  =\Yii::$app->basePath.'/files/auditorias/'.$model->id.'/'.$_FILES['SolAudFs']['name'][$index]['imageFile'];

                $modelanexo->anexo  = str_replace(' ', '', $ruta );

                $modelanexo->extension = $modelanexo->imageFile->extension;

                $modelanexo->base_url = str_replace(' ', '',$_FILES['SolAudFs']['name'][$index]['imageFile']) ;
               }
                  if(!empty($modelanexo->imagefile2->extension)){

                // FileHelper::createDirectory(\Yii::$app->basePath.'/files/auditorias/'.$model->id);
                $ruta2  =\Yii::$app->basePath.'/files/auditorias/'.$model->id.'/'.$_FILES['SolAudFs']['name'][$index]['imagefile2'];

                $modelanexo->anexo2  = str_replace(' ', '', $ruta2 );

                $modelanexo->extension2 = $modelanexo->imagefile2->extension;

                $modelanexo->base_url2 = str_replace(' ', '',$_FILES['SolAudFs']['name'][$index]['imagefile2']) ;
               }
               //    if(!empty($modelanexo->imagefile3->extension)){

               //  // FileHelper::createDirectory(\Yii::$app->basePath.'/files/auditorias/'.$model->id);
               //  $ruta3  =\Yii::$app->basePath.'/files/auditorias/'.$model->id.'/'.$_FILES['SolAudFs']['name'][$index]['imagefile3'];

               //  $modelanexo->anexo3  = str_replace(' ', '', $ruta3 );

               //  $modelanexo->extension3 = $modelanexo->imagefile3->extension;

               //  $modelanexo->base_url3 = str_replace(' ', '',$_FILES['SolAudFs']['name'][$index]['imagefile3']) ;
               // }
               //    if(!empty($modelanexo->imagefile4->extension)){
               //  // FileHelper::createDirectory(\Yii::$app->basePath.'/files/auditorias/'.$model->id);
               //  $ruta4  =\Yii::$app->basePath.'/files/auditorias/'.$model->id.'/'.$_FILES['SolAudFs']['name'][$index]['imagefile4'];

               //  $modelanexo->anexo4  = str_replace(' ', '', $ruta4 );

               //  $modelanexo->extension4 = $modelanexo->imagefile4->extension;

               //  $modelanexo->base_url4= str_replace(' ', '',$_FILES['SolAudFs']['name'][$index]['imagefile4']) ;
               // }

                

                if($modelanexo->save(false)){

                move_uploaded_file($_FILES['SolAudFs']['tmp_name'][$index]['imageFile'],str_replace(' ', '',  \Yii::$app->basePath.'/files/auditorias/'.$model->id.'/'.$_FILES['SolAudFs']['name'][$index]['imageFile']));
                move_uploaded_file($_FILES['SolAudFs']['tmp_name'][$index]['imagefile2'],str_replace(' ', '',  \Yii::$app->basePath.'/files/auditorias/'.$model->id.'/'.$_FILES['SolAudFs']['name'][$index]['imagefile2']));
                // move_uploaded_file($_FILES['SolAudFs']['tmp_name'][$index]['imagefile3'],str_replace(' ', '',  \Yii::$app->basePath.'/files/auditorias/'.$model->id.'/'.$_FILES['SolAudFs']['name'][$index]['imagefile3']));
                // move_uploaded_file($_FILES['SolAudFs']['tmp_name'][$index]['imagefile4'],str_replace(' ', '',  \Yii::$app->basePath.'/files/auditorias/'.$model->id.'/'.$_FILES['SolAudFs']['name'][$index]['imagefile4']));
                }

                }
            }

            //dinamico de imagenes

              // imagenes

            
                   $modelimagenes = Model::createMultiple(ImagenesFs::classname());
                Model::loadMultiple($modelanexos, Yii::$app->request->post());

                foreach ($modelimagenes as $index => $modelanexo) {
                $modelanexo->imageFile = \yii\web\UploadedFile::getInstance($modelanexo, "[{$index}]imageFile");
                  
                if(!empty($modelanexo->imageFile->extension)){

                $modelanexo->id_auditoriafs = $model->id;
               
                FileHelper::createDirectory(\Yii::$app->basePath.'/files/auditorias/'.$model->id);
                $ruta  =\Yii::$app->basePath.'/files/auditorias/'.$model->id.'/'.$_FILES['ImagenesFs']['name'][$index]['imageFile'];

                $modelanexo->anexo  = str_replace(' ', '', $ruta );

                $modelanexo->extension = $modelanexo->imageFile->extension;

                $modelanexo->base_url = str_replace(' ', '',$_FILES['ImagenesFs']['name'][$index]['imageFile']) ;

                if($modelanexo->save()){

                   move_uploaded_file($_FILES['ImagenesFs']['tmp_name'][$index]['imageFile'],str_replace(' ', '',  \Yii::$app->basePath.'/files/auditorias/'.$model->id.'/'.$_FILES['ImagenesFs']['name'][$index]['imageFile']));
                }

                }
            }
          


           
                if (isset($_POST['OtrasFs'][0]['causa'])) {
           
                  foreach ($_POST['OtrasFs'] as $numeros) {
                      $otras = new OtrasFs();
                      $otras->causa = $numeros['causa'];
                      $otras->observacion = $numeros['observacion'];
                      $otras->solicitud = $numeros['solicitud'];
                      $otras->responsable = $numeros['responsable'];
                      $otras->id_aud_fs = $model->id;
                       $otras->save(false);
                        unset($f_ser);
                  }
                  // \Yii::$app->getSession()->setFlash('success', '¡El Rubro ha sido cargado con éxito!');
              }

                if (isset($_POST['AddFs'][0]['causa'])) {
           
                  foreach ($_POST['AddFs'] as $numeros) {
                      $sol = new AddFs();
                      $sol->causa = $numeros['causa'];
                      $sol->observacion = $numeros['observacion'];
                      $sol->ficha = $numeros['ficha'];
                      $sol->id_modelo = $numeros['id_modelo'];
                      $sol->id_aud_fs = $model->id;
                       $sol->save(false);
                        unset($f_ser);
                  }
                  // \Yii::$app->getSession()->setFlash('success', '¡El Rubro ha sido cargado con éxito!');
              }
           \Yii::$app->getSession()->setFlash('success', '¡La Auditoria ha sido cargado con éxito, por favor continue el proceso de auditoria!');
           }

 
              return $this->redirect('/km100/auditoriageneral/index'); 
           
            }
        else{

      
                 return $this->render('auditoriafs',[
                 'dataProvider' => $dataProvider,
                 'variable' => $variable,
                 'model_est' => $model_est,
                 'model' => $model,
                 'modelsfs' => (empty($modelsfs)) ? [new SolAudFs] : $modelsfs,
                 'modelsadd' => (empty($modelsadd)) ? [new AddFs] : $modelsadd,
                 'modelanexos' => (empty($modelanexos)) ? [new SolAudFs] : $modelanexos,
                 'models' => $models,
                 'modelsotras' => $modelsotras,
                 'modelimagenes' => $modelimagenes,

            ]);
       

        }
 
        
     

    }
 
    /**
     * Lists all Auditoriafs models.
     * @return mixed
     */
    public function actionIndexpendientes()
    {

        $searchModel = new estacionesSearch();
        $dataProvider = $searchModel->searchmensual(Yii::$app->request->queryParams);

        return $this->render('indexpendientes', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Auditoriafs model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Auditoriafs model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Auditoriafs();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Auditoriafs model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Auditoriafs model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Auditoriafs model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Auditoriafs the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Auditoriafs::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
