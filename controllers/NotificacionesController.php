<?php

namespace app\controllers;

use Yii;
use app\models\notificaciones;
use app\models\NotificacionesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * NotificacionesController implements the CRUD actions for notificaciones model.
 */
class NotificacionesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all notificaciones models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NotificacionesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all notificaciones de placas por vencer.
     * @return mixed
     */
    public function actionIndexplacas()
    {
        $searchModel = new NotificacionesSearch();
        $dataProvider = $searchModel->searchplacas1(Yii::$app->request->queryParams);

        return $this->render('indexplacas', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
     /**
     * Lists all notificaciones de seguros por vencer.
     * @return mixed
     */
    public function actionIndexseguros()
    {
        $searchModel = new NotificacionesSearch();
        $dataProvider = $searchModel->searchseguros(Yii::$app->request->queryParams);

        return $this->render('indexseguros', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
 /**
     * Lists all notificaciones de marbetes por vencer.
     * @return mixed
     */
    public function actionIndexmarbetes()
    {
        $searchModel = new NotificacionesSearch();
        $dataProvider = $searchModel->searchmarbetes(Yii::$app->request->queryParams);

        return $this->render('indexmarbetes', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Lists all notificaciones de registro mercantil por vencer.
     * @return mixed
     */
    public function actionIndexrm()
    {
        $searchModel = new NotificacionesSearch();
        $dataProvider = $searchModel->searchrm(Yii::$app->request->queryParams);

        return $this->render('indexrm', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
        /**
     * Lists all notificaciones de Acuerdo por vencer.
     * @return mixed
     */
    public function actionIndexacuerdo()
    {
        $searchModel = new NotificacionesSearch();
        $dataProvider = $searchModel->searchacuerdo(Yii::$app->request->queryParams);

        return $this->render('indexacuerdo', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Displays a single notificaciones model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new notificaciones model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new notificaciones();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing notificaciones model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing notificaciones model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the notificaciones model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return notificaciones the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = notificaciones::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
