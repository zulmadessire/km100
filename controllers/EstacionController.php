<?php

namespace app\controllers;

use Yii;
use app\models\Estacion;
use app\models\Empresa;
use app\models\Cliente;
use yii\helpers\Json;
use app\models\EstacionesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EstacionController implements the CRUD actions for Estacion model.
 */
class EstacionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Estacion models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EstacionesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Estacion model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Estacion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Estacion();
        $modelempresa = new Empresa();
        $model->id_wizard = 0;
        
        $modelcliente = new Cliente();

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
            //Guardar codigo de estacion
            $model2 = Estacion::findOne($model->id_estacion);
            $codigo_estacion = 'EST-0' . $model->id_estacion;           
            $model2->codigo_generado = $codigo_estacion;
            $model2->update(false);

            \Yii::$app->getSession()->setFlash('success', '¡La estación ha sido creada con éxito!');
            return $this->redirect(['view', 'id' => $model->id_estacion]);

        } else {
            
            return $this->render('create', [
                'model' => $model,
                'modelempresa' => $modelempresa,
                'modelcliente' => $modelcliente,
            ]);
        }
    }
  /**
     * Obtener empresas de un cliente
     * @param integer $id
     * @return mixed
     */
    public function actionGetempresa()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $id_marca = $parents[0];

                $out = Empresa::find()->select(['id' => 'id_empresa','name' =>'nombre' ])->where(['cliente_id' => $id_marca])->orderBy('nombre')->asArray()->all();
                echo Json::encode(['output'=>$out, 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }


    /**
     * Updates an existing Estacion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);

         if (!$model->codigo_generado) {
                $codigo_estacion = 'EST0' . $id;           
                $model->codigo_generado = $codigo_estacion;
            }

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) { 

           
 
            return $this->redirect(['view', 'id' => $model->id_estacion]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Estacion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Estacion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Estacion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Estacion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
