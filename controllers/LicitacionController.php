<?php

namespace app\controllers;

use Yii;
use app\models\Licitacion;
use app\models\Solicitudes;
use app\models\Capacitaciones;
use app\models\LicitacionSolicitud;
use app\models\ProveedorPieza;
use app\models\Proveedor;
use app\models\ProveedorPiezaLic;
use app\models\PrecioProveedor;
use app\models\Precios;
use app\models\Pieza;
use app\models\User;
use app\models\LicitacionesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\controllers\DateTime;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use kartik\mpdf\Pdf;
/**
 * LicitacionController implements the CRUD actions for Licitacion model.
 */
class LicitacionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Licitacion models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LicitacionesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Licitacion model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Licitacion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Licitacion();
        $dias_semana = '';
        $model_solicitudes = new Solicitudes();
        
        $modelsCap= [new Capacitaciones];
        if ($model->load(Yii::$app->request->post())) {
            $checkboxes = Yii::$app->request->post('pie');
            $dias = Yii::$app->request->post('sem');
            if ($dias) {
              # code...
            foreach($dias as $c)
            $dias_semana = $dias_semana .'*'.$c;  
            }
            $model->dias = $dias_semana;
            $model->id_estado_lic = 1;
            $fechahoy = date('Y-m-j');
            $nuevafecha = strtotime ( '+6 month' , strtotime ( $fechahoy ) ) ;
            $nuevafecha = date ( 'Y-m-j' , $nuevafecha );
            $model->fecha_hora = date('Y-m-j H:i:s');
            $model->contador = 0;
            $model->fecha_culminacion = $nuevafecha;
            $model->save();
            if ($checkboxes ) {
               
            foreach ($checkboxes as $key => $value) {
            $model_lic_sol = new LicitacionSolicitud();
            $model_lic_sol->id_solicitud = $value;
            $model_lic_sol->id_licitacion = $model->id;
            $model_lic_sol->save(false);
            
            }
            }

             if (isset($_POST['Capacitaciones'][0]['tipo_capacitacion'])) {
                  foreach ($_POST['Capacitaciones'] as $modelcap) {
                      $model_capacitacion = new Capacitaciones();
                      $model_capacitacion->id_licitacion = $model->id;
                      $model_capacitacion->numero = $modelcap['numero'];
                      $model_capacitacion->tipo_capacitacion = $modelcap['tipo_capacitacion'];
                      $model_capacitacion->save(false);
              
                  }
    $prov='';
    $email = '';
    $nombre = '';
    $model_proveedores = ProveedorPieza::find()->where([ 'pieza_id' => $model->id_rubro ])->all();
 

    if ($model_proveedores) {
        foreach ($model_proveedores as $key => $value) {
    $model_lic = new ProveedorPiezaLic();
    $prov = Proveedor::find()->where([ 'id' => $value->proveedor_id ])->one();
    $model_lic->id_licitacion = $model->id;
    $model_lic->id_prov_pieza = $prov['id'];
    $model_lic->estado = 1;
    $model_lic->fecha = date('Y-m-d');
    $model_lic->save(false);

         $email = $prov['correo_representante'];
         $nombre = $prov['nombre_representante'];
         $this->notificacion($email, $nombre);
    }
    }
                      
                    
                  \Yii::$app->getSession()->setFlash('success', '¡La licitación ha sido cargada con éxito!');
              }
        
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'modelscap' => (empty($modelsCap)) ? [new Capacitaciones] : $modelsCap,
            ]);
        }
    }
    // Envio de email a los proveedores Rosana Sanchez
 public function notificacion($email,$nombre)
    {
        $envio = 0;
        $mensaje_sup ='  <p> Hola '.$nombre.'<br>
                        Le informamos que la empresa Km100 esta solicitando cotizacion para piezas.
                    </p>
                    <p>
                         
                        <strong>Observación: </strong>: Por favor inicie sesión y verifique las licitaciones.<br>
                         
                    </p>
                    <br>
                    <hr>
                    <p style="color:#999;">Favor <strong>no</strong> responder este correo.</p>
        ';
        if (!\Yii::$app  ->mailer->compose()
                        ->setFrom('notificaciones.km100@gmail.com')
                        ->setTo($email)
                        ->setSubject('Km100 - Notificación Licitación de Piezas')
                        ->setHtmlBody($mensaje_sup)
                        ->send())
        {
            $envio=0;
        }
     

        if ($envio==0){
            return true;
        }else{
            return false;
        }
    }

      /**
     * pantalla del proveedor
     */
    public function actionIndexlicproveedor()
    {

          $id_proveedor = User::findOne(Yii::$app->user->id);

        $searchModel = new LicitacionesSearch();
        $dataProvider = $searchModel->searchProveedores(Yii::$app->request->queryParams, $id_proveedor['id_proveedor']);
 
        return $this->render('index_lic_proveedor', [
              'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

     /**
     * pantalla del proveedor
     */
    public function actionMejoropcion($id)
    {

          $model = $this->findModel($id);
   $piezas = ArrayHelper::map(Pieza::find()->where('tipo_pieza_id = 1')->all(), 'id_pieza', 'nombre');
   $licitaciones_recibidas = PrecioProveedor::find()->joinWith('idProvLicitacion')->where('proveedor_pieza_lic.id_licitacion ='.$model->id )->all();

   // $cotizaciones_piezas_rechazadas = CotizacionProveedorPieza::find()->joinWith('idCotizacionProveedor')->joinWith('idCotizacionProveedor.idSolicitudCompraProveedor')->where('solicitud_compra_proveedor.id_solicitud_compra_pieza ='.$model->id .' AND cotizacion_proveedor_pieza.aprobacion = 5')->all();
 
        return $this->render('mejoropcion', [
              'model' => $model,
              'piezas' => $piezas,
              'licitaciones_recibidas' => $licitaciones_recibidas,
        ]);
    }
          /**
     * pantalla del proveedor index
     */
    public function actionLicproveedor($id)
    {
          $id_proveedor = User::findOne(Yii::$app->user->id);

        $searchModel = new LicitacionesSearch();
        $dataProvider = $searchModel->searchProveedores(Yii::$app->request->queryParams, $id_proveedor['id_proveedor']);
 
        return $this->render('index_lic_proveedor', [
              'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * tarea programada del cambio de estado
     */
    public function actionCambio()
    {
            $edo_1 = Licitacion::find()->where([ 'id_estado_lic' =>1 ])->all();
          foreach ($edo_1 as $key => $value) {
        
           

            $date = date_create($value->fecha_hora);
            $hora=date_format($date, 'H:i:s');
            $dias=date_format($date, 'Y-m-d');
         
     
            if ($hora>='08:00:00' && $hora<='15:00:00') {
              if ($value->contador<=7200) {
                if ($value->contador==7200) {
                $model = $this->findModel($value->id);
                $model->contador = $value->contador+1;
                $model->id_estado_lic = 2;
                $model->update();
                }else{

                $model = $this->findModel($value->id);
                $model->contador = $value->contador+1;
                $model->update();
                }
              }
             }else{
              $horacero = date('Y-m-j 08:00:00');
              // $hora2=date_format($horacero, 'H:i:s');
              
   
$datetime2 = date_create('2009-10-13 08:00:00');
$interval = date_diff($date, $datetime2);
$h =  $interval->format("%H"); 
$m =  $interval->format("%I"); 
$s =  $interval->format("%S"); 
$h = $h*3600;
$m = $m*60;
$segundos = $h+$m+$s+7200;
 if ($value->contador<=$segundos) {
                if ($value->contador==$segundos) {
                $model = $this->findModel($value->id);
                $model->contador = $value->contador+1;
                $model->id_estado_lic = 2;
                $model->update();
                }else{

                $model = $this->findModel($value->id);
                $model->contador = $value->contador+1;
                $model->update();
                }
              }
              
             }
           }
      die();
        return $this->render('prueba', [
           
        ]);
    }
           /**
     * pantalla del proveedor Registro de licitación
     */
    public function actionRegistroproveedor($id)
    {
          $id_proveedor = User::findOne(Yii::$app->user->id);

          $model_licitacion = $this->findModel($id);

          $modelsprecios= [new Precios];

          $modelprecioprov =  new PrecioProveedor();

          $modelsolic =  new LicitacionSolicitud();
          
          $id_ppl = ProveedorPiezaLic::find()->where([ 'id_licitacion' => $model_licitacion->id ])->one();

        if ($modelprecioprov->load(Yii::$app->request->post())) {


            $dias = Yii::$app->request->post('sem');
            $dias_semana='';
            if ($dias) {
              # code...
            foreach($dias as $c)
            $dias_semana = $dias_semana .'*'.$c;  
   
            }


          $modelprecioprov->garantia = $modelprecioprov['garantia'];
          $modelprecioprov->tiempo_santo_domingo= $modelprecioprov['tiempo_santo_domingo'];
          $modelprecioprov->tiempo_interior = $modelprecioprov['tiempo_interior'];
          $modelprecioprov->dias = $dias_semana;
          $modelprecioprov->alineacion = $modelprecioprov['alineacion'];
          $modelprecioprov->obs_alineacion = $modelprecioprov['obs_alineacion'];
          $modelprecioprov->capacitacion = $modelprecioprov['capacitacion'];
          $modelprecioprov->obs_capacitacion = $modelprecioprov['obs_capacitacion'];
          $modelprecioprov->entrega_via = $modelprecioprov['entrega_via'];
          $modelprecioprov->obs_entrega_via = $modelprecioprov['obs_entrega_via']; 
          $modelprecioprov->delivery = $modelprecioprov['delivery']; 
          $modelprecioprov->obs_delivery = $modelprecioprov['obs_delivery']; 
          $modelprecioprov->id_prov_licitacion = $id_ppl['id'];
          $modelprecioprov->save();
          //guardo Dinamico
                        if (isset($_POST['Precios'][0]['marca'])) {
                  foreach ($_POST['Precios'] as $modelprec) {
                      $model_precios = new Precios();
                      $model_precios->modelo = $modelprec['modelo'];
                      $model_precios->marca = $modelprec['marca'];
                      $model_precios->precio_mercado= $modelprec['precio_mercado'];
                      $model_precios->porc_descuento = $modelprec['porc_descuento'];
                      $model_precios->precio_f = $modelprec['precio_f'];
                      $model_precios->id_prec_prov = $modelprecioprov->id;
                      $model_precios->save(false);
              
                  }
                }
                $id_ppl->estado = 2;
                $id_ppl->update();
                  \Yii::$app->getSession()->setFlash('success', '¡La licitación ha sido cargada con éxito!');
                return $this->redirect(['indexlicproveedor']);
          }
          else{

        return $this->render('registro_lic_prov', [
            'model_licitacion' => $model_licitacion,
            'id_proveedor' => $id_proveedor,
            'modelprecioprov' => $modelprecioprov,
            'modelsolic' => $modelsolic,
            'modelsprecios' => (empty($modelsprecios)) ? [new Precios] : $modelsprecios,
        ]);
          }
    }

    /**
     * Displays a single Licitacion model.
     * @param integer $id
     * @return mixed
     */
    public function actionVistaproveedor($id,$prov)
    {
      $id_proveedor = User::findOne(Yii::$app->user->id);
      $ppl = ProveedorPiezaLic::find()->where(['id_licitacion'=> $id])->andWhere(['id_prov_pieza'=> $prov])->one();
      $precprov = PrecioProveedor::find()->where(['id_prov_licitacion'=>$ppl['id']])->One();

        $searchModel = new LicitacionesSearch();
        $dataProvider = $searchModel->searchVistaprov(Yii::$app->request->queryParams, $precprov['id']);
        return $this->render('vista_proveedor', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'precprov' => $precprov,
        ]);
    }
    /**
     * Updates an existing Licitacion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Licitacion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Licitacion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Licitacion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Licitacion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
