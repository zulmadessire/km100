<?php

namespace app\controllers;

use Yii;
use app\models\Pieza;
use app\models\ModeloPieza;
use app\models\MarcaInsumo;
use app\models\TipoPieza;
use app\models\PiezasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PiezaController implements the CRUD actions for Pieza model.
 */
class PiezaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pieza models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PiezasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionListarpiezas($id)
    {

        if (\Yii::$app->request->isAjax ) {
            $query = new Query();
            $query->select('*')
            ->from('modelo_pieza')
            ->where(['=','pieza_id', $id])
            ->orderBy('modelo_id_modelo');
            $rows = $query->all();

            if (count($rows) > 0) {
                echo "<option value=''> Seleccione... </option>";
                foreach ($rows as $aux) {
                    echo "<option value='".$aux['modelo_id_modelo']."'>" . $aux['modelo_id_modelo'] ."</option>";
                }
            } else {
                echo "<option> -- </option>";
            }
        }
        \Yii::$app->end();
    }
    /**
     * Displays a single Pieza model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pieza model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
    
       $model = new Pieza();
       $model_marca = new MarcaInsumo();
       $model_tipo = new TipoPieza();
        $modelpieza = [new ModeloPieza()];

        if ($model->load(Yii::$app->request->post())) {
 
        if ($_POST['TipoPieza']['id'] ==4 || $_POST['TipoPieza']['id'] ==5 ) {

     
            $model->tipo_pieza_id = $_POST['TipoPieza']['id'];
            $model->nombre = $_POST['Pieza']['nombre'];
            $model->unidad = $_POST['Pieza']['unidad'];
            $model->tamano = $_POST['Pieza']['tamano'];
             $model->tamano = $_POST['Pieza']['codigo'];

         
             if($model->save(false)){
               

                      if (isset($_POST['ModeloPieza'][0]['modelo_id_modelo'])) {
                  foreach ($_POST['ModeloPieza'] as $modelrub) {
                      $nuevo = new ModeloPieza();
                      $nuevo->modelo_id_modelo = $modelrub['modelo_id_modelo'];
                      $nuevo->pieza_id = $model->id_pieza;
                      $nuevo->anho = $modelrub['anho'];
                      $nuevo->save();
                      \Yii::$app->getSession()->setFlash('success', '¡El Material ha sido cargado con éxito!');
                  }
                 
              }
               
             } 
                }else{  
                    $model_marca2 = new Pieza();
                    $model_marca2->tipo_pieza_id = $_POST['TipoPieza']['id'];
                    $model_marca2->nombre = $_POST['Pieza']['nombre'];
                    $model_marca2->codigo = $_POST['Pieza']['codigo'];
                    $model_marca2->marca_insumo = $_POST['Pieza']['marca_insumo'];
                    $model_marca2->unidad = $_POST['Pieza']['unidad'];
                    $model_marca2->tamano = $_POST['Pieza']['tamano'];
                    $model_marca2->save(false);
                    \Yii::$app->getSession()->setFlash('success', '¡El Material ha sido cargado con éxito!');
                }
                


//print_r(Yii::$app->request->post()); die();

       return $this->redirect('index');

       
        } else {
            return $this->render('create', [
                'model' => $model,
                'model_tipo' => $model_tipo,
                'model_marca' => $model_marca,
                'modelpieza' => (empty($modelpieza)) ? [new Modelopieza] : $modelpieza,
            ]);
        }



    }

    /**
     * Updates an existing Pieza model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_pieza]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Pieza model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pieza model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pieza the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pieza::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
