<?php

namespace app\controllers;

use Yii;
use app\models\AuditoriaGeneral;
use app\models\AuditoriageneralSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\EstacionesSearch;
use app\models\Auditoriafs;
use app\models\SolAudFs;
use app\models\OtrasFs;
use app\models\AuditoriaEjecMant;
use app\models\AuditoriaMnto; 
use app\models\AddMnto;
use app\models\SolAudMnto;
use app\models\AspectosEvaluar;
use app\models\Aspectos;
use app\models\ImagenesFs;
use app\models\Empresa;
use app\models\Addfs;
use app\models\Estacion;
use yii\helpers\Json;
use kartik\mpdf\Pdf;
use mPDF;
/**
 * AuditoriageneralController implements the CRUD actions for Auditoriageneral model.
 */
class AuditoriageneralController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Auditoriageneral models.
     * @return mixed
     */
    public function actionIndex()
    {
       $searchModel = new estacionesSearch();
        $dataProvider = $searchModel->searchgeneral(Yii::$app->request->queryParams);
         
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

        ]);
    }

      /**
     * Lists all Auditoriageneral index auditorias continuas.
     * @return mixed
     */
    public function actionIndexcontinuo()
    {
       $searchModel = new estacionesSearch();
        $dataProvider = $searchModel->searchgeneral2(Yii::$app->request->queryParams);
         
        return $this->render('index_completo', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

        ]);
    }
 
    /**
     * Displays a single Auditoriageneral model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Auditoriageneral model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Auditoriageneral();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Auditoriageneral model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

       /**
     * Historico de auditorias
     * @autor Rosana Sanchez
     */
    public function actionHistorico()
    {
        $searchModel = new AuditoriageneralSearch();
        $dataProvider = $searchModel->searchhistorico(Yii::$app->request->queryParams);

        return $this->render('historico', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

           /**
     * Historico de auditorias
     * @autor Rosana Sanchez
     */
    public function actionHistoricogeneral()
    {
        $searchModel = new AuditoriageneralSearch();
        $dataProvider = $searchModel->searchhistoricogeneral(Yii::$app->request->queryParams);

        return $this->render('historico_general', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionImprimirseparadopdf($id,$fecha)
    {

        //Obtengo la auditoria fs
        $model_general = AuditoriaGeneral::find()->where(['id'=>$id])->orderBy('id DESC')->one();
        $modelfs = '';
        $modeladd = '';
        $modelsolaud = '';
        $modelmnto = '';
        $modeladdmnto = '';
        $modelsolaudmnto = '';
        $modelejec = '';
        $modelejec = '';
        $modelaspectos = '';

        if ($model_general->id_aud_fs) {
            
        $modelfs = AuditoriaFs::find()->where(['id' => $model_general->id_aud_fs])->one();
        $modeladd = Addfs::find()->where(['id_aud_fs'=>$modelfs['id']])->all();
        $modelsolaud = SolAudFs::find()->where(['id_aud_fs'=>$modelfs['id']])->all();
        $modelsotras = OtrasFs::find()->where(['id_aud_fs'=>$modelfs['id']])->all();
        $modelsimagenes = ImagenesFs::find()->where(['id_auditoriafs'=>$modelfs['id']])->all();

       
        }
        if ($model_general->id_aud_mnto) {
          
        $modelmnto = AuditoriaMnto::find()->where(['id' => $model_general->id_aud_mnto])->one();
        $modeladdmnto = AddMnto::find()->where(['id_aud_mnto'=>$modelmnto['id']])->all();
        $modelsolaudmnto = SolAudMnto::find()->where(['id_aud_mnto'=>$modelmnto['id']])->all();
        
        }
        if ($model_general->id_aud_ejec) {
            
        $modelejec = AuditoriaEjecMant::find()->where(['id' => $model_general->id_aud_mnto])->one();
             
        $modelaspectos = AspectosEvaluar::find()->where(['id_auditoria_ejec_mant'=>$modelejec['id']])->orderBy('id ASC')->all();
        }

 
        $pdf = new Pdf([
            'content' => $this->renderPartial('audtodospdf',[
            'modelfs' => $modelfs,
            'model_general' => $model_general,
            'modelejec' => $modelejec,
            'modelmnto' => $modelmnto,
            'modeladd' => $modeladd,
            'modelsolaud' => $modelsolaud,
            'modeladdmnto' => $modeladdmnto, 
            'modelsolaudmnto'=>$modelsolaudmnto,
            'modelaspectos' => $modelaspectos,
            'modelsotras' => $modelsotras,
            'modelsimagenes' => $modelsimagenes,
             ]),
            'destination' => Pdf::DEST_BROWSER,
            'mode' => Pdf::MODE_CORE,
            'format' => Pdf::FORMAT_A4,
            'filename' => 'OC_'.$model_general['id'].'.pdf',
            'options' => [
                'title' => 'AUDITORIAS',
                'subject' => 'AUD_'.$model_general['id'],
            ],
        ]);
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');

        return $pdf->render(); 
        exit;
    }

       /**
     * Visualizar orden compra en PDF
     * @autor Zulma Hernández
     */
    public function actionImprimirpdf($id,$fecha)
    {

        //Obtengo la auditoria fs
        
        $modelfs = AuditoriaFs::find()->where(['fecha'=>$fecha])->andWhere(['id_estacion'=>$id])->andWhere(['mensual' => 1])->orderBy('id DESC')->one();
        $modelmnto = AuditoriaMnto::find()->where(['fecha'=>$fecha])->andWhere(['id_estacion'=>$id])->andWhere(['mensual' => 1])->one();
        $modelejec = AuditoriaEjecMant::find()->where(['fecha'=>$fecha])->andWhere(['id_estacion'=>$id])->andWhere(['mensual' => 1])->one();
        $modeladd = Addfs::find()->where(['id_aud_fs'=>$modelfs['id']])->all();
        $modelsolaud = SolAudFs::find()->where(['id_aud_fs'=>$modelfs['id']])->all();
        $modelsotras = OtrasFs::find()->where(['id_aud_fs'=>$modelfs['id']])->all();
        $modeladdmnto = AddMnto::find()->where(['id_aud_mnto'=>$modelmnto['id']])->all();
        $modelsolaudmnto = SolAudMnto::find()->where(['id_aud_mnto'=>$modelmnto['id']])->all();
        $modelaspectos = AspectosEvaluar::find()->where(['id_auditoria_ejec_mant'=>$modelejec['id']])->orderBy('id ASC')->all();


        //  return $this->render('imprimirpdf', [
        //     'modelfs' => $modelfs,
        //     'modelejec' => $modelejec,
        //     'modelmnto' => $modelmnto,
        //     'modeladd' => $modeladd,
        //     'modelsolaud' => $modelsolaud,
        //     'modeladdmnto' => $modeladdmnto, 
        //     'modelsolaudmnto'=>$modelsolaudmnto,
        //     'modelaspectos' => $modelaspectos,
        // ]);

        /*echo "<pre>";
        print_r( $piezas );
        echo "</pre>";*/
        $pdf = new Pdf([
            'content' => $this->renderPartial('imprimirpdf',[
             'modelfs' => $modelfs,
            'modelejec' => $modelejec,
            'modelmnto' => $modelmnto,
            'modeladd' => $modeladd,
            'modelsolaud' => $modelsolaud,
            'modeladdmnto' => $modeladdmnto, 
            'modelsolaudmnto'=>$modelsolaudmnto,
            'modelaspectos' => $modelaspectos,
            'modelsotras' => $modelsotras,
             ]),
            'destination' => Pdf::DEST_BROWSER,
            'mode' => Pdf::MODE_CORE,
            'format' => Pdf::FORMAT_A4,
            'filename' => 'OC_'.$modelfs['id'].'.pdf',
            'options' => [
                'title' => 'AUDITORIAS',
                'subject' => 'AUD_'.$modelfs['id'],
            ],
        ]);
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');

        return $pdf->render(); 
        exit;
    }

    /**
     * Deletes an existing Auditoriageneral model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Auditoriageneral model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Auditoriageneral the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Auditoriageneral::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
