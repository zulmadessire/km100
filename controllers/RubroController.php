<?php

namespace app\controllers;

use Yii;
use app\models\Rubro;
use app\models\RubroSearch;
use app\models\ModeloRubro;
use app\models\RubroModeloAsignado;
use yii\web\Controller;
use yii\db\Query;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RubroController implements the CRUD actions for Rubro model.
 */
class RubroController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Rubro models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RubroSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Rubro model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Rubro model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Rubro();
        $modelrubro = [new ModeloRubro()];

        if ($model->load(Yii::$app->request->post())) {
//print_r(Yii::$app->request->post()); die();
            if($model->save()){
              if (isset($_POST['ModeloRubro'][0]['id_modelo_rubro'])) {
                  foreach ($_POST['ModeloRubro'] as $modelrub) {
                      $nuevo_modelrub = new RubroModeloAsignado();
                      $nuevo_modelrub->id_modelo_rubro = $modelrub['id_modelo_rubro'];
                      $nuevo_modelrub->id_rubro = $model->id_rubro;
                      die();
                      $nuevo_modelrub->save();
                  }
                  \Yii::$app->getSession()->setFlash('success', '¡El Rubro ha sido cargado con éxito!');
              }
            }
            return $this->redirect(['view', 'id' => $model->id_rubro]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'modelrubro' => (empty($modelrubro)) ? [new ModeloRubro] : $modelrubro,
            ]);
        }
    }

    public function actionListarmodelosrubro($id)
    {
        if (\Yii::$app->request->isAjax ) {
            $query = new Query();
            $query->select('*')
                ->from('modelo_rubro')
                ->where(['=','id_marca_rubro', $id])
                ->orderBy('nombre');
            $rows = $query->all();
            if (count($rows) > 0) {
                echo "<option value=''> Seleccione... </option>";
                foreach ($rows as $aux) {
                    echo "<option value='".$aux['id_modelo_rubro']."'>" . $aux['nombre'] ."</option>";
                }
            } else {
                echo "<option> -- </option>";
            }
        }
        \Yii::$app->end();
    }

    /**
     * Updates an existing Rubro model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $modelrubro = ModeloRubro::find()
            ->joinWith('rubroModeloAsignados')
            ->where([ 'rubro_modelo_asignado.id_rubro' => $id])
            ->all();
        if ($model->load(Yii::$app->request->post())) {
          if($model->save()){
            if (isset($_POST['ModeloRubro'][0]['id_modelo_rubro'])) {
                $rubroeliminar = RubroModeloAsignado::find()->where([ 'id_rubro' => $model->id_rubro])->All();
                foreach ($rubroeliminar as $key => $value) {
                  $value->delete();
                }
                foreach ($_POST['ModeloRubro'] as $modelrub) {
                    $nuevo_modelrub = new RubroModeloAsignado();
                    $nuevo_modelrub->id_modelo_rubro = $modelrub['id_modelo_rubro'];
                    $nuevo_modelrub->id_rubro = $model->id_rubro;
                    $nuevo_modelrub->save();
                }
                \Yii::$app->getSession()->setFlash('success', '¡El Rubro ha sido modificado con éxito!');
            }
          }
            return $this->redirect(['view', 'id' => $model->id_rubro]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelrubro' => $modelrubro,
            ]);
        }
    }

    /**
     * Deletes an existing Rubro model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Rubro model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Rubro the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Rubro::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
