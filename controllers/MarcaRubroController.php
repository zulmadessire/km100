<?php

namespace app\controllers;

use Yii;
use app\models\MarcaRubro;
use app\models\ModeloRubro;
use app\models\MarcaRubroSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MarcaRubroController implements the CRUD actions for MarcaRubro model.
 */
class MarcaRubroController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MarcaRubro models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MarcaRubroSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MarcaRubro model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MarcaRubro model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MarcaRubro();
        $modelrubro = [new ModeloRubro()];

        if ($model->load(Yii::$app->request->post())) {
          if ($model->save()) {
            if (isset($_POST['ModeloRubro'][0]['nombre'])) {
                foreach ($_POST['ModeloRubro'] as $mod) {
                    $nuevo_modelo = new ModeloRubro();
                    $nuevo_modelo->nombre = $mod['nombre'];
                    $nuevo_modelo->id_marca_rubro = $model->id_marca_rubro;
                    $nuevo_modelo->save();
                }
            }
          }
            return $this->redirect(['/rubro/create', 'id' => $model->id_marca_rubro]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'modelrubro' => (empty($modelrubro)) ? [new ModeloRubro] : $modelrubro,
            ]);
        }
    }

    /**
     * Updates an existing MarcaRubro model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_marca_rubro]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MarcaRubro model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MarcaRubro model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MarcaRubro the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MarcaRubro::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
