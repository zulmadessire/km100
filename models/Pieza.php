<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pieza".
 *
 * @property integer $id_pieza
 * @property string $nombre
 * @property string $tamano
 * @property string $unidad
 * @property integer $tipo_pieza_id
 * @property string $marca_insumo
 * @property string $codigo
 * @property string $marca
 * @property string $anho
 * @property integer $modelo_id_modelo
 *
 * @property ActividadPieza[] $actividadPiezas
 * @property ChecklistAvaluoPieza[] $checklistAvaluoPiezas
 * @property CotizacionTallerPieza[] $cotizacionTallerPiezas
 * @property TipoPieza $tipoPieza
 * @property Modelo $modeloIdModelo
 * @property ProveedorPieza[] $proveedorPiezas
 */
class Pieza extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pieza';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipo_pieza_id', 'nombre'], 'required'],
            [['tipo_pieza_id', 'nombre'], 'safe'],
            [['tipo_pieza_id', 'modelo_id_modelo'], 'integer'],
            ['nombre', 'unique', 'targetAttribute' => ['nombre'], 'message' => 'Nombre de pieza en uso'],
            [['nombre'], 'string', 'max' => 50],
            [['tamano', 'unidad'], 'string', 'max' => 45],
            [['marca_insumo', 'marca', 'anho'], 'string', 'max' => 100],
            [['codigo','serial'], 'string', 'max' => 30],
            [['tipo_pieza_id'], 'exist', 'skipOnError' => true, 'targetClass' => TipoPieza::className(), 'targetAttribute' => ['tipo_pieza_id' => 'id']],
            [['modelo_id_modelo'], 'exist', 'skipOnError' => true, 'targetClass' => Modelo::className(), 'targetAttribute' => ['modelo_id_modelo' => 'id_modelo']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_pieza' => 'Pieza',
            'nombre' => 'Nombre',
            'tamano' => 'Tamaño',
            'unidad' => 'Unidad',
            'tipo_pieza_id' => 'Tipo de Pieza',
            'marca_insumo' => 'Marca del Insumo',
            'codigo' => 'Codigo',
            'marca' => 'Marca',
            'anho' => 'Año',
            'serial' => 'Serial',
            'modelo_id_modelo' => 'Modelo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActividadPiezas()
    {
        return $this->hasMany(ActividadPieza::className(), ['id_pieza' => 'id_pieza']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChecklistAvaluoPiezas()
    {
        return $this->hasMany(ChecklistAvaluoPieza::className(), ['pieza_id' => 'id_pieza']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCotizacionTallerPiezas()
    {
        return $this->hasMany(CotizacionTallerPieza::className(), ['id_pieza' => 'id_pieza']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoPieza()
    {
        return $this->hasOne(TipoPieza::className(), ['id' => 'tipo_pieza_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModeloIdModelo()
    {
        return $this->hasOne(Modelo::className(), ['id_modelo' => 'modelo_id_modelo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProveedorPiezas()
    {
        return $this->hasMany(ProveedorPieza::className(), ['pieza_id' => 'id_pieza']);
    }
}
