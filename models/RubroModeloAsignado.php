<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rubro_modelo_asignado".
 *
 * @property integer $id_rubro_modelo_asignado
 * @property integer $id_rubro
 * @property integer $id_modelo_rubro
 *
 * @property ModeloRubro $idModeloRubro
 * @property Rubro $idRubro
 */
class RubroModeloAsignado extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rubro_modelo_asignado';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_rubro', 'id_modelo_rubro'], 'required'],
            [['id_rubro', 'id_modelo_rubro'], 'integer'],
            [['id_modelo_rubro'], 'exist', 'skipOnError' => true, 'targetClass' => ModeloRubro::className(), 'targetAttribute' => ['id_modelo_rubro' => 'id_modelo_rubro']],
            [['id_rubro'], 'exist', 'skipOnError' => true, 'targetClass' => Rubro::className(), 'targetAttribute' => ['id_rubro' => 'id_rubro']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_rubro_modelo_asignado' => 'Id Rubro Modelo Asignado',
            'id_rubro' => 'Id Rubro',
            'id_modelo_rubro' => 'Id Modelo Rubro',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdModeloRubro()
    {
        return $this->hasOne(ModeloRubro::className(), ['id_modelo_rubro' => 'id_modelo_rubro']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRubro()
    {
        return $this->hasOne(Rubro::className(), ['id_rubro' => 'id_rubro']);
    }
}
