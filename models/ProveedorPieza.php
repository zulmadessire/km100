<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proveedor_pieza".
 *
 * @property integer $id
 * @property integer $proveedor_id
 * @property integer $pieza_id
 * @property integer $marca_insumo_id
 *
 * @property MarcaInsumo $marcaInsumo
 * @property Pieza $pieza
 * @property Proveedor $proveedor
 */
class ProveedorPieza extends \yii\db\ActiveRecord
{
    public $type;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'proveedor_pieza';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['proveedor_id','type'], 'required'],
            ['pieza_id', 'required', 'when' => function ($model) {
        return $model->type == 4 || $model->type == 5;
    }, 'whenClient' => "function (attribute, value) {
        return $('#tipo_pieza').val() == 4 || $('#tipo_pieza').val() == 5;
    }"],
    ['marca_insumo_id', 'required', 'when' => function ($model) {
        return $model->type == 1;
    }, 'whenClient' => "function (attribute, value) {
        return $('#tipo_pieza').val() == 1;
    }"],
            [['proveedor_id', 'pieza_id', 'marca_insumo_id','type'], 'integer'],
            [['pieza_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pieza::className(), 'targetAttribute' => ['pieza_id' => 'id_pieza']],
            [['proveedor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Proveedor::className(), 'targetAttribute' => ['proveedor_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'proveedor_id' => 'Proveedor ID',
            'pieza_id' => 'Pieza',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPieza()
    {
        return $this->hasOne(Pieza::className(), ['id_pieza' => 'pieza_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProveedor()
    {
        return $this->hasOne(Proveedor::className(), ['id' => 'proveedor_id']);
    }
}
