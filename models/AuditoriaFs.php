<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "auditoria_fs".
 *
 * @property integer $id
 * @property string $fecha
 * @property integer $estado
 * @property string $motivo
 * @property integer $desviacion
 * @property integer $veh_fuera_ser_est
 * @property string $plan_accion
 * @property string $observacion
 * @property integer $id_estacion
 * @property integer $veh_fuera_serv_reg
 *
 * @property AddFs[] $addFs
 * @property Estacion $idEstacion
 * @property AuditoriaGeneral[] $auditoriaGenerals
 * @property SolAudFs[] $solAudFs
 */
class AuditoriaFs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auditoria_fs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['estado', 'desviacion', 'veh_fuera_ser_est', 'id_estacion', 'veh_fuera_serv_reg','mensual'], 'integer'],
            [['id_estacion'], 'required'],
            [['motivo'], 'string', 'max' => 500],
            [['plan_accion', 'observacion'], 'string', 'max' => 1000],
            [['id_estacion'], 'exist', 'skipOnError' => true, 'targetClass' => Estacion::className(), 'targetAttribute' => ['id_estacion' => 'id_estacion']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha' => 'Fecha',
            'estado' => 'Estado',
            'motivo' => 'Motivo',
            'desviacion' => 'Desviacion',
            'veh_fuera_ser_est' => 'Veh Fuera Ser Est',
            'plan_accion' => 'Plan Accion',
            'observacion' => 'Observacion',
            'id_estacion' => 'Id Estacion',
            'veh_fuera_serv_reg' => 'Veh Fuera Serv Reg',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddFs()
    {
        return $this->hasMany(AddFs::className(), ['id_aud_fs' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstacion()
    {
        return $this->hasOne(Estacion::className(), ['id_estacion' => 'id_estacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuditoriaGenerals()
    {
        return $this->hasMany(AuditoriaGeneral::className(), ['id_aud_fs' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSolAudFs()
    {
        return $this->hasMany(SolAudFs::className(), ['id_aud_fs' => 'id']);
    }
   
}
