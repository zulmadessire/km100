<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vehiculo_taller".
 *
 * @property integer $id_vehiculo_taller
 * @property integer $id_modelo
 * @property integer $id_taller
 *
 * @property Modelo $idModelo
 * @property Taller $idTaller
 */
class VehiculoTaller extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vehiculo_taller';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_modelo', 'id_taller'], 'required'],
            [['id_modelo', 'id_taller'], 'integer'],
            [['id_modelo'], 'exist', 'skipOnError' => true, 'targetClass' => Modelo::className(), 'targetAttribute' => ['id_modelo' => 'id_modelo']],
            [['id_taller'], 'exist', 'skipOnError' => true, 'targetClass' => Taller::className(), 'targetAttribute' => ['id_taller' => 'id_taller']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_vehiculo_taller' => 'Id Vehiculo Taller',
            'id_modelo' => 'Id Modelo',
            'id_taller' => 'Id Taller',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdModelo()
    {
        return $this->hasOne(Modelo::className(), ['id_modelo' => 'id_modelo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTaller()
    {
        return $this->hasOne(Taller::className(), ['id_taller' => 'id_taller']);
    }
}
