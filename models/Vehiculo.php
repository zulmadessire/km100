<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vehiculo".
 *
 * @property integer $id
 * @property string $color
 * @property string $transmision
 * @property string $chasis
 * @property string $placa
 * @property string $mva
 * @property integer $id_categoria
 * @property string $clase
 * @property string $combustible
 * @property string $fecha_compra
 * @property string $fecha_inicial
 * @property integer $tiempo_vida
 * @property string $km
 * @property string $fecha_renovacion_placa
 * @property string $fecha_emision_placa
 * @property integer $wizard
 * @property integer $exactus
 * @property integer $id_modelo
 * @property integer $id_concesionario
 * @property integer $transito
 * @property integer $anho
 * @property integer $id_propietario
 * @property string $fecha_emision_seguro
 * @property string $fecha_venc_seguro
 * @property string $fecha_emision_marbete
 * @property string $fecha_venc_marbete
 * @property string $file
 * @property string $fecha_venta
 *
 * @property EstadoVehiculo[] $estadoVehiculos
 * @property GarantiaVehiculo[] $garantiaVehiculos
 * @property Notificaciones[] $notificaciones
 * @property ProyeccionMantenimiento[] $proyeccionMantenimientos
 * @property Servicio[] $servicios
 * @property SolicitudCompraEstacion[] $solicitudCompraEstacions
 * @property Categoria $idCategoria
 * @property Concesionario $idConcesionario
 * @property Modelo $idModelo
 */
class Vehiculo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vehiculo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_categoria', 'tiempo_vida', 'wizard', 'exactus', 'id_modelo', 'id_concesionario', 'transito', 'anho', 'id_propietario'], 'integer'],
            [['fecha_compra', 'fecha_inicial', 'fecha_renovacion_placa', 'fecha_emision_placa', 'fecha_emision_seguro', 'fecha_venc_seguro', 'fecha_emision_marbete', 'fecha_venc_marbete', 'fecha_venta'], 'safe'],
            [['color', 'transmision', 'chasis', 'placa', 'mva', 'clase', 'combustible', 'km'], 'string', 'max' => 45],
            [['file'], 'string', 'max' => 200],
            [['id_categoria'], 'exist', 'skipOnError' => true, 'targetClass' => Categoria::className(), 'targetAttribute' => ['id_categoria' => 'id']],
            [['id_concesionario'], 'exist', 'skipOnError' => true, 'targetClass' => Concesionario::className(), 'targetAttribute' => ['id_concesionario' => 'id']],
            [['id_modelo'], 'exist', 'skipOnError' => true, 'targetClass' => Modelo::className(), 'targetAttribute' => ['id_modelo' => 'id_modelo']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'color' => 'Color',
            'transmision' => 'Transmision',
            'chasis' => 'Chasis',
            'placa' => 'Placa',
            'mva' => 'Mva',
            'id_categoria' => 'Id Categoria',
            'clase' => 'Clase',
            'combustible' => 'Combustible',
            'fecha_compra' => 'Fecha Compra',
            'fecha_inicial' => 'Fecha Inicial',
            'tiempo_vida' => 'Tiempo Vida',
            'km' => 'Km',
            'fecha_renovacion_placa' => 'Fecha Renovacion Placa',
            'fecha_emision_placa' => 'Fecha Emision Placa',
            'wizard' => 'Wizard',
            'exactus' => 'Exactus',
            'id_modelo' => 'Id Modelo',
            'id_concesionario' => 'Id Concesionario',
            'transito' => 'Transito',
            'anho' => 'Año',
            'id_propietario' => 'Id Propietario',
            'fecha_emision_seguro' => 'Fecha Emision Seguro',
            'fecha_venc_seguro' => 'Fecha Venc Seguro',
            'fecha_emision_marbete' => 'Fecha Emision Marbete',
            'fecha_venc_marbete' => 'Fecha Venc Marbete',
            'file' => 'File',
            'fecha_venta' => 'Fecha Venta',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstadoVehiculos()
    {
        return $this->hasMany(EstadoVehiculo::className(), ['id_vehiculo' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGarantiaVehiculos()
    {
        return $this->hasMany(GarantiaVehiculo::className(), ['id_vehiculo' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificaciones()
    {
        return $this->hasMany(Notificaciones::className(), ['id_vehiculo' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProyeccionMantenimientos()
    {
        return $this->hasMany(ProyeccionMantenimiento::className(), ['id_vehiculo' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicios()
    {
        return $this->hasMany(Servicio::className(), ['id_vehiculo' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSolicitudCompraEstacions()
    {
        return $this->hasMany(SolicitudCompraEstacion::className(), ['id_vehiculo' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCategoria()
    {
        return $this->hasOne(Categoria::className(), ['id' => 'id_categoria']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdConcesionario()
    {
        return $this->hasOne(Concesionario::className(), ['id' => 'id_concesionario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdModelo()
    {
        return $this->hasOne(Modelo::className(), ['id_modelo' => 'id_modelo']);
    }
     /**
     * Devuelve el estatus actual del vehiculo
     */
    public function getEstatusMantenimiento()
    {
        $estado = EstadoVehiculo::find()->joinWith('idEstado')->where([ 'id_vehiculo' => $this->id, 'estado.id_tipo_estado' => 4 ])->orderBy('id DESC')->one();

        return '<span class="label label-oval" style="background-color:'.$estado->idEstado->color.'; ">'.$estado->idEstado->nombre.'</span>';

    }
}
