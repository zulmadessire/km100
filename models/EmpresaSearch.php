<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Empresa;

/**
 * EmpresaSearch represents the model behind the search form about `app\models\Empresa`.
 */
class EmpresaSearch extends Empresa
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_empresa'], 'integer'],
            [['nombre', 'nombre_comercial'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    { 
        $query = Empresa::find();

        if ( isset( $params['EmpresaSearch']['id_estacion'] ) AND $params['EmpresaSearch']['id_estacion'] != '') {
           $query->andWhere( '(SELECT es.id_estacion FROM estacion es INNER JOIN empresa em ON em.id_empresa = es.id_empresa AND es.id_estacion = '.$params['EmpresaSearch']['id_estacion'].' WHERE em.id_empresa = empresa.id_empresa LIMIT 1) = '.$params['EmpresaSearch']['id_estacion'] );

       
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_empresa' => $this->id_empresa,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'nombre_comercial', $this->nombre_comercial]);

        return $dataProvider;
    }
}
