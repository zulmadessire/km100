<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TipoMantenimiento;



/**
 * TipoMantenimientoSearch represents the model behind the search form about `app\models\TipoMantenimiento`.
 */
class TipoMantenimientoSearch extends TipoMantenimiento
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_tipo_mnto'], 'integer'],
            [['nombre'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        // $query = TipoMantenimiento::find();
          $query = TipoMantenimiento::find()
        ->where(['estado' => '1']);
       
        // add conditions that should always apply here

        if ( isset( $params['TipoMantenimientoSearch']['id_actividad_mnto'] ) AND $params['TipoMantenimientoSearch']['id_actividad_mnto'] != '') {
            $query->andWhere( '(SELECT am.id_actividad_mnto FROM actividad_mnto am INNER JOIN tipo_mantenimiento tm ON tm.id_tipo_mnto = am.id_tipo_mnto AND am.id_actividad_mnto = '.$params['TipoMantenimientoSearch']['id_actividad_mnto'].' WHERE tm.id_tipo_mnto = tipo_mantenimiento.id_tipo_mnto LIMIT 1) = '.$params['TipoMantenimientoSearch']['id_actividad_mnto'] );
            
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_tipo_mnto' => $this->id_tipo_mnto,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre]);

        return $dataProvider;
    }
}
