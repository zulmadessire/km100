<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "actividad_mnto".
 *
 * @property integer $id_actividad_mnto
 * @property string $nombre
 * @property integer $tipo_actividad
 * @property integer $id_tipo_mnto
 *
 * @property ActividadPieza[] $actividadPiezas
 */
class ActividadMnto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'actividad_mnto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'tipo_actividad'], 'required'],
            [['tipo_actividad', 'id_tipo_mnto'], 'integer'],
            [['nombre'], 'string', 'max' => 50],
            [['id_tipo_mnto'], 'exist', 'skipOnError' => true, 'targetClass' => TipoMantenimiento::className(), 'targetAttribute' => ['id_tipo_mnto' => 'id_tipo_mnto']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_actividad_mnto' => 'Id Actividad Mnto',
            'nombre' => 'Nombre',
            'tipo_actividad' => 'Tipo Actividad',
            'id_tipo_mnto' => 'Id Tipo Mnto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActividadPiezas()
    {
        return $this->hasMany(ActividadPieza::className(), ['id_actividad_mnto' => 'id_actividad_mnto']);
    }

    // viejos
        /**
     * @return \yii\db\ActiveQuery
     */
 
}
