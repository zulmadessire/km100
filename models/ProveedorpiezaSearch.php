<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Proveedorpieza;

/**
 * ProveedorpiezaSearch represents the model behind the search form about `app\models\Proveedorpieza`.
 */
class ProveedorpiezaSearch extends Proveedorpieza
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pieza_id', 'proveedor_id'], 'integer'],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Proveedorpieza::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'pieza_id' => $this->pieza_id,
            'proveedor_id' => $this->proveedor_id,
        ]);

        

        return $dataProvider;
    }
}
