<?php
namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;
use app\models\MantenimientoPreventivo;

class UploadMantenimiento extends Model
{
    /**
     * @var UploadedFile[]
     */

    public $imageFiles;

    public function rules()
    {
        return [
            [['imageFiles'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf', 'maxFiles' => 1, 'maxSize' => 4194304],
        ];
    }

    public function attributeLabels()
    {
        return [
            'imageFiles' => 'IMAGEN DE SOPORTE',
        ];
    }

    public function upload($id)
    {
     
        //if ($this->validate()) {
            foreach ($this->imageFiles as $i => $file) {
                $soporte = MantenimientoPreventivo::find()->where(['id' => $id])->One();

                if ($soporte->imagen_soporte) {
                    unlink('.'.$soporte->imagen_soporte);
                }
                $text = $file->baseName;
                $nom_img  = str_replace(' ', '', $text);
                $file->saveAs(\Yii::$app->basePath.'/files/preventivo/'.$id.'-'.$nom_img.'.'.$file->extension);

                $soporte->imagen_soporte=\Yii::$app->basePath.'/files/preventivo/'.$id.'-'.$nom_img.'.'.$file->extension;
                $soporte->save();
            }
            return true;
        /*} else {
          die('aqui oy');
            return false;
        }*/
    }
}
