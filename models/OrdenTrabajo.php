<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orden_trabajo".
 *
 * @property integer $id
 * @property string $fecha
 * @property string $observaciones
 * @property integer $estado
 * @property integer $id_cotizacion_taller
 * @property integer $motivo_cancelacion
 * @property integer $aprobacion_cancelacion
 *
 * @property CotizacionTaller $idCotizacionTaller
 * @property SolicitudCompraPieza[] $solicitudCompraPiezas
 */
class OrdenTrabajo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orden_trabajo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['observaciones'], 'string'],
            [['estado', 'id_cotizacion_taller', 'motivo_cancelacion', 'aprobacion_cancelacion'], 'integer'],
            [['id_cotizacion_taller'], 'required'],
            [['id_cotizacion_taller'], 'exist', 'skipOnError' => true, 'targetClass' => CotizacionTaller::className(), 'targetAttribute' => ['id_cotizacion_taller' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha' => 'Fecha',
            'observaciones' => 'Observaciones',
            'estado' => 'Estado',
            'id_cotizacion_taller' => 'Id Cotizacion Taller',
            'motivo_cancelacion' => 'Motivo Cancelacion',
            'aprobacion_cancelacion' => 'Aprobacion Cancelacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCotizacionTaller()
    {
        return $this->hasOne(CotizacionTaller::className(), ['id' => 'id_cotizacion_taller']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSolicitudCompraPiezas()
    {
        return $this->hasMany(SolicitudCompraPieza::className(), ['id_orden_trabajo' => 'id']);
    }
}
