<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Vehiculo;

/**
 * VehiculoSearch represents the model behind the search form about `app\models\Vehiculo`.
 */
class VehiculoSearch extends Vehiculo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_categoria', 'tiempo_vida', 'wizard', 'exactus', 'id_modelo', 'id_concesionario'], 'integer'],
            [['color', 'transmision', 'chasis', 'placa', 'mva', 'clase', 'combustible', 'fecha_compra', 'fecha_inicial', 'km', 'fecha_renovacion_placa', 'fecha_emision_placa'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        
        $query = Vehiculo::find();

        if ( isset($params['VehiculoSearch']) ) {

            if ( isset($params['VehiculoSearch']['id_estacion']) AND $params['VehiculoSearch']['id_estacion'] != '') {
                $query->where( '(SELECT id_estacion FROM flota WHERE id_vehiculo = vehiculo.id ORDER BY id DESC LIMIT 1) = '.$params['VehiculoSearch']['id_estacion'] );
            }

            if ( isset($params['VehiculoSearch']['id_marca']) AND $params['VehiculoSearch']['id_marca'] != '') {
                $query->andwhere( '(SELECT id_marca FROM modelo WHERE id_modelo = vehiculo.id_modelo LIMIT 1) = '.$params['VehiculoSearch']['id_marca'] );
            }

            if ( isset($params['VehiculoSearch']['id_empresa']) AND $params['VehiculoSearch']['id_empresa'] != '') {
                $query->andWhere( '(SELECT emp.nombre_comercial FROM flota f INNER JOIN estacion es ON es.id_estacion = f.id_estacion INNER JOIN empresa emp ON es.id_empresa = emp.id_empresa WHERE f.id_vehiculo = vehiculo.id ORDER BY f.id DESC LIMIT 1) = '."'".$params['VehiculoSearch']['id_empresa']."'" );
            }

            if ( isset( $params['VehiculoSearch']['id_estado'] ) AND $params['VehiculoSearch']['id_estado'] != '') {
                $query->andWhere( '(SELECT ev.id_estado FROM estado_vehiculo ev INNER JOIN estado es ON es.id = ev.id_estado AND es.id_tipo_estado = 1  WHERE ev.id_vehiculo = vehiculo.id ORDER BY ev.id DESC LIMIT 1) = '.$params['VehiculoSearch']['id_estado'] );
            }

            if ( isset( $params['VehiculoSearch']['id_concesionario'] ) AND $params['VehiculoSearch']['id_concesionario'] != '') {
                $query->andWhere( '(SELECT id FROM concesionario WHERE id = vehiculo.id_concesionario LIMIT 1) = '.$params['VehiculoSearch']['id_concesionario'] );
            }
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fecha_compra' => $this->fecha_compra,
            'fecha_inicial' => $this->fecha_inicial,
            'tiempo_vida' => $this->tiempo_vida,
            'fecha_renovacion_placa' => $this->fecha_renovacion_placa,
            'fecha_emision_placa' => $this->fecha_emision_placa,
            'wizard' => $this->wizard,
            'exactus' => $this->exactus,
            'id_modelo' => $this->id_modelo,
            'id_concesionario' => $this->id_concesionario,
            'id_categoria' => $this->id_categoria, 
        ]);

        $query->andFilterWhere(['like', 'color', $this->color])
            ->andFilterWhere(['like', 'transmision', $this->transmision])
            ->andFilterWhere(['like', 'chasis', $this->chasis])
            ->andFilterWhere(['like', 'placa', $this->placa])
            ->andFilterWhere(['like', 'mva', $this->mva])
            ->andFilterWhere(['like', 'clase', $this->clase])
            ->andFilterWhere(['like', 'combustible', $this->combustible])
            ->andFilterWhere(['like', 'km', $this->km]);

        return $dataProvider;
    }
}
