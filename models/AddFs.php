<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "add_fs".
 *
 * @property integer $id
 * @property string $causa
 * @property string $observacion
 * @property integer $id_aud_fs
 * @property string $ficha
 * @property integer $id_modelo
 * @property string $km_actual
 * @property integer $id_tipo_mnto
 *
 * @property AuditoriaFs $idAudFs
 * @property TipoMantenimiento $idTipoMnto
 * @property Modelo $idModelo
 */
class AddFs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'add_fs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_aud_fs'], 'required'],
            [['id_aud_fs', 'id_modelo', 'id_tipo_mnto'], 'integer'],
            [['causa'], 'string', 'max' => 500],
            [['observacion'], 'string', 'max' => 1000],
            [['ficha'], 'string', 'max' => 20],
            [['km_actual'], 'string', 'max' => 40],
            [['id_aud_fs'], 'exist', 'skipOnError' => true, 'targetClass' => AuditoriaFs::className(), 'targetAttribute' => ['id_aud_fs' => 'id']],
            [['id_tipo_mnto'], 'exist', 'skipOnError' => true, 'targetClass' => TipoMantenimiento::className(), 'targetAttribute' => ['id_tipo_mnto' => 'id_tipo_mnto']],
            [['id_modelo'], 'exist', 'skipOnError' => true, 'targetClass' => Modelo::className(), 'targetAttribute' => ['id_modelo' => 'id_modelo']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'causa' => 'Causa',
            'observacion' => 'Observacion',
            'id_aud_fs' => 'Id Aud Fs',
            'ficha' => 'Ficha',
            'id_modelo' => 'Id Modelo',
            'km_actual' => 'Km Actual',
            'id_tipo_mnto' => 'Id Tipo Mnto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAudFs()
    {
        return $this->hasOne(AuditoriaFs::className(), ['id' => 'id_aud_fs']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTipoMnto()
    {
        return $this->hasOne(TipoMantenimiento::className(), ['id_tipo_mnto' => 'id_tipo_mnto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdModelo()
    {
        return $this->hasOne(Modelo::className(), ['id_modelo' => 'id_modelo']);
    }
}
