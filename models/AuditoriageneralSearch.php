<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Auditoriageneral;

/**
 * AuditoriageneralSearch represents the model behind the search form about `app\models\Auditoriageneral`.
 */
class AuditoriageneralSearch extends Auditoriageneral
{
    public $empresa;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'estado','empresa', 'id_estacion', 'id_aud_fs', 'id_aud_mnto', 'id_aud_ejec'], 'integer'],
            [['fecha'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Auditoriageneral::find();

        if ( isset($params['AuditoriageneralSearch']['empresa']) AND $params['AuditoriageneralSearch']['empresa'] != '') {
                $query->andWhere( '(SELECT emp.id_empresa FROM auditoria_general ag INNER JOIN estacion es ON es.id_estacion = ag.id_estacion INNER JOIN empresa emp ON es.id_empresa = emp.id_empresa WHERE ag.id = auditoria_general.id  LIMIT 1) = '. $params['AuditoriageneralSearch']['empresa'] );
            }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fecha' => $this->fecha,
            'estado' => $this->estado,
            'id_estacion' => $this->id_estacion,
            'id_aud_fs' => $this->id_aud_fs,
            'id_aud_mnto' => $this->id_aud_mnto,
            'id_aud_ejec' => $this->id_aud_ejec,
        ]);

        return $dataProvider;
    }
     public function searchhistoricogeneral($params)
    {
        $query = Auditoriageneral::find();
      

        if ( isset($params['AuditoriageneralSearch']['empresa']) AND $params['AuditoriageneralSearch']['empresa'] != '') {
                $query->andWhere( '(SELECT emp.id_empresa FROM auditoria_general ag INNER JOIN estacion es ON es.id_estacion = ag.id_estacion INNER JOIN empresa emp ON es.id_empresa = emp.id_empresa WHERE ag.id = auditoria_general.id  LIMIT 1) = '. $params['AuditoriageneralSearch']['empresa'] );
            }

       if (isset($params['AuditoriageneralSearch']['fecha1']) AND $params['AuditoriageneralSearch']['fecha1'] != '' AND isset($params['AuditoriageneralSearch']['fecha1']) AND $params['AuditoriageneralSearch']['fecha1'] != '' ) {
            $query->andwhere(['between', 'fecha',  $params['AuditoriageneralSearch']['fecha1'], $params['AuditoriageneralSearch']['fecha2'] ]);
    
        }
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fecha' => $this->fecha,
            'estado' => $this->estado,
            'id_estacion' => $this->id_estacion,
            'id_aud_fs' => $this->id_aud_fs,
            'id_aud_mnto' => $this->id_aud_mnto,
            'id_aud_ejec' => $this->id_aud_ejec,
        ]);

        return $dataProvider;
    }
      public function searchhistorico($params)
    {
        $query = Auditoriageneral::find()
        ->where('estado = 3')
        ->groupBy(['fecha'])
        ->groupBy(['id_estacion']);
      

        if ( isset($params['AuditoriageneralSearch']['empresa']) AND $params['AuditoriageneralSearch']['empresa'] != '') {
                $query->andWhere( '(SELECT emp.id_empresa FROM auditoria_general ag INNER JOIN estacion es ON es.id_estacion = ag.id_estacion INNER JOIN empresa emp ON es.id_empresa = emp.id_empresa WHERE ag.id = auditoria_general.id  LIMIT 1) = '. $params['AuditoriageneralSearch']['empresa'] );
            }

       if (isset($params['AuditoriageneralSearch']['fecha1']) AND $params['AuditoriageneralSearch']['fecha1'] != '' AND isset($params['AuditoriageneralSearch']['fecha1']) AND $params['AuditoriageneralSearch']['fecha1'] != '' ) {
            $query->andwhere(['between', 'fecha',  $params['AuditoriageneralSearch']['fecha1'], $params['AuditoriageneralSearch']['fecha2'] ]);
    
        }
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fecha' => $this->fecha,
            'estado' => $this->estado,
            'id_estacion' => $this->id_estacion,
            'id_aud_fs' => $this->id_aud_fs,
            'id_aud_mnto' => $this->id_aud_mnto,
            'id_aud_ejec' => $this->id_aud_ejec,
        ]);

        return $dataProvider;
    }
}
