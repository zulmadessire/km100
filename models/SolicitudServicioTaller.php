<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "solicitud_servicio_taller".
 *
 * @property integer $id
 * @property double $costo_promedio
 * @property integer $disponibilidad
 * @property double $cumplimiento
 * @property integer $garantia
 * @property integer $id_servicio
 * @property integer $id_taller
 *
 * @property CotizacionTaller[] $cotizacionTallers
 * @property Servicio $idServicio
 * @property Taller $idTaller
 */
class SolicitudServicioTaller extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'solicitud_servicio_taller';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['costo_promedio', 'cumplimiento'], 'number'],
            [['disponibilidad', 'garantia', 'id_servicio', 'id_taller'], 'integer'],
            [['id_servicio', 'id_taller'], 'required'],
            [['id_servicio'], 'exist', 'skipOnError' => true, 'targetClass' => Servicio::className(), 'targetAttribute' => ['id_servicio' => 'id']],
            [['id_taller'], 'exist', 'skipOnError' => true, 'targetClass' => Taller::className(), 'targetAttribute' => ['id_taller' => 'id_taller']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'costo_promedio' => 'Costo Promedio',
            'disponibilidad' => 'Disponibilidad',
            'cumplimiento' => 'Cumplimiento',
            'garantia' => 'Garantia',
            'id_servicio' => 'Id Servicio',
            'id_taller' => 'Id Taller',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCotizacionTallers()
    {
        return $this->hasMany(CotizacionTaller::className(), ['id_solicitud_servicio_taller' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdServicio()
    {
        return $this->hasOne(Servicio::className(), ['id' => 'id_servicio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTaller()
    {
        return $this->hasOne(Taller::className(), ['id_taller' => 'id_taller']);
    }
 
}
 
 
 