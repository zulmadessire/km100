<?php
namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;
use app\models\CotizacionTaller;
use yii\imagine\Image;  
use Imagine\Image\Box;

class UploadCotizacionTaller extends Model
{
    /**
     * @var UploadedFile[]
     */

    public $imageFiles,$imageFiles1,$imageFiles2,$imageFiles3,$imageFiles4;

    public $basePath = '/uploads/cotizacion-taller/';

    public function rules()
    {
        return [
            [['imageFiles'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, PNG, JPG, JPEG', 'maxFiles' => 1, 'maxSize' => 4194304],
            [['imageFiles1'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, PNG, JPG, JPEG', 'maxFiles' => 1, 'maxSize' => 4194304],
            [['imageFiles2'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, PNG, JPG, JPEG', 'maxFiles' => 1, 'maxSize' => 4194304],
            [['imageFiles3'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, PNG, JPG, JPEG', 'maxFiles' => 1, 'maxSize' => 4194304],
            [['imageFiles4'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, PNG, JPG, JPEG', 'maxFiles' => 1, 'maxSize' => 4194304],
            [['imageFiles',], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'imageFiles' => 'Imágen Placa',
            'imageFiles1' => 'Imágen Soporte',
            'imageFiles2' => 'Imágen Soporte',
            'imageFiles3' => 'Imágen Soporte',
            'imageFiles4' => 'Imágen Soporte',
        ];
    }

    public function upload_soporte_cotizacion_taller($id_cotizacion_taller)
    {

        foreach ($this->imageFiles as $i => $file) {
            $soporte = CotizacionTaller::findOne($id_cotizacion_taller);

            if ($soporte->imagen_soporte) {
                unlink('.'.$soporte->imagen_soporte);
            }
            /*$text = $file->baseName;
            $nom_img  = str_replace(' ', '', $text);*/
            $file->saveAs('.' . $this->basePath . $id_cotizacion_taller . '/cotizacion-placa'.$id_cotizacion_taller.'.'.$file->extension);

            Image::resize('.' . $this->basePath . $id_cotizacion_taller . '/cotizacion-placa'.$id_cotizacion_taller.'.'.$file->extension, 1200, 800)
                ->save('.' . $this->basePath . $id_cotizacion_taller . '/cotizacion-placa'.$id_cotizacion_taller.'.'.$file->extension, 
                        ['quality' => 70]);


            $soporte->imagen_soporte=$this->basePath . $id_cotizacion_taller . '/cotizacion-placa'.$id_cotizacion_taller.'.'.$file->extension;
            $soporte->save();
        }
        return true;
    }

     public function upload_soporte_cotizacion_taller1($id_cotizacion_taller)
    {
        foreach ($this->imageFiles1 as $i => $file) {
     

            $soporte = CotizacionTaller::findOne($id_cotizacion_taller);

            if ($soporte->imagen_soporte1) {
                unlink('.'.$soporte->imagen_soporte1);
            }

            $file->saveAs('.' . $this->basePath . $id_cotizacion_taller . '/cotizacion-soporte1'.$id_cotizacion_taller.'.'.$file->extension);

            Image::resize('.' . $this->basePath . $id_cotizacion_taller . '/cotizacion-soporte1'.$id_cotizacion_taller.'.'.$file->extension, 1200, 800)
                ->save('.' . $this->basePath . $id_cotizacion_taller . '/cotizacion-soporte1'.$id_cotizacion_taller.'.'.$file->extension, 
                        ['quality' => 70]);


            $soporte->imagen_soporte1 = $this->basePath . $id_cotizacion_taller . '/cotizacion-soporte1'.$id_cotizacion_taller.'.'.$file->extension;
            $soporte->save();
        }
        return true;
    }

     public function upload_soporte_cotizacion_taller2($id_cotizacion_taller)
    {
        foreach ($this->imageFiles2 as $i => $file) {
            $soporte = CotizacionTaller::findOne($id_cotizacion_taller);

            if ($soporte->imagen_soporte2) {
                unlink('.'.$soporte->imagen_soporte2);
            }
            $file->saveAs('.' . $this->basePath . $id_cotizacion_taller . '/cotizacion-soorte2'.$id_cotizacion_taller.'.'.$file->extension);

            Image::resize('.' . $this->basePath . $id_cotizacion_taller . '/cotizacion-soorte2'.$id_cotizacion_taller.'.'.$file->extension, 1200, 800)
                ->save('.' . $this->basePath . $id_cotizacion_taller . '/cotizacion-soorte2'.$id_cotizacion_taller.'.'.$file->extension, 
                        ['quality' => 70]);


            $soporte->imagen_soporte2 = $this->basePath . $id_cotizacion_taller . '/cotizacion-soorte2'.$id_cotizacion_taller.'.'.$file->extension;
            $soporte->save();

        }
        return true;
    }

     public function upload_soporte_cotizacion_taller3($id_cotizacion_taller)
    {
        foreach ($this->imageFiles3 as $i => $file) {
            $soporte = CotizacionTaller::findOne($id_cotizacion_taller);

            if ($soporte->imagen_soporte3) {
                unlink('.'.$soporte->imagen_soporte3);
            }
            
            $file->saveAs('.' . $this->basePath . $id_cotizacion_taller . '/cotizacion-soorte3'.$id_cotizacion_taller.'.'.$file->extension);

            Image::resize('.' . $this->basePath . $id_cotizacion_taller . '/cotizacion-soorte3'.$id_cotizacion_taller.'.'.$file->extension, 1200, 800)
                ->save('.' . $this->basePath . $id_cotizacion_taller . '/cotizacion-soorte3'.$id_cotizacion_taller.'.'.$file->extension, 
                        ['quality' => 70]);


            $soporte->imagen_soporte3 = $this->basePath . $id_cotizacion_taller . '/cotizacion-soorte3'.$id_cotizacion_taller.'.'.$file->extension;
            $soporte->save();
        }
        return true;
    }

     public function upload_soporte_cotizacion_taller4($id_cotizacion_taller)
    {
        foreach ($this->imageFiles4 as $i => $file) {
            $soporte = CotizacionTaller::findOne($id_cotizacion_taller);

            if ($soporte->imagen_soporte4) {
                unlink('.'.$soporte->imagen_soporte4);
            }

            $file->saveAs('.' . $this->basePath . $id_cotizacion_taller . '/cotizacion-soorte4'.$id_cotizacion_taller.'.'.$file->extension);

            Image::resize('.' . $this->basePath . $id_cotizacion_taller . '/cotizacion-soorte4'.$id_cotizacion_taller.'.'.$file->extension, 1200, 800)
                ->save('.' . $this->basePath . $id_cotizacion_taller . '/cotizacion-soorte4'.$id_cotizacion_taller.'.'.$file->extension, 
                        ['quality' => 70]);


            $soporte->imagen_soporte4 = $this->basePath . $id_cotizacion_taller . '/cotizacion-soorte4'.$id_cotizacion_taller.'.'.$file->extension;
            $soporte->save();
        }
        return true;
    }

}
