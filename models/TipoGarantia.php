<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipo_garantia".
 *
 * @property int $id
 * @property string $nombre
 *
 * @property Partes[] $partes
 */
class TipoGarantia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipo_garantia';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartes()
    {
        return $this->hasMany(Partes::className(), ['id_tipo_garantia' => 'id']);
    }
}
