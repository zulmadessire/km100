<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "licitacion".
 *
 * @property integer $id
 * @property string $fecha_inicio
 * @property string $fecha_culminacion
 * @property double $objetivo
 * @property string $garantia
 * @property integer $tiempo_entrega_s_d
 * @property integer $tiempo_entrega_i
 * @property string $dias
 * @property integer $id_rubro
 * @property integer $id_estado_lic
 *
 * @property Capacitaciones[] $capacitaciones
 * @property EstadoLicitacion $idEstadoLic
 * @property Pieza $idRubro
 * @property LicitacionSolicitud[] $licitacionSolicituds
 * @property ProveedorPiezaLic[] $proveedorPiezaLics
 */
class Licitacion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'licitacion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha_inicio', 'fecha_culminacion','fecha_hora'], 'safe'],
            [['objetivo'], 'number'],
            [['tiempo_entrega_s_d', 'tiempo_entrega_i', 'id_rubro', 'id_estado_lic','contador'], 'integer'],
            [['garantia', 'dias'], 'string', 'max' => 100],
            [['id_estado_lic'], 'exist', 'skipOnError' => true, 'targetClass' => EstadoLicitacion::className(), 'targetAttribute' => ['id_estado_lic' => 'id']],
            [['id_rubro'], 'exist', 'skipOnError' => true, 'targetClass' => Pieza::className(), 'targetAttribute' => ['id_rubro' => 'id_pieza']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha_inicio' => 'Fecha Inicio',
            'fecha_culminacion' => 'Fecha Culminacion',
            'objetivo' => 'Objetivo',
            'garantia' => 'Garantia',
            'tiempo_entrega_s_d' => 'Tiempo Entrega S D',
            'tiempo_entrega_i' => 'Tiempo Entrega I',
            'dias' => 'Dias',
            'id_rubro' => 'Id Rubro',
            'contador' => 'contador',
            'fecha_hora' => 'fecha_hora',
            'id_estado_lic' => 'Id Estado Lic',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCapacitaciones()
    {
        return $this->hasMany(Capacitaciones::className(), ['id_licitacion' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstadoLic()
    {
        return $this->hasOne(EstadoLicitacion::className(), ['id' => 'id_estado_lic']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRubro()
    {
        return $this->hasOne(Pieza::className(), ['id_pieza' => 'id_rubro']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLicitacionSolicituds()
    {
        return $this->hasMany(LicitacionSolicitud::className(), ['id_licitacion' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProveedorPiezaLics()
    {
        return $this->hasMany(ProveedorPiezaLic::className(), ['id_licitacion' => 'id']);
    }
}
