<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "precios".
 *
 * @property integer $id
 * @property string $modelo
 * @property string $marca
 * @property double $precio_mercado
 * @property double $porc_descuento
 * @property double $precio_f
 * @property integer $id_prec_prov
 *
 * @property PrecioProveedor $idPrecProv
 */
class Precios extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'precios';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['precio_mercado', 'porc_descuento', 'precio_f'], 'number'],
            [['id_prec_prov'], 'integer'],
            [['modelo', 'marca'], 'string', 'max' => 40],
            [['id_prec_prov'], 'exist', 'skipOnError' => true, 'targetClass' => PrecioProveedor::className(), 'targetAttribute' => ['id_prec_prov' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'modelo' => 'Modelo',
            'marca' => 'Marca',
            'precio_mercado' => 'Precio Mercado',
            'porc_descuento' => 'Porc Descuento',
            'precio_f' => 'Precio F',
            'id_prec_prov' => 'Id Prec Prov',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPrecProv()
    {
        return $this->hasOne(PrecioProveedor::className(), ['id' => 'id_prec_prov']);
    }
}
