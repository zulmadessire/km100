<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Flota;

/**
 * FlotaSearch represents the model behind the search form about `app\models\Flota`.
 */
class ResumenSearch extends Flota
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_estacion', 'id_vehiculo'], 'integer'],
            [['fecha_inicio', 'fecha_fin'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Flota::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fecha_inicio' => $this->fecha_inicio,
            'fecha_fin' => $this->fecha_fin,
            'id_estacion' => $this->id_estacion,
            'id_vehiculo' => $this->id_vehiculo,
        ]);

        return $dataProvider;
    }

     public function resumen($params)
    {
        $query = Flota::find()
                ->joinWith('idEstacion')
                ->where(['not', ['flota.fecha_inicio' => null]])
                ->andWhere(['not', ['flota.fecha_fin' => null]])
        
->groupBy(['estacion.id_empresa']);

 
 


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}
