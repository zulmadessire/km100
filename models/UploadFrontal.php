<?php
namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;
use app\models\Soporte;
use yii\imagine\Image;  
use Imagine\Image\Box;

class UploadFrontal extends Model
{
    /**
     * @var UploadedFile[]
     */

    public $imageFiles;

    const SCENARIO_CREAR = 'crear';
    const SCENARIO_MODIF = 'modif';

    public function scenarios()
    {
        return [
            self::SCENARIO_CREAR => ['imageFiles'],
            self::SCENARIO_MODIF => ['imageFiles'],
        ];
    }

    public function rules()
    {
        return [
            [['imageFiles'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, PNG', 'maxFiles' => 10, 'maxSize' => 4194304],
            ['imageFiles', 'required', 'on' => self::SCENARIO_CREAR, 'whenClient' => "function (tipo_accidente, value) {
                                                            return ($('#field_tipo_accidente').val() == 1 || $('#field_tipo_accidente').val() == 2 || $('#field_tipo_accidente').val() == 3 || $('#field_tipo_accidente').val() == 9 || $('#field_tipo_accidente').val() == 10 || $('#field_tipo_accidente').val() == 11 || $('#field_tipo_accidente').val() == 12 || $('#field_tipo_accidente').val() == 13);
                                                        }"],
        ];
    }

    public function attributeLabels()
    {
        return [
            'imageFiles' => 'Imágen Frontal',
        ];
    }


    public function upload($id_caso)
    {
        $basePath = '/uploads/casos/'.$id_caso;

        if ($this->validate()) {
            foreach ($this->imageFiles as $i => $file) {
                $sop_ante = Soporte::find()->where(['id_caso' => $id_caso,'bloque' => '0'])->One();
                if ($sop_ante) {
                    unlink('.'.$sop_ante->ubicacion);
                    $sop_ante->delete();
                }

                /*$text = $file->baseName;
                $nom_img  = str_replace(' ', '', $text);*/
                $file->saveAs('.' . $basePath . '/soporte-frontal-'.$id_caso.'.'.$file->extension);

                Image::resize('.' . $basePath . '/soporte-frontal-'.$id_caso.'.'.$file->extension, 1200, 800)
                ->save('.' . $basePath . '/soporte-frontal-'.$id_caso.'.'.$file->extension, 
                        ['quality' => 70]);

                $soporte = new Soporte();
                $soporte->ubicacion=$basePath . '/soporte-frontal-'.$id_caso.'.'.$file->extension;
                $soporte->bloque=0; // Frontal
                $soporte->id_caso=$id_caso;
                $soporte->save();
            }
            return true;
        } else {
            return false;
        }
    }
}
