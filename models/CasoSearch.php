<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Caso;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * CasoSearch represents the model behind the search form about `app\models\Caso`.
 */
class CasoSearch extends Caso
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_caso', 'tipo_caso', 'fuente_accidente', 'amet', 'num_contrato', 'tipo_cliente', 'cobertura_cdw', 'claim', 'deducible', 'registrado_por', 'id_estacion', 'id_modelo', 'id_tipo_accidente', 'id_ubicacion', 'req_cotizacion'], 'integer'],
            [['ficha', 'placa', 'nombre_empleado', 'fecha_acc_inc', 'descripcion', 'fecha_registro'], 'safe'],
            [['monto_deducible', 'tarifa_base'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Caso::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_caso' => $this->id_caso,
            'tipo_caso' => $this->tipo_caso,
            'fuente_accidente' => $this->fuente_accidente,
            'amet' => $this->amet,
            'num_contrato' => $this->num_contrato,
            'tipo_cliente' => $this->tipo_cliente,
            'cobertura_cdw' => $this->cobertura_cdw,
            'claim' => $this->claim,
            'deducible' => $this->deducible,
            'monto_deducible' => $this->monto_deducible,
            'tarifa_base' => $this->tarifa_base,
            'fecha_acc_inc' => $this->fecha_acc_inc,
            'registrado_por' => $this->registrado_por,
            'id_estacion' => $this->id_estacion,
            'id_modelo' => $this->id_modelo,
            'id_tipo_accidente' => $this->id_tipo_accidente,
            'id_ubicacion' => $this->id_ubicacion,
            'req_cotizacion' => $this->req_cotizacion,
            'fecha_registro' => $this->fecha_registro,
        ]);

        $query->andFilterWhere(['like', 'ficha', $this->ficha])
            ->andFilterWhere(['like', 'placa', $this->placa])
            ->andFilterWhere(['like', 'nombre_empleado', $this->nombre_empleado])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion]);

        return $dataProvider;
    }

     public function searchCasos($params)
    {
        if(\Yii::$app->user->can('admin') || \Yii::$app->user->can('casos todos')){ //Si es ADMIN o tiene permiso CASOS TODOS trae todos los casos
            $query = Caso::find()
                ->join('JOIN','estacion','caso.id_estacion = estacion.id_estacion')->orderBy('id_caso ASC');
        }else if(\Yii::$app->user->can('casos cliente')){ // CASO CLIENTE puede ver los casos asociados a LAS EMPRESAS con el mismo NOMBRE COMERCIAL de la suya
            $id_cliente = EstacionUsuario::find()->where(['user_id'=>Yii::$app->user->identity->ID ])->one()->estacion->idEmpresa->cliente->id;
            $query = Caso::find()
                ->join('JOIN','estacion','caso.id_estacion = estacion.id_estacion')
                ->join('JOIN','empresa','estacion.id_empresa = empresa.id_empresa')
                ->where('empresa.cliente_id=:id_cliente',[':id_cliente' => $id_cliente])
                ->orderBy(['caso.id_caso'=> SORT_ASC]);
        } else if(\Yii::$app->user->can('casos estaciones asociadas')){ // El permiso CASOS ESTACIONES ASOCIADAS ve solo los casos de las estaciones que tiene asociadas
            $estaciones_asociadas = ArrayHelper::getColumn(EstacionUsuario::find()->select('estacion_id')->where(['user_id'=>Yii::$app->user->identity->ID ])->asArray()->all(), 'estacion_id');
            $query = Caso::find()
                ->where(['id_estacion' => $estaciones_asociadas])
                ->orderBy(['id_caso'=> SORT_ASC]);
        } else if(\Yii::$app->user->can('casos propios')){ // el permiso CASOS PROPIOS solo ve los casos registrados por ellos mismos
            $query = Caso::find()
                ->join('JOIN','estacion','caso.id_estacion = estacion.id_estacion')
                ->where('caso.registrado_por=:sup',[':sup' => Yii::$app->user->identity->id])
                ->orderBy(['caso.id_caso'=> SORT_ASC]);
        }else{
            die('Error! No se encontró usuario en roles consultados. Comuniquese con el Administrados del Sistema...');
        }

        $query->orderBy('id_caso DESC');
        
        $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]);

        $this->load($params);
        if(!$this->validate()){
            return $dataProvider;
        }
   
        // grid filtering conditions
        $query->andFilterWhere([
            'caso.id_caso' => $this->id_caso,
            'caso.tipo_caso' => $this->tipo_caso,
            'caso.id_estacion' => $this->id_estacion,
            //'caso.fecha_registro' => $this->fecha_registro,
        ]);

        $query->andFilterWhere(['like', 'caso.ficha', $this->ficha])
            ->andFilterWhere(['like', 'caso.placa', $this->placa])
            ->andFilterWhere(['like', 'caso.descripcion', $this->descripcion])
            ->andFilterWhere(['like', 'caso.fecha_registro', $this->fecha_registro]);

        return $dataProvider;
    }

    public function searchCasosCobrosPendientes($params)
    {
        if(\Yii::$app->user->can('admin') || \Yii::$app->user->can('casos todos')){ //Si es ADMIN o tiene permiso CASOS TODOS trae todos los casos
            $query = Caso::find()
                ->join('JOIN','estacion','caso.id_estacion = estacion.id_estacion');
        }else if(\Yii::$app->user->can('casos cliente')){ // CASO CLIENTE puede ver los casos asociados a LAS EMPRESAS con el mismo NOMBRE COMERCIAL de la suya
            $id_cliente = EstacionUsuario::find()->where(['user_id'=>Yii::$app->user->identity->ID ])->one()->estacion->idEmpresa->cliente->id;
            $query = Caso::find()
                ->join('JOIN','estacion','caso.id_estacion = estacion.id_estacion')
                ->join('JOIN','empresa','estacion.id_empresa = empresa.id_empresa')
                ->where('empresa.cliente_id=:id_cliente',[':id_cliente' => $id_cliente])
                ->orderBy(['caso.id_caso'=> SORT_ASC]);
        } else if(\Yii::$app->user->can('casos estaciones asociadas')){ // El permiso CASOS ESTACIONES ASOCIADAS ve solo los casos de las estaciones que tiene asociadas
            $estaciones_asociadas = ArrayHelper::getColumn(EstacionUsuario::find()->select('estacion_id')->where(['user_id'=>Yii::$app->user->identity->ID ])->asArray()->all(), 'estacion_id');
            $query = Caso::find()
                ->where(['id_estacion' => $estaciones_asociadas])
                ->orderBy(['id_caso'=> SORT_ASC]);
        } else if(\Yii::$app->user->can('casos propios')){ // el permiso CASOS PROPIOS solo ve los casos registrados por ellos mismos
            $query = Caso::find()
                ->join('JOIN','estacion','caso.id_estacion = estacion.id_estacion')
                ->where('caso.registrado_por=:sup',[':sup' => Yii::$app->user->identity->id])
                ->orderBy(['caso.id_caso'=> SORT_ASC]);
        }else{
            die('Error! No se encontró usuario en roles consultados. Comuniquese con el Administrados del Sistema...');
        }
        //$query->joinWith(['idCobroCaso']);
        $query->leftJoin('cobro_caso', '`cobro_caso`.`id_caso` = `caso`.`id_caso`')->where(['cobro_caso.id_caso' => NULL]);
        $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]);

        $this->load($params);
        if(!$this->validate()){
            return $dataProvider;
        }
   
        // grid filtering conditions
        $query->andFilterWhere([
            'caso.id_caso' => $this->id_caso,
            'caso.tipo_caso' => $this->tipo_caso,
            'caso.id_estacion' => $this->id_estacion,
            //'caso.fecha_registro' => $this->fecha_registro,
        ]);

        $query->andFilterWhere(['like', 'caso.ficha', $this->ficha])
            ->andFilterWhere(['like', 'caso.placa', $this->placa])
            ->andFilterWhere(['like', 'caso.descripcion', $this->descripcion])
            ->andFilterWhere(['like', 'caso.fecha_registro', $this->fecha_registro]);

        //$query->andWhere(['not', ['cobro_caso.id_caso' => null]]);
        // var_dump($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);
        // exit();
        return $dataProvider;
    }
}
