<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SolicitudCompraEstacion;

/**
 * SolicitudCompraEstacionSearch represents the model behind the search form about `app\models\SolicitudCompraEstacion`.
 */
class SolicitudCompraEstacionSearch extends SolicitudCompraEstacion
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tipo_solicitud', 'id_estacion', 'id_modelo'], 'integer'],
            [['ficha', 'placa', 'fecha_registro'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SolicitudCompraEstacion::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tipo_solicitud' => $this->tipo_solicitud,
            'id_estacion' => $this->id_estacion,
            'id_modelo' => $this->id_modelo,
            'fecha_registro' => $this->fecha_registro,
        ]);

        $query->andFilterWhere(['like', 'ficha', $this->ficha])
            ->andFilterWhere(['like', 'placa', $this->placa]);

        return $dataProvider;
    }
}
