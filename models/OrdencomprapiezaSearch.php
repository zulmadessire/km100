<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\OrdenCompraPieza;

/**
 * OrdenCompraPiezaSearch represents the model behind the search form about `app\models\OrdenCompraPieza`.
 */
class OrdenCompraPiezaSearch extends OrdenCompraPieza
{
    /**
     * @inheritdoc
     */
    public $id_marca, $id_modelo, $chasis, $anho, $clase, $fecha_devolucion;
    public function rules()
    {
        return [
            [['id', 'estado', 'id_solicitud_compra_pieza', 'id_cotizacion_proveedor', 'id_proveedor', 'id_marca', 'id_modelo'], 'integer'],
            [['chasis'], 'string'],
            [['fecha', 'observaciones', 'chasis', 'fecha_devolucion'], 'safe'],
            [['total'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $query = OrdenCompraPieza::find()
        ->join('JOIN','solicitud_compra_pieza','solicitud_compra_pieza.id = orden_compra_pieza.id_solicitud_compra_pieza')
        ->where('solicitud_compra_pieza.origen_solicitud = 1')
        ->orWhere('solicitud_compra_pieza.origen_solicitud IS NULL');

        
        if ( isset( $params['OrdenCompraPiezaSearch']['id_proveedor'] ) AND $params['OrdenCompraPiezaSearch']['id_proveedor'] != '') {
            $query->andWhere( 'id_proveedor = '.$params['OrdenCompraPiezaSearch']['id_proveedor'] );
        }

        if ( isset( $params['OrdenCompraPiezaSearch']['id_marca'] ) AND $params['OrdenCompraPiezaSearch']['id_marca'] != '') {
            $query->andWhere( '(SELECT mo.id_marca FROM solicitud_compra_pieza scp 
            INNER JOIN servicio s ON s.id = scp.id_servicio 
            INNER JOIN vehiculo v ON s.id_vehiculo = v.id 
            INNER JOIN modelo mo ON mo.id_modelo = v.id_modelo  
            WHERE scp.id = orden_compra_pieza.id_solicitud_compra_pieza LIMIT 1) = '.$params['OrdenCompraPiezaSearch']['id_marca'] );
        }

        if ( isset( $params['OrdenCompraPiezaSearch']['id_modelo'] ) AND $params['OrdenCompraPiezaSearch']['id_modelo'] != '') {
            $query->andWhere( '(SELECT v.id_modelo FROM solicitud_compra_pieza scp 
            INNER JOIN servicio s ON s.id = scp.id_servicio 
            INNER JOIN vehiculo v ON s.id_vehiculo = v.id 
            WHERE scp.id = orden_compra_pieza.id_solicitud_compra_pieza LIMIT 1) = '.$params['OrdenCompraPiezaSearch']['id_modelo'] );
        }

        if ( isset( $params['OrdenCompraPiezaSearch']['chasis'] ) AND $params['OrdenCompraPiezaSearch']['chasis'] != '') {
            $query->andWhere( '(SELECT v.chasis FROM solicitud_compra_pieza scp 
            INNER JOIN servicio s ON s.id = scp.id_servicio 
            INNER JOIN vehiculo v ON s.id_vehiculo = v.id 
            WHERE scp.id = orden_compra_pieza.id_solicitud_compra_pieza LIMIT 1) like '."'%".strtoupper($params['OrdenCompraPiezaSearch']['chasis'])."%'" );
        }

        if ( isset( $params['OrdenCompraPiezaSearch']['anho'] ) AND $params['OrdenCompraPiezaSearch']['anho'] != '') {
            $query->andWhere( '(SELECT v.anho FROM solicitud_compra_pieza scp 
            INNER JOIN servicio s ON s.id = scp.id_servicio 
            INNER JOIN vehiculo v ON s.id_vehiculo = v.id 
            WHERE scp.id = orden_compra_pieza.id_solicitud_compra_pieza LIMIT 1) like '."'%".strtoupper($params['OrdenCompraPiezaSearch']['anho'])."%'" );
        }

        if ( isset( $params['OrdenCompraPiezaSearch']['clase'] ) AND $params['OrdenCompraPiezaSearch']['clase'] != '') {
            $query->andWhere( '(SELECT v.clase FROM solicitud_compra_pieza scp 
            INNER JOIN servicio s ON s.id = scp.id_servicio 
            INNER JOIN vehiculo v ON s.id_vehiculo = v.id 
            WHERE scp.id = orden_compra_pieza.id_solicitud_compra_pieza LIMIT 1) like '."'%".strtoupper($params['OrdenCompraPiezaSearch']['clase'])."%'" );
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'orden_compra_pieza.id' => $this->id,
            'fecha' => $this->fecha,
            'estado' => $this->estado,
            'total' => $this->total,
            'id_solicitud_compra_pieza' => $this->id_solicitud_compra_pieza,
            'id_cotizacion_proveedor' => $this->id_cotizacion_proveedor,
            'id_proveedor' => $this->id_proveedor,
        ]);

        $query->andFilterWhere(['like', 'observaciones', $this->observaciones]);

        return $dataProvider;
    }

    public function searchDevoluciones($params)
    {
      
        $query = OrdenCompraPieza::find();

        $query->joinWith('idCotizacionProveedor');
        $query->joinWith('idCotizacionProveedor.cotizacionProveedorPiezas');
        $query->andWhere('cotizacion_proveedor_pieza.cantidad_devolucion IS NOT NULL');

        if (!empty($params['OrdenCompraPiezaSearch']['fecha_devolucion'])) {
            $query->andWhere("cotizacion_proveedor_pieza.fecha_devolucion = '".date('Y-m-d', strtotime($params['OrdenCompraPiezaSearch']['fecha_devolucion']))."'"); //falta acomodar la fecha que se recibe
        }

        $query->groupBy('orden_compra_pieza.id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fecha' => $this->fecha,
            'estado' => $this->estado,
            'total' => $this->total,
            'id_solicitud_compra_pieza' => $this->id_solicitud_compra_pieza,
            'id_cotizacion_proveedor' => $this->id_cotizacion_proveedor,
            'id_proveedor' => $this->id_proveedor,
        ]);

        $query->andFilterWhere(['like', 'observaciones', $this->observaciones]);

        return $dataProvider;
    }

    public function searchEstacion($params)
    {
        $query = OrdenCompraPieza::find()
        ->join('JOIN','solicitud_compra_pieza','solicitud_compra_pieza.id = orden_compra_pieza.id_solicitud_compra_pieza')
        ->where('solicitud_compra_pieza.origen_solicitud = 2');

        if ( isset( $params['OrdenCompraPiezaSearch']['id_proveedor'] ) AND $params['OrdenCompraPiezaSearch']['id_proveedor'] != '') {
            $query->andWhere( 'id_proveedor = '.$params['OrdenCompraPiezaSearch']['id_proveedor'] );
        }

        if ( isset( $params['OrdenCompraPiezaSearch']['id_marca'] ) AND $params['OrdenCompraPiezaSearch']['id_marca'] != '') {
            $query->andWhere( '(SELECT mo.id_marca FROM solicitud_compra_pieza scp 
            INNER JOIN solicitud_compra_estacion s ON s.id = scp.id_solicitud_compra_estacion 
            INNER JOIN modelo mo ON s.id_modelo = mo.id_modelo  
            WHERE scp.id = orden_compra_pieza.id_solicitud_compra_pieza LIMIT 1) = '.$params['OrdenCompraPiezaSearch']['id_marca'] );
        }

        if ( isset( $params['OrdenCompraPiezaSearch']['id_modelo'] ) AND $params['OrdenCompraPiezaSearch']['id_modelo'] != '') {
            $query->andWhere( '(SELECT s.id_modelo FROM solicitud_compra_pieza scp 
            INNER JOIN solicitud_compra_estacion s ON s.id = scp.id_solicitud_compra_estacion 
            WHERE scp.id = orden_compra_pieza.id_solicitud_compra_pieza LIMIT 1) = '.$params['OrdenCompraPiezaSearch']['id_modelo'] );
        }

        if ( isset( $params['OrdenCompraPiezaSearch']['chasis'] ) AND $params['OrdenCompraPiezaSearch']['chasis'] != '') {
            $query->andWhere( '(SELECT v.chasis FROM solicitud_compra_pieza scp 
            INNER JOIN solicitud_compra_estacion s ON s.id = scp.id_solicitud_compra_estacion 
            INNER JOIN vehiculo v ON s.id_vehiculo = v.id 
            WHERE scp.id = orden_compra_pieza.id_solicitud_compra_pieza LIMIT 1) like '."'%".strtoupper($params['OrdenCompraPiezaSearch']['chasis'])."%'" );
        }

        if ( isset( $params['OrdenCompraPiezaSearch']['anho'] ) AND $params['OrdenCompraPiezaSearch']['anho'] != '') {
            $query->andWhere( '(SELECT v.anho FROM solicitud_compra_pieza scp 
            INNER JOIN solicitud_compra_estacion s ON s.id = scp.id_solicitud_compra_estacion 
            INNER JOIN vehiculo v ON s.id_vehiculo = v.id 
            WHERE scp.id = orden_compra_pieza.id_solicitud_compra_pieza LIMIT 1) like '."'%".strtoupper($params['OrdenCompraPiezaSearch']['anho'])."%'" );
        }

        if ( isset( $params['OrdenCompraPiezaSearch']['clase'] ) AND $params['OrdenCompraPiezaSearch']['clase'] != '') {
            $query->andWhere( '(SELECT v.clase FROM solicitud_compra_pieza scp 
            INNER JOIN solicitud_compra_estacion s ON s.id = scp.id_solicitud_compra_estacion 
            INNER JOIN vehiculo v ON s.id_vehiculo = v.id 
            WHERE scp.id = orden_compra_pieza.id_solicitud_compra_pieza LIMIT 1) like '."'%".strtoupper($params['OrdenCompraPiezaSearch']['clase'])."%'" );
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'orden_compra_pieza.id' => $this->id,
            'fecha' => $this->fecha,
            'estado' => $this->estado,
            'total' => $this->total,
            'id_solicitud_compra_pieza' => $this->id_solicitud_compra_pieza,
            'id_cotizacion_proveedor' => $this->id_cotizacion_proveedor,
            'id_proveedor' => $this->id_proveedor,
        ]);

        $query->andFilterWhere(['like', 'observaciones', $this->observaciones]);

        return $dataProvider;
    }
}
