<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notificaciones".
 *
 * @property integer $id
 * @property integer $tiempo
 * @property integer $tipo
 * @property integer $estado
 * @property integer $id_vehiculo
 * @property integer $id_taller
 *
 * @property Taller $idTaller
 * @property Vehiculo $idVehiculo
 */
class Notificaciones extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notificaciones';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tiempo', 'tipo', 'estado', 'id_vehiculo', 'id_taller'], 'integer'],
            [['id_taller'], 'exist', 'skipOnError' => true, 'targetClass' => Taller::className(), 'targetAttribute' => ['id_taller' => 'id_taller']],
            [['id_vehiculo'], 'exist', 'skipOnError' => true, 'targetClass' => Vehiculo::className(), 'targetAttribute' => ['id_vehiculo' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tiempo' => 'Tiempo',
            'tipo' => 'Tipo',
            'estado' => 'Estado',
            'id_vehiculo' => 'Id Vehiculo',
            'id_taller' => 'Id Taller',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTaller()
    {
        return $this->hasOne(Taller::className(), ['id_taller' => 'id_taller']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdVehiculo()
    {
        return $this->hasOne(Vehiculo::className(), ['id' => 'id_vehiculo']);
    }
}
