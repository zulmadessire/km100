<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cotizacion_taller_pieza".
 *
 * @property integer $id
 * @property integer $cantidad
 * @property integer $id_cotizacion_taller
 * @property integer $id_pieza
 *
 * @property CotizacionTaller $idCotizacionTaller
 * @property Pieza $idPieza
 */
class CotizacionTallerPieza extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $tipo,$posicion, $imageFiles;
    public static function tableName()
    {
        return 'cotizacion_taller_pieza';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['cantidad', 'id_cotizacion_taller', 'id_pieza'], 'required'],
            [['cantidad', 'id_cotizacion_taller', 'id_pieza', 'tipo', 'posicion'], 'integer'],
            //[['cotizacion'], 'integer'],
            [['observacion_pieza', 'imagen_pieza'], 'string'],
            [['precio'], 'number'],
            [['id_cotizacion_taller'], 'exist', 'skipOnError' => true, 'targetClass' => CotizacionTaller::className(), 'targetAttribute' => ['id_cotizacion_taller' => 'id']],
            [['id_pieza'], 'exist', 'skipOnError' => true, 'targetClass' => Pieza::className(), 'targetAttribute' => ['id_pieza' => 'id_pieza']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cantidad' => 'Cantidad',
            'id_cotizacion_taller' => 'Id Cotizacion Taller',
            'id_pieza' => 'Pieza',
            'cotizacion' => 'cotizacion',
            'precio' => 'precio',
            'serial' => 'serial',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCotizacionTaller()
    {
        return $this->hasOne(CotizacionTaller::className(), ['id' => 'id_cotizacion_taller']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPieza()
    {
        return $this->hasOne(Pieza::className(), ['id_pieza' => 'id_pieza']);
    }
}
