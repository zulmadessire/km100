<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SolicitudServicioTaller;

/**
 * SolicitudServicioTallerSearch represents the model behind the search form about `app\models\SolicitudServicioTaller`.
 */
class SolicitudServicioTallerSearch extends SolicitudServicioTaller
{
    /**
     * @inheritdoc
     */
    public $id_estacion, $fecha;
    public function rules()
    {
        return [
            [['id', 'disponibilidad', 'garantia', 'id_servicio', 'id_taller', 'id_estacion'], 'integer'],
            [['costo_promedio', 'cumplimiento'], 'number'],
            [['id_estacion', 'fecha'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SolicitudServicioTaller::find();

        // add conditions that should always apply here
        //Estacion
        if ( isset( $params['ServicioSearch']['id_estacion'] ) AND $params['ServicioSearch']['id_estacion'] != '') {
            $query->andWhere( '(SELECT es.id_estacion FROM servicio se 
            INNER JOIN solicitud_servicio_taller st ON st.id_servicio = se.id 
            INNER JOIN estacion es ON se.id_estacion_actual = es.id_estacion  
            WHERE se.id = solicitud_servicio_taller.id_servicio LIMIT 1) = '.$params['ServicioSearch']['id_estacion'] );
        }
        //Empresa
        if ( isset( $params['ServicioSearch']['id_company'] ) AND $params['ServicioSearch']['id_company'] != '') {
            $query->andWhere( '(SELECT em.id_empresa FROM servicio se 
            INNER JOIN solicitud_servicio_taller st ON st.id_servicio = se.id 
            INNER JOIN estacion es ON se.id_estacion_actual = es.id_estacion  
            INNER JOIN empresa em ON es.id_empresa = em.id_empresa
            WHERE se.id = solicitud_servicio_taller.id_servicio LIMIT 1) = '.$params['ServicioSearch']['id_company'] );
        }
        //Estatus
        if ( isset( $params['ServicioSearch']['id_estatus'] ) AND $params['ServicioSearch']['id_estatus'] != '') {              
            $query->andWhere( '(SELECT ts.id FROM servicio se 
            INNER JOIN pipeline_servicio ps ON ps.id_servicio = se.id
            INNER JOIN estado_servicio ts ON ps.id_estado_servicio = ts.id  
            WHERE se.id = solicitud_servicio_taller.id_servicio ORDER BY ps.id DESC LIMIT 1) = '.$params['ServicioSearch']['id_estatus'] );  
        }
        //Tipo Solicitud
        if ( isset( $params['ServicioSearch']['id_servicio'] ) AND $params['ServicioSearch']['id_servicio'] != '') {              
            $query->andWhere( '(SELECT se.servicio FROM servicio se 
            WHERE se.id = solicitud_servicio_taller.id_servicio
            LIMIT 1) like '."'%".$params['ServicioSearch']['id_servicio']."%'" ); 
        }

        //Fecha
        if (!empty($params['SolicitudServicioTallerSearch']['fecha'])) {
            $query->andWhere( "(SELECT s.fecha_envio_estacion FROM servicio s 
            WHERE s.id = solicitud_servicio_taller.id_servicio
            LIMIT 1) = '".date('Y-m-d', strtotime($params['SolicitudServicioTallerSearch']['fecha']))."'" );
        }

        $query->andWhere( "(SELECT s.aprobacion_estacion FROM servicio s 
            WHERE s.id = solicitud_servicio_taller.id_servicio
            LIMIT 1) = 1 ");


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'costo_promedio' => $this->costo_promedio,
            'disponibilidad' => $this->disponibilidad,
            'cumplimiento' => $this->cumplimiento,
            'garantia' => $this->garantia,
            'id_servicio' => $this->id_servicio,
            'id_taller' => $this->id_taller,
        ]);

        //var_dump($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);
        //exit();

        return $dataProvider;
    }

    public function searchT($params,$analista)
    {
        $query = SolicitudServicioTaller::find();
        
                // ->join('JOIN','cotizacion_taller','cotizacion_taller.id_solicitud_servicio_taller = Solicitud_Servicio_Taller.id')
                // ->join('JOIN','cotizacion_taller_analista','cotizacion_taller_analista.id_cotizacion_taller = cotizacion_taller.id')
                // ->where(['cotizacion_taller_analista.id_analista' => $analista]);

                // ->join('JOIN','servicio','solicitud_servicio_taller.id_servicio = servicio.id')
                // ->join('JOIN','checklist_avaluo','checklist_avaluo.id_servicio = servicio.id')
                // ->where(['checklist_avaluo.id_analista' => $analista]);
         

        // add conditions that should always apply here
        //Estacion
        if ( isset( $params['ServicioSearch']['id_estacion'] ) AND $params['ServicioSearch']['id_estacion'] != '') {
            $query->andWhere( '(SELECT es.id_estacion FROM servicio se 
            INNER JOIN solicitud_servicio_taller st ON st.id_servicio = se.id 
            INNER JOIN estacion es ON se.id_estacion_actual = es.id_estacion  
            WHERE se.id = solicitud_servicio_taller.id_servicio LIMIT 1) = '.$params['ServicioSearch']['id_estacion'] );
        }
        //Empresa
        if ( isset( $params['ServicioSearch']['id_company'] ) AND $params['ServicioSearch']['id_company'] != '') {
            $query->andWhere( '(SELECT em.id_empresa FROM servicio se 
            INNER JOIN solicitud_servicio_taller st ON st.id_servicio = se.id 
            INNER JOIN estacion es ON se.id_estacion_actual = es.id_estacion  
            INNER JOIN empresa em ON es.id_empresa = em.id_empresa
            WHERE se.id = solicitud_servicio_taller.id_servicio LIMIT 1) = '.$params['ServicioSearch']['id_company'] );
        }
        //Estatus
        if ( isset( $params['ServicioSearch']['id_estatus'] ) AND $params['ServicioSearch']['id_estatus'] != '') {              
            $query->andWhere( '(SELECT ts.id FROM servicio se 
            INNER JOIN pipeline_servicio ps ON ps.id_servicio = se.id
            INNER JOIN estado_servicio ts ON ps.id_estado_servicio = ts.id  
            WHERE se.id = solicitud_servicio_taller.id_servicio ORDER BY ps.id DESC LIMIT 1) = '.$params['ServicioSearch']['id_estatus'] );  
        }
        //Tipo Solicitud
        if ( isset( $params['ServicioSearch']['id_servicio'] ) AND $params['ServicioSearch']['id_servicio'] != '') {              
            $query->andWhere( '(SELECT se.servicio FROM servicio se 
            WHERE se.id = solicitud_servicio_taller.id_servicio
            LIMIT 1) like '."'%".$params['ServicioSearch']['id_servicio']."%'" ); 
        }
        //Nivel
        if ( isset( $params['ServicioSearch']['nivel'] ) AND $params['ServicioSearch']['nivel'] != '') {              
            $query->andWhere( '(SELECT c.activo FROM cotizacion_taller c
            WHERE solicitud_servicio_taller.id = c.id_solicitud_servicio_taller LIMIT 1) = '.$params['ServicioSearch']['nivel'] );
        }
        
        $query->andWhere( '(SELECT ts.id FROM servicio se 
        INNER JOIN pipeline_servicio ps ON ps.id_servicio = se.id
        INNER JOIN estado_servicio ts ON ps.id_estado_servicio = ts.id  
        WHERE se.id = solicitud_servicio_taller.id_servicio ORDER BY ps.id DESC LIMIT 1) = 3');  

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'costo_promedio' => $this->costo_promedio,
            'disponibilidad' => $this->disponibilidad,
            'cumplimiento' => $this->cumplimiento,
            'garantia' => $this->garantia,
            'id_servicio' => $this->id_servicio,
            'id_taller' => $this->id_taller,
        ]);

        //var_dump($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);
        //exit();

        return $dataProvider;
    }
}

