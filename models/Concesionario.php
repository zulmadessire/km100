<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "concesionario".
 *
 * @property integer $id
 * @property string $nombre
 *
 * @property DetalleGarantia[] $detalleGarantias
 * @property SucursalConcesionario[] $sucursalConcesionarios
 * @property Vehiculo[] $vehiculos
 */
class Concesionario extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'concesionario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetalleGarantias()
    {
        return $this->hasMany(DetalleGarantia::className(), ['id_concesionario' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSucursalConcesionarios()
    {
        return $this->hasMany(SucursalConcesionario::className(), ['concesionario_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehiculos()
    {
        return $this->hasMany(Vehiculo::className(), ['id_concesionario' => 'id']);
    }
}
