<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Auditoriafs;
use app\models\Estacion;
use yii\db\Query;


/**
 * AuditoriafsSearch represents the model behind the search form about `app\models\Auditoriafs`.
 */
class AuditoriafsSearch extends Auditoriafs
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'estado', 'desviacion', 'veh_fuera_ser_est', 'id_estacion'], 'integer'],
            [['fecha', 'motivo', 'plan_accion', 'observacion','comentario'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Auditoriafs::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fecha' => $this->fecha,
            'estado' => $this->estado,
            'desviacion' => $this->desviacion,
            'veh_fuera_ser_est' => $this->veh_fuera_ser_est,
            'id_estacion' => $this->id_estacion,
        ]);

        $query->andFilterWhere(['like', 'motivo', $this->motivo])
            ->andFilterWhere(['like', 'plan_accion', $this->plan_accion])
            ->andFilterWhere(['like', 'observacion', $this->observacion]);

        return $dataProvider;
    }


         public function searchmensual($params)
    {
 
        //  $query = Auditoria::find()
        // ->where('MONTH(fecha)=MONTH(CURDATE()) AND estado=0');
 


        $query2 = new query();
                $query2->select('es.id_estacion')
                    ->from('estacion es')
                    ->join('INNER JOIN','auditoria_fs aud_fs','es.id_estacion = aud_fs.id_estacion')
                    ->where( 'MONTH(aud_fs.fecha)=MONTH(CURDATE())')
                    ->andwhere( 'YEAR(aud_fs.fecha)=YEAR(CURDATE())')
                    ->andwhere( 'aud_fs.estado=0');

        $sql = $query2->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
        $agencia = Yii::$app->db->createCommand($sql)->queryOne();
        $array[]="";
       
        foreach ($agencia as $value) {
             $array = $value;
        }

        $query=Estacion::find()
           ->Where(['NOT IN','id_estacion', $array]);
 
    $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fecha' => $this->fecha,
            'estado' => $this->estado,
            'desviacion' => $this->desviacion,
            'veh_fuera_ser_est' => $this->veh_fuera_ser_est,
            'id_estacion' => $this->id_estacion,
        ]);

        $query->andFilterWhere(['like', 'motivo', $this->motivo])
            ->andFilterWhere(['like', 'plan_accion', $this->plan_accion])
            ->andFilterWhere(['like', 'observacion', $this->observacion]);

        return $dataProvider;
    }

}
