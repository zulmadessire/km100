<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "servicio_vehiculo_escaneado".
 *
 * @property integer $id
 * @property string $ficha
 * @property string $modelo
 * @property string $empresa
 * @property string $tipo_solicitud
 * @property integer $id_servicio
 */
class ServicioVehiculoEscaneado extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'servicio_vehiculo_escaneado';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ficha', 'modelo', 'empresa', 'tipo_solicitud', 'id_servicio'], 'required'],
            [['id_servicio'], 'integer'],
            [['ficha', 'modelo', 'empresa'], 'string', 'max' => 45],
            [['tipo_solicitud'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ficha' => 'Ficha',
            'modelo' => 'Modelo',
            'empresa' => 'Empresa',
            'tipo_solicitud' => 'Tipo Solicitud',
            'id_servicio' => 'Id Servicio',
        ];
    }
}
