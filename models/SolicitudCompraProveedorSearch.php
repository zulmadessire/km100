<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SolicitudCompraProveedor;

/**
 * SolicitudCompraProveedorSearch represents the model behind the search form about `app\models\SolicitudCompraProveedor`.
 */
class SolicitudCompraProveedorSearch extends SolicitudCompraProveedor
{
    /**
     * @inheritdoc
     */
    public $id_marca, $id_modelo, $anho, $clase, $chasis;
    public function rules()
    {
        return [
            [['id', 'id_solicitud_compra_pieza', 'id_proveedor', 'id_marca', 'id_modelo'], 'integer'],
            [['chasis'], 'string'],
            [['id_marca', 'id_modelo', 'chasis'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SolicitudCompraProveedor::find()
        ->join('JOIN', 'cotizacion_proveedor', 'cotizacion_proveedor.id_solicitud_compra_proveedor = solicitud_compra_proveedor.id')
        ->join('JOIN', 'solicitud_compra_pieza', 'solicitud_compra_pieza.id = solicitud_compra_proveedor.id_solicitud_compra_pieza')
        ->where('cotizacion_proveedor.estado = 0')
        ->orWhere('cotizacion_proveedor.estado = 1')
        ->orWhere('cotizacion_proveedor.estado = 3')
        ->andWhere('solicitud_compra_pieza.origen_solicitud is null')
        ->orWhere('solicitud_compra_pieza.origen_solicitud = 1')        
        // ->join('JOIN','solicitud_compra_pieza', 'solicitud_compra_pieza.id = solicitud_compra_proveedor.id_solicitud_compra_pieza')
        // ->where('solicitud_compra_pieza.aprobacion is NULL');
        ->orderBy('id desc'); 


        // add conditions that should always apply here

        //Marca
        if ( isset( $params['SolicitudCompraProveedorSearch']['id_marca'] ) AND $params['SolicitudCompraProveedorSearch']['id_marca'] != '') {
            $query->andWhere( '(SELECT mo.id_marca FROM solicitud_compra_pieza scp INNER JOIN servicio s ON s.id = scp.id_servicio INNER JOIN vehiculo v ON s.id_vehiculo = v.id INNER JOIN modelo mo ON mo.id_modelo = v.id_modelo WHERE scp.id = solicitud_compra_proveedor.id_solicitud_compra_pieza LIMIT 1) = '.$params['SolicitudCompraProveedorSearch']['id_marca'] );
        }

        //Modelo
        if ( isset( $params['SolicitudCompraProveedorSearch']['id_modelo'] ) AND $params['SolicitudCompraProveedorSearch']['id_modelo'] != '') {
            $query->andWhere( '(SELECT v.id_modelo FROM solicitud_compra_pieza scp INNER JOIN servicio s ON s.id = scp.id_servicio INNER JOIN vehiculo v ON s.id_vehiculo = v.id WHERE scp.id = solicitud_compra_proveedor.id_solicitud_compra_pieza LIMIT 1) = '.$params['SolicitudCompraProveedorSearch']['id_modelo'] );
        }

        //Chasis
        if ( isset( $params['SolicitudCompraProveedorSearch']['chasis'] ) AND $params['SolicitudCompraProveedorSearch']['chasis'] != '') {
            $query->andWhere( '(SELECT v.chasis FROM solicitud_compra_pieza scp INNER JOIN servicio s ON s.id = scp.id_servicio INNER JOIN vehiculo v ON s.id_vehiculo = v.id WHERE scp.id = solicitud_compra_proveedor.id_solicitud_compra_pieza LIMIT 1) like '."'%".strtoupper($params['SolicitudCompraProveedorSearch']['chasis'])."%'" );
        }

        //Anho
        if ( isset( $params['SolicitudCompraProveedorSearch']['anho'] ) AND $params['SolicitudCompraProveedorSearch']['anho'] != '') {
            $query->andWhere( '(SELECT v.anho FROM solicitud_compra_pieza scp 
            INNER JOIN servicio s ON s.id = scp.id_servicio 
            INNER JOIN vehiculo v ON s.id_vehiculo = v.id 
            WHERE scp.id = solicitud_compra_proveedor.id_solicitud_compra_pieza LIMIT 1) like '."'%".strtoupper($params['SolicitudCompraProveedorSearch']['anho'])."%'" );
        }

        //clase
        if ( isset( $params['SolicitudCompraProveedorSearch']['clase'] ) AND $params['SolicitudCompraProveedorSearch']['clase'] != '') {
            $query->andWhere( '(SELECT v.clase FROM solicitud_compra_pieza scp 
            INNER JOIN servicio s ON s.id = scp.id_servicio 
            INNER JOIN vehiculo v ON s.id_vehiculo = v.id 
            WHERE scp.id = solicitud_compra_proveedor.id_solicitud_compra_pieza LIMIT 1) like '."'%".strtoupper($params['SolicitudCompraProveedorSearch']['clase'])."%'" );
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_solicitud_compra_pieza' => $this->id_solicitud_compra_pieza,
            'id_proveedor' => $this->id_proveedor,
        ]);
        // var_dump($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);
        // exit();
        return $dataProvider;
    }

    public function searchEstacion($params)
    {
        $query = SolicitudCompraProveedor::find()
        ->join('JOIN', 'cotizacion_proveedor', 'cotizacion_proveedor.id_solicitud_compra_proveedor = solicitud_compra_proveedor.id')
        ->join('JOIN', 'solicitud_compra_pieza', 'solicitud_compra_pieza.id = solicitud_compra_proveedor.id_solicitud_compra_pieza')
        ->where('cotizacion_proveedor.estado = 0')
        ->orWhere('cotizacion_proveedor.estado = 1')
        ->orWhere('cotizacion_proveedor.estado = 3')
        ->andWhere('solicitud_compra_pieza.origen_solicitud = 2')        
        // ->join('JOIN','solicitud_compra_pieza', 'solicitud_compra_pieza.id = solicitud_compra_proveedor.id_solicitud_compra_pieza')
        // ->where('solicitud_compra_pieza.aprobacion is NULL');
        ->orderBy('id desc'); 


        // add conditions that should always apply here

        //Marca
        if ( isset( $params['SolicitudCompraProveedorSearch']['id_marca'] ) AND $params['SolicitudCompraProveedorSearch']['id_marca'] != '') {
            $query->andWhere( '(SELECT mo.id_marca FROM solicitud_compra_pieza scp 
            INNER JOIN solicitud_compra_estacion s ON s.id = scp.id_solicitud_compra_estacion 
            INNER JOIN modelo mo ON mo.id_modelo = s.id_modelo 
            WHERE scp.id = solicitud_compra_proveedor.id_solicitud_compra_pieza LIMIT 1) = '.$params['SolicitudCompraProveedorSearch']['id_marca'] );
        }

        //Modelo
        if ( isset( $params['SolicitudCompraProveedorSearch']['id_modelo'] ) AND $params['SolicitudCompraProveedorSearch']['id_modelo'] != '') {
            $query->andWhere( '(SELECT s.id_modelo FROM solicitud_compra_pieza scp 
            INNER JOIN solicitud_compra_estacion s ON s.id = scp.id_solicitud_compra_estacion 
            WHERE scp.id = solicitud_compra_proveedor.id_solicitud_compra_pieza LIMIT 1) = '.$params['SolicitudCompraProveedorSearch']['id_modelo'] );
        }

        //Chasis
        if ( isset( $params['SolicitudCompraProveedorSearch']['chasis'] ) AND $params['SolicitudCompraProveedorSearch']['chasis'] != '') {
            $query->andWhere( '(SELECT v.chasis FROM solicitud_compra_pieza scp 
            INNER JOIN solicitud_compra_estacion s ON s.id = scp.id_solicitud_compra_estacion 
            INNER JOIN vehiculo v ON s.id_vehiculo = v.id 
            WHERE scp.id = solicitud_compra_proveedor.id_solicitud_compra_pieza LIMIT 1) like '."'%".strtoupper($params['SolicitudCompraProveedorSearch']['chasis'])."%'" );
        }


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_solicitud_compra_pieza' => $this->id_solicitud_compra_pieza,
            'id_proveedor' => $this->id_proveedor,
        ]);
        // var_dump($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);
        // exit();
        return $dataProvider;
    }
}
