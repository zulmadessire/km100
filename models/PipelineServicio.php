<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pipeline_servicio".
 *
 * @property integer $id
 * @property string $fecha
 * @property integer $id_servicio
 * @property integer $id_estado_servicio
 *
 * @property EstadoServicio $idEstadoServicio
 * @property Servicio $idServicio
 */
class PipelineServicio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pipeline_servicio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha', 'id_servicio', 'id_estado_servicio'], 'required'],
            [['fecha'], 'safe'],
            [['id_servicio', 'id_estado_servicio'], 'integer'],
            [['id_estado_servicio'], 'exist', 'skipOnError' => true, 'targetClass' => EstadoServicio::className(), 'targetAttribute' => ['id_estado_servicio' => 'id']],
            [['id_servicio'], 'exist', 'skipOnError' => true, 'targetClass' => Servicio::className(), 'targetAttribute' => ['id_servicio' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha' => 'Fecha',
            'id_servicio' => 'Id Servicio',
            'id_estado_servicio' => 'Id Estado Servicio',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstadoServicio()
    {
        return $this->hasOne(EstadoServicio::className(), ['id' => 'id_estado_servicio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdServicio()
    {
        return $this->hasOne(Servicio::className(), ['id' => 'id_servicio']);
    }
}
