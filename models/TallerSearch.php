<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Taller;

/**
 * TallerSearch represents the model behind the search form of `app\models\Taller`.
 */
class TallerSearch extends Taller
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_taller', 'tipo_persona', 'tipo_pago', 'espacios', 'nivel_servicio', 'garantia_global', 'tipo_taller', 'tipo_servicio_taller'], 'integer'],
            [['rnc', 'nombre', 'telefono', 'direccion', 'coord_google_maps', 'nombre_representante', 'identif_representante', 'email_representante', 'celular_representante', 'acuerdo_comercial', 'user', 'dias', 'tlf1', 'tlf2', 'registro_mercantil', 'vencimiento_rm', 'vencimiento_acuerdo', 'email_taller'], 'safe'],
            [['descuento', 'limite_credito', 'precio_pieza', 'precio_pieza_mecanica'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Taller::find();

        // add conditions that should always apply here

        if ( isset( $params['TallerSearch']['id_marca'] ) AND $params['TallerSearch']['id_marca'] != '') {
            $query->andWhere( '(SELECT m.id_marca FROM vehiculo_taller vt INNER JOIN modelo m ON vt.id_modelo = m.id_modelo AND m.id_marca = '.$params['TallerSearch']['id_marca'].' WHERE vt.id_taller = taller.id_taller LIMIT 1) = '.$params['TallerSearch']['id_marca'] );
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_taller' => $this->id_taller,
            'tipo_persona' => $this->tipo_persona,
            'tipo_pago' => $this->tipo_pago,
            'descuento' => $this->descuento,
            'limite_credito' => $this->limite_credito,
            'espacios' => $this->espacios,
            'nivel_servicio' => $this->nivel_servicio,
            'vencimiento_rm' => $this->vencimiento_rm,
            'vencimiento_acuerdo' => $this->vencimiento_acuerdo,
            'precio_pieza' => $this->precio_pieza,
            'garantia_global' => $this->garantia_global,
            'tipo_taller' => $this->tipo_taller,
            'tipo_servicio_taller' => $this->tipo_servicio_taller,
            'precio_pieza_mecanica' => $this->precio_pieza_mecanica,
        ]);

        $query->andFilterWhere(['like', 'rnc', $this->rnc])
            ->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'telefono', $this->telefono])
            ->andFilterWhere(['like', 'direccion', $this->direccion])
            ->andFilterWhere(['like', 'coord_google_maps', $this->coord_google_maps])
            ->andFilterWhere(['like', 'nombre_representante', $this->nombre_representante])
            ->andFilterWhere(['like', 'identif_representante', $this->identif_representante])
            ->andFilterWhere(['like', 'email_representante', $this->email_representante])
            ->andFilterWhere(['like', 'celular_representante', $this->celular_representante])
            ->andFilterWhere(['like', 'acuerdo_comercial', $this->acuerdo_comercial])
            ->andFilterWhere(['like', 'user', $this->user])
            ->andFilterWhere(['like', 'dias', $this->dias])
            ->andFilterWhere(['like', 'tlf1', $this->tlf1])
            ->andFilterWhere(['like', 'tlf2', $this->tlf2])
            ->andFilterWhere(['like', 'registro_mercantil', $this->registro_mercantil])
            ->andFilterWhere(['like', 'email_taller', $this->email_taller]);

        return $dataProvider;
    }
}
