<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CobroCaso;

/**
 * CobroCasoSerach represents the model behind the search form about `app\models\CobroCaso`.
 */
class CobroCasoSerach extends CobroCaso
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'forma_pago', 'motivo_cobro', 'id_caso'], 'integer'],
            [['monto'], 'number'],
            [['fecha', 'numero_factura'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CobroCaso::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'monto' => $this->monto,
            'forma_pago' => $this->forma_pago,
            'motivo_cobro' => $this->motivo_cobro,
            'fecha' => $this->fecha,
            'id_caso' => $this->id_caso,
        ]);

        $query->andFilterWhere(['like', 'numero_factura', $this->numero_factura]);

        return $dataProvider;
    }
}
