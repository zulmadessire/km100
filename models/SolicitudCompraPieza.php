<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "solicitud_compra_pieza".
 *
 * @property integer $id
 * @property string $fecha
 * @property integer $codigo_estado
 * @property integer $id_servicio
 * @property integer $id_cotizacion_taller
 * @property integer $id_orden_trabajo
 *
 * @property PipelineCompraPieza[] $pipelineCompraPiezas
 * @property OrdenTrabajo $idOrdenTrabajo
 * @property SolicitudCompraProveedor[] $solicitudCompraProveedors
 */
class SolicitudCompraPieza extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'solicitud_compra_pieza';
    }

    /**
     * @inheritdoc
     */
    //$mlp = monto limite de pieza
    public $estado, $mlp;
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['codigo_estado', 'id_servicio', 'id_cotizacion_taller', 'id_orden_trabajo', 'estado', 'id_caso_estacion'], 'integer'],
            //[['id_orden_trabajo'], 'required'],
            [['mlp'], 'number'],
 
            [['observacion', 'observacion_recepcion', 'observacion_gerente', 'motivo_rechazo'], 'string'],
 
            [['id_orden_trabajo'], 'exist', 'skipOnError' => true, 'targetClass' => OrdenTrabajo::className(), 'targetAttribute' => ['id_orden_trabajo' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha' => 'Fecha',
            'codigo_estado' => 'Codigo Estado',
            'id_servicio' => 'Id Servicio',
            'id_cotizacion_taller' => 'Id Cotizacion Taller',
            'id_orden_trabajo' => 'Id Orden Trabajo',
            'observacion' => 'Observacion',
            'aprobacion' => 'Aprobacion',
            'observacion_recepcion' => 'Observacion Recepcion',
            'dias' => 'Días en Gestión',
            'observacion_gerente' => 'Observaciones',
            'motivo_rechazo' => 'Motivo de rechazo'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPipelineCompraPiezas()
    {
        return $this->hasMany(PipelineCompraPieza::className(), ['id_solicitud_compra_pieza' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdOrdenTrabajo()
    {
        return $this->hasOne(OrdenTrabajo::className(), ['id' => 'id_orden_trabajo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSolicitudCompraProveedors()
    {
        return $this->hasMany(SolicitudCompraProveedor::className(), ['id_solicitud_compra_pieza' => 'id']);
    }

    public function getDias() {

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("
            (SELECT DATEDIFF(NOW(), s.fecha) as dias FROM solicitud_compra_pieza s WHERE s.id = ".$this->id.")");

        $result = $command->queryOne();

        return $result['dias'].' DÍAS';
    }
}
