<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "taller".
 *
 * @property integer $id_taller
 * @property integer $tipo_persona
 * @property string $rnc
 * @property string $nombre
 * @property string $telefono
 * @property string $direccion
 * @property string $coord_google_maps
 * @property integer $tipo_pago
 * @property double $descuento
 * @property double limite_credito
 * @property string $nombre_representante
 * @property string $identif_representante
 * @property string $email_representante
 * @property string $celular_representante
 * @property integer $espacios
 * @property integer $nivel_servicio
 * @property string $acuerdo_comercial
 * @property integer $tipo_taller
 * @property int $tipo_servicio_taller 0-Des. y Pint., 1-Mecánica Line, 2-Des. y Pint / Mecánica
 * @property double $precio_pieza_mecanica
 *
 * @property ServicioTaller[] $servicioTallers
 * @property VehiculoTaller[] $vehiculoTallers
 */
class Taller extends \yii\db\ActiveRecord
{
    public $precio_temp;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'taller';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipo_persona', 'tipo_taller', 'rnc', 'nombre', 'email_taller', 'telefono', 'direccion', 'coord_google_maps', 'tipo_pago', 'espacios','user', 'tipo_servicio_taller'], 'required'],
            [['tipo_persona', 'tipo_taller', 'tipo_pago', 'espacios', 'garantia_global', 'nivel_servicio', 'tipo_servicio_taller'], 'integer'],
            [['descuento', 'limite_credito', 'precio_pieza', 'precio_pieza_mecanica'], 'number'],
            [['rnc', 'nombre', 'nombre_representante','dias', 'tlf1', 'tlf2'], 'string', 'max' => 50],
            [['telefono', 'coord_google_maps', 'identif_representante', 'celular_representante','user'], 'string', 'max' => 200],
            [['direccion'], 'string', 'max' => 150],
            [['email_representante'], 'email'],
            [['acuerdo_comercial'], 'string', 'max' => 100],
            [['registro_mercantil'], 'string', 'max' => 100],
            [['vencimiento_rm'], 'safe'],
            [['vencimiento_acuerdo'], 'safe'],
            [['nombre'], 'unique'],

            ////Cuando tipo_pago == PRONTO PAGO
            [['descuento'], 'required', 'when' =>  function ($model) {
                                                        return ($model->tipo_pago == 0);
                                                    }, 'whenClient' =>"function (tipo_pago, value) {
                                                            return ($('#field_tipo_pago').val() == 0);
                                                        }"],

            ////Cuando tipo_pago == CRÉDITO
            [['limite_credito'], 'required', 'when' =>  function ($model) {
                                                        return ($model->tipo_pago == 1);
                                                    }, 'whenClient' =>"function (tipo_pago, value) {
                                                            return ($('#field_tipo_pago').val() == 1);
                                                        }"],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_taller' => 'Id Taller',
            'tipo_persona' => 'TIPO DE PERSONA',
            'rnc' => 'RNC',
            'nombre' => 'NOMBRE',
            'email_taller' => 'EMAIL TALLER',
            'telefono' => 'TELÉFONO',
            'direccion' => 'DIRECCIÓN',
            'coord_google_maps' => 'COORDENADAS GOOGLE MAPS',
            'tipo_pago' => 'TIPO DE PAGO',
            'descuento' => 'DESCUENTO POR PRONTO PAGO',
            'limite_credito' => 'LÍMITE DE CRÉDITO',
            'nombre_representante' => 'NOMBRE',
            'identif_representante' => 'NÚMERO DE IDENTIFICACIÓN',
            'email_representante' => 'CORREO ELECTRÓNICO',
            'celular_representante' => 'CELULAR',
            'espacios' => 'ESPACIOS',
            'nivel_servicio' => 'NIVEL SERVICIO',
            'acuerdo_comercial' => 'ACUERDO COMERCIAL',
            'vencimiento_acuerdo' => 'VENCIMIENTO ACUERDO COMERCIAL',            
            'registro_mercantil' => 'REGISTRO MERCANTIL',
            'vencimiento_rm' => 'VENCIMIENTO REGISTRO MERCANTIL',
            'user' => 'NOMBRE DE USUARIO',
            'dias' => 'DÍAS DE PAGO',
            'tlf1' => 'TELÉFONO 1',
            'tlf2' => 'TELÉFONO 2',
            'garantia_global' => 'DÍAS DE GARANTÍA GLOBAL',
            'precio_pieza' => 'PRECIO POR PIEZA DES. Y PINT.',
            'tipo_taller' => 'TIPO DE TALLER',
            'tipo_servicio_taller' => 'TIPO SERVICIO TALLER',
            'precio_pieza_mecanica' => 'PRECIO PIEZA MECÁNICA',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDespachos()
    {
       return $this->hasMany(Despacho::className(), ['id_taller' => 'id_taller']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificaciones()
    {
        return $this->hasMany(Notificaciones::className(), ['id_taller' => 'id_taller']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSolicitudServicioTallers()
    {
        return $this->hasMany(SolicitudServicioTaller::className(), ['id_taller' => 'id_taller']);
    }

    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function getVehiculoTallers()
    {
       return $this->hasMany(VehiculoTaller::className(), ['id_taller' => 'id_taller']); 
    } 
}
