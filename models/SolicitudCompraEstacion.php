<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "solicitud_compra_estacion".
 *
 * @property integer $id
 * @property integer $tipo_solicitud
 * @property integer $id_estacion
 * @property string $ficha
 * @property string $placa
 * @property integer $id_modelo
 * @property string $fecha_registro
 * @property integer $id_vehiculo
 *
 * @property Estacion $idEstacion
 * @property Modelo $idModelo
 * @property Vehiculo $idVehiculo
 * @property SolicitudPiezaEstacion[] $solicitudPiezaEstacions
 */
class SolicitudCompraEstacion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'solicitud_compra_estacion';
    }

    public $id_marca, $id_modelo2;
    /**
     * @inheritdoc
     */
    public function rules()
    {//'ficha' 'id_modelo', 'placa'
        return [
 
            // [['tipo_solicitud', 'id_estacion', 'id_modelo', 'id_vehiculo', 'id_caso'], 'integer'],
 
            // [['tipo_solicitud', 'id_estacion', ], 'required'],
 
            [['tipo_solicitud', 'id_estacion', 'id_modelo', 'id_vehiculo', 'id_marca', 'id_modelo2', 'id_caso'], 'integer'],
            [['tipo_solicitud', 'id_estacion'], 'required'],
 
            [['fecha_registro'], 'safe'],
            [['ficha', 'placa'], 'string', 'max' => 20],
            [['id_estacion'], 'exist', 'skipOnError' => true, 'targetClass' => Estacion::className(), 'targetAttribute' => ['id_estacion' => 'id_estacion']],
            [['id_modelo'], 'exist', 'skipOnError' => true, 'targetClass' => Modelo::className(), 'targetAttribute' => ['id_modelo' => 'id_modelo']],
            [['id_vehiculo'], 'exist', 'skipOnError' => true, 'targetClass' => Vehiculo::className(), 'targetAttribute' => ['id_vehiculo' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tipo_solicitud' => 'Tipo Solicitud',
            'id_estacion' => 'Id Estacion',
            'ficha' => 'Ficha',
            'placa' => 'Placa',
            'id_modelo' => 'Id Modelo',
            'fecha_registro' => 'Fecha Registro',
            'id_vehiculo' => 'Id Vehiculo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstacion()
    {
        return $this->hasOne(Estacion::className(), ['id_estacion' => 'id_estacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdModelo()
    {
        return $this->hasOne(Modelo::className(), ['id_modelo' => 'id_modelo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdVehiculo()
    {
        return $this->hasOne(Vehiculo::className(), ['id' => 'id_vehiculo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSolicitudPiezaEstacions()
    {
        return $this->hasMany(SolicitudPiezaEstacion::className(), ['id_solicitud_compra_estacion' => 'id']);
    }
}
