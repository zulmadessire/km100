<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proyeccion_mantenimiento".
 *
 * @property integer $id
 * @property string $fecha_proyectada
 * @property string $km_proyectado
 * @property integer $realizado
 * @property integer $serie
 * @property integer $id_vehiculo
 * @property integer $id_proximo_tipo_mantenimiento
 * @property string $fecha_reporte
 * @property integer $id_estacion_reporte
 *
 * @property MantenimientoPreventivo[] $mantenimientoPreventivos
 * @property TipoMantenimiento $idProximoTipoMantenimiento
 * @property Vehiculo $idVehiculo
 */
class ProyeccionMantenimiento extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'proyeccion_mantenimiento';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha_proyectada', 'fecha_reporte'], 'safe'],
            [['realizado', 'serie', 'id_vehiculo', 'id_proximo_tipo_mantenimiento', 'id_estacion_reporte'], 'integer'],
            [['id_vehiculo', 'id_proximo_tipo_mantenimiento'], 'required'],
            [['km_proyectado'], 'string', 'max' => 45],
            [['id_proximo_tipo_mantenimiento'], 'exist', 'skipOnError' => true, 'targetClass' => TipoMantenimiento::className(), 'targetAttribute' => ['id_proximo_tipo_mantenimiento' => 'id_tipo_mnto']],
            [['id_vehiculo'], 'exist', 'skipOnError' => true, 'targetClass' => Vehiculo::className(), 'targetAttribute' => ['id_vehiculo' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha_proyectada' => 'Fecha Proyectada',
            'km_proyectado' => 'Km Proyectado',
            'realizado' => 'Realizado',
            'serie' => 'Serie',
            'id_vehiculo' => 'Id Vehiculo',
            'id_proximo_tipo_mantenimiento' => 'Id Proximo Tipo Mantenimiento',
            'fecha_reporte' => 'Fecha Reporte',
            'id_estacion_reporte' => 'Id Estacion Reporte',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMantenimientoPreventivos()
    {
        return $this->hasMany(MantenimientoPreventivo::className(), ['id_proyeccion_mantenimiento' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProximoTipoMantenimiento()
    {
        return $this->hasOne(TipoMantenimiento::className(), ['id_tipo_mnto' => 'id_proximo_tipo_mantenimiento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdVehiculo()
    {
        return $this->hasOne(Vehiculo::className(), ['id' => 'id_vehiculo']);
    }
}
