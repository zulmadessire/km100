<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Licitacion;

/**
 * LicitacionesSearch represents the model behind the search form about `app\models\Licitacion`.
 */
class LicitacionesSearch extends Licitacion
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tiempo_entrega_s_d', 'tiempo_entrega_i', 'id_rubro', 'id_estado_lic'], 'integer'],
            [['fecha_inicio', 'fecha_culminacion', 'garantia', 'dias'], 'safe'],
            [['objetivo'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Licitacion::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fecha_inicio' => $this->fecha_inicio,
            'fecha_culminacion' => $this->fecha_culminacion,
            'objetivo' => $this->objetivo,
            'tiempo_entrega_s_d' => $this->tiempo_entrega_s_d,
            'tiempo_entrega_i' => $this->tiempo_entrega_i,
            'id_rubro' => $this->id_rubro,
            'id_estado_lic' => $this->id_estado_lic,
        ]);

        $query->andFilterWhere(['like', 'garantia', $this->garantia])
            ->andFilterWhere(['like', 'dias', $this->dias]);

        return $dataProvider;
    }
    public function searchProveedores($params,$id_proveedor)
    {
        $query = Licitacion::find()
            ->join('JOIN','proveedor_pieza_lic','proveedor_pieza_lic.id_licitacion = licitacion.id')
            ->where(['proveedor_pieza_lic.id_prov_pieza' => $id_proveedor]);
         // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fecha_inicio' => $this->fecha_inicio,
            'fecha_culminacion' => $this->fecha_culminacion,
            'objetivo' => $this->objetivo,
            'tiempo_entrega_s_d' => $this->tiempo_entrega_s_d,
            'tiempo_entrega_i' => $this->tiempo_entrega_i,
            'id_rubro' => $this->id_rubro,
            'id_estado_lic' => $this->id_estado_lic,
        ]);

        $query->andFilterWhere(['like', 'garantia', $this->garantia])
            ->andFilterWhere(['like', 'dias', $this->dias]);

        return $dataProvider;
    }
     public function searchVistaprov($params,$id_prec_prov)
    {
        $query = Precios::find()
            ->where(['id_prec_prov' => $id_prec_prov]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fecha_inicio' => $this->fecha_inicio,
            'fecha_culminacion' => $this->fecha_culminacion,
            'objetivo' => $this->objetivo,
            'tiempo_entrega_s_d' => $this->tiempo_entrega_s_d,
            'tiempo_entrega_i' => $this->tiempo_entrega_i,
            'id_rubro' => $this->id_rubro,
            'id_estado_lic' => $this->id_estado_lic,
        ]);

        $query->andFilterWhere(['like', 'garantia', $this->garantia])
            ->andFilterWhere(['like', 'dias', $this->dias]);

        return $dataProvider;
    }
}
