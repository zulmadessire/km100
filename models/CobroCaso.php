<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cobro_caso".
 *
 * @property integer $id
 * @property double $monto
 * @property integer $forma_pago
 * @property integer $motivo_cobro
 * @property string $fecha
 * @property integer $id_caso
 * @property string $numero_factura
 *
 * @property Caso $idCaso
 */
class CobroCaso extends \yii\db\ActiveRecord
{
    public $opcobro;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cobro_caso';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['monto', 'forma_pago', 'motivo_cobro', 'fecha', 'id_caso', 'numero_factura'], 'required','whenClient' => "function (attribute, value) {
                return $('#opm').val() == 1;
            }"],
            [['monto'], 'number'],
            [['forma_pago', 'motivo_cobro', 'id_caso','opcobro'], 'integer'],
            [['fecha'], 'safe'],
            [['numero_factura'], 'string', 'max' => 45],
            [['id_caso'], 'exist', 'skipOnError' => true, 'targetClass' => Caso::className(), 'targetAttribute' => ['id_caso' => 'id_caso']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'monto' => 'Monto',
            'forma_pago' => 'Forma de Pago',
            'motivo_cobro' => 'Motivo de Cobro',
            'fecha' => 'Fecha de Cobro',
            'id_caso' => 'Id Caso',
            'numero_factura' => 'Número de Factura',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCaso()
    {
        return $this->hasOne(Caso::className(), ['id_caso' => 'id_caso']);
    }
}
