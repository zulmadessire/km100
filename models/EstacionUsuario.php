<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estacion_usuario".
 *
 * @property integer $id
 * @property integer $estacion_id
 * @property integer $user_id
 *
 * @property Estacion $estacion
 * @property User $user
 */
class EstacionUsuario extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'estacion_usuario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['estacion_id', 'user_id'], 'integer'],
            [['estacion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Estacion::className(), 'targetAttribute' => ['estacion_id' => 'id_estacion']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'estacion_id' => 'Estacion ID',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstacion()
    {
        return $this->hasOne(Estacion::className(), ['id_estacion' => 'estacion_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
