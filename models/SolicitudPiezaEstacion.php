<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "solicitud_pieza_estacion".
 *
 * @property integer $id
 * @property integer $cantidad
 * @property integer $id_solicitud_compra_estacion
 * @property integer $id_pieza
 * @property string $descipcion
 * @property integer $prioridad
 * @property string $observacion
 */
class SolicitudPiezaEstacion extends \yii\db\ActiveRecord
{
    public $tipo,  $imageFiles;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'solicitud_pieza_estacion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cantidad', 'id_solicitud_compra_estacion', 'id_pieza', 'tipo', 'prioridad'], 'integer'],
            [['observacion_pieza'], 'string'],
            [['imagen_pieza'], 'string', 'max' => 255],
            [['descipcion', 'observacion'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cantidad' => 'Cantidad',
            'id_solicitud_compra_estacion' => 'Id Solicitud Compra Estacion',
            'id_pieza' => 'Id Pieza',
            'descipcion' => 'Descipcion',
            'prioridad' => 'Prioridad',
            'observacion' => 'Observacion',
        ];
    }

    public function getIdPieza()
    {
        return $this->hasOne(Pieza::className(), ['id_pieza' => 'id_pieza']);
    }
}
