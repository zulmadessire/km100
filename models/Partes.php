<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "partes".
 *
 * @property int $id
 * @property string $nombre
 * @property int $id_tipo_garantia
 * @property int $estado
 *
 * @property GarantiaVehiculo[] $garantiaVehiculos
 * @property TipoGarantia $tipoGarantia
 * @property PartesGarantia[] $partesGarantias
 */
class Partes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'partes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['id_tipo_garantia', 'estado'], 'integer'],
            [['nombre'], 'string', 'max' => 45],
            [['id_tipo_garantia'], 'exist', 'skipOnError' => true, 'targetClass' => TipoGarantia::className(), 'targetAttribute' => ['id_tipo_garantia' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'id_tipo_garantia' => 'Id Tipo Garantia',
            'estado' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGarantiaVehiculos()
    {
        return $this->hasMany(GarantiaVehiculo::className(), ['id_partes' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoGarantia()
    {
        return $this->hasOne(TipoGarantia::className(), ['id' => 'id_tipo_garantia']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartesGarantias()
    {
        return $this->hasMany(PartesGarantia::className(), ['partes_id' => 'id']);
    }
}
