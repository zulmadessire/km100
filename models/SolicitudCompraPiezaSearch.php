<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SolicitudCompraPieza;
use yii\helpers\VarDumper;

/**
 * SolicitudCompraPiezaSearch represents the model behind the search form about `app\models\SolicitudCompraPieza`.
 */
class SolicitudCompraPiezaSearch extends SolicitudCompraPieza
{
    /**
     * @inheritdoc
     */
    public $id_modelo, $mva, $id_analista, $dias, $id_taller, $chasis, $id_estacion;
    public function rules()
    {
        return [
            [['id', 'codigo_estado', 'id_servicio', 'id_cotizacion_taller', 'id_orden_trabajo', 'id_modelo', 'id_analista', 'dias', 'id_estacion', 'id_taller', 'aprobacion'], 'integer'],
            [['mva', 'chasis', 'observacion_gerente'], 'string'],
            [['fecha', 'mva'], 'safe'],
            [['dias'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

 
        $query = SolicitudCompraPieza::find();
 
        // $query->where('codigo_estado = 4');
        //  ->join('JOIN','Pipeline_compra_pieza','Pipeline_compra_pieza.id_solicitud_compra_pieza = Solicitud_compra_pieza.id')
        //  ->where('Pipeline_compra_pieza.id_estado_compra_pieza = 4');
        // $query->Where('(SELECT f.* FROM flota f INNER JOIN estacion e 
        //             ON e.id_estacion = f.id_estacion INNER JOIN vehiculo v 
        //             ON v.id = f.id_vehiculo 
        //             INNER JOIN (SELECT  * FROM proyeccion_mantenimiento 
        //             GROUP BY id_vehiculo ORDER BY `id_vehiculo` DESC) p 
        //             ON p.id_vehiculo = v.id INNER JOIN  
        //             (SELECT ev.* FROM estado_vehiculo ev INNER JOIN estado es 
        //             ON es.id = ev.id_estado AND es.id_tipo_estado = 4 
        //             WHERE ev.id_vehiculo = 1 
        //             AND ev.id IN ( SELECT MAX(id) FROM estado_vehiculo GROUP BY id_vehiculo ) ) ev 
        //             ON ev.id_vehiculo = v.id AND ev.id_estado != 10 
        //             WHERE e.id_estacion = 17 AND f.fecha_fin IS NULL)');
 
 
        $query = SolicitudCompraPieza::find()
        ->where('origen_solicitud = 1')
        ->orWhere('origen_solicitud is null');
 

        // Taller
        if ( isset( $params['SolicitudCompraPiezaSearch']['id_taller'] ) AND $params['SolicitudCompraPiezaSearch']['id_taller'] != '') {
            $query->andWhere( '(SELECT ta.id_taller FROM cotizacion_taller c 
            INNER JOIN solicitud_servicio_taller sst ON c.id_solicitud_servicio_taller = sst.id 
            INNER JOIN taller ta ON ta.id_taller = sst.id_taller  
            WHERE c.id = solicitud_compra_pieza.id_cotizacion_taller LIMIT 1) = '.$params['SolicitudCompraPiezaSearch']['id_taller'] );
        }

        // Chasis
        if ( isset( $params['SolicitudCompraPiezaSearch']['chasis'] ) AND $params['SolicitudCompraPiezaSearch']['chasis'] != '') {
            $query->andWhere( '(SELECT ve.chasis FROM servicio se 
            INNER JOIN vehiculo ve ON se.id_vehiculo = ve.id 
            WHERE se.id = solicitud_compra_pieza.id_servicio LIMIT 1) like '."'%".$params['SolicitudCompraPiezaSearch']['chasis']."%'" ); 
        }

        //Analista que aprobo solicitud
        if ( isset( $params['SolicitudCompraPiezaSearch']['id_analista'] ) AND $params['SolicitudCompraPiezaSearch']['id_analista'] != '') {
            $query->andWhere( '(SELECT id FROM user 
            WHERE user.id = solicitud_compra_pieza.id_analista LIMIT 1) = '.$params['SolicitudCompraPiezaSearch']['id_analista'] );
        }

                // //Analista de mantenimiento
                // if ( isset( $params['SolicitudCompraPiezaSearch']['id_analista'] ) AND $params['SolicitudCompraPiezaSearch']['id_analista'] != '') {
                //     $query->andWhere( '(SELECT cta.id_analista FROM cotizacion_taller c 
                //     INNER JOIN cotizacion_taller_analista cta ON c.id = cta.id_cotizacion_taller 
                //     WHERE c.id = solicitud_compra_pieza.id_cotizacion_taller LIMIT 1) = '.$params['SolicitudCompraPiezaSearch']['id_analista'] );
                // }

        //Modelo
        if ( isset( $params['SolicitudCompraPiezaSearch']['id_modelo'] ) AND $params['SolicitudCompraPiezaSearch']['id_modelo'] != '') {
            $query->andWhere( '(SELECT v.id_modelo FROM servicio s 
            INNER JOIN vehiculo v ON s.id_vehiculo = v.id 
            WHERE s.id = solicitud_compra_pieza.id_servicio LIMIT 1) = '.$params['SolicitudCompraPiezaSearch']['id_modelo'] );
        }

        //MVA
        if ( isset( $params['SolicitudCompraPiezaSearch']['mva'] ) AND $params['SolicitudCompraPiezaSearch']['mva'] != '') {
            $query->andWhere( '(SELECT v.mva FROM servicio s 
            INNER JOIN vehiculo v ON s.id_vehiculo = v.id 
            WHERE s.id = solicitud_compra_pieza.id_servicio LIMIT 1) = '."'".strtoupper($params['SolicitudCompraPiezaSearch']['mva'])."'" );
        }

        //Dias
        if ( isset( $params['SolicitudCompraPiezaSearch']['dias'] ) AND $params['SolicitudCompraPiezaSearch']['dias'] != '') {
            $query->andWhere( '(SELECT DATEDIFF(NOW(), s.fecha) as dias FROM solicitud_compra_pieza s 
            WHERE s.id = solicitud_compra_pieza.id) = '.$params['SolicitudCompraPiezaSearch']['dias'] );
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fecha' => $this->fecha,
            'codigo_estado' => $this->codigo_estado,
            'id_servicio' => $this->id_servicio,
            'id_cotizacion_taller' => $this->id_cotizacion_taller,
            'id_orden_trabajo' => $this->id_orden_trabajo,
            'aprobacion' => $this->aprobacion,
        ]);



        return $dataProvider;
    }

    public function searchEstacion($params)
    {
        // print_r($_GET);
        // die('entra');

        $query = SolicitudCompraPieza::find()
        ->where('origen_solicitud = 2');

 
        // Taller
        if ( isset( $params['SolicitudCompraPiezaSearch']['id_estacion'] ) AND $params['SolicitudCompraPiezaSearch']['id_estacion'] != '') {
            $query->andWhere( '(SELECT ta.id_estacion FROM solicitud_compra_estacion c 
            INNER JOIN estacion ta ON c.id_estacion = ta.id_estacion  
            WHERE c.id = solicitud_compra_pieza.id_solicitud_compra_estacion LIMIT 1) = '.$params['SolicitudCompraPiezaSearch']['id_estacion'] );
        }

        // Chasis
        if ( isset( $params['SolicitudCompraPiezaSearch']['chasis'] ) AND $params['SolicitudCompraPiezaSearch']['chasis'] != '') {
            $query->andWhere( '(SELECT ve.chasis FROM solicitud_compra_estacion se 
            INNER JOIN vehiculo ve ON se.id_vehiculo = ve.id 
            WHERE se.id = solicitud_compra_pieza.id_solicitud_compra_estacion LIMIT 1) like '."'%".$params['SolicitudCompraPiezaSearch']['chasis']."%'" ); 
        }

        //Analista que aprobo solicitud
        if ( isset( $params['SolicitudCompraPiezaSearch']['id_analista'] ) AND $params['SolicitudCompraPiezaSearch']['id_analista'] != '') {
            $query->andWhere( '(SELECT id FROM user 
            WHERE user.id = solicitud_compra_pieza.id_analista LIMIT 1) = '.$params['SolicitudCompraPiezaSearch']['id_analista'] );
        }

        //Modelo
        if ( isset( $params['SolicitudCompraPiezaSearch']['id_modelo'] ) AND $params['SolicitudCompraPiezaSearch']['id_modelo'] != '') {
            $query->andWhere( '(SELECT mo.id_modelo FROM solicitud_compra_estacion s 
            INNER JOIN modelo mo ON mo.id_modelo = s.id_modelo 
            WHERE s.id = solicitud_compra_pieza.id_solicitud_compra_estacion LIMIT 1) = '.$params['SolicitudCompraPiezaSearch']['id_modelo'] );
        }

        //MVA
        if ( isset( $params['SolicitudCompraPiezaSearch']['mva'] ) AND $params['SolicitudCompraPiezaSearch']['mva'] != '') {
            $query->andWhere( '(SELECT v.mva FROM servicio s 
            INNER JOIN vehiculo v ON s.id_vehiculo = v.id 
            WHERE s.id = solicitud_compra_pieza.id_servicio LIMIT 1) = '."'".strtoupper($params['SolicitudCompraPiezaSearch']['mva'])."'" );
        }

        //Dias
        if ( isset( $params['SolicitudCompraPiezaSearch']['dias'] ) AND $params['SolicitudCompraPiezaSearch']['dias'] != '') {
            $query->andWhere( '(SELECT DATEDIFF(NOW(), s.fecha) as dias FROM solicitud_compra_pieza s 
            WHERE s.id = solicitud_compra_pieza.id) = '.$params['SolicitudCompraPiezaSearch']['dias'] );
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fecha' => $this->fecha,
            'codigo_estado' => $this->codigo_estado,
            'id_servicio' => $this->id_servicio,
            'id_cotizacion_taller' => $this->id_cotizacion_taller,
            'id_solicitud_compra_estacion' => $this->id_solicitud_compra_estacion,
            'id_orden_trabajo' => $this->id_orden_trabajo,
            'aprobacion' => $this->aprobacion,
        ]);



        return $dataProvider;
    }
}



