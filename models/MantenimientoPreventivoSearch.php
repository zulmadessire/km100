<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MantenimientoPreventivo;

/**
 * MantenimientoPreventivoSearch represents the model behind the search form about `app\models\MantenimientoPreventivo`.
 */
class MantenimientoPreventivoSearch extends MantenimientoPreventivo
{
    public $fecha, $id_tipo, $id_cliente,$fechas;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_estacion','id_cliente','fechas', 'id_tipo_mantenimiento','id_proyeccion_mantenimiento', 'id_concesionario','id_tipo'], 'integer'],
            [['imagen_soporte', 'responsable', 'fecha_realizacion', 'observaciones', 'km', 'observacion_adicional','fecha'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MantenimientoPreventivo::find();

 
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        // if ( isset($params['MantenimientoPreventivoSearch']['fecha']) AND $params['MantenimientoPreventivoSearch']['fecha'] != '') {
        //         $query->andWhere( '(SELECT mp.fecha_realizacion FROM mantenimiento_preventivo mp WHERE mp.fecha_realizacion = mantenimiento_preventivo.fecha_realizacion LIMIT 1) = '. $params['MantenimientoPreventivoSearch']['fecha'] );
        //     }
            if ( isset($params['MantenimientoPreventivoSearch']['id_tipo']) AND $params['MantenimientoPreventivoSearch']['id_tipo'] != '') {
               
                $query->andWhere( '(SELECT mp.id_tipo_mantenimiento FROM mantenimiento_preventivo mp INNER JOIN tipo_mantenimiento tm ON mp.id_tipo_mantenimiento=tm.id_tipo_mnto WHERE mp.id_tipo_mantenimiento = mantenimiento_preventivo.id_tipo_mantenimiento LIMIT 1) = '. $params['MantenimientoPreventivoSearch']['id_tipo'] );
            
            }
            if ( isset($params['MantenimientoPreventivoSearch']['id_estacion']) AND $params['MantenimientoPreventivoSearch']['id_estacion'] != '') { 
                $query->andWhere( '(SELECT mp.id_estacion FROM mantenimiento_preventivo mp INNER JOIN estacion es ON mp.id_estacion=es.id_estacion WHERE mp.id_estacion = mantenimiento_preventivo.id_estacion LIMIT 1) = '. $params['MantenimientoPreventivoSearch']['id_estacion'] );
            }

             if ( isset($params['MantenimientoPreventivoSearch']['id_cliente']) AND $params['MantenimientoPreventivoSearch']['id_cliente'] != '') { 
               
                // $query->andWhere( '(SELECT cli.id FROM mantenimiento_preventivo mp INNER JOIN estacion es ON mp.id_estacion=es.id_estacion INNER JOIN empresa em ON em.id_empresa=es.id_estacion INNER JOIN cliente cli ON cli.id = em.cliente_id WHERE mp.id = mantenimiento_preventivo.id LIMIT 1) = '. $params['MantenimientoPreventivoSearch']['id_cliente'] );    
                $query->andWhere( '(SELECT cli.id FROM mantenimiento_preventivo mp 
                                    INNER JOIN estacion es ON mp.id_estacion=es.id_estacion 
                                    INNER JOIN empresa em ON em.id_empresa=es.id_empresa 
                                    INNER JOIN cliente cli ON cli.id = em.cliente_id 
                                    WHERE mp.id_estacion = mantenimiento_preventivo.id_estacion LIMIT 1) = '. $params['MantenimientoPreventivoSearch']['id_cliente'] );
            }
            if ( isset( $params['MantenimientoPreventivoSearch']['fechas'] ) AND $params['MantenimientoPreventivoSearch']['fechas'] != '') {
            $query->andWhere( '(SELECT  fecha_realizacion FROM mantenimiento_preventivo
                                WHERE id = mantenimiento_preventivo.id ORDER BY id DESC LIMIT 1) = '.$params['MantenimientoPreventivoSearch']['fechas'] );
        }

        
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fecha_realizacion' => $this->fecha_realizacion,
            'id_estacion' => $this->id_estacion,
            'id_tipo_mantenimiento' => $this->id_tipo_mantenimiento,
            'id_proyeccion_mantenimiento' => $this->id_proyeccion_mantenimiento,
            'id_concesionario' => $this->id_concesionario,
        ]);

        $query->andFilterWhere(['like', 'imagen_soporte', $this->imagen_soporte])
            ->andFilterWhere(['like', 'responsable', $this->responsable])
            ->andFilterWhere(['like', 'observaciones', $this->observaciones])
            ->andFilterWhere(['like', 'km', $this->km])
            ->andFilterWhere(['like', 'observacion_adicional', $this->observacion_adicional]);

        return $dataProvider;
    }
     public function search_realizados($params)
    {
        
        $query = MantenimientoPreventivo::find()
        ->joinWith('idProyeccionMantenimiento')
        ->where('proyeccion_mantenimiento.realizado = 1 AND proyeccion_mantenimiento.id_vehiculo = '.$params['id']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}
