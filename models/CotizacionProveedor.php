<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cotizacion_proveedor".
 *
 * @property integer $id
 * @property string $fecha
 * @property integer $estado
 * @property integer $id_solicitud_compra_proveedor
 * @property integer $tipo_envio
 * @property double $subtotal
 * @property double $descuento
 * @property double $itbs
 *
 * @property SolicitudCompraProveedor $idSolicitudCompraProveedor
 * @property CotizacionProveedorPieza[] $cotizacionProveedorPiezas
 * @property OrdenCompraPieza[] $ordenCompraPiezas
 */
class CotizacionProveedor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cotizacion_proveedor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['estado', 'id_solicitud_compra_proveedor', 'tipo_envio'], 'integer'],
            [['id_solicitud_compra_proveedor'], 'required'],
            [['subtotal', 'descuento', 'itbs'], 'number'],
            [['tipo_envio'], 'required', 'on' => 'update'],
            [['id_solicitud_compra_proveedor'], 'exist', 'skipOnError' => true, 'targetClass' => SolicitudCompraProveedor::className(), 'targetAttribute' => ['id_solicitud_compra_proveedor' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha' => 'Fecha',
            'estado' => 'Estado',
            'id_solicitud_compra_proveedor' => 'Id Solicitud Compra Proveedor',
            'tipo_envio' => 'Tipo de Traslado',
            'subtotal' => 'Subtotal',
            'descuento' => 'Descuento',
            'itbs' => 'Itbs',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSolicitudCompraProveedor()
    {
        return $this->hasOne(SolicitudCompraProveedor::className(), ['id' => 'id_solicitud_compra_proveedor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCotizacionProveedorPiezas()
    {
        return $this->hasMany(CotizacionProveedorPieza::className(), ['id_cotizacion_proveedor' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdenCompraPiezas()
    {
        return $this->hasMany(OrdenCompraPieza::className(), ['id_cotizacion_proveedor' => 'id']);
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['update'] = ['tipo_envio'];//Scenario Values Only Accepted
        return $scenarios;
    }
}
