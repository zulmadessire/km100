<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estado_licitacion".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $color
 *
 * @property Licitacion[] $licitacions
 */
class EstadoLicitacion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'estado_licitacion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 45],
            [['color'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'color' => 'Color',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLicitacions()
    {
        return $this->hasMany(Licitacion::className(), ['id_estado_lic' => 'id']);
    }
}
