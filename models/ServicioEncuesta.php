<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "servicio_encuesta".
 *
 * @property integer $id
 * @property integer $id_servicio
 * @property integer $id_pregunta
 * @property integer $respuesta
 *
 * @property Servicio $idServicio
 */
class ServicioEncuesta extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'servicio_encuesta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_servicio', 'id_pregunta'], 'required'],
            [['id_servicio', 'id_pregunta', 'respuesta'], 'integer'],
            [['id_servicio'], 'exist', 'skipOnError' => true, 'targetClass' => Servicio::className(), 'targetAttribute' => ['id_servicio' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_servicio' => 'Id Servicio',
            'id_pregunta' => 'Id Pregunta',
            'respuesta' => 'Respuesta',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdServicio()
    {
        return $this->hasOne(Servicio::className(), ['id' => 'id_servicio']);
    }
}
