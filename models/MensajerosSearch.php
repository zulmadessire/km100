<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Mensajero;

/**
 * MensajerosSearch represents the model behind the search form about `app\models\Mensajero`.
 */
class MensajerosSearch extends Mensajero
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_modelo_id'], 'integer'],
            [['nombre', 'cedula', 'telefono', 'direccion', 'placa', 'anho'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Mensajero::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
         if ( isset( $params['MensajerosSearch']['id_marca'] ) AND $params['MensajerosSearch']['id_marca'] != '') {
            $query->andWhere( '(SELECT m.id_modelo FROM mensajero vt INNER JOIN modelo m ON vt.id_modelo_id = m.id_modelo AND m.id_modelo = '.$params['MensajerosSearch']['id_marca'].' WHERE vt.id = mensajero.id LIMIT 1) = '.$params['MensajerosSearch']['id_marca'] );

       
        }
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_modelo_id' => $this->id_modelo_id,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'cedula', $this->cedula])
            ->andFilterWhere(['like', 'telefono', $this->telefono])
            ->andFilterWhere(['like', 'direccion', $this->direccion])
            ->andFilterWhere(['like', 'placa', $this->placa])
            ->andFilterWhere(['like', 'anho', $this->anho]);

        return $dataProvider;
    }
}
