<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "auditoria_general".
 *
 * @property integer $id
 * @property string $fecha
 * @property integer $estado
 * @property integer $id_estacion
 * @property integer $id_aud_fs
 * @property integer $id_aud_mnto
 * @property integer $id_aud_ejec
 *
 * @property Estacion $idEstacion
 * @property AuditoriaEjecMant $idAudEjec
 * @property AuditoriaFs $idAudFs
 * @property AuditoriaMnto $idAudMnto
 */
class AuditoriaGeneral extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auditoria_general';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['estado', 'id_estacion', 'id_aud_fs', 'id_aud_mnto', 'id_aud_ejec'], 'integer'],
            [['id_estacion'], 'required'],
            [['id_estacion'], 'exist', 'skipOnError' => true, 'targetClass' => Estacion::className(), 'targetAttribute' => ['id_estacion' => 'id_estacion']],
            [['id_aud_ejec'], 'exist', 'skipOnError' => true, 'targetClass' => AuditoriaEjecMant::className(), 'targetAttribute' => ['id_aud_ejec' => 'id']],
            [['id_aud_fs'], 'exist', 'skipOnError' => true, 'targetClass' => AuditoriaFs::className(), 'targetAttribute' => ['id_aud_fs' => 'id']],
            [['id_aud_mnto'], 'exist', 'skipOnError' => true, 'targetClass' => AuditoriaMnto::className(), 'targetAttribute' => ['id_aud_mnto' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha' => 'Fecha',
            'estado' => 'Estado',
            'id_estacion' => 'Id Estacion',
            'id_aud_fs' => 'Id Aud Fs',
            'id_aud_mnto' => 'Id Aud Mnto',
            'id_aud_ejec' => 'Id Aud Ejec',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstacion()
    {
        return $this->hasOne(Estacion::className(), ['id_estacion' => 'id_estacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAudEjec()
    {
        return $this->hasOne(AuditoriaEjecMant::className(), ['id' => 'id_aud_ejec']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAudFs()
    {
        return $this->hasOne(AuditoriaFs::className(), ['id' => 'id_aud_fs']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAudMnto()
    {
        return $this->hasOne(AuditoriaMnto::className(), ['id' => 'id_aud_mnto']);
    }
}
