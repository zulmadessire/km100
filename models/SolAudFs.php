<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sol_aud_fs".
 *
 * @property integer $id
 * @property string $ficha
 * @property integer $id_modelo
 * @property string $causa
 * @property string $responsable
 * @property string $observacion
 * @property integer $id_aud_fs
 * @property integer $numero
 * @property string $solicitud
 * @property integer $cantidad
 * @property string $anexo
 * @property string $decripcion
 * @property string $extension
 * @property string $base_url
 * @property string $imageFile
 * @property string $anexo2
 * @property string $extension2
 * @property string $base_url2
 * @property string $anexo3
 * @property string $extension3
 * @property string $base_url3
 * @property string $anexo4
 * @property string $extension4
 * @property string $base_url4
 *
 * @property Anexos[] $anexos
 * @property AuditoriaFs $idAudFs
 * @property Modelo $idModelo
 */
class SolAudFs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sol_aud_fs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_modelo', 'id_aud_fs', 'numero', 'cantidad'], 'integer'],
            [['id_aud_fs'], 'required'],
            [['ficha', 'responsable'], 'string', 'max' => 20],
            [['causa'], 'string', 'max' => 200],
            [['observacion', 'anexo', 'extension', 'base_url', 'imageFile', 'anexo2', 'extension2', 'base_url2', 'anexo3', 'extension3', 'base_url3', 'anexo4', 'extension4', 'base_url4'], 'string', 'max' => 1000],
            [['solicitud', 'decripcion'], 'string', 'max' => 500],
            [['id_aud_fs'], 'exist', 'skipOnError' => true, 'targetClass' => AuditoriaFs::className(), 'targetAttribute' => ['id_aud_fs' => 'id']],
            [['id_modelo'], 'exist', 'skipOnError' => true, 'targetClass' => Modelo::className(), 'targetAttribute' => ['id_modelo' => 'id_modelo']],
            [['imageFile','imagefile2','imagefile3','imagefile4'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg,jpeg, pdf, mp3, wav, mp4'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ficha' => 'Ficha',
            'id_modelo' => 'Id Modelo',
            'causa' => 'Causa',
            'responsable' => 'Responsable',
            'observacion' => 'Observacion',
            'id_aud_fs' => 'Id Aud Fs',
            'numero' => 'Numero',
            'solicitud' => 'Solicitud',
            'cantidad' => 'Cantidad',
            'anexo' => 'Anexo',
            'decripcion' => 'Decripcion',
            'extension' => 'Extension',
            'base_url' => 'Base Url',
            'imageFile' => 'Image File',
            'imagefile2' => 'Image File',
            'imagefile3' => 'Image File',
            'imagefile4' => 'Image File',
            'anexo2' => 'Anexo2',
            'extension2' => 'Extension2',
            'base_url2' => 'Base Url2',
            'anexo3' => 'Anexo3',
            'extension3' => 'Extension3',
            'base_url3' => 'Base Url3',
            'anexo4' => 'Anexo4',
            'extension4' => 'Extension4',
            'base_url4' => 'Base Url4',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnexos()
    {
        return $this->hasMany(Anexos::className(), ['id_sol_aud_fs' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAudFs()
    {
        return $this->hasOne(AuditoriaFs::className(), ['id' => 'id_aud_fs']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdModelo()
    {
        return $this->hasOne(Modelo::className(), ['id_modelo' => 'id_modelo']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnexos0()
    {
        return $this->hasMany(Anexos::className(), ['id_sol_aud_fs' => 'id']);
    }

 


 public function upload()
    {
        if ($this->validate()) {
            $this->imageFile->saveAs('uploads/' . $this->imageFile->baseName . '.' . $this->imageFile->extension);
            return true;
        } else {
            return false;
        }
    }


    public function uploadArchive($id)
    {

            $this->imageFile->saveAs(\Yii::$app->basePath.'/files/'.$id.'/'.$this->imageFile->baseName .'.'. $this->imageFile->extension);

            return true;
         
    }
}
