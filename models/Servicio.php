<?php

namespace app\models;

use Yii;
use app\models\PipelineServicio;

/**
 * This is the model class for table "servicio".
 *
 * @property int $id
 * @property int $prioridad 0-NORMAL, 1-URGENTE
 * @property int $dano 0-BAJO, 1-MEDIO, 2-ALTO
 * @property string $img_reporte
 * @property string $img_acta
 * @property string $servicio
 * @property string $evaluacion_tecnica
 * @property string $responsable
 * @property string $fecha
 * @property string $ficha
 * @property string $placa
* @property int $tipo_caso
* @property int $linea 0-Quick Line, 1-Normal Line
* @property int $cobro_cliente
* @property int $id_estacion_actual
* @property int $id_vehiculo
* @property int $decision
* @property string $observacion_recepcion
* @property string $observacion_cancelado 
* @property int $usuario_cancela 
* @property int $solicita_cotizacion Indica solicitud de cotizacion de piezas: 0-No solicita, 1-Si solicita 
* @property string $observacion_finalizado 
* @property int $entrega_estacion 
* @property int $id_analista 
*
* @property CasoServicio[] $casoServicios
* @property ChecklistAvaluo[] $checklistAvaluos 
* @property PipelineServicio[] $pipelineServicios
* @property Estacion $idEstacionActual
* @property Vehiculo $idVehiculo
* @property Estacion $estacionActual
* @property User $analista
* @property Vehiculo $vehiculo
* @property ServicioEncuesta[] $servicioEncuestas
* @property ServicioVehiculoEscaneado[] $servicioVehiculoEscaneados
* @property SolicitudServicioTaller[] $solicitudServicioTallers
 */
class Servicio extends \yii\db\ActiveRecord
{
    public $temporal;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'servicio';
    }

    /**
     * @inheritdoc
     */

    public $enviarx, $accion;

    /*const SCENARIO_ENVIAR_ESTACION = 'enviar-estacion';
    const SCENARIO_ENVIAR_ESTACION_ACCION = 'accion';
    //const SCENARIO_MODIF = 'modif';

    public function scenarios()
    {
        return [
            self::SCENARIO_ENVIAR_ESTACION => ['combustible_estacion', 'km_estacion'],
            self::SCENARIO_ENVIAR_ESTACION_ACCION => ['accion'],
            //self::SCENARIO_MODIF => ['imageFiles'],
        ];
    }*/

    public function rules()
    {
        return [
            [['prioridad', 'dano', 'servicio', 'evaluacion_tecnica', 'responsable', 'ficha', 'placa', 'tipo_caso', 'linea', 'cobro_cliente', 'id_estacion_actual', 'id_vehiculo'], 'required'],
            [['prioridad', 'dano', 'tipo_caso', 'linea', 'cobro_cliente', 'id_estacion_actual', 'id_vehiculo', 'decision', 'usuario_cancela', 'solicita_cotizacion', 'entrega_estacion', 'id_analista', 'aprobacion_estacion', 'enviarx', 'accion'], 'integer'],
            [['evaluacion_tecnica', 'observacion_recepcion', 'observacion_cancelado', 'observacion_finalizado'], 'string'],
            [['fecha', 'fecha_envio_estacion'], 'safe'],
            [['combustible_estacion', 'combustible_taller'], 'number'],
            [['img_reporte', 'img_acta', 'observacion_gerente', 'observacion_director'], 'string', 'max' => 200],
            [['servicio', 'responsable'], 'string', 'max' => 255],
            [['ficha', 'placa'], 'string', 'max' => 20],
            [['km_estacion', 'km_taller'], 'string', 'max' => 100], 
            [['id_estacion_actual'], 'exist', 'skipOnError' => true, 'targetClass' => Estacion::className(), 'targetAttribute' => ['id_estacion_actual' => 'id_estacion']],
            [['id_analista'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_analista' => 'id']], 
            [['id_vehiculo'], 'exist', 'skipOnError' => true, 'targetClass' => Vehiculo::className(), 'targetAttribute' => ['id_vehiculo' => 'id']],
            [['id_analista'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_analista' => 'id']], 
            [['accion'], 'required', 'on' => 'accion'],
            [['combustible_taller', 'km_taller', 'entrega_estacion'], 'required', 'on' => 'finaliza_taller'],
            [['combustible_estacion', 'km_estacion'], 'required', 'on' => 'accion', 'whenClient' => "function (accion, value) {
                                                            return ($('#accion').val() == 0);
                                                        }"],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Numero de Solicitud',
            'prioridad' => 'Prioridad',
            'dano' => 'Daño',
            'img_reporte' => 'Reporte de Accidente',
            'img_acta' => 'Acta Policial',
            'servicio' => 'Servicio',
            'evaluacion_tecnica' => 'Evaluación Técnica',
            'responsable' => 'Nombre del técnico',
            'fecha' => 'Fecha',
            'ficha' => 'MVA o Ficha',
            'placa' => 'Placa',
            'tipo_caso' => 'Tipo Caso',
            'linea' => 'Linea',
            'cobro_cliente' => 'Cobro Cliente',
            'id_estacion_actual' => 'Estacion Actual',
            'id_vehiculo' => 'Id Vehiculo',
            'decision' => 'Decisión',
            'observacion_recepcion' => 'Observación',
            'dias' => 'Días en Gestión',
            'observacion_cancelado' => 'Motivo de cancelación',
            'estatus' => 'Estatus',
            'usuario_cancela' => 'Usuario Cancela',
            'solicita_cotizacion' => 'Solicita Cotizacion',
            'observacion_finalizado' => 'Observacion Finalizado',
            'entrega_estacion' => 'Entrega Estacion',
            'id_analista' => 'Id Analista',
            'observacion_gerente' => 'Observacion Gerente',
            'observacion_director' => 'Observacion Director',
            'aprobacion_estacion' => 'Aprobacion Estacion',
            'fecha_envio_estacion' => 'Fecha Envio Estacion',
            'km_estacion' => 'Km Estacion',
            'combustible_estacion' => 'Combustible Estacion',
            'km_taller' => 'Km',
            'combustible_taller' => 'Combustible',
        ];
    }
      
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCasoServicios()
    {
        return $this->hasMany(CasoServicio::className(), ['id_servicio' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPipelineServicios()
    {
        return $this->hasMany(PipelineServicio::className(), ['id_servicio' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstacionActual()
    {
        return $this->hasOne(Estacion::className(), ['id_estacion' => 'id_estacion_actual']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdVehiculo()
    {
        return $this->hasOne(Vehiculo::className(), ['id' => 'id_vehiculo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicioVehiculoEscaneados()
    {
        return $this->hasMany(ServicioVehiculoEscaneado::className(), ['id_servicio' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSolicitudServicioTallers()
    {
        return $this->hasMany(SolicitudServicioTaller::className(), ['id_servicio' => 'id']);
    }

    public function getDias() {

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("
            (SELECT DATEDIFF(NOW(), s.fecha) as dias FROM servicio s WHERE s.id = ".$this->id.")");

        $result = $command->queryOne();

        return $result['dias'].' DÍAS';
    }

    public function getEstatus() {

        $estado = PipelineServicio::find()->joinWith('idEstadoServicio')->where([ 'id_servicio' => $this->id ])->orderBy('id DESC')->one();

        if ( $estado->idEstadoServicio->codigo == 11 && $this->aprobacion_estacion === 0 ) {
            return '<span class="label label-oval" style="background-color:#da0f3a; ">Aprobación Estación</span>';
        } else{
            return '<span class="label label-oval" style="background-color:'.$estado->idEstadoServicio->color.'; ">'.$estado->idEstadoServicio->nombre.'</span>';
        }
        
    }

    public function getTaller() {
        $taller = SolicitudServicioTaller::find()->where(['id_servicio' => $this->id])->one();
        if ($taller) {
            $taller = $taller->idTaller->nombre;
        } else{
            $taller = '-';
        }
        return ucfirst($taller);
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['accion'] = ['combustible_estacion', 'km_estacion'];//Scenario Values Only Accepted
        $scenarios['finaliza_taller'] = ['combustible_taller', 'km_taller', 'entrega_estacion'];//Scenario Values Only Accepted
        return $scenarios;
    }
}
