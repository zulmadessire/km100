<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "checklist_avaluo".
 *
 * @property int $id
 * @property string $danos
 * @property string $diagnostico
 * @property int $id_cotizacion_taller_analista
 * @property int $id_servicio
 * @property int $id_analista
 *
 * @property CotizacionTallerAnalista $cotizacionTallerAnalista
 * @property Servicio $servicio
 * @property User $analista
 * @property ChecklistAvaluoPieza[] $checklistAvaluoPiezas
 */
class ChecklistAvaluo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'checklist_avaluo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_cotizacion_taller_analista', 'id_servicio', 'id_analista'], 'integer'],
            [['danos', 'diagnostico'], 'string', 'max' => 45],
            //[['id_cotizacion_taller_analista'], 'exist', 'skipOnError' => true, 'targetClass' => CotizacionTallerAnalista::className(), 'targetAttribute' => ['id_cotizacion_taller_analista' => 'id']],
            [['id_servicio'], 'exist', 'skipOnError' => true, 'targetClass' => Servicio::className(), 'targetAttribute' => ['id_servicio' => 'id']],
            [['id_analista'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_analista' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'danos' => 'Danos',
            'diagnostico' => 'Diagnostico',
            'id_cotizacion_taller_analista' => 'Id Cotizacion Taller Analista',
            'id_servicio' => 'Id Servicio',
            'id_analista' => 'Id Analista',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCotizacionTallerAnalista()
    {
        return $this->hasOne(CotizacionTallerAnalista::className(), ['id' => 'id_cotizacion_taller_analista']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicio()
    {
        return $this->hasOne(Servicio::className(), ['id' => 'id_servicio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnalista()
    {
        return $this->hasOne(User::className(), ['id' => 'id_analista']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChecklistAvaluoPiezas()
    {
        return $this->hasMany(ChecklistAvaluoPieza::className(), ['id_checklist_avaluo' => 'id']);
    }

    public function getIdUser() 
    { 
        return $this->hasOne(User::className(), ['id' => 'id_analista']); 
    }
}
