<?php
namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;
use app\models\Taller;


class UploadRegistroMercantil extends Model
{
    /**
     * @var UploadedFile[]
     */

    public $imageFiles;

    public function rules()
    {
        return [
            [['imageFiles'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf, png, jpg, jpeg', 'maxFiles' => 1, 'maxSize' => 4194304],
        ];
    }

    public function attributeLabels()
    {
        return [
            'imageFiles' => 'REGISTRO MERCANTIL',
        ];
    }

    public function upload($id_taller)
    {
        $basePath = '/uploads/talleres/' . $id_taller;

        //if ($this->validate()) {
            foreach ($this->imageFiles as $i => $file) {
                $soporte = Taller::find()->where(['id_taller' => $id_taller])->One();

                if ($soporte->registro_mercantil) {
                    unlink('.'.$soporte->registro_mercantil);
                }
                /*$text = $file->baseName;
                $nom_img  = str_replace(' ', '', $text);*/
                $file->saveAs('.'. $basePath .'/registro-mercantil-'.$id_taller.'.'.$file->extension);

                $soporte->registro_mercantil=$basePath .'/registro-mercantil-'.$id_taller.'.'.$file->extension;
                $soporte->save();
            }
            return true;
        /*} else {
          die('aqui oy');
            return false;
        }*/
    }
}
?>