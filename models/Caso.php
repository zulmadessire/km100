<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "caso".
 *
 * @property integer $id_caso
 * @property string $ficha
 * @property string $placa
 * @property integer $tipo_caso
 * @property integer $fuente_accidente
 * @property integer $amet
 * @property integer $num_contrato
 * @property integer $tipo_cliente
 * @property integer $cobertura_cdw
 * @property integer $claim
 * @property integer $deducible
 * @property double $monto_deducible
 * @property double $tarifa_base
 * @property string $nombre_empleado
 * @property string $fecha_acc_inc
 * @property string $descripcion
 * @property integer $registrado_por
 * @property integer $id_estacion
 * @property integer $id_modelo
 * @property integer $id_tipo_accidente
 * @property integer $id_ubicacion
 * @property integer $req_cotizacion
 *
 * @property Estacion $idEstacion
 * @property Modelo $idModelo
 * @property Tipoaccidente $idTipoAccidente
 * @property Ubicacionunidad $idUbicacion
 * @property User $registradoPor
 * @property Soporte[] $soportes
 */
class Caso extends \yii\db\ActiveRecord
{
    public $id_marca;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'caso';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ficha', 'placa', 'tipo_caso', 'registrado_por', 'id_estacion', 'id_modelo'], 'required'],
            [['tipo_caso', 'fuente_accidente', 'amet', 'tipo_cliente', 'cobertura_cdw', 'claim', 'deducible', 'registrado_por', 'id_estacion', 'id_modelo', 'id_tipo_accidente', 'id_ubicacion', 'req_cotizacion'], 'integer'],
            [['monto_deducible', 'tarifa_base'], 'number'],
            [['fecha_acc_inc', 'fecha_registro'], 'safe'],
            [['ficha', 'placa', 'num_contrato'], 'string', 'max' => 20],
            [['nombre_empleado'], 'string', 'max' => 50],
            [['nombre_empleado'], 'match', 'pattern' =>  '/^[a-zA-Z" "]+$/'],
            //[['placa'], 'string', 'max' => 7, 'min' => 7],
            //[['placa'], 'match', 'pattern' =>  '/^[A-Z][0-9]+$/'],
            [['descripcion'], 'string', 'max' => 1000],
            [['piezas'], 'string', 'max' => 500],  /////Campo temporal...
            [['id_estacion'], 'exist', 'skipOnError' => true, 'targetClass' => Estacion::className(), 'targetAttribute' => ['id_estacion' => 'id_estacion']],
            [['id_modelo'], 'exist', 'skipOnError' => true, 'targetClass' => Modelo::className(), 'targetAttribute' => ['id_modelo' => 'id_modelo']],
            [['id_tipo_accidente'], 'exist', 'skipOnError' => true, 'targetClass' => Tipoaccidente::className(), 'targetAttribute' => ['id_tipo_accidente' => 'id_tipoaccidente']],
            [['id_ubicacion'], 'exist', 'skipOnError' => true, 'targetClass' => Ubicacionunidad::className(), 'targetAttribute' => ['id_ubicacion' => 'id_ubicacionunidad']],
            [['registrado_por'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['registrado_por' => 'id']],

            ////Cuando tipo_caso = Accidente
            [['fuente_accidente', 'fecha_acc_inc', 'id_tipo_accidente'/*, 'id_ubicacion'*/], 'required', 'when' =>  function ($model) {
                                                                                                                    return $model->tipo_caso == '0';
                                                                                                                }, 'whenClient' =>"function (tipo_caso, value) {
                                                                                                                        return $('#field_tipo_caso').val() == '0';
                                                                                                                    }"],

            ////Cuando tipo_caso = Incidente
            /*[['fecha_acc_inc', 'id_tipo_accidente', 'id_ubicacion'], 'required', 'when' =>  function ($model) {
                                                                                                return $model->tipo_caso == 1;
                                                                                            }, 'whenClient' =>"function (tipo_caso, value) {
                                                                                                    return $('#field_tipo_caso').val() == 1;
                                                                                                }"], */
            ////Cuando id_tipo_accidente != Robo de la Unidad o vacio
            [['id_ubicacion'], 'required', 'when' =>  function ($model) {
                                                            return ($model->id_tipo_accidente != 15 && $model->id_tipo_accidente >= 1);
                                                        }, 'whenClient' =>"function (tipo_accidente, value) {
                                                                return ($('#field_tipo_accidente').val() != 15 && $('#field_tipo_accidente').val() >= 1 && $('#field_tipo_accidente').val() != 18);
                                                            }"],

            ////Cuando fuente_accidente = Caso Interno o Empleado
            ['nombre_empleado', 'required', 'when' =>   function ($model) {
                                                            return $model->fuente_accidente == 1;
                                                        }, 'whenClient' =>"function (fuente_accidente, value) {
                                                                return $('#field_fuente_accidente').val() == 1;
                                                            }"],

            ////Cuando fuente_accidente = Cliente final
            [['num_contrato', 'tipo_cliente', 'cobertura_cdw', 'deducible', 'req_cotizacion'], 'required', 'when' =>  function ($model) {
                                                                                                                        return $model->fuente_accidente == '0';
                                                                                                                    }, 'whenClient' =>"function (fuente_accidente, value) {
                                                                                                                            return $('#field_fuente_accidente').val() == '0';
                                                                                                                        }"],

            ////Cuando req_cotizacion = SI
            [['tarifa_base','piezas'], 'required', 'when' =>   function ($model) {
                                                        return $model->req_cotizacion == 1;
                                                    }, 'whenClient' =>"function (req_cotizacion, value) {
                                                            return $('#field_req_cotizacion').val() == 1;
                                                        }"],

            ////Cuando deducible = SI
            ['monto_deducible', 'required', 'when' =>   function ($model) {
                                                            return $model->deducible == 1;
                                                        }, 'whenClient' =>"function (deducible, value) {
                                                                return $('#field_deducible').val() == 1;
                                                            }"],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_caso' => 'Nº Caso',
            'ficha' => 'Ficha o MVA',
            'placa' => 'Número de Placa',
            'tipo_caso' => 'Tipo de Caso',
            'fuente_accidente' => 'Fuente del Accidente',
            'amet' => 'Número de Reporte de AMET',
            'num_contrato' => 'Número de Contrato',
            'tipo_cliente' => 'Tipo de Cliente',
            'cobertura_cdw' => 'Tomo Cobertura CDW',
            'claim' => 'Número de CLAIM',
            'deducible' => 'Contrato con Deducible',
            'monto_deducible' => 'Monto de Deducible',
            'tarifa_base' => 'Tarifa Base',
            'nombre_empleado' => 'Nombre Empleado',
            'fecha_acc_inc' => 'Fecha de Accidente o Incidente',
            'descripcion' => 'Descripción breve de lo ocurrido',
            'registrado_por' => 'Registrado Por',
            'id_estacion' => 'Estación',
            'id_modelo' => 'Modelo',
            'id_tipo_accidente' => 'Tipo de Accidente',
            'id_ubicacion' => 'Dónde se encuentra la Unidad',
            'req_cotizacion' => 'Requiere Cotización',
            'id_marca' => 'Marca',
            'fecha_registro' => 'Fecha de Registro',
            'piezas' => 'Piezas a Cotizar',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstacion()
    {
        return $this->hasOne(Estacion::className(), ['id_estacion' => 'id_estacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdModelo()
    {
        return $this->hasOne(Modelo::className(), ['id_modelo' => 'id_modelo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTipoAccidente()
    {
        return $this->hasOne(Tipoaccidente::className(), ['id_tipoaccidente' => 'id_tipo_accidente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUbicacion()
    {
        return $this->hasOne(Ubicacionunidad::className(), ['id_ubicacionunidad' => 'id_ubicacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegistradoPor()
    {
        return $this->hasOne(User::className(), ['id' => 'registrado_por']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSoportes()
    {
        return $this->hasMany(Soporte::className(), ['id_caso' => 'id_caso']);
    }

    public function getIdCobroCaso()
    {
        return $this->hasOne(CobroCaso::className(), ['id_caso' => 'id_caso']);
    }
}
