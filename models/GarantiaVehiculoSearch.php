<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\GarantiaVehiculo;

/**
 * GarantiaVehiculoSearch represents the model behind the search form of `app\models\GarantiaVehiculo`.
 */
class GarantiaVehiculoSearch extends GarantiaVehiculo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'motivo', 'id_partes', 'id_vehiculo', 'id_usuario_anula'], 'integer'],
            [['tiempo', 'km', 'anular', 'fecha_anula'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GarantiaVehiculo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'motivo' => $this->motivo,
            'id_partes' => $this->id_partes,
            'id_vehiculo' => $this->id_vehiculo,
            'id_usuario_anula' => $this->id_usuario_anula,
            'fecha_anula' => $this->fecha_anula,
        ]);

        $query->andFilterWhere(['like', 'tiempo', $this->tiempo])
            ->andFilterWhere(['like', 'km', $this->km])
            ->andFilterWhere(['like', 'anular', $this->anular]);

        return $dataProvider;
    }
}
