<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rubro".
 *
 * @property integer $id_rubro
 * @property string $nombre
 * @property integer $tamanio
 * @property string $unidad_medicion
 *
 * @property RubroModeloAsignado[] $rubroModeloAsignados
 */
class Rubro extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rubro';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'unidad_medicion'], 'required'],
            [['tamanio'], 'integer'],
            [['nombre'], 'string', 'max' => 50],
            [['unidad_medicion'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_rubro' => 'Id Material',
           'nombre' => 'Material',
           'tamanio' => 'Tamaño',
           'unidad_medicion' => 'Unidad de medición',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRubroModeloAsignados()
    {
        return $this->hasMany(RubroModeloAsignado::className(), ['id_rubro' => 'id_rubro']);
    }
}
