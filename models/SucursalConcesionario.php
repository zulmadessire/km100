<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sucursal_concesionario".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $direccion
 * @property integer $concesionario_id
 * @property string $nombre_asesor
 * @property string $apellido_asesor
 * @property string $telefono_asesor
 * @property string $email_asesor
 * @property string $coord_google_maps
 * @property string $email
 * @property string $telefono
 *
 * @property Concesionario $concesionario
 */
class SucursalConcesionario extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sucursal_concesionario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'direccion', 'concesionario_id'], 'required'],
            [['concesionario_id'], 'integer'],
            [['nombre', 'direccion', 'nombre_asesor', 'apellido_asesor'], 'string', 'max' => 45],
            [['telefono_asesor', 'email_asesor'], 'string', 'max' => 100],
            [['coord_google_maps', 'email', 'telefono'], 'string', 'max' => 200],
            [['concesionario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Concesionario::className(), 'targetAttribute' => ['concesionario_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'direccion' => 'Direccion',
            'concesionario_id' => 'Concesionario ID',
            'nombre_asesor' => 'Nombre Asesor',
            'apellido_asesor' => 'Apellido Asesor',
            'telefono_asesor' => 'Telefono Asesor',
            'email_asesor' => 'Email Asesor',
            'coord_google_maps' => 'Coord Google Maps',
            'email' => 'Email',
            'telefono' => 'Telefono',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConcesionario()
    {
        return $this->hasOne(Concesionario::className(), ['id' => 'concesionario_id']);
    }
}
