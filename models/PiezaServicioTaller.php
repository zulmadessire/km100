<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pieza_servicio_taller".
 *
 * @property int $id
 * @property string $nombre
 * @property double $carro
 * @property double $camion
 * @property double $camioneta
 * @property double $furgoneta
 * @property double $suv
 * @property double $van
 * @property double $jeepeta
 * @property double $salvado_leve
 * @property double $salvado_fuerte
 * @property int $id_servicio_taller
 * @property int $estado
 *
 * @property CotizacionTallerActividad[] $cotizacionTallerActividads
 * @property ServicioTaller $servicioTaller
 */
class PiezaServicioTaller extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pieza_servicio_taller';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'id_servicio_taller'], 'required'],
            [['carro', 'camion', 'camioneta', 'furgoneta', 'suv', 'van', 'jeepeta', 'salvado_leve', 'salvado_fuerte'], 'number'],
            [['id_servicio_taller', 'estado'], 'integer'],
            [['nombre'], 'string', 'max' => 200],
            [['id_servicio_taller'], 'exist', 'skipOnError' => true, 'targetClass' => ServicioTaller::className(), 'targetAttribute' => ['id_servicio_taller' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'carro' => 'Automovil',
            'camion' => 'Camion',
            'camioneta' => 'Camioneta',
            'furgoneta' => 'Furgoneta',
            'suv' => 'Suv',
            'van' => 'Microbus',
            'jeepeta' => 'Jeepeta',
            'salvado_leve' => 'Salvado Leve',
            'salvado_fuerte' => 'Salvado Fuerte',
            'id_servicio_taller' => 'Id Servicio Taller',
            'estado' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCotizacionTallerActividads()
    {
        return $this->hasMany(CotizacionTallerActividad::className(), ['id_pieza_servicio_taller' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicioTaller()
    {
        return $this->hasOne(ServicioTaller::className(), ['id' => 'id_servicio_taller']);
    }
}
