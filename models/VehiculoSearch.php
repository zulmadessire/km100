<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Vehiculo;
use yii\db\Query;

/**
 * VehiculoSearch represents the model behind the search form about `app\models\Vehiculo`.
 */
class VehiculoSearch extends Vehiculo
{
    public $id_concesionario, $id_estado,$id_estacion, $id_empresa, $id_cliente, $id_marca, $id_modelo, $empresa, $cliente,$id_marcas,$id_modelos, $mvas,$estado,$proyectado,$actual,$fecha1,$fecha2,$clientes,$empresas, $estatusMantenimiento;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_estacion', 'id_estado' ,'id', 'id_empresa' , 'id_cliente', 'id_categoria', 'id_marca' , 'wizard', 'exactus','actual', 'empresas', 'mvas','clientes' ,'fecha1','fecha2','estado','id_modelo','id_modelos','cliente','empresa','id_marcas','id_concesionario','transito','id_modelo'], 'integer'],
            [['color', 'transmision', 'chasis', 'placa', 'mva', 'clase', 'combustible', 'fecha_compra', 'fecha_inicial', 'km', 'fecha_renovacion_placa', 'fecha_emision_placa', 'estatusMantenimiento'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        
         // $query = Vehiculo::find();

           // $query->andWhere( '(SELECT es.id_estacion FROM flota f INNER JOIN estacion es ON es.id_estacion = f.id_estacion INNER JOIN estacion_usuario eu ON eu.estacion_id=es.id_estacion WHERE eu.user_id=1 )');

        if(\Yii::$app->user->can('admin') || \Yii::$app->user->can('flota todos')){ //Si es ADMIN o tiene permiso FLOTA TODOS trae todos los casos
            $query = Vehiculo::find();
        }
        else if(\Yii::$app->user->can('flota estaciones asociadas')){ //  puede ver las flotas de sus estaciones asociadas
            $query = Vehiculo::find()
                ->join('JOIN','flota','flota.id_vehiculo = vehiculo.id')
                ->join('JOIN','estacion','flota.id_estacion = estacion.id_estacion')
                ->join('JOIN','empresa','estacion.id_empresa = empresa.id_empresa')
                ->join('JOIN','estacion_usuario','estacion_usuario.estacion_id = estacion.id_estacion')
                ->where(['estacion_usuario.user_id' => Yii::$app->user->identity->ID])
                ->orderBy(['flota.id'=> SORT_ASC]);
        }
        else{
            die('Error! No se encontró usuario en roles consultados. Comuniquese con el Administrados del Sistema...');
        }

        if ( isset($params['VehiculoSearch']) ) {

            if ( isset($params['VehiculoSearch']['id_cliente']) AND $params['VehiculoSearch']['id_cliente'] != '') {
                $query->andWhere( '(SELECT emp.cliente_id FROM flota f INNER JOIN estacion es ON es.id_estacion = f.id_estacion INNER JOIN empresa emp ON es.id_empresa = emp.id_empresa WHERE f.id_vehiculo = vehiculo.id ORDER BY f.id DESC LIMIT 1) = '. $params['VehiculoSearch']['id_cliente'] );
            }

            if ( isset($params['VehiculoSearch']['id_estacion']) AND $params['VehiculoSearch']['id_estacion'] != '') {
                $query->where( '(SELECT id_estacion FROM flota WHERE id_vehiculo = vehiculo.id ORDER BY id DESC LIMIT 1) = '.$params['VehiculoSearch']['id_estacion'] );
            }

            if ( isset($params['VehiculoSearch']['id_marca']) AND $params['VehiculoSearch']['id_marca'] != '') {
                $query->andwhere( '(SELECT id_marca FROM modelo WHERE id_modelo = vehiculo.id_modelo LIMIT 1) = '.$params['VehiculoSearch']['id_marca'] );
            }

            if ( isset($params['VehiculoSearch']['id_modelo']) AND $params['VehiculoSearch']['id_modelo'] != '') {
                $query->andwhere( '(SELECT id_modelo FROM modelo WHERE id_modelo = vehiculo.id_modelo LIMIT 1) = '.$params['VehiculoSearch']['id_modelo'] );
            }

            if ( isset($params['VehiculoSearch']['id_empresa']) AND $params['VehiculoSearch']['id_empresa'] != '') {
                $query->andWhere( '(SELECT emp.id_empresa FROM flota f INNER JOIN estacion es ON es.id_estacion = f.id_estacion INNER JOIN empresa emp ON es.id_empresa = emp.id_empresa WHERE f.id_vehiculo = vehiculo.id ORDER BY f.id DESC LIMIT 1) = '. $params['VehiculoSearch']['id_empresa'] );
            }

            if ( isset( $params['VehiculoSearch']['id_estado'] ) AND $params['VehiculoSearch']['id_estado'] != '') {
                $query->andWhere( '(SELECT ev.id_estado FROM estado_vehiculo ev INNER JOIN estado es ON es.id = ev.id_estado AND es.id_tipo_estado = 1  WHERE ev.id_vehiculo = vehiculo.id ORDER BY ev.id DESC LIMIT 1) = '.$params['VehiculoSearch']['id_estado'] );
            }

            if ( isset( $params['VehiculoSearch']['id_concesionario'] ) AND $params['VehiculoSearch']['id_concesionario'] != '') {
                $query->andWhere( '(SELECT id FROM concesionario WHERE id = vehiculo.id_concesionario LIMIT 1) = '.$params['VehiculoSearch']['id_concesionario'] );
            }

        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
             'pagination' => [
                    'pageSize' => 20,
                ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fecha_compra' => $this->fecha_compra,
            'fecha_inicial' => $this->fecha_inicial,
            'fecha_renovacion_placa' => $this->fecha_renovacion_placa,
            'fecha_emision_placa' => $this->fecha_emision_placa,
            'wizard' => $this->wizard,
            'exactus' => $this->exactus,
            'id_modelo' => $this->id_modelo,
            'id_concesionario' => $this->id_concesionario,
            'id_categoria' => $this->id_categoria, 
        ]);

        $query->andFilterWhere(['like', 'color', $this->color])
            ->andFilterWhere(['like', 'transmision', $this->transmision])
            ->andFilterWhere(['like', 'chasis', $this->chasis])
            ->andFilterWhere(['like', 'placa', $this->placa])
            ->andFilterWhere(['like', 'mva', $this->mva])
            ->andFilterWhere(['like', 'clase', $this->clase])
            ->andFilterWhere(['like', 'combustible', $this->combustible])
            ->andFilterWhere(['like', 'km', $this->km]);

        return $dataProvider;
    }

    public function mantenimientosearch($params)
    {
        $query = Vehiculo::find()
        ->join('JOIN','proyeccion_mantenimiento','vehiculo.id = proyeccion_mantenimiento.id_vehiculo')
                ->join('JOIN','mantenimiento_preventivo','mantenimiento_preventivo.id_proyeccion_mantenimiento = proyeccion_mantenimiento.id')
                ->orderBy(['vehiculo.id'=> SORT_ASC]);
      

        //Marca
        if ( isset($params['VehiculoSearch']['id_marca']) AND $params['VehiculoSearch']['id_marca'] != '') {
            $query->andwhere( '(SELECT id_marca FROM modelo WHERE id_modelo = vehiculo.id_modelo LIMIT 1) = '.$params['VehiculoSearch']['id_marca'] );
        }

        //Modelo
        if ( isset($params['VehiculoSearch']['id_modelo']) AND $params['VehiculoSearch']['id_modelo'] != '') {
            $query->andwhere( '(SELECT id_modelo FROM modelo WHERE id_modelo = vehiculo.id_modelo LIMIT 1) = '.$params['VehiculoSearch']['id_modelo'] );
        }

        //Empresa
        if ( isset($params['VehiculoSearch']['id_empresa']) AND $params['VehiculoSearch']['id_empresa'] != '') {
            $query->andWhere( '(SELECT emp.id_empresa FROM flota f INNER JOIN estacion es ON es.id_estacion = f.id_estacion INNER JOIN empresa emp ON es.id_empresa = emp.id_empresa WHERE f.id_vehiculo = vehiculo.id ORDER BY f.id DESC LIMIT 1) = '. $params['VehiculoSearch']['id_empresa'] );
        }

        //Estacion
        if ( isset($params['VehiculoSearch']['id_estacion']) AND $params['VehiculoSearch']['id_estacion'] != '') {
            $query->where( '(SELECT id_estacion FROM flota WHERE id_vehiculo = vehiculo.id ORDER BY id DESC LIMIT 1) = '.$params['VehiculoSearch']['id_estacion'] );
        }

        //Tipo mantenimiento
        if ( isset($params['VehiculoSearch']['id_tipo_mantenimiento']) AND $params['VehiculoSearch']['id_tipo_mantenimiento'] != '') {
            $query->where( '(SELECT p.id_proximo_tipo_mantenimiento FROM proyeccion_mantenimiento p WHERE p.id_vehiculo = vehiculo.id ORDER BY id DESC LIMIT 1) = '.$params['VehiculoSearch']['id_tipo_mantenimiento'] );
        }

        //Estatus
        if ( isset($params['VehiculoSearch']['estado']) AND $params['VehiculoSearch']['estado'] != '') {
            $query->andwhere( '(SELECT ev.id_estado FROM estado_vehiculo ev
                                    INNER JOIN estado es ON es.id = ev.id_estado
                                    WHERE ev.id_vehiculo = vehiculo.id ORDER BY ev.id DESC LIMIT 1) = '.$params['VehiculoSearch']['estado'] );
        }
 
        /*      $query = Vehiculo::find()
            ->join('JOIN','estado_vehiculo','vehiculo.id = estado_vehiculo.id_vehiculo')
            ->join('JOIN','estado','estado_vehiculo.id_estado = estado.id')
            ->where(['estado.nombre' => 'Atrasado'])
            ->orWhere(['estado.nombre' => 'Asignar'])
            ->orWhere(['estado.nombre' => 'Vigente'])
            ->distinct();

        if (isset($params['VehiculoSearch']['fecha1']) AND $params['VehiculoSearch']['fecha1'] != '' AND isset($params['VehiculoSearch']['fecha1']) AND $params['VehiculoSearch']['fecha1'] != '' ) {
            $query->join('JOIN','proyeccion_mantenimiento','vehiculo.id = proyeccion_mantenimiento.id_vehiculo')
            ->join('JOIN','mantenimiento_preventivo','proyeccion_mantenimiento.id = mantenimiento_preventivo.id_proyeccion_mantenimiento')
            ->andwhere(['between', 'mantenimiento_preventivo.fecha_realizacion',  $params['VehiculoSearch']['fecha1'], $params['VehiculoSearch']['fecha2'] ]);
    
        }
        if ( isset($params['VehiculoSearch']['id_marcas']) AND $params['VehiculoSearch']['id_marcas'] != '') {
                $query->andwhere( '(SELECT mar.id_marca FROM vehiculo ve
                                        INNER JOIN modelo mo ON ve.id_modelo = mo.id_modelo 
                                        INNER JOIN marca mar ON mar.id_marca = mo.id_marca
                                        WHERE ve.id = vehiculo.id LIMIT 1) = '.$params['VehiculoSearch']['id_marcas'] ); 
            }

        if ( isset($params['VehiculoSearch']['id_modelos']) AND $params['VehiculoSearch']['id_modelos'] != '') {
                $query->andwhere( '(SELECT mo.id_modelo FROM vehiculo ve 
                                    INNER JOIN modelo mo ON ve.id_modelo = mo.id_modelo 
                                    WHERE ve.id = vehiculo.id LIMIT 1) = '.$params['VehiculoSearch']['id_modelos'] );
            
            }

             if ( isset($params['VehiculoSearch']['mvas']) AND $params['VehiculoSearch']['mvas'] != '') {
                $query->andwhere( '(SELECT ve.mva FROM vehiculo ve
                                    WHERE ve.id = vehiculo.id LIMIT 1) = '."'".strtoupper($params['VehiculoSearch']['mvas'])."'" );
            
            }

                  if ( isset($params['VehiculoSearch']['estado']) AND $params['VehiculoSearch']['estado'] != '') {
                $query->andwhere( '(SELECT es.id FROM vehiculo ve
                                        INNER JOIN estado_vehiculo ev ON ve.id = ev.id_vehiculo 
                                        INNER JOIN estado es ON es.id = ev.id_estado
                                        WHERE ve.id = vehiculo.id LIMIT 1) = '.$params['VehiculoSearch']['estado'] );
            }
                 if ( isset($params['VehiculoSearch']['mantenimiento']) AND $params['VehiculoSearch']['mantenimiento'] != '') {
                $query->andwhere( '(SELECT es.id FROM mantenimiento_preventivo mp
                                        INNER JOIN proyeccion_mantenimiento pm ON mp.id_proyeccion_mantenimiento = pm.id
                                        INNER JOIN vehiculo ve ON ve.id = pm.id_vehiculo 
                                        INNER JOIN estado_vehiculo ev ON ev.id_vehiculo = ve.id 
                                        INNER JOIN estado es ON es.id = ev.id_estado
                                        WHERE ve.id = vehiculo.id LIMIT 1)=9 ');
                 
            }
            if ( isset($params['VehiculoSearch']['atrasado']) AND $params['VehiculoSearch']['atrasado'] != '') {
                $query->andwhere( '(SELECT es.id FROM mantenimiento_preventivo mp
                                        INNER JOIN proyeccion_mantenimiento pm ON mp.id_proyeccion_mantenimiento = pm.id
                                        INNER JOIN vehiculo ve ON ve.id = pm.id_vehiculo 
                                        INNER JOIN estado_vehiculo ev ON ev.id_vehiculo = ve.id 
                                        INNER JOIN estado es ON es.id = ev.id_estado
                                        WHERE ve.id = vehiculo.id LIMIT 1)=8 ');
                 
            }

                       if ( isset($params['VehiculoSearch']['clientes']) AND $params['VehiculoSearch']['clientes'] != '') {
                $query->andwhere( '(SELECT cli.id FROM vehiculo ve
                                        INNER JOIN flota f ON ve.id = f.id_vehiculo 
                                        INNER JOIN estacion es ON es.id_estacion = f.id_estacion
                                        INNER JOIN empresa em ON em.id_empresa = es.id_empresa
                                        INNER JOIN cliente cli ON cli.id = em.cliente_id
                                        WHERE ve.id = vehiculo.id LIMIT 1) = '.$params['VehiculoSearch']['clientes'] );
                 
                 
           
                 
            }
               if ( isset($params['VehiculoSearch']['empresas']) AND $params['VehiculoSearch']['empresas'] != '') {
                $query->andwhere( '(SELECT em.id_empresa FROM vehiculo ve
                                        INNER JOIN flota f ON ve.id = f.id_vehiculo 
                                        INNER JOIN estacion es ON es.id_estacion = f.id_estacion
                                        INNER JOIN empresa em ON em.id_empresa = es.id_empresa
                                        WHERE ve.id = vehiculo.id LIMIT 1) = '.$params['VehiculoSearch']['empresas'] );
                 
                 
           
                 
            }*/
         
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
             'pagination' => [
                    'pageSize' => 20,
                ],
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fecha_compra' => $this->fecha_compra,
            'fecha_inicial' => $this->fecha_inicial,
            'fecha_renovacion_placa' => $this->fecha_renovacion_placa,
            'fecha_emision_placa' => $this->fecha_emision_placa,
            'wizard' => $this->wizard,
            'exactus' => $this->exactus,
            'id_modelo' => $this->id_modelo,
            'id_concesionario' => $this->id_concesionario,
            'id_categoria' => $this->id_categoria, 
        ]);

           $query->andFilterWhere(['like', 'color', $this->color])
            ->andFilterWhere(['like', 'transmision', $this->transmision])
            ->andFilterWhere(['like', 'chasis', $this->chasis])
            ->andFilterWhere(['like', 'placa', $this->placa])
            ->andFilterWhere(['like', 'mva', $this->mva])
            ->andFilterWhere(['like', 'clase', $this->clase])
            ->andFilterWhere(['like', 'combustible', $this->combustible])
            ->andFilterWhere(['like', 'km', $this->km]);


        return $dataProvider;
    }
       public function proximosearch($params)
    {
             
            
        if ( isset($params['VehiculoSearch']['id_marcas']) AND $params['VehiculoSearch']['id_marcas'] != '') {
                $query->andwhere( '(SELECT mar.id_marca FROM mantenimiento_preventivo mp
                                        INNER JOIN proyeccion_mantenimiento pm ON mp.id_proyeccion_mantenimiento = pm.id
                                        INNER JOIN vehiculo ve ON ve.id = pm.id_vehiculo 
                                        INNER JOIN modelo mo ON ve.id_modelo = mo.id_modelo 
                                        INNER JOIN marca mar ON mar.id_marca = mo.id_marca
                                        WHERE pm.id_vehiculo = flota.id_vehiculo LIMIT 1) = '.$params['VehiculoSearch']['id_marcas'] );
                 
                 
           
                 
            }
             if ( isset($params['VehiculoSearch']['proyectado']) AND $params['VehiculoSearch']['proyectado'] != '') {
                $query->andwhere( '(SELECT pm.km_proyectado FROM mantenimiento_preventivo mp
                                        INNER JOIN proyeccion_mantenimiento pm ON mp.id_proyeccion_mantenimiento = pm.id
                                        WHERE pm.id_vehiculo = flota.id_vehiculo LIMIT 1) = '.$params['VehiculoSearch']['proyectado'] );
     
            }
            if ( isset($params['VehiculoSearch']['actual']) AND $params['VehiculoSearch']['actual'] != '') {
                $query->andwhere( '(SELECT ve.km FROM mantenimiento_preventivo mp
                                        INNER JOIN proyeccion_mantenimiento pm ON mp.id_proyeccion_mantenimiento = pm.id
                                        INNER JOIN vehiculo ve ON ve.id = pm.id_vehiculo 
                                         WHERE pm.id_vehiculo = flota.id_vehiculo LIMIT 1) = '.$params['VehiculoSearch']['actual'] );
     
            }
            if ( isset($params['VehiculoSearch']['estado']) AND $params['VehiculoSearch']['estado'] != '') {
                $query->andwhere( '(SELECT es.id FROM mantenimiento_preventivo mp
                                        INNER JOIN proyeccion_mantenimiento pm ON mp.id_proyeccion_mantenimiento = pm.id
                                        INNER JOIN vehiculo ve ON ve.id = pm.id_vehiculo 
                                        INNER JOIN estado_vehiculo ev ON ve.id = ev.id_vehiculo 
                                        INNER JOIN estado es ON es.id = ev.id_estado
                                        WHERE pm.id_vehiculo = flota.id_vehiculo LIMIT 1) = '.$params['VehiculoSearch']['estado'] );
                 
                 
           
                 
            }
  if ( isset($params['VehiculoSearch']['id_modelos']) AND $params['VehiculoSearch']['id_modelos'] != '') {
                $query->andwhere( '(SELECT mo.id_modelo FROM mantenimiento_preventivo mp INNER JOIN proyeccion_mantenimiento pm ON mp.id_proyeccion_mantenimiento = pm.id INNER JOIN vehiculo ve ON ve.id = pm.id_vehiculo INNER JOIN modelo mo ON ve.id_modelo = mo.id_modelo WHERE WHERE pm.id_vehiculo = flota.id_vehiculo LIMIT 1) = '.$params['VehiculoSearch']['id_modelos'] );
            
            }

        if ( isset($params['VehiculoSearch']['mvas']) AND $params['VehiculoSearch']['mvas'] != '') {
                $query->andwhere( '(SELECT ve.mva FROM mantenimiento_preventivo mp INNER JOIN proyeccion_mantenimiento pm ON mp.id_proyeccion_mantenimiento = pm.id INNER JOIN vehiculo ve ON ve.id = pm.id_vehiculo WHERE WHERE pm.id_vehiculo = flota.id_vehiculo LIMIT 1) like '."'%".$params['VehiculoSearch']['mvas']."%'"  ); 
            
            }

         if ( isset($params['VehiculoSearch']['mantenimiento']) AND $params['VehiculoSearch']['mantenimiento'] != '') {
                $query->andwhere( '(SELECT es.id FROM mantenimiento_preventivo mp
                                        INNER JOIN proyeccion_mantenimiento pm ON mp.id_proyeccion_mantenimiento = pm.id
                                        INNER JOIN vehiculo ve ON ve.id = pm.id_vehiculo 
                                        INNER JOIN estado_vehiculo ev ON ev.id_vehiculo = ve.id 
                                        INNER JOIN estado es ON es.id = ev.id_estado
                                       WHERE pm.id_vehiculo = flota.id_vehiculo LIMIT 1)=9 ');
                 
            }
              if ( isset($params['VehiculoSearch']['atrasado']) AND $params['VehiculoSearch']['atrasado'] != '') {
                $query->andwhere( '(SELECT es.id FROM mantenimiento_preventivo mp
                                        INNER JOIN proyeccion_mantenimiento pm ON mp.id_proyeccion_mantenimiento = pm.id
                                        INNER JOIN vehiculo ve ON ve.id = pm.id_vehiculo 
                                        INNER JOIN estado_vehiculo ev ON ev.id_vehiculo = ve.id 
                                        INNER JOIN estado es ON es.id = ev.id_estado
                                        WHERE pm.id_vehiculo = flota.id_vehiculo LIMIT 1)=8 ');
                 
            }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
             'pagination' => [
                    'pageSize' => 20,
                ],
        ]);


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fecha_compra' => $this->fecha_compra,
            'fecha_inicial' => $this->fecha_inicial,
            'fecha_renovacion_placa' => $this->fecha_renovacion_placa,
            'fecha_emision_placa' => $this->fecha_emision_placa,
            'wizard' => $this->wizard,
            'exactus' => $this->exactus,
            'id_modelo' => $this->id_modelo,
            'id_concesionario' => $this->id_concesionario,
            'id_categoria' => $this->id_categoria, 
        ]);

           $query->andFilterWhere(['like', 'color', $this->color])
            ->andFilterWhere(['like', 'transmision', $this->transmision])
            ->andFilterWhere(['like', 'chasis', $this->chasis])
            ->andFilterWhere(['like', 'placa', $this->placa])
            ->andFilterWhere(['like', 'mva', $this->mva])
            ->andFilterWhere(['like', 'clase', $this->clase])
            ->andFilterWhere(['like', 'combustible', $this->combustible])
            ->andFilterWhere(['like', 'km', $this->km]);


        return $dataProvider;
    }



      public function fuerasearch($params,$id_estacion)
    {

             $query = Vehiculo::find()
                ->join('JOIN','solicitud_compra_estacion','solicitud_compra_estacion.id_vehiculo = vehiculo.id')
                ->join('JOIN','solicitud_compra_pieza','solicitud_compra_pieza.id_solicitud_compra_estacion = solicitud_compra_estacion.id')
                ->where('solicitud_compra_pieza.codigo_estado<6')
                ->andWhere('solicitud_compra_pieza.origen_solicitud=2')
                ->andWhere('solicitud_compra_estacion.id_estacion='.$id_estacion)
                ->orderBy('vehiculo.id ASC');

      
            
        if ( isset($params['VehiculoSearch']['id_marcas']) AND $params['VehiculoSearch']['id_marcas'] != '') {
                $query->andwhere( '(SELECT mar.id_marca FROM mantenimiento_preventivo mp
                                        INNER JOIN proyeccion_mantenimiento pm ON mp.id_proyeccion_mantenimiento = pm.id
                                        INNER JOIN vehiculo ve ON ve.id = pm.id_vehiculo 
                                        INNER JOIN modelo mo ON ve.id_modelo = mo.id_modelo 
                                        INNER JOIN marca mar ON mar.id_marca = mo.id_marca
                                        WHERE pm.id_vehiculo = flota.id_vehiculo LIMIT 1) = '.$params['VehiculoSearch']['id_marcas'] );
                 
                 
           
                 
            }
             if ( isset($params['VehiculoSearch']['proyectado']) AND $params['VehiculoSearch']['proyectado'] != '') {
                $query->andwhere( '(SELECT pm.km_proyectado FROM mantenimiento_preventivo mp
                                        INNER JOIN proyeccion_mantenimiento pm ON mp.id_proyeccion_mantenimiento = pm.id
                                        WHERE pm.id_vehiculo = flota.id_vehiculo LIMIT 1) = '.$params['VehiculoSearch']['proyectado'] );
     
            }
            if ( isset($params['VehiculoSearch']['actual']) AND $params['VehiculoSearch']['actual'] != '') {
                $query->andwhere( '(SELECT ve.km FROM mantenimiento_preventivo mp
                                        INNER JOIN proyeccion_mantenimiento pm ON mp.id_proyeccion_mantenimiento = pm.id
                                        INNER JOIN vehiculo ve ON ve.id = pm.id_vehiculo 
                                         WHERE pm.id_vehiculo = flota.id_vehiculo LIMIT 1) = '.$params['VehiculoSearch']['actual'] );
     
            }
            if ( isset($params['VehiculoSearch']['estado']) AND $params['VehiculoSearch']['estado'] != '') {
                $query->andwhere( '(SELECT es.id FROM mantenimiento_preventivo mp
                                        INNER JOIN proyeccion_mantenimiento pm ON mp.id_proyeccion_mantenimiento = pm.id
                                        INNER JOIN vehiculo ve ON ve.id = pm.id_vehiculo 
                                        INNER JOIN estado_vehiculo ev ON ve.id = ev.id_vehiculo 
                                        INNER JOIN estado es ON es.id = ev.id_estado
                                        WHERE pm.id_vehiculo = flota.id_vehiculo LIMIT 1) = '.$params['VehiculoSearch']['estado'] );
                 
                 
           
                 
            }
  if ( isset($params['VehiculoSearch']['id_modelos']) AND $params['VehiculoSearch']['id_modelos'] != '') {
                $query->andwhere( '(SELECT mo.id_modelo FROM mantenimiento_preventivo mp INNER JOIN proyeccion_mantenimiento pm ON mp.id_proyeccion_mantenimiento = pm.id INNER JOIN vehiculo ve ON ve.id = pm.id_vehiculo INNER JOIN modelo mo ON ve.id_modelo = mo.id_modelo WHERE WHERE pm.id_vehiculo = flota.id_vehiculo LIMIT 1) = '.$params['VehiculoSearch']['id_modelos'] );
            
            }

        if ( isset($params['VehiculoSearch']['mvas']) AND $params['VehiculoSearch']['mvas'] != '') {
                $query->andwhere( '(SELECT ve.mva FROM mantenimiento_preventivo mp INNER JOIN proyeccion_mantenimiento pm ON mp.id_proyeccion_mantenimiento = pm.id INNER JOIN vehiculo ve ON ve.id = pm.id_vehiculo WHERE WHERE pm.id_vehiculo = flota.id_vehiculo LIMIT 1) like '."'%".$params['VehiculoSearch']['mvas']."%'"  ); 
            
            }

         if ( isset($params['VehiculoSearch']['mantenimiento']) AND $params['VehiculoSearch']['mantenimiento'] != '') {
                $query->andwhere( '(SELECT es.id FROM mantenimiento_preventivo mp
                                        INNER JOIN proyeccion_mantenimiento pm ON mp.id_proyeccion_mantenimiento = pm.id
                                        INNER JOIN vehiculo ve ON ve.id = pm.id_vehiculo 
                                        INNER JOIN estado_vehiculo ev ON ev.id_vehiculo = ve.id 
                                        INNER JOIN estado es ON es.id = ev.id_estado
                                       WHERE pm.id_vehiculo = flota.id_vehiculo LIMIT 1)=9 ');
                 
            }
              if ( isset($params['VehiculoSearch']['atrasado']) AND $params['VehiculoSearch']['atrasado'] != '') {
                $query->andwhere( '(SELECT es.id FROM mantenimiento_preventivo mp
                                        INNER JOIN proyeccion_mantenimiento pm ON mp.id_proyeccion_mantenimiento = pm.id
                                        INNER JOIN vehiculo ve ON ve.id = pm.id_vehiculo 
                                        INNER JOIN estado_vehiculo ev ON ev.id_vehiculo = ve.id 
                                        INNER JOIN estado es ON es.id = ev.id_estado
                                        WHERE pm.id_vehiculo = flota.id_vehiculo LIMIT 1)=8 ');
                 
            }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
             'pagination' => [
                    'pageSize' => 20,
                ],
        ]);
 

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fecha_compra' => $this->fecha_compra,
            'fecha_inicial' => $this->fecha_inicial,
            'fecha_renovacion_placa' => $this->fecha_renovacion_placa,
            'fecha_emision_placa' => $this->fecha_emision_placa,
            'wizard' => $this->wizard,
            'exactus' => $this->exactus,
            'id_modelo' => $this->id_modelo,
            'id_concesionario' => $this->id_concesionario,
            'id_categoria' => $this->id_categoria, 
        ]);

           $query->andFilterWhere(['like', 'color', $this->color])
            ->andFilterWhere(['like', 'transmision', $this->transmision])
            ->andFilterWhere(['like', 'chasis', $this->chasis])
            ->andFilterWhere(['like', 'placa', $this->placa])
            ->andFilterWhere(['like', 'mva', $this->mva])
            ->andFilterWhere(['like', 'clase', $this->clase])
            ->andFilterWhere(['like', 'combustible', $this->combustible])
            ->andFilterWhere(['like', 'km', $this->km]);


        return $dataProvider;
    }
}
