<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "aspectos_evaluar".
 *
 * @property integer $id
 * @property integer $respuesta
 * @property integer $id_aspecto
 * @property integer $id_auditoria_ejec_mant
 *
 * @property Aspectos $idAspecto
 * @property AuditoriaEjecMant $idAuditoriaEjecMant
 */
class AspectosEvaluar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'aspectos_evaluar';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['respuesta', 'id_aspecto', 'id_auditoria_ejec_mant'], 'integer'],
            [['id_aspecto', 'id_auditoria_ejec_mant'], 'required'],
            [['id_aspecto'], 'exist', 'skipOnError' => true, 'targetClass' => Aspectos::className(), 'targetAttribute' => ['id_aspecto' => 'id']],
            [['id_auditoria_ejec_mant'], 'exist', 'skipOnError' => true, 'targetClass' => AuditoriaEjecMant::className(), 'targetAttribute' => ['id_auditoria_ejec_mant' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'respuesta' => 'Respuesta',
            'id_aspecto' => 'Id Aspecto',
            'id_auditoria_ejec_mant' => 'Id Auditoria Ejec Mant',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAspecto()
    {
        return $this->hasOne(Aspectos::className(), ['id' => 'id_aspecto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAuditoriaEjecMant()
    {
        return $this->hasOne(AuditoriaEjecMant::className(), ['id' => 'id_auditoria_ejec_mant']);
    }
}
