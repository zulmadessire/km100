<?php  
 
namespace app\models;
 
use yii\web\UploadedFile;

class transito extends Model
{
    /**
     * @var UploadedFile
     */
    public $file;

    public function rules()
    {
        return [
            [
                ['file'], 'file', 'skipOnEmpty' => false, 
                'extensions' => 'png, jpg, csv, pdf , xlsx',
                'checkExtensionByMimeType' => false,
 
            ],
        ];
    }
    
    public function upload()
    {
        if ($this->validate()) {
            $this->file->saveAs( './files/transito/transito-'.$this->file->baseName . '.' . $this->file->extension);
            return true;
        } else {
            return false;
        }
    }
}
?>