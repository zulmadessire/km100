<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Despacho;

/**
 * DespachoSearch represents the model behind the search form about `app\models\Despacho`.
 */
class DespachoSearch extends Despacho
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'numero', 'estado', 'estado_recepcion_taller', 'id_solicitud_compra_pieza'], 'integer'],
            [['fecha', 'traslado'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Despacho::find();

        // add conditions that should always apply here

         $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'numero' => $this->numero,
            'estado' => $this->estado,
            'fecha' => $this->fecha,
            'estado_recepcion_taller' => $this->estado_recepcion_taller,
            'id_solicitud_compra_pieza' => $this->id_solicitud_compra_pieza,
        ]);

        $query->andFilterWhere(['like', 'traslado', $this->traslado]);

        return $dataProvider;
    }


      public function search2($params,$id)
    {
        $query = Despacho::find()

        ->join('JOIN','despacho_proveedor_pieza','despacho_proveedor_pieza.id_despacho = despacho.id')
        ->join('JOIN','cotizacion_proveedor_pieza','cotizacion_proveedor_pieza.id = despacho_proveedor_pieza.id_cotizacion_proveedor_pieza')
        ->join('JOIN','cotizacion_taller_pieza','cotizacion_proveedor_pieza.id_cotizacion_taller_pieza = cotizacion_taller_pieza.id')
        ->join('JOIN','pieza','cotizacion_taller_pieza.id_pieza = pieza.id_pieza')
        ->where('despacho_proveedor_pieza.id_despacho='.$id);

       


        // add conditions that should always apply here

         $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'numero' => $this->numero,
            'estado' => $this->estado,
            'fecha' => $this->fecha,
            'estado_recepcion_taller' => $this->estado_recepcion_taller,
            'id_solicitud_compra_pieza' => $this->id_solicitud_compra_pieza,
        ]);

        $query->andFilterWhere(['like', 'traslado', $this->traslado]);

        return $dataProvider;
    }
}
