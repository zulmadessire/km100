<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orden_compra_pieza".
 *
 * @property integer $id
 * @property string $fecha
 * @property integer $estado
 * @property string $observaciones
 * @property double $total
 * @property integer $id_solicitud_compra_pieza
 * @property integer $id_cotizacion_proveedor
 * @property integer $id_proveedor
 * @property string $observacion_recepcion
 *
 * @property OrdenCompraFactura[] $ordenCompraFacturas
 * @property CotizacionProveedor $idCotizacionProveedor
 */
class OrdenCompraPieza extends \yii\db\ActiveRecord
{
    public $pro, $totales;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orden_compra_pieza';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['estado', 'id_cotizacion_proveedor'], 'integer'],
            [['observaciones'], 'string', 'max' => 450],
            [['estado', 'id_solicitud_compra_pieza', 'id_cotizacion_proveedor', 'id_proveedor'], 'integer'],
            [['observaciones', 'observacion_recepcion'], 'string'],
            [['total'], 'number'],
            [['id_cotizacion_proveedor', 'id_proveedor'], 'required'],
            [['id_cotizacion_proveedor'], 'exist', 'skipOnError' => true, 'targetClass' => CotizacionProveedor::className(), 'targetAttribute' => ['id_cotizacion_proveedor' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha' => 'Fecha',
            'estado' => 'Estado',
            'observaciones' => 'Observaciones',
            'id_cotizacion_proveedor' => 'Id Cotizacion Proveedor',
            'total' => 'Total',
            'id_solicitud_compra_pieza' => 'Id Solicitud Compra Pieza',
            'id_cotizacion_proveedor' => 'Id Cotizacion Proveedor',
            'id_proveedor' => 'Id Proveedor',
            'observacion_recepcion' => 'Observacion Recepcion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdenCompraFacturas()
    {
        return $this->hasMany(OrdenCompraFactura::className(), ['id_orden_compra_pieza' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCotizacionProveedor()
    {
        return $this->hasOne(CotizacionProveedor::className(), ['id' => 'id_cotizacion_proveedor']);
    }
}
