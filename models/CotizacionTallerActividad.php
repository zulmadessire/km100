<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cotizacion_taller_actividad".
 *
 * @property int $id
 * @property int $id_pieza_servicio_taller
 * @property int $labor
 * @property double $precio
 * @property double $total_servicio
 * @property int $id_cotizacion_taller
 * @property int $conforme_recepcion
 * @property int $id_vehiculo
 * @property int $tipo_servicio_taller
 * @property double $decimal 
 *
 * @property CotizacionTaller $cotizacionTaller
 * @property PiezaServicioTaller $piezaServicioTaller
 */
class CotizacionTallerActividad extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cotizacion_taller_actividad';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pieza_servicio_taller', 'id_cotizacion_taller', 'labor', 'tipo_servicio_taller'], 'required'],
            [['id_pieza_servicio_taller', 'labor', 'id_cotizacion_taller', 'conforme_recepcion', 'id_vehiculo', 'tipo_servicio_taller'], 'integer'],
            [['precio', 'total_servicio', 'decimal'], 'number'],
            [['id_cotizacion_taller'], 'exist', 'skipOnError' => true, 'targetClass' => CotizacionTaller::className(), 'targetAttribute' => ['id_cotizacion_taller' => 'id']],
            [['id_pieza_servicio_taller'], 'exist', 'skipOnError' => true, 'targetClass' => PiezaServicioTaller::className(), 'targetAttribute' => ['id_pieza_servicio_taller' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_pieza_servicio_taller' => 'Pieza',
            'labor' => 'Labor',
            'precio' => 'Precio',
            'total_servicio' => 'Total Servicio',
            'id_cotizacion_taller' => 'Id Cotizacion Taller',
            'conforme_recepcion' => 'Conforme Recepcion',
            'id_vehiculo' => 'Id Vehiculo',
            'tipo_servicio_taller' => 'Servicio',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCotizacionTaller()
    {
        return $this->hasOne(CotizacionTaller::className(), ['id' => 'id_cotizacion_taller']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPiezaServicioTaller()
    {
        return $this->hasOne(PiezaServicioTaller::className(), ['id' => 'id_pieza_servicio_taller']);
    }

    public static function getSubTotal($provider)
    {
        $subtotal = 0;

        foreach ($provider as $item) {
            $subtotal += ($item['precio']*$item['cantidad']);
        }

        return $subtotal;
    }
       public static function getSubTotal2($provider)
    {
        $subtotal = 0;

        foreach ($provider as $item) {
            $subtotal += ($item['total_servicio']);
        }

        return $subtotal;
    }

    public static function getItbis($provider)
    {
        $subtotal = 0;

        foreach ($provider as $item) {
            $subtotal += ($item['precio']*$item['cantidad']);
        }

        return ($subtotal * 0.18);
    }
    public static function getItbis2($provider)
    {
        $subtotal = 0;

        foreach ($provider as $item) {
            $subtotal += ($item['total_servicio']);
        }

        return ($subtotal * 0.18);
    }

    public static function getTotal($provider)
    {
        $subtotal = 0;
        $total = 0;

        foreach ($provider as $item) {
            $subtotal += ($item['precio']*$item['cantidad']);
        }

        $itbis = ($subtotal * 0.18);
        $total = $subtotal + $itbis;
        return $total;
    }
     public static function getTotal2($provider)
    {
        $subtotal = 0;
        $total = 0;

        foreach ($provider as $item) {
            $subtotal += ($item['total_servicio']);
        }

        $itbis = ($subtotal * 0.18);
        $total = $subtotal + $itbis;
        return $total;
    }
}
