<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Auditoriaejecmant;

/**
 * AuditoriaejecmantSearch represents the model behind the search form about `app\models\Auditoriaejecmant`.
 */
class AuditoriaejecmantSearch extends Auditoriaejecmant
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_tipo_mnto', 'estado', 'id_estacion'], 'integer'],
            [['tecnico', 'mva_vehiculo', 'fecha'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Auditoriaejecmant::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_tipo_mnto' => $this->id_tipo_mnto,
            'estado' => $this->estado,
            'fecha' => $this->fecha,
            'id_estacion' => $this->id_estacion,
        ]);

        $query->andFilterWhere(['like', 'tecnico', $this->tecnico])
            ->andFilterWhere(['like', 'mva_vehiculo', $this->mva_vehiculo]);

        return $dataProvider;
    }
}
