<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipo_pieza".
 *
 * @property integer $id
 * @property string $nombre
 *
 * @property MarcaInsumo[] $marcaInsumos
 * @property Pieza[] $piezas
 */
class TipoPieza extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipo_pieza';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Tipo de pieza',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarcaInsumos()
    {
        return $this->hasMany(MarcaInsumo::className(), ['tipo_pieza_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPiezas()
    {
        return $this->hasMany(Pieza::className(), ['tipo_pieza_id' => 'id']);
    }
}
