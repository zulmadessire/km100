<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "auditoria".
 *
 * @property integer $id
 * @property string $motivo
 * @property string $planaccion
 * @property string $fecha
 * @property integer $estacion_id_estacion
 * @property integer $estado
 * @property string $observacion
 *
 * @property Estacion $estacionIdEstacion
 * @property FueraServicio[] $fueraServicios
 * @property Solicitudes[] $solicitudes
 */
class Auditoria extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auditoria';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['estacion_id_estacion'], 'required'],
            [['estacion_id_estacion', 'estado'], 'integer'],
            [['motivo', 'planaccion', 'fecha'], 'string', 'max' => 200],
            [['observacion'], 'string', 'max' => 500],
            [['estacion_id_estacion'], 'exist', 'skipOnError' => true, 'targetClass' => Estacion::className(), 'targetAttribute' => ['estacion_id_estacion' => 'id_estacion']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'motivo' => 'Motivo',
            'planaccion' => 'Planaccion',
            'fecha' => 'Fecha',
            'estacion_id_estacion' => 'Estacion Id Estacion',
            'estado' => 'Estado',
            'observacion' => 'Observacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstacionIdEstacion()
    {
        return $this->hasOne(Estacion::className(), ['id_estacion' => 'estacion_id_estacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFueraServicios()
    {
        return $this->hasMany(FueraServicio::className(), ['id_auditoria' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSolicitudes()
    {
        return $this->hasMany(Solicitudes::className(), ['id_auditoria' => 'id']);
    }
}
