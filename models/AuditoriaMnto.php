<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "auditoria_mnto".
 *
 * @property integer $id
 * @property integer $desviacion
 * @property integer $mntos_realizados
 * @property integer $mntos_smna_anterior
 * @property string $fecha
 * @property string $observacion
 * @property integer $estado
 * @property integer $id_estacion
 *
 * @property AddMnto[] $addMntos
 * @property Estacion $idEstacion
 * @property SolAudMnto[] $solAudMntos
 */
class AuditoriaMnto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auditoria_mnto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['desviacion', 'mntos_realizados', 'mntos_smna_anterior','mensual', 'estado', 'id_estacion'], 'integer'],
            [['fecha'], 'safe'],
            [['id_estacion'], 'required'],
            [['observacion'], 'string', 'max' => 1000],
            [['id_estacion'], 'exist', 'skipOnError' => true, 'targetClass' => Estacion::className(), 'targetAttribute' => ['id_estacion' => 'id_estacion']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'desviacion' => 'Desviacion',
            'mntos_realizados' => 'Mntos Realizados',
            'mntos_smna_anterior' => 'Mntos Smna Anterior',
            'fecha' => 'Fecha',
            'observacion' => 'Observacion',
            'estado' => 'Estado',
            'id_estacion' => 'Id Estacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddMntos()
    {
        return $this->hasMany(AddMnto::className(), ['id_aud_mnto' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstacion()
    {
        return $this->hasOne(Estacion::className(), ['id_estacion' => 'id_estacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSolAudMntos()
    {
        return $this->hasMany(SolAudMnto::className(), ['id_aud_mnto' => 'id']);
    }
}
