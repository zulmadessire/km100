<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "caso_servicio".
 *
 * @property integer $id
 * @property integer $id_servicio
 * @property integer $id_caso
 *
 * @property Caso $idCaso
 * @property Servicio $idServicio
 */
class CasoServicio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'caso_servicio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_servicio', 'id_caso'], 'required'],
            [['id_servicio', 'id_caso'], 'integer'],
            [['id_caso'], 'exist', 'skipOnError' => true, 'targetClass' => Caso::className(), 'targetAttribute' => ['id_caso' => 'id_caso']],
            [['id_servicio'], 'exist', 'skipOnError' => true, 'targetClass' => Servicio::className(), 'targetAttribute' => ['id_servicio' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_servicio' => 'Id Servicio',
            'id_caso' => 'Id Caso',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCaso()
    {
        return $this->hasOne(Caso::className(), ['id_caso' => 'id_caso']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdServicio()
    {
        return $this->hasOne(Servicio::className(), ['id' => 'id_servicio']);
    }
}
