<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Proveedor;

/**
 * ProveedorSearch represents the model behind the search form about `app\models\proveedor`.
 */
class ProveedorSearch extends proveedor
{
    public $id_pieza_insumos;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tipo_empresa_id', 'plazo_pago','id_pieza_insumos'], 'integer'],
            [['tipo_persona', 'identificacion', 'direccion', 'descripcion', 'telefono', 'nombre_representante', 'identificacion_representante', 'puesto_representante', 'correo_representante', 'celular_representante', 'telefono_representante', 'garantia', 'domicilio', 'nombre', 'Tipo_pago'], 'safe'],
            [['credito', 'porcentaje'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = proveedor::find();

        // add conditions that should always apply here

       if ( isset( $params['ProveedorSearch']['id_pieza'] ) AND $params['ProveedorSearch']['id_pieza'] != '') {
            
       $query->andWhere( '(SELECT p.id_pieza FROM proveedor_pieza pp INNER JOIN pieza p ON pp.pieza_id = p.id_pieza AND p.id_pieza = '.$params['ProveedorSearch']['id_pieza'].' WHERE pp.proveedor_id = proveedor.id LIMIT 1) = '.$params['ProveedorSearch']['id_pieza'] );
       
        }
          if ( isset( $params['ProveedorSearch']['id_marca'] ) AND $params['ProveedorSearch']['id_marca'] != '') {
            $query->andWhere( '(SELECT ts.id_marca FROM marca_proveedor st INNER JOIN marca ts ON st.id_marca = ts.id_marca AND ts.id_marca = '.$params['ProveedorSearch']['id_marca'].' WHERE st.id_proveedor = proveedor.id LIMIT 1) = '.$params['ProveedorSearch']['id_marca'] );
 
        }
        if ( isset( $params['ProveedorSearch']['id'] ) AND $params['ProveedorSearch']['id'] != '') {
            
       $query->andWhere( '(SELECT mi.id FROM proveedor_pieza pp INNER JOIN marca_insumo mi ON pp.marca_insumo_id = mi.id AND mi.id = '.$params['ProveedorSearch']['id'].' WHERE pp.proveedor_id = proveedor.id LIMIT 1) = '.$params['ProveedorSearch']['id'] );
       
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'credito' => $this->credito,
            'porcentaje' => $this->porcentaje,
            'tipo_empresa_id' => $this->tipo_empresa_id,
            'plazo_pago' => $this->plazo_pago,
        ]);

        $query->andFilterWhere(['like', 'tipo_persona', $this->tipo_persona])
            ->andFilterWhere(['like', 'identificacion', $this->identificacion])
            ->andFilterWhere(['like', 'direccion', $this->direccion])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion])
            ->andFilterWhere(['like', 'telefono', $this->telefono])
            ->andFilterWhere(['like', 'nombre_representante', $this->nombre_representante])
            ->andFilterWhere(['like', 'identificacion_representante', $this->identificacion_representante])
            ->andFilterWhere(['like', 'puesto_representante', $this->puesto_representante])
            ->andFilterWhere(['like', 'correo_representante', $this->correo_representante])
            ->andFilterWhere(['like', 'celular_representante', $this->celular_representante])
            ->andFilterWhere(['like', 'telefono_representante', $this->telefono_representante])
            ->andFilterWhere(['like', 'garantia', $this->garantia])
            ->andFilterWhere(['like', 'domicilio', $this->domicilio])
            ->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'Tipo_pago', $this->Tipo_pago]);

        return $dataProvider;
    }
}
