<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "capacitaciones".
 *
 * @property integer $id
 * @property string $tipo_capacitacion
 * @property string $numero
 * @property string $id_licitacion
 */
class Capacitaciones extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'capacitaciones';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'id_licitacion'], 'required'],
            [['tipo_capacitacion', 'id_licitacion'], 'string', 'max' => 100],
            [['numero'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tipo_capacitacion' => 'Tipo Capacitacion',
            'numero' => 'Numero',
            'id_licitacion' => 'Id Licitacion',
        ];
    }
}
