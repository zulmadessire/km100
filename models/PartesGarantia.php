<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "partes_garantia".
 *
 * @property int $id
 * @property string $kilometraje
 * @property string $tiempo
 * @property int $partes_id
 * @property int $detalle_garantia_id
 * @property int $periodo 0-Dias, 1-Meses, 2-Años
 *
 * @property DetalleGarantia $detalleGarantia
 * @property Partes $partes
 */
class PartesGarantia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'partes_garantia';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partes_id', 'detalle_garantia_id'], 'required'],
            [['partes_id', 'detalle_garantia_id', 'periodo'], 'integer'],
            [['kilometraje', 'tiempo'], 'string', 'max' => 45],
            [['detalle_garantia_id'], 'exist', 'skipOnError' => true, 'targetClass' => DetalleGarantia::className(), 'targetAttribute' => ['detalle_garantia_id' => 'id']],
            [['partes_id'], 'exist', 'skipOnError' => true, 'targetClass' => Partes::className(), 'targetAttribute' => ['partes_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kilometraje' => 'Kilometraje',
            'tiempo' => 'Tiempo',
            'partes_id' => 'Partes ID',
            'detalle_garantia_id' => 'Detalle Garantia ID',
            'periodo' => 'Periodo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetalleGarantia()
    {
        return $this->hasOne(DetalleGarantia::className(), ['id' => 'detalle_garantia_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartes()
    {
        return $this->hasOne(Partes::className(), ['id' => 'partes_id']);
    }
}
