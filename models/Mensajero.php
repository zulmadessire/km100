<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mensajero".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $cedula
 * @property string $telefono
 * @property string $direccion
 * @property string $placa
 * @property string $anho
 * @property integer $id_modelo_id
 * @property string $user
 *
 * @property Modelo $idModelo
 */
class Mensajero extends \yii\db\ActiveRecord
{
    public $marca;
        /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mensajero';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'user', 'correo'], 'required'],
            [['id_modelo_id', 'marca'], 'integer'],
            [['nombre', 'user'], 'string', 'max' => 200],
            [['correo'],'email'],
            [['cedula', 'placa', 'anho'], 'string', 'max' => 20],
            [['telefono'], 'string', 'max' => 40],
            [['direccion'], 'string', 'max' => 450],
            [['id_modelo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Modelo::className(), 'targetAttribute' => ['id_modelo_id' => 'id_modelo']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'cedula' => 'Cédula',
            'telefono' => 'Teléfono',
            'direccion' => 'Dirección',
            'correo' => 'Correo',
            'placa' => 'Placa',
            'anho' => 'Año',
            'id_modelo_id' => 'Id Modelo ID',
            'user' => 'Usuario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdModelo()
    {
        return $this->hasOne(Modelo::className(), ['id_modelo' => 'id_modelo_id']);
    }
}
