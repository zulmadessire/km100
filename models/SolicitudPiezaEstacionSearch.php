<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SolicitudPiezaEstacion;

/**
 * SolicitudPiezaEstacionSearch represents the model behind the search form about `app\models\SolicitudPiezaEstacion`.
 */
class SolicitudPiezaEstacionSearch extends SolicitudPiezaEstacion
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'cantidad', 'id_solicitud_compra_estacion', 'id_pieza', 'prioridad'], 'integer'],
            [['descipcion', 'observacion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SolicitudPiezaEstacion::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'cantidad' => $this->cantidad,
            'id_solicitud_compra_estacion' => $this->id_solicitud_compra_estacion,
            'id_pieza' => $this->id_pieza,
            'prioridad' => $this->prioridad,
        ]);

        $query->andFilterWhere(['like', 'descipcion', $this->descipcion])
            ->andFilterWhere(['like', 'observacion', $this->observacion]);

        return $dataProvider;
    }
}
