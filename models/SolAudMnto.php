<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sol_aud_mnto".
 *
 * @property integer $id
 * @property integer $numero
 * @property string $ficha_mva
 * @property string $km_actual
 * @property string $observacion
 * @property integer $id_modelo
 * @property integer $id_tipo_mnto
 * @property integer $id_aud_mnto
 *
 * @property AuditoriaMnto $idAudMnto
 * @property Modelo $idModelo
 * @property TipoMantenimiento $idTipoMnto
 */
class SolAudMnto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sol_aud_mnto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['numero', 'id_modelo', 'id_tipo_mnto', 'id_aud_mnto'], 'integer'],
            [['id_aud_mnto'], 'required'],
            [['ficha_mva', 'km_actual'], 'string', 'max' => 100],
            [['observacion'], 'string', 'max' => 1000],
            [['id_aud_mnto'], 'exist', 'skipOnError' => true, 'targetClass' => AuditoriaMnto::className(), 'targetAttribute' => ['id_aud_mnto' => 'id']],
            [['id_modelo'], 'exist', 'skipOnError' => true, 'targetClass' => Modelo::className(), 'targetAttribute' => ['id_modelo' => 'id_modelo']],
            [['id_tipo_mnto'], 'exist', 'skipOnError' => true, 'targetClass' => TipoMantenimiento::className(), 'targetAttribute' => ['id_tipo_mnto' => 'id_tipo_mnto']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'numero' => 'Numero',
            'ficha_mva' => 'Ficha Mva',
            'km_actual' => 'Km Actual',
            'observacion' => 'Observacion',
            'id_modelo' => 'Id Modelo',
            'id_tipo_mnto' => 'Id Tipo Mnto',
            'id_aud_mnto' => 'Id Aud Mnto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAudMnto()
    {
        return $this->hasOne(AuditoriaMnto::className(), ['id' => 'id_aud_mnto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdModelo()
    {
        return $this->hasOne(Modelo::className(), ['id_modelo' => 'id_modelo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTipoMnto()
    {
        return $this->hasOne(TipoMantenimiento::className(), ['id_tipo_mnto' => 'id_tipo_mnto']);
    }
}
