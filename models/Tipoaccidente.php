<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipoaccidente".
 *
 * @property integer $id_tipoaccidente
 * @property string $nombre
 *
 * @property Caso[] $casos
 */
class Tipoaccidente extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipoaccidente';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_tipoaccidente' => 'Id Tipoaccidente',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCasos()
    {
        return $this->hasMany(Caso::className(), ['id_tipo_accidente' => 'id_tipoaccidente']);
    }
}
