<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "auditoria_ejec_mant".
 *
 * @property integer $id
 * @property string $tecnico
 * @property string $mva_vehiculo
 * @property integer $id_tipo_mnto
 * @property integer $estado
 * @property string $fecha
 * @property integer $id_estacion
 *
 * @property AspectosEvaluar[] $aspectosEvaluars
 * @property Estacion $idEstacion
 */
class AuditoriaEjecMant extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auditoria_ejec_mant';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_tipo_mnto', 'estado', 'id_estacion','mensual'], 'integer'],
            [['fecha'], 'safe'],
            [['id_estacion'], 'required'],
            [['tecnico'], 'string', 'max' => 1000],
            [['mva_vehiculo'], 'string', 'max' => 100],
            [['id_estacion'], 'exist', 'skipOnError' => true, 'targetClass' => Estacion::className(), 'targetAttribute' => ['id_estacion' => 'id_estacion']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tecnico' => 'Tecnico',
            'mva_vehiculo' => 'Mva Vehiculo',
            'id_tipo_mnto' => 'Id Tipo Mnto',
            'estado' => 'Estado',
            'fecha' => 'Fecha',
            'id_estacion' => 'Id Estacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAspectosEvaluars()
    {
        return $this->hasMany(AspectosEvaluar::className(), ['id_auditoria_ejec_mant' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstacion()
    {
        return $this->hasOne(Estacion::className(), ['id_estacion' => 'id_estacion']);
    }
}
