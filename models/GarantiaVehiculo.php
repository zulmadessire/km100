<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "garantia_vehiculo".
 *
 * @property int $id
 * @property string $tiempo
 * @property string $km
 * @property int $anular
 * @property int $motivo
 * @property int $id_partes
 * @property int $id_vehiculo
 * @property int $id_usuario_anula
 * @property string $fecha_anula
 * @property int $periodo 0-Dias, 1-Meses, 2-Años
 *
 * @property User $usuarioAnula
 * @property Vehiculo $vehiculo
 * @property Partes $partes
 */
class GarantiaVehiculo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'garantia_vehiculo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['motivo', 'id_partes', 'id_vehiculo', 'id_usuario_anula', 'periodo'], 'integer'],
            [['id_partes', 'id_vehiculo'], 'required'],
            [['fecha_anula'], 'safe'],
            [['tiempo', 'km'], 'string', 'max' => 45],
            [['anular'], 'string', 'max' => 1],
            [['id_usuario_anula'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_usuario_anula' => 'id']],
            [['id_vehiculo'], 'exist', 'skipOnError' => true, 'targetClass' => Vehiculo::className(), 'targetAttribute' => ['id_vehiculo' => 'id']],
            [['id_partes'], 'exist', 'skipOnError' => true, 'targetClass' => Partes::className(), 'targetAttribute' => ['id_partes' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tiempo' => 'Tiempo',
            'km' => 'Km',
            'anular' => 'Anular',
            'motivo' => 'Motivo',
            'id_partes' => 'Id Partes',
            'id_vehiculo' => 'Id Vehiculo',
            'id_usuario_anula' => 'Id Usuario Anula',
            'fecha_anula' => 'Fecha Anula',
            'periodo' => 'Periodo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarioAnula()
    {
        return $this->hasOne(User::className(), ['id' => 'id_usuario_anula']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehiculo()
    {
        return $this->hasOne(Vehiculo::className(), ['id' => 'id_vehiculo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartes()
    {
        return $this->hasOne(Partes::className(), ['id' => 'id_partes']);
    }
}
