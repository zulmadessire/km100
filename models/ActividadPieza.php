<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "actividad_pieza".
 *
 * @property integer $id_activ_pieza
 * @property integer $id_actividad_mnto
 * @property integer $id_pieza
 *
 * @property ActividadMnto $idActividadMnto
 * @property Pieza $idPieza
 * @property Consumo[] $consumos
 */
class ActividadPieza extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'actividad_pieza';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_actividad_mnto', 'id_pieza'], 'required'],
            [['id_actividad_mnto', 'id_pieza'], 'integer'],
            [['id_actividad_mnto'], 'exist', 'skipOnError' => true, 'targetClass' => ActividadMnto::className(), 'targetAttribute' => ['id_actividad_mnto' => 'id_actividad_mnto']],
            [['id_pieza'], 'exist', 'skipOnError' => true, 'targetClass' => Pieza::className(), 'targetAttribute' => ['id_pieza' => 'id_pieza']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_activ_pieza' => 'Id Activ Pieza',
            'id_actividad_mnto' => 'Id Actividad Mnto',
            'id_pieza' => 'Pieza',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdActividadMnto()
    {
        return $this->hasOne(ActividadMnto::className(), ['id_actividad_mnto' => 'id_actividad_mnto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPieza()
    {
        return $this->hasOne(Pieza::className(), ['id_pieza' => 'id_pieza']);
    }


 
}
