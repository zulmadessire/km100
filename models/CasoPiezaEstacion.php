<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "caso_pieza_estacion".
 *
 * @property int $id
 * @property int $id_caso
 * @property int $id_solicitud_compra_estacion
 */
class CasoPiezaEstacion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'caso_pieza_estacion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_caso', 'id_solicitud_compra_estacion'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_caso' => 'Id Caso',
            'id_solicitud_compra_estacion' => 'Id Solicitud Compra Estacion',
        ];
    }
}
