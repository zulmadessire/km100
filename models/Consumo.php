<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "consumo".
 *
 * @property integer $id
 * @property integer $cantidad
 * @property integer $id_actividad_pieza
 * @property integer $id_mantenimiento_preventivo
 *
 * @property ActividadPieza $idActividadPieza
 * @property MantenimientoPreventivo $idMantenimientoPreventivo
 */
class Consumo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'consumo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cantidad', 'id_actividad_pieza', 'id_mantenimiento_preventivo'], 'integer'],
            [['id_actividad_pieza', 'id_mantenimiento_preventivo'], 'required'],
            [['id_actividad_pieza'], 'exist', 'skipOnError' => true, 'targetClass' => ActividadPieza::className(), 'targetAttribute' => ['id_actividad_pieza' => 'id_activ_pieza']],
            [['id_mantenimiento_preventivo'], 'exist', 'skipOnError' => true, 'targetClass' => MantenimientoPreventivo::className(), 'targetAttribute' => ['id_mantenimiento_preventivo' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cantidad' => 'Cantidad',
            'id_actividad_pieza' => 'Id Actividad Pieza',
            'id_mantenimiento_preventivo' => 'Id Mantenimiento Preventivo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdActividadPieza()
    {
        return $this->hasOne(ActividadPieza::className(), ['id_activ_pieza' => 'id_actividad_pieza']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMantenimientoPreventivo()
    {
        return $this->hasOne(MantenimientoPreventivo::className(), ['id' => 'id_mantenimiento_preventivo']);
    }
}
