<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "add_mnto".
 *
 * @property integer $id
 * @property string $observacion
 * @property integer $id_aud_mnto
 *
 * @property AuditoriaMnto $idAudMnto
 */
class AddMnto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'add_mnto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_aud_mnto'], 'required'],
            [['id_aud_mnto','id_modelo','km_actual'], 'integer'],
            [['observacion','ficha'], 'string', 'max' => 1000],
            [['id_aud_mnto'], 'exist', 'skipOnError' => true, 'targetClass' => AuditoriaMnto::className(), 'targetAttribute' => ['id_aud_mnto' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'observacion' => 'Observacion',
            'id_aud_mnto' => 'Id Aud Mnto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAudMnto()
    {
        return $this->hasOne(AuditoriaMnto::className(), ['id' => 'id_aud_mnto']);
    }
}
