<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Estacion;
use yii\db\Query;

/**
 * EstacionesSearch represents the model behind the search form about `app\models\Estacion`.
 */
class EstacionesSearch extends Estacion
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_estacion', 'id_empresa', 'coordinador', 'id_wizard'], 'integer'],
            [['codigo', 'nombre', 'direccion', 'coord_google_maps'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Estacion::find();
 
        if ( isset( $params['EstacionesSearch']['id_cliente'] ) AND $params['EstacionesSearch']['id_cliente'] != '') {
            $query->andWhere( '(SELECT cli.id FROM cliente cli INNER JOIN empresa emp ON cli.id = emp.cliente_id INNER JOIN estacion est ON est.id_empresa=emp.id_empresa AND cli.id = '.$params['EstacionesSearch']['id_cliente'].' WHERE emp.id_empresa = estacion.id_empresa LIMIT 1) = '.$params['EstacionesSearch']['id_cliente'] );    
            
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_estacion' => $this->id_estacion,
            'id_empresa' => $this->id_empresa,
            'coordinador' => $this->coordinador,
            'id_wizard' => $this->id_wizard,
        ]);

        $query->andFilterWhere(['like', 'codigo', $this->codigo])
            ->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'direccion', $this->direccion])
            ->andFilterWhere(['like', 'coord_google_maps', $this->coord_google_maps]);

        return $dataProvider;
    }
    
     public function searchmensual($params)
    {
 
// $model = Estacion::find()
//     ->JoinWith('auditoriaFs a')
//     ->where('MONTH(a.fecha)=MONTH(CURDATE())')
//     ->andwhere('YEAR(a.fecha) = YEAR(CURDATE())');
        $query2 = new query();
        $query2->select('es.id_estacion')
        ->from('estacion es')
        ->join('INNER JOIN','auditoria_fs aud','es.id_estacion = aud.id_estacion')
        ->where( 'MONTH(aud.fecha)=MONTH(CURDATE())')
        ->andwhere( 'YEAR(aud.fecha)=YEAR(CURDATE())')
        ->andwhere( 'aud.estado = 1');
        
                
                $sql = $query2->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                $model = Yii::$app->db->createCommand($sql)->queryOne();
        $i = 0;
        $array[]='';
        if ($model) {
            # code...
        foreach ($model as $value) {
             $array[$i] = $value;
             $i++;
 $query=Estacion::find()
           ->Where(['NOT IN','id_estacion', $query2]);


        }
        }
 
    


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

            // grid filtering conditions
        $query->andFilterWhere([
            'id_estacion' => $this->id_estacion,
            'id_empresa' => $this->id_empresa,
            'coordinador' => $this->coordinador,
            'id_wizard' => $this->id_wizard,
        ]);

        $query->andFilterWhere(['like', 'codigo', $this->codigo])
            ->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'direccion', $this->direccion])
            ->andFilterWhere(['like', 'coord_google_maps', $this->coord_google_maps]);

        return $dataProvider;
}
 public function searchgeneral($params)
    {
 
        $query2 = new query();
        $query2->select('es.id_estacion')
        ->from('estacion es')
        ->join('INNER JOIN','auditoria_general aud','es.id_estacion = aud.id_estacion')
        ->where( 'MONTH(aud.fecha)=MONTH(CURDATE())' )
        ->andwhere( 'YEAR(aud.fecha)=YEAR(CURDATE())')
        ->andwhere( 'auditoria_general.id_aud_fs != null' OR 'auditoria_general.id_aud_mnto != null' OR 'auditoria_general.id_aud_ejec != null')        
        ->andwhere( 'aud.estado = 3');
        
        
        // ->groupBy('es.id_estacion');
        $sql = $query2->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
        $model = Yii::$app->db->createCommand($sql)->queryOne();
      

        $i = 0;
        $array[]='';
        if ($model) {
            # code...
        foreach ($model as $value) {
             $array[$i] = $value;
             $i++;
 $query=Estacion::find()       
 ->where('audita = 1')
        ->andWhere(['NOT IN','id_estacion', $query2]);


        }
        }else{
            $query=Estacion::find() ->where('audita = 1');
        }
   
 
    


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

                   // grid filtering conditions
        $query->andFilterWhere([
            'id_estacion' => $this->id_estacion,
            'id_empresa' => $this->id_empresa,
            'coordinador' => $this->coordinador,
            'id_wizard' => $this->id_wizard,
        ]);

        $query->andFilterWhere(['like', 'codigo', $this->codigo])
            ->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'direccion', $this->direccion])
            ->andFilterWhere(['like', 'coord_google_maps', $this->coord_google_maps]);

        return $dataProvider;
}
//estaciones de auditorias continuas
public function searchgeneral2($params)
    {
 
        
 $query=Estacion::find()       
 ->where('audita = 2');


              $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

                   // grid filtering conditions
        $query->andFilterWhere([
            'id_estacion' => $this->id_estacion,
            'id_empresa' => $this->id_empresa,
            'coordinador' => $this->coordinador,
            'id_wizard' => $this->id_wizard,
        ]);

        $query->andFilterWhere(['like', 'codigo', $this->codigo])
            ->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'direccion', $this->direccion])
            ->andFilterWhere(['like', 'coord_google_maps', $this->coord_google_maps]);

        return $dataProvider;
}

 public function searchmnto($params)
    {

        $query2 = new query();
        $query2->select('es.id_estacion')
        ->from('estacion es')
        ->join('INNER JOIN','auditoria_mnto aud','es.id_estacion = aud.id_estacion')
        ->where( 'MONTH(aud.fecha)=MONTH(CURDATE())')
        ->andwhere( 'YEAR(aud.fecha)=YEAR(CURDATE())')
        ->andwhere( 'aud.estado = 1');
                   

                $sql = $query2->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                $model = Yii::$app->db->createCommand($sql)->queryOne();
        $i = 0;
        $array[]='';
        if ($model) {
            # code...
        foreach ($model as $value) {
             $array[$i] = $value;
             $i++;

        }
        }
 
 $query=Estacion::find()
           ->Where(['NOT IN','id_estacion', $query2]);
    


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
}
        public function searchmensualejec($params)
    {
 
// $model = Estacion::find()
//     ->JoinWith('auditoriaFs a')
//     ->where('MONTH(a.fecha)=MONTH(CURDATE())')
//     ->andwhere('YEAR(a.fecha) = YEAR(CURDATE())');
        $query3 = new query();
        $query3->select('es.id_estacion')
        ->from('estacion es')
        ->join('INNER JOIN','auditoria_ejec_mant aud','es.id_estacion = aud.id_estacion')
        ->where( 'MONTH(aud.fecha)=MONTH(CURDATE())')
        ->andwhere( 'YEAR(aud.fecha)=YEAR(CURDATE())')
        ->andwhere( 'aud.estado = 1');
                   
                $sql = $query3->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                $model2 = Yii::$app->db->createCommand($sql)->queryOne();
 
        $i = 0;
        $array[]='';
        if ($model2) {
            # code...
        foreach ($model2 as $value2) {
             $array[$i] = $value2;
             $i++;

        }
        }
 
 $query=Estacion::find()
           ->Where(['NOT IN','id_estacion', $query3]);
    


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
}


}
