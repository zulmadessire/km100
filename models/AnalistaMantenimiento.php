<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "analista_mantenimiento".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $telefono
 * @property string $direccion
 * @property string $correo
 * @property string $user
 *
 * @property CotizacionTallerAnalista[] $cotizacionTallerAnalistas
 */
class AnalistaMantenimiento extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'analista_mantenimiento';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'telefono', 'direccion', 'correo'], 'required'],
            [['nombre', 'direccion', 'correo', 'user'], 'string', 'max' => 200],
            [['telefono'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'telefono' => 'Telefono',
            'direccion' => 'Direccion',
            'correo' => 'Correo',
            'user' => 'User',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCotizacionTallerAnalistas()
    {
        return $this->hasMany(CotizacionTallerAnalista::className(), ['id_analista' => 'id']);
    }
}
