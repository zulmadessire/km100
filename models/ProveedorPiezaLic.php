<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proveedor_pieza_lic".
 *
 * @property integer $id
 * @property integer $id_licitacion
 * @property integer $id_prov_pieza
 * @property integer $estado
 * @property string $fecha
 *
 * @property Licitacion $idLicitacion
 * @property ProveedorPieza $idProvPieza
 */
class ProveedorPiezaLic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'proveedor_pieza_lic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_licitacion', 'id_prov_pieza', 'estado'], 'integer'],
            [['fecha'], 'safe'],
            [['id_licitacion'], 'exist', 'skipOnError' => true, 'targetClass' => Licitacion::className(), 'targetAttribute' => ['id_licitacion' => 'id']],
            [['id_prov_pieza'], 'exist', 'skipOnError' => true, 'targetClass' => ProveedorPieza::className(), 'targetAttribute' => ['id_prov_pieza' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_licitacion' => 'Id Licitacion',
            'id_prov_pieza' => 'Id Prov Pieza',
            'estado' => 'Estado',
            'fecha' => 'Fecha',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLicitacion()
    {
        return $this->hasOne(Licitacion::className(), ['id' => 'id_licitacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProvPieza()
    {
        return $this->hasOne(ProveedorPieza::className(), ['id' => 'id_prov_pieza']);
    }
}
