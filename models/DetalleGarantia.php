<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "detalle_garantia".
 *
 * @property integer $id
 * @property integer $id_marca
 * @property integer $id_concesionario
 *
 * @property Concesionario $idConcesionario
 * @property Marca $idMarca
 * @property PartesGarantia[] $partesGarantias
 */
class DetalleGarantia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'detalle_garantia';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_marca', 'id_concesionario'], 'required'],
            [['id_marca', 'id_concesionario'], 'integer'],
            [['id_concesionario'], 'exist', 'skipOnError' => true, 'targetClass' => Concesionario::className(), 'targetAttribute' => ['id_concesionario' => 'id']],
            [['id_marca'], 'exist', 'skipOnError' => true, 'targetClass' => Marca::className(), 'targetAttribute' => ['id_marca' => 'id_marca']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_marca' => 'Id Marca',
            'id_concesionario' => 'Id Concesionario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdConcesionario()
    {
        return $this->hasOne(Concesionario::className(), ['id' => 'id_concesionario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMarca()
    {
        return $this->hasOne(Marca::className(), ['id_marca' => 'id_marca']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartesGarantias()
    {
        return $this->hasMany(PartesGarantia::className(), ['detalle_garantia_id' => 'id']);
    }
}
