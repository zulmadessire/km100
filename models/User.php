<?php

namespace app\models;

use Yii;
use app\models\Empresa;
use dektrium\user\models\User as BaseUser;

class User extends BaseUser
{
    public function rules()
    {       
        $rules = parent::rules();
        //$rules[] =[['cargo'], 'required', 'on' => ['register', 'create']];
        //$rules[] =[['cargo'], 'integer'];
        $rules[] =[['id_taller'], 'integer'];
        //$rules[] =[['id_empresa'], 'required', 'on' => ['register', 'create']];
        //$rules[] =[['cargo_gerencial'], 'required', 'on' => ['register', 'create']];
        //$rules[] =[['cargo_gerencial'], 'integer'];
        return $rules;
    }
    
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        //$labels[] = ['cargo' => Yii::t('user', 'Cargo a desempeñar')];
        $labels[] = ['id_taller' => Yii::t('user', 'Taller asociado')];
        //$labels[] = ['cargo_gerencial' => Yii::t('user', '¿Cargo Gerencial?')];
        return $labels;
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getAuthAssignments()
    {
        return $this->hasMany(AuthAssignment::className(), ['user_id' => 'id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getItemNames()
    {
        return $this->hasMany(AuthItem::className(), ['name' => 'item_name'])->viaTable('auth_assignment', ['user_id' => 'id']);
    }

    public function getChecklistAvaluos()
    {
        return $this->hasMany(ChecklistAvaluo::className(), ['id_analista' => 'id']);
    }
}

?>