<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "otras_fs".
 *
 * @property integer $id
 * @property integer $id_modelo
 * @property string $causa
 * @property string $responsable
 * @property string $observacion
 * @property integer $id_aud_fs
 * @property integer $numero
 * @property string $solicitud
 * @property integer $cantidad
 *
 * @property AuditoriaFs $idAudFs
 * @property Modelo $idModelo
 */
class OtrasFs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'otras_fs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_modelo', 'id_aud_fs', 'numero', 'cantidad'], 'integer'],
            [['id_aud_fs'], 'required'],
            [['causa'], 'string', 'max' => 200],
            [['responsable'], 'string', 'max' => 20],
            [['observacion'], 'string', 'max' => 1000],
            [['solicitud'], 'string', 'max' => 500],
            [['id_aud_fs'], 'exist', 'skipOnError' => true, 'targetClass' => AuditoriaFs::className(), 'targetAttribute' => ['id_aud_fs' => 'id']],
            [['id_modelo'], 'exist', 'skipOnError' => true, 'targetClass' => Modelo::className(), 'targetAttribute' => ['id_modelo' => 'id_modelo']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_modelo' => 'Id Modelo',
            'causa' => 'Causa',
            'responsable' => 'Responsable',
            'observacion' => 'Observacion',
            'id_aud_fs' => 'Id Aud Fs',
            'numero' => 'Numero',
            'solicitud' => 'Solicitud',
            'cantidad' => 'Cantidad',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAudFs()
    {
        return $this->hasOne(AuditoriaFs::className(), ['id' => 'id_aud_fs']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdModelo()
    {
        return $this->hasOne(Modelo::className(), ['id_modelo' => 'id_modelo']);
    }
}
