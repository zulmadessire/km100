<?php
namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;
use app\models\Servicio;
use yii\imagine\Image;  
use Imagine\Image\Box;


class UploadReporteAccidente extends Model
{
    /**
     * @var UploadedFile[]
     */

    public $imageFiles;

    public function rules()
    {
        return [
            [['imageFiles'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'maxFiles' => 1, 'maxSize' => 4194304],
            [['imageFiles'], 'required', 'on' => 'acta', 'whenClient' => "function (tipo_caso, value) {
                                                            return ($('#tipo_caso').val() == 0);
                                                        }"],
        ];
    }

    public function attributeLabels()
    {
        return [
            'imageFiles' => 'REPORTE DE ACCIDENTE',
        ];
    }

    public function upload($id_servicio)
    {
        $basePath = '/uploads/servicios/';
        //if ($this->validate()) {
            foreach ($this->imageFiles as $i => $file) {

                $soporte = Servicio::find()->where(['id' => $id_servicio])->One();

                if ($soporte->img_reporte) {
                    unlink('.'.$soporte->img_reporte);
                }

                $file->saveAs('.'.$basePath . $id_servicio . '/reporte-accidente-'.$id_servicio.'.'.$file->extension);

                Image::resize('.'.$basePath . $id_servicio . '/reporte-accidente-'.$id_servicio.'.'.$file->extension, 1200, 800)
                ->save('.'.$basePath . $id_servicio . '/reporte-accidente-'.$id_servicio.'.'.$file->extension, 
                        ['quality' => 70]);

                $soporte->img_reporte=$basePath . $id_servicio . '/reporte-accidente-'.$id_servicio.'.'.$file->extension;
                $soporte->save();
            }
            return;
            
        /*} else {
          die('aqui oy');
            return false;
        }*/
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['acta'] = ['imageFiles'];//Scenario Values Only Accepted
        return $scenarios;
    }
}
?>