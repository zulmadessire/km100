<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Vehiculo;

/**
 * wizardSearch represents the model behind the search form about `app\models\Wizard`.
 */
class WizardSearch extends Vehiculo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_categoria', 'transito', 'anho','id_concesionario'], 'integer'],
            [['color', 'transmision', 'mva', 'chasis', 'placa', 'clase', 'combustible', 'fecha_compra','transito', 'fecha_inicial', 'anho', 'fecha_renovacion_placa', 'id_modelo'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Vehiculo::find();


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_categoria' => $this->id_categoria,
            'id_concesionario' => $this->id_concesionario,
            'fecha_compra' => $this->fecha_compra,
            'fecha_inicial' => $this->fecha_inicial,
            'transito' => $this->transito,
        ]);

        $query->andFilterWhere(['like', 'color', $this->color])
            ->andFilterWhere(['like', 'transmision', $this->transmision])
            ->andFilterWhere(['like', 'transito', 1])
            ->andFilterWhere(['like', 'mva', $this->mva])
            ->andFilterWhere(['like', 'chasis', $this->chasis])
            ->andFilterWhere(['like', 'placa', $this->placa])
            ->andFilterWhere(['like', 'clase', $this->clase])
            ->andFilterWhere(['like', 'combustible', $this->combustible])
            ->andFilterWhere(['like', 'anho', $this->anho])
            ->andFilterWhere(['like', 'fecha_renovacion_placa', $this->fecha_renovacion_placa])
            ->andFilterWhere(['like', 'id_modelo', $this->id_modelo])
            ->andFilterWhere(['like', 'id_concesionario', $this->id_concesionario]);

        return $dataProvider;
    }
       public function search_realizados($params)
    {
        
        $query = Vehiculo::find()
        ->groupBy(['id_modelo'])
        ->all();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }



     public function search_wizard($params)
    {
        $query = Vehiculo::find()
->where(['transito' => '1'])
->groupBy(['id_modelo']);


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}
