<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "wizard".
 *
 * @property integer $id
 * @property string $color
 * @property string $transmision
 * @property string $mva
 * @property string $chasis
 * @property string $placa
 * @property integer $id_categoria
 * @property string $clase
 * @property string $combustible
 * @property string $fecha_compra
 * @property string $fecha_inicial
 * @property integer $km
 * @property string $anho
 * @property string $frenovacion
 * @property string $id_modelo
 * @property integer $transito
 */
class Wizard extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wizard';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['chasis', 'placa', 'id_categoria', 'clase', 'combustible', 'fecha_compra', 'fecha_inicial', 'km', 'anho', 'frenovacion', 'id_modelo', 'transito'], 'required'],
            [['id_categoria', 'km', 'transito'], 'integer'],
            [['fecha_compra', 'fecha_inicial'], 'safe'],
            [['color', 'transmision', 'mva'], 'string', 'max' => 45],
            [['chasis', 'placa', 'clase', 'combustible'], 'string', 'max' => 200],
            [['anho', 'frenovacion', 'id_modelo'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'color' => 'Color',
            'transmision' => 'Transmision',
            'mva' => 'Mva',
            'chasis' => 'Chasis',
            'placa' => 'Placa',
            'id_categoria' => 'Id Categoria',
            'clase' => 'Clase',
            'combustible' => 'Combustible',
            'fecha_compra' => 'Fecha Compra',
            'fecha_inicial' => 'Fecha Inicial',
            'km' => 'Km',
            'anho' => 'Anho',
            'frenovacion' => 'Frenovacion',
            'id_modelo' => 'Id Modelo',
            'transito' => 'Transito',
        ];
    }
}
