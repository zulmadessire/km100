<?php
 
namespace app\models;
use Yii;
use yii\base\model;
 
class Indicador extends model {

    public $mes;
    public $ano;
    public $estacion;
    public $tipo_trabajo;

    public function rules()
    {
        return [
            [['mes', 'ano', 'estacion', 'tipo_trabajo'], 'string'],     
        ];
    }
}

?>