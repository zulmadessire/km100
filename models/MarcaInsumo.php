<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "marca_insumo".
 *
 * @property integer $id
 * @property string $nombre
 * @property integer $tipo_pieza_id
 * @property string $marca
 * @property string $unidad_medicion
 * @property string $tamanho
 *
 * @property TipoPieza $tipoPieza
 * @property ProveedorPieza[] $proveedorPiezas
 */
class MarcaInsumo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'marca_insumo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'tipo_pieza_id'], 'required'],
            [['tipo_pieza_id'], 'integer'],
            [['nombre'], 'string', 'max' => 45],
            [['marca', 'unidad_medicion', 'tamanho'], 'string', 'max' => 100],
            [['tipo_pieza_id'], 'exist', 'skipOnError' => true, 'targetClass' => TipoPieza::className(), 'targetAttribute' => ['tipo_pieza_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'tipo_pieza_id' => 'Tipo de pieza',
            'marca' => 'Marca',
            'unidad_medicion' => 'Unidad de Medicion',
            'tamanho' => 'Tamaño',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoPieza()
    {
        return $this->hasOne(TipoPieza::className(), ['id' => 'tipo_pieza_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProveedorPiezas()
    {
        return $this->hasMany(ProveedorPieza::className(), ['marca_insumo_id' => 'id']);
    }
}
