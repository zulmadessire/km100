<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "checklist_avaluo_pieza".
 *
 * @property integer $id
 * @property integer $accion
 * @property integer $id_checklist_avaluo
 * @property integer $pieza_id
 *
 * @property ChecklistAvaluo $idChecklistAvaluo
 * @property Pieza $pieza
 */
class ChecklistAvaluoPieza extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $tipo;
    public static function tableName()
    {
        return 'checklist_avaluo_pieza';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_checklist_avaluo', 'pieza_id','tipo'], 'required'],
            [['accion', 'id_checklist_avaluo', 'pieza_id','tipo'], 'integer'],
            [['id_checklist_avaluo'], 'exist', 'skipOnError' => true, 'targetClass' => ChecklistAvaluo::className(), 'targetAttribute' => ['id_checklist_avaluo' => 'id']],
            [['pieza_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pieza::className(), 'targetAttribute' => ['pieza_id' => 'id_pieza']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'accion' => 'Accion',
            'id_checklist_avaluo' => 'Id Checklist Avaluo',
            'pieza_id' => 'Pieza',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdChecklistAvaluo()
    {
        return $this->hasOne(ChecklistAvaluo::className(), ['id' => 'id_checklist_avaluo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPieza()
    {
        return $this->hasOne(Pieza::className(), ['id_pieza' => 'pieza_id']);
    }
}
