<?php
namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;
use app\models\Soporte;
use yii\imagine\Image;  
use Imagine\Image\Box;

class UploadForm extends Model
{
    /**
     * @var UploadedFile[]
     */

    public $imageFiles;

    public function rules()
    {
        return [
            [['imageFiles'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, PNG', 'maxFiles' => 10, 'maxSize' => 4194304],
        ];
    }

    public function attributeLabels()
    {
        return [
            'imageFiles' => 'Imágenes Adicionales',
        ];
    }

    public function upload($id_caso)
    {
        $basePath = '/uploads/casos/'.$id_caso;

        if ($this->validate()) {
            foreach ($this->imageFiles as $i => $file) {
                /*$text = $file->baseName;
                $nom_img  = str_replace(' ', '', $text);*/
                $file->saveAs('.' . $basePath . '/soporte-adicional-'.$id_caso.'-'.$i.'.'.$file->extension);

                Image::resize('.' . $basePath . '/soporte-adicional-'.$id_caso.'-'.$i.'.'.$file->extension, 1200, 800)
                ->save('.' . $basePath . '/soporte-adicional-'.$id_caso.'-'.$i.'.'.$file->extension, 
                        ['quality' => 70]);

                $soporte = new Soporte();
                $soporte->ubicacion=$basePath . '/soporte-adicional-'.$id_caso.'-'.$i.'.'.$file->extension;
                $soporte->bloque=6; // Adicionales
                $soporte->id_caso=$id_caso;
                $soporte->save();
            }
            return true;
        } else {
            return false;
        }
    }
}
