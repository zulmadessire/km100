<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "soporte".
 *
 * @property integer $id_soporte
 * @property string $ubicacion
 * @property integer $bloque
 * @property integer $id_caso
 *
 * @property Caso $idCaso
 */
class Soporte extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'soporte';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ubicacion', 'bloque', 'id_caso'], 'required'],
            [['bloque', 'id_caso'], 'integer'],
            [['ubicacion'], 'string', 'max' => 200],
            [['id_caso'], 'exist', 'skipOnError' => true, 'targetClass' => Caso::className(), 'targetAttribute' => ['id_caso' => 'id_caso']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_soporte' => 'Id Soporte',
            'ubicacion' => 'Ubicacion',
            'bloque' => 'Bloque',
            'id_caso' => 'Id Caso',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCaso()
    {
        return $this->hasOne(Caso::className(), ['id_caso' => 'id_caso']);
    }
}
