<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "modelo".
 *
 * @property integer $id_modelo
 * @property string $nombre
 * @property integer $id_marca
 *
 * @property Caso[] $casos
 * @property Mensajero[] $mensajeros
 * @property Marca $idMarca
 * @property Pieza[] $piezas
 * @property SolAudMnto[] $solAudMntos
 * @property SolicitudCompraEstacion[] $solicitudCompraEstacions
 * @property Vehiculo[] $vehiculos
 * @property VehiculoTaller[] $vehiculoTallers
 */
class Modelo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'modelo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'id_marca'], 'required'],
            [['id_marca'], 'integer'],
            [['nombre'], 'string', 'max' => 50],
            [['id_marca'], 'exist', 'skipOnError' => true, 'targetClass' => Marca::className(), 'targetAttribute' => ['id_marca' => 'id_marca']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_modelo' => 'Id Modelo',
            'nombre' => 'Nombre',
            'id_marca' => 'Id Marca',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCasos()
    {
        return $this->hasMany(Caso::className(), ['id_modelo' => 'id_modelo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMensajeros()
    {
        return $this->hasMany(Mensajero::className(), ['id_modelo_id' => 'id_modelo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMarca()
    {
        return $this->hasOne(Marca::className(), ['id_marca' => 'id_marca']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPiezas()
    {
        return $this->hasMany(Pieza::className(), ['modelo_id_modelo' => 'id_modelo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSolAudMntos()
    {
        return $this->hasMany(SolAudMnto::className(), ['id_modelo' => 'id_modelo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSolicitudCompraEstacions()
    {
        return $this->hasMany(SolicitudCompraEstacion::className(), ['id_modelo' => 'id_modelo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehiculos()
    {
        return $this->hasMany(Vehiculo::className(), ['id_modelo' => 'id_modelo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehiculoTallers()
    {
        return $this->hasMany(VehiculoTaller::className(), ['id_modelo' => 'id_modelo']);
    }
}
