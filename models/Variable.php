<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "variables".
 *
 * @property integer $id
 * @property string $label
 * @property string $descripcion
 * @property string $value
 */
class Variable extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'variables';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'value'], 'required'],
            [['label', 'value'], 'string', 'max' => 100],
            [['descripcion'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'label' => 'Label',
            'descripcion' => 'Descripción',
            'value' => 'Value',
        ];
    }
}
