<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "marca_rubro".
 *
 * @property integer $id_marca_rubro
 * @property string $nombre
 *
 * @property ModeloRubro[] $modeloRubros
 */
class MarcaRubro extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'marca_rubro';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_marca_rubro' => 'Id Marca Rubro',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModeloRubros()
    {
        return $this->hasMany(ModeloRubro::className(), ['id_marca_rubro' => 'id_marca_rubro']);
    }
}
