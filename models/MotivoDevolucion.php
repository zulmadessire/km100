<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "motivo_devolucion".
 *
 * @property int $id
 * @property string $nombre
 *
 * @property CotizacionProveedorPieza[] $cotizacionProveedorPiezas
 */
class MotivoDevolucion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'motivo_devolucion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCotizacionProveedorPiezas()
    {
        return $this->hasMany(CotizacionProveedorPieza::className(), ['id_motivo_devolucion' => 'id']);
    }
}
