<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Notificaciones;

/**
 * NotificacionesSearch represents the model behind the search form about `app\models\notificaciones`.
 */
class NotificacionesSearch extends notificaciones
{
    public $placa, $mva, $fecha_renovacion_placa, $fecha_venc_marbete, $fecha_venc_seguro, $nombre, $telefono, $nombre_representante, $vencimiento_rm, $vencimiento_acuerdo;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tiempo', 'tipo', 'estado', 'id_vehiculo', 'id_taller','placa','mva','fecha_renovacion_placa','fecha_venc_marbete','fecha_venc_seguro','nombre','telefono','nombre_representante','vencimiento_rm','vencimiento_acuerdo'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = notificaciones::find();
 
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tiempo' => $this->tiempo,
            'tipo' => $this->tipo,
            'estado' => $this->estado,
            'id_vehiculo' => $this->id_vehiculo,
            'id_taller' => $this->id_taller,
        ]);

        return $dataProvider;
    }



     public function searchplacas1($params)
    {
         
            $query = notificaciones::find()
             ->where('notificaciones.tipo = 0')
             ->orderBy(['notificaciones.id'=> SORT_ASC]);

 
           if ( isset($params['NotificacionesSearch']['placa']) AND $params['NotificacionesSearch']['placa'] != '') {
               $query->andwhere( '(SELECT placa FROM vehiculo WHERE id_vehiculo = notificaciones.id_vehiculo LIMIT 1) = '."'".$params['NotificacionesSearch']['placa']."'" );
           }
                if ( isset($params['NotificacionesSearch']['mva']) AND $params['NotificacionesSearch']['mva'] != '') {
               $query->andwhere( '(SELECT mva FROM vehiculo WHERE id_vehiculo = notificaciones.id_vehiculo LIMIT 1) = '."'".$params['NotificacionesSearch']['mva']."'" );
           }
         
            if ( isset($params['NotificacionesSearch']['fecha_renovacion_placa']) AND $params['NotificacionesSearch']['fecha_renovacion_placa'] != '') {
               $query->andwhere( '(SELECT fecha_renovacion_placa FROM vehiculo WHERE id_vehiculo = notificaciones.id_vehiculo LIMIT 1) = '."'".$params['NotificacionesSearch']['fecha_renovacion_placa']."'" );
           }
               
        $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]);

        $this->load($params);
        if(!$this->validate()){
            return $dataProvider;
        }
   
       // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tiempo' => $this->tiempo,
            'tipo' => $this->tipo,
            'estado' => $this->estado,
            'id_vehiculo' => $this->id_vehiculo,
            'id_taller' => $this->id_taller,
        ]);

        return $dataProvider;
    }
     public function searchseguros($params)
    {
         
            $query = notificaciones::find()
             ->where('notificaciones.tipo = 1')
             ->orderBy(['notificaciones.id'=> SORT_ASC]);

                if ( isset($params['NotificacionesSearch']['placa']) AND $params['NotificacionesSearch']['placa'] != '') {
               $query->andwhere( '(SELECT placa FROM vehiculo WHERE id_vehiculo = notificaciones.id_vehiculo LIMIT 1) = '."'".$params['NotificacionesSearch']['placa']."'" );
           }
                if ( isset($params['NotificacionesSearch']['mva']) AND $params['NotificacionesSearch']['mva'] != '') {
               $query->andwhere( '(SELECT mva FROM vehiculo WHERE id_vehiculo = notificaciones.id_vehiculo LIMIT 1) = '."'".$params['NotificacionesSearch']['mva']."'" );
           }
         
            if ( isset($params['NotificacionesSearch']['fecha_venc_seguro']) AND $params['NotificacionesSearch']['fecha_venc_seguro'] != '') {
               $query->andwhere( '(SELECT fecha_venc_seguro FROM vehiculo WHERE id_vehiculo = notificaciones.id_vehiculo LIMIT 1) = '."'".$params['NotificacionesSearch']['fecha_venc_seguro']."'" );
           }


        $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]);

        $this->load($params);
        if(!$this->validate()){
            return $dataProvider;
        }
   
       // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tiempo' => $this->tiempo,
            'tipo' => $this->tipo,
            'estado' => $this->estado,
            'id_vehiculo' => $this->id_vehiculo,
            'id_taller' => $this->id_taller,
        ]);

        return $dataProvider;
    }

     public function searchmarbetes($params)
    {
         
            $query = notificaciones::find()
             ->where('notificaciones.tipo = 2')
             ->orderBy(['notificaciones.id'=> SORT_ASC]);

                if ( isset($params['NotificacionesSearch']['placa']) AND $params['NotificacionesSearch']['placa'] != '') {
               $query->andwhere( '(SELECT placa FROM vehiculo WHERE id_vehiculo = notificaciones.id_vehiculo LIMIT 1) = '."'".$params['NotificacionesSearch']['placa']."'" );
           }
                if ( isset($params['NotificacionesSearch']['mva']) AND $params['NotificacionesSearch']['mva'] != '') {
               $query->andwhere( '(SELECT mva FROM vehiculo WHERE id_vehiculo = notificaciones.id_vehiculo LIMIT 1) = '."'".$params['NotificacionesSearch']['mva']."'" );
           }
         
            if ( isset($params['NotificacionesSearch']['fecha_venc_marbete']) AND $params['NotificacionesSearch']['fecha_venc_marbete'] != '') {
               $query->andwhere( '(SELECT fecha_venc_marbete FROM vehiculo WHERE id_vehiculo = notificaciones.id_vehiculo LIMIT 1) = '."'".$params['NotificacionesSearch']['fecha_venc_marbete']."'" );
           }


        $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]);

        $this->load($params);
        if(!$this->validate()){
            return $dataProvider;
        }
   
       // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tiempo' => $this->tiempo,
            'tipo' => $this->tipo,
            'estado' => $this->estado,
            'id_vehiculo' => $this->id_vehiculo,
            'id_taller' => $this->id_taller,
        ]);

        return $dataProvider;
    }
      public function searchrm($params)
    {
         
            $query = notificaciones::find()
             ->where('notificaciones.tipo = 3')
             ->orderBy(['notificaciones.id'=> SORT_ASC]);

                if ( isset($params['NotificacionesSearch']['nombre']) AND $params['NotificacionesSearch']['nombre'] != '') {
               $query->andwhere( '(SELECT nombre FROM taller WHERE id_taller = notificaciones.id_taller LIMIT 1) = '."'".$params['NotificacionesSearch']['nombre']."'" );
           }
                if ( isset($params['NotificacionesSearch']['telefono']) AND $params['NotificacionesSearch']['telefono'] != '') {
               $query->andwhere( '(SELECT telefono FROM taller WHERE id_taller = notificaciones.id_taller LIMIT 1) = '."'".$params['NotificacionesSearch']['telefono']."'" );
           }
               if ( isset($params['NotificacionesSearch']['nombre_representante']) AND $params['NotificacionesSearch']['nombre_representante'] != '') {
               $query->andwhere( '(SELECT nombre_representante FROM taller WHERE id_taller = notificaciones.id_taller LIMIT 1) = '."'".$params['NotificacionesSearch']['nombre_representante']."'" );
           }
         
            if ( isset($params['NotificacionesSearch']['vencimiento_rm']) AND $params['NotificacionesSearch']['vencimiento_rm'] != '') {
               $query->andwhere( '(SELECT vencimiento_rm FROM taller WHERE id_taller = notificaciones.id_taller LIMIT 1) = '."'".$params['NotificacionesSearch']['vencimiento_rm']."'" );
           }


        $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]);

        $this->load($params);
        if(!$this->validate()){
            return $dataProvider;
        }
   
       // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tiempo' => $this->tiempo,
            'tipo' => $this->tipo,
            'estado' => $this->estado,
            'id_vehiculo' => $this->id_vehiculo,
            'id_taller' => $this->id_taller,
        ]);

        return $dataProvider;
    }
     public function searchacuerdo($params)
    {
         
            $query = notificaciones::find()
             ->where('notificaciones.tipo = 4')
             ->orderBy(['notificaciones.id'=> SORT_ASC]);

                if ( isset($params['NotificacionesSearch']['nombre']) AND $params['NotificacionesSearch']['nombre'] != '') {
               $query->andwhere( '(SELECT nombre FROM taller WHERE id_taller = notificaciones.id_taller LIMIT 1) = '."'".$params['NotificacionesSearch']['nombre']."'" );
           }
                if ( isset($params['NotificacionesSearch']['telefono']) AND $params['NotificacionesSearch']['telefono'] != '') {
               $query->andwhere( '(SELECT telefono FROM taller WHERE id_taller = notificaciones.id_taller LIMIT 1) = '."'".$params['NotificacionesSearch']['telefono']."'" );
           }
               if ( isset($params['NotificacionesSearch']['nombre_representante']) AND $params['NotificacionesSearch']['nombre_representante'] != '') {
               $query->andwhere( '(SELECT nombre_representante FROM taller WHERE id_taller = notificaciones.id_taller LIMIT 1) = '."'".$params['NotificacionesSearch']['nombre_representante']."'" );
           }
         
            if ( isset($params['NotificacionesSearch']['vencimiento_acuerdo']) AND $params['NotificacionesSearch']['vencimiento_acuerdo'] != '') {
               $query->andwhere( '(SELECT vencimiento_acuerdo FROM taller WHERE id_taller = notificaciones.id_taller LIMIT 1) = '."'".$params['NotificacionesSearch']['vencimiento_acuerdo']."'" );
           }


        $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]);

        $this->load($params);
        if(!$this->validate()){
            return $dataProvider;
        }
   
       // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tiempo' => $this->tiempo,
            'tipo' => $this->tipo,
            'estado' => $this->estado,
            'id_vehiculo' => $this->id_vehiculo,
            'id_taller' => $this->id_taller,
        ]);

        return $dataProvider;
    }
}
