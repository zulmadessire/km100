<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "servicio_taller".
 *
 * @property int $id
 * @property string $nombre
 *
 * @property PiezaServicioTaller[] $piezaServicioTallers
 */
class ServicioTaller extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'servicio_taller';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPiezaServicioTallers()
    {
        return $this->hasMany(PiezaServicioTaller::className(), ['id_servicio_taller' => 'id']);
    }
}
