<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mecanico".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $cedula
 * @property string $telefono
 * @property string $direccion
 */
class Mecanico extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mecanico';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre','cedula'], 'required'],
            [['nombre'], 'string', 'max' => 200],
            [['cedula'], 'string', 'max' => 20],
            [['telefono'], 'string', 'max' => 40],
            [['direccion'], 'string', 'max' => 450],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'cedula' => 'Cédula',
            'telefono' => 'Teléfono',
            'direccion' => 'Dirección',
        ];
    }
}
