<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProyeccionMantenimiento;

/**
 * ProyeccionMantenimientosSearch represents the model behind the search form about `app\models\ProyeccionMantenimiento`.
 */
class ProyeccionMantenimientosSearch extends ProyeccionMantenimiento
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'realizado', 'serie', 'id_vehiculo', 'id_proximo_tipo_mantenimiento', 'id_estacion_reporte'], 'integer'],
            [['fecha_proyectada', 'km_proyectado', 'fecha_reporte'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProyeccionMantenimiento::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fecha_proyectada' => $this->fecha_proyectada,
            'realizado' => $this->realizado,
            'serie' => $this->serie,
            'id_vehiculo' => $this->id_vehiculo,
            'id_proximo_tipo_mantenimiento' => $this->id_proximo_tipo_mantenimiento,
            'fecha_reporte' => $this->fecha_reporte,
            'id_estacion_reporte' => $this->id_estacion_reporte,
        ]);

        $query->andFilterWhere(['like', 'km_proyectado', $this->km_proyectado]);

        return $dataProvider;
    }
}
