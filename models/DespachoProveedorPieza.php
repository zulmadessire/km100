<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "despacho_proveedor_pieza".
 *
 * @property integer $id
 * @property integer $cantidad_enviada_taller
 * @property integer $cantidad_recibida_taller
 * @property integer $id_despacho
 * @property integer $id_cotizacion_proveedor_pieza
 *
 * @property CotizacionProveedorPieza $idCotizacionProveedorPieza
 * @property Despacho $idDespacho
 */
class DespachoProveedorPieza extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'despacho_proveedor_pieza';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cantidad_enviada_taller', 'cantidad_recibida_taller', 'id_despacho', 'id_cotizacion_proveedor_pieza','recibido'], 'integer'],
            [['id_despacho', 'id_cotizacion_proveedor_pieza'], 'required'],
            [['id_cotizacion_proveedor_pieza'], 'exist', 'skipOnError' => true, 'targetClass' => CotizacionProveedorPieza::className(), 'targetAttribute' => ['id_cotizacion_proveedor_pieza' => 'id']],
            [['id_despacho'], 'exist', 'skipOnError' => true, 'targetClass' => Despacho::className(), 'targetAttribute' => ['id_despacho' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cantidad_enviada_taller' => 'Cantidad Enviada Taller',
            'cantidad_recibida_taller' => 'Cantidad Recibida Taller',
            'id_despacho' => 'Id Despacho',
            'id_cotizacion_proveedor_pieza' => 'Id Cotizacion Proveedor Pieza',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCotizacionProveedorPieza()
    {
        return $this->hasOne(CotizacionProveedorPieza::className(), ['id' => 'id_cotizacion_proveedor_pieza']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdDespacho()
    {
        return $this->hasOne(Despacho::className(), ['id' => 'id_despacho']);
    }
}
