<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "servicio_mensaje".
 *
 * @property int $id
 * @property string $mensaje
 * @property string $fecha
 * @property int $motivo 0-Cotización, 1-Negociación, 2-Evaluación Analista, 3-Evaluación Gerente, 4-Evaluación Director, 5-Salvamento, 6-Otro
 * @property int $estado
 * @property int $codigo_pipeline
 * @property int $id_servicio
 * @property int $id_user
 *
 * @property Servicio $servicio
 * @property User $user
 */
class ServicioMensaje extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'servicio_mensaje';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mensaje'], 'string'],
            [['fecha'], 'safe'],
            [['motivo', 'estado', 'codigo_pipeline', 'id_servicio', 'id_user'], 'integer'],
            [['id_servicio', 'id_user'], 'required'],
            [['id_servicio'], 'exist', 'skipOnError' => true, 'targetClass' => Servicio::className(), 'targetAttribute' => ['id_servicio' => 'id']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mensaje' => 'Mensaje',
            'fecha' => 'Fecha',
            'motivo' => 'Motivo',
            'estado' => 'Estado',
            'codigo_pipeline' => 'Codigo Pipeline',
            'id_servicio' => 'Id Servicio',
            'id_user' => 'Id User',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicio()
    {
        return $this->hasOne(Servicio::className(), ['id' => 'id_servicio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }
}
