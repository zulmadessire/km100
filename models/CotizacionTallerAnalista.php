<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cotizacion_taller_analista".
 *
 * @property integer $id
 * @property integer $id_cotizacion_taller
 * @property integer $id_analista
 *
 * @property AnalistaMantenimiento $idAnalista
 * @property CotizacionTaller $idCotizacionTaller
 */
class CotizacionTallerAnalista extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cotizacion_taller_analista';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_cotizacion_taller', 'id_analista'], 'required'],
            [['id_cotizacion_taller', 'id_analista'], 'integer'],
            [['id_analista'], 'exist', 'skipOnError' => true, 'targetClass' => AnalistaMantenimiento::className(), 'targetAttribute' => ['id_analista' => 'id']],
            [['id_cotizacion_taller'], 'exist', 'skipOnError' => true, 'targetClass' => CotizacionTaller::className(), 'targetAttribute' => ['id_cotizacion_taller' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_cotizacion_taller' => 'Id Cotizacion Taller',
            'id_analista' => 'Id Analista',
        ];
    }
     /**
     * @return \yii\db\ActiveQuery
     */
    public function getChecklistAvaluos()
    {
        return $this->hasMany(ChecklistAvaluo::className(), ['id_cotizacion_taller_analista' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAnalista()
    {
        return $this->hasOne(AnalistaMantenimiento::className(), ['id' => 'id_analista']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCotizacionTaller()
    {
        return $this->hasOne(CotizacionTaller::className(), ['id' => 'id_cotizacion_taller']);
    }
}
