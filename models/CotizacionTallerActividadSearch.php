<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CotizacionTallerActividad;

/**
 * CotizacionTallerActividadSearch represents the model behind the search form of `app\models\CotizacionTallerActividad`.
 */
class CotizacionTallerActividadSearch extends CotizacionTallerActividad
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_pieza_servicio_taller', 'labor', 'id_cotizacion_taller', 'conforme_recepcion', 'id_vehiculo', 'tipo_servicio_taller'], 'integer'],
            [['precio', 'total_servicio'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CotizacionTallerActividad::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_pieza_servicio_taller' => $this->id_pieza_servicio_taller,
            'labor' => $this->labor,
            'precio' => $this->precio,
            'total_servicio' => $this->total_servicio,
            'id_cotizacion_taller' => $this->id_cotizacion_taller,
            'conforme_recepcion' => $this->conforme_recepcion,
            'id_vehiculo' => $this->id_vehiculo,
            'tipo_servicio_taller' => $this->tipo_servicio_taller,
        ]);

        return $dataProvider;
    }
}
