<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "despacho".
 *
 * @property integer $id
 * @property integer $numero
 * @property integer $estado
 * @property string $fecha
 * @property integer $estado_recepcion_taller
 * @property string $traslado
 * @property integer $id_solicitud_compra_pieza
 * @property integer $id_taller
 * @property integer $id_estacion
 * @property integer $id_mensajero
 *
 * @property Estacion $idEstacion
 * @property Mensajero $idMensajero
 * @property SolicitudCompraPieza $idSolicitudCompraPieza
 * @property Taller $idTaller
 * @property DespachoProveedorPieza[] $despachoProveedorPiezas
 */
class Despacho extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'despacho';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['numero', 'estado', 'estado_recepcion_taller', 'id_solicitud_compra_pieza', 'id_taller', 'id_estacion', 'id_mensajero'], 'integer'],
            [['fecha'], 'safe'],
            [['id_solicitud_compra_pieza'], 'required'],
            [['id_mensajero'], 'required', 'on' => 'update'],
            [['traslado'], 'string', 'max' => 45],
            [['id_estacion'], 'exist', 'skipOnError' => true, 'targetClass' => Estacion::className(), 'targetAttribute' => ['id_estacion' => 'id_estacion']],
            [['id_mensajero'], 'exist', 'skipOnError' => true, 'targetClass' => Mensajero::className(), 'targetAttribute' => ['id_mensajero' => 'id']],
            [['id_solicitud_compra_pieza'], 'exist', 'skipOnError' => true, 'targetClass' => SolicitudCompraPieza::className(), 'targetAttribute' => ['id_solicitud_compra_pieza' => 'id']],
            [['id_taller'], 'exist', 'skipOnError' => true, 'targetClass' => Taller::className(), 'targetAttribute' => ['id_taller' => 'id_taller']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'numero' => 'Numero',
            'estado' => 'Estado',
            'fecha' => 'Fecha',
            'estado_recepcion_taller' => 'Estado Recepcion Taller',
            'traslado' => 'Traslado',
            'id_solicitud_compra_pieza' => 'Id Solicitud Compra Pieza',
            'id_taller' => 'Id Taller',
            'id_estacion' => 'Id Estacion',
            'id_mensajero' => 'Representante de Reparto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstacion()
    {
        return $this->hasOne(Estacion::className(), ['id_estacion' => 'id_estacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMensajero()
    {
        return $this->hasOne(Mensajero::className(), ['id' => 'id_mensajero']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSolicitudCompraPieza()
    {
        return $this->hasOne(SolicitudCompraPieza::className(), ['id' => 'id_solicitud_compra_pieza']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTaller()
    {
        return $this->hasOne(Taller::className(), ['id_taller' => 'id_taller']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDespachoProveedorPiezas()
    {
        return $this->hasMany(DespachoProveedorPieza::className(), ['id_despacho' => 'id']);
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['update'] = ['id_mensajero'];//Scenario Values Only Accepted
        return $scenarios;
    }
}
