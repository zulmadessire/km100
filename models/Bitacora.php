<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bitacora".
 *
 * @property integer $id
 * @property string $color
 * @property string $transmision
 * @property string $chasis
 * @property string $placa
 * @property string $mva
 * @property integer $id_categoria
 * @property integer $clase
 * @property integer $combustible
 * @property string $fecha_compra
 * @property string $fecha_inicial
 * @property integer $tiempo_vida
 * @property string $km
 * @property integer $fecha_renovacion_placa
 * @property integer $fecha_emision_placa
 * @property integer $wizard
 * @property integer $tsd
 * @property integer $exactus
 * @property integer $id_modelo
 * @property integer $id_concesionario
 * @property integer $transito
 * @property integer $anho
 * @property string $fecha_actual
 */
class Bitacora extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bitacora';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'color', 'transmision', 'chasis', 'placa', 'mva', 'id_categoria', 'clase', 'combustible', 'fecha_compra', 'fecha_inicial', 'tiempo_vida', 'km', 'fecha_renovacion_placa', 'fecha_emision_placa', 'wizard', 'tsd', 'exactus', 'id_modelo', 'id_concesionario', 'transito', 'anho', 'fecha_actual'], 'required'],
            [['id', 'id_categoria', 'clase', 'combustible', 'tiempo_vida', 'fecha_renovacion_placa', 'fecha_emision_placa', 'wizard', 'tsd', 'exactus', 'id_modelo', 'id_concesionario', 'transito', 'anho'], 'integer'],
            [['fecha_compra', 'fecha_inicial', 'fecha_actual'], 'safe'],
            [['color', 'transmision', 'chasis', 'placa', 'mva', 'km'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'color' => 'Color',
            'transmision' => 'Transmision',
            'chasis' => 'Chasis',
            'placa' => 'Placa',
            'mva' => 'Mva',
            'id_categoria' => 'Id Categoria',
            'clase' => 'Clase',
            'combustible' => 'Combustible',
            'fecha_compra' => 'Fecha Compra',
            'fecha_inicial' => 'Fecha Inicial',
            'tiempo_vida' => 'Tiempo Vida',
            'km' => 'Km',
            'fecha_renovacion_placa' => 'Fecha Renovacion Placa',
            'fecha_emision_placa' => 'Fecha Emision Placa',
            'wizard' => 'Wizard',
            'tsd' => 'Tsd',
            'exactus' => 'Exactus',
            'id_modelo' => 'Id Modelo',
            'id_concesionario' => 'Id Concesionario',
            'transito' => 'Transito',
            'anho' => 'Anho',
            'fecha_actual' => 'Fecha Actual',
        ];
    }
}
