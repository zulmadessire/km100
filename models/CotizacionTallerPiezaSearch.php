<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CotizacionTallerPieza;

/**
 * CotizacionTallerPiezaSearch represents the model behind the search form about `app\models\CotizacionTallerPieza`.
 */
class CotizacionTallerPiezaSearch extends CotizacionTallerPieza
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'cantidad', 'id_cotizacion_taller', 'id_pieza'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CotizacionTallerPieza::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'cantidad' => $this->cantidad,
            'id_cotizacion_taller' => $this->id_cotizacion_taller,
            'id_pieza' => $this->id_pieza,
        ]);

        return $dataProvider;
    }
}
