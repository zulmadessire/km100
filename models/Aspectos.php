<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "aspectos".
 *
 * @property integer $id
 * @property string $descripcion
 *
 * @property AspectosEvaluar[] $aspectosEvaluars
 */
class Aspectos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'aspectos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['descripcion'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'descripcion' => 'Descripcion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAspectosEvaluars()
    {
        return $this->hasMany(AspectosEvaluar::className(), ['id_aspecto' => 'id']);
    }
}
