<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipo_mantenimiento".
 *
 * @property integer $id_tipo_mnto
 * @property string $nombre
 * @property integer $estado
 *
 * @property MantenimientoPreventivo[] $mantenimientoPreventivos
 * @property ProyeccionMantenimiento[] $proyeccionMantenimientos
 * @property SolAudMnto[] $solAudMntos
 */
class TipoMantenimiento extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipo_mantenimiento';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['estado'], 'integer'],
            [['nombre'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_tipo_mnto' => 'Id Tipo Mnto',
            'nombre' => 'Nombre',
            'estado' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMantenimientoPreventivos()
    {
        return $this->hasMany(MantenimientoPreventivo::className(), ['id_tipo_mantenimiento' => 'id_tipo_mnto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProyeccionMantenimientos()
    {
        return $this->hasMany(ProyeccionMantenimiento::className(), ['id_proximo_tipo_mantenimiento' => 'id_tipo_mnto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSolAudMntos()
    {
        return $this->hasMany(SolAudMnto::className(), ['id_tipo_mnto' => 'id_tipo_mnto']);
    }
}
