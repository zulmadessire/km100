<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cotizacion_taller".
 *
 * @property integer $id
 * @property string $imagen_soporte
 * @property integer $tipo_pago
 * @property string $fecha
 * @property integer $estado
 * @property integer $activo
 * @property integer $id_solicitud_servicio_taller
 *
 * @property SolicitudServicioTaller $idSolicitudServicioTaller
 * @property CotizacionTallerActividad[] $cotizacionTallerActividads
 * @property CotizacionTallerAnalista[] $cotizacionTallerAnalistas
 * @property CotizacionTallerPieza[] $cotizacionTallerPiezas
 * @property OrdenTrabajo[] $ordenTrabajos
 */
class CotizacionTaller extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cotizacion_taller';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipo_pago', 'estado', 'activo', 'id_solicitud_servicio_taller'], 'integer'],
            [['fecha'], 'safe'],
            [['tipo_pago', 'id_solicitud_servicio_taller', 'cancelado','status_cotizacion'], 'integer'],
            [['id_solicitud_servicio_taller','imagen_soporte',], 'required'],
            [['imagen_soporte','imagen_soporte1','imagen_soporte2','imagen_soporte3','imagen_soporte4','observacion','observacion_cancela'], 'string', 'max' => 200],
            [['id_solicitud_servicio_taller'], 'exist', 'skipOnError' => true, 'targetClass' => SolicitudServicioTaller::className(), 'targetAttribute' => ['id_solicitud_servicio_taller' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'imagen_soporte' => 'Imagen Soporte',
            'imagen_soporte1' => 'Imagen Soporte',
            'imagen_soporte2' => 'Imagen Soporte',
            'imagen_soporte3' => 'Imagen Soporte',
            'imagen_soporte4' => 'Imagen Soporte',
            'tipo_pago' => 'Tipo Pago',
            'fecha' => 'Fecha',
            'estado' => 'Estado',
            'activo' => 'Activo',
            'cancelado' => 'cancelado',
            'observacion' => 'observacion',
            'id_solicitud_servicio_taller' => 'Id Solicitud Servicio Taller',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSolicitudServicioTaller()
    {
        return $this->hasOne(SolicitudServicioTaller::className(), ['id' => 'id_solicitud_servicio_taller']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCotizacionTallerActividads()
    {
        return $this->hasMany(CotizacionTallerActividad::className(), ['id_cotizacion_taller' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCotizacionTallerAnalistas()
    {
        return $this->hasMany(CotizacionTallerAnalista::className(), ['id_cotizacion_taller' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCotizacionTallerPiezas()
    {
        return $this->hasMany(CotizacionTallerPieza::className(), ['id_cotizacion_taller' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdenTrabajos()
    {
        return $this->hasMany(OrdenTrabajo::className(), ['id_cotizacion_taller' => 'id']);
    }
}
