<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CotizacionTaller;

/**
 * CotizacionTallerSearch represents the model behind the search form about `app\models\CotizacionTaller`.
 */
class CotizacionTallerSearch extends CotizacionTaller
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tipo_pago', 'id_solicitud_servicio_taller'], 'integer'],
            [['imagen_soporte'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CotizacionTaller::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tipo_pago' => $this->tipo_pago,
            'id_solicitud_servicio_taller' => $this->id_solicitud_servicio_taller,
        ]);

        $query->andFilterWhere(['like', 'imagen_soporte', $this->imagen_soporte]);

        return $dataProvider;
    }
}
