<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pieza;

/**
 * PiezasSearch represents the model behind the search form about `app\models\Pieza`.
 */
class PiezasSearch extends Pieza
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pieza', 'tipo_pieza_id' ], 'integer'],
            [['nombre', 'tamano', 'unidad'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pieza::find(); 
    // if ( isset( $params['PiezasSearch']['id_modelo'] ) AND $params['PiezasSearch']['id_modelo'] != '') {
    //         $query->andWhere( '(SELECT ts.id_modelo FROM modelo_pieza st INNER JOIN modelo ts ON st.modelo_id_modelo = ts.id_modelo AND ts.id_modelo = '.$params['PiezasSearch']['id_modelo'].' WHERE st.pieza_id = pieza.id_pieza LIMIT 1) = '.$params['PiezasSearch']['id_modelo'] );
 
    //     }
 if ( isset( $params['PiezasSearch']['id_modelo'] ) AND $params['PiezasSearch']['id_modelo'] != '') {
            $query->andWhere( '(SELECT m.id_modelo FROM pieza vt INNER JOIN modelo m ON vt.modelo_id_modelo = m.id_modelo AND m.id_modelo = '.$params['PiezasSearch']['id_modelo'].' WHERE vt.id_pieza = pieza.id_pieza LIMIT 1) = '.$params['PiezasSearch']['id_modelo'] );
        // add conditions that should always apply here
}
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_pieza' => $this->id_pieza,
            'tipo_pieza_id' => $this->tipo_pieza_id,
         
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'tamano', $this->tamano])
            ->andFilterWhere(['like', 'unidad', $this->unidad]);
             

        return $dataProvider;
    }
}
