<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "imagenes_fs".
 *
 * @property integer $id
 * @property string $descripcion
 * @property string $anexo
 * @property string $extension
 * @property string $base_url
 * @property string $imageFile
 * @property integer $id_auditoriafs
 *
 * @property AuditoriaFs $idAuditoriafs
 */
class ImagenesFs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'imagenes_fs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_auditoriafs'], 'required'],
            [['id_auditoriafs'], 'integer'],
            [['descripcion'], 'string', 'max' => 500],
            [['anexo', 'extension', 'base_url'], 'string', 'max' => 1000],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg,jpeg, pdf, mp3, wav, mp4'],
            [['id_auditoriafs'], 'exist', 'skipOnError' => true, 'targetClass' => AuditoriaFs::className(), 'targetAttribute' => ['id_auditoriafs' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'descripcion' => 'descripcion',
            'anexo' => 'Anexo',
            'extension' => 'Extension',
            'base_url' => 'Base Url',
            'imageFile' => 'Image File',
            'id_auditoriafs' => 'Id Auditoriafs',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAuditoriafs()
    {
        return $this->hasOne(AuditoriaFs::className(), ['id' => 'id_auditoriafs']);
    }
}
