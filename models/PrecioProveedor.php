<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "precio_proveedor".
 *
 * @property integer $id
 * @property string $garantia
 * @property string $tiempo_santo_domingo
 * @property string $tiempo_interior
 * @property string $dias
 * @property integer $alineacion
 * @property string $obs_alineacion
 * @property integer $capacitacion
 * @property string $obs_capacitacion
 * @property string $entrega_via
 * @property string $obs_entrega_via
 * @property integer $delivery
 * @property string $obs_delivery
 * @property integer $id_prov_licitacion
 *
 * @property ProveedorPiezaLic $idProvLicitacion
 * @property Precios[] $precios
 */
class PrecioProveedor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'precio_proveedor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['alineacion', 'capacitacion', 'delivery', 'id_prov_licitacion'], 'integer'],
            [['garantia', 'dias'], 'string', 'max' => 20],
            [['tiempo_santo_domingo', 'tiempo_interior'], 'string', 'max' => 30],
            [['obs_alineacion', 'obs_capacitacion', 'entrega_via', 'obs_entrega_via', 'obs_delivery'], 'string', 'max' => 10],
            [['id_prov_licitacion'], 'exist', 'skipOnError' => true, 'targetClass' => ProveedorPiezaLic::className(), 'targetAttribute' => ['id_prov_licitacion' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'garantia' => 'Garantia',
            'tiempo_santo_domingo' => 'Tiempo Santo Domingo',
            'tiempo_interior' => 'Tiempo Interior',
            'dias' => 'Dias',
            'alineacion' => 'Alineacion',
            'obs_alineacion' => 'Obs Alineacion',
            'capacitacion' => 'Capacitacion',
            'obs_capacitacion' => 'Obs Capacitacion',
            'entrega_via' => 'Entrega Via',
            'obs_entrega_via' => 'Obs Entrega Via',
            'delivery' => 'Delivery',
            'obs_delivery' => 'Obs Delivery',
            'id_prov_licitacion' => 'Id Prov Licitacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProvLicitacion()
    {
        return $this->hasOne(ProveedorPiezaLic::className(), ['id' => 'id_prov_licitacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrecios()
    {
        return $this->hasMany(Precios::className(), ['id_prec_prov' => 'id']);
    }
}
