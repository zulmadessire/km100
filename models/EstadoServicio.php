<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estado_servicio".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $color
 * @property integer $codigo
 * @property integer $dias_optimos
 *
 * @property PipelineServicio[] $pipelineServicios
 */
class EstadoServicio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'estado_servicio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'color'], 'required'],
            [['codigo', 'dias_optimos'], 'integer'],
            [['nombre'], 'string', 'max' => 45],
            [['color'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'color' => 'Color',
            'codigo' => 'Codigo',
            'dias_optimos' => 'Dias Optimos',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPipelineServicios()
    {
        return $this->hasMany(PipelineServicio::className(), ['id_estado_servicio' => 'id']);
    }
}
