<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Auditoriamnto;

/**
 * AuditoriamntoSearch represents the model behind the search form about `app\models\Auditoriamnto`.
 */
class AuditoriamntoSearch extends Auditoriamnto
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'desviacion', 'mntos_realizados', 'mntos_smna_anterior', 'estado', 'id_estacion'], 'integer'],
            [['fecha', 'observacion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Auditoriamnto::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'desviacion' => $this->desviacion,
            'mntos_realizados' => $this->mntos_realizados,
            'mntos_smna_anterior' => $this->mntos_smna_anterior,
            'fecha' => $this->fecha,
            'estado' => $this->estado,
            'id_estacion' => $this->id_estacion,
        ]);

        $query->andFilterWhere(['like', 'observacion', $this->observacion]);

        return $dataProvider;
    }
}
