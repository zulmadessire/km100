<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ubicacionunidad".
 *
 * @property integer $id_ubicacionunidad
 * @property string $nombre
 *
 * @property Caso[] $casos
 */
class Ubicacionunidad extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ubicacionunidad';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_ubicacionunidad' => 'Id Ubicacionunidad',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCasos()
    {
        return $this->hasMany(Caso::className(), ['id_ubicacion' => 'id_ubicacionunidad']);
    }
}
