<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cotizacion_proveedor_pieza".
 *
 * @property int $id
 * @property int $cantidad_disponible
 * @property int $cantidad_solicitada
 * @property string $fecha_entrega
 * @property double $costo
 * @property int $ahorro
 * @property int $id_cotizacion_proveedor
 * @property int $id_cotizacion_taller_pieza
 * @property int $cantidad_comprar
 * @property int $aprobacion
 * @property int $cantidad_recibida
 * @property string $garantia
 * @property int $inventario indica si paso la pieza a inventario
 * @property string $serial
 * @property int $estado_despacho Indica en que estado se encuentra el despacho de la pieza 0-En espera, 1-Enviada a mensajero, 2-Recibida por mensajero, 3-Entregada
 * @property int $cantidad_devolucion
 * @property int $id_usuario_devuelve
 * @property string $fecha_devolucion
 * @property string $fecha_entrega_nueva
 * @property int $id_motivo_devolucion
 *
 * @property CotizacionProveedor $cotizacionProveedor
 * @property CotizacionTallerPieza $cotizacionTallerPieza
 * @property MotivoDevolucion $motivoDevolucion
 * @property DespachoProveedorPieza[] $despachoProveedorPiezas
 */
class CotizacionProveedorPieza extends \yii\db\ActiveRecord
{
    public $imageFiles;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cotizacion_proveedor_pieza';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cantidad_disponible', 'cantidad_solicitada', 'ahorro', 'id_cotizacion_proveedor', 'id_cotizacion_taller_pieza', 'cantidad_comprar', 'aprobacion', 'cantidad_recibida', 'inventario', 'estado_despacho', 'cantidad_devolucion', 'id_usuario_devuelve', 'id_motivo_devolucion', 'id_solicitud_pieza_estacion', 'id_pieza', 'km_garantia'], 'integer'],
            [['fecha_entrega', 'fecha_devolucion', 'fecha_entrega_nueva'], 'safe'],
            [['costo'], 'number'],
            //[['id_cotizacion_proveedor', 'id_cotizacion_taller_pieza'], 'required'],
            [['id_cotizacion_proveedor'], 'required'],
            [['observacion_pieza'], 'string'],
            [['imagen_pieza'], 'string', 'max' => 255],
            [['garantia'], 'string', 'max' => 100],
            [['serial'], 'string', 'max' => 50],
            [['id_cotizacion_proveedor'], 'exist', 'skipOnError' => true, 'targetClass' => CotizacionProveedor::className(), 'targetAttribute' => ['id_cotizacion_proveedor' => 'id']],
            [['id_cotizacion_taller_pieza'], 'exist', 'skipOnError' => true, 'targetClass' => CotizacionTallerPieza::className(), 'targetAttribute' => ['id_cotizacion_taller_pieza' => 'id']],
            [['id_motivo_devolucion'], 'exist', 'skipOnError' => true, 'targetClass' => MotivoDevolucion::className(), 'targetAttribute' => ['id_motivo_devolucion' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cantidad_disponible' => 'Cantidad Disponible',
            'cantidad_solicitada' => 'Cantidad Solicitada',
            'fecha_entrega' => 'Fecha Entrega',
            'costo' => 'Costo',
            'ahorro' => 'Descuento',
            'id_cotizacion_proveedor' => 'Id Cotizacion Proveedor',
            'id_cotizacion_taller_pieza' => 'Id Cotizacion Taller Pieza',
            'cantidad_comprar' => 'Cantidad Comprar',
            'aprobacion' => 'Aprobacion',
            'cantidad_recibida' => 'Cantidad Recibida',
            'garantia' => 'Garantia',
            'inventario' => 'Inventario',
            'serial' => 'Serial',
            'estado_despacho' => 'Estado Despacho',
            'cantidad_devolucion' => 'Cantidad Devolucion',
            'id_usuario_devuelve' => 'Id Usuario Devuelve',
            'fecha_devolucion' => 'Fecha Devolucion',
            'fecha_entrega_nueva' => 'Fecha Entrega Nueva',
            'id_motivo_devolucion' => 'Id Motivo Devolucion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCotizacionProveedor()
    {
        return $this->hasOne(CotizacionProveedor::className(), ['id' => 'id_cotizacion_proveedor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCotizacionTallerPieza()
    {
        return $this->hasOne(CotizacionTallerPieza::className(), ['id' => 'id_cotizacion_taller_pieza']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMotivoDevolucion()
    {
        return $this->hasOne(MotivoDevolucion::className(), ['id' => 'id_motivo_devolucion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDespachoProveedorPiezas()
    {
        return $this->hasMany(DespachoProveedorPieza::className(), ['id_cotizacion_proveedor_pieza' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSolicitudPiezaEstacion()
    {
        return $this->hasOne(SolicitudPiezaEstacion::className(), ['id' => 'id_solicitud_pieza_estacion']);
    }
}
