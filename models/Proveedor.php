<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proveedor".
 *
 * @property integer $id
 * @property string $codigo_proveedor
 * @property string $tipo_persona
 * @property string $identificacion
 * @property string $direccion
 * @property string $descripcion
 * @property string $telefono
 * @property double $credito
 * @property string $nombre_representante
 * @property string $identificacion_representante
 * @property string $puesto_representante
 * @property string $correo_representante
 * @property string $celular_representante
 * @property string $telefono_representante
 * @property string $garantia
 * @property string $domicilio
 * @property string $nombre
 * @property double $porcentaje
 * @property string $Tipo_pago
 * @property integer $plazo_pago
 * @property integer $tipo_empresa_id
 * @property string $user
 * @property string $moneda
 *
 * @property MarcaProveedor[] $marcaProveedors
 * @property TipoEmpresa $tipoEmpresa
 * @property ProveedorPieza[] $proveedorPiezas
 * @property SolicitudCompraProveedor[] $solicitudCompraProveedors
 */
class Proveedor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'proveedor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipo_persona', 'identificacion', 'direccion', 'descripcion', 'telefono', 'nombre_representante', 'identificacion_representante', 'puesto_representante', 'correo_representante', 'celular_representante', 'telefono_representante', 'garantia', 'domicilio', 'nombre', 'porcentaje', 'Tipo_pago', 'moneda'], 'required'],
            [['credito', 'porcentaje'], 'number'],
            [['plazo_pago', 'tipo_empresa_id'], 'integer'],
            [['tipo_persona', 'identificacion', 'direccion', 'descripcion', 'telefono', 'nombre_representante', 'identificacion_representante', 'puesto_representante', 'correo_representante', 'celular_representante', 'telefono_representante', 'garantia', 'domicilio', 'nombre'], 'string', 'max' => 100],
            [['Tipo_pago'], 'string', 'max' => 20],
            [['codigo_proveedor'], 'string', 'max' => 20],
            [['user'], 'string', 'max' => 200],
            [['moneda'], 'string', 'max' => 10],
            [['tipo_empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => TipoEmpresa::className(), 'targetAttribute' => ['tipo_empresa_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tipo_persona' => 'Tipo Persona',
            'identificacion' => 'Identificacion',
            'direccion' => 'Direccion',
            'descripcion' => 'Descripcion',
            'telefono' => 'Telefono',
            'credito' => 'Credito',
            'nombre_representante' => 'Nombre Representante',
            'identificacion_representante' => 'Identificacion Representante',
            'puesto_representante' => 'Puesto Representante',
            'correo_representante' => 'Correo Representante',
            'celular_representante' => 'Celular Representante',
            'telefono_representante' => 'Telefono Representante',
            'garantia' => 'Garantia',
            'domicilio' => 'Domicilio',
            'nombre' => 'Nombre',
            'porcentaje' => 'Porcentaje',
            'Tipo_pago' => 'Tipo Pago',
            'plazo_pago' => 'Plazo Pago',
            'tipo_empresa_id' => 'Tipo Empresa ID',
            'user' => 'User',
            'moneda' => 'Moneda',
            'codigo_proveedor' => 'Código proveerdor'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarcaProveedors()
    {
        return $this->hasMany(MarcaProveedor::className(), ['id_proveedor' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoEmpresa()
    {
        return $this->hasOne(TipoEmpresa::className(), ['id' => 'tipo_empresa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProveedorPiezas()
    {
        return $this->hasMany(ProveedorPieza::className(), ['proveedor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSolicitudCompraProveedors()
    {
        return $this->hasMany(SolicitudCompraProveedor::className(), ['id_proveedor' => 'id']);
    }
}
