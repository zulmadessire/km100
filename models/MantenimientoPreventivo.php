<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mantenimiento_preventivo".
 *
 * @property integer $id
 * @property string $imagen_soporte
 * @property string $responsable
 * @property string $fecha_realizacion
 * @property string $observaciones
 * @property string $km
 * @property integer $id_estacion
 * @property integer $id_tipo_mantenimiento
 * @property integer $id_proyeccion_mantenimiento
 *
 * @property Consumo[] $consumos
 * @property Estacion $idEstacion
 * @property ProyeccionMantenimiento $idProyeccionMantenimiento
 * @property TipoMantenimiento $idTipoMantenimiento
 */
class MantenimientoPreventivo extends \yii\db\ActiveRecord
{
    public $diferencia;
 
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mantenimiento_preventivo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['responsable'], 'required'],
            [['fecha_realizacion'], 'safe'],
            [['observaciones','observacion_adicional'], 'string'],
            [['id_estacion', 'id_tipo_mantenimiento', 'id_proyeccion_mantenimiento'], 'integer'],
            [['imagen_soporte','diferencia'], 'string', 'max' => 200],
            [['responsable', 'km'], 'string', 'max' => 45],
            [['id_estacion'], 'exist', 'skipOnError' => true, 'targetClass' => Estacion::className(), 'targetAttribute' => ['id_estacion' => 'id_estacion']],
            [['id_proyeccion_mantenimiento'], 'exist', 'skipOnError' => true, 'targetClass' => ProyeccionMantenimiento::className(), 'targetAttribute' => ['id_proyeccion_mantenimiento' => 'id']],
            [['id_tipo_mantenimiento'], 'exist', 'skipOnError' => true, 'targetClass' => TipoMantenimiento::className(), 'targetAttribute' => ['id_tipo_mantenimiento' => 'id_tipo_mnto']],
 
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'imagen_soporte' => 'Imagen Soporte',
            'responsable' => 'Responsable',
            'fecha_realizacion' => 'Fecha Realizacion',
            'observaciones' => 'Observaciones',
            'km' => 'Km',
            'id_estacion' => 'Id Estacion',
            'id_tipo_mantenimiento' => 'Id Tipo Mantenimiento',
            'id_proyeccion_mantenimiento' => 'Id Proyeccion Mantenimiento',
            'observacion_adicional' => 'observación adicional',
        ];
    }
 
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsumos()
    {
        return $this->hasMany(Consumo::className(), ['id_mantenimiento_preventivo' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstacion()
    {
        return $this->hasOne(Estacion::className(), ['id_estacion' => 'id_estacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProyeccionMantenimiento()
    {
        return $this->hasOne(ProyeccionMantenimiento::className(), ['id' => 'id_proyeccion_mantenimiento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTipoMantenimiento()
    {
        return $this->hasOne(TipoMantenimiento::className(), ['id_tipo_mnto' => 'id_tipo_mantenimiento']);
    }
}