<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "flota".
 *
 * @property integer $id
 * @property string $fecha_inicio
 * @property string $fecha_fin
 * @property integer $id_estacion
 * @property integer $id_vehiculo
 *
 * @property Estacion $idEstacion
 * @property Vehiculo $idVehiculo
 */
class Flota extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'flota';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha_inicio', 'id_estacion', 'id_vehiculo'], 'required'],
            [['fecha_inicio', 'fecha_fin','fecha'], 'safe'],
            [['id_estacion', 'id_vehiculo','kilometraje'], 'integer'],
            [['id_estacion'], 'exist', 'skipOnError' => true, 'targetClass' => Estacion::className(), 'targetAttribute' => ['id_estacion' => 'id_estacion']],
            [['id_vehiculo'], 'exist', 'skipOnError' => true, 'targetClass' => Vehiculo::className(), 'targetAttribute' => ['id_vehiculo' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha_inicio' => 'Fecha Inicio',
            'fecha_fin' => 'Fecha Fin',
            'id_estacion' => 'Id Estacion',
            'id_vehiculo' => 'Id Vehiculo',
            'kilometraje' => 'kilometraje',
            'fecha' => 'fecha',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstacion()
    {
        return $this->hasOne(Estacion::className(), ['id_estacion' => 'id_estacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdVehiculo()
    {
        return $this->hasOne(Vehiculo::className(), ['id' => 'id_vehiculo']);
    }
}
