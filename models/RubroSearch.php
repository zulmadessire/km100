<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Rubro;

/**
 * RubroSearch represents the model behind the search form about `app\models\Rubro`.
 */
class RubroSearch extends Rubro
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_rubro'], 'integer'],
            [['nombre', 'tamanio', 'unidad_medicion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Rubro::find();

        if ( isset( $params['RubroSearch']['id_marca_rubro'] ) AND $params['RubroSearch']['id_marca_rubro'] != '') {
            $query->andWhere( '(SELECT mo.id_marca_rubro FROM rubro_modelo_asignado rma INNER JOIN modelo_rubro mo ON rma.id_modelo_rubro = mo.id_modelo_rubro AND mo.id_marca_rubro = '.$params['RubroSearch']['id_marca_rubro'].' WHERE rma.id_rubro = rubro.id_rubro LIMIT 1) = '.$params['RubroSearch']['id_marca_rubro'] );
            
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_rubro' => $this->id_rubro,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'tamanio', $this->tamanio])
            ->andFilterWhere(['like', 'unidad_medicion', $this->unidad_medicion]);

        return $dataProvider;
    }
}
