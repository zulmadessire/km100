<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fuera_servicio".
 *
 * @property integer $id
 * @property string $causa
 * @property string $observacion
 * @property integer $id_pipeline
 * @property integer $id_auditoria
 *
 * @property Auditoria $idAuditoria
 * @property PipelineServicio $idPipeline
 */
class FueraServicio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fuera_servicio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pipeline', 'id_auditoria'], 'integer'],
            [['causa'], 'string', 'max' => 450],
            [['observacion'], 'string', 'max' => 600],
            [['id_auditoria'], 'exist', 'skipOnError' => true, 'targetClass' => Auditoria::className(), 'targetAttribute' => ['id_auditoria' => 'id']],
            [['id_pipeline'], 'exist', 'skipOnError' => true, 'targetClass' => PipelineServicio::className(), 'targetAttribute' => ['id_pipeline' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'causa' => 'Causa',
            'observacion' => 'Observacion',
            'id_pipeline' => 'Id Pipeline',
            'id_auditoria' => 'Id Auditoria',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAuditoria()
    {
        return $this->hasOne(Auditoria::className(), ['id' => 'id_auditoria']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPipeline()
    {
        return $this->hasOne(PipelineServicio::className(), ['id' => 'id_pipeline']);
    }
}
