<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DetalleGarantia;

/**
 * DetalleGarantiasSearch represents the model behind the search form about `app\models\DetalleGarantia`.
 */
class DetalleGarantiasSearch extends DetalleGarantia
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'partes_id', 'modelo_id_modelo', 'sucursal_concesionario_id'], 'integer'],
            [['descripion', 'tipo', 'kilometraje'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DetalleGarantia::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'partes_id' => $this->partes_id,
            'modelo_id_modelo' => $this->modelo_id_modelo,
            'sucursal_concesionario_id' => $this->sucursal_concesionario_id,
        ]);

        $query->andFilterWhere(['like', 'descripion', $this->descripion])
            ->andFilterWhere(['like', 'tipo', $this->tipo])
            ->andFilterWhere(['like', 'kilometraje', $this->kilometraje]);

        return $dataProvider;
    }
}
