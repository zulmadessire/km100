<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Auditoria;
use app\models\Estacion;
use yii\db\Query;

/**
 * AuditoriasSearch represents the model behind the search form about `app\models\Auditoria`.
 */
class AuditoriasSearch extends Auditoria
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'estacion_id_estacion'], 'integer'],
            [['motivo', 'planaccion', 'fecha'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Auditoria::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'estacion_id_estacion' => $this->estacion_id_estacion,
            
        ]);

         $query->andFilterWhere([
            'id' => $this->id,
            'estacion_id_estacion' => $this->estacion_id_estacion,
        ]);

         $mes = (date('m'));
        
         $now = getdate();
         $anho = $now['year'];
        
          $query->orFilterWhere(['<', 'MONTH(fecha)', $mes])
            ->orFilterWhere(['<', 'YEAR(fecha)', $anho])
            ->orFilterWhere(['=', 'fecha', '0'])
            ->andFilterWhere(['like', 'motivo', $this->motivo])
            ->andFilterWhere(['like', 'planaccion', $this->planaccion])
            ->andFilterWhere(['like', 'fecha', $this->fecha]);


        return $dataProvider;
    }

         public function searchmensual($params)
    {
 
        //  $query = Auditoria::find()
        // ->where('MONTH(fecha)=MONTH(CURDATE()) AND estado=0');
 


        $query2 = new query();
                $query2->select('es.id_estacion')
                    ->from('estacion es')
                    ->join('INNER JOIN','auditoria_fs aud_fs','es.id_estacion = aud_fs.id_estacion')
                    ->join('INNER JOIN','auditoria_mnto aud_mnto','es.id_estacion = aud_mnto.id_estacion')
                    ->join('INNER JOIN','auditoria_ejec_mant aud_ejec','es.id_estacion = aud_ejec.id_estacion')
                    ->where( 'MONTH(aud_fs.fecha)=MONTH(CURDATE())')
                    ->andwhere( 'YEAR(aud_fs.fecha)=YEAR(CURDATE())')
                    ->andwhere( 'aud_fs.estado=0')
                    ->andwhere( 'MONTH(aud_mnto.fecha)=MONTH(CURDATE())')
                    ->andwhere( 'YEAR(aud_mnto.fecha)=YEAR(CURDATE())')
                    ->andwhere( 'aud_mnto.estado=0')
                    ->andwhere( 'MONTH(aud_ejec.fecha)=MONTH(CURDATE())')
                    ->andwhere( 'YEAR(aud_ejec.fecha)=YEAR(CURDATE())')
                    ->andwhere( 'aud_ejec.estado=0')

        $sql = $query2->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
        $agencia = Yii::$app->db->createCommand($sql)->queryOne();
        $array[]="";
        foreach ($agencia as $value) {
             $array = $value;
        }

 $query=Estacion::find()->Where(['NOT IN','id_estacion', $array]);
 



        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
}

}
