<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "solicitud_compra_proveedor".
 *
 * @property integer $id
 * @property integer $id_solicitud_compra_pieza
 * @property integer $id_proveedor
 *
 * @property CotizacionProveedor[] $cotizacionProveedors
 * @property Proveedor $idProveedor
 * @property SolicitudCompraPieza $idSolicitudCompraPieza
 */
class SolicitudCompraProveedor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'solicitud_compra_proveedor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_solicitud_compra_pieza', 'id_proveedor', 'chasis'], 'required'],
            [['id_solicitud_compra_pieza', 'id_proveedor'], 'integer'],
            [['id_proveedor'], 'exist', 'skipOnError' => true, 'targetClass' => Proveedor::className(), 'targetAttribute' => ['id_proveedor' => 'id']],
            [['id_solicitud_compra_pieza'], 'exist', 'skipOnError' => true, 'targetClass' => SolicitudCompraPieza::className(), 'targetAttribute' => ['id_solicitud_compra_pieza' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_solicitud_compra_pieza' => 'Id Solicitud Compra Pieza',
            'id_proveedor' => 'Id Proveedor',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCotizacionProveedors()
    {
        return $this->hasMany(CotizacionProveedor::className(), ['id_solicitud_compra_proveedor' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProveedor()
    {
        return $this->hasOne(Proveedor::className(), ['id' => 'id_proveedor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSolicitudCompraPieza()
    {
        return $this->hasOne(SolicitudCompraPieza::className(), ['id' => 'id_solicitud_compra_pieza']);
    }
}
