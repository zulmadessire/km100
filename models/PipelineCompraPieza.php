<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pipeline_compra_pieza".
 *
 * @property integer $id
 * @property string $fecha
 * @property integer $id_solicitud_compra_pieza
 * @property integer $id_estado_compra_pieza
 *
 * @property EstadoCompraPieza $idEstadoCompraPieza
 * @property SolicitudCompraPieza $idSolicitudCompraPieza
 */
class PipelineCompraPieza extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pipeline_compra_pieza';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha', 'id_solicitud_compra_pieza', 'id_estado_compra_pieza'], 'required'],
            [['fecha'], 'safe'],
            [['id_solicitud_compra_pieza', 'id_estado_compra_pieza'], 'integer'],
            [['id_estado_compra_pieza'], 'exist', 'skipOnError' => true, 'targetClass' => EstadoCompraPieza::className(), 'targetAttribute' => ['id_estado_compra_pieza' => 'id']],
            [['id_solicitud_compra_pieza'], 'exist', 'skipOnError' => true, 'targetClass' => SolicitudCompraPieza::className(), 'targetAttribute' => ['id_solicitud_compra_pieza' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha' => 'Fecha',
            'id_solicitud_compra_pieza' => 'Id Solicitud Compra Pieza',
            'id_estado_compra_pieza' => 'Id Estado Compra Pieza',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstadoCompraPieza()
    {
        return $this->hasOne(EstadoCompraPieza::className(), ['id' => 'id_estado_compra_pieza']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSolicitudCompraPieza()
    {
        return $this->hasOne(SolicitudCompraPieza::className(), ['id' => 'id_solicitud_compra_pieza']);
    }
}
