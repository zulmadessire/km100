<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "categoria".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $seguimiento_km
 * @property string $declive_km
 * @property string $seguimiento_fecha
 * @property string $declive_fecha
 *
 * @property Vehiculo[] $vehiculos
 */
class Categoria extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categoria';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre', 'seguimiento_km', 'declive_km'], 'string', 'max' => 45],
            [['seguimiento_fecha', 'declive_fecha'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'seguimiento_km' => 'Seguimiento Km',
            'declive_km' => 'Declive Km',
            'seguimiento_fecha' => 'Seguimiento Fecha',
            'declive_fecha' => 'Declive Fecha',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehiculos()
    {
        return $this->hasMany(Vehiculo::className(), ['id_categoria' => 'id']);
    }
}
