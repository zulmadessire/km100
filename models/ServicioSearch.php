<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Servicio;

/**
 * ServicioSearch represents the model behind the search form about `app\models\Servicio`.
 */
class ServicioSearch extends Servicio
{
    /**
     * @inheritdoc
     */
    public $dias, $id_servicio, $id_company, $estatus, $id_cliente, $id_modelo, $taller, $id_marca;

    public function rules()
    {
        return [
            [['id', 'id_servicio','prioridad', 'dano', 'tipo_caso', 'id_estacion_actual', 'id_vehiculo', 'id_company', 'id_analista'], 'integer'],
            [['img_reporte', 'img_acta', 'servicio', 'evaluacion_tecnica', 'responsable', 'fecha', 'ficha', 'placa'], 'safe'],
            [['dias', 'estatus', 'id_company', 'id_cliente', 'id_modelo', 'taller', 'id_marca'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Servicio::find();

        $query->andWhere( '(SELECT ts.codigo FROM pipeline_servicio st INNER JOIN estado_servicio ts ON st.id_estado_servicio = ts.id  WHERE st.id_servicio = servicio.id ORDER BY st.id DESC LIMIT 1) != 8' );

        $query->andWhere( '(SELECT ts.codigo FROM pipeline_servicio st INNER JOIN estado_servicio ts ON st.id_estado_servicio = ts.id  WHERE st.id_servicio = servicio.id ORDER BY st.id DESC LIMIT 1) != 12' );

        //Clientes
        if ( isset( $params['ServicioSearch']['id_cliente'] ) AND $params['ServicioSearch']['id_cliente'] != '') {
            //$query->andWhere( '(SELECT es.id_empresa FROM estacion es INNER JOIN servicio ser ON ser.id_estacion_actual = es.id_estacion INNER JOIN empresa em ON em.id_empresa = es.id_empresa INNER JOIN cliente cli ON cli.id = em.cliente_id WHERE es.id_empresa = id_empresa LIMIT 1) = '.$params['ServicioSearch']['id_empresa'] );
            $query->andWhere( '(SELECT cli.id FROM estacion es INNER JOIN servicio ser ON ser.id_estacion_actual = es.id_estacion INNER JOIN empresa em ON em.id_empresa = es.id_empresa INNER JOIN cliente cli ON cli.id = em.cliente_id WHERE es.id_estacion = servicio.id_estacion_actual LIMIT 1) = '.$params['ServicioSearch']['id_cliente'] );
        }
        //Modelo
        if ( isset( $params['ServicioSearch']['id_modelo'] ) AND $params['ServicioSearch']['id_modelo'] != '') {
            $query->andWhere( '(SELECT mo.id_modelo FROM vehiculo ve INNER JOIN servicio ser ON ser.id_vehiculo = ve.id INNER JOIN modelo mo ON ve.id_modelo = mo.id_modelo  WHERE ve.id = servicio.id_vehiculo LIMIT 1) = '.$params['ServicioSearch']['id_modelo'] );
        }
        //Empresa
        if ( isset( $params['ServicioSearch']['id_company'] ) AND $params['ServicioSearch']['id_company'] != '') {
            $query->andWhere( '(SELECT em.id_empresa FROM estacion es INNER JOIN servicio ser ON ser.id_estacion_actual = es.id_estacion INNER JOIN empresa em ON es.id_empresa = em.id_empresa  WHERE es.id_estacion = servicio.id_estacion_actual LIMIT 1) = '.$params['ServicioSearch']['id_company'] );
        }
        //Dias
        if ( isset( $params['ServicioSearch']['dias'] ) AND $params['ServicioSearch']['dias'] != '') {
            $query->andWhere( '(SELECT DATEDIFF(NOW(), s.fecha) as dias FROM servicio s WHERE s.id = servicio.id) = '.$params['ServicioSearch']['dias'] );
        }
        //Estatus
        if ( isset( $params['ServicioSearch']['estatus'] ) AND $params['ServicioSearch']['estatus'] != '') {
            $query->andWhere( '(SELECT p.id_estado_servicio FROM pipeline_servicio p WHERE p.id_servicio = servicio.id ORDER BY p.id DESC LIMIT 1) = '.$params['ServicioSearch']['estatus'] );
        }
        // add conditions that should always apply here
        /*$query->joinWith(['pipelineServicios']);

        $subQuery = '(SELECT MAX(id) id FROM pipeline_servicio GROUP BY id_servicio)';

        $query->innerJoin(['p' => $subQuery], 'p.id = pipeline_servicio.id');*/

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'prioridad' => $this->prioridad,
            'dano' => $this->dano,
            'fecha' => $this->fecha,
            'tipo_caso' => $this->tipo_caso,
            'id_estacion_actual' => $this->id_estacion_actual,
            'id_vehiculo' => $this->id_vehiculo,
        ]);

        $query->andFilterWhere(['like', 'img_reporte', $this->img_reporte])
            ->andFilterWhere(['like', 'img_acta', $this->img_acta])
            ->andFilterWhere(['like', 'servicio', $this->servicio])
            ->andFilterWhere(['like', 'evaluacion_tecnica', $this->evaluacion_tecnica])
            ->andFilterWhere(['like', 'responsable', $this->responsable])
            ->andFilterWhere(['like', 'ficha', $this->ficha])
            ->andFilterWhere(['like', 'placa', $this->placa]);
            /*->andFilterWhere(['!=', 'pipeline_servicio.id_estado_servicio', 9])
            ->andFilterWhere(['!=', 'pipeline_servicio.id_estado_servicio', 12]);*/

        return $dataProvider;
    }



    public function searchClose($params)
    {
        $query = Servicio::find();

        $query->andWhere( '(SELECT ts.codigo FROM pipeline_servicio st INNER JOIN estado_servicio ts ON st.id_estado_servicio = ts.id  WHERE st.id_servicio = servicio.id ORDER BY st.id DESC LIMIT 1) = 8' );

        $query->orWhere( '(SELECT ts.codigo FROM pipeline_servicio st INNER JOIN estado_servicio ts ON st.id_estado_servicio = ts.id  WHERE st.id_servicio = servicio.id ORDER BY st.id DESC LIMIT 1) = 12' );

        //Modelo
        if ( isset( $params['ServicioSearch']['id_modelo'] ) AND $params['ServicioSearch']['id_modelo'] != '') {
            $query->andWhere( '(SELECT mo.id_modelo FROM vehiculo ve INNER JOIN servicio ser ON ser.id_vehiculo = ve.id INNER JOIN modelo mo ON ve.id_modelo = mo.id_modelo  WHERE ve.id = servicio.id_vehiculo LIMIT 1) = '.$params['ServicioSearch']['id_modelo'] );
        }
        //Empresa
        if ( isset( $params['ServicioSearch']['id_company'] ) AND $params['ServicioSearch']['id_company'] != '') {
            $query->andWhere( '(SELECT em.id_empresa FROM estacion es INNER JOIN servicio ser ON ser.id_estacion_actual = es.id_estacion INNER JOIN empresa em ON es.id_empresa = em.id_empresa  WHERE es.id_estacion = servicio.id_estacion_actual LIMIT 1) = '.$params['ServicioSearch']['id_company'] );
        }
        //Dias
        if ( isset( $params['ServicioSearch']['dias'] ) AND $params['ServicioSearch']['dias'] != '') {
            $query->andWhere( '(SELECT DATEDIFF(NOW(), s.fecha) as dias FROM servicio s WHERE s.id = servicio.id) = '.$params['ServicioSearch']['dias'] );
        }
        //Estatus
        if ( isset( $params['ServicioSearch']['estatus'] ) AND $params['ServicioSearch']['estatus'] != '') {
            $query->andWhere( '(SELECT p.id_estado_servicio FROM pipeline_servicio p WHERE p.id_servicio = servicio.id ORDER BY p.id DESC LIMIT 1) = '.$params['ServicioSearch']['estatus'] );
        }
        // add conditions that should always apply here

        
        //$query->joinWith(['pipelineServicios']);     
       

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'prioridad' => $this->prioridad,
            'dano' => $this->dano,
            'fecha' => $this->fecha,
            'tipo_caso' => $this->tipo_caso,
            'id_estacion_actual' => $this->id_estacion_actual,
            'id_vehiculo' => $this->id_vehiculo,
        ]);

        $query->andFilterWhere(['like', 'img_reporte', $this->img_reporte])
            ->andFilterWhere(['like', 'img_acta', $this->img_acta])
            ->andFilterWhere(['like', 'servicio', $this->servicio])
            ->andFilterWhere(['like', 'evaluacion_tecnica', $this->evaluacion_tecnica])
            ->andFilterWhere(['like', 'responsable', $this->responsable])
            ->andFilterWhere(['like', 'ficha', $this->ficha])
            ->andFilterWhere(['like', 'placa', $this->placa]);
            /*->andFilterWhere(['=', 'pipeline_servicio.id_estado_servicio', 9])
            ->orFilterWhere(['=', 'pipeline_servicio.id_estado_servicio', 12]);*/


            //var_dump($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);
            //exit();


        return $dataProvider;
    }

    public function searchRealizados($params)
    {
        $query = Servicio::find();

        $query->andWhere( '(SELECT ts.codigo FROM pipeline_servicio st INNER JOIN estado_servicio ts ON st.id_estado_servicio = ts.id  WHERE st.id_servicio = servicio.id ORDER BY st.id DESC LIMIT 1) = 6' );

        $query->orWhere( '(SELECT ts.codigo FROM pipeline_servicio st INNER JOIN estado_servicio ts ON st.id_estado_servicio = ts.id  WHERE st.id_servicio = servicio.id ORDER BY st.id DESC LIMIT 1) = 7' );

        $query->orWhere( '(SELECT ts.codigo FROM pipeline_servicio st INNER JOIN estado_servicio ts ON st.id_estado_servicio = ts.id  WHERE st.id_servicio = servicio.id ORDER BY st.id DESC LIMIT 1) = 8' );

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'prioridad' => $this->prioridad,
            'dano' => $this->dano,
            'fecha' => $this->fecha,
            'tipo_caso' => $this->tipo_caso,
            'id_estacion_actual' => $this->id_estacion_actual,
            'id_vehiculo' => $this->id_vehiculo,
        ]);

        $query->andFilterWhere(['like', 'img_reporte', $this->img_reporte])
            ->andFilterWhere(['like', 'img_acta', $this->img_acta])
            ->andFilterWhere(['like', 'servicio', $this->servicio])
            ->andFilterWhere(['like', 'evaluacion_tecnica', $this->evaluacion_tecnica])
            ->andFilterWhere(['like', 'responsable', $this->responsable])
            ->andFilterWhere(['like', 'ficha', $this->ficha])
            ->andFilterWhere(['like', 'placa', $this->placa]);

        return $dataProvider;
    }

    public function gestionadossearch($params)
    {
        $query = Servicio::find();

        //Dias
        if ( isset( $params['ServicioSearch']['dias'] ) AND $params['ServicioSearch']['dias'] != '') {
            $query->andWhere( '(SELECT DATEDIFF(NOW(), s.fecha) as dias FROM servicio s WHERE s.id = servicio.id) = '.$params['ServicioSearch']['dias'] );
        }
        //Servicios
        if ( isset( $params['ServicioSearch']['id_servicio'] ) AND $params['ServicioSearch']['id_servicio'] != '') {
            $query->andWhere( '(SELECT ts.id FROM pipeline_servicio st INNER JOIN estado_servicio ts ON st.id_estado_servicio = ts.id  WHERE st.id_servicio = servicio.id ORDER BY st.id DESC LIMIT 1) = '.$params['ServicioSearch']['id_servicio'] );    
            //var_dump($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);
            //  exit();
        }
        //Modelo
        if ( isset( $params['ServicioSearch']['id_modelo'] ) AND $params['ServicioSearch']['id_modelo'] != '') {
            $query->andWhere( '(SELECT mo.id_modelo FROM vehiculo ve INNER JOIN servicio ser ON ser.id_vehiculo = ve.id INNER JOIN modelo mo ON ve.id_modelo = mo.id_modelo  WHERE ve.id = servicio.id_vehiculo LIMIT 1) = '.$params['ServicioSearch']['id_modelo'] );
        }
        //Clientes
         if ( isset( $params['ServicioSearch']['id_empresa'] ) AND $params['ServicioSearch']['id_empresa'] != '') {
            //$query->andWhere( '(SELECT es.id_empresa FROM estacion es INNER JOIN servicio ser ON ser.id_estacion_actual = es.id_estacion INNER JOIN empresa em ON em.id_empresa = es.id_empresa INNER JOIN cliente cli ON cli.id = em.cliente_id WHERE es.id_empresa = id_empresa LIMIT 1) = '.$params['ServicioSearch']['id_empresa'] );
            $query->andWhere( '(SELECT cli.id FROM estacion es INNER JOIN servicio ser ON ser.id_estacion_actual = es.id_estacion INNER JOIN empresa em ON em.id_empresa = es.id_empresa INNER JOIN cliente cli ON cli.id = em.cliente_id WHERE es.id_estacion = servicio.id_estacion_actual LIMIT 1) = '.$params['ServicioSearch']['id_empresa'] );
        }
        //$query->andWhere( '(SELECT p.id_estado_servicio FROM pipeline_servicio p INNER JOIN estado_servicio es ON es.id = p.id_estado_servicio WHERE p.id_servicio = servicio.id ORDER BY p.id DESC LIMIT 1) != 10');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'prioridad' => $this->prioridad,
            'dano' => $this->dano,
            'fecha' => $this->fecha,
            'tipo_caso' => $this->tipo_caso,
            'id_estacion_actual' => $this->id_estacion_actual,
            'id_vehiculo' => $this->id_vehiculo,
        ]);

        $query->andFilterWhere(['like', 'img_reporte', $this->img_reporte])
            ->andFilterWhere(['like', 'img_acta', $this->img_acta])
            ->andFilterWhere(['like', 'servicio', $this->servicio])
            ->andFilterWhere(['like', 'evaluacion_tecnica', $this->evaluacion_tecnica])
            ->andFilterWhere(['like', 'responsable', $this->responsable])
            ->andFilterWhere(['like', 'ficha', $this->ficha])
            ->andFilterWhere(['like', 'placa', $this->placa]);

        return $dataProvider;
    }

    public function serviciotallersearch($params, $id_taller)
    {

        $query = Servicio::find();

        $query->andWhere( '(SELECT sst.id_taller FROM solicitud_servicio_taller sst WHERE sst.id_servicio = servicio.id) = '.$id_taller);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'prioridad' => $this->prioridad,
            'dano' => $this->dano,
            'fecha' => $this->fecha,
            'tipo_caso' => $this->tipo_caso,
            'id_estacion_actual' => $this->id_estacion_actual,
            'id_vehiculo' => $this->id_vehiculo,
        ]);

        $query->andFilterWhere(['like', 'img_reporte', $this->img_reporte])
            ->andFilterWhere(['like', 'img_acta', $this->img_acta])
            ->andFilterWhere(['like', 'servicio', $this->servicio])
            ->andFilterWhere(['like', 'evaluacion_tecnica', $this->evaluacion_tecnica])
            ->andFilterWhere(['like', 'responsable', $this->responsable])
            ->andFilterWhere(['like', 'ficha', $this->ficha])
            ->andFilterWhere(['like', 'placa', $this->placa]);

        return $dataProvider;
    }

    public function searchEstatus($params)
    {
        $query = Servicio::find();
        if ( isset( $params['ServicioSearch']['id_empresa'] ) AND $params['ServicioSearch']['id_empresa'] != '') {
            $query->andWhere( '(SELECT es.id_empresa FROM estacion es INNER JOIN servicio ser ON ser.id_estacion_actual = es.id_estacion INNER JOIN empresa em ON em.id_empresa = es.id_empresa INNER JOIN cliente cli ON cli.id = em.cliente_id WHERE es.id_empresa = id_empresa LIMIT 1) = '.$params['ServicioSearch']['id_empresa'] );

        }
   
        if ( isset( $params['ServicioSearch']['id_servicio'] ) AND $params['ServicioSearch']['id_servicio'] != '') {
            $query->andWhere( '(SELECT ts.id FROM pipeline_servicio st INNER JOIN estado_servicio ts ON st.id_estado_servicio = ts.id AND ts.id = '.$params['ServicioSearch']['id_servicio'].' WHERE st.id_servicio = servicio.id LIMIT 1) = '.$params['ServicioSearch']['id_servicio'] );    
            
        }
        //Dias
        if ( isset( $params['ServicioSearch']['dias'] ) AND $params['ServicioSearch']['dias'] != '') {
            $query->andWhere( '(SELECT DATEDIFF(NOW(), s.fecha) as dias FROM servicio s WHERE s.id = servicio.id) = '.$params['ServicioSearch']['dias'] );
        }
        // add conditions that should always apply here

        
        $query->joinWith(['pipelineServicios']);     
       

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'prioridad' => $this->prioridad,
            'dano' => $this->dano,
            'fecha' => $this->fecha,
            'tipo_caso' => $this->tipo_caso,
            'id_estacion_actual' => $this->id_estacion_actual,
            'id_vehiculo' => $this->id_vehiculo,
        ]);

        $query->andFilterWhere(['like', 'img_reporte', $this->img_reporte])
            ->andFilterWhere(['like', 'img_acta', $this->img_acta])
            ->andFilterWhere(['like', 'servicio', $this->servicio])
            ->andFilterWhere(['like', 'evaluacion_tecnica', $this->evaluacion_tecnica])
            ->andFilterWhere(['like', 'responsable', $this->responsable])
            ->andFilterWhere(['like', 'ficha', $this->ficha])
            ->andFilterWhere(['like', 'placa', $this->placa])
            ->andFilterWhere(['=', 'pipeline_servicio.id_estado_servicio', 3]);


            // var_dump($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);
            // exit();


        return $dataProvider;
    }

     public function searchGerente($params)
    {
        $query = Servicio::find();

        //Estatus
        $query->andWhere( '(SELECT ts.codigo FROM pipeline_servicio st INNER JOIN estado_servicio ts ON st.id_estado_servicio = ts.id  WHERE st.id_servicio = servicio.id ORDER BY st.id DESC LIMIT 1) = 2' );
        
        //Clientes
        if ( isset( $params['ServicioSearch']['id_empresa'] ) AND $params['ServicioSearch']['id_empresa'] != '') {
            //$query->andWhere( '(SELECT es.id_empresa FROM estacion es INNER JOIN servicio ser ON ser.id_estacion_actual = es.id_estacion INNER JOIN empresa em ON em.id_empresa = es.id_empresa INNER JOIN cliente cli ON cli.id = em.cliente_id WHERE es.id_empresa = id_empresa LIMIT 1) = '.$params['ServicioSearch']['id_empresa'] );
            $query->andWhere( '(SELECT cli.id FROM estacion es INNER JOIN servicio ser ON ser.id_estacion_actual = es.id_estacion INNER JOIN empresa em ON em.id_empresa = es.id_empresa INNER JOIN cliente cli ON cli.id = em.cliente_id WHERE es.id_estacion = servicio.id_estacion_actual LIMIT 1) = '.$params['ServicioSearch']['id_empresa'] );
        }
        //Modelo
        if ( isset( $params['ServicioSearch']['id_modelo'] ) AND $params['ServicioSearch']['id_modelo'] != '') {
            $query->andWhere( '(SELECT mo.id_modelo FROM vehiculo ve INNER JOIN servicio ser ON ser.id_vehiculo = ve.id INNER JOIN modelo mo ON ve.id_modelo = mo.id_modelo  WHERE ve.id = servicio.id_vehiculo LIMIT 1) = '.$params['ServicioSearch']['id_modelo'] );
        }
        //Empresa
        if ( isset( $params['ServicioSearch']['id_company'] ) AND $params['ServicioSearch']['id_company'] != '') {
            $query->andWhere( '(SELECT em.id_empresa FROM estacion es INNER JOIN servicio ser ON ser.id_estacion_actual = es.id_estacion INNER JOIN empresa em ON es.id_empresa = em.id_empresa  WHERE es.id_estacion = servicio.id_estacion_actual LIMIT 1) = '.$params['ServicioSearch']['id_company'] );
        }
        //Dias
        if ( isset( $params['ServicioSearch']['dias'] ) AND $params['ServicioSearch']['dias'] != '') {
            $query->andWhere( '(SELECT DATEDIFF(NOW(), s.fecha) as dias FROM servicio s WHERE s.id = servicio.id) = '.$params['ServicioSearch']['dias'] );
        }
        //Nivel
        if ( isset( $params['ServicioSearch']['nivel'] ) AND $params['ServicioSearch']['nivel'] != '') {              
            $query->andWhere( '(SELECT c.activo FROM cotizacion_taller c INNER JOIN solicitud_servicio_taller st ON st.id = c.id_solicitud_servicio_taller 
            WHERE st.id_servicio = servicio.id  LIMIT 1) = '.$params['ServicioSearch']['nivel'] );
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'prioridad' => $this->prioridad,
            'dano' => $this->dano,
            'fecha' => $this->fecha,
            'tipo_caso' => $this->tipo_caso,
            'id_estacion_actual' => $this->id_estacion_actual,
            'id_vehiculo' => $this->id_vehiculo,
            'id_analista' => $this->id_analista,
        ]);

        $query->andFilterWhere(['like', 'img_reporte', $this->img_reporte])
            ->andFilterWhere(['like', 'img_acta', $this->img_acta])
            ->andFilterWhere(['like', 'servicio', $this->servicio])
            ->andFilterWhere(['like', 'evaluacion_tecnica', $this->evaluacion_tecnica])
            ->andFilterWhere(['like', 'responsable', $this->responsable])
            ->andFilterWhere(['like', 'ficha', $this->ficha])
            ->andFilterWhere(['like', 'placa', $this->placa]);
            
        return $dataProvider;
    }

    public function searchEnvioEstacion($params){

        $query = Servicio::find();

        //Estatus
        $query->andWhere( '(SELECT ts.codigo FROM pipeline_servicio st INNER JOIN estado_servicio ts ON st.id_estado_servicio = ts.id  WHERE st.id_servicio = servicio.id ORDER BY st.id DESC LIMIT 1) = 11' );

        $query->andWhere('aprobacion_estacion = 0');

        //Marca
        if ( isset( $params['ServicioSearch']['id_marca'] ) AND $params['ServicioSearch']['id_marca'] != '') {
            $query->andWhere( '(SELECT mo.id_marca FROM vehiculo ve INNER JOIN servicio ser ON ser.id_vehiculo = ve.id INNER JOIN modelo mo ON ve.id_modelo = mo.id_modelo  WHERE ve.id = servicio.id_vehiculo LIMIT 1) = '.$params['ServicioSearch']['id_marca'] );
        }

        //Modelo
        if ( isset( $params['ServicioSearch']['id_modelo'] ) AND $params['ServicioSearch']['id_modelo'] != '') {
            $query->andWhere( '(SELECT mo.id_modelo FROM vehiculo ve INNER JOIN servicio ser ON ser.id_vehiculo = ve.id INNER JOIN modelo mo ON ve.id_modelo = mo.id_modelo  WHERE ve.id = servicio.id_vehiculo LIMIT 1) = '.$params['ServicioSearch']['id_modelo'] );
        }

        //Taller
        if ( isset( $params['ServicioSearch']['taller'] ) AND $params['ServicioSearch']['taller'] != '') {
            $query->andWhere( '(SELECT id_taller FROM solicitud_servicio_taller WHERE id_servicio = servicio.id LIMIT 1) = '.$params['ServicioSearch']['taller'] );
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'prioridad' => $this->prioridad,
            'dano' => $this->dano,
            'fecha' => $this->fecha,
            'tipo_caso' => $this->tipo_caso,
            'id_estacion_actual' => $this->id_estacion_actual,
            'id_vehiculo' => $this->id_vehiculo,
            'id_analista' => $this->id_analista,
        ]);

        $query->andFilterWhere(['like', 'img_reporte', $this->img_reporte])
            ->andFilterWhere(['like', 'img_acta', $this->img_acta])
            ->andFilterWhere(['like', 'servicio', $this->servicio])
            ->andFilterWhere(['like', 'evaluacion_tecnica', $this->evaluacion_tecnica])
            ->andFilterWhere(['like', 'responsable', $this->responsable])
            ->andFilterWhere(['like', 'ficha', $this->ficha])
            ->andFilterWhere(['like', 'placa', $this->placa]);
            
        return $dataProvider;
    }

    public function searchAsignacion($params)
    {
        $query = Servicio::find();

        //Estatus
        $query->andWhere( '(SELECT ts.codigo FROM pipeline_servicio st INNER JOIN estado_servicio ts ON st.id_estado_servicio = ts.id  WHERE st.id_servicio = servicio.id ORDER BY st.id DESC LIMIT 1) = 11' );
        
        //Clientes
        if ( isset( $params['ServicioSearch']['id_empresa'] ) AND $params['ServicioSearch']['id_empresa'] != '') {
            //$query->andWhere( '(SELECT es.id_empresa FROM estacion es INNER JOIN servicio ser ON ser.id_estacion_actual = es.id_estacion INNER JOIN empresa em ON em.id_empresa = es.id_empresa INNER JOIN cliente cli ON cli.id = em.cliente_id WHERE es.id_empresa = id_empresa LIMIT 1) = '.$params['ServicioSearch']['id_empresa'] );
            $query->andWhere( '(SELECT cli.id FROM estacion es INNER JOIN servicio ser ON ser.id_estacion_actual = es.id_estacion INNER JOIN empresa em ON em.id_empresa = es.id_empresa INNER JOIN cliente cli ON cli.id = em.cliente_id WHERE es.id_estacion = servicio.id_estacion_actual LIMIT 1) = '.$params['ServicioSearch']['id_empresa'] );
        }
        //Modelo
        if ( isset( $params['ServicioSearch']['id_modelo'] ) AND $params['ServicioSearch']['id_modelo'] != '') {
            $query->andWhere( '(SELECT mo.id_modelo FROM vehiculo ve INNER JOIN servicio ser ON ser.id_vehiculo = ve.id INNER JOIN modelo mo ON ve.id_modelo = mo.id_modelo  WHERE ve.id = servicio.id_vehiculo LIMIT 1) = '.$params['ServicioSearch']['id_modelo'] );
        }
        //Empresa
        if ( isset( $params['ServicioSearch']['id_company'] ) AND $params['ServicioSearch']['id_company'] != '') {
            $query->andWhere( '(SELECT em.id_empresa FROM estacion es INNER JOIN servicio ser ON ser.id_estacion_actual = es.id_estacion INNER JOIN empresa em ON es.id_empresa = em.id_empresa  WHERE es.id_estacion = servicio.id_estacion_actual LIMIT 1) = '.$params['ServicioSearch']['id_company'] );
        }
        //Dias
        if ( isset( $params['ServicioSearch']['dias'] ) AND $params['ServicioSearch']['dias'] != '') {
            $query->andWhere( '(SELECT DATEDIFF(NOW(), s.fecha) as dias FROM servicio s WHERE s.id = servicio.id) = '.$params['ServicioSearch']['dias'] );
        }
        //Nivel
        if ( isset( $params['ServicioSearch']['nivel'] ) AND $params['ServicioSearch']['nivel'] != '') {              
            $query->andWhere( '(SELECT c.activo FROM cotizacion_taller c INNER JOIN solicitud_servicio_taller st ON st.id = c.id_solicitud_servicio_taller 
            WHERE st.id_servicio = servicio.id  LIMIT 1) = '.$params['ServicioSearch']['nivel'] );
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'prioridad' => $this->prioridad,
            'dano' => $this->dano,
            'fecha' => $this->fecha,
            'tipo_caso' => $this->tipo_caso,
            'id_estacion_actual' => $this->id_estacion_actual,
            'id_vehiculo' => $this->id_vehiculo,
            'id_analista' => $this->id_analista,
        ]);

        $query->andFilterWhere(['like', 'img_reporte', $this->img_reporte])
            ->andFilterWhere(['like', 'img_acta', $this->img_acta])
            ->andFilterWhere(['like', 'servicio', $this->servicio])
            ->andFilterWhere(['like', 'evaluacion_tecnica', $this->evaluacion_tecnica])
            ->andFilterWhere(['like', 'responsable', $this->responsable])
            ->andFilterWhere(['like', 'ficha', $this->ficha])
            ->andFilterWhere(['like', 'placa', $this->placa]);
            
        return $dataProvider;
    }

}
