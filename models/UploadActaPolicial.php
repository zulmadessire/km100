<?php
namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;
use app\models\Servicio;
use yii\imagine\Image;  
use Imagine\Image\Box;


class UploadActaPolicial extends Model
{
    /**
     * @var UploadedFile[]
     */

    public $imageFiles;

    public function rules()
    {
        return [
            [['imageFiles'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'maxFiles' => 1, 'maxSize' => 4194304],
            [['imageFiles'], 'required', 'on' => 'acta', 'whenClient' => "function (tipo_caso, value) {
                                                            return ($('#tipo_caso').val() == 0);
                                                        }"],
        ];
    }

    public function attributeLabels()
    {
        return [
            'imageFiles' => 'ACTA POLICIAL',
        ];
    }

    public function upload($id_servicio)
    {
        $basePath = '/uploads/servicios/';
 
        //if ($this->validate()) {
            foreach ($this->imageFiles as $i => $file) {
                $soporte = Servicio::find()->where(['id' => $id_servicio])->One();

                if ($soporte->img_acta) {
                    unlink('.'.$soporte->img_acta);
                }

                $file->saveAs('.'.$basePath . $id_servicio . '/policial-'.$id_servicio.'.'.$file->extension);

                Image::resize('.'.$basePath . $id_servicio . '/policial-'.$id_servicio.'.'.$file->extension, 1200, 800)
                ->save('.'.$basePath . $id_servicio . '/policial-'.$id_servicio.'.'.$file->extension, 
                        ['quality' => 70]);

                $soporte->img_acta=$basePath . $id_servicio . '/policial-'.$id_servicio.'.'.$file->extension;
                $soporte->save();
            }
            return true;
        /*} else {
          die('aqui oy');
            return false;
        }*/
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['acta'] = ['imageFiles'];//Scenario Values Only Accepted
        return $scenarios;
    }
}
?>