<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "licitacion_solicitud".
 *
 * @property integer $id
 * @property integer $id_solicitud
 * @property integer $id_licitacion
 *
 * @property Licitacion $idLicitacion
 * @property Solicitudes $idSolicitud
 */
class LicitacionSolicitud extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'licitacion_solicitud';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_solicitud', 'id_licitacion'], 'integer'],
            [['id_licitacion'], 'exist', 'skipOnError' => true, 'targetClass' => Licitacion::className(), 'targetAttribute' => ['id_licitacion' => 'id']],
            [['id_solicitud'], 'exist', 'skipOnError' => true, 'targetClass' => Solicitudes::className(), 'targetAttribute' => ['id_solicitud' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_solicitud' => 'Id Solicitud',
            'id_licitacion' => 'Id Licitacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLicitacion()
    {
        return $this->hasOne(Licitacion::className(), ['id' => 'id_licitacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSolicitud()
    {
        return $this->hasOne(Solicitudes::className(), ['id' => 'id_solicitud']);
    }
}
