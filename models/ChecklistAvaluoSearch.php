<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ChecklistAvaluo;

/**
 * ChecklistAvaluoSearch represents the model behind the search form of `app\models\ChecklistAvaluo`.
 */
class ChecklistAvaluoSearch extends ChecklistAvaluo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_cotizacion_taller_analista', 'id_servicio', 'id_analista'], 'integer'],
            [['danos', 'diagnostico'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ChecklistAvaluo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_cotizacion_taller_analista' => $this->id_cotizacion_taller_analista,
            'id_servicio' => $this->id_servicio,
            'id_analista' => $this->id_analista,
        ]);

        $query->andFilterWhere(['like', 'danos', $this->danos])
            ->andFilterWhere(['like', 'diagnostico', $this->diagnostico]);

        return $dataProvider;
    }
}
