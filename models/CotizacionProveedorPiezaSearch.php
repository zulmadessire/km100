<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CotizacionProveedorPieza;

/**
 * CotizacionProveedorPiezaSearch represents the model behind the search form of `app\models\CotizacionProveedorPieza`.
 */
class CotizacionProveedorPiezaSearch extends CotizacionProveedorPieza
{
    public $estado_despacho, $seriales;
    /**
     * @inheritdoc
     */
    public $id_solicitud, $tipo_pieza;
    public function rules()
    {
        return [
            [['id', 'cantidad_disponible', 'cantidad_solicitada', 'ahorro', 'id_cotizacion_proveedor', 'id_cotizacion_taller_pieza', 'cantidad_comprar', 'aprobacion', 'cantidad_recibida', 'inventario', 'estado_despacho', 'id_pieza'], 'integer'],
            [['fecha_entrega', 'garantia', 'serial','seriales'], 'safe'],
            [['costo'], 'number'],
            [['id_solicitud'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CotizacionProveedorPieza::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'cantidad_disponible' => $this->cantidad_disponible,
            'cantidad_solicitada' => $this->cantidad_solicitada,
            'fecha_entrega' => $this->fecha_entrega,
            'costo' => $this->costo,
            'ahorro' => $this->ahorro,
            'id_cotizacion_proveedor' => $this->id_cotizacion_proveedor,
            'id_cotizacion_taller_pieza' => $this->id_cotizacion_taller_pieza,
            'cantidad_comprar' => $this->cantidad_comprar,
            'aprobacion' => $this->aprobacion,
            'cantidad_recibida' => $this->cantidad_recibida,
            'inventario' => $this->inventario,
            'estado_despacho' => $this->estado_despacho,
            'id_pieza' => $this->id_pieza,
        ]);

        $query->andFilterWhere(['like', 'garantia', $this->garantia])
            ->andFilterWhere(['like', 'serial', $this->serial]);
            //  var_dump($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);
            //  exit();
        return $dataProvider;
    }

    public function search2($params)
    {
        $query = CotizacionProveedorPieza::find();

        // add conditions that should always apply here

    
        //tipopieza
        if ( isset( $params['CotizacionProveedorPiezaSearch']['tipo_pieza'] ) AND $params['CotizacionProveedorPiezaSearch']['tipo_pieza'] != '') {
 
            $query->andWhere( '(SELECT tp.id FROM Cotizacion_proveedor_pieza cpp INNER JOIN cotizacion_taller_pieza ctp ON cpp.id_cotizacion_taller_pieza = ctp.id INNER JOIN pieza pi ON ctp.id_pieza=pi.id_pieza INNER JOIN Tipo_Pieza tp ON pi.tipo_pieza_id = tp.id WHERE ctp.id = Cotizacion_proveedor_pieza.id_cotizacion_taller_pieza LIMIT 1) = '.$params['CotizacionProveedorPiezaSearch']['tipo_pieza'] );
 
        }
        //pieza
        if ( isset( $params['CotizacionProveedorPiezaSearch']['pieza'] ) AND $params['CotizacionProveedorPiezaSearch']['pieza'] != '') {
 
            $query->andWhere( '(SELECT pi.id_pieza FROM Cotizacion_proveedor_pieza cpp INNER JOIN cotizacion_taller_pieza ctp ON cpp.id_cotizacion_taller_pieza = ctp.id INNER JOIN pieza pi ON ctp.id_pieza=pi.id_pieza WHERE ctp.id = Cotizacion_proveedor_pieza.id_cotizacion_taller_pieza LIMIT 1) = '.$params['CotizacionProveedorPiezaSearch']['pieza'] );
 
        }


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        //id solicitud
        if ( isset($this->id_solicitud) ) {
            $query->joinWith('idCotizacionProveedor')->joinWith('idCotizacionProveedor.idSolicitudCompraProveedor')->joinWith('idCotizacionProveedor.idSolicitudCompraProveedor.idSolicitudCompraPieza')->where('solicitud_compra_pieza.id = '. $this->id_solicitud );
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'cantidad_disponible' => $this->cantidad_disponible,
            'cantidad_solicitada' => $this->cantidad_solicitada,
            'cantidad_recibida' => $this->cantidad_recibida,
            'fecha_entrega' => $this->fecha_entrega,
            'costo' => $this->costo,
            'ahorro' => $this->ahorro,
            'id_cotizacion_proveedor' => $this->id_cotizacion_proveedor,
            'id_cotizacion_taller_pieza' => $this->id_cotizacion_taller_pieza,
            'inventario' => $this->inventario,
        ]);

        $query->andFilterWhere(['like', 'serial', $this->serial]);

        return $dataProvider;
    }

    public function searchHistoricoPieza($params)
    {
        $query = CotizacionProveedorPieza::find()
        ->join('JOIN','orden_compra_pieza','orden_compra_pieza.id_cotizacion_proveedor = cotizacion_proveedor_pieza.id_cotizacion_proveedor')
        ->where('orden_compra_pieza.estado = 0');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'cantidad_disponible' => $this->cantidad_disponible,
            'cantidad_solicitada' => $this->cantidad_solicitada,
            'fecha_entrega' => $this->fecha_entrega,
            'costo' => $this->costo,
            'ahorro' => $this->ahorro,
            'id_cotizacion_proveedor' => $this->id_cotizacion_proveedor,
            'id_cotizacion_taller_pieza' => $this->id_cotizacion_taller_pieza,
            'cantidad_comprar' => $this->cantidad_comprar,
            'aprobacion' => 2,
            'cantidad_recibida' => $this->cantidad_recibida,
            'inventario' => $this->inventario,
            'estado_despacho' => $this->estado_despacho,
            'id_pieza' => $this->id_pieza,
        ]);

        $query->andFilterWhere(['like', 'garantia', $this->garantia])
            ->andFilterWhere(['like', 'serial', $this->serial]);
            //  var_dump($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);
            //  exit();
        return $dataProvider;
    }
}
