<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ServicioVehiculoEscaneado;

/**
 * ServicioVehiculoEscaneadoSearch represents the model behind the search form about `app\models\ServicioVehiculoEscaneado`.
 */
class ServicioVehiculoEscaneadoSearch extends ServicioVehiculoEscaneado
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_servicio'], 'integer'],
            [['ficha', 'modelo', 'empresa', 'tipo_solicitud'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ServicioVehiculoEscaneado::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_servicio' => $this->id_servicio,
        ]);

        $query->andFilterWhere(['like', 'ficha', $this->ficha])
            ->andFilterWhere(['like', 'modelo', $this->modelo])
            ->andFilterWhere(['like', 'empresa', $this->empresa])
            ->andFilterWhere(['like', 'tipo_solicitud', $this->tipo_solicitud]);

        return $dataProvider;
    }
}
