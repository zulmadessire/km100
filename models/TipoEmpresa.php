<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipo_empresa".
 *
 * @property integer $id
 * @property string $nombre
 *
 * @property Proveedor[] $proveedors
 */
class TipoEmpresa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipo_empresa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'TIPO DE EMPRESA',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProveedors()
    {
        return $this->hasMany(Proveedor::className(), ['tipo_empresa_id' => 'id']);
    }
}
