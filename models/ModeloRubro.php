<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "modelo_rubro".
 *
 * @property integer $id_modelo_rubro
 * @property string $nombre
 * @property integer $id_marca_rubro
 *
 * @property MarcaRubro $idMarcaRubro
 * @property RubroModeloAsignado[] $rubroModeloAsignados
 */
class ModeloRubro extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'modelo_rubro';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'id_marca_rubro'], 'required'],
            [['id_marca_rubro'], 'integer'],
            [['nombre'], 'string', 'max' => 50],
            [['id_marca_rubro'], 'exist', 'skipOnError' => true, 'targetClass' => MarcaRubro::className(), 'targetAttribute' => ['id_marca_rubro' => 'id_marca_rubro']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_modelo_rubro' => 'Id Modelo Rubro',
            'nombre' => 'Nombre',
            'id_marca_rubro' => 'Id Marca Rubro',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMarcaRubro()
    {
        return $this->hasOne(MarcaRubro::className(), ['id_marca_rubro' => 'id_marca_rubro']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRubroModeloAsignados()
    {
        return $this->hasMany(RubroModeloAsignado::className(), ['id_modelo_rubro' => 'id_modelo_rubro']);
    }
}
