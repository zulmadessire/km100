<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estacion".
 *
 * @property integer $id_estacion
 * @property string $codigo
 * @property string $nombre
 * @property integer $id_empresa
 * @property integer $coordinador
 * @property string $direccion
 * @property string $coord_google_maps
 * @property integer $id_wizard
 * @property integer $asoc_taller
 * @property string $codigo_generado
 *
 * @property AuditoriaEjecMant[] $auditoriaEjecMants
 * @property AuditoriaFs[] $auditoriaFs
 * @property AuditoriaMnto[] $auditoriaMntos
 * @property Caso[] $casos
 * @property Despacho[] $despachos
 * @property Empresa $idEmpresa
 * @property EstacionUsuario[] $estacionUsuarios
 * @property MantenimientoPreventivo[] $mantenimientoPreventivos
 * @property Servicio[] $servicios
 */
class Estacion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'estacion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'id_empresa', 'direccion', 'coord_google_maps', 'id_wizard', 'asoc_taller'], 'required'],
            [['id_empresa', 'coordinador', 'id_wizard', 'asoc_taller'], 'integer'],
            [['codigo', 'codigo_generado'], 'string', 'max' => 10],
            [['nombre'], 'string', 'max' => 50],
            [['direccion', 'coord_google_maps'], 'string', 'max' => 200],
            [['id_empresa'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['id_empresa' => 'id_empresa']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_estacion' => 'Id Estacion',
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
            'id_empresa' => 'Id Empresa',
            'coordinador' => 'Coordinador',
            'direccion' => 'Direccion',
            'coord_google_maps' => 'Coord Google Maps',
            'id_wizard' => 'Id Wizard',
            'asoc_taller' => 'Asoc Taller',
            'codigo_generado' => 'Codigo Generado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuditoriaEjecMants()
    {
        return $this->hasMany(AuditoriaEjecMant::className(), ['id_estacion' => 'id_estacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuditoriaFs()
    {
        return $this->hasMany(AuditoriaFs::className(), ['id_estacion' => 'id_estacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuditoriaMntos()
    {
        return $this->hasMany(AuditoriaMnto::className(), ['id_estacion' => 'id_estacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCasos()
    {
        return $this->hasMany(Caso::className(), ['id_estacion' => 'id_estacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDespachos()
    {
        return $this->hasMany(Despacho::className(), ['id_estacion' => 'id_estacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id_empresa' => 'id_empresa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstacionUsuarios()
    {
        return $this->hasMany(EstacionUsuario::className(), ['estacion_id' => 'id_estacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMantenimientoPreventivos()
    {
        return $this->hasMany(MantenimientoPreventivo::className(), ['id_estacion' => 'id_estacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicios()
    {
        return $this->hasMany(Servicio::className(), ['id_estacion_actual' => 'id_estacion']);
    }
}
