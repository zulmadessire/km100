<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipo_actividad".
 *
 * @property integer $id
 * @property string $nombre
 *
 * @property Pieza[] $piezas
 */
class TipoActividad extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipo_actividad';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPiezas()
    {
        return $this->hasMany(Pieza::className(), ['tipo_actividad_id' => 'id']);
    }
}
