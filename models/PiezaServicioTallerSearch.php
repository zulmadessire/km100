<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PiezaServicioTaller;

/**
 * PiezaServicioTallerSearch represents the model behind the search form of `app\models\PiezaServicioTaller`.
 */
class PiezaServicioTallerSearch extends PiezaServicioTaller
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_servicio_taller', 'estado'], 'integer'],
            [['nombre'], 'safe'],
            [['carro', 'camion', 'camioneta', 'furgoneta', 'suv', 'van', 'jeepeta', 'salvado_leve', 'salvado_fuerte'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PiezaServicioTaller::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'carro' => $this->carro,
            'camion' => $this->camion,
            'camioneta' => $this->camioneta,
            'furgoneta' => $this->furgoneta,
            'suv' => $this->suv,
            'van' => $this->van,
            'jeepeta' => $this->jeepeta,
            'salvado_leve' => $this->salvado_leve,
            'salvado_fuerte' => $this->salvado_fuerte,
            'id_servicio_taller' => $this->id_servicio_taller,
            'estado' => $this->estado,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre]);

        return $dataProvider;
    }
}
