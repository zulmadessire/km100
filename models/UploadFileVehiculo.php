<?php  
 
namespace app\models;
 
use yii\web\UploadedFile;

class UploadFileVehiculo extends Model
{
    /**
     * @var UploadedFile
     */
    public $file;

    public function rules()
    {
        return [
            [
                ['file'], 'file', 'skipOnEmpty' => false, 
                'extensions' => 'xlsx',
                'checkExtensionByMimeType' => false,
                'maxFiles' => 1,
 
            ],
        ];
    }
    
    public function upload()
    {
        if ($this->validate()) {
            $this->file->saveAs( './files/vehiculos/file-vehiculo-registro_vehiculos.'. $this->file->extension);
            return true;
        } else {
            return false;
        }
    }
}
?>