<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DespachoProveedorPieza;

/**
 * DespachoProveedorPiezaSearch represents the model behind the search form about `app\models\DespachoProveedorPieza`.
 */
class DespachoProveedorPiezaSearch extends DespachoProveedorPieza
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'cantidad_enviada_taller', 'cantidad_recibida_taller', 'id_despacho', 'id_cotizacion_proveedor_pieza'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$id)
    {
        $a = 1;
        $query = DespachoProveedorPieza::find()
        ->join('JOIN','cotizacion_proveedor_pieza','cotizacion_proveedor_pieza.id = despacho_proveedor_pieza.id_cotizacion_proveedor_pieza')
        ->join('JOIN','cotizacion_taller_pieza','cotizacion_proveedor_pieza.id_cotizacion_taller_pieza = cotizacion_taller_pieza.id')
        ->join('JOIN','pieza','cotizacion_taller_pieza.id_pieza = pieza.id_pieza')
        ->where('despacho_proveedor_pieza.id_despacho='.$id)
        ->andWhere('despacho_proveedor_pieza.recibido is null');
        

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'cantidad_enviada_taller' => $this->cantidad_enviada_taller,
            'cantidad_recibida_taller' => $this->cantidad_recibida_taller,
            'id_despacho' => $this->id_despacho,
            'id_cotizacion_proveedor_pieza' => $this->id_cotizacion_proveedor_pieza,
        ]);

        return $dataProvider;
    }

    public function searchPiezasTaller($params,$id)
    {
        $a = 1;
        $query = DespachoProveedorPieza::find()
        ->join('JOIN','cotizacion_proveedor_pieza','cotizacion_proveedor_pieza.id = despacho_proveedor_pieza.id_cotizacion_proveedor_pieza')
        ->join('JOIN','cotizacion_taller_pieza','cotizacion_proveedor_pieza.id_cotizacion_taller_pieza = cotizacion_taller_pieza.id')
        ->join('JOIN','pieza','cotizacion_taller_pieza.id_pieza = pieza.id_pieza')
        ->where('despacho_proveedor_pieza.id_despacho='.$id)
        ->andWhere('despacho_proveedor_pieza.recibido = 1');
        

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'cantidad_enviada_taller' => $this->cantidad_enviada_taller,
            'cantidad_recibida_taller' => $this->cantidad_recibida_taller,
            'id_despacho' => $this->id_despacho,
            'id_cotizacion_proveedor_pieza' => $this->id_cotizacion_proveedor_pieza,
        ]);

        return $dataProvider;
    }
    public function searchPiezasTallerRecibidas($params,$id)
    {
        $a = 1;
        $query = DespachoProveedorPieza::find()
        ->join('JOIN','cotizacion_proveedor_pieza','cotizacion_proveedor_pieza.id = despacho_proveedor_pieza.id_cotizacion_proveedor_pieza')
        ->join('JOIN','cotizacion_taller_pieza','cotizacion_proveedor_pieza.id_cotizacion_taller_pieza = cotizacion_taller_pieza.id')
        ->join('JOIN','pieza','cotizacion_taller_pieza.id_pieza = pieza.id_pieza')
        ->where('despacho_proveedor_pieza.id_despacho='.$id)
        ->andWhere('despacho_proveedor_pieza.recibido = 2');
        

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'cantidad_enviada_taller' => $this->cantidad_enviada_taller,
            'cantidad_recibida_taller' => $this->cantidad_recibida_taller,
            'id_despacho' => $this->id_despacho,
            'id_cotizacion_proveedor_pieza' => $this->id_cotizacion_proveedor_pieza,
        ]);

        return $dataProvider;
    }

    public function searchEstacion($params,$id)
    {
        $a = 1;
        $query = DespachoProveedorPieza::find()
        ->join('JOIN','cotizacion_proveedor_pieza','cotizacion_proveedor_pieza.id = despacho_proveedor_pieza.id_cotizacion_proveedor_pieza')
        ->join('JOIN','solicitud_pieza_estacion','cotizacion_proveedor_pieza.id_solicitud_pieza_estacion = solicitud_pieza_estacion.id')
        ->join('JOIN','pieza','solicitud_pieza_estacion.id_pieza = pieza.id_pieza')
        ->where('despacho_proveedor_pieza.id_despacho='.$id)
        ->andWhere('despacho_proveedor_pieza.recibido is null');
        

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'cantidad_enviada_taller' => $this->cantidad_enviada_taller,
            'cantidad_recibida_taller' => $this->cantidad_recibida_taller,
            'id_despacho' => $this->id_despacho,
            'id_cotizacion_proveedor_pieza' => $this->id_cotizacion_proveedor_pieza,
        ]);

        return $dataProvider;
    }

    public function searchPiezasEstacion($params,$id)
    {
        $a = 1;
        $query = DespachoProveedorPieza::find()
        ->join('JOIN','cotizacion_proveedor_pieza','cotizacion_proveedor_pieza.id = despacho_proveedor_pieza.id_cotizacion_proveedor_pieza')
        ->join('JOIN','solicitud_pieza_estacion','cotizacion_proveedor_pieza.id_solicitud_pieza_estacion = solicitud_pieza_estacion.id')
        ->join('JOIN','pieza','solicitud_pieza_estacion.id_pieza = pieza.id_pieza')
        ->where('despacho_proveedor_pieza.id_despacho='.$id)
        ->andWhere('despacho_proveedor_pieza.recibido = 1');
        

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'cantidad_enviada_taller' => $this->cantidad_enviada_taller,
            'cantidad_recibida_taller' => $this->cantidad_recibida_taller,
            'id_despacho' => $this->id_despacho,
            'id_cotizacion_proveedor_pieza' => $this->id_cotizacion_proveedor_pieza,
        ]);

        return $dataProvider;
    }

    public function searchPiezasEstacionRecibidas($params,$id)
    {
        $a = 1;
        $query = DespachoProveedorPieza::find()
        ->join('JOIN','cotizacion_proveedor_pieza','cotizacion_proveedor_pieza.id = despacho_proveedor_pieza.id_cotizacion_proveedor_pieza')
        ->join('JOIN','solicitud_pieza_estacion','cotizacion_proveedor_pieza.id_solicitud_pieza_estacion = solicitud_pieza_estacion.id')
        ->join('JOIN','pieza','solicitud_pieza_estacion.id_pieza = pieza.id_pieza')
        ->where('despacho_proveedor_pieza.id_despacho='.$id)
        ->andWhere('despacho_proveedor_pieza.recibido = 2');
        

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'cantidad_enviada_taller' => $this->cantidad_enviada_taller,
            'cantidad_recibida_taller' => $this->cantidad_recibida_taller,
            'id_despacho' => $this->id_despacho,
            'id_cotizacion_proveedor_pieza' => $this->id_cotizacion_proveedor_pieza,
        ]);

        return $dataProvider;
    }
}
