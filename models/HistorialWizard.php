<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "historial_wizard".
 *
 * @property integer $id
 * @property string $fecha
 * @property string $nombre_archivo
 * @property integer $estado
 */
class HistorialWizard extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'historial_wizard';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha', 'nombre_archivo', 'estado'], 'required'],
            [['fecha'], 'safe'],
            [['estado'], 'integer'],
            [['nombre_archivo'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha' => 'Fecha',
            'nombre_archivo' => 'Nombre Archivo',
            'estado' => 'Estado',
        ];
    }
}
