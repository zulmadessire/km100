<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "modelo_pieza".
 *
 * @property integer $id
 * @property integer $modelo_id_modelo
 * @property integer $pieza_id
 * @property string $anho
 *
 * @property Modelo $modeloIdModelo
 * @property Pieza $pieza
 */
class ModeloPieza extends \yii\db\ActiveRecord
{
    public $marca;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'modelo_pieza';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['modelo_id_modelo', 'pieza_id','marca'], 'integer'],
            [['anho'], 'string', 'max' => 450],
            [['modelo_id_modelo'], 'exist', 'skipOnError' => true, 'targetClass' => Modelo::className(), 'targetAttribute' => ['modelo_id_modelo' => 'id_modelo']],
            [['pieza_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pieza::className(), 'targetAttribute' => ['pieza_id' => 'id_pieza']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'modelo_id_modelo' => 'Modelo',
            'pieza_id' => 'Pieza',
            'anho' => 'Año',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModeloIdModelo()
    {
        return $this->hasOne(Modelo::className(), ['id_modelo' => 'modelo_id_modelo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPieza()
    {
        return $this->hasOne(Pieza::className(), ['id_pieza' => 'pieza_id']);
    }
}
