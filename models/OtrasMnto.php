<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "otras_mnto".
 *
 * @property integer $id
 * @property string $km_proximo
 * @property string $ficha_mva
 * @property string $km_actual
 * @property string $observacion
 * @property integer $id_aud_mnto
 *
 * @property AuditoriaMnto $idAudMnto
 */
class OtrasMnto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'otras_mnto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_aud_mnto'], 'required'],
            [['id_aud_mnto'], 'integer'],
            [['km_proximo', 'ficha_mva', 'km_actual'], 'string', 'max' => 100],
            [['observacion'], 'string', 'max' => 1000],
            [['id_aud_mnto'], 'exist', 'skipOnError' => true, 'targetClass' => AuditoriaMnto::className(), 'targetAttribute' => ['id_aud_mnto' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'km_proximo' => 'Km Proximo',
            'ficha_mva' => 'Ficha Mva',
            'km_actual' => 'Km Actual',
            'observacion' => 'Observacion',
            'id_aud_mnto' => 'Id Aud Mnto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAudMnto()
    {
        return $this->hasOne(AuditoriaMnto::className(), ['id' => 'id_aud_mnto']);
    }
}
