<?php

namespace app\models;
use Yii;

use yii\web\UploadedFile;
use yii\helpers\FileHelper;
/**
 * This is the model class for table "anexos".
 *
 * @property integer $id
 * @property string $descripcion
 * @property string $anexo
 * @property integer $id_sol_aud_fs
 *
 * @property Brief $idBrief
 */
class Anexos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

     public $imageFile;


    public static function tableName()
    {
        return 'anexos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['descripcion', 'anexo','extension','base_url'], 'string'],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg,jpeg, pdf, mp3, wav, mp4'],
            [['id_sol_aud_fs'], 'required'],
            [['id_sol_aud_fs'], 'integer'],
            [['id_sol_aud_fs'], 'exist', 'skipOnError' => true, 'targetClass' => SolAudFs::className(), 'targetAttribute' => ['id_sol_aud_fs' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'descripcion' => 'Descripcion',
            'anexo' => 'Anexo',
            'id_sol_aud_fs' => 'Id SolAudFs',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSolAudFs()
    {
        return $this->hasOne(SolAudFs::className(), ['id' => 'id_sol_aud_fs']);
    }


 public function upload()
    {
        if ($this->validate()) {
            $this->imageFile->saveAs('uploads/' . $this->imageFile->baseName . '.' . $this->imageFile->extension);
            return true;
        } else {
            return false;
        }
    }


    public function uploadArchive($id)
    {

            $this->imageFile->saveAs(\Yii::$app->basePath.'/files/'.$id.'/'.$this->imageFile->baseName .'.'. $this->imageFile->extension);

            return true;
         
    }
}
