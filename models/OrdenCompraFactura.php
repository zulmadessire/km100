<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orden_compra_factura".
 *
 * @property integer $id
 * @property string $numero_factura
 * @property string $nic
 * @property string $fecha_emision
 * @property string $fecha_vencimiento
 * @property string $fecha
 * @property string $condicion_pago
 * @property double $monto
 * @property integer $estado
 * @property integer $id_orden_compra_pieza
 *
 * @property OrdenCompraPieza $idOrdenCompraPieza
 */
class OrdenCompraFactura extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orden_compra_factura';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha_emision', 'fecha_vencimiento', 'fecha'], 'safe'],
            [['monto'], 'number'],
            [['estado', 'id_orden_compra_pieza'], 'integer'],
            [['id_orden_compra_pieza'], 'required'],
            [['numero_factura', 'nic', 'condicion_pago'], 'string', 'max' => 45],
            [['id_orden_compra_pieza'], 'exist', 'skipOnError' => true, 'targetClass' => OrdenCompraPieza::className(), 'targetAttribute' => ['id_orden_compra_pieza' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'numero_factura' => 'Numero Factura',
            'nic' => 'Nic',
            'fecha_emision' => 'Fecha Emision',
            'fecha_vencimiento' => 'Fecha Vencimiento',
            'fecha' => 'Fecha',
            'condicion_pago' => 'Condicion Pago',
            'monto' => 'Monto',
            'estado' => 'Estado',
            'id_orden_compra_pieza' => 'Id Orden Compra Pieza',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdOrdenCompraPieza()
    {
        return $this->hasOne(OrdenCompraPieza::className(), ['id' => 'id_orden_compra_pieza']);
    }
}
