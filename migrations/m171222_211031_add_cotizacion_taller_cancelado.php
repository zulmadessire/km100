<?php

use yii\db\Migration;

class m171222_211031_add_cotizacion_taller_cancelado extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cotizacion_taller', 'cancelado',  $this->integer()); 
        $this->addColumn('cotizacion_taller', 'observacion', $this->string(200));
    }

    public function safeDown()
    {
        echo "m171222_211031_add_cotizacion_taller_cancelado cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171222_211031_add_cotizacion_taller_cancelado cannot be reverted.\n";

        return false;
    }
    */
}
