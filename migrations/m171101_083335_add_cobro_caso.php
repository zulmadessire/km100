<?php

use yii\db\Migration;

class m171101_083335_add_cobro_caso extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cobro_caso', 'numero_factura', $this->string(45)->notNull());
    }

    public function safeDown()
    {
        echo "m171101_083335_add_cobro_caso cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171101_083335_add_cobro_caso cannot be reverted.\n";

        return false;
    }
    */
}
