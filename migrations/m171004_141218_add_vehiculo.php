<?php

use yii\db\Migration;

class m171004_141218_add_vehiculo extends Migration
{
    public function safeUp()
    {

        $this->addColumn('vehiculo', 'fecha_emision_seguro', $this->date());
        $this->addColumn('vehiculo', 'fecha_venc_seguro', $this->date());
        $this->addColumn('vehiculo', 'fecha_emision_marbete', $this->date());
        $this->addColumn('vehiculo', 'fecha_venc_marbete', $this->date());
        $this->addColumn('vehiculo', 'fecha_venta', $this->date());


    }

    public function safeDown()
    {
        echo "m171004_141218_add_vehiculo cannot be reverted.\n";
          

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171004_141218_add_vehiculo cannot be reverted.\n";

        return false;
    }
    */
}
