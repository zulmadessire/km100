<?php

use yii\db\Migration;

class m180314_180833_create_add_auditoria_anexos extends Migration
{
    public function safeUp()
    {
        
        $this->addColumn('sol_aud_fs', 'anexo2', $this->string(1000));
        $this->addColumn('sol_aud_fs', 'extension2', $this->string(1000));
        $this->addColumn('sol_aud_fs', 'base_url2', $this->string(1000));
        $this->addColumn('sol_aud_fs', 'imagefile2', $this->string(1000));
        $this->addColumn('sol_aud_fs', 'anexo3', $this->string(1000));
        $this->addColumn('sol_aud_fs', 'extension3', $this->string(1000));
        $this->addColumn('sol_aud_fs', 'base_url3', $this->string(1000));
        $this->addColumn('sol_aud_fs', 'imagefile3', $this->string(1000));
        $this->addColumn('sol_aud_fs', 'anexo4', $this->string(1000));
        $this->addColumn('sol_aud_fs', 'extension4', $this->string(1000));
        $this->addColumn('sol_aud_fs', 'base_url4', $this->string(1000));
        $this->addColumn('sol_aud_fs', 'imagefile4', $this->string(1000));
    }

    public function safeDown()
    {
        echo "m180314_180833_create_add_auditoria_anexos cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180314_180833_create_add_auditoria_anexos cannot be reverted.\n";

        return false;
    }
    */
}
