<?php

use yii\db\Migration;

class m170802_201815_create_modelo_pieza extends Migration
{
    public function safeUp()
    {
        $this->createTable('tipo_empresa', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(45)->notNull(),
 
        ]);
        
        $this->createTable('proveedor', [
            'id' => $this->primaryKey(),
            'tipo_persona' => $this->string(100)->notNull(),
            'identificacion' => $this->string(100)->notNull(),
            'direccion' => $this->string(100)->notNull(),
            'descripcion' => $this->string(100)->notNull(),
            'telefono' => $this->string(100)->notNull(),
            'credito' => $this->double()->notNull(),
            'nombre_representante'=> $this->string(100)->notNull(),
            'identificacion_representante'=> $this->string(100)->notNull(),
            'puesto_representante'=> $this->string(100)->notNull(),
            'correo_representante'=> $this->string(100)->notNull(),
            'celular_representante'=> $this->string(100)->notNull(),
            'telefono_representante'=> $this->string(100)->notNull(),
            'garantia'=> $this->string(100)->notNull(),
            'domicilio'=> $this->string(100)->notNull(),
            'nombre'=> $this->string(100)->notNull(),
            'porcentaje' => $this->double()->notNull(),
            'Tipo_pago'=> $this->string(100)->notNull(),
            'plazo_pago'=> $this->integer()->Null(),
            'id_estado' => $this->integer()->notNull(),
            'id_vehiculo' => $this->integer()->notNull(),
            'tipo_empresa_id'=>$this->integer()->Null(),

        ]);

        $this->addForeignKey('fk-tipo-empresa-id', 'proveedor', 'tipo_empresa_id', 'tipo_empresa', 'id', 'CASCADE');

        $this->createTable('marca_insumo', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(45)->notNull(),
            'tipo_pieza_id' => $this->integer()->notNull(),
            
        ]);

        $this->addForeignKey('fk-marca-insumo-tipo-pieza-id', 'marca_insumo', 'tipo_pieza_id', 'tipo_pieza', 'id', 'CASCADE');

        $this->createTable('proveedor_pieza', [
            'id' => $this->primaryKey(),
            'proveedor_id' =>$this->integer()->notNull(),
            'pieza_id' => $this->integer()->Null(),
            'marca_insumo_id' => $this->integer()->Null(),
            
        ]);

        $this->addForeignKey('fk-proveedor-id', 'proveedor_pieza', 'proveedor_id', 'proveedor', 'id', 'CASCADE');
        $this->addForeignKey('fk-pieza-id', 'proveedor_pieza', 'pieza_id', 'pieza', 'id_pieza', 'CASCADE');
        $this->addForeignKey('fk-marca-insumo-id', 'proveedor_pieza', 'marca_insumo_id', 'marca_insumo', 'id', 'CASCADE');

        $this->createTable('modelo_pieza', [
            'id' => $this->primaryKey(),
            'anho' => $this->string(45)->Null(),
            'proveedor_pieza_id' => $this->integer()->notNull(),
            'modelo_id_modelo' => $this->integer()->notNull(),
        ]);
           $this->addForeignKey('fk-proveedor-pieza-id', 'modelo_pieza', 'proveedor_pieza_id', 'proveedor_pieza', 'id', 'CASCADE');
           $this->addForeignKey('fk-modelo-id-modelo', 'modelo_pieza', 'modelo_id_modelo', 'modelo', 'id_modelo', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m170802_201815_create_modelo_pieza cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170802_201815_create_modelo_pieza cannot be reverted.\n";

        return false;
    }
    */
}
