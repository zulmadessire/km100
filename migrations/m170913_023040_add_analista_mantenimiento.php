<?php

use yii\db\Migration;

class m170913_023040_add_analista_mantenimiento extends Migration
{
    public function safeUp()
    {
        $this->addColumn( 'analista_mantenimiento', 'user', 'character varying(200)' );
    }

    public function safeDown()
    {
        echo "m170913_023040_add_analista_mantenimiento cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170913_023040_add_analista_mantenimiento cannot be reverted.\n";

        return false;
    }
    */
}
