<?php

use yii\db\Migration;

class m180119_013650_create_variables extends Migration
{
    public function safeUp()
    {
        $this->createTable('variables', [
            'id' => $this->primaryKey(),
            'label' => $this->string(100)->notNull(),
            'descripcion' => $this->string(200),
            'value' => $this->string(100)->notNull(),
        ]);
    }

    public function safeDown()
    {
        echo "m180119_013650_create_variables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180119_013650_create_variables cannot be reverted.\n";

        return false;
    }
    */
}
