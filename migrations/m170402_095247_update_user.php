<?php

use yii\db\Migration;

class m170402_095247_update_user extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'cargo_gerencial', 'integer not null DEFAULT 0');
        $this->addCommentOnColumn ('user', 'cargo_gerencial', '0-Otros Cargos  1-Cargo Gerencial');
        $this->addColumn ('user', 'id_empresa', 'integer');

        $this->createIndex(
            'idx-user-id_empresa',
            'user',
            'id_empresa'
        );
        $this->addForeignKey('fk-empresa-user', 'user', 'id_empresa', 'empresa', 'id_empresa', 'CASCADE');
    }

    public function down()
    {
        echo "m170402_095247_update_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
