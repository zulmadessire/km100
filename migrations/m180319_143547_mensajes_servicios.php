<?php

use yii\db\Migration;

/**
 * Class m180319_143547_mensajes_servicios
 */
class m180319_143547_mensajes_servicios extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('servicio_mensaje', [
            'id' => $this->primaryKey(),
            'mensaje' => $this->text(),
            'fecha' => $this->dateTime(),
            'motivo' => $this->integer(),
            'estado' => $this->integer(),
            'codigo_pipeline' => $this->integer(),
            'id_servicio' => $this->integer()->notNull(),
            'id_user' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-servicio_mensaje','servicio_mensaje','id_servicio');
        $this->addForeignKey('fk-servicio_mensaje', 'servicio_mensaje', 'id_servicio', 'servicio', 'id', 'CASCADE');

        $this->createIndex('idx-servicio_mensaje_user','servicio_mensaje','id_user');
        $this->addForeignKey('fk-servicio_mensaje_user', 'servicio_mensaje', 'id_user', 'user', 'id', 'CASCADE');
        
        $this->addCommentOnColumn ('servicio_mensaje', 'motivo', '0-Cotización, 1-Negociación, 2-Evaluación Analista, 3-Evaluación Gerente, 4-Evaluación Director, 5-Salvamento, 6-Otro');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180319_143547_mensajes_servicios cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180319_143547_mensajes_servicios cannot be reverted.\n";

        return false;
    }
    */
}
