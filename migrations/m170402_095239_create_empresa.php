<?php

use yii\db\Migration;

class m170402_095239_create_empresa extends Migration
{
    public function up()
    {
        $this->createTable('empresa', [
            'id_empresa' => $this->primaryKey(),
            'nombre' => $this->string(200)->notNull(),
        ]);
    }

    public function down()
    {
        echo "m170402_095239_create_empresa cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
