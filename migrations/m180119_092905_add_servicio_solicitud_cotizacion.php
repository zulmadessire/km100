<?php

use yii\db\Migration;

/**
 * Class m180119_092905_add_servicio_solicitud_cotizacion
 */
class m180119_092905_add_servicio_solicitud_cotizacion extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        //Añado un campo en la tabla servicico para indicar que se le ha solicitado una cotizacion
        $this->addColumn('servicio', 'solicita_cotizacion',  $this->integer());
        $this->addCommentOnColumn ('servicio', 'solicita_cotizacion', 'Indica solicitud de cotizacion de piezas: 0-No solicita, 1-Si solicita');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180119_092905_add_servicio_solicitud_cotizacion cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180119_092905_add_servicio_solicitud_cotizacion cannot be reverted.\n";

        return false;
    }
    */
}
