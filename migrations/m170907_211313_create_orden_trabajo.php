<?php

use yii\db\Migration;

class m170907_211313_create_orden_trabajo extends Migration
{
    public function safeUp()
    {
        $this->createTable('orden_trabajo', [
            'id' => $this->primaryKey(),
            'fecha' => $this->timestamp(),
            'observaciones' => $this->text(),
            'estado' => $this->integer(1),
            'id_cotizacion_taller' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-cotizacion_taller_orden_trabajo','orden_trabajo','id_cotizacion_taller');
        $this->addForeignKey('fk-cotizacion_taller_orden_trabajo', 'orden_trabajo', 'id_cotizacion_taller', 'cotizacion_taller', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m170907_211313_create_orden_trabajo cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170907_211313_create_orden_trabajo cannot be reverted.\n";

        return false;
    }
    */
}
