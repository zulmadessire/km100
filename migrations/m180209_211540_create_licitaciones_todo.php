<?php

use yii\db\Migration;

class m180209_211540_create_licitaciones_todo extends Migration
{
    public function safeUp()
    {
  $this->createTable('solicitudes', [
            'id' => $this->primaryKey(),
            'descripcion' => $this->string(200),
        ]);
            
            $this->createTable('estado_licitacion', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(45)->notNull(),
            'color' => $this->string(10),
        ]);
         

            $this->createTable('licitacion', [
            'id' => $this->primaryKey(),
            'fecha_inicio' => $this->date(),
            'fecha_culminacion' => $this->date(),
            'objetivo' => $this->double(),
            'garantia' => $this->string(100),
            'tiempo_entrega_s_d' => $this->integer(),
            'tiempo_entrega_i' => $this->integer(),
            'dias' => $this->string(100),
            'id_rubro' => $this->integer(),
            'id_estado_lic' => $this->integer(),
       ]);



                
              $this->addForeignKey('fk-solicitudes-id_rubro', 'licitacion', 'id_rubro', 'pieza', 'id_pieza', 'CASCADE');

              $this->addForeignKey('fk-solicitudes-id_estado_lic', 'licitacion', 'id_estado_lic', 'estado_licitacion', 'id', 'CASCADE');

        $this->createTable('licitacion_solicitud', [
            'id' => $this->primaryKey(),
            'id_solicitud' => $this->integer(),
            'id_licitacion' => $this->integer(),
        ]);

               $this->addForeignKey('fk-id_solicitudes_licitacion_solicitud', 'licitacion_solicitud', 'id_solicitud', 'solicitudes', 'id', 'CASCADE');
               $this->addForeignKey('fk-id_licitacion_licitacion_solicitud', 'licitacion_solicitud', 'id_licitacion', 'licitacion', 'id', 'CASCADE');


            $this->createTable('capacitaciones', [
            'id' => $this->primaryKey(),
            'tipo_capacitacion' => $this->string(100),
            'numero' => $this->string(200),
            'id_licitacion' => $this->integer(),
        ]);


              $this->addForeignKey('fk-capacitaciones_id_licitacion', 'capacitaciones', 'id_licitacion', 'licitacion', 'id', 'CASCADE');
    
    }

    public function safeDown()
    {
        echo "m180209_211540_create_licitaciones_todo cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180209_211540_create_licitaciones_todo cannot be reverted.\n";

        return false;
    }
    */
}
