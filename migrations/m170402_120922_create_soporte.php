<?php

use yii\db\Migration;

class m170402_120922_create_soporte extends Migration
{
    public function up()
    {
        $this->createTable('soporte', [
            'id_soporte' => $this->primaryKey(),
            'ubicacion' => $this->string(200)->notNull(),
            'bloque' => $this->integer(1)->notNull(),
            'id_caso' => $this->integer()->notNull(),
        ]);
        $this->createIndex(
            'idx-caso-soporte',
            'soporte',
            'id_caso'
        );
        $this->addForeignKey('fk-caso-soporte', 'soporte', 'id_caso', 'caso', 'id_caso', 'CASCADE');
        
        $this->addCommentOnColumn ('soporte', 'bloque', '0-Frontal, 1-Lateral izquierdo, 2-Lateral derecho, 3-Trasero, 4-Interior - Cabina, 5-Kilometraje, 6-Extras');
    }

    public function down()
    {
        echo "m170402_120922_create_soporte cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
