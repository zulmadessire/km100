<?php

use yii\db\Migration;

class m180225_114925_add_licitaciones extends Migration
{
    public function safeUp()
    {
        $this->addColumn('licitacion', 'fecha_hora', $this->datetime());
        $this->addColumn('licitacion', 'contador', $this->integer());

             
    }

    public function safeDown()
    {
        echo "m180225_114925_add_licitaciones cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180225_114925_add_licitaciones cannot be reverted.\n";

        return false;
    }
    */
}
