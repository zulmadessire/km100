<?php

use yii\db\Migration;

class m180117_213924_create_encuesta extends Migration
{
    public function safeUp()
    {
$this->createTable('preguntas', [
            'id' => $this->primaryKey(),
            'pregunta' => $this->string(1000), 
          
        ]);

$this->createTable('servicio_encuesta', [
            'id' => $this->primaryKey(),
            'id_servicio' => $this->integer()->notNull(),
            'id_pregunta' => $this->integer()->notNull(),
            'respuesta' => $this->integer(),
        ]);
 
        $this->addForeignKey('fk-servicio_encuesta_id_servicio', 'servicio_encuesta', 'id_servicio', 'servicio', 'id', 'CASCADE');
        $this->addForeignKey('fk-servicio_encuesta_pregunta', 'servicio_encuesta', 'id_pregunta', 'preguntas', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m180117_213924_create_encuesta cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180117_213924_create_encuesta cannot be reverted.\n";

        return false;
    }
    */
}
