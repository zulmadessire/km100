<?php

use yii\db\Migration;

class m171220_220648_add_taller_tipo_taller extends Migration
{
    public function safeUp()
    {
        $this->addColumn('taller', 'tipo_taller',  $this->integer(1)->notNull());     
        $this->addCommentOnColumn ('taller', 'tipo_taller', '0-Quick Line, 1-Normal Line, 2-Taller km100');    
    }

    public function safeDown()
    {
        echo "m171220_220648_add_taller_tipo_taller cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171220_220648_add_taller_tipo_taller cannot be reverted.\n";

        return false;
    }
    */
}
