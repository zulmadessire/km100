<?php

use yii\db\Migration;

/**
 * Class m180307_185745_add_observacion_pieza_imagen_pieza_solicitud_pieza_estacion
 */
class m180307_185745_add_observacion_pieza_imagen_pieza_solicitud_pieza_estacion extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('solicitud_pieza_estacion', 'imagen_pieza', $this->string(255));
        $this->addColumn('solicitud_pieza_estacion', 'observacion_pieza', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180307_185745_add_observacion_pieza_imagen_pieza_solicitud_pieza_estacion cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180307_185745_add_observacion_pieza_imagen_pieza_solicitud_pieza_estacion cannot be reverted.\n";

        return false;
    }
    */
}
