<?php

use yii\db\Migration;

class m180126_200957_add_key_variables extends Migration
{
    public function safeUp()
    {
        $this->addColumn('variables', 'key',  $this->string());
    }

    public function safeDown()
    {
        echo "m180126_200957_add_key_variables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180126_200957_add_key_variables cannot be reverted.\n";

        return false;
    }
    */
}
