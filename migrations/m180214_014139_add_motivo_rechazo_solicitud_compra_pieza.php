<?php

use yii\db\Migration;

class m180214_014139_add_motivo_rechazo_solicitud_compra_pieza extends Migration
{
    public function safeUp()
    {
        $this->addColumn('solicitud_compra_pieza', 'motivo_rechazo', $this->text());
    }

    public function safeDown()
    {
        echo "m180214_014139_add_motivo_rechazo_solicitud_compra_pieza cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180214_014139_add_motivo_rechazo_solicitud_compra_pieza cannot be reverted.\n";

        return false;
    }
    */
}
