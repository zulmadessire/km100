<?php

use yii\db\Migration;

class m170817_192341_create_partes_garantia extends Migration
{
    public function safeUp()
    {
         $this->createTable('partes', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(45)->notNull(),
        ]);

         $this->createTable('partes_garantia', [
            'id' => $this->primaryKey(),
            'kilometraje' => $this->string(45)->notNull(),
            'tiempo' => $this->string(45)->notNull(),
            'partes_id' => $this->integer()->notNull(),
            'detalle_garantia_id' => $this->integer()->notNull(),
        ]);
    
    $this->addForeignKey('fk-partes-garantia-detalle_garantia_id', 'partes_garantia', 'detalle_garantia_id', 'detalle_garantia', 'id', 'CASCADE');

         
    $this->addForeignKey('fk-partes-garantia-partes-id', 'partes_garantia', 'partes_id', 'partes', 'id', 'CASCADE');

    }

    public function safeDown()
    {
        echo "m170817_192341_create_partes_garantia cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170817_192341_create_partes_garantia cannot be reverted.\n";

        return false;
    }
    */
}
