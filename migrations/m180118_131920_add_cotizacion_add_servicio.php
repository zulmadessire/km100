<?php

use yii\db\Migration;

class m180118_131920_add_cotizacion_add_servicio extends Migration
{
    public function safeUp()
    {
            $this->addColumn('cotizacion_taller_actividad', 'total_servicio', $this->double());
            $this->addColumn('cotizacion_taller_actividad', 'id_tipo_servicio',  $this->integer());

             $this->addForeignKey('fk-cotizacion_taller_actividad_id_tipo_servicio', 'cotizacion_taller_actividad', 'id_tipo_servicio', 'tipo_servicio', 'id_tipo_servicio', 'CASCADE');

    }

    public function safeDown()
    {
        echo "m180118_131920_add_cotizacion_add_servicio cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180118_131920_add_cotizacion_add_servicio cannot be reverted.\n";

        return false;
    }
    */
}
