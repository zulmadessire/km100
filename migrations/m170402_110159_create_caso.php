<?php

use yii\db\Migration;

class m170402_110159_create_caso extends Migration
{
    public function up()
    {
        $this->createTable('caso', [
            'id_caso' => $this->primaryKey(),
            'ficha' => $this->string(20)->notNull(),
            'placa' => $this->string(20)->notNull(),
            'tipo_caso' => $this->integer(1)->notNull(),
            'fuente_accidente' => $this->integer(1),
            'amet' => $this->integer(),
            'num_contrato' => $this->integer(),
            'tipo_cliente' => $this->integer(1),
            'cobertura_cdw' => $this->integer(1),
            'claim' => $this->integer(),
            'deducible' => $this->integer(1),
            'monto_deducible' => $this->double(),
            'tarifa_base' => $this->double(),
            'nombre_empleado' => $this->string(50),
            'fecha_acc_inc' => $this->date(),
            'descripcion' => $this->string(1000),
            //'taller' => $this->integer(1)->notNull(),

            'registrado_por' => $this->integer()->notNull(),
            'id_estacion' => $this->integer()->notNull(),
            'id_modelo' => $this->integer()->notNull(),
            'id_tipo_accidente' => $this->integer(),
            'id_ubicacion' => $this->integer(),
        ]);
        $this->createIndex('idx-user-caso','caso','registrado_por');
        $this->addForeignKey('fk-user-caso', 'caso', 'registrado_por', 'user', 'id', 'CASCADE');

        $this->createIndex('idx-estacion-caso','caso','id_estacion');
        $this->addForeignKey('fk-estacion-caso', 'caso', 'id_estacion', 'estacion', 'id_estacion', 'CASCADE');

        $this->createIndex('idx-modelo-caso','caso','id_modelo');
        $this->addForeignKey('fk-modelo-caso', 'caso', 'id_modelo', 'modelo', 'id_modelo', 'CASCADE');

        $this->createIndex('idx-tipoaccidente-caso','caso','id_tipo_accidente');
        $this->addForeignKey('fk-tipoaccidente-caso', 'caso', 'id_tipo_accidente', 'tipoaccidente', 'id_tipoaccidente', 'CASCADE');

        $this->createIndex('idx-ubicacionunidad-caso','caso','id_ubicacion');
        $this->addForeignKey('fk-ubicacionunidad-caso', 'caso', 'id_ubicacion', 'ubicacionunidad', 'id_ubicacionunidad', 'CASCADE');

        $this->addCommentOnColumn ('caso', 'tipo_caso', '0-Accidente, 1-Incidente, 2-Falla mecánica, 3-Aire acondicionado, 4-Restauración');
        $this->addCommentOnColumn ('caso', 'fuente_accidente', '0-Cliente final, 1-Caso interno o empleado');
        $this->addCommentOnColumn ('caso', 'tipo_cliente', '0-Retail, 1-Corporativo, 2-Asegurado, 3-Leasing');
        $this->addCommentOnColumn ('caso', 'cobertura_cdw', '0-NO, 1-SI');
        $this->addCommentOnColumn ('caso', 'deducible', '0-NO, 1-SI');
        //$this->addCommentOnColumn ('caso', 'taller', '0-Taller Interno, 1-Taller Externo');
    }

    public function down()
    {
        echo "m170402_110159_create_caso cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
