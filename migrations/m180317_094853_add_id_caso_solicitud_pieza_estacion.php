<?php

use yii\db\Migration;

/**
 * Class m180317_094853_add_id_caso_solicitud_pieza_estacion
 */
class m180317_094853_add_id_caso_solicitud_pieza_estacion extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('solicitud_compra_estacion', 'id_caso', $this->integer());
        $this->addColumn('solicitud_compra_pieza', 'id_caso_estacion', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180317_094853_add_id_caso_solicitud_pieza_estacion cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180317_094853_add_id_caso_solicitud_pieza_estacion cannot be reverted.\n";

        return false;
    }
    */
}
