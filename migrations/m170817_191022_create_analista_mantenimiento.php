<?php

use yii\db\Migration;

class m170817_191022_create_analista_mantenimiento extends Migration
{
    public function safeUp()
    {
         $this->createTable('analista_mantenimiento', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(200)->notNull(),
            'telefono' => $this->string(45)->notNull(),
            'direccion' => $this->string(200)->notNull(),
            'correo' => $this->string(200)->notNull(),
        ]);

    }

    public function safeDown()
    {
        echo "m170817_191022_create_analista_mantenimiento cannot be reverted.\n";


        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170817_191022_create_analista_mantenimiento cannot be reverted.\n";

        return false;
    }
    */
}
