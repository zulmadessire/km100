<?php

use yii\db\Migration;

/**
 * Class m180312_163705_garantias
 */
class m180312_163705_garantias extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('garantia_vehiculo');
        $this->dropTable('garantia');

        $this->createTable('tipo_garantia', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(45),
        ]);

        $this->insert('tipo_garantia',[
            'nombre' =>'Concesionario'
        ]);

        $this->insert('tipo_garantia',[
            'nombre' =>'Taller'
        ]);

        $this->insert('tipo_garantia',[
            'nombre' =>'Proveedor'
        ]);

        $this->addColumn('partes', 'id_tipo_garantia', $this->integer());
        $this->addColumn('partes', 'estado', $this->integer()->defaultValue(1)); //Si se habilita o deshabilita la parte de la garantia

        $this->createIndex('idx-tipo_garantia_partes','partes','id_tipo_garantia');
        $this->addForeignKey('fk-tipo_garantia_partes', 'partes', 'id_tipo_garantia', 'tipo_garantia', 'id', 'CASCADE');

        $this->createTable('garantia_vehiculo', [
            'id' => $this->primaryKey(),
            'tiempo' => $this->string(45),
            'periodo' => $this->integer(),
            'km' => $this->string(45),
            'anular' => $this->boolean()->defaultValue(0),
            'motivo' => $this->integer(),
            'id_partes' => $this->integer()->notNull(),
            'id_vehiculo' => $this->integer()->notNull(),
            'id_usuario_anula' => $this->integer(),
            'fecha_anula' => $this->date(),
            'fecha_inicio' => $this->date(),
            'km_inicio' => $this->string(45),
            'id_concesionario' => $this->integer(),
            'id_proveedor' => $this->integer(),
            'id_taller' => $this->integer(),
            'cantidad' => $this->integer(),
            'consumido' => $this->integer(),
        ]);

        $this->createIndex('idx-partes_garantia_vehiculo','garantia_vehiculo','id_partes');
        $this->addForeignKey('fk-partes_garantia_vehiculo', 'garantia_vehiculo', 'id_partes', 'partes', 'id', 'CASCADE');

        $this->createIndex('idx-garantia_vehiculo_vehiculo','garantia_vehiculo','id_vehiculo');
        $this->addForeignKey('fk-garantia_vehiculo_vehiculo', 'garantia_vehiculo', 'id_vehiculo', 'vehiculo', 'id', 'CASCADE');

        $this->createIndex('idx-garantia_vehiculo_user','garantia_vehiculo','id_usuario_anula');
        $this->addForeignKey('fk-garantia_vehiculo_user', 'garantia_vehiculo', 'id_usuario_anula', 'user', 'id', 'CASCADE');

        $this->addCommentOnColumn ('garantia_vehiculo', 'periodo', '0-Dias, 1-Meses, 2-Años');

        $this->addColumn('partes_garantia', 'periodo', $this->integer());
        $this->addCommentOnColumn ('partes_garantia', 'periodo', '0-Dias, 1-Meses, 2-Años');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180312_163705_garantias cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180312_163705_garantias cannot be reverted.\n";

        return false;
    }
    */
}
