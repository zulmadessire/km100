<?php

use yii\db\Migration;

class m170913_021827_add_taller extends Migration
{
    public function safeUp()
    {
        $this->addColumn( 'taller', 'user', 'character varying(500)' );
    }

    public function safeDown()
    {
        echo "m170913_021827_add_taller cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170913_021827_add_taller cannot be reverted.\n";

        return false;
    }
    */
}
