<?php

use yii\db\Migration;

class m170803_194635_create_servicio extends Migration
{
    public function safeUp()
    {
        $this->createTable('servicio', [
            'id' => $this->primaryKey(),
            'prioridad' => $this->integer()->notNull(),
            'dano' => $this->integer()->notNull(),
            'img_reporte' => $this->string(200)->notNull(),
            'img_acta' => $this->string(200),
            'servicio' => $this->string(20)->notNull(),
            'evaluacion_tecnica' => $this->text()->notNull(),
            'responsable' => $this->string(45)->notNull(),
            'fecha' => $this->date(),
            'ficha' => $this->string(20)->notNull(),
            'placa' => $this->string(20)->notNull(),
            'tipo_caso' => $this->integer(1)->notNull(),
            'linea' => $this->integer(1)->notNull(),
            'cobro_cliente' => $this->integer(1)->notNull(),
            'id_estacion_actual' => $this->integer()->notNull(),
            'id_vehiculo' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-servicio_estacion','servicio','id_estacion_actual');
        $this->addForeignKey('fk-servicio_estacion', 'servicio', 'id_estacion_actual', 'estacion', 'id_estacion', 'CASCADE');

        $this->createIndex('idx-servicio_vehiculo','servicio','id_vehiculo');
        $this->addForeignKey('fk-servicio_vehiculo', 'servicio', 'id_vehiculo', 'vehiculo', 'id', 'CASCADE');

        $this->addCommentOnColumn ('servicio', 'tipo_caso', '0-Accidente, 1-Incidente, 2-Falla mecánica, 3-Aire acondicionado, 4-Restauración');
        $this->addCommentOnColumn ('servicio', 'prioridad', '0-NORMAL, 1-URGENTE');
        $this->addCommentOnColumn ('servicio', 'linea', '0-Quick Line, 1-Normal Line');
        $this->addCommentOnColumn ('servicio', 'cobro_cliente', '0-No, 1-Si');
        $this->addCommentOnColumn ('servicio', 'dano', '0-BAJO, 1-MEDIO, 2-ALTO');
        $this->addCommentOnColumn ('servicio', 'servicio', '0-Mecánica Ligera, 1-Mecánica Profunda, 2-Aire Acondicionado, 3-Desabolladura y pintura, 4-Accesorios');

        $this->createTable('caso_servicio', [
            'id' => $this->primaryKey(),
            'id_servicio' => $this->integer()->notNull(),
            'id_caso' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-servicio_caso_servicio','caso_servicio','id_servicio');
        $this->addForeignKey('fk-servicio_caso_servicio', 'caso_servicio', 'id_servicio', 'servicio', 'id', 'CASCADE');

        $this->createIndex('idx-caso_servicio_caso','caso_servicio','id_caso');
        $this->addForeignKey('fk-caso_servicio_caso', 'caso_servicio', 'id_caso', 'caso', 'id_caso', 'CASCADE');

        $this->createTable('estado_servicio', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(45)->notNull(),
            'color' => $this->string(10)->notNull(),
            'codigo' => $this->integer(),
        ]);

        $this->insert('estado_servicio',[
            'nombre' => 'Tránsito',
            'color' => '#ed7d31',
            'codigo' => 0,
        ]);

        $this->insert('estado_servicio',[
            'nombre' => 'Evaluación',
            'color' => '#ffc000',
            'codigo' => 1,
        ]);

        $this->insert('estado_servicio',[
            'nombre' => 'Aprobación',
            'color' => '#e7e6e6',
            'codigo' => 2,
        ]);

        $this->insert('estado_servicio',[
            'nombre' => 'Por pieza',
            'color' => '#9dc3e6',
            'codigo' => 3,
        ]);

        $this->insert('estado_servicio',[
            'nombre' => 'En proceso',
            'color' => '#2e75b6',
            'codigo' => 4,
        ]);

        $this->insert('estado_servicio',[
            'nombre' => 'Finalizado',
            'color' => '#3c5c26',
            'codigo' => 5,
        ]);

        $this->insert('estado_servicio',[
            'nombre' => 'Entregado',
            'color' => '#70ad47',
            'codigo' => 6,
        ]);

        $this->insert('estado_servicio',[
            'nombre' => 'Garantía',
            'color' => '#58028e',
            'codigo' => 7,
        ]);

        $this->insert('estado_servicio',[
            'nombre' => 'Cerrado',
            'color' => '#58028e',
            'codigo' => 8,
        ]);

        $this->insert('estado_servicio',[
            'nombre' => 'Por escanear',
            'color' => '#64d8af',
            'codigo' => 10,
        ]);

        $this->insert('estado_servicio',[
            'nombre' => 'Asignación',
            'color' => '#44546a',
            'codigo' => 11,
        ]);

        $this->createTable('pipeline_servicio', [
            'id' => $this->primaryKey(),
            'fecha' => $this->date()->notNull(),
            'id_servicio' => $this->integer()->notNull(),
            'id_estado_servicio' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-servicio_pipeline_servicio','pipeline_servicio','id_servicio');
        $this->addForeignKey('fk-servicio_pipeline_servicio', 'pipeline_servicio', 'id_servicio', 'servicio', 'id', 'CASCADE');

        $this->createIndex('idx-estado_pipeline_servicio','pipeline_servicio','id_estado_servicio');
        $this->addForeignKey('fk-estado_pipeline_servicio', 'pipeline_servicio', 'id_estado_servicio', 'estado_servicio', 'id', 'CASCADE');


    }

    public function safeDown()
    {
        echo "m170803_194635_create_servicio cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170803_194635_create_servicio cannot be reverted.\n";

        return false;
    }
    */
}
