<?php

use yii\db\Migration;

/**
 * Class m180228_211917_update_servicio_taller
 */
class m180228_211917_update_servicio_taller extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropTable('cotizacion_taller_actividad');
        $this->dropTable('servicio_taller');
        $this->dropTable('tipo_servicio');

        $this->createTable('servicio_taller', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(100)->notNull(),
        ]);

        $this->insert('servicio_taller',[
            'nombre' =>'Desabolladura y Pintura'
        ]);

        $this->insert('servicio_taller',[
            'nombre' =>'Mecánica'
        ]);

        $this->createTable('pieza_servicio_taller', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(200)->notNull(),
            'carro' => $this->double(),
            'camion' => $this->double(),
            'camioneta' => $this->double(),
            'furgoneta' => $this->double(),
            'suv' => $this->double(),
            'van' => $this->double(),
            'jeepeta' => $this->double(),
            'salvado_leve' => $this->double(),
            'salvado_fuerte' => $this->double(),
            'id_servicio_taller' => $this->integer()->notNull(),
            'estado' => $this->integer(),
        ]);

        $this->createIndex('idx-pieza_servicio_taller-servicio_taller','pieza_servicio_taller','id_servicio_taller');
        $this->addForeignKey('fk-pieza_servicio_taller-servicio_taller', 'pieza_servicio_taller', 'id_servicio_taller', 'servicio_taller', 'id', 'CASCADE');
        
        $this->insert('pieza_servicio_taller',[
            'nombre' => 'Bumper Delantero',
            'carro' => 1,
            'jeepeta' => 1,
            'camioneta' => 1,
            'van' => 1,
            'camion' => 0,
            'furgoneta' => 0,
            'suv' => 0,
            'salvado_leve' => 0.5,
            'salvado_fuerte' => 1,
            'id_servicio_taller' => 1
        ]);

        $this->insert('pieza_servicio_taller',[
            'nombre' => 'Bumper Delantero Inferior',
            'carro' => 0.5,
            'jeepeta' => 0.5,
            'camioneta' => 0,
            'van' => 0,
            'camion' => 0,
            'furgoneta' => 0,
            'suv' => 0,
            'salvado_leve' => 0,
            'salvado_fuerte' => 0.5,
            'id_servicio_taller' => 1
        ]);

        $this->createTable('cotizacion_taller_actividad', [
            'id' => $this->primaryKey(),
            'id_pieza_servicio_taller' => $this->integer()->notNull(),
            'labor' => $this->integer(),
            'precio' => $this->double(),
            'total_servicio' => $this->double(),
            'id_cotizacion_taller' => $this->integer()->notNull(),
            'conforme_recepcion' => $this->integer(),
            'id_vehiculo' => $this->integer(),
            'tipo_servicio_taller' => $this->integer(),
            'decimal' => $this->double(),
        ]);

        $this->createIndex('idx-cotizacion_taller_actividad','cotizacion_taller_actividad','id_cotizacion_taller');
        $this->addForeignKey('fk-cotizacion_taller_actividad', 'cotizacion_taller_actividad', 'id_cotizacion_taller', 'cotizacion_taller', 'id', 'CASCADE');

        $this->createIndex('idx-cotizacion_taller_actividad_pieza','cotizacion_taller_actividad','id_pieza_servicio_taller');
        $this->addForeignKey('fk-cotizacion_taller_actividad_pieza', 'cotizacion_taller_actividad', 'id_pieza_servicio_taller', 'pieza_servicio_taller', 'id', 'CASCADE');
        
        $this->addColumn('taller', 'tipo_servicio_taller', $this->integer());
        $this->addCommentOnColumn ('taller', 'tipo_servicio_taller', '0-Des. y Pint., 1-Mecánica Line, 2-Des. y Pint / Mecánica');    
        $this->addColumn('taller', 'precio_pieza_mecanica', $this->double());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180228_211917_update_servicio_taller cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180228_211917_update_servicio_taller cannot be reverted.\n";

        return false;
    }
    */
}
