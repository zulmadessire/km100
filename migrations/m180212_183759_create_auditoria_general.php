<?php

use yii\db\Migration;

class m180212_183759_create_auditoria_general extends Migration
{
    public function safeUp()
    {
         $this->createTable('auditoria_general', [
            'id' => $this->primaryKey(),
            'fecha' => $this->date(),
            'estado' => $this->integer(),
            'id_estacion' => $this->integer()->notNull(),
            'id_aud_fs' => $this->integer(),
            'id_aud_mnto' => $this->integer(),
            'id_aud_ejec' => $this->integer(),

            ]);

        $this->addForeignKey('fk-estacion_id_estacion', 'auditoria_general', 'id_estacion', 'estacion', 'id_estacion', 'CASCADE');
        $this->addForeignKey('fk-id_aud_fs_auditoria_general', 'auditoria_general', 'id_aud_fs', 'auditoria_fs', 'id', 'CASCADE');
        $this->addForeignKey('fk-id_aud_mnto_auditoria_general', 'auditoria_general', 'id_aud_mnto', 'auditoria_mnto', 'id', 'CASCADE');
        $this->addForeignKey('fk-id_aud_ejec_auditoria_general', 'auditoria_general', 'id_aud_ejec', 'auditoria_ejec_mant', 'id', 'CASCADE');
        
    }

    public function safeDown()
    {
        echo "m180212_183759_create_auditoria_general cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180212_183759_create_auditoria_general cannot be reverted.\n";

        return false;
    }
    */
}
