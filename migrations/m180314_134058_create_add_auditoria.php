<?php

use yii\db\Migration;

class m180314_134058_create_add_auditoria extends Migration
{
    public function safeUp()
    {
 
        $this->createTable('otras_fs', [
            'id' => $this->primaryKey(),
            'id_modelo' => $this->integer(),
            'causa' => $this->string(200),
            'responsable' => $this->string(20),
            'observacion' => $this->string(1000),
            'id_aud_fs' => $this->integer()->notNull(),
            'numero' => $this->integer(),
            'solicitud' => $this->string(500),
            'cantidad' => $this->integer(),
            ]);

        $this->addForeignKey('fk-otras_fs_id_modelo', 'otras_fs', 'id_modelo', 'modelo', 'id_modelo', 'CASCADE');
        $this->addForeignKey('fk-id_aud_fs_otras_fs', 'otras_fs', 'id_aud_fs', 'auditoria_fs', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m180314_134058_create_add_auditoria cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180314_134058_create_add_auditoria cannot be reverted.\n";

        return false;
    }
    */
}
