<?php

use yii\db\Migration;

class m171117_113432_create_marca_proveedor extends Migration
{
    public function safeUp()
    {
             $this->createTable('marca_proveedor', [
            'id' => $this->primaryKey(),
            'id_proveedor' => $this->integer(),
            'id_marca' => $this->integer(),
 


        ]);

        $this->addForeignKey('fk-id_marca_proveedor_id_marca', 'marca_proveedor', 'id_marca', 'marca', 'id_marca', 'CASCADE');
        $this->addForeignKey('fk-id_marca_proveedor_id_proveedor', 'marca_proveedor', 'id_proveedor', 'proveedor', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m171117_113432_create_marca_proveedor cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171117_113432_create_marca_proveedor cannot be reverted.\n";

        return false;
    }
    */
}
