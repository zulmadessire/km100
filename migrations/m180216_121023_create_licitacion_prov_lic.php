<?php

use yii\db\Migration;

class m180216_121023_create_licitacion_prov_lic extends Migration
{
    public function safeUp()
    {
$this->createTable('proveedor_pieza_lic', [
            'id' => $this->primaryKey(),
            'id_licitacion' => $this->integer(),
            'id_prov_pieza' => $this->integer(),
            'estado' => $this->integer(),
            'fecha' => $this->date(),
       ]);
            
        $this->addForeignKey('fk-proveedor_pieza_lic_id_licitacion', 'proveedor_pieza_lic', 'id_licitacion', 'licitacion', 'id', 'CASCADE');
        $this->addForeignKey('fk-proveedor_pieza_lic_id_prov_pieza', 'proveedor_pieza_lic', 'id_prov_pieza', 'proveedor_pieza', 'id', 'CASCADE');

          $this->createTable('precio_proveedor', [
            'id' => $this->primaryKey(),
            'garantia' => $this->string(20),
            'tiempo_santo_domingo' => $this->string(30),
            'tiempo_interior' => $this->string(30),
            'dias' => $this->string(20),
            'alineacion' => $this->integer(),
            'obs_alineacion' => $this->string(10),
            'capacitacion' => $this->integer(),
            'obs_capacitacion' => $this->string(10),
            'entrega_via' => $this->string(10),
            'obs_entrega_via' => $this->string(10),
            'delivery' => $this->integer(),
            'obs_delivery' => $this->string(10),
            'id_prov_licitacion' => $this->integer(),

       ]);

        $this->addForeignKey('fk-precio_proveedor_id_prov_licitacion', 'precio_proveedor', 'id_prov_licitacion', 'proveedor_pieza_lic', 'id', 'CASCADE');

        $this->createTable('precios', [
            'id' => $this->primaryKey(),
            'modelo' => $this->string(40),
            'marca' => $this->string(40),
            'precio_mercado' => $this->double(),
            'porc_descuento' => $this->double(),
            'precio_f' => $this->double(),
            'id_prec_prov' => $this->integer(),

       ]);

        $this->addForeignKey('fk-precios_id_prec_prov', 'precios', 'id_prec_prov', 'precio_proveedor', 'id', 'CASCADE');

    }

    public function safeDown()
    {
        echo "m180216_121023_create_licitacion_prov_lic cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180216_121023_create_licitacion_prov_lic cannot be reverted.\n";

        return false;
    }
    */
}
