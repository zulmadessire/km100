<?php

use yii\db\Migration;

class m171002_121540_add_empresa extends Migration
{
    public function safeUp()
    {
        $this->addColumn('estacion', 'direccion', $this->string(200)->notNull());
        $this->addColumn('estacion', 'coord_google_maps', $this->string(200)->notNull());
        $this->addColumn('estacion', 'id_wizard', $this->integer()->notNull());
    }

    public function safeDown()
    {
        echo "m171002_121540_add_empresa cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171002_121540_add_empresa cannot be reverted.\n";

        return false;
    }
    */
}
