<?php

use yii\db\Migration;

/**
 * Class m180212_092633_add_id_servicio_taller_cotizacion
 */
class m180212_092633_add_id_servicio_taller_cotizacion extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('cotizacion_taller_actividad', 'id_servicio_taller',  $this->integer());

        $this->addForeignKey('fk-cotizacion_taller_actividad_id_servicio_taller', 'cotizacion_taller_actividad', 'id_servicio_taller', 'servicio_taller', 'id_servicio_taller', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180212_092633_add_id_servicio_taller_cotizacion cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180212_092633_add_id_servicio_taller_cotizacion cannot be reverted.\n";

        return false;
    }
    */
}
