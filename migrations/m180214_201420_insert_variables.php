<?php

use yii\db\Migration;

/**
 * Class m180214_201420_insert_variables
 */
class m180214_201420_insert_variables extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('variables',[
            'label' => 'Monto límite mantenimiento Analista',
            'descripcion' => 'Monto límite de mantenimiento Analista',
            'value' => '2000',
            'key' => 'mlma',
        ]);

        $this->addColumn('servicio', 'id_analista',  $this->integer());

        $this->addForeignKey('fk-servicio_id_analista', 'servicio', 'id_analista', 'user', 'id', 'CASCADE');



    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180214_201420_insert_variables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180214_201420_insert_variables cannot be reverted.\n";

        return false;
    }
    */
}
