<?php

use yii\db\Migration;

class m170913_110724_add_orden_trabajo extends Migration
{
    public function safeUp()
    {
        $this->addColumn('orden_trabajo', 'motivo_cancelacion', $this->integer());
        $this->addColumn('orden_trabajo', 'aprobacion_cancelacion', $this->integer());
    }

    public function safeDown()
    {
        echo "m170913_110724_add_orden_trabajo cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170913_110724_add_orden_trabajo cannot be reverted.\n";

        return false;
    }
    */
}
