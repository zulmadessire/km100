<?php

use yii\db\Migration;

/**
 * Class m180322_163542_add_km_garantia_cotizacion_proveedor_pieza
 */
class m180322_163542_add_km_garantia_cotizacion_proveedor_pieza extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('cotizacion_proveedor_pieza', 'km_garantia', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180322_163542_add_km_garantia_cotizacion_proveedor_pieza cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180322_163542_add_km_garantia_cotizacion_proveedor_pieza cannot be reverted.\n";

        return false;
    }
    */
}
