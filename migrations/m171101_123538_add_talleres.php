<?php

use yii\db\Migration;

class m171101_123538_add_talleres extends Migration
{
    public function safeUp()
    {
        $this->addColumn('taller', 'dias', $this->string(20));
    }

    public function safeDown()
    {
        echo "m171101_123538_add_talleres cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171101_123538_add_talleres cannot be reverted.\n";

        return false;
    }
    */
}
