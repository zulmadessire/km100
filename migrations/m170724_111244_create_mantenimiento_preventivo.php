<?php

use yii\db\Migration;

class m170724_111244_create_mantenimiento_preventivo extends Migration
{
    public function safeUp()
    {
        $this->createTable('mantenimiento_preventivo', [
            'id' => $this->primaryKey(),
            'imagen_soporte' => $this->string(200),
            'responsable' => $this->string(45)->notNull(),
            'fecha_realizacion' => $this->date(),
            'observaciones' => $this->text(),
            'km' => $this->string(45),
            'id_estacion' => $this->integer()->notNull(),
            'id_tipo_mantenimiento' => $this->integer(),
            'id_proyeccion_mantenimiento' => $this->integer(),
        ]);

        $this->createIndex('idx-mantenimiento_preventivo_estacion','mantenimiento_preventivo','id_estacion');
        $this->addForeignKey('fk-mantenimiento_preventivo_estacion', 'mantenimiento_preventivo', 'id_estacion', 'estacion', 'id_estacion', 'CASCADE');

        $this->createIndex('idx-mantenimiento_preventivo_tipo_mnto','mantenimiento_preventivo','id_tipo_mantenimiento');
        $this->addForeignKey('fk-mantenimiento_preventivo_tipo_mnto', 'mantenimiento_preventivo', 'id_tipo_mantenimiento', 'tipo_mantenimiento', 'id_tipo_mnto', 'CASCADE');

        $this->createIndex('idx-mantenimiento_preventivo_proyeccion','mantenimiento_preventivo','id_proyeccion_mantenimiento');
        $this->addForeignKey('fk-mantenimiento_preventivo_proyeccion', 'mantenimiento_preventivo', 'id_proyeccion_mantenimiento', 'proyeccion_mantenimiento', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m170724_111244_create_mantenimiento_preventivo cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170724_111244_create_mantenimiento_preventivo cannot be reverted.\n";

        return false;
    }
    */
}
