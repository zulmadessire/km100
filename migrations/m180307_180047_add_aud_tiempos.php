<?php

use yii\db\Migration;

class m180307_180047_add_aud_tiempos extends Migration
{
    public function safeUp()
    {
        $this->addColumn('auditoria_fs', 'mensual', $this->integer());
        $this->addColumn('auditoria_mnto', 'mensual', $this->integer());
        $this->addColumn('auditoria_ejec_mant', 'mensual', $this->integer());


    }

    public function safeDown()
    {
        echo "m180307_180047_add_aud_tiempos cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180307_180047_add_aud_tiempos cannot be reverted.\n";

        return false;
    }
    */
}
