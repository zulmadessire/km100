<?php

use yii\db\Migration;

/**
 * Class m180218_103207_create_devolucion_piezas
 */
class m180218_103207_create_devolucion_piezas extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('motivo_devolucion', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(200)->notNull(),
        ]);

        $this->insert('motivo_devolucion',[
            'nombre' => 'Entrega de pieza incorrecta',
        ]);
        $this->insert('motivo_devolucion',[
            'nombre' => 'Pieza de baja calidad',
        ]);
        $this->insert('motivo_devolucion',[
            'nombre' => 'Falta de verificación logistica KM100',
        ]);
        $this->insert('motivo_devolucion',[
            'nombre' => 'Aprobación de pieza solicitada anteriormente',
        ]);
        $this->insert('motivo_devolucion',[
            'nombre' => 'Solicitud de pieza erronea',
        ]);

        $this->addColumn('cotizacion_proveedor_pieza', 'cantidad_devolucion',  $this->integer());
        $this->addColumn('cotizacion_proveedor_pieza', 'id_usuario_devuelve',  $this->integer());
        $this->addColumn('cotizacion_proveedor_pieza', 'fecha_devolucion',  $this->date());
        $this->addColumn('cotizacion_proveedor_pieza', 'fecha_entrega_nueva',  $this->date());
        $this->addColumn('cotizacion_proveedor_pieza', 'id_motivo_devolucion',  $this->integer());

        $this->createIndex('idx-cotizacion_proveedor_pieza_motivo','cotizacion_proveedor_pieza','id_motivo_devolucion');
        $this->addForeignKey('fk-cotizacion_proveedor_pieza_motivo', 'cotizacion_proveedor_pieza', 'id_motivo_devolucion', 'motivo_devolucion', 'id', 'CASCADE');

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180218_103207_create_devolucion_piezas cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180218_103207_create_devolucion_piezas cannot be reverted.\n";

        return false;
    }
    */
}
