<?php

use yii\db\Migration;

class m180126_142304_add_observacion_gerente_solicitud_compra_pieza extends Migration
{
    public function safeUp()
    {
        $this->addColumn('solicitud_compra_pieza', 'observacion_gerente', $this->text());
    }

    public function safeDown()
    {
        echo "m180126_142304_add_observacion_gerente_solicitud_compra_pieza cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180126_142304_add_observacion_gerente_solicitud_compra_pieza cannot be reverted.\n";

        return false;
    }
    */
}
