<?php

use yii\db\Migration;

class m171124_133231_create_cliente extends Migration
{
    public function safeUp()
    {
          $this->createTable('cliente', [
            'id' => $this->primaryKey(),
            'nombre_comercial' => $this->string(100),
        ]);
        $this->createTable('estacion_usuario', [
            'id' => $this->primaryKey(),
           'user_id' => $this->integer()->notNull(),
           'id_estacion' => $this->integer()->notNull(),
        ]);

        $this->addColumn('empresa', 'cliente_id', $this->integer());
        $this->addForeignKey('fk-cliente_id_empresa_cliente', 'empresa', 'cliente_id', 'cliente', 'id', 'CASCADE');
        $this->addForeignKey('fk-user_id_estacion_usuario', 'estacion_usuario', 'user_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk-id_estacion_estacion_usuario', 'estacion_usuario', 'id_estacion', 'estacion', 'id_estacion', 'CASCADE');
    }

    public function safeDown()
    {
      
        echo "m171124_133231_create_cliente cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171124_133231_create_cliente cannot be reverted.\n";

        return false;
    }
    */
}
