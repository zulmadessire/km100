<?php

use yii\db\Migration;

/**
 * Class m180227_180521_add_proyeccion_mantenimiento
 */
class m180227_180521_add_proyeccion_mantenimiento extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('proyeccion_mantenimiento', 'fecha_reporte',  $this->date());
        $this->addColumn('proyeccion_mantenimiento', 'id_estacion_reporte',  $this->integer());

        $this->addColumn('mantenimiento_preventivo', 'id_usuario_registra',  $this->integer());
        $this->addColumn('mantenimiento_preventivo', 'id_vehiculo',  $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180227_180521_add_proyeccion_mantenimiento cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180227_180521_add_proyeccion_mantenimiento cannot be reverted.\n";

        return false;
    }
    */
}
