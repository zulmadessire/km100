<?php

use yii\db\Migration;

class m180223_180231_delete_fk_id_taller_despacho extends Migration
{
    public function safeUp()
    {
        $this->dropForeignKey(
            'fk-despacho_taller',
            'despacho'
        );

        $this->dropIndex(
            'idx-despacho_taller',
            'despacho'
        );
    }

    public function safeDown()
    {
        echo "m180223_180231_delete_fk_id_taller_despacho cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180223_180231_delete_fk_id_taller_despacho cannot be reverted.\n";

        return false;
    }
    */
}
