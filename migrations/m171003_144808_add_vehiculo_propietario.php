<?php

use yii\db\Migration;

class m171003_144808_add_vehiculo_propietario extends Migration
{
    public function safeUp()
    {

        $this->addColumn('vehiculo', 'id_propietario', $this->integer());
    }

    public function safeDown()
    {
        echo "m171003_144808_add_vehiculo_propietario cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171003_144808_add_vehiculo_propietario cannot be reverted.\n";

        return false;
    }
    */
}
