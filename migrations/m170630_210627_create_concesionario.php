<?php

use yii\db\Migration;

class m170630_210627_create_concesionario extends Migration
{
    public function up()
    {
         $this->createTable('concesionario', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(200)->notNull(),
        ]);
    }

    public function down()
    {
        echo "m170630_210627_create_concesionario cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
