<?php

use yii\db\Migration;

class m180117_190727_add_servicio_entrega_estacion extends Migration
{
    public function safeUp()
    {
        $this->addColumn('servicio', 'entrega_estacion', $this->integer(1));
    }

    public function safeDown()
    {
        echo "m180117_190727_add_servicio_entrega_estacion cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180117_190727_add_servicio_entrega_estacion cannot be reverted.\n";

        return false;
    }
    */
}
