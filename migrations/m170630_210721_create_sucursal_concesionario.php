<?php

use yii\db\Migration;

class m170630_210721_create_sucursal_concesionario extends Migration
{
    public function up()
    {
        $this->createTable('sucursal_concesionario', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(45)->notNull(),
            'direccion' => $this->string(200)->notNull(),
            'id_concesionario' => $this->integer(),
        ]);

        $this->createIndex('idx-concesionario_sucursal_concesionario','sucursal_concesionario','id_concesionario');
        $this->addForeignKey('fk-concesionario_sucursal_concesionario', 'sucursal_concesionario', 'id_concesionario', 'concesionario', 'id', 'CASCADE');
    }

    public function down()
    {
        echo "m170630_210721_create_sucursal_concesionario cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
