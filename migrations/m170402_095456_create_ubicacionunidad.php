<?php

use yii\db\Migration;

class m170402_095456_create_ubicacionunidad extends Migration
{
    public function up()
    {
        $this->createTable('ubicacionunidad', [
            'id_ubicacionunidad' => $this->primaryKey(),
            'nombre' => $this->string(50)->notNull(),
        ]);
    }

    public function down()
    {
        echo "m170402_095456_create_ubicacionunidad cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
