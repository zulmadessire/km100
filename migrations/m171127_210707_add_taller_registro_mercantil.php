<?php

use yii\db\Migration;

class m171127_210707_add_taller_registro_mercantil extends Migration
{
    public function safeUp()
    {
        $this->addColumn('taller', 'registro_mercantil', $this->string(100));
        $this->addColumn('taller', 'vencimiento_rm', $this->date());
    }

    public function safeDown()
    {
        echo "m171127_210707_add_taller_registro_mercantil cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171127_210707_add_taller_registro_mercantil cannot be reverted.\n";

        return false;
    }
    */
}
