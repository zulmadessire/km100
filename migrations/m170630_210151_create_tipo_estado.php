<?php

use yii\db\Migration;

class m170630_210151_create_tipo_estado extends Migration
{
    public function up()
    {
        $this->createTable('tipo_estado', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(45)->notNull(),
        ]);

        $this->addCommentOnColumn ('tipo_estado', 'nombre', '1-Estatus, 2-Fuera de servicio, 3-Documentación, 4-Mantenimiento');
    }

    public function down()
    {
        echo "m170630_210151_create_tipo_estado cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
