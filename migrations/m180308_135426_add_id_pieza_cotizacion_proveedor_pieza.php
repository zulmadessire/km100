<?php

use yii\db\Migration;

/**
 * Class m180308_135426_add_id_pieza_cotizacion_proveedor_pieza
 */
class m180308_135426_add_id_pieza_cotizacion_proveedor_pieza extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('cotizacion_proveedor_pieza', 'id_pieza', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180308_135426_add_id_pieza_cotizacion_proveedor_pieza cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180308_135426_add_id_pieza_cotizacion_proveedor_pieza cannot be reverted.\n";

        return false;
    }
    */
}
