<?php

use yii\db\Migration;

class m170928_111233_create_compra_pieza_cotizacion_proveedor extends Migration
{
    public function safeUp()
    {
        $this->createTable('solicitud_compra_proveedor', [
            'id' => $this->primaryKey(),
            'id_solicitud_compra_pieza' => $this->integer()->notNull(),
            'id_proveedor' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-solicitud_compra_proveedor_solicitud_compra_pieza','solicitud_compra_proveedor','id_solicitud_compra_pieza');
        $this->addForeignKey('fk-solicitud_compra_proveedor_solicitud_compra_pieza', 'solicitud_compra_proveedor', 'id_solicitud_compra_pieza', 'solicitud_compra_pieza', 'id', 'CASCADE');

        $this->createIndex('idx-solicitud_compra_proveedor_proveedor','solicitud_compra_proveedor','id_proveedor');
        $this->addForeignKey('fk-solicitud_compra_proveedor_proveedor', 'solicitud_compra_proveedor', 'id_proveedor', 'proveedor', 'id', 'CASCADE');
        
        $this->createTable('cotizacion_proveedor', [
            'id' => $this->primaryKey(),
            'fecha' => $this->timestamp(),
            'estado' => $this->integer(),
            'id_solicitud_compra_proveedor' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-cotizacion_proveedor_solicitud_compra_pieza','cotizacion_proveedor','id_solicitud_compra_proveedor');
        $this->addForeignKey('fk-cotizacion_proveedor_solicitud_compra_pieza', 'cotizacion_proveedor', 'id_solicitud_compra_proveedor', 'solicitud_compra_proveedor', 'id', 'CASCADE');
        
        $this->createTable('cotizacion_proveedor_pieza', [
            'id' => $this->primaryKey(),
            'cantidad_disponible' => $this->integer(),
            'cantidad_solicitada' => $this->integer(),
            'fecha_entrega' => $this->date(),
            'costo' => $this->double(),
            'ahorro' => $this->integer(),
            'id_cotizacion_proveedor' => $this->integer()->notNull(),
            'id_cotizacion_taller_pieza' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-cotizacion_proveedor_pieza_cotizacion_proveedor','cotizacion_proveedor_pieza','id_cotizacion_proveedor');
        $this->addForeignKey('fk-cotizacion_proveedor_pieza_cotizacion_proveedor', 'cotizacion_proveedor_pieza', 'id_cotizacion_proveedor', 'cotizacion_proveedor', 'id', 'CASCADE');

        $this->createIndex('idx-cotizacion_proveedor_pieza_cotizacion_taller_pieza','cotizacion_proveedor_pieza','id_cotizacion_taller_pieza');
        $this->addForeignKey('fk-cotizacion_proveedor_pieza_cotizacion_taller_pieza', 'cotizacion_proveedor_pieza', 'id_cotizacion_taller_pieza', 'cotizacion_taller_pieza', 'id', 'CASCADE');

    }

    public function safeDown()
    {
        echo "m170928_111233_create_compra_pieza_cotizacion_proveedor cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170928_111233_create_compra_pieza_cotizacion_proveedor cannot be reverted.\n";

        return false;
    }
    */
}
