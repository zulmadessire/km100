<?php

use yii\db\Migration;

class m171221_171146_add_cotizacion_taller_actividad_labor extends Migration
{
    public function safeUp()
    {
$this->addColumn('cotizacion_taller_actividad', 'labor',  $this->integer()); 
    }

    public function safeDown()
    {
        echo "m171221_171146_add_cotizacion_taller_actividad_labor cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171221_171146_add_cotizacion_taller_actividad_labor cannot be reverted.\n";

        return false;
    }
    */
}
