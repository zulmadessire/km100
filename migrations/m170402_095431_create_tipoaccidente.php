<?php

use yii\db\Migration;

class m170402_095431_create_tipoaccidente extends Migration
{
    public function up()
    {
        $this->createTable('tipoaccidente', [
            'id_tipoaccidente' => $this->primaryKey(),
            'nombre' => $this->string(100)->notNull(),
        ]);
    }

    public function down()
    {
        echo "m170402_095431_create_tipoaccidente cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
