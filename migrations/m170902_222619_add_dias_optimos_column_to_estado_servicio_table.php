<?php

use yii\db\Migration;

/**
 * Handles adding dias_optimos to table `estado_servicio`.
 */
class m170902_222619_add_dias_optimos_column_to_estado_servicio_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('estado_servicio', 'dias_optimos', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('estado_servicio', 'dias_optimos');
    }
}
