<?php

use yii\db\Migration;

/**
 * Class m171209_214154_update_concesionario
 */
class m171209_214154_update_concesionario extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        //Elimino los valores de las tablas
        $this->truncateTable('sucursal_concesionario');
        $this->truncateTable('detalle_garantia');
        $this->truncateTable('partes_garantia');

        //Elimino los campos de concesionario
        $this->dropColumn('concesionario', 'nombre_asesor');
        $this->dropColumn('concesionario', 'apellido');
        $this->dropColumn('concesionario', 'tlf');
        $this->dropColumn('concesionario', 'email');

        //Añado los campos del asesor a sucursal_concesionario
        $this->addColumn('sucursal_concesionario', 'nombre_asesor',  $this->string(45));
        $this->addColumn('sucursal_concesionario', 'apellido_asesor',  $this->string(45));
        $this->addColumn('sucursal_concesionario', 'telefono_asesor',  $this->string(100));
        $this->addColumn('sucursal_concesionario', 'email_asesor',  $this->string(100));
        $this->addColumn('sucursal_concesionario', 'coord_google_maps',  $this->string(200));
        $this->addColumn('sucursal_concesionario', 'email',  $this->string(200));
        $this->addColumn('sucursal_concesionario', 'telefono',  $this->string(200));

        //elimino los campos de detalle garantia y agrego la marca y el concesionario
        $this->dropColumn('detalle_garantia', 'modelo_id_modelo');
        $this->dropColumn('detalle_garantia', 'sucursal_concesionario_id');
        $this->addColumn('detalle_garantia', 'id_marca',  $this->integer()->notNull());
        $this->addColumn('detalle_garantia', 'id_concesionario',  $this->integer()->notNull());

        $this->createIndex('idx-marca_detalle_garantia','detalle_garantia','id_marca');
        $this->addForeignKey('fk-marca_detalle_garantia', 'detalle_garantia', 'id_marca', 'marca', 'id_marca', 'CASCADE');

        $this->createIndex('idx-concesionario_detalle_garantia','detalle_garantia','id_concesionario');
        $this->addForeignKey('fk-concesionario_detalle_garantia', 'detalle_garantia', 'id_concesionario', 'concesionario', 'id', 'CASCADE');

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171209_214154_update_concesionario cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171209_214154_update_concesionario cannot be reverted.\n";

        return false;
    }
    */
}
