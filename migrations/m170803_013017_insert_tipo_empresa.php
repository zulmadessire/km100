<?php

use yii\db\Migration;

class m170803_013017_insert_tipo_empresa extends Migration
{
    public function safeUp()
    {
         $this->insert('tipo_empresa',[
            'id' => '1',
            'nombre' =>'Nacional'
        ]);
             $this->insert('tipo_empresa',[
            'id' => '2',
            'nombre' =>'Internacional'
        ]);
    }

    public function safeDown()
    {
        echo "m170803_013017_insert_tipo_empresa cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170803_013017_insert_tipo_empresa cannot be reverted.\n";

        return false;
    }
    */
}
