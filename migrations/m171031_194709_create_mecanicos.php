<?php

use yii\db\Migration;

class m171031_194709_create_mecanicos extends Migration
{
    public function safeUp()
    {
       $this->createTable('mecanico', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(200)->notNull(),
            'cedula' => $this->string(20),
            'telefono' => $this->string(40),
            'direccion' => $this->string(450),

        ]);

    }

    public function safeDown()
    {
        echo "m171031_194709_create_mecanico cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171031_194709_create_mecanico cannot be reverted.\n";

        return false;
    }
    */
}
