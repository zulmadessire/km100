<?php

use yii\db\Migration;

class m171205_200846_add_profile extends Migration
{
    public function safeUp()
    {
        $this->addColumn('profile', 'telefono', $this->string(30));
        $this->addColumn('profile', 'cedula', $this->string(30));
    }

    public function safeDown()
    {
        echo "m171205_200846_add_profile cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171205_200846_add_profile cannot be reverted.\n";

        return false;
    }
    */
}
