<?php

use yii\db\Migration;

class m180305_015833_add_auditoria_mnto extends Migration
{
    public function safeUp()
    {
$this->addColumn('add_mnto', 'ficha', $this->string(50));
$this->addColumn('add_mnto', 'id_modelo', $this->integer());

 
    }

    public function safeDown()
    {
        echo "m180305_015833_add_auditoria_mnto cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180305_015833_add_auditoria_mnto cannot be reverted.\n";

        return false;
    }
    */
}
