<?php

use yii\db\Migration;

class m170402_105128_create_modelo extends Migration
{
    public function up()
    {
        $this->createTable('modelo', [
            'id_modelo' => $this->primaryKey(),
            'nombre' => $this->string(50)->notNull(),
            'id_marca' => $this->integer()->notNull(),
        ]);
        $this->createIndex(
            'idx-marca-modelo',
            'modelo',
            'id_marca'
        );
        $this->addForeignKey('fk-marca-modelo', 'modelo', 'id_marca', 'marca', 'id_marca', 'CASCADE');
    }

    public function down()
    {
        echo "m170402_105128_create_modelo cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
