<?php

use yii\db\Migration;

class m170919_090846_create_gestion_piezas extends Migration
{
    public function safeUp()
    {
        $this->createTable('solicitud_compra_pieza', [
            'id' => $this->primaryKey(),
            'fecha' => $this->timestamp(),
            'codigo_estado' => $this->integer(),
            'id_servicio' => $this->integer(),
            'id_cotizacion_taller' => $this->integer(),
            'id_orden_trabajo' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-orden_trabajo_solicitud_compra_pieza','solicitud_compra_pieza','id_orden_trabajo');
        $this->addForeignKey('fk-orden_trabajo_solicitud_compra_pieza', 'solicitud_compra_pieza', 'id_orden_trabajo', 'orden_trabajo', 'id', 'CASCADE');
        
        $this->createTable('estado_compra_pieza', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(45)->notNull(),
            'color' => $this->string(10)->notNull(),
            'codigo' => $this->integer(),
            'dias_optimos' => $this->integer(),
        ]);

        $this->insert('estado_compra_pieza',[
            'nombre' => 'Asignado',
            'color' => '#5b9bd5',
            'codigo' => 0,
            'dias_optimos' => 1,
        ]);

        $this->insert('estado_compra_pieza',[
            'nombre' => 'En cotización',
            'color' => '#ad2e74',
            'codigo' => 1,
            'dias_optimos' => 1,
        ]);

        $this->insert('estado_compra_pieza',[
            'nombre' => 'Aprobación',
            'color' => '#ed7d31',
            'codigo' => 2,
            'dias_optimos' => 1,
        ]);

        $this->insert('estado_compra_pieza',[
            'nombre' => 'Por recibir',
            'color' => '#ecb407',
            'codigo' => 3,
            'dias_optimos' => 1,
        ]);

        $this->insert('estado_compra_pieza',[
            'nombre' => 'Asignacion ruta',
            'color' => '#676767',
            'codigo' => 4,
            'dias_optimos' => 1,
        ]);

        $this->insert('estado_compra_pieza',[
            'nombre' => 'Entregada',
            'color' => '#70b93e',
            'codigo' => 5,
            'dias_optimos' => 1,
        ]);

        $this->insert('estado_compra_pieza',[
            'nombre' => 'Cerrado',
            'color' => '#58028e',
            'codigo' => 6,
            'dias_optimos' => 1,
        ]);

        $this->createTable('pipeline_compra_pieza', [
            'id' => $this->primaryKey(),
            'fecha' => $this->date()->notNull(),
            'id_solicitud_compra_pieza' => $this->integer()->notNull(),
            'id_estado_compra_pieza' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-solicitud_compra_pieza_pipeline','pipeline_compra_pieza','id_solicitud_compra_pieza');
        $this->addForeignKey('fk-solicitud_compra_pieza_pipeline', 'pipeline_compra_pieza', 'id_solicitud_compra_pieza', 'solicitud_compra_pieza', 'id', 'CASCADE');

        $this->createIndex('idx-estado_compra_pieza_pipeline','pipeline_compra_pieza','id_estado_compra_pieza');
        $this->addForeignKey('fk-estado_compra_pieza_pipeline', 'pipeline_compra_pieza', 'id_estado_compra_pieza', 'estado_compra_pieza', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m170919_090846_create_gestion_piezas cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170919_090846_create_gestion_piezas cannot be reverted.\n";

        return false;
    }
    */
}
