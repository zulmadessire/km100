<?php

use yii\db\Migration;

class m171128_183720_add_proveedor_codigo_proveedor extends Migration
{
    public function safeUp()
    {
        $this->addColumn('proveedor', 'codigo_proveedor', $this->string(30));
    }

    public function safeDown()
    {
        echo "m171128_183720_add_proveedor_codigo_proveedor cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171128_183720_add_proveedor_codigo_proveedor cannot be reverted.\n";

        return false;
    }
    */
}
