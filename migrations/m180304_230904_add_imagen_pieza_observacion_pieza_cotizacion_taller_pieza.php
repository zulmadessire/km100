<?php

use yii\db\Migration;

/**
 * Class m180304_230904_add_imagen_pieza_observacion_pieza_cotizacion_taller_pieza
 */
class m180304_230904_add_imagen_pieza_observacion_pieza_cotizacion_taller_pieza extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('cotizacion_taller_pieza', 'imagen_pieza', $this->string(255));
        $this->addColumn('cotizacion_taller_pieza', 'observacion_pieza', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180304_230904_add_imagen_pieza_observacion_pieza_cotizacion_taller_pieza cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180304_230904_add_imagen_pieza_observacion_pieza_cotizacion_taller_pieza cannot be reverted.\n";

        return false;
    }
    */
}
