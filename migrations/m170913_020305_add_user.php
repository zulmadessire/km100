<?php

use yii\db\Migration;

class m170913_020305_add_user extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user', 'id_analista', $this->integer());
    }

    public function safeDown()
    {
        echo "m170913_020305_add_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170913_020305_add_user cannot be reverted.\n";

        return false;
    }
    */
}
