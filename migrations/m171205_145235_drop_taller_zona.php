<?php

use yii\db\Migration;

class m171205_145235_drop_taller_zona extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('taller', 'zona');
    }

    public function safeDown()
    {
        $this->addColumn('taller', 'zona', $this->string(50));
        echo "m171205_145235_drop_taller_zona cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171205_145235_drop_taller_zona cannot be reverted.\n";

        return false;
    }
    */
}
