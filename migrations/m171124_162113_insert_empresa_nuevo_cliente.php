<?php

use yii\db\Migration;

class m171124_162113_insert_empresa_nuevo_cliente extends Migration
{
    public function safeUp()
    {
             $this->insert('empresa',[
            'nombre' => 'AVIS',
            'cliente_id' => '1',
        ]);
              $this->insert('empresa',[
            'nombre' => 'Budget',
            'cliente_id' => '1',
        ]);
            $this->insert('empresa',[
            'nombre' => 'Europcar',
            'cliente_id' => '2',
        ]);
            $this->insert('empresa',[
            'nombre' => 'Hertz',
            'cliente_id' => '3',
        ]);
            $this->insert('empresa',[
            'nombre' => 'Nelly',
            'cliente_id' => '4',
        ]);
            $this->insert('empresa',[
            'nombre' => 'Payless',
            'cliente_id' => '1',
        ]);
            $this->insert('empresa',[
            'nombre' => 'Thrifty',
            'cliente_id' => '5',
        ]);
                  $this->insert('empresa',[
            'nombre' => 'Fideicomiso Administrativo',
            'cliente_id' => '6',
        ]);
            $this->insert('empresa',[
            'nombre' => 'Firefly',
            'cliente_id' => '5',
        ]);
    }

    public function safeDown()
    {
        echo "m171124_162113_insert_empresa_nuevo_cliente cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171124_162113_insert_empresa_nuevo_cliente cannot be reverted.\n";

        return false;
    }
    */
}
