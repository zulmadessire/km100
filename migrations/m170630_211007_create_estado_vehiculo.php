<?php

use yii\db\Migration;

class m170630_211007_create_estado_vehiculo extends Migration
{
    public function up()
    {
        $this->createTable('estado_vehiculo', [
            'id' => $this->primaryKey(),
            'id_estado' => $this->integer()->notNull(),
            'id_vehiculo' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-estado_estado_vehiculo','estado_vehiculo','id_estado');
        $this->addForeignKey('fk-estado_estado_vehiculo', 'estado_vehiculo', 'id_estado', 'estado', 'id', 'CASCADE');

        $this->createIndex('idx-vehiculo_estado_vehiculo','estado_vehiculo','id_vehiculo');
        $this->addForeignKey('fk-vehiculo_estado_vehiculo', 'estado_vehiculo', 'id_vehiculo', 'vehiculo', 'id', 'CASCADE');
    }

    public function down()
    {
        echo "m170630_211007_create_estado_vehiculo cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
