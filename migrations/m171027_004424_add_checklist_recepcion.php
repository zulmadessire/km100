<?php

use yii\db\Migration;

class m171027_004424_add_checklist_recepcion extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cotizacion_proveedor_pieza', 'cantidad_recibida', $this->integer());
        $this->addColumn('orden_compra_pieza', 'observacion_recepcion', $this->text());
        $this->addColumn('solicitud_compra_pieza', 'observacion_recepcion', $this->text());
    }

    public function safeDown()
    {
        echo "m171027_004424_add_checklist_recepcion cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171027_004424_add_checklist_recepcion cannot be reverted.\n";

        return false;
    }
    */
}
