<?php

use yii\db\Migration;

class m170726_225651_create_conf_taller extends Migration
{
    public function safeUp()
    {
        $this->createTable('taller', [
            'id_taller' => $this->primaryKey(),
            'tipo_persona' => $this->integer(1)->notNull(),
            'rnc' => $this->string(50)->notNull(),
            'zona' => $this->string(50)->notNull(),   //////////// pendiente.....
            'nombre' => $this->string(50)->notNull(),
            'telefono' => $this->string(20)->notNull(),
            'direccion' => $this->string(150)->notNull(),
            'coord_google_maps' => $this->string(20)->notNull(),
            'tipo_pago' => $this->integer(1)->notNull(),
            'descuento' => $this->double(),
            'limite_credito' => $this->double(),
            'nombre_representante' => $this->string(50)->notNull(),
            'identif_representante' => $this->string(20)->notNull(),
            'email_representante' => $this->string(30)->notNull(),
            'celular_representante' => $this->string(20),
            'espacios' => $this->integer(),
            'nivel_servicio' => $this->integer(),
            'acuerdo_comercial' => $this->string(100)
        ]);
        $this->addCommentOnColumn ('taller', 'tipo_persona', '0-Natural, 1-Juridica');
        $this->addCommentOnColumn ('taller', 'tipo_pago', '0-Pronto Pago, 1-Crédito, 2-Contado');


        $this->createTable('tipo_servicio', [
            'id_tipo_servicio' => $this->primaryKey(),
            'nombre' => $this->string(50)->notNull()
        ]);


        $this->createTable('servicio_taller', [
            'id_servicio_taller' => $this->primaryKey(),
            'garantia' => $this->integer(1)->notNull(),
            'linea' => $this->integer(1)->notNull(),
            'id_tipo_servicio' => $this->integer()->notNull(),
            'id_taller' => $this->integer()->notNull()
        ]);
        $this->addCommentOnColumn ('servicio_taller', 'garantia', 'De 0 a 24 meses');
        $this->addCommentOnColumn ('servicio_taller', 'linea', '0-Quick Line, 1-Normal Line');
        $this->createIndex('idx-tiposervicio-serviciotaller','servicio_taller','id_tipo_servicio');
        $this->addForeignKey('fk-tiposervicio-serviciotaller', 'servicio_taller', 'id_tipo_servicio', 'tipo_servicio', 'id_tipo_servicio', 'CASCADE');
        $this->createIndex('idx-taller-serviciotaller','servicio_taller','id_taller');
        $this->addForeignKey('fk-taller-serviciotaller', 'servicio_taller', 'id_taller', 'taller', 'id_taller', 'CASCADE');


        $this->createTable('vehiculo_taller', [
            'id_vehiculo_taller' => $this->primaryKey(),
            'id_modelo' => $this->integer()->notNull(),
            'id_taller' => $this->integer()->notNull()
        ]);
        $this->createIndex('idx-modelo-vehiculotaller','vehiculo_taller','id_modelo');
        $this->addForeignKey('fk-modelo-vehiculotaller', 'vehiculo_taller', 'id_modelo', 'modelo', 'id_modelo', 'CASCADE');
        $this->createIndex('idx-taller-vehiculotaller','vehiculo_taller','id_taller');
        $this->addForeignKey('fk-taller-vehiculotaller', 'vehiculo_taller', 'id_taller', 'taller', 'id_taller', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m170726_225651_create_conf_taller cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170726_225651_create_conf_taller cannot be reverted.\n";

        return false;
    }
    */
}
