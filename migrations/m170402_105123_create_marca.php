<?php

use yii\db\Migration;

class m170402_105123_create_marca extends Migration
{
    public function up()
    {
        $this->createTable('marca', [
            'id_marca' => $this->primaryKey(),
            'nombre' => $this->string(50)->notNull(),
        ]);
    }

    public function down()
    {
        echo "m170402_105123_create_marca cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
