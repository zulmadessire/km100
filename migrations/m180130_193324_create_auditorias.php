<?php

use yii\db\Migration;

class m180130_193324_create_auditorias extends Migration
{
    public function safeUp()
    {
         $this->createTable('auditoria_fs', [
            'id' => $this->primaryKey(),
            'fecha' => $this->date(),
            'estado' => $this->integer(),
            'motivo' => $this->string(500),
            'desviacion' => $this->integer(),
            'veh_fuera_ser_est' => $this->integer(),
            'plan_accion' => $this->string(1000),
            'observacion' => $this->string(1000),
            'id_estacion' => $this->integer()->notNull(),
            ]);

        $this->addForeignKey('fk-estacion_auditoria_fs', 'auditoria_fs', 'id_estacion', 'estacion', 'id_estacion', 'CASCADE');

        $this->createTable('sol_aud_fs', [
            'id' => $this->primaryKey(),
            'numero' => $this->integer(),
            'solicitud' => $this->string(500),
            'cantidad' => $this->integer(),
            'observacion' => $this->string(1000),
            'id_aud_fs' => $this->integer()->notNull(),
            ]);

        $this->addForeignKey('fk-id_aud_fs_sol_aud_fs', 'sol_aud_fs', 'id_aud_fs', 'auditoria_fs', 'id', 'CASCADE');

        $this->createTable('add_fs', [
            'id' => $this->primaryKey(),
            'causa' => $this->string(500),
            'observacion' => $this->string(1000),
            'id_aud_fs' => $this->integer()->notNull(),
            ]);

        $this->addForeignKey('fk-id_aud_fs_add_fs', 'add_fs', 'id_aud_fs', 'auditoria_fs', 'id', 'CASCADE');

         
    }

    public function safeDown()
    {
        echo "m180130_193324_create_auditorias cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180130_193324_create_auditorias cannot be reverted.\n";

        return false;
    }
    */
}
