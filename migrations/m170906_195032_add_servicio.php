<?php

use yii\db\Migration;

class m170906_195032_add_servicio extends Migration
{
    public function safeUp()
    {
        $this->addColumn('servicio', 'decision', $this->integer(1));
    }

    public function safeDown()
    {
        echo "m170906_195032_add_servicio cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170906_195032_add_servicio cannot be reverted.\n";

        return false;
    }
    */
}
