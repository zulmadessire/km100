<?php

use yii\db\Migration;

class m170630_210821_create_vehiculo extends Migration
{
    public function up()
    {
        $this->createTable('categoria', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(45)->notNull(),
            'seguimiento_km' => $this->string(45),
            'declive_km' => $this->string(45),
            'seguimiento_fecha' => $this->string(5),
            'declive_fecha' => $this->string(5),
        ]);
        
        $this->createTable('vehiculo', [
            'id' => $this->primaryKey(),
            'color' => $this->string(45),
            'transmision' => $this->string(45),
            'chasis' => $this->string(45),
            'placa' => $this->string(45),
            'mva' => $this->string(45),
            'categoria' => $this->string(45),
            'clase' => $this->string(45),
            'combustible' => $this->string(45),
            'fecha_compra' => $this->date(),
            'fecha_inicial' => $this->date(),
            'tiempo_vida' => $this->integer(),
            'km' => $this->string(45),
            'fecha_renovacion_placa' => $this->date(),
            'fecha_emision_placa' => $this->date(),
            'wizard' => $this->boolean(),
            'exactus' => $this->boolean(),
            'id_modelo' => $this->integer(),
            'id_concesionario' => $this->integer(),
            'id_categoria' => $this->integer(),
        ]);

        $this->createIndex('idx-vehiculo_modelo','vehiculo','id_modelo');
        $this->addForeignKey('fk-vehiculo_modelo', 'vehiculo', 'id_modelo', 'modelo', 'id_modelo', 'CASCADE');

        $this->createIndex('idx-vehiculo_concesionario','vehiculo','id_concesionario');
        $this->addForeignKey('fk-vehiculo_concesionario', 'vehiculo', 'id_concesionario', 'concesionario', 'id', 'CASCADE');

        $this->createIndex('idx-vehiculo_categoria','vehiculo','id_categoria');
        $this->addForeignKey('fk-vehiculo_categoria', 'vehiculo', 'id_categoria', 'categoria', 'id', 'CASCADE');
    }

    public function down()
    {
        echo "m170630_210821_create_vehiculo cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
