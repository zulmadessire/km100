<?php

use yii\db\Migration;

class m170908_142546_create_auditoria extends Migration
{
    public function safeUp()
    {
        $this->createTable('auditoria', [
            'id' => $this->primaryKey(),
            'motivo' => $this->string(200),
            'planaccion' => $this->string(200),
            'fecha' => $this->string(200),
            'estacion_id_estacion' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-auditoria_estacion','auditoria','estacion_id_estacion');
        $this->addForeignKey('fk-auditoria_estacion', 'auditoria', 'estacion_id_estacion', 'estacion', 'id_estacion', 'CASCADE');

    }

    public function safeDown()
    {
        echo "m170908_142546_create_auditoria cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170908_142546_create_auditoria cannot be reverted.\n";

        return false;
    }
    */
}
