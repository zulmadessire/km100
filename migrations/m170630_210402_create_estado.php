<?php

use yii\db\Migration;

class m170630_210402_create_estado extends Migration
{
    public function up()
    {
        $this->createTable('estado', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(45)->notNull(),
            'color' => $this->string(10),
            'id_tipo_estado' => $this->integer(),
        ]);

        $this->createIndex('idx-estado_tipo_estado','estado','id_tipo_estado');
        $this->addForeignKey('fk-estado_tipo_estado', 'estado', 'id_tipo_estado', 'tipo_estado', 'id', 'CASCADE');

        $this->addCommentOnColumn ('estado', 'nombre', '1.1-Operativo, 1.2-Asignado, 1.3-Salvamento, 1.4-Remarketing, 1.5-Taller, 2.1-Estación, 2.2-Taller, 3.1-Registro, 4.1-Vencido, 4.2-Asignar, 4.3-Vigente');
    }

    public function down()
    {
        echo "m170630_210402_create_estado cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
