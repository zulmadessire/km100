<?php

use yii\db\Migration;

/**
 * Class m171114_204909_add_cotizacion_pieza_proveedor
 */
class m171114_204909_add_cotizacion_pieza_proveedor extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('cotizacion_proveedor', 'tipo_envio',  $this->integer());
        $this->addColumn('cotizacion_proveedor', 'subtotal',  $this->double());
        $this->addColumn('cotizacion_proveedor', 'descuento',  $this->double());
        $this->addColumn('cotizacion_proveedor', 'itbs',  $this->double());

        $this->addCommentOnColumn ('cotizacion_proveedor', 'tipo_envio', '0-Entrega a Domicio a Km100, 1-Entrega a Domicilio Taller, 2-No entrega a domicilio');
        
        $this->addColumn('cotizacion_proveedor_pieza', 'garantia',  $this->string(100));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171114_204909_add_cotizacion_pieza_proveedor cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171114_204909_add_cotizacion_pieza_proveedor cannot be reverted.\n";

        return false;
    }
    */
}
