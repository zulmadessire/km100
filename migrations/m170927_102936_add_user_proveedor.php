<?php

use yii\db\Migration;

class m170927_102936_add_user_proveedor extends Migration
{
    public function safeUp()
    {
        $this->addColumn('proveedor', 'user', $this->string(200));
        $this->addColumn('user', 'id_proveedor', $this->integer());
    }

    public function safeDown()
    {
        echo "m170927_102936_add_user_proveedor cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170927_102936_add_user_proveedor cannot be reverted.\n";

        return false;
    }
    */
}
