<?php

use yii\db\Migration;

class m180130_205335_create_auditorias_2 extends Migration
{
    public function safeUp()
    {
         //Auditorias de mantenimiento

        $this->createTable('auditoria_mnto', [
            'id' => $this->primaryKey(),
            'desviacion' => $this->integer(),
            'mntos_realizados' => $this->integer(),
            'mntos_smna_anterior' => $this->integer(),
            'fecha' => $this->date(),
            'observacion' => $this->string(1000),
            'estado' => $this->integer(),
            'id_estacion' => $this->integer()->notNull(),
            ]);

        $this->addForeignKey('fk-estacion_auditoria_mnto', 'auditoria_mnto', 'id_estacion', 'estacion', 'id_estacion', 'CASCADE');
        
        $this->createTable('add_mnto', [
            'id' => $this->primaryKey(),
            'observacion' => $this->string(1000),
            'id_aud_mnto' => $this->integer()->notNull(),
            ]);

        $this->addForeignKey('fk-id_aud_mnto_add_mnto', 'add_mnto', 'id_aud_mnto', 'auditoria_mnto', 'id', 'CASCADE');


        $this->createTable('sol_aud_mnto', [
            'id' => $this->primaryKey(),
            'numero' => $this->integer(),
            'ficha_mva' => $this->string(100),
            'km_actual' => $this->string(100),
            'observacion' => $this->string(1000),
            'id_modelo' => $this->integer(),
            'id_tipo_mnto' => $this->integer(),
            'id_aud_mnto' => $this->integer()->notNull(),
            ]);
        
        $this->addForeignKey('fk-id_aud_mnto_sol_aud_mnto', 'sol_aud_mnto', 'id_aud_mnto', 'auditoria_mnto', 'id', 'CASCADE');
        $this->addForeignKey('fk-id_tipo_mnto_sol_aud_mnto', 'sol_aud_mnto', 'id_tipo_mnto', 'tipo_mantenimiento', 'id_tipo_mnto', 'CASCADE');
        $this->addForeignKey('fk-id_modelo_sol_aud_mnto', 'sol_aud_mnto', 'id_modelo', 'modelo', 'id_modelo', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m180130_205335_create_auditorias_2 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180130_205335_create_auditorias_2 cannot be reverted.\n";

        return false;
    }
    */
}
