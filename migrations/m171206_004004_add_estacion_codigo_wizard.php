<?php

use yii\db\Migration;

class m171206_004004_add_estacion_codigo_wizard extends Migration
{
    public function safeUp()
    {
        $this->addColumn('estacion', 'codigo_generado', $this->string(10));
    }

    public function safeDown()
    {
        echo "m171206_004004_add_estacion_codigo_wizard cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171206_004004_add_estacion_codigo_wizard cannot be reverted.\n";

        return false;
    }
    */
}
