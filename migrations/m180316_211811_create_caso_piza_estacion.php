<?php

use yii\db\Migration;

/**
 * Class m180316_211811_create_caso_piza_estacion
 */
class m180316_211811_create_caso_piza_estacion extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('caso_pieza_estacion', [
            'id' => $this->primaryKey(),
            'id_caso' => $this->integer(),
            'id_solicitud_compra_estacion' => $this->integer(),
       ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180316_211811_create_caso_piza_estacion cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180316_211811_create_caso_piza_estacion cannot be reverted.\n";

        return false;
    }
    */
}
