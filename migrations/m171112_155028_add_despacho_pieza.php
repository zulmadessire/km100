<?php

use yii\db\Migration;

/**
 * Class m171112_155028_add_despacho_pieza
 */
class m171112_155028_add_despacho_pieza extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $this->addColumn('despacho', 'id_taller',  $this->integer());
        $this->addColumn('despacho', 'id_estacion',  $this->integer());
        $this->addColumn('despacho', 'id_mensajero',  $this->integer());

        $this->createIndex('idx-despacho_mensajero','despacho','id_mensajero');
        $this->addForeignKey('fk-despacho_mensajero', 'despacho', 'id_mensajero', 'mensajero', 'id', 'CASCADE');

        $this->createIndex('idx-despacho_taller','despacho','id_taller');
        $this->addForeignKey('fk-despacho_taller', 'despacho', 'id_taller', 'taller', 'id_taller', 'CASCADE');
        
        $this->createIndex('idx-despacho_estacion','despacho','id_estacion');
        $this->addForeignKey('fk-despacho_estacion', 'despacho', 'id_estacion', 'estacion', 'id_estacion', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171112_155028_add_despacho_pieza cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171112_155028_add_despacho_pieza cannot be reverted.\n";

        return false;
    }
    */
}
