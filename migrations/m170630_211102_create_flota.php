<?php

use yii\db\Migration;

class m170630_211102_create_flota extends Migration
{
    public function up()
    {
        $this->createTable('flota', [
            'id' => $this->primaryKey(),
            'fecha_inicio' => $this->date()->notNull(),
            'fecha_fin' => $this->date(),
            'id_estacion' => $this->integer()->notNull(),
            'id_vehiculo' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-flota_estacion','flota','id_estacion');
        $this->addForeignKey('fk-flota_estacion', 'flota', 'id_estacion', 'estacion', 'id_estacion', 'CASCADE');

        $this->createIndex('idx-flota_vehiculo','flota','id_vehiculo');
        $this->addForeignKey('fk-flota_vehiculo', 'flota', 'id_vehiculo', 'vehiculo', 'id', 'CASCADE');
    }

    public function down()
    {
        echo "m170630_211102_create_flota cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
