<?php

use yii\db\Migration;

class m170713_012841_create_garantia_vehiculo extends Migration
{
    public function safeUp()
    {
        $this->createTable('garantia_vehiculo', [
            'id' => $this->primaryKey(),
            'tiempo' => $this->integer(),
            'periodo' => $this->string(10),
            'km' => $this->string(45),
            'anular' => $this->boolean()->defaultValue(0),
            'motivo' => $this->integer(),
            'id_garantia' => $this->integer()->notNull(),
            'id_vehiculo' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-garantia_garantia_vehiculo','garantia_vehiculo','id_garantia');
        $this->addForeignKey('fk-garantia_garantia_vehiculo', 'garantia_vehiculo', 'id_garantia', 'garantia', 'id', 'CASCADE');

        $this->createIndex('idx-garantia_vehiculo_vehiculo','garantia_vehiculo','id_vehiculo');
        $this->addForeignKey('fk-garantia_vehiculo_vehiculo', 'garantia_vehiculo', 'id_vehiculo', 'vehiculo', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m170713_012841_create_garantia_vehiculo cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170713_012841_create_garantia_vehiculo cannot be reverted.\n";

        return false;
    }
    */
}
