<?php

use yii\db\Migration;

class m171128_205314_add_taller_vencimiento_acuerdo extends Migration
{
    public function safeUp()
    {
        $this->addColumn('taller', 'vencimiento_acuerdo', $this->date());
    }

    public function safeDown()
    {
        echo "m171128_205314_add_taller_vencimiento_acuerdo cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171128_205314_add_taller_vencimiento_acuerdo cannot be reverted.\n";

        return false;
    }
    */
}
