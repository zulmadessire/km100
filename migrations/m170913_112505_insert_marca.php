<?php

use yii\db\Migration;

class m170913_112505_insert_marca extends Migration
{
    public function safeUp()
    {
        $this->insert('marca',[
            'id_marca' => '25',
            'nombre' => 'BMW',
        ]);
     
    }

    public function safeDown()
    {
        echo "m170913_112505_insert_marca cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170913_112505_insert_marca cannot be reverted.\n";

        return false;
    }
    */
}
