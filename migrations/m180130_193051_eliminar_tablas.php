<?php

use yii\db\Migration;

class m180130_193051_eliminar_tablas extends Migration
{
    public function safeUp()
    {
        $this->dropTable('solicitudes');
        $this->dropTable('auditoria');
    }

    public function safeDown()
    {
        echo "m180130_193051_eliminar_tablas cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180130_193051_eliminar_tablas cannot be reverted.\n";

        return false;
    }
    */
}
