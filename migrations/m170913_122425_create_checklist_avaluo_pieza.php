<?php

use yii\db\Migration;

class m170913_122425_create_checklist_avaluo_pieza extends Migration
{
    public function safeUp()
    {
        $this->createTable('checklist_avaluo_pieza', [
            'id' => $this->primaryKey(),
            'accion' => $this->integer(),
            'id_checklist_avaluo' => $this->integer(),
            'pieza_id' => $this->integer(),
        ]);

        $this->addForeignKey('fk-pieza', 'checklist_avaluo_pieza', 'pieza_id', 'pieza', 'id_pieza', 'CASCADE');
        $this->addForeignKey('fk-checklist_avaluo', 'checklist_avaluo_pieza', 'id_checklist_avaluo', 'checklist_avaluo', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m170913_122425_create_checklist_avaluo_pieza cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170913_122425_create_checklist_avaluo_pieza cannot be reverted.\n";

        return false;
    }
    */
}
