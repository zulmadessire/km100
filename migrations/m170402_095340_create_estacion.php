<?php

use yii\db\Migration;

class m170402_095340_create_estacion extends Migration
{
    public function up()
    {
        $this->createTable('estacion', [
            'id_estacion' => $this->primaryKey(),
            'codigo' => $this->string(10)->notNull(),
            'nombre' => $this->string(50)->notNull(),
            'id_empresa' => $this->integer()->notNull(),
        ]);
        $this->createIndex(
            'idx-empresa-estacion',
            'estacion',
            'id_empresa'
        );
        $this->addForeignKey('fk-empresa-estacion', 'estacion', 'id_empresa', 'empresa', 'id_empresa', 'CASCADE');
    }

    public function down()
    {
        echo "m170402_095340_create_estacion cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
