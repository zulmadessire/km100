<?php

use yii\db\Migration;

class m170630_211416_insert_tipo_estado extends Migration
{
    public function up()
    {
        $this->insert('tipo_estado',[
            'nombre' => 'Estatus',
        ]);

        $this->insert('tipo_estado',[
            'nombre' => 'Fuera de servicio',
        ]);

        $this->insert('tipo_estado',[
            'nombre' => 'Documentación',
        ]);

        $this->insert('tipo_estado',[
            'nombre' => 'Mantenimiento',
        ]);

        $this->insert('tipo_estado',[
            'nombre' => 'Concesionario',
        ]);
    }

    public function down()
    {
        echo "m170630_211416_insert_tipo_estado cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
