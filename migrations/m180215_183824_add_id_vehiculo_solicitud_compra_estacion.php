<?php

use yii\db\Migration;

class m180215_183824_add_id_vehiculo_solicitud_compra_estacion extends Migration
{
    public function safeUp()
    {
        $this->addColumn('solicitud_compra_estacion', 'id_vehiculo', $this->integer());
        $this->createIndex('idx-solicitud-compra-estacion','solicitud_compra_estacion','id_vehiculo');
        $this->addForeignKey('fk-solicitud-compra-estacion', 'solicitud_compra_estacion', 'id_vehiculo', 'vehiculo', 'id', 'CASCADE');

    }

    public function safeDown()
    {
        echo "m180215_183824_add_id_vehiculo_solicitud_compra_estacion cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180215_183824_add_id_vehiculo_solicitud_compra_estacion cannot be reverted.\n";

        return false;
    }
    */
}
