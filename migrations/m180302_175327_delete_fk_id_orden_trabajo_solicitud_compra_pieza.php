<?php

use yii\db\Migration;

/**
 * Class m180302_175327_delete_fk_id_orden_trabajo_solicitud_compra_pieza
 */
class m180302_175327_delete_fk_id_orden_trabajo_solicitud_compra_pieza extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey(
            'fk-orden_trabajo_solicitud_compra_pieza',
            'solicitud_compra_pieza'
        );

        $this->dropIndex(
            'idx-orden_trabajo_solicitud_compra_pieza',
            'solicitud_compra_pieza'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180302_175327_delete_fk_id_orden_trabajo_solicitud_compra_pieza cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180302_175327_delete_fk_id_orden_trabajo_solicitud_compra_pieza cannot be reverted.\n";

        return false;
    }
    */
}
