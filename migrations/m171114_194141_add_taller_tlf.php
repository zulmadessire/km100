<?php

use yii\db\Migration;

class m171114_194141_add_taller_tlf extends Migration
{
    public function safeUp()
    {
        $this->addColumn('taller', 'tlf1',  $this->string(40));  
        $this->addColumn('taller', 'tlf2',  $this->string(40)); 
    }

    public function safeDown()
    {
        echo "m171114_194141_add_taller_tlf cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171114_194141_add_taller_tlf cannot be reverted.\n";

        return false;
    }
    */
}
