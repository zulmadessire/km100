<?php

use yii\db\Migration;

class m171124_143942_insert_cliente extends Migration
{
    public function safeUp()
    {
           $this->insert('cliente',[
            'nombre_comercial' => 'Servicolt/Leasing del atlántico',
        ]);
            $this->insert('cliente',[
            'nombre_comercial' => 'Leasing automotriz del sur',
        ]);
            $this->insert('cliente',[
            'nombre_comercial' => 'Mercantil Santo Domingo',
        ]);
            $this->insert('cliente',[
            'nombre_comercial' => 'Leasing de la Hispaniola',
        ]);
            $this->insert('cliente',[
            'nombre_comercial' => 'Andel Star',
        ]);
            $this->insert('cliente',[
            'nombre_comercial' => 'Fideicomiso Administrativo',
        ]);
  
    }

    public function safeDown()
    {
        echo "m171124_143942_insert_cliente cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171124_143942_insert_cliente cannot be reverted.\n";

        return false;
    }
    */
}
