<?php

use yii\db\Migration;

class m170913_113005_insert_modelo extends Migration
{
    public function safeUp()
    {

        $this->insert('modelo',[
            'nombre' => 'F150',
            'id_marca' => '10',
        ]);
        $this->insert('modelo',[
            'nombre' => 'AVEO',
            'id_marca' => '17',
        ]);
        $this->insert('modelo',[
            'nombre' => 'L300',
            'id_marca' => '14',
        ]);
        $this->insert('modelo',[
            'nombre' => 'Way',
            'id_marca' => '3',
        ]);
        $this->insert('modelo',[
            'nombre' => 'I-10',
            'id_marca' => '4',
        ]);
        $this->insert('modelo',[
            'nombre' => 'Udok',
            'id_marca' => '15',
        ]);
        $this->insert('modelo',[
            'nombre' => 'A6',
            'id_marca' => '9',
        ]);
        $this->insert('modelo',[
            'nombre' => '318I',
            'id_marca' => '25',
        ]);

    }

    public function safeDown()
    {
        echo "m170913_113005_insert_modelo cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170913_113005_insert_modelo cannot be reverted.\n";

        return false;
    }
    */
}
