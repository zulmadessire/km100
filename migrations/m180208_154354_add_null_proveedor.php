<?php

use yii\db\Migration;

class m180208_154354_add_null_proveedor extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('proveedor', 'credito');
        $this->addColumn('proveedor', 'credito', $this->double());
    }

    public function safeDown()
    {
        echo "m180208_154354_add_null_proveedor cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180208_154354_add_null_proveedor cannot be reverted.\n";

        return false;
    }
    */
}
