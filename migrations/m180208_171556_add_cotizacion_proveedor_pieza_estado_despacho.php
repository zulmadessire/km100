<?php

use yii\db\Migration;

/**
 * Class m180208_171556_add_cotizacion_proveedor_pieza_estado_despacho
 */
class m180208_171556_add_cotizacion_proveedor_pieza_estado_despacho extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('cotizacion_proveedor_pieza', 'estado_despacho',  $this->integer());
        $this->addCommentOnColumn ('cotizacion_proveedor_pieza', 'estado_despacho', 'Indica en que estado se encuentra el despacho de la pieza 0-En espera, 1-Enviada a mensajero, 2-Recibida por mensajero, 3-Entregada');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180208_171556_add_cotizacion_proveedor_pieza_estado_despacho cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180208_171556_add_cotizacion_proveedor_pieza_estado_despacho cannot be reverted.\n";

        return false;
    }
    */
}
