<?php

use yii\db\Migration;

class m180215_154232_create_solicitud_pieza_estacion extends Migration
{
    public function safeUp()
    {
        $this->createTable('solicitud_compra_estacion', [
            'id'                => $this->primaryKey(), 
            'tipo_solicitud'    => $this->integer(), 
            'id_estacion'       => $this->integer(),            
            'ficha'             => $this->string(20),
            'placa'             => $this->string(20),
            'id_modelo'         => $this->integer(),
            'fecha_registro'    => $this->date(),
        ]);

        $this->createIndex('idx-estacion-solicitud-compra-estacion','solicitud_compra_estacion','id_estacion');
        $this->addForeignKey('fk-estacion-solicitud-compra-estacion', 'solicitud_compra_estacion', 'id_estacion', 'estacion', 'id_estacion', 'CASCADE');

        $this->createIndex('idx-modelo-solicitud-compra-estacion','solicitud_compra_estacion','id_modelo');
        $this->addForeignKey('fk-modelo-solicitud-compra-estacion', 'solicitud_compra_estacion', 'id_modelo', 'modelo', 'id_modelo', 'CASCADE');
    

        $this->createTable('solicitud_pieza_estacion', [
            'id'                            => $this->primaryKey(), 
            'cantidad'                      => $this->integer(), 
            'id_solicitud_compra_estacion'  => $this->integer(),            
            'id_pieza'                      => $this->integer(),
            'descipcion'                    => $this->string(1000),
            'prioridad'                     => $this->integer(),
            'observacion'                   => $this->string(1000),
        ]);


        $this->createIndex('idx-solicitud-compra-estacion-solicitud-pieza-estacion','solicitud_pieza_estacion','id_solicitud_compra_estacion');
        $this->addForeignKey('fk-solicitud-compra-estacion-solicitud-pieza-estacion', 'solicitud_pieza_estacion', 'id_solicitud_compra_estacion', 'solicitud_compra_estacion', 'id', 'CASCADE');
    
        $this->createIndex('idx-pieza-solicitud-pieza-estacion','solicitud_pieza_estacion','id_pieza');
        $this->addForeignKey('fk-pieza-solicitud-pieza-estacion', 'solicitud_pieza_estacion', 'id_pieza', 'pieza', 'id_pieza', 'CASCADE');
        
    }

    public function safeDown()
    {
        echo "m180215_154232_create_solicitud_pieza_estacion cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180215_154232_create_solicitud_pieza_estacion cannot be reverted.\n";

        return false;
    }
    */
}
