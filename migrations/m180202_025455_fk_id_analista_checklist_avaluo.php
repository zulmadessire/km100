<?php

use yii\db\Migration;

class m180202_025455_fk_id_analista_checklist_avaluo extends Migration
{
    public function safeUp()
    {
        $this->dropForeignKey(
            'fk-taller_checklist_avaluo',
            'checklist_avaluo'
        );

        $this->dropIndex(
            'idx-taller_checklist_avaluo',
            'checklist_avaluo'
        );
    }

    public function safeDown()
    {
        echo "m180202_025455_fk_id_analista_checklist_avaluo cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180202_025455_fk_id_analista_checklist_avaluo cannot be reverted.\n";

        return false;
    }
    */
}
