<?php

use yii\db\Migration;

class m171006_090625_add_cotizaciones_recibidas extends Migration
{
    public function safeUp()
    {
        $this->addColumn('solicitud_compra_pieza', 'observacion', $this->text());
        $this->addColumn('solicitud_compra_pieza', 'aprobacion', $this->text());
        $this->addColumn('cotizacion_proveedor_pieza', 'cantidad_comprar', $this->integer());
        $this->addColumn('cotizacion_proveedor_pieza', 'aprobacion', $this->integer());
    }

    public function safeDown()
    {
        echo "m171006_090625_add_cotizaciones_recibidas cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171006_090625_add_cotizaciones_recibidas cannot be reverted.\n";

        return false;
    }
    */
}
