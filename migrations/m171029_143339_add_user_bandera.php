<?php

use yii\db\Migration;

class m171029_143339_add_user_bandera extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user', 'bandera', $this->integer());
         $this->addColumn('auditoria', 'observacion', $this->integer());
    }

    public function safeDown()
    {
        echo "m171029_143339_add_user_bandera cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171029_143339_add_user_bandera cannot be reverted.\n";

        return false;
    }
    */
}
