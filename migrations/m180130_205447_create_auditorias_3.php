<?php

use yii\db\Migration;

class m180130_205447_create_auditorias_3 extends Migration
{
    public function safeUp()
    {
         //auditoria ejecucion de mantenimiento

        $this->createTable('auditoria_ejec_mant', [
            'id' => $this->primaryKey(),
            'tecnico' => $this->string(1000),
            'mva_vehiculo' => $this->string(100),
            'id_tipo_mnto' => $this->integer(),
            'estado' => $this->integer(),
            'fecha' => $this->date(),
            'id_estacion' => $this->integer()->notNull(),

            ]);

        $this->addForeignKey('fk-estacion_auditoria_ejec_mant', 'auditoria_ejec_mant', 'id_estacion', 'estacion', 'id_estacion', 'CASCADE');
        
         $this->createTable('aspectos', [
            'id' => $this->primaryKey(),
            'descripcion' => $this->string(1000),     
            ]);

        $this->createTable('aspectos_evaluar', [
            'id' => $this->primaryKey(),
            'respuesta' => $this->integer(),
            'id_aspecto' => $this->integer()->notNull(),
            'id_auditoria_ejec_mant' => $this->integer()->notNull(),

            ]);
        $this->addForeignKey('fk-id_aspecto_aspectos_evaluar', 'aspectos_evaluar', 'id_aspecto', 'aspectos', 'id', 'CASCADE');
        $this->addForeignKey('fk-id_auditoria_ejec_mant_aspectos_evaluar', 'aspectos_evaluar', 'id_auditoria_ejec_mant', 'auditoria_ejec_mant', 'id', 'CASCADE');
            


      
        
    }

    public function safeDown()
    {
        echo "m180130_205447_create_auditorias_3 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180130_205447_create_auditorias_3 cannot be reverted.\n";

        return false;
    }
    */
}
