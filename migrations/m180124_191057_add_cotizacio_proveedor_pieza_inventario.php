<?php

use yii\db\Migration;

class m180124_191057_add_cotizacio_proveedor_pieza_inventario extends Migration
{
    public function safeUp()
    {
     $this->addColumn('cotizacion_proveedor_pieza', 'inventario',  $this->integer());
        $this->addCommentOnColumn ('cotizacion_proveedor_pieza', 'inventario', 'indica si paso la pieza a inventario');
    }

    public function safeDown()
    {
        echo "m180124_191057_add_cotizacio_proveedor_pieza_inventario cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180124_191057_add_cotizacio_proveedor_pieza_inventario cannot be reverted.\n";

        return false;
    }
    */
}
