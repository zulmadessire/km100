<?php

use yii\db\Migration;

class m170915_171739_insert_estacion extends Migration
{
    public function safeUp()
    {
            $this->insert('estacion',[
            'id_estacion' => '2505',
            'codigo' => 'LRM',
            'nombre' => 'La Romana Intl Airport',
            'id_empresa' => '1',
        ]);
             $this->insert('estacion',[
            'id_estacion' => '2917',
            'codigo' => 'RD5',
            'nombre' => 'Casa De Campo',
            'id_empresa' => '1',
        ]);
            $this->insert('estacion',[
            'id_estacion' => '31220',
            'codigo' => 'LR2',
            'nombre' => 'Central Romana Port',
            'id_empresa' => '1',
        ]);
            $this->insert('estacion',[
            'id_estacion' => '6809',
            'codigo' => 'R1D',
            'nombre' => 'La Romana Downtown',
            'id_empresa' => '1',
        ]);
            $this->insert('estacion',[
            'id_estacion' => '31819',
            'codigo' => 'U1Y',
            'nombre' => 'Multiplaza La Sirena',
            'id_empresa' => '1',
        ]);
            $this->insert('estacion',[
            'id_estacion' => '3356',
            'codigo' => 'POP',
            'nombre' => 'La Union Intl Airport',
            'id_empresa' => '1',
        ]);
            $this->insert('estacion',[
            'id_estacion' => '3232',
            'codigo' => 'P9P',
            'nombre' => 'Carretera Luperon km 4 1/2',
            'id_empresa' => '1',
        ]);
            $this->insert('estacion',[
            'id_estacion' => '30164',
            'codigo' => 'D3M',
            'nombre' => 'Super Pola Mall',
            'id_empresa' => '1',
        ]);
            $this->insert('estacion',[
            'id_estacion' => '5424',
            'codigo' => 'PUJ',
            'nombre' => 'Punta Cana Intl Airport',
            'id_empresa' => '1',
        ]);
            $this->insert('estacion',[
            'id_estacion' => '6118',
            'codigo' => 'PU3',
            'nombre' => 'Bavaro Car Rental Center',
            'id_empresa' => '1',
        ]);
            $this->insert('estacion',[
            'id_estacion' => '1641',
            'codigo' => 'STI',
            'nombre' => 'Santiago Airport',
            'id_empresa' => '1',
        ]);
            $this->insert('estacion',[
            'id_estacion' => '3350',
            'codigo' => 'SG9',
            'nombre' => 'Antigua Carretera Duarte KM2',
            'id_empresa' => '1',
        ]);
            $this->insert('estacion',[
            'id_estacion' => '3903',
            'codigo' => 'SDQ',
            'nombre' => 'Las Americas Intl Airport',
            'id_empresa' => '1',
        ]);
            $this->insert('estacion',[
            'id_estacion' => '31189',
            'codigo' => 'X3D',
            'nombre' => 'Galerias 360 Mall',
            'id_empresa' => '1',
        ]);
            $this->insert('estacion',[
            'id_estacion' => '30933',
            'codigo' => 'N6M',
            'nombre' => 'Novocentro Mall',
            'id_empresa' => '1',
        ]);
            $this->insert('estacion',[
            'id_estacion' => '3904',
            'codigo' => 'SD5',
            'nombre' => 'Main Office',
            'id_empresa' => '1',
        ]);
            $this->insert('estacion',[
            'id_estacion' => '30225',
            'codigo' => 'JBQ',
            'nombre' => 'Aeropuerto La Isabela',
            'id_empresa' => '1',
        ]);
            $this->insert('estacion',[
            'id_estacion' => '31223',
            'codigo' => 'S5D',
            'nombre' => 'Puerto Don Diego',
            'id_empresa' => '1',
        ]);
            $this->insert('estacion',[
            'id_estacion' => '31993',
            'codigo' => 'U6C',
            'nombre' => 'Downtown Mall',
            'id_empresa' => '1',
        ]);
            $this->insert('estacion',[
            'id_estacion' => '31889',
            'codigo' => 'Y2F',
            'nombre' => 'Las Terrenas',
            'id_empresa' => '1',
        ]);
            $this->insert('estacion',[
            'id_estacion' => '1714',
            'codigo' => 'LRM',
            'nombre' => 'La Romana Intl Airport',
            'id_empresa' => '2',
        ]);
            $this->insert('estacion',[
            'id_estacion' => '3251',
            'codigo' => 'POP',
            'nombre' => 'La Union Intl Airport',
            'id_empresa' => '2',
        ]);
            $this->insert('estacion',[
            'id_estacion' => '30716',
            'codigo' => 'P1P',
            'nombre' => 'Playa Dorada Plaza',
            'id_empresa' => '2',
        ]);
            $this->insert('estacion',[
            'id_estacion' => '539',
            'codigo' => 'PUJ',
            'nombre' => 'Punta Cana Intl Airport',
            'id_empresa' => '2',
        ]);
            $this->insert('estacion',[
            'id_estacion' => '30902',
            'codigo' => 'PU5',
            'nombre' => 'Bavaro Car Rental Center',
            'id_empresa' => '2',
        ]);
            $this->insert('estacion',[
            'id_estacion' => '6376',
            'codigo' => 'STI',
            'nombre' => 'Santiago Airport',
            'id_empresa' => '2',
        ]);
             $this->insert('estacion',[
            'id_estacion' => '3529',
            'codigo' => 'SDQ',
            'nombre' => 'Las Americas Intl Airport',
            'id_empresa' => '2',
        ]);
            $this->insert('estacion',[
            'id_estacion' => '4692',
            'codigo' => 'SB5',
            'nombre' => 'Main Office',
            'id_empresa' => '2',
        ]);
            $this->insert('estacion',[
            'id_estacion' => '31746',
            'codigo' => 'ID5',
            'nombre' => 'Independencia',
            'id_empresa' => '2',
        ]);
            $this->insert('estacion',[
            'id_estacion' => '30034',
            'codigo' => 'SB6',
            'nombre' => 'Sambil',
            'id_empresa' => '2',
        ]);
    }

    public function safeDown()
    {
        echo "m170915_171739_insert_estacion cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170915_171739_insert_estacion cannot be reverted.\n";

        return false;
    }
    */
}
