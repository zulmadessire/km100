<?php

use yii\db\Migration;

class m170715_023236_create_extraccion_wizard extends Migration
{
    public function safeUp()
    {
        $this->createTable('extraccion_wizard', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(50)->notNull(),
            'posicion' => $this->integer()->notNull(),
            'pos_ini' => $this->integer()->notNull(),
            'pos_fin' => $this->integer()->notNull(),
        ]);
    }

    public function safeDown()
    {
        echo "m170715_023236_create_extraccion_wizard cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170715_023236_create_extraccion_wizard cannot be reverted.\n";

        return false;
    }
    */
}
