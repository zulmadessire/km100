<?php

use yii\db\Migration;

class m180212_153325_add_auditoria_ejecucion extends Migration
{
    public function safeUp()
    {
        $this->addColumn('auditoria_ejec_mant', 'acuerdo', $this->string(1000));
        $this->addColumn('auditoria_ejec_mant', 'compromiso', $this->string(1000));
    }

    public function safeDown()
    {
        echo "m180212_153325_add_auditoria_ejecucion cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180212_153325_add_auditoria_ejecucion cannot be reverted.\n";

        return false;
    }
    */
}
