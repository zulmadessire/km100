<?php

use yii\db\Migration;

class m180112_132646_add_checklist_avaluo_analista_solicitud extends Migration
{
    public function safeUp()
    {
        $this->addColumn('checklist_avaluo', 'id_servicio',  $this->integer());
        $this->addColumn('checklist_avaluo', 'id_analista',  $this->integer());

        $this->createIndex('idx-servicio_checklist_avaluo','checklist_avaluo','id_servicio');
        $this->addForeignKey('fk-servicio_checklist_avaluo', 'checklist_avaluo', 'id_servicio', 'servicio', 'id', 'CASCADE');

        $this->createIndex('idx-taller_checklist_avaluo','checklist_avaluo','id_analista');
        $this->addForeignKey('fk-taller_checklist_avaluo', 'checklist_avaluo', 'id_analista', 'analista_mantenimiento', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m180112_132646_add_checklist_avaluo_analista_solicitud cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180112_132646_add_checklist_avaluo_analista_solicitud cannot be reverted.\n";

        return false;
    }
    */
}
