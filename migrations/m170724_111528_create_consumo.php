<?php

use yii\db\Migration;

class m170724_111528_create_consumo extends Migration
{
    public function safeUp()
    {
        $this->createTable('consumo', [
            'id' => $this->primaryKey(),
            'cantidad' => $this->integer()->notNull()->defaultValue(0),
            'id_actividad_pieza' => $this->integer()->notNull(),
            'id_mantenimiento_preventivo' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-consumo_actividad_pieza','consumo','id_actividad_pieza');
        $this->addForeignKey('fk-consumo_actividad_pieza', 'consumo', 'id_actividad_pieza', 'actividad_pieza', 'id_activ_pieza', 'CASCADE');

        $this->createIndex('idx-consumo_mantenimiento_preventivo','consumo','id_mantenimiento_preventivo');
        $this->addForeignKey('fk-consumo_mantenimiento_preventivo', 'consumo', 'id_mantenimiento_preventivo', 'mantenimiento_preventivo', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m170724_111528_create_consumo cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170724_111528_create_consumo cannot be reverted.\n";

        return false;
    }
    */
}
