<?php

use yii\db\Migration;

class m180110_132021_add_mantenimiento_preventivo_concesionario extends Migration
{
    public function safeUp()
    {
        $this->addColumn('mantenimiento_preventivo', 'id_concesionario',  $this->integer());
        $this->addForeignKey('fk-mantenimiento_preventivo_concesionario', 'mantenimiento_preventivo', 'id_concesionario', 'concesionario', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m180110_132021_add_mantenimiento_preventivo_concesionario cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180110_132021_add_mantenimiento_preventivo_concesionario cannot be reverted.\n";

        return false;
    }
    */
}
