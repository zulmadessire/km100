<?php

use yii\db\Migration;

class m170803_014655_insert_marca_insumo extends Migration
{
    public function safeUp()
    {
        $this->insert('marca_insumo',[
            'id' => '1',
            'nombre' =>'Shell',
            'tipo_pieza_id'=>'1'
        ]);
        $this->insert('marca_insumo',[
            'id' => '2',
            'nombre' =>'Venoco',
            'tipo_pieza_id'=>'1'
        ]);
    }

    public function safeDown()
    {
        echo "m170803_014655_insert_marca_insumo cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170803_014655_insert_marca_insumo cannot be reverted.\n";

        return false;
    }
    */
}
