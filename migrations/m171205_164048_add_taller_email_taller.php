<?php

use yii\db\Migration;

class m171205_164048_add_taller_email_taller extends Migration
{
    public function safeUp()
    {
        $this->addColumn('taller', 'email_taller',  $this->string(100)); 
    }

    public function safeDown()
    {
        echo "m171205_164048_add_taller_email_taller cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171205_164048_add_taller_email_taller cannot be reverted.\n";

        return false;
    }
    */
}
