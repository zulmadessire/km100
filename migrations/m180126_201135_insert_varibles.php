<?php

use yii\db\Migration;

class m180126_201135_insert_varibles extends Migration
{
    public function safeUp()
    {
        $this->insert('variables',[
            'label' => 'Monto límite mantenimiento',
            'descripcion' => 'Monto límite de mantenimiento',
            'value' => '2000',
            'key' => 'mlm',
        ]);

        $this->insert('variables',[
            'label' => 'Monto límite pieza',
            'descripcion' => 'Monto límite compra pieza',
            'value' => '2000',
            'key' => 'mlp',
        ]);
    }

    public function safeDown()
    {
        echo "m180126_201135_insert_varibles cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180126_201135_insert_varibles cannot be reverted.\n";

        return false;
    }
    */
}
