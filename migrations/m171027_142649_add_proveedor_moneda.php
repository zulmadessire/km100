<?php

use yii\db\Migration;

class m171027_142649_add_proveedor_moneda extends Migration
{
    public function safeUp()
    {
        $this->addColumn('proveedor', 'moneda', $this->integer());
        $this->addCommentOnColumn ('proveedor', 'moneda', '0-RD, 1-USD');
    }

    public function safeDown()
    {
        echo "m171027_142649_add_proveedor_moneda cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171027_142649_add_proveedor_moneda cannot be reverted.\n";

        return false;
    }
    */
}
