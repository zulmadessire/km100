<?php

use yii\db\Migration;

class m170803_141834_insert_pieza extends Migration
{
    public function safeUp()
    {
        $this->insert('pieza',[
            'nombre' =>'Liga',
            'unidad' =>'unidad',
            'tipo_pieza_id'=>'1'
        ]);
        $this->insert('pieza',[
            'nombre' =>'Amortiguador Derecho',
            'unidad' =>'unidad',
            'tipo_pieza_id'=>'4'
        ]);
        $this->insert('pieza',[
            'nombre' =>'Alternador del Motor',
            'unidad' =>'unidad',
            'tipo_pieza_id'=>'4'
        ]);
        $this->insert('pieza',[
            'nombre' =>'Estopera',
            'unidad' =>'unidad',
            'tipo_pieza_id'=>'4'
        ]);
        $this->insert('pieza',[
            'nombre' =>'Amortiguador Izquierdo',
            'unidad' =>'unidad',
            'tipo_pieza_id'=>'4'
        ]);
        $this->insert('pieza',[
            'nombre' =>'Espejo Retrovisor',
            'unidad' =>'unidad',
            'tipo_pieza_id'=>'5'
        ]);
        $this->insert('pieza',[
            'nombre' =>'Guarda Barros',
            'unidad' =>'unidad',
            'tipo_pieza_id'=>'5'
        ]);
        $this->insert('pieza',[
            'nombre' =>'Limpia Parabrisas',
            'unidad' =>'unidad',
            'tipo_pieza_id'=>'5'
        ]);
        $this->insert('pieza',[
            'nombre' =>'Bocina',
            'unidad' =>'unidad',
            'tipo_pieza_id'=>'1'
        ]);
        $this->insert('pieza',[
            'nombre' =>'Aceite',
            'unidad' =>'unidad',
            'tipo_pieza_id'=>'1'
        ]);
        $this->insert('pieza',[
            'nombre' =>'Filtro',
            'unidad' =>'unidad',
            'tipo_pieza_id'=>'1'
        ]);
        $this->insert('pieza',[
            'nombre' =>'Bujia',
            'unidad' =>'unidad',
            'tipo_pieza_id'=>'4'
        ]);
        $this->insert('pieza',[
            'nombre' =>'Correa',
            'unidad' =>'unidad',
            'tipo_pieza_id'=>'4'
        ]);
        $this->insert('pieza',[
            'nombre' =>'Manocrrea',
            'unidad' =>'unidad',
            'tipo_pieza_id'=>'4'
        ]);
        $this->insert('pieza',[
            'nombre' =>'Escobilla',
            'unidad' =>'unidad',
            'tipo_pieza_id'=>'1'
        ]);
        $this->insert('pieza',[
            'nombre' =>'Bombillo',
            'unidad' =>'unidad',
            'tipo_pieza_id'=>'1'
        ]);
        $this->insert('pieza',[
            'nombre' =>'Banda de frenos',
            'unidad' =>'unidad',
            'tipo_pieza_id'=>'1'
        ]);
        $this->insert('pieza',[
            'nombre' =>'Bobina',
            'unidad' =>'unidad',
            'tipo_pieza_id'=>'4'
        ]);
    }

    public function safeDown()
    {
        echo "m170803_141834_insert_pieza cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170803_141834_insert_pieza cannot be reverted.\n";

        return false;
    }
    */
}
