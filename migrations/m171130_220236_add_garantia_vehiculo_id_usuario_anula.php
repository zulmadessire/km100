<?php

use yii\db\Migration;

class m171130_220236_add_garantia_vehiculo_id_usuario_anula extends Migration
{
    public function safeUp()
    {
        $this->addcolumn('garantia_vehiculo', 'id_usuario_anula', $this->integer());
    }

    public function safeDown()
    {
        echo "m171130_220236_add_garantia_vehiculo_id_usuario_anula cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171130_220236_add_garantia_vehiculo_id_usuario_anula cannot be reverted.\n";

        return false;
    }
    */
}
