<?php

use yii\db\Migration;

class m170802_205603_create_detalle_garantia extends Migration
{
    public function safeUp()
    {
        $this->createTable('detalle_garantia', [
            'id' => $this->primaryKey(),
            'modelo_id_modelo' => $this->integer()->notNull(),
            'sucursal_concesionario_id' => $this->integer()->notNull(),
        ]);

        
        $this->addForeignKey('fk-detalle-garantia-modelo-id-modelo', 'detalle_garantia', 'modelo_id_modelo', 'modelo', 'id_modelo', 'CASCADE');
        $this->addForeignKey('fk-detalle-garantia-sucursal-concesionario-id', 'detalle_garantia', 'sucursal_concesionario_id', 'sucursal_concesionario', 'id', 'CASCADE');
    }
    public function safeDown()
    {
        echo "m170802_205603_create_detalle_garantia cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170802_205603_create_detalle_garantia cannot be reverted.\n";

        return false;
    }
    */
}
