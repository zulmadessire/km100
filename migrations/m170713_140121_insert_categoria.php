<?php

use yii\db\Migration;

class m170713_140121_insert_categoria extends Migration
{
    public function safeUp()
    {
        $this->insert('categoria',[
            'nombre' => 'Carro',
            'seguimiento_km' => '70000',
            'declive_km' => '80000',
            'seguimiento_fecha' => '5',
            'declive_fecha' => '5',
        ]);

        $this->insert('categoria',[
            'nombre' => 'Camión',
            'seguimiento_km' => '180000',
            'declive_km' => '200000',
            'seguimiento_fecha' => '5',
            'declive_fecha' => '5',
        ]);

        $this->insert('categoria',[
            'nombre' => 'Camioneta',
            'seguimiento_km' => '180000',
            'declive_km' => '200000',
            'seguimiento_fecha' => '5',
            'declive_fecha' => '5',
        ]);

        $this->insert('categoria',[
            'nombre' => 'Furgoneta',
            'seguimiento_km' => '180000',
            'declive_km' => '200000',
            'seguimiento_fecha' => '5',
            'declive_fecha' => '5',
        ]);

        $this->insert('categoria',[
            'nombre' => 'Suv',
            'seguimiento_km' => '180000',
            'declive_km' => '200000',
            'seguimiento_fecha' => '5',
            'declive_fecha' => '5',
        ]);

        $this->insert('categoria',[
            'nombre' => 'Van',
            'seguimiento_km' => '180000',
            'declive_km' => '200000',
            'seguimiento_fecha' => '5',
            'declive_fecha' => '5',
        ]);
    }

    public function safeDown()
    {
        echo "m170713_140121_insert_categoria cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170713_140121_insert_categoria cannot be reverted.\n";

        return false;
    }
    */
}
