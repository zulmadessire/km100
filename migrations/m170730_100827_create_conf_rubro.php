<?php

use yii\db\Migration;

class m170730_100827_create_conf_rubro extends Migration
{
  public function safeUp()
  {
        $this->createTable('rubro', [
            'id_rubro' => $this->primaryKey(),
            'nombre' => $this->string(50)->notNull(),
            'tamanio' => $this->integer(),
            'unidad_medicion' => $this->string(20)->notNull(),
        ]);
        $this->addCommentOnColumn ('rubro', 'unidad_medicion', '0=Unidades, 1=Litros');

        $this->createTable('marca_rubro', [
            'id_marca_rubro' => $this->primaryKey(),
            'nombre' => $this->string(50)->notNull(),
        ]);

        $this->createTable('modelo_rubro', [
            'id_modelo_rubro' => $this->primaryKey(),
            'nombre' => $this->string(50)->notNull(),
            'id_marca_rubro' => $this->integer()->notNull(),
        ]);
        $this->createIndex('idx-marcarubro-modelorubro','modelo_rubro','id_marca_rubro');
        $this->addForeignKey('fk-marcarubro-modelorubro', 'modelo_rubro', 'id_marca_rubro', 'marca_rubro', 'id_marca_rubro', 'CASCADE');

        $this->createTable('rubro_modelo_asignado', [
            'id_rubro_modelo_asignado' => $this->primaryKey(),
            'id_rubro' => $this->integer()->notNull(),
            'id_modelo_rubro' => $this->integer()->notNull(),
        ]);
        $this->createIndex('idx-modelorubro-rubromodeloasignado','rubro_modelo_asignado','id_modelo_rubro');
        $this->addForeignKey('fk-modelorubro-rubromodeloasignado', 'rubro_modelo_asignado', 'id_modelo_rubro', 'modelo_rubro', 'id_modelo_rubro', 'CASCADE');
        $this->createIndex('idx-rubro-rubromodeloasignado','rubro_modelo_asignado','id_rubro');
        $this->addForeignKey('fk-rubro-rubromodeloasignado', 'rubro_modelo_asignado', 'id_rubro', 'rubro', 'id_rubro', 'CASCADE');
  }

    public function safeDown()
    {
        echo "m170730_100827_create_conf_rubro cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170730_100827_create_conf_rubro cannot be reverted.\n";

        return false;
    }
    */
}
