<?php

use yii\db\Migration;

class m180126_021105_add_despacho_proveedor_pieza_recibido extends Migration
{
    public function safeUp()
    {
     $this->addColumn('despacho_proveedor_pieza', 'recibido',  $this->integer());

    }

    public function safeDown()
    {
        echo "m180126_021105_add_despacho_proveedor_pieza_recibido cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180126_021105_add_despacho_proveedor_pieza_recibido cannot be reverted.\n";

        return false;
    }
    */
}
