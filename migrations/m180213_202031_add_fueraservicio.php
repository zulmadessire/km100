<?php

use yii\db\Migration;

class m180213_202031_add_fueraservicio extends Migration
{
    public function safeUp()
    {
        $this->addColumn('auditoria_fs', 'veh_fuera_serv_reg', $this->integer());

    }

    public function safeDown()
    {
        echo "m180213_202031_add_fueraservicio cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180213_202031_add_fueraservicio cannot be reverted.\n";

        return false;
    }
    */
}
