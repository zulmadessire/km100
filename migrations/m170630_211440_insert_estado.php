<?php

use yii\db\Migration;

class m170630_211440_insert_estado extends Migration
{
    public function up()
    {
        $this->insert('estado',[
            'nombre' => 'Operativo',
            'id_tipo_estado' => '1',
            'color' => '#70ad47'
        ]);

        $this->insert('estado',[
            'nombre' => 'Salvamento',
            'id_tipo_estado' => '1',
            'color' => '#d9d9d9'
        ]);

        $this->insert('estado',[
            'nombre' => 'Remarketing',
            'id_tipo_estado' => '1',
            'color' => '#ed7d31'
        ]);

        $this->insert('estado',[
            'nombre' => 'Taller',
            'id_tipo_estado' => '1',
            'color' => '#0070c0'
        ]);

        $this->insert('estado',[
            'nombre' => 'Estación',
            'id_tipo_estado' => '2',
            'color' => '#8faadc'
        ]);

        $this->insert('estado',[
            'nombre' => 'Taller',
            'id_tipo_estado' => '2',
            'color' => '#8497b0'
        ]);

        $this->insert('estado',[
            'nombre' => 'Registro',
            'id_tipo_estado' => '3',
            'color' => '#ffe699'
        ]);

        $this->insert('estado',[
            'nombre' => 'Vencido',
            'id_tipo_estado' => '4',
            'color' => '#c00000'
        ]);

        $this->insert('estado',[
            'nombre' => 'Asignar',
            'id_tipo_estado' => '4',
            'color' => '#ed7d31'
        ]);

        $this->insert('estado',[
            'nombre' => 'Vigente',
            'id_tipo_estado' => '4',
            'color' => '#70ad47'
        ]);

        $this->insert('estado',[
            'nombre' => 'Estación',
            'id_tipo_estado' => '5',
            'color' => '#8faadc'
        ]);

        $this->insert('estado',[
            'nombre' => 'Taller',
            'id_tipo_estado' => '5',
            'color' => '#8497b0'
        ]);
    }

    public function down()
    {
        echo "m170630_211440_insert_estado cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
