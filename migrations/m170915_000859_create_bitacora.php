<?php

use yii\db\Migration;

class m170915_000859_create_bitacora extends Migration
{
    public function safeUp()
    {
         $this->createTable('bitacora', [
            'id' => $this->primaryKey(),
            'color' => $this->string(45),
            'transmision' => $this->string(45),
            'chasis' => $this->string(45),
            'placa' => $this->string(45),
            'mva' => $this->string(45),
            'categoria' => $this->string(45),
            'clase' => $this->string(45),
            'combustible' => $this->string(45),
            'fecha_compra' => $this->date(),
            'fecha_inicial' => $this->date(),
            'fecha_actual' => $this->date(),
            'tiempo_vida' => $this->integer(),
            'km' => $this->string(45),
            'fecha_renovacion_placa' => $this->date(),
            'fecha_emision_placa' => $this->date(),
            'wizard' => $this->boolean(),
            'exactus' => $this->boolean(),
            'tsd' => $this->boolean(),
            'id_modelo' => $this->integer(),
            'anho' => $this->integer(),
            'id_concesionario' => $this->integer(),
            'id_categoria' => $this->integer(),
        ]);
    }

    public function safeDown()
    {
        echo "m170915_000859_create_bitacora cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170915_000859_create_bitacora cannot be reverted.\n";

        return false;
    }
    */
}
