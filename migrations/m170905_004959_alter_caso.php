<?php

use yii\db\Migration;

class m170905_004959_alter_caso extends Migration
{
    public function safeUp()
    {
      $this->addColumn( 'caso', 'piezas', 'character varying(500)' );
      $this->addCommentOnColumn ('caso', 'piezas', 'Campo temporal para la primera versión en producción del Sistema Km100');
    }

    public function safeDown()
    {
        echo "m170905_004959_alter_caso cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170905_004959_alter_caso cannot be reverted.\n";

        return false;
    }
    */
}
