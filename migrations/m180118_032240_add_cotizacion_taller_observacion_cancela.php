<?php

use yii\db\Migration;

class m180118_032240_add_cotizacion_taller_observacion_cancela extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cotizacion_taller', 'observacion_cancela',  $this->text()); 
    }

    public function safeDown()
    {
        echo "m180118_032240_add_cotizacion_taller_observacion_cancela cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180118_032240_add_cotizacion_taller_observacion_cancela cannot be reverted.\n";

        return false;
    }
    */
}
