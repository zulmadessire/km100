<?php

use yii\db\Migration;

class m171130_121541_create_estacion_usuario extends Migration
{
    public function safeUp()
    {
         $this->createTable('estacion_usuario', [
            'id' => $this->primaryKey(),
            'estacion_id' => $this->integer(),
            'user_id' => $this->integer(),
            ]);
        $this->addForeignKey('fk-estacion_usuario_estacion_id', 'estacion_usuario', 'estacion_id', 'estacion', 'id_estacion', 'CASCADE');
        $this->addForeignKey('fk-estacion_usuario_user_id', 'estacion_usuario', 'user_id', 'user', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m171130_121541_create_estacion_usuario cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171130_121541_create_estacion_usuario cannot be reverted.\n";

        return false;
    }
    */
}
