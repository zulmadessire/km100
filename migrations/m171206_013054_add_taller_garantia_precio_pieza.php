<?php

use yii\db\Migration;

class m171206_013054_add_taller_garantia_precio_pieza extends Migration
{
    public function safeUp()
    {
        $this->addColumn('taller', 'precio_pieza',  $this->double()); 
        $this->addColumn('taller', 'garantia_global',  $this->integer()); 
    }

    public function safeDown()
    {
        echo "m171206_013054_add_taller_garantia_precio_pieza cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171206_013054_add_taller_garantia_precio_pieza cannot be reverted.\n";

        return false;
    }
    */
}
