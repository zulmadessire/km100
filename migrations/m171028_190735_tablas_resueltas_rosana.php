<?php

use yii\db\Migration;

class m171028_190735_tablas_resueltas_rosana extends Migration
{
    public function safeUp()
    {


        $this ->dropTable( 'rubro_modelo_asignado' );
        $this ->dropTable( 'modelo_rubro' );
        $this ->dropTable( 'marca_rubro' );
        $this ->dropTable( 'rubro' );
        $this ->dropTable( 'modelo_pieza' );


 
     


        $this->addColumn('tipo_mantenimiento', 'estado',  $this->integer());
       $this->addColumn('pieza', 'marca_insumo', $this->string(100));
       $this->addColumn('pieza', 'marca', $this->string(100));
       $this->addColumn('pieza', 'codigo', $this->string(100));
       $this->addColumn('pieza', 'anho', $this->string(50));
       $this->addColumn('pieza', 'modelo_id_modelo',$this->integer());



        $this->addForeignKey('fk-modelo_pieza_id_modelo_pieza', 'pieza', 'modelo_id_modelo', 'modelo', 'id_modelo', 'CASCADE');
 
    

    }

    public function safeDown()
    {
        echo "m171028_190735_tablas_resueltas_rosana cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171028_190735_tablas_resueltas_rosana cannot be reverted.\n";

        return false;
    }
    */
}
