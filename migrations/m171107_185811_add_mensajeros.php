<?php

use yii\db\Migration;

class m171107_185811_add_mensajeros extends Migration
{
    public function safeUp()
    {
        $this->addColumn('mensajero', 'user', $this->string(200));
        $this->addColumn('mensajero', 'correo', $this->string(200));

    }

    public function safeDown()
    {
        echo "m171107_185811_add_mensajero cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171107_185811_add_mensajero cannot be reverted.\n";

        return false;
    }
    */
}
