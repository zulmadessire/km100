<?php

use yii\db\Migration;

class m180123_001152_add_pieza_serial extends Migration
{
    public function safeUp()
    {
        $this->addColumn('pieza', 'serial', $this->string(50));
    }

    public function safeDown()
    {
        echo "m180123_001152_add_pieza_serial cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180123_001152_add_pieza_serial cannot be reverted.\n";

        return false;
    }
    */
}
