<?php

use yii\db\Migration;

class m171115_185850_add_user_seguridad extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user', 'pregunta',  $this->string(400));  
        $this->addColumn('user', 'respuesta',  $this->string(400));  
    }

    public function safeDown()
    {
        echo "m171115_185850_add_user_seguridad cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171115_185850_add_user_seguridad cannot be reverted.\n";

        return false;
    }
    */
}
