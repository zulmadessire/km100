<?php

use yii\db\Migration;

class m180118_172752_add_cotizacion_taller_status_cotizacion extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cotizacion_taller', 'status_cotizacion',  $this->integer());
    }

    public function safeDown()
    {
        echo "m180118_172752_add_cotizacion_taller_status_cotizacion cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180118_172752_add_cotizacion_taller_status_cotizacion cannot be reverted.\n";

        return false;
    }
    */
}
