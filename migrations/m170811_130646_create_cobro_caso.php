<?php

use yii\db\Migration;

class m170811_130646_create_cobro_caso extends Migration
{
    public function safeUp()
    {
        $this->createTable('cobro_caso', [
            'id' => $this->primaryKey(),
            'monto' => $this->double()->notNull(),
            'forma_pago' => $this->integer(1)->notNull(),
            'motivo_cobro' => $this->integer()->notNull(),
            'fecha' => $this->date()->notNull(),
            'id_caso' => $this->integer()->notNull(),
        ]);

        $this->addCommentOnColumn ('cobro_caso', 'forma_pago', '0-Efectivo, 1-Tarjeta de crédito, 2-Crédito, 3-CLAIM, 4-Compañía privada de cobranza');

        $this->addCommentOnColumn ('cobro_caso', 'motivo_cobro', '0-No presentó acta policial, 1-Declinación de cobertura, 2-Deducible, 3-Daños con objetos fijos, 4-Deslizamiento, 5-Negligencia, 6-Daños maliciosos, 7-Imprudencia, 8-Motín, 9-Actividades políticas, 10-Manejo en terrenos rocosos, 11-Exceso de velocidad, 12-Vandalismo, 13-No fue cargado por daños');  

        $this->createIndex('idx-cobro_caso','cobro_caso','id_caso');
        $this->addForeignKey('fk-cobro_caso', 'cobro_caso', 'id_caso', 'caso', 'id_caso', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m170811_130646_create_cobro_caso cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170811_130646_create_cobro_caso cannot be reverted.\n";

        return false;
    }
    */
}
