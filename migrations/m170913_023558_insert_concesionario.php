<?php

use yii\db\Migration;

class m170913_023558_insert_concesionario extends Migration
{
    public function safeUp()
    {
        $this->insert('concesionario',[
            'nombre' => 'Magna Motors',
        ]);
            $this->insert('concesionario',[
            'nombre' => 'Grupo Viamar',
        ]);
           $this->insert('concesionario',[
            'nombre' => 'Agencia Bella',
        ]);
           $this->insert('concesionario',[
            'nombre' => 'Sto. Dgo. Motors',
        ]);
           $this->insert('concesionario',[
            'nombre' => 'Delta Comercial',
        ]);
           $this->insert('concesionario',[
            'nombre' => 'Avelino Abreu',
        ]);
        $this->insert('concesionario',[
            'nombre' => 'Bonanza Dominicana',
        ]);
        $this->insert('concesionario',[
            'nombre' => 'Euromotors',
        ]);
        $this->insert('concesionario',[
            'nombre' => 'Auto Ozama',
        ]);

    }

    public function safeDown()
    {
        echo "m170913_023558_insert_concesionario cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170913_023558_insert_concesionario cannot be reverted.\n";

        return false;
    }
    */
}
