<?php

use yii\db\Migration;

class m180315_213449_create_imagenes_fs_mas extends Migration
{
    public function safeUp()
    {
$this->createTable('imagenes_fs', [
             'id' => $this->primaryKey(),
            'descripcion' => $this->string(500),
            'anexo' => $this->string(1000),
            'extension' => $this->string(1000),
            'base_url' => $this->string(1000),
            'imageFile' => $this->string(1000),
            'id_auditoriafs' => $this->integer()->notNull(),
          

            ]);
        $this->addForeignKey('fk-id_auditoriafs_imagenes_fs', 'imagenes_fs', 'id_auditoriafs', 'auditoria_fs', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m180315_213449_create_imagenes_fs_mas cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180315_213449_create_imagenes_fs_mas cannot be reverted.\n";

        return false;
    }
    */
}
