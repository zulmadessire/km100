<?php

use yii\db\Migration;

class m170908_141700_add_vehiculo_flota extends Migration
{
    public function safeUp()
    {
        $this->addColumn('vehiculo', 'transito', $this->integer());
        $this->addColumn('vehiculo', 'anho', $this->integer());
        
        $this->addColumn('flota', 'kilometraje', $this->integer());
        $this->addColumn('flota', 'fecha', $this->date());
    }

    public function safeDown()
    {
        echo "m170908_141700_add_vehiculo_flota cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170908_141700_add_vehiculo_flota cannot be reverted.\n";

        return false;
    }
    */
}
