<?php

use yii\db\Migration;

class m171107_195041_add_user extends Migration
{
    public function safeUp()
    {

        $this->addColumn('user', 'id_mensajero', $this->integer());

    }

    public function safeDown()
    {
        echo "m171107_195041_add_mensajero cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171107_195041_add_mensajero cannot be reverted.\n";

        return false;
    }
    */
}
