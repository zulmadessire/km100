<?php

use yii\db\Migration;

class m170809_202413_create_servicio_vehiculos_escaneados extends Migration
{
    public function safeUp()
    {
        $this->createTable('servicio_vehiculo_escaneado', [
            'id' => $this->primaryKey(),
            'ficha' => $this->string(45)->notNull(),
            'modelo' => $this->string(45)->notNull(),
            'empresa' => $this->string(45)->notNull(),
            'tipo_solicitud' => $this->string(20)->notNull(),
            'id_servicio' => $this->integer()->notNull(),
        ]);
        $this->createIndex('idx-servicio_vehiculo_escaneado','servicio_vehiculo_escaneado','id_servicio');
        $this->addForeignKey('fk-servicio_vehiculo_escaneado', 'servicio_vehiculo_escaneado', 'id_servicio', 'servicio', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m170809_202413_create_servicio_vehiculos_escaneados cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170809_202413_create_servicio_vehiculos_escaneados cannot be reverted.\n";

        return false;
    }
    */
}
