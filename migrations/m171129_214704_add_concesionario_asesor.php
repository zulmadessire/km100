<?php

use yii\db\Migration;

class m171129_214704_add_concesionario_asesor extends Migration
{
    public function safeUp()
    {
        $this->addColumn('concesionario', 'nombre_asesor',  $this->string(200)); 
        $this->addColumn('concesionario', 'apellido',  $this->string(200)); 
        $this->addColumn('concesionario', 'tlf',  $this->string(40)); 
        $this->addColumn('concesionario', 'email',  $this->string(100)); 
    }

    public function safeDown()
    {
        echo "m171129_214704_add_concesionario_asesor cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171129_214704_add_concesionario_asesor cannot be reverted.\n";

        return false;
    }
    */
}
