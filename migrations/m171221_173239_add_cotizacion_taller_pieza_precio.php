<?php

use yii\db\Migration;

class m171221_173239_add_cotizacion_taller_pieza_precio extends Migration
{
    public function safeUp()
    {
            $this->addColumn('cotizacion_taller_pieza', 'precio', $this->double(5,3)); 
            $this->addColumn('cotizacion_taller_pieza', 'cotiza',  $this->integer()); 
    }

    public function safeDown()
    {
        echo "m171221_173239_add_cotizacion_taller_pieza_precio cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171221_173239_add_cotizacion_taller_pieza_precio cannot be reverted.\n";

        return false;
    }
    */
}
