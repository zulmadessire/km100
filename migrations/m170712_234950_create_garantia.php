<?php

use yii\db\Migration;

class m170712_234950_create_garantia extends Migration
{
    public function safeUp()
    {
        $this->createTable('garantia', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(45)->notNull(),
        ]);
    }

    public function safeDown()
    {
        echo "m170712_234950_create_garantia cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170712_234950_create_garantia cannot be reverted.\n";

        return false;
    }
    */
}
