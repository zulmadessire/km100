<?php

use yii\db\Migration;

class m170803_013449_insert_tipo_pieza extends Migration
{
    public function safeUp()
    {
        $this->insert('tipo_pieza',[
            'id' => '1',
            'nombre' =>'Insumo'
        ]);
        $this->insert('tipo_pieza',[
            'id' => '4',
            'nombre' =>'Mecánica'
        ]);
        $this->insert('tipo_pieza',[
            'id' => '5',
            'nombre' =>'Carrocería'
        ]);
    }

    public function safeDown()
    {
        echo "m170803_013449_insert_tipo_pieza cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170803_013449_insert_tipo_pieza cannot be reverted.\n";

        return false;
    }
    */
}
