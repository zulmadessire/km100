<?php

use yii\db\Migration;

class m180110_165944_add_mantenimiento_preventivo_observaciones_adicionales extends Migration
{
    public function safeUp()
    {
         $this->addColumn('mantenimiento_preventivo', 'observacion_adicional', $this->string(1000));
    }

    public function safeDown()
    {
        echo "m180110_165944_add_mantenimiento_preventivo_observaciones_adicionales cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180110_165944_add_mantenimiento_preventivo_observaciones_adicionales cannot be reverted.\n";

        return false;
    }
    */
}
