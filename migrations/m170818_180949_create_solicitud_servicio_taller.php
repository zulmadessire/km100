<?php

use yii\db\Migration;

class m170818_180949_create_solicitud_servicio_taller extends Migration
{
    public function safeUp()
    {
        $this->createTable('solicitud_servicio_taller', [
            'id' => $this->primaryKey(),
            'costo_promedio' => $this->double(),
            'disponibilidad' => $this->integer(),
            'cumplimiento' => $this->double(),
            'garantia' => $this->integer(1),
            'id_servicio' => $this->integer()->notNull(),
            'id_taller' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-solicitud_servicio_taller_servicio','solicitud_servicio_taller','id_servicio');
        $this->addForeignKey('fk-solicitud_servicio_taller_servicio', 'solicitud_servicio_taller', 'id_servicio', 'servicio', 'id', 'CASCADE');

        $this->createIndex('idx-solicitud_servicio_taller_taller','solicitud_servicio_taller','id_taller');
        $this->addForeignKey('fk-solicitud_servicio_taller_taller', 'solicitud_servicio_taller', 'id_taller', 'taller', 'id_taller', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m170818_180949_create_solicitud_servicio_taller cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170818_180949_create_solicitud_servicio_taller cannot be reverted.\n";

        return false;
    }
    */
}
