<?php

use yii\db\Migration;

class m171109_191253_add_estacion_talleres extends Migration
{
    public function safeUp()
    {
                $this->addColumn('estacion', 'asoc_taller',  $this->integer()->notNull()); // 1 es que si y 0 es que no
    }

    public function safeDown()
    {
        echo "m171109_191253_add_estacion_talleres cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171109_191253_add_estacion_talleres cannot be reverted.\n";

        return false;
    }
    */
}
