<?php

use yii\db\Migration;

/**
 * Class m180307_144310_add_observacion_pieza_imagen_pieza_cotizacion_proveedor_pieza
 */
class m180307_144310_add_observacion_pieza_imagen_pieza_cotizacion_proveedor_pieza extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('cotizacion_proveedor_pieza', 'imagen_pieza', $this->string(255));
        $this->addColumn('cotizacion_proveedor_pieza', 'observacion_pieza', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180307_144310_add_observacion_pieza_imagen_pieza_cotizacion_proveedor_pieza cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180307_144310_add_observacion_pieza_imagen_pieza_cotizacion_proveedor_pieza cannot be reverted.\n";

        return false;
    }
    */
}
