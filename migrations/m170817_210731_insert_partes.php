<?php

use yii\db\Migration;

class m170817_210731_insert_partes extends Migration
{
    public function safeUp()
    {
        $this->insert('partes',[
            'id' => '1',
            'nombre' =>'Vehículo'
        ]);
            $this->insert('partes',[
            'id' => '2',
            'nombre' =>'Batería'
        ]);
            $this->insert('partes',[
            'id' => '3',
            'nombre' =>'Gomas'
        ]);
            $this->insert('partes',[
            'id' => '4',
            'nombre' =>'Sistema Eléctrico'
        ]);
            $this->insert('partes',[
            'id' => '5',
            'nombre' =>'Tren Delantero'
        ]);   
         $this->insert('partes',[
            'id' => '6',
            'nombre' =>'Motor'
        ]);
        $this->insert('partes',[
            'id' => '7',
            'nombre' =>'Transmisión'
        ]);
        $this->insert('partes',[
            'id' => '8',
            'nombre' =>'Corrosión'
        ]);
        $this->insert('partes',[
            'id' => '9',
            'nombre' =>'Pintura'
        ]);
            $this->insert('partes',[
            'id' => '10',
            'nombre' =>'Bumper'
        ]);
            $this->insert('partes',[
            'id' => '11',
            'nombre' =>'Aire Acondicionado'
        ]);
    }

    public function safeDown()
    {
        echo "m170817_210731_insert_partes cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170817_210731_insert_partes cannot be reverted.\n";

        return false;
    }
    */
}
