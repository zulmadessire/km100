<?php

use yii\db\Migration;

class m170716_171550_create_conf_mantenimiento extends Migration
{
    public function safeUp()
    {
        $this->createTable('tipo_mantenimiento', [
            'id_tipo_mnto' => $this->primaryKey(),
            'nombre' => $this->string(50)->notNull(),
        ]);

        //////////////////////////////////////////////////////////
        $this->createTable('actividad_mnto', [
            'id_actividad_mnto' => $this->primaryKey(),
            'nombre' => $this->string(50)->notNull(),
            'tipo_actividad' => $this->integer(1)->notNull(),
            'id_tipo_mnto' => $this->integer()->notNull(),
        ]);
        $this->createIndex('idx-tipomantenimiento-actividadmnto','actividad_mnto','id_tipo_mnto');
        $this->addForeignKey('fk-tipomantenimiento-actividadmnto', 'actividad_mnto', 'id_tipo_mnto', 'tipo_mantenimiento', 'id_tipo_mnto', 'CASCADE');
        $this->addCommentOnColumn ('actividad_mnto', 'tipo_actividad', '0-Cambio, 1-Chequeo');

        //////////////////////////////////////////////////////////
        $this->createTable('tipo_pieza', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(45)->notNull(),   
        ]);

        $this->createTable('pieza', [
            'id_pieza' => $this->primaryKey(),
            'nombre' => $this->string(50)->notNull(),
            'tamano' => $this->string(45),
            'unidad' => $this->string(45),
            'tipo_pieza_id' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-pieza-tipo-pieza-id','pieza','tipo_pieza_id');
        $this->addForeignKey('fk-pieza-tipo-pieza-id', 'pieza', 'tipo_pieza_id', 'tipo_pieza', 'id', 'CASCADE');

        //////////////////////////////////////////////////////////
        $this->createTable('actividad_pieza', [
            'id_activ_pieza' => $this->primaryKey(),
            'id_actividad_mnto' => $this->integer()->notNull(),
            'id_pieza' => $this->integer()->notNull(),
        ]);
        $this->createIndex('idx-actividadmnto-activpieza','actividad_pieza','id_actividad_mnto');
        $this->addForeignKey('fk-actividadmnto-activpieza', 'actividad_pieza', 'id_actividad_mnto', 'actividad_mnto', 'id_actividad_mnto', 'CASCADE');
        $this->createIndex('idx-pieza-activpieza','actividad_pieza','id_pieza');
        $this->addForeignKey('fk-pieza-activpieza', 'actividad_pieza', 'id_pieza', 'pieza', 'id_pieza', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m170716_171550_create_conf_mantenimiento cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170716_171550_create_conf_mantenimiento cannot be reverted.\n";

        return false;
    }
    */
}
