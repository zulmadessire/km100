<?php

use yii\db\Migration;

class m180321_212207_drop_tablas_licitaciones extends Migration
{
    public function safeUp()
    {

        $this->dropTable('precios');
        $this->dropTable('precio_proveedor');
        $this->dropTable('proveedor_pieza_lic');
        $this->dropTable('licitacion_solicitud');
        $this->dropTable('solicitudes');
        $this->dropTable('capacitaciones');
        $this->dropTable('licitacion');
        $this->dropTable('estado_licitacion');
    }

    public function safeDown()
    {
        echo "m180321_212207_drop_tablas_licitaciones cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180321_212207_drop_tablas_licitaciones cannot be reverted.\n";

        return false;
    }
    */
}
