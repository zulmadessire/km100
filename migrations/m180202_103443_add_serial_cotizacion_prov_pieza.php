<?php

use yii\db\Migration;

class m180202_103443_add_serial_cotizacion_prov_pieza extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cotizacion_proveedor_pieza', 'serial', $this->string(50));
    }

    public function safeDown()
    {
        echo "m180202_103443_add_serial_cotizacion_prov_pieza cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180202_103443_add_serial_cotizacion_prov_pieza cannot be reverted.\n";

        return false;
    }
    */
}
