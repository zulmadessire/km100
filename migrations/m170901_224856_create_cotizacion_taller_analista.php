<?php

use yii\db\Migration;

class m170901_224856_create_cotizacion_taller_analista extends Migration
{
    public function safeUp()
    {
        $this->createTable('cotizacion_taller_analista', [
            'id' => $this->primaryKey(),
            'id_cotizacion_taller' => $this->integer()->notNull(),
            'id_analista' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-cotizacion_taller_analista_cotizacion_taller','cotizacion_taller_analista','id_cotizacion_taller');
        $this->addForeignKey('fk-cotizacion_taller_analista_cotizacion_taller', 'cotizacion_taller_analista', 'id_cotizacion_taller', 'cotizacion_taller', 'id', 'CASCADE');

        $this->createIndex('idx-cotizacion_taller_analista_analista','cotizacion_taller_analista','id_analista');
        $this->addForeignKey('fk-cotizacion_taller_analista_analista', 'cotizacion_taller_analista', 'id_analista', 'analista_mantenimiento', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m170901_224856_create_cotizacion_taller_analista cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170901_224856_create_cotizacion_taller_analista cannot be reverted.\n";

        return false;
    }
    */
}
