<?php

use yii\db\Migration;

class m171012_202709_create_orden_compra_pieza extends Migration
{
    public function safeUp()
    {
        $this->createTable('orden_compra_pieza', [
            'id' => $this->primaryKey(),
            'fecha' => $this->timestamp(),
            'estado' => $this->integer(),
            'observaciones' => $this->text(),
            'total' => $this->double(),
            'id_solicitud_compra_pieza' => $this->integer(),
            'id_proveedor' => $this->integer(),
            'id_cotizacion_proveedor' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-orden_compra_pieza_cotizacion_proveedor','orden_compra_pieza','id_cotizacion_proveedor');
        $this->addForeignKey('fk-orden_compra_pieza_cotizacion_proveedor', 'orden_compra_pieza', 'id_cotizacion_proveedor', 'cotizacion_proveedor', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m171012_202709_create_orden_compra_pieza cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171012_202709_create_orden_compra_pieza cannot be reverted.\n";

        return false;
    }
    */
}
