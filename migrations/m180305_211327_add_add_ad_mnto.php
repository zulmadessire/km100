<?php

use yii\db\Migration;

class m180305_211327_add_add_ad_mnto extends Migration
{
    public function safeUp()
    {
$this->addColumn('add_mnto', 'km_actual', $this->integer());

    }

    public function safeDown()
    {
        echo "m180305_211327_add_add_ad_mnto cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180305_211327_add_add_ad_mnto cannot be reverted.\n";

        return false;
    }
    */
}
