<?php

use yii\db\Migration;

class m180304_195628_add_auditoria_imagenfile extends Migration
{
    public function safeUp()
    {
 

$this->addColumn('sol_aud_fs', 'imageFile', $this->string(1000));

    }

    public function safeDown()
    {
        echo "m180304_195628_add_auditoria_imagenfile cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180304_195628_add_auditoria_imagenfile cannot be reverted.\n";

        return false;
    }
    */
}
