<?php

use yii\db\Migration;

class m171213_151704_create_notificaciones_venc_ extends Migration
{
    public function safeUp()
    {
        
         $this->createTable('notificaciones', [
            'id' => $this->primaryKey(),
            'tiempo' => $this->integer(),
            'tipo' => $this->integer(),
            'estado' => $this->integer(),
            'id_vehiculo' => $this->integer(),
            'id_taller' => $this->integer(),
        ]);

        $this->addForeignKey('fk-id_vehiculo_notificaciones', 'notificaciones', 'id_vehiculo', 'vehiculo', 'id', 'CASCADE');

        $this->addForeignKey('fk-id_taller_notificaciones', 'notificaciones', 'id_taller', 'taller', 'id_taller', 'CASCADE');
    
    }

    public function safeDown()
    {
        echo "m171213_151704_create_notificaciones_venc_ cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171213_151704_create_notificaciones_venc_ cannot be reverted.\n";

        return false;
    }
    */
}
