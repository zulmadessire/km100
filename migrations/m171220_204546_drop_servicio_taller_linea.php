<?php

use yii\db\Migration;

class m171220_204546_drop_servicio_taller_linea extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('servicio_taller', 'linea');
    }

    public function safeDown()
    {
        echo "m171220_204546_drop_servicio_taller_linea cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171220_204546_drop_servicio_taller_linea cannot be reverted.\n";

        return false;
    }
    */
}
