<?php

use yii\db\Migration;

class m171221_144145_add_servicio_taller_labor extends Migration
{
    public function safeUp()
    {
        $this->addColumn('servicio_taller', 'labor',  $this->integer()); 

    }

    public function safeDown()
    {
        echo "m171221_144145_add_servicio_taller_labor cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171221_144145_add_servicio_taller_labor cannot be reverted.\n";

        return false;
    }
    */
}
