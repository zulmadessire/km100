<?php

use yii\db\Migration;

class m180304_194001_add_auditoria_anexo extends Migration
{
    public function safeUp()
    {
$this->addColumn('sol_aud_fs', 'anexo', $this->string(1000));
$this->addColumn('sol_aud_fs', 'decripcion', $this->string(500));
$this->addColumn('sol_aud_fs', 'extension', $this->string(1000));
$this->addColumn('sol_aud_fs', 'base_url', $this->string(1000));
 
    }

    public function safeDown()
    {
        echo "m180304_194001_add_auditoria_anexo cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180304_194001_add_auditoria_anexo cannot be reverted.\n";

        return false;
    }
    */
}
