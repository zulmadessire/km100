<?php

use yii\db\Migration;

class m180307_125204_add_auditorias_fs_comentarios extends Migration
{
    public function safeUp()
    {
        
        $this->addColumn('auditoria_mnto', 'comentario', $this->string(1000));

    }

    public function safeDown()
    {
        echo "m180307_125204_add_auditorias_fs_comentarios cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180307_125204_add_auditorias_fs_comentarios cannot be reverted.\n";

        return false;
    }
    */
}
