<?php

use yii\db\Migration;

class m171102_130752_create_mensajero extends Migration
{
    public function safeUp()
    {
         $this->createTable('mensajero', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(200)->notNull(),
            'cedula' => $this->string(20),
            'telefono' => $this->string(40),
            'direccion' => $this->string(450),
            'placa' => $this->string(20),
            'anho' => $this->string(20),
            'id_modelo_id' => $this->integer(),


        ]);

        $this->addForeignKey('fk-id_modelo_id_mensajero', 'mensajero', 'id_modelo_id', 'modelo', 'id_modelo', 'CASCADE');

    }

    public function safeDown()
    {
        echo "m171102_130752_create_mensajero cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171102_130752_create_mensajero cannot be reverted.\n";

        return false;
    }
    */
}
