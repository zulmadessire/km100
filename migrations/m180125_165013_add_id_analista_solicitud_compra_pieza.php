<?php

use yii\db\Migration;

class m180125_165013_add_id_analista_solicitud_compra_pieza extends Migration
{
    public function safeUp()
    {
        $this->addColumn('solicitud_compra_pieza', 'id_analista',  $this->integer());
    }

    public function safeDown()
    {
        echo "m180125_165013_add_id_analista_solicitud_compra_pieza cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180125_165013_add_id_analista_solicitud_compra_pieza cannot be reverted.\n";

        return false;
    }
    */
}
