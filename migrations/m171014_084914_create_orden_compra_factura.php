<?php

use yii\db\Migration;

class m171014_084914_create_orden_compra_factura extends Migration
{
    public function safeUp()
    {
        $this->createTable('orden_compra_factura', [
            'id' => $this->primaryKey(),
            'numero_factura' => $this->string(45),
            'nic' => $this->string(45),
            'fecha_emision' => $this->date(),
            'fecha_vencimiento' => $this->date(),
            'fecha' => $this->timestamp(),
            'condicion_pago' => $this->string(45),
            'monto' => $this->double(),
            'estado' => $this->integer(),
            'id_orden_compra_pieza' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-orden_compra_factura_orden_compra_pieza','orden_compra_factura','id_orden_compra_pieza');
        $this->addForeignKey('fk-orden_compra_factura_orden_compra_pieza', 'orden_compra_factura', 'id_orden_compra_pieza', 'orden_compra_pieza', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m171014_084914_create_orden_compra_factura cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171014_084914_create_orden_compra_factura cannot be reverted.\n";

        return false;
    }
    */
}
