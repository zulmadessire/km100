<?php

use yii\db\Migration;

class m171105_213736_create_despacho_pieza extends Migration
{
    public function safeUp()
    {
        $this->createTable('despacho', [
            'id' => $this->primaryKey(),
            'numero' => $this->integer(),
            'estado' => $this->integer(),
            'fecha' => $this->date(),
            'estado_recepcion_taller' => $this->integer(),
            'traslado' => $this->string(45),
            'id_solicitud_compra_pieza' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-despacho_solicitud_compra_pieza','despacho','id_solicitud_compra_pieza');
        $this->addForeignKey('fk-despacho_solicitud_compra_pieza', 'despacho', 'id_solicitud_compra_pieza', 'solicitud_compra_pieza', 'id', 'CASCADE');

        $this->createTable('despacho_proveedor_pieza', [
            'id' => $this->primaryKey(),
            'cantidad_enviada_taller' => $this->integer(),
            'cantidad_recibida_taller' => $this->integer(),
            'id_despacho' => $this->integer()->notNull(),
            'id_cotizacion_proveedor_pieza' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-despacho_proveedor_pieza_despacho','despacho_proveedor_pieza','id_despacho');
        $this->addForeignKey('fk-despacho_proveedor_pieza_despacho', 'despacho_proveedor_pieza', 'id_despacho', 'despacho', 'id', 'CASCADE');

        $this->createIndex('idx-despacho_proveedor_cotizacion_pieza','despacho_proveedor_pieza','id_cotizacion_proveedor_pieza');
        $this->addForeignKey('fk-despacho_proveedor_cotizacion_pieza', 'despacho_proveedor_pieza', 'id_cotizacion_proveedor_pieza', 'cotizacion_proveedor_pieza', 'id', 'CASCADE');

    }

    public function safeDown()
    {
        echo "m171105_213736_create_despacho_pieza cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171105_213736_create_despacho_pieza cannot be reverted.\n";

        return false;
    }
    */
}
