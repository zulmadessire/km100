<?php

use yii\db\Migration;

class m170724_110659_create_proyeccion_mantenimiento extends Migration
{
    public function safeUp()
    {
        $this->createTable('proyeccion_mantenimiento', [
            'id' => $this->primaryKey(),
            'fecha_proyectada' => $this->date(),
            'km_proyectado' => $this->string(45),
            'realizado' => $this->boolean()->defaultValue(0),
            'serie' => $this->integer(),
            'id_vehiculo' => $this->integer()->notNull(),
            'id_proximo_tipo_mantenimiento' => $this->integer()->notNull(),
        ]);

        $this->addCommentOnColumn('proyeccion_mantenimiento', 'serie', '1-General, 2-General, 3-Intermedio, 4-General, 5-General, 6-Detallado');

        $this->createIndex('idx-proyeccion_mantenimiento_vehiculo','proyeccion_mantenimiento','id_vehiculo');
        $this->addForeignKey('fk-proyeccion_mantenimiento_vehiculo', 'proyeccion_mantenimiento', 'id_vehiculo', 'vehiculo', 'id', 'CASCADE');

        $this->createIndex('idx-proyeccion_mantenimiento_tipo_mnto_prox','proyeccion_mantenimiento','id_proximo_tipo_mantenimiento');
        $this->addForeignKey('fk-proyeccion_mantenimiento_tipo_mnto_prox', 'proyeccion_mantenimiento', 'id_proximo_tipo_mantenimiento', 'tipo_mantenimiento', 'id_tipo_mnto', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m170724_110659_create_proyeccion_mantenimiento cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170724_110659_create_proyeccion_mantenimiento cannot be reverted.\n";

        return false;
    }
    */
}
