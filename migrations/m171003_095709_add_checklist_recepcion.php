<?php

use yii\db\Migration;

class m171003_095709_add_checklist_recepcion extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cotizacion_taller_actividad', 'conforme_recepcion', $this->integer());
        $this->addColumn('servicio', 'observacion_recepcion', $this->text());
    }

    public function safeDown()
    {
        echo "m171003_095709_add_checklist_recepcion cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171003_095709_add_checklist_recepcion cannot be reverted.\n";

        return false;
    }
    */
}
