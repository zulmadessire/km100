<?php

use yii\db\Migration;

class m180216_004538_add_id_solicitud_pieza_estacion_cotizacion_proveedor_pieza extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cotizacion_proveedor_pieza', 'id_solicitud_pieza_estacion', $this->integer());
    }

    public function safeDown()
    {
        echo "m180216_004538_add_id_solicitud_pieza_estacion_cotizacion_proveedor_pieza cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180216_004538_add_id_solicitud_pieza_estacion_cotizacion_proveedor_pieza cannot be reverted.\n";

        return false;
    }
    */
}
