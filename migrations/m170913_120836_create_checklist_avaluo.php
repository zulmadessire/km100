<?php

use yii\db\Migration;

class m170913_120836_create_checklist_avaluo extends Migration
{
    public function safeUp()
    {
        $this->createTable('checklist_avaluo', [
            'id' => $this->primaryKey(),
            'danos' => $this->string(45),
            'diagnostico' => $this->string(45),
            'id_cotizacion_taller_analista' => $this->integer(),
        ]);

        $this->addForeignKey('fk-checklist_avaluo_cotizacion_taller_analista', 'checklist_avaluo', 'id_cotizacion_taller_analista', 'cotizacion_taller_analista', 'id', 'CASCADE');
        
    }

    public function safeDown()
    {
        echo "m170913_120836_create_checklist_avaluo cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170913_120836_create_checklist_avaluo cannot be reverted.\n";

        return false;
    }
    */
}
