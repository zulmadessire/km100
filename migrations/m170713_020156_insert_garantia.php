<?php

use yii\db\Migration;

class m170713_020156_insert_garantia extends Migration
{
    public function safeUp()
    {
        $this->insert('garantia',[
            'nombre' => 'Vehículo',
        ]);

        $this->insert('garantia',[
            'nombre' => 'Batería',
        ]);

        $this->insert('garantia',[
            'nombre' => 'Gomas',
        ]);

        $this->insert('garantia',[
            'nombre' => 'Sistema eléctrico',
        ]);
    }

    public function safeDown()
    {
        echo "m170713_020156_insert_garantia cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170713_020156_insert_garantia cannot be reverted.\n";

        return false;
    }
    */
}
