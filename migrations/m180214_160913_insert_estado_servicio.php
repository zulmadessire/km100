<?php

use yii\db\Migration;

class m180214_160913_insert_estado_servicio extends Migration
{
    public function safeUp()
    {
        $this->insert('estado_servicio',[
            'nombre'        => 'Cancelado',
            'color'         => '#dd4b39',
            'codigo'        => 12,

        ]);

        $this->insert('estado_compra_pieza',[
            'nombre'        => 'Cancelado',
            'color'         => '#dd4b39',
            'codigo'        => 7,
            'dias_optimos'  => 1
        ]);
    }

    public function safeDown()
    {
        echo "m180214_160913_insert_estado_servicio cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180214_160913_insert_estado_servicio cannot be reverted.\n";

        return false;
    }
    */
}
