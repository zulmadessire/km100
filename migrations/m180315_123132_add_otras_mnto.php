<?php

use yii\db\Migration;

class m180315_123132_add_otras_mnto extends Migration
{
    public function safeUp()
    {
                $this->createTable('otras_mnto', [
            'id' => $this->primaryKey(),
            'km_proximo' => $this->string(100),
            'ficha_mva' => $this->string(100),
            'km_actual' => $this->string(100),
            'observacion' => $this->string(1000),
            'id_aud_mnto' => $this->integer()->notNull(),
            ]);
        
        $this->addForeignKey('fk-id_aud_mnto_otrasmnto', 'otras_mnto', 'id_aud_mnto', 'auditoria_mnto', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m180315_123132_add_otras_mnto cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180315_123132_add_otras_mnto cannot be reverted.\n";

        return false;
    }
    */
}
