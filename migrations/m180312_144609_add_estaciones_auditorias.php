<?php

use yii\db\Migration;

class m180312_144609_add_estaciones_auditorias extends Migration
{
    public function safeUp()
    {

        $this->addColumn('estacion', 'audita', $this->integer());

    }

    public function safeDown()
    {
        echo "m180312_144609_add_estaciones_auditorias cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180312_144609_add_estaciones_auditorias cannot be reverted.\n";

        return false;
    }
    */
}
