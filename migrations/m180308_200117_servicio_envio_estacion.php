<?php

use yii\db\Migration;

/**
 * Class m180308_200117_servicio_envio_estacion
 */
class m180308_200117_servicio_envio_estacion extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('servicio', 'aprobacion_estacion',  $this->integer());
        $this->addColumn('servicio', 'fecha_envio_estacion',  $this->date());
        $this->addColumn('servicio', 'km_estacion',  $this->string(100));
        $this->addColumn('servicio', 'combustible_estacion',  $this->double());
        $this->addColumn('servicio', 'km_taller',  $this->string(100));
        $this->addColumn('servicio', 'combustible_taller',  $this->double());
        $this->addColumn('servicio', 'registrado_por',  $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180308_200117_servicio_envio_estacion cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180308_200117_servicio_envio_estacion cannot be reverted.\n";

        return false;
    }
    */
}
