<?php

use yii\db\Migration;

class m170826_102305_create_cotizacion_taller extends Migration
{
    public function safeUp()
    {
        $this->createTable('cotizacion_taller', [
            'id' => $this->primaryKey(),
            'imagen_soporte' => $this->string(200),
            'tipo_pago' => $this->integer(1),
            'fecha' => $this->timestamp(),
            'estado' => $this->integer(1),
            'activo' => $this->integer(1),
            'id_solicitud_servicio_taller' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-solicitud_servicio_taller_cotizacion_taller','cotizacion_taller','id_solicitud_servicio_taller');
        $this->addForeignKey('fk-solicitud_servicio_taller_cotizacion_taller', 'cotizacion_taller', 'id_solicitud_servicio_taller', 'solicitud_servicio_taller', 'id', 'CASCADE');

        $this->createTable('cotizacion_taller_pieza', [
            'id' => $this->primaryKey(),
            'cantidad' => $this->integer()->notNull(),
            'id_cotizacion_taller' => $this->integer()->notNull(),
            'id_pieza' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-cotizacion_taller_pieza','cotizacion_taller_pieza','id_cotizacion_taller');
        $this->addForeignKey('fk-cotizacion_taller_pieza', 'cotizacion_taller_pieza', 'id_cotizacion_taller', 'cotizacion_taller', 'id', 'CASCADE');

        $this->createIndex('idx-pieza_cotizacion_taller_pieza','cotizacion_taller_pieza','id_pieza');
        $this->addForeignKey('fk-pieza_cotizacion_taller_pieza', 'cotizacion_taller_pieza', 'id_pieza', 'pieza', 'id_pieza', 'CASCADE');


        $this->createTable('cotizacion_taller_actividad', [
            'id' => $this->primaryKey(),
            'descripcion' => $this->string()->notNull(),
            'cantidad' => $this->integer()->notNull(),
            'precio' => $this->double()->notNull(),
            'id_cotizacion_taller' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-cotizacion_taller_actividad','cotizacion_taller_actividad','id_cotizacion_taller');
        $this->addForeignKey('fk-cotizacion_taller_actividad', 'cotizacion_taller_actividad', 'id_cotizacion_taller', 'cotizacion_taller', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m170826_102305_create_cotizacion_taller cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170826_102305_create_cotizacion_taller cannot be reverted.\n";

        return false;
    }
    */
}
