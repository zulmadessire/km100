<?php

use yii\db\Migration;

class m180112_024748_add_servicio_cancelado extends Migration
{
    public function safeUp()
    {
        $this->addColumn('servicio', 'observacion_cancelado',  $this->text());   
        $this->addColumn('servicio', 'usuario_cancela',  $this->integer());  
        
    }

    public function safeDown()
    {
        echo "m180112_024748_add_servicio_cancelado cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180112_024748_add_servicio_cancelado cannot be reverted.\n";

        return false;
    }
    */
}
