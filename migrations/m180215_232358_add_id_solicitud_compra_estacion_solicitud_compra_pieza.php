<?php

use yii\db\Migration;

class m180215_232358_add_id_solicitud_compra_estacion_solicitud_compra_pieza extends Migration
{
    public function safeUp()
    {
        $this->addColumn('solicitud_compra_pieza', 'id_solicitud_compra_estacion', $this->integer());
        $this->addColumn('solicitud_compra_pieza', 'origen_solicitud', $this->integer());
    }

    public function safeDown()
    {
        echo "m180215_232358_add_id_solicitud_compra_estacion_solicitud_compra_pieza cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180215_232358_add_id_solicitud_compra_estacion_solicitud_compra_pieza cannot be reverted.\n";

        return false;
    }
    */
}
