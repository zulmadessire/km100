<?php

use yii\db\Migration;

class m171029_145030_create_solicitudes extends Migration
{
    public function safeUp()
    {


 $this->createTable('solicitudes', [
            'id' => $this->primaryKey(),
            'numero' => $this->integer(),
            'solicitud' => $this->string(450),
            'cantidad' => $this->integer(),
            'observacion' => $this->string(450),
            'id_auditoria' => $this->integer(),

        ]);

        $this->addForeignKey('fk-id_auditoria_solicitudes', 'solicitudes', 'id_auditoria', 'auditoria', 'id', 'CASCADE');

 }
    public function safeDown()
    {
        echo "m171029_145030_create_solicitudes cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171029_145030_create_solicitudes cannot be reverted.\n";

        return false;
    }
    */
}
