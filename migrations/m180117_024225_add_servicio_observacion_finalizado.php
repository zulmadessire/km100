<?php

use yii\db\Migration;

class m180117_024225_add_servicio_observacion_finalizado extends Migration
{
    public function safeUp()
    {
        $this->addColumn('servicio', 'observacion_finalizado',  $this->text());  
    }

    public function safeDown()
    {
        echo "m180117_024225_add_servicio_observacion_finalizado cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180117_024225_add_servicio_observacion_finalizado cannot be reverted.\n";

        return false;
    }
    */
}
