<?php

use yii\db\Migration;

class m180227_024803_add_add_fuera_servicio extends Migration
{
    public function safeUp()
    {
        $this->addColumn('add_fs', 'id_vehiculo', $this->integer());
        $this->addColumn('add_fs', 'id_orden_compra', $this->integer());
        $this->addColumn('add_fs', 'estado', $this->integer());
        $this->addForeignKey('fk-add_fs_id_vehiculo', 'add_fs', 'id_vehiculo', 'vehiculo', 'id', 'CASCADE');
        $this->addForeignKey('fk-add_fs_id_orden_compra', 'add_fs', 'id_orden_compra', 'solicitud_compra_pieza', 'id', 'CASCADE');


    }

    public function safeDown()
    {
        echo "m180227_024803_add_add_fuera_servicio cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180227_024803_add_add_fuera_servicio cannot be reverted.\n";

        return false;
    }
    */
}
