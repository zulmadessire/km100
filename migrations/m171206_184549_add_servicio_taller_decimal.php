<?php

use yii\db\Migration;

class m171206_184549_add_servicio_taller_decimal extends Migration
{
    public function safeUp()
    {
        $this->addColumn('servicio_taller', 'decimal',  $this->double(5,3)); 
    }

    public function safeDown()
    {
        echo "m171206_184549_add_servicio_taller_decimal cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171206_184549_add_servicio_taller_decimal cannot be reverted.\n";

        return false;
    }
    */
}
