<?php

use yii\db\Migration;

class m171124_142312_create_cliente_drop extends Migration
{
    public function safeUp()
    {
           $this->dropColumn('empresa', 'nombre_comercial');

          $this->dropForeignKey(
            'fk-empresa-user',
            'user'
        );
           $this->dropIndex(
            'idx-user-id_empresa',
            'user'
        );
        $this->dropColumn('user', 'id_empresa');
    }

    public function safeDown()
    {
        echo "m171124_142312_create_cliente_drop cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171124_142312_create_cliente_drop cannot be reverted.\n";

        return false;
    }
    */
}
