<?php

use yii\db\Migration;

class m180109_205222_add_cotizacion_taller_imagenes extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cotizacion_taller', 'imagen_soporte1', $this->string(200));
        $this->addColumn('cotizacion_taller', 'imagen_soporte2', $this->string(200));
        $this->addColumn('cotizacion_taller', 'imagen_soporte3', $this->string(200));
        $this->addColumn('cotizacion_taller', 'imagen_soporte4', $this->string(200));
    }

    public function safeDown()
    {
        echo "m180109_205222_add_cotizacion_taller_imagenes cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180109_205222_add_cotizacion_taller_imagenes cannot be reverted.\n";

        return false;
    }
    */
}
