<?php

use yii\db\Migration;

/**
 * Class m180216_104237_add_servicio_info_salvamento
 */
class m180216_104237_add_servicio_info_salvamento extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('servicio', 'observacion_gerente',  $this->string(200));
        $this->addColumn('servicio', 'observacion_director',  $this->string(200));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180216_104237_add_servicio_info_salvamento cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180216_104237_add_servicio_info_salvamento cannot be reverted.\n";

        return false;
    }
    */
}
