<?php

use yii\db\Migration;

/**
 * Class m180302_170639_delete_fk_id_cotizacion_taller_pieza_cotizacion_proveedor_pieza
 */
class m180302_170639_delete_fk_id_cotizacion_taller_pieza_cotizacion_proveedor_pieza extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey(
            'fk-cotizacion_proveedor_pieza_cotizacion_taller_pieza',
            'cotizacion_proveedor_pieza'
        );

        $this->dropIndex(
            'idx-cotizacion_proveedor_pieza_cotizacion_taller_pieza',
            'cotizacion_proveedor_pieza'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180302_170639_delete_fk_id_cotizacion_taller_pieza_cotizacion_proveedor_pieza cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180302_170639_delete_fk_id_cotizacion_taller_pieza_cotizacion_proveedor_pieza cannot be reverted.\n";

        return false;
    }
    */
}
