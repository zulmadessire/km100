<?php

use yii\db\Migration;

class m180304_182328_add_auditoria_fuera_servicio extends Migration
{
    public function safeUp()
    {
    $this->dropForeignKey(
            'fk-id_aud_fs_sol_aud_fs',
            'sol_aud_fs'
        );
        $this->dropForeignKey(
            'fk-id_aud_fs_add_fs',
            'add_fs'
        );
        $this->dropTable('sol_aud_fs');
        $this->dropTable('add_fs');
    

        
        $this->createTable('sol_aud_fs', [
            'id' => $this->primaryKey(),
            'ficha' => $this->string(20),
            'id_modelo' => $this->integer(),
            'causa' => $this->string(200),
            'responsable' => $this->string(20),
            'observacion' => $this->string(1000),
            'id_aud_fs' => $this->integer()->notNull(),
            'numero' => $this->integer(),
            'solicitud' => $this->string(500),
            'cantidad' => $this->integer(),
            ]);

        $this->addForeignKey('fk-sol_aud_fs_id_modelo', 'sol_aud_fs', 'id_modelo', 'modelo', 'id_modelo', 'CASCADE');
        $this->addForeignKey('fk-id_aud_fs_sol_aud_fs', 'sol_aud_fs', 'id_aud_fs', 'auditoria_fs', 'id', 'CASCADE');

        $this->createTable('add_fs', [
             'id' => $this->primaryKey(),
            'causa' => $this->string(500),
            'observacion' => $this->string(1000),
            'id_aud_fs' => $this->integer()->notNull(),
            'ficha' => $this->string(20),
            'id_modelo' => $this->integer(),
            'km_actual' => $this->string(40),
            'id_tipo_mnto' => $this->integer(),

            ]);

        $this->addForeignKey('fk-id_aud_fs_add_fs', 'add_fs', 'id_aud_fs', 'auditoria_fs', 'id', 'CASCADE');
        $this->addForeignKey('fk-id_aud_fs_tipo_mnto', 'add_fs', 'id_tipo_mnto', 'tipo_mantenimiento', 'id_tipo_mnto', 'CASCADE');
        $this->addForeignKey('fk-id_aud_id_modelo', 'add_fs', 'id_modelo', 'modelo', 'id_modelo', 'CASCADE');

            $this->createTable('anexos', [
             'id' => $this->primaryKey(),
            'decripcion' => $this->string(500),
            'anexo' => $this->string(1000),
            'extension' => $this->string(1000),
            'base_url' => $this->string(1000),
            'id_sol_aud_fs' => $this->integer()->notNull(),

            ]);
        $this->addForeignKey('fk-id_sol_aud_fs_anexos', 'anexos', 'id_sol_aud_fs', 'sol_aud_fs', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m180304_182328_add_auditoria_fuera_servicio cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180304_182328_add_auditoria_fuera_servicio cannot be reverted.\n";

        return false;
    }
    */
}
