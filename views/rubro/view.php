<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\RubroModeloAsignado;
use app\models\ModeloRubro;
use app\models\MarcaRubro;

/* @var $this yii\web\View */
/* @var $model app\models\Rubro */

$this->title = 'INFORMACIÓN DE MATERIAL ';
?>
<div class="rubro-view">

    	<div class="row">
    		<div class="col-md-1 col-xs-3">
    			<h4 class="titulo"><i class="fa fa-tint fa-4x" aria-hidden="true"></i></h4>
    		</div>
    		<div class="col-md-10 col-xs-9" style="padding-top: 30px;">
    			<h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
    		</div>
    	</div>
    	</br>
      <div class="row" style="border-bottom: 1px solid #222d32;">
            <div class="col-xs-12 col-md-6">
                <h4>DATOS DEL MATERIAL</h4>
            </div>
            <div class="col-xs-12 col-md-6">
              <div align="right">
                    <?php
                    echo Html::a('Listado de materiales', ['index'], ['class' => 'btn btn-accion']);
                    ?>&nbsp;&nbsp;&nbsp;<?php
                    echo Html::a('Modificar material', ['update', 'id' => $model->id_rubro], ['class' => 'btn btn-accion']);
                    ?>
              </div>
            </div>
        </div>
        <br>
      <p>
        <table class ="tablacaso" border="1">
                <tr><th style="width: 30%;">Nombre: </th>           <td><?= $model->nombre ?></td></tr>
                <tr><th>Tamaño: </th>                               <td><?= $model->tamanio ?></td></tr>
                <tr><th>Unidad de medición: </th>                   <td><?= ($model->unidad_medicion == 0) ? 'Unidades' : 'Litros' ?></td></tr>
                <tr><th>Marcas -> Modelos Asociados</th>
                  <td>
                  <?php
                    $rma = RubroModeloAsignado::find()->where(['id_rubro' => $model->id_rubro])->All();
                    foreach ($rma as $key => $r) {
                      $modelo = ModeloRubro::find()->where(['id_modelo_rubro' => $r->id_modelo_rubro])->One();
                      $marca = MarcaRubro::find()->where(['id_marca_rubro' => $modelo->id_marca_rubro])->One();
                      echo ''.$marca->nombre.' -> '.$modelo->nombre.'<br>';
                    }
                   ?>
                 </td>
                </tr>

        </table><br>
</div>
