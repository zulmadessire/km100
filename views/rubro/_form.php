<?php

use yii\helpers\Html;
use app\models\MarcaRubro;
use app\models\ModeloRubro;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Rubro */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rubro-form">
  <?php $form = ActiveForm::begin(['id' => 'dynamic_lista-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>

  <br>
  <div class="row">
      <div class="col-xs-12 col-md-4">
          <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
      </div>
      <div class="col-xs-12 col-md-4">
          <?php
              $unidades = [0 => 'Unidades', 1 => 'Litros'];
              echo $form->field($model, "unidad_medicion")->dropDownList(
                  $unidades,
                  [
                      'prompt'=>'Selecciona la unidad',
                      'class' => 'form-control',
                  ]
                  );
          ?>
      </div>
      <div class="col-xs-12 col-md-4">
          <?= $form->field($model, 'tamanio')->textInput(['maxlength' => true]) ?>
      </div>
  </div>

  <br>
<?php DynamicFormWidget::begin([
      'widgetContainer' => 'dynamicform_wrapper_resultados',
      'widgetBody' => '.container-items',
      'widgetItem' => '.item',
      'limit' => 50,
      'min' => 1,
      'insertButton' => '.add-item',
      'deleteButton' => '.remove-item',
      'model' => $modelrubro[0],
      'formId' => 'dynamic_lista-form',
      'formFields' => [
          'id_marca_rubro',
          'id_modelo_rubro',
      ],
]); ?>
<table class="table table-hover">
    <tbody class="container-items">
    <?php foreach ($modelrubro as $i => $modelrub): ?>
        <tr class="item panel panel-default">
            <td>
              <div class="col-xs-12 col-md-4"><?php
                  $marcas = ArrayHelper::map(MarcaRubro::find()->orderBy('nombre')->all(), 'id_marca_rubro', 'nombre');
                  echo $form->field($modelrub, "[{$i}]id_marca_rubro")->dropDownList(
                          $marcas,
                          [
                              'prompt'=>'Selecciona la marca',
                              'class' => 'form-control',
                              'onchange'=>'
                                  var ident = $(this).attr("id");
                                  var nom = ident.split("-");
                                  var nmodelo = "modelorubro-"+nom[1]+"-id_modelo_rubro";
                                  $.get( "'.Url::toRoute('rubro/listarmodelosrubro').'", { id: $(this).val() } )
                                      .done(function( data ) {
                                          $( "#"+nmodelo ).html( null );
                                          $( "#"+nmodelo ).html( data );
                                      }
                                  );
                              ',
                          ] )->label('MARCA'); ?>
                  </div>
              <div class="col-xs-12 col-md-7">
                <?php
                  $modelos=array(); //echo "trae: {$i} --- ";$modelos=array();
                  if(!$modelrub->isNewRecord){
                      $modelos = ArrayHelper::map(ModeloRubro::find()->where(['id_marca_rubro'=>$modelrub->id_marca_rubro])->orderBy('nombre')->all(), 'id_modelo_rubro', 'nombre');
                  }
                  echo $form->field($modelrub, "[{$i}]id_modelo_rubro")->dropDownList(
                      $modelos,
                      [
                          'prompt'=>'Selecciona el modelo',
                          'class' => 'form-control']
                      )->label('MODELOS');
                ?>
              </div>

              <div class="col-xs-12 col-md-1 text-center">
                  <button type="button" class="remove-item btn btn-cancelar btn-xs" style="margin-top:28px;">
                      <i class="glyphicon glyphicon-minus"></i></button>
              </div>
          </td>
        </tr>
    <?php endforeach; ?>
<?php DynamicFormWidget::end(); ?>
    </tbody>
</table>

<button type="button" class="add-item btn btn-accion  btn-xs pull-right">Agregar servicio</button>
<br>



  <?php
      if(!$model->isNewRecord){
      ?>
          <div class="row" align="center">
              <div>
                  <?= Html::a('CANCELAR', ['view', 'id' => $model->id_rubro], ['class' => 'btn btn-cancelar']) ?>
                  &nbsp;&nbsp;&nbsp;
                  <?= Html::submitButton('GUARDAR CAMBIOS', ['class' => 'btn btn-primario']) ?>
              </div>
          </div>
      <?php
      }else{
      ?>
          <div class="form-group" align="center">
              <?= Html::submitButton('GUARDAR', ['class' => 'btn btn-primario']) ?>
          </div>
      <?php
      }
  ?>
<?php ActiveForm::end(); ?>
</div>
