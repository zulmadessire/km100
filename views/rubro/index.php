<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\RubroModeloAsignado;
use app\models\ModeloRubro;
use app\models\MarcaRubro;
use yii\helpers\ArrayHelper;
use kartik\form\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RubroSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'LISTADO DE MATERIALES';
?>
<div class="rubro-index">

    <div class="row">
      <div class="col-md-1 col-xs-3">
        <h4 class="titulo"><i class="fa fa-tint fa-4x" aria-hidden="true"></i></h4>
      </div>
      <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
        <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
      </div>
    </div>
    </br>
    <div class="row">
        <?php
            echo Html::a('Registrar Material', ['create'], ['class' => 'btn btn-accion', 'style' => 'float:right;']);
        ?>
    </div>
    <br>
    <div class="table-responsive" style="padding-bottom:80px;">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table table-responsive'],
        'summary' => '',
        'options' =>[
                        'class' => 'grid',
                    ],
        'columns' => [
            [
                'attribute' => 'nombre',
                'label' => 'Material',
                'value' => 'nombre',
                'format' => 'text',
                'filter' => true,
                'enableSorting' => true,
              ],
            [
                'label' => 'Marca -> Modelo',
                'format' => 'html',
                'value' => function ($model) {
                            $modelos = '';
                            $rma = RubroModeloAsignado::find()->where(['id_rubro' => $model->id_rubro])->All();
                            foreach ($rma as $r) {
                              $modelo = ModeloRubro::find()->where(['id_modelo_rubro' => $r->id_modelo_rubro])->One();
                              $marca = MarcaRubro::find()->where(['id_marca_rubro' => $modelo->id_marca_rubro])->One();
                              $modelos.=$marca->nombre.' -> '.$modelo->nombre.'<br>';
                            }
                            return $modelos;
                },
                'filter' => Select2::widget([
                    'name' => 'RubroSearch[id_marca_rubro]',
                    'data' => ArrayHelper::map(MarcaRubro::find()->orderBy('nombre')->all(), 'id_marca_rubro', 'nombre'),
                    'hideSearch' => true,
                    'options' => ['placeholder' => 'Marca',],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                     ]),
            ],
            [


                'attribute' => 'tamanio',
                'label' => 'Tamaño',
                'value' => 'nombre',
                'format' => 'text',
                'filter' => true,
                'enableSorting' => true,
                'value' => function ($model) {
                              return ($model->tamanio) ? $model->tamanio : "-";
                            },
            ],
            [
               'attribute' => 'unidad_medicion',
                'label' => 'Unidad de medición',
                'format' => 'text',
                'filter' => ['0'=>'Unidades','1'=>'Litros'],
                'enableSorting' => true,
                'value' => function ($model) {
                              return ($model->unidad_medicion==0) ? "Unidades" : "Litros";
                            },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'',
                'template' => '{menu}',
                'buttons' => [
                    'menu' => function ($url, $model) {
                         return  '<div class="dropdown">
                                  <button class="btn btn-default dropdown-toggle btn-sm" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i>
                                  <span class="caret"></span></button>
                                  <ul class="dropdown-menu">
                                    <li>'.Html::a('<span><i class="fa fa-eye fa-sm" aria-hidden="true"></i></span> Ver detalle',[ '/rubro/view', 'id' => $model->id_rubro ]).' </li>
                                    <li>'.Html::a('<span><i class="fa fa-pencil fa-sm" aria-hidden="true"></i></span> Modificar datos',[ '/rubro/update', 'id' => $model->id_rubro]).' </li>
                                    <li>'.Html::a('<span><i class="fa fa-trash-o fa-sm" aria-hidden="true"></i></span> Eliminar Material', ['delete', 'id' => $model->id_rubro], [
                                             'title' => Yii::t('user', 'Eliminar rubro'),
                                             'data-confirm' => Yii::t('user', 'Esta seguro de eliminar este rubro?'),
                                             'data-method' => 'POST',
                                         ]).' </li>
                                  </ul>
                                </div>';


                    },
                ],
           ],
        ],
    ]); ?>
  </div>
</div>
