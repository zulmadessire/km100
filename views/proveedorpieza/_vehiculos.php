<?php

use yii\helpers\Html;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\helpers\ArrayHelper;
use app\models\Marca;
use app\models\Modelo;
use yii\helpers\Url;
 
use yii\db\Query;
use yii\web\view;
 
use yii\bootstrap\ActiveForm;
 

?>
 
<?php DynamicFormWidget::begin([
    'widgetContainer' => 'dynamicform_inner',
    'widgetBody' => '.container-rooms',
    'widgetItem' => '.room-item',
    'limit' => 4,
    'min' => 1,
    'insertButton' => '.add-room',
    'deleteButton' => '.remove-room',
    'model' => $modelsVehiculos[0],
    'formId' => 'dynamic-form',
    'formFields' => [
        'description'
    ],
]); ?>
<table class="table table-bordered panel-default2">
    <thead>
        <tr>
            <th></th>
            <th class="text-center">
                <button type="button" class="add-room btn btn-gris btn-xs"><span class="glyphicon glyphicon-plus"></span></button>
            </th>

        </tr>
    </thead>
    <tbody class="container-rooms">
    <?php foreach ($modelsVehiculos as $indexVehiculo => $modelVehiculo): ?>
        <tr class="room-item">
            <td class="vcenter">

                <?php
                    // necessary for update action.
                $modelosList = [];
                    if (! $modelVehiculo->isNewRecord) {
                        echo Html::activeHiddenInput($modelVehiculo, "[{$indexPieza}][{$indexVehiculo}]id");
                        $modelVehiculo->marca = $modelVehiculo->modeloIdModelo->id_marca;
        $modelosList = ArrayHelper::map(Modelo::find(['nombre'.'unidad'])->orderBy('nombre')->where(['id_marca'=>$modelVehiculo->marca])->all(), 'id_modelo', 'nombre');
                     
                    }
                ?>




  <div class="col-md-4">
 

 
 
</div>

 <div class="col-md-4">
            
         
     

</div>



          <div class="col-md-4">      


  
</div>



            </td>
            <td class="text-center vcenter" style="width: 90px;">
                <button type="button" class="remove-room btn btn-danger btn-xs"><span class="glyphicon glyphicon-minus"></span></button>
            </td>
        </tr>
     <?php endforeach; ?>
    </tbody>
</table>
<?php DynamicFormWidget::end(); ?>