  <?php

  use yii\helpers\Html;
  use yii\db\Query;
  use yii\helpers\ArrayHelper;
  use yii\helpers\Url;
  use kartik\select2\Select2;

  use kartik\date\DatePicker;
  use kartik\file\FileInput;
  use kartik\form\ActiveForm;
  use yii\widgets\InputWidget;
  use kartik\widgets\DepDrop;

  use app\models\Empresa;
  use app\models\Pieza;
  use app\models\User;
  use app\models\AuthAssignment;
  use app\models\Marca;
  use app\models\Modelo;
  use app\models\TipoEmpresa;
  use app\models\Proveedor;
  use app\models\TipoPieza;
  use app\models\MarcaInsumo;
  use app\models\MarcaProveedor;
  


  use wbraganca\dynamicform\DynamicFormWidget;

  /* @var $this yii\web\View */
  /* @var $model app\models\Proveedorpieza */
  /* @var $form yii\widgets\ActiveForm */
  ?>

  <?php 
    $usuario  = User::findOne(['id'=>Yii::$app->user->identity->id]);
    $rol  = AuthAssignment::findOne(['user_id'=>Yii::$app->user->identity->id]);

   ?>

  <div class="paquete-form">
  <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

     <!-- --DATOS DE LA EMPRESA-- -->
    <br>

    <br>
    <div class="row">
        
       <div class="col-md-4">

            
            

               <?php 
                $tip_clientes = ['Natural'=>'Natural', 'Juridico'=>'Juridico'];
                echo $form->field($modelprov, 'tipo_persona')->dropDownList(
                        $tip_clientes,
                        [
                            'prompt'=>'Seleccione...',
                            'class' => 'form-control',
                        ] )->label('Tipo de persona<span class="obligatorio">*&nbsp;</span>');
            ?>



       </div>
       <div class="col-md-4">

            
            <?= $form->field($modelprov, 'identificacion')->textInput(['maxlength' => true, 'placeholder'=>'E-10029838'])->label('CÉDULA/RNC<span class="obligatorio">*&nbsp;</span>'); ?>

       </div>
       <div class="col-md-4">

            <!-- <span class="obligatorio2">*&nbsp;</span><label class="control-label">TIPO DE EMPRESA</label> -->

             <?= $form->field($modelprov, 'tipo_empresa_id')->dropDownList(
                ArrayHelper::map(TipoEmpresa::find()->all(), 'id' , 'nombre'),
                    ['prompt' => 'Seleccione el tipo de empresa', 'class' => 'form-control'
                     
                        
                    ]
            )->label('TIPO DE EMPRESA <span class="obligatorio">*&nbsp;</span>');   ?>

       </div>
          
    </div>

     <div class="row">
        
       <div class="col-md-8">

            
            <?= $form->field($modelprov, 'nombre')->textInput(['maxlength' => true])->label('NOMBRE <span class="obligatorio">*&nbsp;</span>');  ?>

       </div>
       <div class="col-md-4">

            
            <?= $form->field($modelprov, 'telefono')->textInput(['maxlength' => true])->label('TELÉFONO <span class="obligatorio">*&nbsp;</span>');  ?>

       </div>
         
    </div>
    <div class="row">
        
       <div class="col-md-12">

            
            <?= $form->field($modelprov, 'direccion')->textInput(['maxlength' => true])->label('DIRECCIÓN <span class="obligatorio">*&nbsp;</span>');  ?>

       </div>
         
    </div>
         <div class="row">
        
       <div class="col-md-12">

            
            <?= $form->field($modelprov, 'descripcion')->textInput(['maxlength' => true])->label('DESCRIPCIÓN DE ACTIVIDAD ECONÓMICA <span class="obligatorio">*&nbsp;</span>');  ?>

       </div>

         
    </div>
     <!-- --DATOS DEL REPRESENTANTE LEGAL-- -->
    <br>
    <div class="row" style="border-bottom: 1px solid #222d32;">
        <h4>DATOS DEL REPRESENTANTE LEGAL</h4>
    </div>
    <br>
    

     <div class="row">
        
       <div class="col-md-4">

            
            <?= $form->field($modelprov, 'nombre_representante')->textInput(['maxlength' => true])->label('NOMBRE <span class="obligatorio">*&nbsp;</span>');  ?>

       </div>
       <div class="col-md-4">

            
            <?= $form->field($modelprov, 'identificacion_representante')->textInput(['maxlength' => true])->label('NUMERO DE IDENTIFICACIÓN <span class="obligatorio">*&nbsp;</span>');  ?>

       </div>
       <div class="col-md-4">

            
            <?= $form->field($modelprov, 'puesto_representante')->textInput(['maxlength' => true])->label('PUESTO <span class="obligatorio">*&nbsp;</span>');  ?>

       </div>
          
    </div>

     <div class="row">
        
       <div class="col-md-4">

            
            <?= $form->field($modelprov, 'correo_representante')->textInput(['maxlength' => true])->label('CORREO<span class="obligatorio">*&nbsp;</span>');  ?>

       </div>
       <div class="col-md-4">

            
            <?= $form->field($modelprov, 'celular_representante')->textInput(['maxlength' => true])->label('CELULAR <span class="obligatorio">*&nbsp;</span>');  ?>

       </div>
       <div class="col-md-4">

            
            <?= $form->field($modelprov, 'telefono_representante')->textInput(['maxlength' => true])->label('TELÉFONO<span class="obligatorio">*&nbsp;</span>');  ?>

       </div>

        <div class="col-md-4">
            <?= $form->field($modelprov, 'user')->textInput(['maxlength' => true])->label('NOMBRE DE USUARIO<span class="obligatorio">*&nbsp;</span>');  ?>
        </div>
          
    </div>


    <br>
    <div class="row" style="border-bottom: 1px solid #222d32;">
        <h4>DEFINICIÓN DE PAGOS</h4>
    </div>
    <br>
    <div class="row">
      <?php 
        if($rol->item_name=='proveedor'  && !$modelprov->isNewRecord){?>
             <div class="col-md-4">

            
              <?php 
                $tip_clientes = ['0'=>'RD', '1'=>'USD'];
                echo $form->field($modelprov, 'moneda')->dropDownList(
                        $tip_clientes,
                        [
                            'prompt'=>'Seleccione...',
                            'class' => 'form-control',
                            'disabled' => true,
                        ] )->label('MONEDA <span class="obligatorio">*&nbsp;</span>');
            ?>

       </div>
        <?php }else{?>
          <div class="col-md-4">

            
              <?php 
                $tip_clientes = ['0'=>'RD', '1'=>'USD'];
                echo $form->field($modelprov, 'moneda')->dropDownList(
                        $tip_clientes,
                        [
                            'prompt'=>'Seleccione...',
                            'class' => 'form-control',
                        ] )->label('MONEDA <span class="obligatorio">*&nbsp;</span>');
            ?>

       </div>
       <?php }
       ?>
       
      <?php 
        if($rol->item_name=='proveedor' && !$modelprov->isNewRecord){?>
               <div class="col-md-4">

            
              <?php 
                $tip_clientes = ['credito'=>'Crédito', 'contado'=>'Contado'];
                echo $form->field($modelprov, 'Tipo_pago')->dropDownList(
                        $tip_clientes,
                        [
                            'prompt'=>'Seleccione...',
                            'disabled'=>true,
                            'class' => 'form-control',
                                      'onchange' => 'if(($(this).val() == "credito")){
                                          document.getElementById("monto").style.display="block";
                                          document.getElementById("plazo").style.display="block";
                                       
                                        }else if(($(this).val() == "contado")){
                                          document.getElementById("monto").style.display="none";
                                          document.getElementById("plazo").style.display="none";
                                        }else{
                                          document.getElementById("monto").style.display="none";
                                          document.getElementById("plazo").style.display="none";
                                        }',
                        ] )->label('TIPO DE PAGO <span class="obligatorio">*&nbsp;</span>'); ;
            ?>

       </div>
        <?php }else{?>
             <div class="col-md-4">

            
              <?php 
                $tip_clientes = ['credito'=>'Crédito', 'contado'=>'Contado'];
                echo $form->field($modelprov, 'Tipo_pago')->dropDownList(
                        $tip_clientes,
                        [
                            'prompt'=>'Seleccione...',
                            'class' => 'form-control',
                                      'onchange' => 'if(($(this).val() == "credito")){
                                          document.getElementById("monto").style.display="block";
                                          document.getElementById("plazo").style.display="block";
                                       
                                        }else if(($(this).val() == "contado")){
                                          document.getElementById("monto").style.display="none";
                                          document.getElementById("plazo").style.display="none";
                                        }else{
                                          document.getElementById("monto").style.display="none";
                                          document.getElementById("plazo").style.display="none";
                                        }',
                        ] )->label('TIPO DE PAGO <span class="obligatorio">*&nbsp;</span>'); ;
            ?>

       </div>
       <?php }
       ?>
        
     <?php 
        if($rol->item_name=='proveedor' && !$modelprov->isNewRecord){?>
           <div class="col-md-4 " id="monto" style="display: none;">
                    <?php echo $form->field($modelprov, 'credito')->label('MONTO DE CRÉDITO APROBADO <span class="obligatorio">*&nbsp;</span>'); ?>

       </div>
        <?php }else{?>
         <div class="col-md-4 " id="monto" style="display: none;">
                    <?php echo $form->field($modelprov, 'credito')->label('MONTO DE CRÉDITO APROBADO <span class="obligatorio">*&nbsp;</span>'); ?>

       </div>
       <?php }
       ?>
        
         <?php 
        if($rol->item_name=='proveedor' && !$modelprov->isNewRecord){?>
           <div class="col-md-4 " id="plazo" style="display: none;">
         
                      <?php echo $form->field($modelprov, 'plazo_pago', [
                    'addon' => ['prepend' => ['content'=>'Días']]
                ])->textInput(['readonly'=> true])->label('PLAZO DE PAGO');  ?>

       </div>
        <?php }else{?>
         <div class="col-md-4 " id="plazo" style="display: none;">
         
                      <?php echo $form->field($modelprov, 'plazo_pago', [
                    'addon' => ['prepend' => ['content'=>'Días']]
                ])->label('PLAZO DE PAGO');  ?>

       </div>
       <?php }
       ?>
     
        
       

    </div>

     


    <br>
    
        <div class="row" style="border-bottom: 1px solid #222d32;">
        <h4>DEFINICIÓN DE SERVICIOS</h4>
    </div>
    <br>
   <?php 
        if($rol->item_name=='proveedor'  && !$modelprov->isNewRecord){?>
           <div class="col-md-4">

            
              <?php 
                $tip_clientes = ['30 dias'=>'30 días', '60 dias'=>'60 días','90 dias'=>'90 días', '3 meses'=>'3 meses', '6 meses'=>'6 meses', '12 meses'=>'12 meses', '2 anos'=>'2 años', '3 años'=>'3 anos'];
                echo $form->field($modelprov, 'garantia')->dropDownList(
                        $tip_clientes,
                        [
                            'prompt'=>'Seleccione...',
                            'class' => 'form-control',
                            'disabled'=>true,
                        ] )->label('GARANTÍA <span class="obligatorio">*&nbsp;</span>');
            ?>

    </div>
        <?php }else{?>


         <div class="col-md-4">

            
              <?php 
                $tip_clientes = ['30 dias'=>'30 días', '60 dias'=>'60 días','90 dias'=>'90 días', '3 meses'=>'3 meses', '6 meses'=>'6 meses', '12 meses'=>'12 meses', '2 anos'=>'2 años', '3 años'=>'3 anos'];
                echo $form->field($modelprov, 'garantia')->dropDownList(
                        $tip_clientes,
                        [
                            'prompt'=>'Seleccione...',
                            'class' => 'form-control',
                        ] )->label('GARANTÍA <span class="obligatorio">*&nbsp;</span>');
            ?>

    </div>
       <?php }
       ?>
    
       <?php 
        if($rol->item_name=='proveedor' && !$modelprov->isNewRecord){?>
           <div class="col-md-4">

            
              <?php 
                $tip_clientes = ['si'=>'Si', 'no'=>'No'];
                echo $form->field($modelprov, 'domicilio')->dropDownList(
                        $tip_clientes,
                        [
                            'prompt'=>'Seleccione...',
                            'class' => 'form-control',
                            'disabled'=>true,
                        ] )->label('ENTREGA A DOMICILIO <span class="obligatorio">*&nbsp;</span>'); ;
            ?>

    </div>
        <?php }else{?>
         <div class="col-md-4">

            
              <?php 
                $tip_clientes = ['si'=>'Si', 'no'=>'No'];
                echo $form->field($modelprov, 'domicilio')->dropDownList(
                        $tip_clientes,
                        [
                            'prompt'=>'Seleccione...',
                            'class' => 'form-control',
                        ] )->label('ENTREGA A DOMICILIO <span class="obligatorio">*&nbsp;</span>'); ;
            ?>

    </div>
       <?php }
       ?>
      <?php 
        if($rol->item_name=='proveedor' && !$modelprov->isNewRecord){?>
            <div class="col-md-4 ">
         
                      <?php echo $form->field($modelprov, 'porcentaje', [
                    'addon' => ['prepend' => ['content'=>'%']]
                ])->textInput(['readonly'=> true])->label('% DE DESCUENTO <span class="obligatorio">*&nbsp;</span>');  ?>

       </div>
        <?php }else{?>
          <div class="col-md-4 ">
         
                      <?php echo $form->field($modelprov, 'porcentaje', [
                    'addon' => ['prepend' => ['content'=>'%']],
                ])->label('% DE DESCUENTO <span class="obligatorio">*&nbsp;</span>');  ?>

       </div>
       <?php }
       ?>
           

    






    <br><br>
    <div class="row" style="border-bottom: 1px solid #222d32;">
        <h4>ASOCIACIÓN DE MARCAS</h4>
    </div>
    <br>



  <!-- _______________________Dinamico____________________________ -->
 


  <?php DynamicFormWidget::begin([
        'widgetContainer' => 'dynamicform_wrapper_resultados',
        'widgetBody' => '.container-items',
        'widgetItem' => '.item',
        'limit' => 50,
        'min' => 1,
        'insertButton' => '.add-item',
        'deleteButton' => '.remove-item',
        'model' => $modelsPiezas[0],
        'formId' => 'dynamic-form',
        'formFields' => [
            'nombre',
            'id_modelo',
            'id_marca',
        ],
  ]); ?>
  <table class="table table-hover">
      <tbody class="container-items">
      <?php foreach ($modelsPiezas as $i => $modelmodel): ?>
          <tr class="item panel panel-default">
              <td>
                  <div class="col-xs-12 col-md-offset-3 col-md-6"><?php
                      $marcas = ArrayHelper::map(Marca::find()->orderBy('nombre')->all(), 'id_marca', 'nombre');
                      echo $form->field($modelmodel, "[{$i}]id_marca")->dropDownList(
                              $marcas,
                              [
                                  'prompt'=>'Selecciona la marca',
                                  'class' => 'form-control',
                                  'onchange'=>'
                                      var ident = $(this).attr("id");
                                      var nom = ident.split("-");
                                      var nmodelo = "modelo-"+nom[1]+"-id_modelo";
                                      $.get( "'.Url::toRoute('caso/listarmodelos').'", { id: $(this).val() } )
                                          .done(function( data ) {
                                              $( "#"+nmodelo ).html( null );
                                              $( "#"+nmodelo ).html( data );
                                          }
                                      );
                                  ',
                              ] )->label('MARCA'); ?>
                      </div>
      

                  <div class="col-xs-12 col-md-1 text-center">
                      <button type="button" class="remove-item btn btn-cancelar btn-xs" style="margin-top:28px;">
                          <i class="glyphicon glyphicon-minus"></i></button>
                  </div>
              </td>
          </tr>
      <?php endforeach; ?>
  <?php DynamicFormWidget::end(); ?>
      </tbody>
  </table>

  <button type="button" class="add-item btn btn-accion  btn-xs pull-right">Agregar marca</button>

 

    <!-- _____________________Fin Dinamico________________________ -->
    

        <br><br>




  <?php
      if(!$modelprov->isNewRecord){
      ?>
          <div class="row" align="center">
              <div>
                  <?= Html::a('CANCELAR', ['view', 'id' => $modelprov->id], ['class' => 'btn btn-cancelar']) ?>
                  &nbsp;&nbsp;&nbsp;
                  <?= Html::submitButton('GUARDAR CAMBIOS', ['class' => 'btn btn-primario']) ?>
              </div>
          </div>
      <?php
      }else{
      ?>
          <div class="form-group" align="center">
              <?= Html::submitButton('GUARDAR', ['class' => 'btn btn-primario']) ?>
          </div>
      <?php
      }
  ?>



    
    <?php ActiveForm::end(); ?>

  </div>





  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <script type="text/javascript">

  $(document).ready(function(){


  /*$(".piezas :input").attr("disabled", true);
  $(".carroceria :input").attr("disabled", true);
  $(".insumos :input").attr("disabled", true);*/

  function initSelect2Loading(a,b){ initS2Loading(a,b); }


  $('#dynamic-form').on('change', '.tipo_articulo', function(event) {

  console.log($(this).val());
  event.preventDefault();

  /* Act on the event */
  });


  });
  </script>
