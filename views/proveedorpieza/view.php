 

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use app\models\modelo;
use app\models\marca;
use app\models\User;
use app\models\AuthAssignment;
use app\models\TipoEmpresa;
use kartik\form\ActiveForm;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $model app\models\Caso */

$this->title = 'DETALLE DEL PROVEEDOR';
?>
<?php 
    $usuario  = User::findOne(['id'=>Yii::$app->user->identity->id]);
    $rol  = AuthAssignment::findOne(['user_id'=>Yii::$app->user->identity->id]);

   ?>
<div class="proveedorpieza-view">


 <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>
         <div class="row">
               <div class="row">
    <div class="col-md-1 col-xs-3">

     
      <h4 class="titulo"><img src="<?= Yii::$app->request->baseUrl ?>/img/prov.png" alt="" width="40" height="40" ></h4>
    </div>
    <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
      <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
    </div>
  </div>
      </div>
 
  

     <!-- --DATOS DE LA EMPRESA-- -->
    <br>

 
<div class="row" style="border-bottom: 1px solid #222d32;">
        <div class="col-md-6">
            <h4>DATOS DEL PROVEEDOR</h4>
        </div>  
    <div class="col-md-6">
        <div class="pull-right">
           <?php 
        if($rol->item_name!='proveedor'){?>
            <?= Html::a('Listado de Proveedores',['proveedor/index'], ['class' => 'btn btn-small-gris']) ?>   
        <?php } ?>
        
                           
        </div> 
    </div>
</div>
<br>
        <!-- fin de cabeza -->

        <div class="row">
            <div class="col-md-4">
                 <?= $form->field($elproveedor, 'tipo_persona')->textInput(['maxlength' => true, 'disable','readonly'=>'true'])?>
           </div>
           <div class="col-md-4">
                 <?= $form->field($elproveedor, 'identificacion')->textInput(['maxlength' => true,'readonly'=>'true']) ?>
           </div>
           <div class="col-md-4">
                 <?= $form->field($empresatipo, 'nombre')->textInput(['maxlength' => true,'readonly'=>'true']) ?>
           </div>
        </div>


        <div class="row">
            <div class="col-md-8">
                 <?= $form->field($elproveedor, 'nombre')->textInput(['maxlength' => true,'readonly'=>'true'])?>
            </div>
           <div class="col-md-4">
                 <?= $form->field($elproveedor, 'telefono')->textInput(['maxlength' => true,'readonly'=>'true']) ?>
           </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                 <?= $form->field($elproveedor, 'direccion')->textInput(['maxlength' => true, 'readonly'=>'true'])?>
           </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                 <?= $form->field($elproveedor, 'descripcion')->textInput(['maxlength' => true, 'readonly'=>'true'])?>
           </div>
        </div>

    <br>
    <div class="row" style="border-bottom: 1px solid #222d32;">
        <h4>DATOS DEL REPRESENTANTE LEGAL</h4>
    </div>
    <br>

         <div class="row">
            <div class="col-md-4">
                 <?= $form->field($elproveedor, 'nombre_representante')->textInput(['maxlength' => true,'readonly'=>'true'])?>
           </div>
           <div class="col-md-4">
                 <?= $form->field($elproveedor, 'identificacion_representante')->textInput(['maxlength' => true,'readonly'=>'true']) ?>
           </div>
           <div class="col-md-4">
                 <?= $form->field($elproveedor, 'puesto_representante')->textInput(['maxlength' => true,'readonly'=>'true']) ?>
           </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                 <?= $form->field($elproveedor, 'correo_representante')->textInput(['maxlength' => true, 'disable','readonly'=>'true'])?>
           </div>
           <div class="col-md-4">
                 <?= $form->field($elproveedor, 'celular_representante')->textInput(['maxlength' => true,'readonly'=>'true']) ?>
<!--  --> 

           </div>
           <div class="col-md-4">
                 <?= $form->field($elproveedor, 'telefono_representante')->textInput(['maxlength' => true,'readonly'=>'true']) ?>
           </div>

        </div>

         <br>
    <div class="row" style="border-bottom: 1px solid #222d32;">
        <h4>DEFINICIÓN DE PAGOS</h4>
    </div>
    <br>

      <div class="row">
            <div class="col-md-4">
                 <?= $form->field($elproveedor, 'Tipo_pago')->textInput(['maxlength' => true,'readonly'=>'true'])?>
           </div>
           <div class="col-md-4">
                 <?= $form->field($elproveedor, 'credito')->textInput(['maxlength' => true,'readonly'=>'true']) ?>
           </div>
           <div class="col-md-4">
                 <?= $form->field($elproveedor, 'plazo_pago')->textInput(['maxlength' => true,'readonly'=>'true']) ?>
           </div>
    </div>

          <br>
    <div class="row" style="border-bottom: 1px solid #222d32;">
        <h4>DEFINICIÓN DE SERVICIOS</h4>
    </div>
    <br>

      <div class="row">
            <div class="col-md-4">
                 <?= $form->field($elproveedor, 'garantia')->textInput(['maxlength' => true,'readonly'=>'true'])?>
           </div>
           <div class="col-md-4">
                 <?= $form->field($elproveedor, 'domicilio')->textInput(['maxlength' => true,'readonly'=>'true']) ?>
           </div>
           <div class="col-md-4">
                 <?= $form->field($elproveedor, 'porcentaje')->textInput(['maxlength' => true,'readonly'=>'true']) ?>
           </div>
    </div>

           <br>
    <div class="row" style="border-bottom: 1px solid #222d32;">
        <h4>ASOCIACIÓN DE MARCAS</h4>
    </div>
    <br>
 
 

   <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary' => '',
        'columns' => [
      

            
                 [
                         
                        'label' => 'Marcas',
                        'format' => 'raw',
                        'value' => function( $model ){
                             
                            return $model->idMarca->nombre;
                            
                        },
                             
                 
                    ],
            
           
        ],
    ]); ?>





</div>
