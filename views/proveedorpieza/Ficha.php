 

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use app\models\modelo;
use app\models\marca;
use app\models\TipoEmpresa;
use kartik\form\ActiveForm;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $model app\models\Caso */

$this->title = 'Ficha de '.$elproveedor->nombre;
 
?>
<div class="proveedorpieza-view">


 <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>
     
   <div class="row">
    <div class="col-md-1 col-xs-3">
      <h4 class="titulo"><img src="<?= Yii::$app->request->baseUrl ?>/img/prov.png" alt="" width="40" height="40" ></h4>
    </div>
    <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
      <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
    </div>
  </div>

     <!-- --DATOS DE LA EMPRESA-- -->
    <br>

 
<div class="row" style="border-bottom: 1px solid #222d32;">
        <div class="col-md-6">
            <h4>DATOS DEL PROVEEDOR</h4>
        </div>  
    <div class="col-md-6">
        <div class="pull-right">
                <?= Html::a('Listado de Proveedores',['proveedor/index'], ['class' => 'btn btn-small-gris']) ?>           
        </div> 
    </div>
</div>
<br>
        <!-- fin de cabeza -->

        <div class="row">
            <div class="col-md-8">
                 <?= $form->field($elproveedor, 'nombre')->textInput(['maxlength' => true,'readonly'=>'true'])?>
            </div>
            <div class="col-md-4">
                 <?= $form->field($elproveedor, 'identificacion')->textInput(['maxlength' => true,'readonly'=>'true']) ?>
           </div>
        </div>


        <div class="row">
             <div class="col-md-8">
                 <?= $form->field($elproveedor, 'direccion')->textInput(['maxlength' => true, 'readonly'=>'true'])?>
           </div>

           <div class="col-md-4">
                 <?= $form->field($elproveedor, 'telefono')->textInput(['maxlength' => true,'readonly'=>'true']) ?>
           </div>
        </div>
 
 
         <div class="row">
            <div class="col-md-4">
                 <?= $form->field($elproveedor, 'nombre_representante')->textInput(['maxlength' => true,'readonly'=>'true'])?>
           </div>
                <div class="col-md-4">
                 <?= $form->field($elproveedor, 'credito')->textInput(['maxlength' => true,'readonly'=>'true']) ?>
           </div>
           <div class="col-md-2">
            <label style="margin-left: 15%">PUNTUACIÓN GRAL</label>
            <br><br>
            <div style="text-align:center">

            <span class="label label-oval" style="background-color:green;padding-top:4%;padding-bottom:4%;font-size:12px;"> 67% </span>
            </div>





           </div>

           <div class="col-md-2">
            <label style="margin-left: 20%">COMPETENCIA</label>
            <br><br>
            <div style="text-align:center">
            <span class="label label-oval" style="background-color:#0070C0;padding-top:4%;padding-bottom:4%; font-size:12px;"> 78% </span>
            </div> 
           </div> 
        </div>

      
 
     
 
 





</div>
