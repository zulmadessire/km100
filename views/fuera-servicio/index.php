<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FueraServiciosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Fuera Servicios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fuera-servicio-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Fuera Servicio', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'causa',
            'observacion',
            'id_orden_compra',
            'id_auditoria',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
