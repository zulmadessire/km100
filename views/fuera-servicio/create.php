<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FueraServicio */

$this->title = 'Create Fuera Servicio';
$this->params['breadcrumbs'][] = ['label' => 'Fuera Servicios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fuera-servicio-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
