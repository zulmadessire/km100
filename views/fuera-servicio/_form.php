<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FueraServicio */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fuera-servicio-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'causa')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'observacion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_orden_compra')->textInput() ?>

    <?= $form->field($model, 'id_auditoria')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
