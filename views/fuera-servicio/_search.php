<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FueraServiciosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fuera-servicio-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'causa') ?>

    <?= $form->field($model, 'observacion') ?>

    <?= $form->field($model, 'id_orden_compra') ?>

    <?= $form->field($model, 'id_auditoria') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
