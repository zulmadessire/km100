<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Auditoriamnto */

$this->title = 'Create Auditoriamnto';
$this->params['breadcrumbs'][] = ['label' => 'Auditoriamntos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auditoriamnto-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
