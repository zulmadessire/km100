<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Auditoriamnto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="auditoriamnto-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'desviacion')->textInput() ?>

    <?= $form->field($model, 'mntos_realizados')->textInput() ?>

    <?= $form->field($model, 'mntos_smna_anterior')->textInput() ?>

    <?= $form->field($model, 'fecha')->textInput() ?>

    <?= $form->field($model, 'observacion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'estado')->textInput() ?>

    <?= $form->field($model, 'id_estacion')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
