<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AuditoriamntoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Auditoriamntos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auditoriamnto-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Auditoriamnto', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'desviacion',
            'mntos_realizados',
            'mntos_smna_anterior',
            'fecha',
            // 'observacion',
            // 'estado',
            // 'id_estacion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
