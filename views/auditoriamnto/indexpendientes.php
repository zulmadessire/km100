<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AuditoriasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'AUDITORIAS MENSUALES PENDIENTES';
?>
<div class="auditoria-index">
      <div class="row">
    <div class="col-md-1 col-xs-3">
      <h4 class="titulo"><i class="fa fa-list-ul fa-4x" aria-hidden="true"></i></h4>
    </div>
    <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
      <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
    </div>
 
  </div>

 </br>
  <div class="row">
      <?php
          echo Html::a('Historico de Auditorias', ['create'], ['class' => 'btn btn-accion', 'style' => 'float:right;']);
      ?>
  </div>

 


  <br>
 
    
</div>
