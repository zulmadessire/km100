<?php

use yii\helpers\Html;
use app\models\Modelo;
use app\models\TipoMantenimiento;
use yii\grid\GridView;
/*use yii\widgets\ActiveForm;*/
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\models\ProyeccionMantenimiento;
use kartik\form\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Vehiculo */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Registro de auditoría - MANTENIMIENTO';
//$this->params['breadcrumbs'][] = $this->title;
 
$fecha_actual = date('Y-m-d');
$lunes = date('Y-m-d', strtotime('last Monday'));
$lunes_anterior = date('Y-m-d', strtotime('last Monday -7 days'));
 
$consulta_proyeccion_anterior = ProyeccionMantenimiento::find()->where([ 'id_estacion_reporte' => $model_est->id_estacion, 'realizado'=>0])->andWhere(['between', 'fecha_reporte', $lunes_anterior, $lunes ])->all();
 
  $acum =(count($consulta_proyeccion_anterior));
 
 
?>
<div class="Audiorias">

  <div class="row">
    <div class="col-md-1 col-xs-3">
      <h4 class="titulo"><i class="fa fa-list-ul fa-4x" aria-hidden="true"></i></h4>
    </div>
    <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
      <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
    </div>
  </div>
 
    <br>
    
    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DEL CLIENTE</p>
            <hr>
        </div>
    </div>

    <div class="row">
       <?php $form = ActiveForm::begin(['id' => 'dynamic_lista-form']); ?>
        
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_est, 'empresa')->textInput(['disabled' => true, 'value' => $model_est->idEmpresa->nombre])->label('Empresa') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'nombre')->textInput(['value' => strtoupper($model_est->nombre), 'disabled' => true])->label('Estación') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'fecha')->textInput(['value' => date('Y-m-d'),'maxlength' => true, 'disabled' => true]) ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'mntos_smna_anterior')->textInput(['value' => $acum,'maxlength' => true, 'disabled' => true])->label('PROYECCIÓN DE MTTO. SEMANA ANTERIOR') ?>
            <?= $form->field($model, 'mntos_smna_anterior')->hiddenInput(['value' => $acum,'maxlength' => true ])->label(false) ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'mntos_realizados')->textInput(['maxlength' => true, 'disabled' => false])->label('MTTOS. REALIZADOS') ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'desviacion')->textInput(['disabled' => true,'maxlength' => true ])->label('DESVIACIÓN') ?>
            <?= $form->field($model, 'desviacion')->hiddenInput(['maxlength' => true ])->label(false) ?>

        </div>
              <?= $form->field($model, 'id_estacion')->hiddenInput(['value' => $model_est->id_estacion])->label(false); ?>
       <div class="col-xs-12">
            <p class="titulo-form">VEHÍCULOS A LOS QUE NO SE LES REGISTRO EL MTTO. PROYECTADO</p>
            <hr>
        </div>
     
           
  <!-- =================== AQUI VA EL GRIDVIEW =====================  -->



 <?= GridView::widget([
                'dataProvider' => $dataProvider,
     
                'tableOptions' => ['class' => 'grid-blue table table-responsive table-bordered'],
                'summary' => '',
       
                'options' =>[
                    'class' => '',
                ],
                'columns' => [
               
                          [
 
                            'label' => 'Ficha/MVA',
                            'format' => 'text',
                            'value' => function( $model ){
                                return $model->mva;
                            },
                                
                       

                            ],

                          [
 
                            'label' => 'Modelo',
                            'format' => 'text',
                            'value' => function( $model ){
                              return $model->idModelo->nombre;
                         },
                       

                            ],

                          [
 
                            'label' => 'KM Actual',
                            'format' => 'text',
                            'value' => function( $model ){
                               return $model->km;
                            },
                                
                       

                            ],

                            [
                        'label' => 'Observación',
                        'format' => 'raw',
                        'filter' => false,
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:center;'],
                         'value' => function( $model ){
                            

                 
                                $vector_piezas[] = Html::input('text', 'observacion[]', '', ['class'=> 'form-control causa', 'style' => 'font-size: 10pt; text-align: center;', 'disabled' => false, 'data' => '']);
        
                            $respuesta_piezas = implode('<br>', $vector_piezas);

                            return $respuesta_piezas;



                        },
                    ],

                     

                  
                ],
              

                
            ]); ?>

  <!-- ===========================AQUI VA EL DINAMICO UNO============== -->


 <div class="col-xs-12">
            <p class="titulo-form">UNIDADES FUERA DE SERVICIO</p>
            <hr>
        </div>
             <div class="row">
  
      <?php DynamicFormWidget::begin([
        'widgetContainer' => 'dynamicform_wrapper_resultados',
        'widgetBody' => '.container-items',
        'widgetItem' => '.item',
        'limit' => 50,
        'min' => 1,
        'insertButton' => '.add-item',
        'deleteButton' => '.remove-item',
        'model' => $modelsSolicitudes[0],
        'formId' => 'dynamic_lista-form',
        'formFields' => [
            'nombre',
            
        ],
  ]); ?>
  <table class="table table-hover">
      <tbody class="container-items">
      <?php foreach ($modelsSolicitudes as $i => $modelmodel): ?>
          <tr class="item panel panel-default">
              <td>
              
                   <div class="col-md-12 col-xs-12">
  
   <div class="col-xs-12 col-md-1"><?= $form->field($modelmodel, "[{$i}]ficha_mva")->textInput()->label('Ficha/MVA'); ?></div>
   <div class="col-xs-12 col-md-2"> 
 <?php $modelos = ArrayHelper::map(Modelo::find()->orderBy('nombre')->all(), 'id_modelo', 'nombre');
                      echo $form->field($modelmodel, "[{$i}]id_modelo")->dropDownList(
                              $modelos,
                              [
                                  'prompt'=>'Selecciona el modelo',
                                  'class' => 'form-control',
                                  
                              ] )->label('MODELO'); ?> </div>
  <div class="col-xs-12 col-md-2"><?= $form->field($modelmodel, "[{$i}]km_actual")->textInput()->label('Km. Actual'); ?></div>
 <div class="col-xs-12 col-md-2"> 
 <?php $modelos = ArrayHelper::map(TipoMantenimiento::find()->orderBy('nombre')->all(), 'id_tipo_mnto', 'nombre');
                      echo $form->field($modelmodel, "[{$i}]id_tipo_mnto")->dropDownList(
                              $modelos,
                              [ 
                                  'prompt'=>'Selecciona la marca',
                                  'class' => 'form-control',
                                  
                              ] )->label('TIPO DE MANTENIMIENTO'); ?> </div>
   
  <div class="col-xs-12 col-md-2"><?= $form->field($modelmodel, "[{$i}]observacion")->textInput()->label('Observaciones'); ?></div>
   <div class="col-xs-12 col-md-1 text-center">
                      <button type="button" class="remove-item btn btn-cancelar btn-xs" style="margin-top:28px;">
                          <i class="glyphicon glyphicon-minus"></i></button>
                  </div>
        </div>
              
           

                 
              </td>
          </tr>
      <?php endforeach; ?>
  <?php DynamicFormWidget::end(); ?>
      </tbody>
  </table>

  <button type="button" class="add-item btn btn-accion  btn-xs pull-right">Agregar Servicio</button>
  <br>
  <br>
  <!-- ===========================AQUI VA EL DINAMICO DOS============== -->


 <div class="col-xs-12">
            <p class="titulo-form">OTRAS SOLICITUDES</p>
            <hr>
        </div>
             <div class="row">
  
      <?php DynamicFormWidget::begin([
        'widgetContainer' => 'dynamicform_wrapper_resultados',
        'widgetBody' => '.container-items2',
        'widgetItem' => '.item2',
        'limit' => 50,
        'min' => 1,
        'insertButton' => '.add-item2',
        'deleteButton' => '.remove-item',
        'model' => $modelsotras[0],
        'formId' => 'dynamic_lista-form',
        'formFields' => [
            'nombre',
            
        ],
  ]); ?>
  <table class="table table-hover">
      <tbody class="container-items2">
      <?php foreach ($modelsotras as $i => $modelmodel): ?>
          <tr class="item2 panel panel-default">
              <td>
              
                   <div class="col-md-12 col-xs-12">
  
   <div class="col-xs-12 col-md-2"><?= $form->field($modelmodel, "[{$i}]ficha_mva")->textInput()->label('Ficha/MVA'); ?></div>
   <div class="col-xs-12 col-md-2"><?= $form->field($modelmodel, "[{$i}]km_actual")->textInput()->label('Km Actual'); ?></div>
   <div class="col-xs-12 col-md-2"><?= $form->field($modelmodel, "[{$i}]km_proximo")->textInput()->label('Km Próximo'); ?></div>
   
  <div class="col-xs-12 col-md-3"><?= $form->field($modelmodel, "[{$i}]observacion")->textInput()->label('Observaciones'); ?></div>
   <div class="col-xs-12 col-md-1 text-center">
                      <button type="button" class="remove-item btn btn-cancelar btn-xs" style="margin-top:28px;">
                          <i class="glyphicon glyphicon-minus"></i></button>
                  </div>
        </div>
              
           

                 
              </td>
          </tr>
      <?php endforeach; ?>
  <?php DynamicFormWidget::end(); ?>
      </tbody>
  </table>

  <button type="button" class="add-item2 btn btn-accion  btn-xs pull-right">Agregar Solicitud</button>
  <br>
  <br>
    <div class="col-md-12 col-xs-12">
            <?= $form->field($model, 'comentario')->textInput(['disabled' => false])->textarea(['rows' => '6']) ->label('Observación') ?>
        </div>
        <div class="col-md-12 col-xs-12">
        <div class="form-group text-center">  
<?= Html::submitButton($model->isNewRecord ? 'GUARDAR' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-submit' : 'btn btn-primary']) ?>
        </div>
    </div>
        <?php ActiveForm::end(); ?>
<script type="text/javascript">

  $(document.body).on('blur','#auditoriamnto-mntos_realizados', function () {
      var registrado= $('#auditoriamnto-mntos_smna_anterior').val();
      var existencia= $('#auditoriamnto-mntos_realizados').val();
      $('#auditoriamnto-desviacion').val((registrado-existencia));
      $('#auditoriamnto-desviacion').val((registrado-existencia));
    });


</script>