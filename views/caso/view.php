<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Soporte;

/* @var $this yii\web\View */
/* @var $model app\models\Caso */

$this->title = 'INFORMACIÓN DE CASO NÚMERO '.$model->id_caso;
//$this->params['breadcrumbs'][] = ['label' => 'CASOS REGISTRADOS', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="caso-view">

    <div class="row">
        <div class="col-xs-12 col-md-6">
          <div class="row">
            <div class="col-md-2 col-xs-3">
                <h4 class="titulo"><i class="fa fa-clipboard fa-4x" aria-hidden="true"></i></h4>
            </div>
            <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
                <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-md-6">
        <ul class="nav nav-pills pull-right">
              <li role="presentation">
                    <?php
                    if(\Yii::$app->user->can('admin') || \Yii::$app->user->can('modificar-caso')){
                        echo Html::a('MODIFICAR CASO', ['update', 'id' => $model->id_caso], ['class' => 'btn btn-small-gris',]);
                    }
                    ?>&nbsp;&nbsp;&nbsp;
              </li>
              <li role="presentation">
              <?php
              echo Html::a('LISTADO DE CASOS', ['index'], ['class' => 'btn btn-small-gris',]);
              echo "<br><br>";
              $tipo_caso = [0 => 'Accidente', 1 => 'Incidente', 2 => 'Falla mecánica', 3 => 'Aire acondicionado'];
              $fuentes_acc = [1 => 'Caso interno o empleado', 0 => 'Cliente final'];
              $tip_clientes = [2=>'Asegurado', 1=>'Corporativo', 3=>'Leasing', 0=>'Retail'];
              ?>
              </li>
            </ul>
        </div>
    </div>
    <br>

    <table class ="tablacaso" border="1">
            <tr><th style="width: 30%;">Fecha de Registro: </th><td><?= $model->fecha_registro ?></td></tr>
            <tr><th>Registrado por Usuario: </th>               <td><?= '<strong>'.$reg_por->username.'</strong> ( '.$reg_por->email.' )' ?></td></tr>
            <tr><th>Cliente: </th>                              <td><?= $estacion->idEmpresa->cliente->nombre_comercial ?></td></tr>
            <tr><th>Empresa: </th>                              <td><?= $estacion->idEmpresa->nombre ?></td></tr>
            <tr><th>Estación: </th>                             <td><?= $estacion->nombre ?></td></tr>
            <tr><th>Marca: </th>                                <td><?= $marca->nombre ?></td></tr>
            <tr><th>Modelo: </th>                               <td><?= $modelo->nombre ?></td></tr>
            <tr><th>Ficha o MVA: </th>                          <td><?= $model->ficha ?></td></tr>
            <tr><th>Número de Placa: </th>                      <td><?= $model->placa ?></td></tr>
            <tr><th>Tipo de Caso: </th>                         <td><?= $tipo_caso[$model->tipo_caso] ?></td></tr>
            <tr><th>Descripción: </th>                          <td><?= $model->descripcion ?></td></tr>

<?php if($model->tipo_caso == 0 || $model->tipo_caso == 1){ ?>
            <tr><th>Número de Reporte de AMET: </th>            <td><?= $model->amet ?></td></tr>
            <tr><th>Fecha de Accidente o Incidente: </th>       <td><?= $model->fecha_acc_inc ?></td></tr>
            <tr><th>Tipo de Accidente: </th>                    <td><?= $tipo_acc->nombre ?></td></tr>
            <tr><th>Dónde se encuentra la Unidad: </th>         <td><?= $ubicacion->nombre ?></td></tr>
    <?php if($model->tipo_caso == 0){ ?>
            <tr><th>Fuente del Accidente: </th>                 <td><?= $fuentes_acc[$model->fuente_accidente] ?></td></tr>
        <?php if($model->fuente_accidente == 0){ ?>
            <tr><th>Número de Contrato: </th>                   <td><?= $model->num_contrato ?></td></tr>
            <tr><th>Tipo de Cliente: </th>                      <td><?= $tip_clientes[$model->tipo_cliente] ?></td></tr>
            <tr><th>Tomo Cobertura CDW: </th>                   <td><?= ($model->cobertura_cdw == 1) ? 'SI' : 'NO' ?></td></tr>
            <tr><th>Número de CLAIM: </th>                      <td><?= $model->claim ?></td></tr>
            <tr><th>Contrato con Deducible: </th>               <td><?= ($model->deducible == 1) ? 'SI' : 'NO' ?></td></tr>
            <tr><th>Monto de Deducible: </th>                   <td><?= $model->monto_deducible ?></td></tr>
            <tr><th>Requiere Cotización: </th>                  <td><?= ($model->req_cotizacion == 1) ? 'SI' : 'NO' ?></td></tr>
            <?php if($model->req_cotizacion == 1){ ?>
            <tr><th>Tarifa Base: </th>                          <td><?= $model->tarifa_base ?></td></tr>
            <tr><th>Piezas a Cotizar: </th>                     <td><?= $model->piezas ?></td></tr>
            <?php } ?>
        <?php }else{ ?>
            <tr><th>Nombre del Empleado: </th>                  <td><?= $model->nombre_empleado ?></td></tr>
        <?php } ?>
    <?php } ?>
<?php } ?>
            <?php
                if(isset($cobro->monto)){
            ?>
            <tr><th colspan="2" style="text-align:center;">IMFORMACIÓN DE COBRO</th></tr>
            <tr><th>Monto Cobrado: </th> <td><?= $cobro->monto ?></td></tr>
            <tr><th>Forma de Pago: </th> <td><?= $cobro->forma_pago ?></td></tr>
            <tr><th>Motivo de Cobro: </th> <td><?= $cobro->motivo_cobro ?></td></tr>
            <tr><th>Número de factura asociada: </th> <td><?= $cobro->numero_factura ?></td></tr>
            <tr><th>Fecha de Cobro: </th> <td><?= $cobro->fecha ?></td></tr>
            <?php
            }
            ?>

            <tr><th colspan="2" style="text-align:center;">IM&Aacute;GENES DE SOPORTE</th></tr>
            <tr><th>Im&aacute;gen Frontal: </th>     <td style="text-align:center;">
                                                <?php $sop = Soporte::find()->where([ 'id_caso' => $model->id_caso, 'bloque' => 0])->One();
                                                    if($sop){
                                                        ?>
                                                        <a href=<?= Yii::$app->request->baseUrl.'/'.$sop->ubicacion ?> data-lightbox=<?= Yii::$app->request->baseUrl.'/'.$sop->ubicacion ?>>
                                                        <img src=<?= Yii::$app->request->baseUrl.'/'.$sop->ubicacion ?> style="max-width:60%;" class=<?= Yii::$app->request->baseUrl.'/'.$sop->ubicacion ?> />
                                                        </a>
                                                        <?php
                                                    }else{
                                                        echo '<span style="color:#999;">No aplica<span>';
                                                    }
                                                ?>
                                            </td></tr>
            <tr><th>Im&aacute;gen Lateral Izquierdo: </th>     <td style="text-align:center;">
                                                <?php $sop = Soporte::find()->where([ 'id_caso' => $model->id_caso, 'bloque' => 1])->One();
                                                    if($sop){
                                                        ?>
                                                        <a href=<?= Yii::$app->request->baseUrl.'/'.$sop->ubicacion ?> data-lightbox=<?= Yii::$app->request->baseUrl.'/'.$sop->ubicacion ?>>
                                                        <img src=<?= Yii::$app->request->baseUrl.'/'.$sop->ubicacion ?> style="max-width:60%;" class=<?= Yii::$app->request->baseUrl.'/'.$sop->ubicacion ?> />
                                                        </a>
                                                        <?php
                                                    }else{
                                                        echo '<span style="color:#999;">No aplica<span>';
                                                    }
                                                ?>
                                            </td></tr>
            <tr><th>Im&aacute;gen Lateral Derecho: </th>     <td style="text-align:center;">
                                                <?php $sop = Soporte::find()->where([ 'id_caso' => $model->id_caso, 'bloque' => 2])->One();
                                                    if($sop){
                                                        ?>
                                                        <a href=<?= Yii::$app->request->baseUrl.'/'.$sop->ubicacion ?> data-lightbox=<?= Yii::$app->request->baseUrl.'/'.$sop->ubicacion ?>>
                                                        <img src=<?= Yii::$app->request->baseUrl.'/'.$sop->ubicacion ?> style="max-width:60%;" class=<?= Yii::$app->request->baseUrl.'/'.$sop->ubicacion ?> />
                                                        </a>
                                                        <?php
                                                    }else{
                                                        echo '<span style="color:#999;">No aplica<span>';
                                                    }
                                                ?>
                                            </td></tr>
            <tr><th>Im&aacute;gen Trasero: </th>     <td style="text-align:center;">
                                                <?php $sop = Soporte::find()->where([ 'id_caso' => $model->id_caso, 'bloque' => 3])->One();
                                                    if($sop){
                                                        ?>
                                                        <a href=<?= Yii::$app->request->baseUrl.'/'.$sop->ubicacion ?> data-lightbox=<?= Yii::$app->request->baseUrl.'/'.$sop->ubicacion ?>>
                                                        <img src=<?= Yii::$app->request->baseUrl.'/'.$sop->ubicacion ?> style="max-width:60%;" class=<?= Yii::$app->request->baseUrl.'/'.$sop->ubicacion ?> />
                                                        </a>
                                                        <?php
                                                    }else{
                                                        echo '<span style="color:#999;">No aplica<span>';
                                                    }
                                                ?>
                                            </td></tr>
            <tr><th>Im&aacute;gen Daño: </th>     <td style="text-align:center;">
                                                <?php $sop = Soporte::find()->where([ 'id_caso' => $model->id_caso, 'bloque' => 4])->One();
                                                    if($sop){
                                                        ?>
                                                        <a href=<?= Yii::$app->request->baseUrl.'/'.$sop->ubicacion ?> data-lightbox=<?= Yii::$app->request->baseUrl.'/'.$sop->ubicacion ?>>
                                                        <img src=<?= Yii::$app->request->baseUrl.'/'.$sop->ubicacion ?> style="max-width:60%;" class=<?= Yii::$app->request->baseUrl.'/'.$sop->ubicacion ?> />
                                                        </a>
                                                        <?php
                                                    }else{
                                                        echo '<span style="color:#999;">No aplica<span>';
                                                    }
                                                ?>
                                            </td></tr>
            <tr><th>Im&aacute;gen Placa: </th>     <td style="text-align:center;">
                                                <?php $sop = Soporte::find()->where([ 'id_caso' => $model->id_caso, 'bloque' => 5])->One();
                                                    if($sop){
                                                        ?>
                                                        <a href=<?= Yii::$app->request->baseUrl.'/'.$sop->ubicacion ?> data-lightbox=<?= Yii::$app->request->baseUrl.'/'.$sop->ubicacion ?>>
                                                        <img src=<?= Yii::$app->request->baseUrl.'/'.$sop->ubicacion ?> style="max-width:60%;" class=<?= Yii::$app->request->baseUrl.'/'.$sop->ubicacion ?> />
                                                        </a>
                                                        <?php
                                                    }else{
                                                        echo '<span style="color:#999;">No aplica<span>';
                                                    }
                                                ?>
            <tr><th>Im&aacute;genes Adicionales: </th>     <td style="text-align:center;">
                                                <?php $sop = Soporte::find()->where([ 'id_caso' => $model->id_caso, 'bloque' => 6])->All();
                                                    if($sop){
                                                        foreach ($sop as $s) {
                                                         ?>
                                                         <div class="row">
                                                            <a href=<?= Yii::$app->request->baseUrl.$s->ubicacion ?> data-lightbox="img-adicionales">
                                                                <img src=<?= Yii::$app->request->baseUrl.$s->ubicacion ?> style="max-width:60%;" class="img-adicionales" />
                                                            </a>
                                                         </div>
                                                         <?php
                                                        }
                                                    }else{
                                                        echo '<span style="color:#999;">No hay Soportes Adicionales<span>';
                                                    }
                                                ?>
                                            </td></tr>
    </table><br>
</div>
