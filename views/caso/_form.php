<?php

use yii\helpers\Html;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;
use kartik\file\FileInput;

use app\models\Empresa;
use app\models\Estacion;
use app\models\Marca;
use app\models\Modelo;
use app\models\Caso;
use app\models\Tipoaccidente;
use app\models\Ubicacionunidad;
use app\models\Cliente;
use app\models\Vehiculo;
use kartik\select2\Select2;
use yii\web\View;
use kartik\depdrop\DepDrop;
use yii\web\JsExpression;
/* @var $this yii\web\View */
/* @var $model app\models\Caso */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="caso-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <!-- --DATOS DE LA EMPRESA-- -->
    <br>
    <div class="row" style="border-bottom: 1px solid #222d32;">
        <h4>DATOS DE LA EMPRESA</h4>
    </div>
    <br>
    <div class="row">
        <!--<div class="col-xs-12 col-md-6">
            <p><strong>Cliente </strong><br><h4><?php //$empresa->nombre ?><h4></p>

        </div>-->
        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <label class="control-label" for="id_estado">CLIENTE <span class="obligatorio">*&nbsp;</span></label>
                <?php if (\Yii::$app->user->can('registro casos todos') || \Yii::$app->user->can('modificar caso') ): ?>
                    <?=  Select2::widget([
                            'name' => 'Caso[id_cliente]',
                            'value' => ($model->id_estacion)?$model->idEstacion->idEmpresa->cliente->id:'',
                            'data' => ArrayHelper::map(Cliente::find()->where('id != 6')->orderBy('nombre_comercial')->all(), 'id', 'nombre_comercial'),
                            'options' => ['placeholder' => 'Selecciona el cliente', 'id' => 'id_cliente', 'initialize' => true,]
                        ]); 
                    ?>
                <?php else: ?>
                    <?= Html::textInput('Caso[id_cliente]', $cliente->nombre_comercial, ['class' => 'form-control', 'id' => 'id_cliente', 'disabled' => true,]) ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <label class="control-label" for="id_estado">EMPRESA <span class="obligatorio">*&nbsp;</span></label>
            <?php if (\Yii::$app->user->can('registro casos todos') || \Yii::$app->user->can('modificar caso') ){ ?>
                <?= DepDrop::widget([
                    'name' => 'Caso[id_empresa]',
                    //'value' => ArrayHelper::map( $empresa, 'id_empresa', 'nombre' ),
                    'data' => ($model->id_estacion)?ArrayHelper::map( Empresa::find()->where(['id_empresa'=>$model->idEstacion->id_empresa])->all(), 'id_empresa', 'nombre' ):[''=>''],
                    'type'=>DepDrop::TYPE_SELECT2,
                    'options' => ['id'=>'id_empresa'],
                    'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                    'pluginOptions' => [
                       'depends'  => ['id_cliente'],
                       'placeholder' => 'Selecciona la empresa',
                       'url' => Url::to(['/caso/getempresas']),
                       'initialize' => true,
                    ]
                ]);  ?>
            <?php } elseif (\Yii::$app->user->can('registro casos cliente')) { ?>
                <?=  
                    Select2::widget([
                            'name' => 'Caso[id_empresa]',
                            'value' => ($model->id_estacion)?$model->idEstacion->id_empresa:'',
                            'data' => ArrayHelper::map(Empresa::find()->where('cliente_id = ' . $cliente->id )->orderBy('nombre')->all(), 'id_empresa', 'nombre'),
                            'options' => ['placeholder' => 'Selecciona la empresa', 'id' => 'id_empresa', 'initialize' => true,]
                        ]); 
                    ?>
            <?php } elseif (\Yii::$app->user->can('registro casos estaciones asociadas')) { ?>
                <?= Html::textInput('Caso[id_empresa]', $empresa->nombre, ['class' => 'form-control', 'id' => 'id_empresa', 'disabled' => true,]) ?>
            <?php } ?>
            
        </div>
        <div class="col-md-4 col-xs-12">
            <?php if (\Yii::$app->user->can('registro casos estaciones asociadas')) { ?>
                <?=
                    $form->field($model, 'id_estacion')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(Estacion::find()->where(['id_estacion' => $estaciones ])->all(), 'id_estacion', 'nombre'),
                        //'value' => ($model->id_estacion)?$model->id_estacion:'',
                        'options' => ['placeholder' => 'Selecciona la estacion', 'id' => 'id_estacion'],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ])->label('ESTACIÓN <span class="obligatorio">*&nbsp;</span>');
                ?>
            <?php } else { ?>
                <?=
                    $form->field($model, 'id_estacion')->widget(DepDrop::classname(), [
                        'type'=>DepDrop::TYPE_SELECT2,
                        'data' => ($model->id_estacion)?ArrayHelper::map( Estacion::find()->where(['id_estacion'=>$model->id_estacion])->all(), 'id_estacion', 'nombre'):[''=>''],
                        'options'=>['id'=>'id_estacion', 'class' => 'form-control'],
                        'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                        'pluginOptions'=>[
                            'depends'=>['id_empresa'],
                            'placeholder'=>'Selecciona la estación',
                            'url'=>Url::to(['/caso/getestaciones']),
                            'initialize' => true,
                        ]
                    ])->label('ESTACIÓN <span class="obligatorio">*&nbsp;</span>');
                ?>
            <?php } ?>
        </div>
    </div>


    <!-- --DATOS DEL VEHÍCULO-- -->
    <br>
    <div class="row" style="border-bottom: 1px solid #222d32;">
        <h4>DATOS DEL VEHÍCULO</h4>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <?=
                $form->field($model, 'ficha')->widget(Select2::classname(), [
                    'data' => $vehiculos,
                    'options' => ['placeholder' => 'Selecciona el MVA', 'id' => 'ficha',],                    
                    'pluginOptions' => [
                        'allowClear' => true,                       
                    ],
                    'pluginEvents' => [                        
                        'change'=> new JsExpression('function(params) {setTimeout(function(){ $("#placa").trigger("change"); }, 3000);  }'),
                    ],
                ])->label('MVA O FICHA <span class="obligatorio">*&nbsp;</span>');
            ?>
        </div>
        <div class="col-xs-12 col-md-6">
            <?=
                $form->field($model, 'placa')->widget(DepDrop::classname(), [
                    'type'=>DepDrop::TYPE_SELECT2,
                    'options'=>['id'=>'placa', 'class' => 'form-control'],
                    'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                    'pluginOptions'=>[
                        'depends'=>['ficha'],
                        'placeholder'=>'Selecciona la placa',
                        'url'=>Url::to(['/caso/getplacas']),
                        'initialize' => true,
                    ]
                ])->label('PLACA <span class="obligatorio">*&nbsp;</span>');
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <div class="form-group">
                <?php 
                    $modeloCarro ='';
                    $marcaCarro = '';
                if(!$model->isNewRecord){
                    
                    
                    $modeloCarro = $model->idModelo->nombre;

                    $marcaCarro =  $model->idModelo->idMarca->nombre;

                }
                ?>
                <?= Html::label('MODELO', 'Caso[modelo]', ['class' => 'control-label']) ?>
                <?= Html::textInput('Caso[modelo]', $modeloCarro ,['class' => 'form-control', 'id' => 'modelo', 'disabled' => true]) ?>             
                <?= Html::hiddenInput('Caso[id_modelo]', '', ['id' => 'caso-id_modelo', ]); ?>
            </div>
        </div>
        <div class="col-xs-12 col-md-6">
            <div class="form-group">
                <?= Html::label('MARCA', 'Caso[marca]', ['class' => 'control-label']) ?>
                <?= Html::textInput('Caso[marca]', $marcaCarro,['class' => 'form-control', 'id' => 'marca', 'disabled' => true ]) ?>
            </div>
        </div>
    </div>


    <!-- --TIPO DE CASO-- -->
    <br>
    <div class="row" style="border-bottom: 1px solid #222d32;">
        <h4>TIPO DE CASO</h4>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <?php
                $opciones = [0 => 'Accidente / Incidente', 3 => 'Aire acondicionado', 2 => 'Falla mecánica'/*, 1 => 'Incidente'*/];
                    $valid_tipo_caso = '    if(($(this).val() == 0)){
                                                document.getElementById("sec_fuente_accidente").style.display="block";
                                                document.getElementById("sec_data_accidente").style.display="block";
                                                document.getElementById("sec_amet").style.display="block";
                                                document.getElementById("sec_imagen_placa").style.display="none";
                                                document.getElementById("sec_imagen_danio").style.display="none";
                                                document.getElementById("sec_imagenes_4").style.display="none";
                                                document.getElementById("obligatorio_danio").style.display="block";
                                                document.getElementById("obligatorio_placa").style.display="block";
                                            }else{
                                                document.getElementById("sec_asignacion_casos").style.display="none";
                                                document.getElementById("sec_fuente_accidente").style.display="none";
                                                document.getElementById("sec_data_accidente").style.display="none";
                                                document.getElementById("sec_amet").style.display="none";
                                                document.getElementById("field_fuente_accidente").value="";
                                                document.getElementById("sec_asignacion_casos").style.display="none";
                                                document.getElementById("n_empleado").style.display="none";
                                                document.getElementById("field_tipo_accidente").value="";
                                                document.getElementById("sec_imagen_placa").style.display="block";
                                                document.getElementById("sec_imagen_danio").style.display="block";
                                                document.getElementById("sec_imagenes_4").style.display="none";
                                                document.getElementById("obligatorio_danio").style.display="none";
                                                document.getElementById("obligatorio_placa").style.display="none";
                                            }';
                echo $form->field($model, 'tipo_caso')->dropDownList(
                        $opciones,
                        [
                            'prompt'=>'Seleccione...',
                            'class' => 'form-control',
                            'onchange' => $valid_tipo_caso,
                            'id' => 'field_tipo_caso'
                        ] )->label('Tipo de Caso <span class="obligatorio">*&nbsp;</span>');
            ?>
        </div>
        <?php if(!$model->isNewRecord && $model->tipo_caso==0){ ?>
        <div id="sec_amet">
        <?php }else{ ?>
        <div id="sec_amet" style="display: none;">
        <?php } ?>
            <div class="col-xs-12 col-md-6">
                <?= $form->field($model, 'amet')->textInput() ?>
            </div>
        </div>
    </div>

<?php if(!$model->isNewRecord && $model->tipo_caso==0){ ?>
<div id="sec_fuente_accidente">
<?php }else{ ?>
<div id="sec_fuente_accidente" style="display: none;">
<?php } ?>
    <!-- --FUENTE DEL ACCIDENTE-- -->
    <br>
    <div class="row" style="border-bottom: 1px solid #222d32;">
        <h4>FUENTE DEL ACCIDENTE</h4>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <?php
                $fuentes_acc = [1 => 'Caso interno o empleado', 0 => 'Cliente final'];
                echo $form->field($model, 'fuente_accidente')->dropDownList(
                        $fuentes_acc,
                        [
                            'prompt'=>'Seleccione...',
                            'class' => 'form-control',
                            'onchange' => ' if($(this).val() == 1){
                                                document.getElementById("n_empleado").style.display="block";
                                                document.getElementById("sec_asignacion_casos").style.display="none";
                                            }else{
                                                document.getElementById("sec_asignacion_casos").style.display="block";
                                                document.getElementById("n_empleado").style.display="none";
                                            }',
                            'id' => 'field_fuente_accidente'
                        ] )->label('Fuente del Accidente <span class="obligatorio">*&nbsp;</span>');
            ?>
        </div>
        <div class="col-xs-12 col-md-6">
            <?php if(!$model->isNewRecord && $model->fuente_accidente==1){ ?>
            <div id="n_empleado">
            <?php }else{ ?>
            <div id="n_empleado" style="display: none;">
            <?php } ?>
                <?= $form->field($model, 'nombre_empleado')->textInput(['maxlength' => true])->label('Nombre Empleado <span class="obligatorio">*&nbsp;</span>') ?>
            </div>
        </div>
    </div>
</div>

<?php if(!$model->isNewRecord && $model->fuente_accidente==0 && $model->tipo_caso==0){ ?>
<div id="sec_asignacion_casos">
<?php }else{ ?>
<div id="sec_asignacion_casos" style="display: none;">
<?php } ?>
    <!-- --ASIGNACIÓN DE CASOS-- -->
    <br>
    <div class="row" style="border-bottom: 1px solid #222d32;">
        <h4>ASIGNACIÓN DE CASOS</h4>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <?= $form->field($model, 'num_contrato')->textInput()->label('Número de Contrato <span class="obligatorio">*&nbsp;</span>') ?>
        </div>
        <div class="col-xs-12 col-md-6">
            <?php
                $tip_clientes = [2=>'Asegurado', 1=>'Corporativo', 3=>'Leasing', 0=>'Retail'];
                echo $form->field($model, 'tipo_cliente')->dropDownList(
                        $tip_clientes,
                        [
                            'prompt'=>'Seleccione...',
                            'class' => 'form-control',
                        ] )->label('Tipo de Cliente <span class="obligatorio">*&nbsp;</span>');
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <?php
                $cob_cdw = [1=>'SI', 0=>'NO'];
                echo $form->field($model, 'cobertura_cdw')->dropDownList(
                        $cob_cdw,
                        [
                            'prompt'=>'Seleccione...',
                            'class' => 'form-control',
                        ] )->label('Tomo Cobertura CDW <span class="obligatorio">*&nbsp;</span>');
            ?>
        </div>
        <div class="col-xs-12 col-md-6">
            <?= $form->field($model, 'claim')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <?php
                $con_deduc = [1=>'SI', 0=>'NO'];
                echo $form->field($model, 'deducible')->dropDownList(
                        $con_deduc,
                        [
                            'prompt'=>'Seleccione...',
                            'class' => 'form-control',
                            'onchange' => ' if($(this).val() == 1){
                                                document.getElementById("mont_deducible").style.display="block";
                                            }else{
                                                document.getElementById("mont_deducible").style.display="none";
                                            }',
                            'id' => 'field_deducible'
                        ] )->label('Contrato con Deducible <span class="obligatorio">*&nbsp;</span>');
            ?>
        </div>
        <div class="col-xs-12 col-md-6">
            <?php if(!$model->isNewRecord && $model->deducible==1){ ?>
            <div id="mont_deducible">
            <?php }else{ ?>
            <div id="mont_deducible" style="display: none;">
            <?php } ?>
                <?php
                    echo $form->field($model, 'monto_deducible', [
                        'inputTemplate' => '<div class="input-group"><span class="input-group-addon">$</span>{input}</div>',
                    ])->label('Monto de Deducible <span class="obligatorio">*&nbsp;</span>');
                ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <?php
                $req_cotizac = [1=>'SI', 0=>'NO'];
                echo $form->field($model, 'req_cotizacion')->dropDownList(
                        $con_deduc,
                        [
                            'prompt'=>'Seleccione...',
                            'class' => 'form-control',
                            'onchange' => ' if($(this).val() == 1){
                                                document.getElementById("tarif_base").style.display="block";
                                                document.getElementById("piezas").style.display="block";
                                            }else{
                                                document.getElementById("tarif_base").style.display="none";
                                                document.getElementById("piezas").style.display="none";
                                                document.getElementById("tarif_base").value="";
                                                document.getElementById("piezas").value="";
                                            }',
                            'id' => 'field_req_cotizacion'
                        ] )->label('Requiere Cotización <span class="obligatorio">*&nbsp;</span>');
            ?>
        </div>
        <div class="col-xs-12 col-md-6">
            <?php if(!$model->isNewRecord && $model->req_cotizacion==1){ ?>
            <div id="tarif_base">
            <?php }else{ ?>
            <div id="tarif_base" style="display: none;">
            <?php } ?>
                 <?php
                    echo $form->field($model, 'tarifa_base', [
                        'inputTemplate' => '<div class="input-group"><span class="input-group-addon">$</span>{input}</div>',
                    ])->label('Tarifa Base <span class="obligatorio">*&nbsp;</span>');
                ?>
            </div>
        </div>
    </div>
</div>
<!-- --ESTE CAMPO ES PROVISIONAL PARA LA PRIMERA VERSION DEL PROYECTO EN PRODUCCIÓN-- -->

      <?php if(!$model->isNewRecord && $model->req_cotizacion==1){ ?>
      <div id="piezas">
      <?php }else{ ?>
      <div id="piezas" style="display: none;">
      <?php } ?>
          <div class="row">
              <div class="col-xs-12 col-md-12">
               <?php
                  echo $form->field($model, 'piezas')->textarea(['rows' => '2', 'style' => 'max-width:100%;'])->label('Piezas a Cotizar <span class="obligatorio">*&nbsp;</span>');
              ?>
            </div>
          </div>
    </div>

    <!-- --DATA DE ACCIDENTE O INCIDENTE-- -->
    <br>
    <div class="row" style="border-bottom: 1px solid #222d32;">
        <h4>DATA DE ACCIDENTE O INCIDENTE</h4>
    </div>
    <br>
<?php if(!$model->isNewRecord && $model->tipo_caso==0){ ?>
<div id="sec_data_accidente">
<?php }else{ ?>
<div id="sec_data_accidente" style="display: none;">
<?php } ?>
    <div class="row">
         <div class="col-xs-12 col-md-6">
            <?php
                echo $form->field($model, 'fecha_acc_inc')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'Seleccione fecha...'],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ])->label('Fecha de Accidente o Incidente <span class="obligatorio">*&nbsp;</span>');
            ?>
        </div>
        <div class="col-xs-12 col-md-6">
            <?php
                $tip_acc = ArrayHelper::map(Tipoaccidente::find()->orderBy('nombre')->all(), 'id_tipoaccidente', 'nombre');
                    $valid_tipo_accident = 'if($(this).val() == 4 || $(this).val() == 5 || $(this).val() == 6 || $(this).val() == 7 || $(this).val() == 8 || $(this).val() == 14 || $(this).val() == 16 || $(this).val() == 17){
                                                document.getElementById("sec_imagen_placa").style.display="block";
                                                document.getElementById("sec_imagen_danio").style.display="block";
                                                document.getElementById("sec_imagenes_4").style.display="none";
                                                document.getElementById("sec_ubicacionunidad").style.display="block";
                                                document.getElementById("field_ubicacion").value="";
                                            }else if($(this).val() == 1 || $(this).val() == 2 || $(this).val() == 3 || $(this).val() == 9 || $(this).val() == 10 || $(this).val() == 11 || $(this).val() == 12 || $(this).val() == 13){
                                                document.getElementById("sec_imagenes_4").style.display="block";
                                                document.getElementById("sec_imagen_placa").style.display="block";
                                                document.getElementById("sec_imagen_danio").style.display="none";
                                                document.getElementById("sec_ubicacionunidad").style.display="block";
                                                document.getElementById("field_ubicacion").value="";
                                            }else{
                                                if($(this).val() == 18){
                                                  document.getElementById("sec_imagen_danio").style.display="block";
                                                }else{
                                                  document.getElementById("sec_imagen_danio").style.display="none";
                                                }
                                                document.getElementById("sec_imagenes_4").style.display="none";
                                                document.getElementById("sec_imagen_placa").style.display="none";
                                                document.getElementById("field_ubicacion").value="";
                                                document.getElementById("sec_ubicacionunidad").style.display="none";
                                            }';
                echo $form->field($model, 'id_tipo_accidente')->dropDownList(
                        $tip_acc,
                        [
                            'prompt'=>'Seleccione...',
                            'class' => 'form-control',
                            'onchange' => $valid_tipo_accident,
                            'id' => 'field_tipo_accidente'
                        ] )->label('Tipo de Accidente <span class="obligatorio">*&nbsp;</span>');
            ?>
        </div>
    </div>
    <?php if(!$model->isNewRecord && $model->id_tipo_accidente!=15){ ?>
    <div id="sec_ubicacionunidad">
    <?php }else{ ?>
    <div id="sec_ubicacionunidad" style="display: none;">
    <?php } ?>
        <div class="row">
             <div class="col-xs-12 col-md-6">
                <?php
                    $u_unidad = ArrayHelper::map(Ubicacionunidad::find()->andWhere(['<>','id_ubicacionunidad', 6])->orderBy('nombre')->all(), 'id_ubicacionunidad', 'nombre');
                    echo $form->field($model, 'id_ubicacion')->dropDownList(
                            $u_unidad,
                            [
                                'prompt'=>'Seleccione...',
                                'class' => 'form-control',
                                'id' => 'field_ubicacion'
                            ] )->label('Dónde se encuentra la Unidad <span class="obligatorio">*&nbsp;</span>');
                ?>
            </div>
        </div>
    </div>
</div>
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <?= $form->field($model, 'descripcion')->textarea(['rows' => '2', 'style' => 'max-width:100%;']) ?>
        </div>
    </div>

    <!-- --REGISTRO DE COBRO-- -->
    <br>
    <div class="row" style="border-bottom: 1px solid #222d32;">
        <h4>REGISTRO DE COBRO</h4>
    </div>        
    <br>

    <div class="row">
        <div class="col-md-4">
        
        <?php
                $op = [1=>'SI', 0=>'NO'];
                echo $form->field($cobro, 'opcobro')->dropDownList(
                        $op,
                        [
                            'prompt'=>'Seleccione...',
                            'class' => 'form-control',
                            'id' => 'opm'
                        ] )->label('Amerita Cobro');
            ?>


        <!--<select name="op" class="form-control" id="opm">
            <option value="">Seleccione</option>
            <option value="0">No</option>
            <option value="1">Si</option>
        </select>-->
        </div>
    </div>
<?php
    if($cobro != NULL){
?>
    <div id="cobro-op" <?php if(isset($cobro->monto)) {?> style="display: block;" <?php }else{ ?> style="display: none;" <?php } ?>>
    
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <?= $form->field($cobro, 'monto')->textInput()->label('MONTO COBRADO') ?>
        </div>
        <div class="col-md-4 col-xs-12">
                <?=
                    $form->field($cobro, 'forma_pago')->widget(Select2::classname(), [
                        'data' => ['0' => 'Efectivo', '1' => 'Tarjeta de crédito', '2' => 'Crédito', '3' => 'CLAIM', '4' => 'Compañía privada de cobranza' ],
                        'options' => ['placeholder' => 'Selecciona la forma de pago'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label('FORMA DE PAGO');
                ?>            
        </div>
        <div class="col-md-4 col-xs-12">
        
        <?=
                    $form->field($cobro, 'motivo_cobro')->widget(Select2::classname(), [
                        'data' => ['0' => 'No presentó acta policial', '1' => 'Declinación de cobertura', '2' => 'Deducible', '3' => 'Daños con objetos fijos', '4' => 'Deslizamiento', '5' => 'Negligencia', '6' => 'Daños maliciosos', '7' => 'Imprudencia', '8' => 'Motín', '9' => 'Actividades políticas', '10' => 'Manejo en terrenos rocosos', '11' => 'Exceso de velocidad', '12' => 'Vandalismo', '13' => 'No fue cargado por daños' ],
                        'options' => ['placeholder' => 'Selecciona el motivo'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label('MOTIVO DE COBRO');
                ?>
        </div>
    </div>
    <div class="row">    
        <div class="col-md-4 col-xs-12">    
            <?= $form->field($cobro, 'numero_factura')->textInput()->label('NÚMERO DE FACTURA ASOCIADA') ?>
        </div>    
        <div class="col-md-4 col-xs-12">    
            <?=
                $form->field($cobro, 'fecha')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'Selecciona la fecha'],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd',
                        'orientation' => 'bottom left',
                    ]
                ]);
            ?>
        </div>
    </div>    
    </div>

    <?php
}
    ?>

    <!-- --IMAGEN DE SOPORTE-- -->
    <br>
    <div class="row" style="border-bottom: 1px solid #222d32;">
        <h4>IM&Aacute;GENES DE SOPORTE</h4>
    </div>
    <?php  if(!$model->isNewRecord){ ?>
            <div class="row"><span class="obligatorio"><strong>¡Atención!</strong> A excepción de las <strong>Im&aacute;genes Adicionales</strong>; las im&aacute;genes que se carguen a continuaci&oacute;n REEMPLAZAR&Aacute;N las existentes...</span></div>
    <?php  }  ?>
    <br>
    <?php if(!$model->isNewRecord && (($model->id_tipo_accidente>0 && $model->id_tipo_accidente<4) || ($model->id_tipo_accidente>8 && $model->id_tipo_accidente<14))){ ?>
    <div id="sec_imagenes_4">
    <?php }else{ ?>
    <div id="sec_imagenes_4" style="display: none;">
    <?php } ?>
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <?php  
                    echo $form->field($frontal, 'imageFiles[]')->fileInput(['multiple' => true, 'accept' => 'image/*', 'class' =>"form-control btn btn-default btn-md"])->label('Imágen Frontal <span class="obligatorio">*&nbsp;</span>');
                ?>
            </div>
            <div class="col-xs-12 col-md-6">
                <?php
                    echo $form->field($izquierdo, 'imageFiles[]')->fileInput(['multiple' => true, 'accept' => 'image/*', 'class' =>"form-control btn btn-default btn-md"])->label('Imágen Lateral Izquierdo <span class="obligatorio">*&nbsp;</span>');
                ?>
            </div>
        </div><br>
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <?php
                    echo $form->field($derecho, 'imageFiles[]')->fileInput(['multiple' => true, 'accept' => 'image/*', 'class' =>"form-control btn btn-default btn-md"])->label('Imágen Lateral Derecho <span class="obligatorio">*&nbsp;</span>');
                ?>
            </div>
            <div class="col-xs-12 col-md-6">
                <?php
                    echo $form->field($trasero, 'imageFiles[]')->fileInput(['multiple' => true, 'accept' => 'image/*', 'class' =>"form-control btn btn-default btn-md"])->label('Imágen Trasero <span class="obligatorio">*&nbsp;</span>');
                ?>
            </div>
        </div><br>
    </div>
    <div class="row">
        <?php if(!$model->isNewRecord && ($model->tipo_caso!=0  || $model->id_tipo_accidente!=15)){ ?>
        <div id="sec_imagen_placa">
        <?php }else{ ?>
        <div id="sec_imagen_placa" style="display: none;">
        <?php } ?>
            <div class="col-xs-12 col-md-6">
                <?php
                    echo $form->field($placa, 'imageFiles[]')->fileInput(['multiple' => true, 'accept' => 'image/*', 'class' =>"form-control btn btn-default btn-md"])->label('Imágen Placa <span class="obligatorio">*&nbsp;</span>');
                ?>
            </div>
        </div>
        <?php if(!$model->isNewRecord && (  $model->tipo_caso!=0 ||
                                            ($model->id_tipo_accidente>3 && $model->id_tipo_accidente<9) ||
                                             $model->id_tipo_accidente==14 ||
                                             $model->id_tipo_accidente==16 ||
                                             $model->id_tipo_accidente==17 )){ ?>
        <div id="sec_imagen_danio">
        <?php }else{ ?>
        <div id="sec_imagen_danio" style="display: none;">
        <?php } ?>
            <div class="col-xs-12 col-md-6">
                <?php
                    echo $form->field($danio, 'imageFiles[]')->fileInput(['multiple' => true, 'accept' => 'image/*', 'class' =>"form-control btn btn-default btn-md"])->label('Imágen Daño <span class="obligatorio">*&nbsp;</span>');
                ?>
            </div>
        </div>
    </div><br>
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <?php
                echo $form->field($soportes, 'imageFiles[]')->fileInput(['multiple' => true, 'accept' => 'image/*', 'class' =>"form-control btn btn-default btn-md"]);
            ?>
        </div>
    </div>
    <br><br>

    <?php
        if(!$model->isNewRecord){
        ?>
            <div class="row" align="center">
                <div>
                    <?= Html::a('CANCELAR', ['view', 'id' => $model->id_caso], ['class' => 'btn btn-cancelar']) ?>
                    &nbsp;&nbsp;&nbsp;
                    <?= Html::submitButton('GUARDAR CAMBIOS', ['class' => 'btn btn-primario']) ?>
                </div>
            </div>
        <?php
        }else{
        ?>
            <div class="form-group" align="center">
                <?= Html::submitButton('REGISTRAR CASO', ['class' => 'btn btn-primario']) ?>
            </div>
        <?php
        }
    ?>
    <?php ActiveForm::end(); ?>

</div>


<script>

 

$('#opm').on('change', function(event) {
    if($(this).val() == 1){
            $("#cobro-op").show();
        }else{
            $("#cobro-op").hide();            
        }
});

$('#placa').on('change', function(event) {

    event.preventDefault();
    var id_vehiculo = $(this).val();
    console.log(id_vehiculo);
    $.ajax({
        url: "<?= Url::toRoute('caso/getvehiculo') ?>",
        type: 'post',
        data: {
            id_vehiculo: id_vehiculo,
        },
        success: function (response) {
            if (response) {
                $('#caso-id_modelo').val(response['id_modelo']);
                $('#modelo').val(response['modelo']);
                $('#marca').val(response['marca']);
            }
        }
    });
});
</script>