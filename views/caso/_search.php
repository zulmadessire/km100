<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CasoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="caso-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_caso') ?>

    <?= $form->field($model, 'ficha') ?>

    <?= $form->field($model, 'placa') ?>

    <?= $form->field($model, 'tipo_caso') ?>

    <?= $form->field($model, 'fuente_accidente') ?>

    <?php // echo $form->field($model, 'amet') ?>

    <?php // echo $form->field($model, 'num_contrato') ?>

    <?php // echo $form->field($model, 'tipo_cliente') ?>

    <?php // echo $form->field($model, 'cobertura_cdw') ?>

    <?php // echo $form->field($model, 'claim') ?>

    <?php // echo $form->field($model, 'deducible') ?>

    <?php // echo $form->field($model, 'monto_deducible') ?>

    <?php // echo $form->field($model, 'tarifa_base') ?>

    <?php // echo $form->field($model, 'nombre_empleado') ?>

    <?php // echo $form->field($model, 'fecha_acc_inc') ?>

    <?php // echo $form->field($model, 'descripcion') ?>

    <?php // echo $form->field($model, 'registrado_por') ?>

    <?php // echo $form->field($model, 'id_estacion') ?>

    <?php // echo $form->field($model, 'id_modelo') ?>

    <?php // echo $form->field($model, 'id_tipo_accidente') ?>

    <?php // echo $form->field($model, 'id_ubicacion') ?>

    <?php // echo $form->field($model, 'req_cotizacion') ?>

    <?php // echo $form->field($model, 'fecha_registro') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
