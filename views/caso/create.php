<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Caso */

$this->title = ' REGISTRO DE CASO';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="caso-create">

  <div class="row">
    <div class="col-md-1 col-xs-3">
      <h4 class="titulo"><i class="fa fa-file-text fa-4x" aria-hidden="true"></i></h4>
    </div>
    <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
      <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
    </div>
  </div>
  </br>
    <?= $this->render('_form', [
        'model' => $model,
        'empresa' => $empresa,
        'estaciones' => $estaciones,
        'soportes' => $soportes,
        'frontal' => $frontal,
        'trasero' => $trasero,
        'izquierdo' => $izquierdo,
        'derecho' => $derecho,
        'danio' => $danio,
        'placa' => $placa,
        'cliente' => $cliente,
        'cobro' => $cobro,
        'vehiculos' => $vehiculos,
    ]) ?>

</div>
