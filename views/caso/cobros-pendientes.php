<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Empresa;
use app\models\Estacion;
use app\models\Marca;
use app\models\Modelo;
use yii\data\Pagination;
use yii\widgets\LinkPager;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CasoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'COBROS PENDIENTES';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="caso-index">
    <div class="row">
        <div class="col-xs-12 col-md-6">
          <div class="row">
            <div class="col-md-2 col-xs-3">
                <h4 class="titulo"><i class="fa fa-list-ol fa-4x" aria-hidden="true"></i></h4>
        	</div>
        	<div class="col-md-10 col-xs-9" style="padding-top: 30px;">
        		<h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
        	</div>
          </div>
        </div>
        <div class="col-xs-12 col-md-6">
        <br>
        <br>
            <ul class="nav nav-pills pull-right">
                <li role="presentation">
                </li>
            </ul>
        </div>
    </div>
  </br>
    <div class="table-responsive">
    <?php
    
        $acciones='{view}';

        if(\Yii::$app->user->can('control')){
           $acciones='{view} {delete}';
          if(\Yii::$app->user->can('modificar-caso')){
             $acciones='{view} {update} {delete}';
          }
        }
        if(\Yii::$app->user->can('admin') || \Yii::$app->user->can('principal')){
           $acciones='{view} {update} {delete}';
        }
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table'],
        'layout' => "{items}\n<div align='center'>{pager}</div>",
        'summary' => '',
        'options' =>[
            'class' => 'grid table-responsive',
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id_caso',
                'format' => 'text',
                'label' => 'Nº Caso',
            ],
            /*[
                'attribute' => 'id_estacion',
                'value' => function ($model) {
                            $estacion = Estacion::findOne($model['id_estacion']);
                            return $estacion->nombre;
                },
                'label' => 'Estación',
                'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'id_estacion',
                                'data' => $filtro_estaciones,
                                'options' => ['placeholder' => 'Seleccione Estación...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ])
            ],*/
            'ficha',
            'placa',
            [
                'attribute' => 'tipo_caso',
                'value' => function ($model) {
                            $tipo_caso = [0 => 'Accidente / Incidente', 3 => 'Aire acondicionado', 2 => 'Falla mecánica'];
                            return $tipo_caso[$model['tipo_caso']];
                },
                'label' => 'Tipo de Caso',
                'filter'=> [0 => 'Accidente / Incidente', 3 => 'Aire acondicionado', 2 => 'Falla mecánica'],
            ],
            [
                'attribute' => 'fecha_registro',
                'format' => 'text',
                'label' => 'Fecha de Registro',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Acciones',
                'template' => $acciones,
            ],
        ],
    ]); ?>
    </div>
</div>
