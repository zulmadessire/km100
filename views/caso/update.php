<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Caso */

$this->title = 'MODIFICAR CASO NÚMERO ' . $model->id_caso;
//$this->params['breadcrumbs'][] = ['label' => 'CASOS REGISTRADOS', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => 'VER CASO '.$model->id_caso, 'url' => ['view', 'id' => $model->id_caso]];
//$this->params['breadcrumbs'][] = 'MODIFICAR CASO';
?>
<div class="caso-update">

    <div class="row">
      <div class="col-md-1 col-xs-3">
        <h4 class="titulo"><i class="fa fa-file-text fa-4x" aria-hidden="true"></i></h4>
      </div>
      <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
        <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
      </div>
    </div>
    <?= $this->render('_form', [
            'model' => $model,
            'cliente' => $cliente,
            'empresa' => $empresa,
            'estaciones' => $estaciones,
            'soportes' => $soportes,
            'frontal' => $frontal,
            'trasero' => $trasero,
            'izquierdo' => $izquierdo,
            'derecho' => $derecho,
            'danio' => $danio,
            'placa' => $placa,
            'cobro' => $cobro,
    ]) ?>

</div>
