<?php
use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Notificaciones;
use app\models\NotificacionesSearch;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\vehiculo;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TallerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'TALLERES CON REGISTRO MERCANTIL POR VENCER';
//$this->params['breadcrumbs'][] = $this->title;
?>

<?php 
    $cantidad_rm = Notificaciones::find()->where(['estado'=>0,'tipo'=>3])->all();
    foreach ($cantidad_rm as $key => $value) {
        $value->estado = 1;
        $value->update(false);
    }
 ?>
<div class="notificaciones-index">

  <div class="row">
    <div class="col-md-1 col-xs-3">
      <h4 class="titulo"><i class="fa fa-car fa-4x" aria-hidden="true"></i></h4>
    </div>
    <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
      <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
    </div>
  </div>
  </br>
  <!--   <div class="row">
        <div class="col-md-12">
            <?php
              echo Html::a('Registrar Taller', ['create'], ['class' => 'btn btn-small-gris', 'style' => 'float:right;']);
            ?>
        </div>
    </div> -->
    <br>
        <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => ['class' => 'table table-responsive'],
                'summary' => '',
                'options' =>[
                    'class' => 'grid',
                ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

                [
                     
                        'label' => 'Nombre',
                        'format' => 'html',
                        'enableSorting' => false,
                        'value' => function ($model) {
                                   
                                    return $model->idTaller->nombre;

                               
                                    
                        },
                        'filter' => Html::input('text', 'NotificacionesSearch[nombre]', $searchModel->nombre, ['class' => 'form-control',]),
                        'contentOptions'=>['style'=>'text-align:center;'],

                    ],
                     [
                     
                        'label' => 'Teléfono',
                        'format' => 'html',
                        'enableSorting' => false,
                        'value' => function ($model) {
                                   
                                    return $model->idTaller->telefono;
                        },

                        'filter' => Html::input('text', 'NotificacionesSearch[telefono]', $searchModel->telefono, ['class' => 'form-control',]),
                        'contentOptions'=>['style'=>'text-align:center;'],
                    ],
                    [
                     
                        'label' => 'Nombre del Representante',
                        'format' => 'html',
                        'enableSorting' => false,
                        'value' => function ($model) {
                                   
                                    return $model->idTaller->nombre_representante;
                        },
                         'filter' => Html::input('text', 'NotificacionesSearch[nombre_representante]', $searchModel->nombre_representante, ['class' => 'form-control',]),
                        'contentOptions'=>['style'=>'text-align:center;'],
                    ],
                          [
                     
                        'label' => 'Fecha de Vencimiento',
                        'format' => 'html',
                        'enableSorting' => false,
                        'value' => function ($model) {
                                   
                                    return $model->idTaller->vencimiento_rm;
                        },
                         // 'filter' => Html::input('text', 'NotificacionesSearch[vencimiento_rm]', $searchModel->vencimiento_rm, ['class' => 'form-control',]),
                        'filter' =>  DatePicker::widget([
                        'name' => 'NotificacionesSearch[vencimiento_rm]',
                        'type' => DatePicker::TYPE_INPUT,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'yyyy-m-dd'
                        ]
                    ]),
                        'contentOptions'=>['style'=>'text-align:center;'],
                    ],
                    [
                        'attribute' => 'tiempo',
                        'label' => 'Estado',
                        'format' => 'html',
                        'filter' => ['0'=>'Vencido','3'=>'Próximo a Vencerse'],
                        'enableSorting' => false,
                        'value' => function ($model) {
                                       if ($model->tiempo > 0) {
                                       return 'Próximo a vencerse';
                                   }
                                   if ($model->tiempo == 0){
                                    return 'Vencido';
                                   }
                                    
                        },

                        'contentOptions'=>['style'=>'text-align:center;'],
                    ],
 
            // 'id_taller',

            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
 
  </div>
 
</div>






















 