<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Marca;
use app\models\Modelo;
use yii\helpers\ArrayHelper;
use app\models\Estacion;
use app\models\TipoServicio;
use app\models\VehiculoTaller;
use kartik\form\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EmpresaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Listado de Empresas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empresa-index">
      <div class="row">
    <div class="col-md-1 col-xs-3">
      <h4 class="titulo"><i class="fa fa-home fa-4x" aria-hidden="true"></i></h4>
    </div>
    <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
      <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
    </div>
  </div>
  </br>
  <div class="row">
      <?php
          echo Html::a('Registrar Empresa', ['create'], ['class' => 'btn btn-accion', 'style' => 'float:right;']);
      ?>
  </div>

 
 
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
                'options' =>[
                        'class' => 'grid',
                    ],
        'columns' => [
             
 
            'nombre',
            'nombre_comercial',

                  [
                         
                        'label' => 'Estación',
                        'format' => 'raw',
                        'value' => function( $model ){
                            $estaciones = "";
                            foreach ($model->estacions as $value) {
                              
                               $estaciones = $estaciones."<br>". $value->nombre;
                            }
                            return $estaciones;
                            
                        },
                            'filter' => Select2::widget([
                            'name' => 'EmpresaSearch[id_estacion]',
                            'data' => ArrayHelper::map(Estacion::find()->orderBy('nombre')->all(), 'id_estacion', 'nombre'),
                            'options' => ['placeholder' => 'Selecciona',],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]),
                 
                    ],

            ['class' => 'yii\grid\ActionColumn',
              'template' => '{update}'],

        ],
    ]); ?>
</div>
