<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\db\Query;
use yii\web\view;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\file\FileInput;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\widgets\InputWidget;
use app\models\Estacion;
use kolyunya\yii2\widgets\MapInputWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Empresa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="empresa-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic_lista-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>
        <div class="row" style="border-bottom: 1px solid #222d32;">
        <div class="col-xs-12 col-md-6">
            <h4>DATOS DE LA EMPRESA</h4>
        </div>
        <div class="col-xs-12 col-md-6">
          <div align="right">
                <?php
                echo Html::a('Listado de Empresas', ['index'], ['class' => 'btn btn-accion']);
                ?>
          </div>
        </div>
    </div>
    <br>

    <div class="row">
    	  <div class="col-xs-12 col-md-6">
            <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-12 col-md-6">
            <?= $form->field($model, 'nombre_comercial')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
   <!-- --ASOCIACIÓN DE estaciones-- -->
     <br>
    <div class="form-group" style="text-align:center;">
        <?= Html::submitButton($model->isNewRecord ? 'Registrar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
