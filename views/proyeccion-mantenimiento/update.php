<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProyeccionMantenimiento */

$this->title = 'Update Proyeccion Mantenimiento: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Proyeccion Mantenimientos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="proyeccion-mantenimiento-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
