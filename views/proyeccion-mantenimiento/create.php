<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ProyeccionMantenimiento */

$this->title = 'Create Proyeccion Mantenimiento';
$this->params['breadcrumbs'][] = ['label' => 'Proyeccion Mantenimientos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proyeccion-mantenimiento-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
