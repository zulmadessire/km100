<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProyeccionMantenimientosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proyeccion-mantenimiento-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'fecha_proyectada') ?>

    <?= $form->field($model, 'km_proyectado') ?>

    <?= $form->field($model, 'realizado') ?>

    <?= $form->field($model, 'serie') ?>

    <?php // echo $form->field($model, 'id_vehiculo') ?>

    <?php // echo $form->field($model, 'id_proximo_tipo_mantenimiento') ?>

    <?php // echo $form->field($model, 'fecha_reporte') ?>

    <?php // echo $form->field($model, 'id_estacion_reporte') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
