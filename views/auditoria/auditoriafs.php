<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
/*use yii\widgets\ActiveForm;*/
use yii\db\Query;
use kartik\form\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Vehiculo */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Registro de auditoría - Unidades fuera de servicio';
//$this->params['breadcrumbs'][] = $this->title;


	$acumulador = 0;

      foreach ($model_est->servicios as $value) {
                           					
        foreach ($value->solicitudServicioTallers as $value2) {
                           							
          foreach ($value2->cotizacionTallers as $value3) {
                           									
            foreach ($value3->ordenTrabajos as $value4) {
                           											
              foreach ($value4->solicitudCompraPiezas as $value5) {
                           														
                foreach ($value5->solicitudCompraProveedors as $value6) {
                           															
                  foreach ($value6->cotizacionProveedors as $value7) {
                           																	
                     foreach ($value7->ordenCompraPiezas as $value7) {
                           																		 	
                       $acumulador = $acumulador+1;
                     }
                  }
                }
              }
            }
          }
        }
      }
      $acumulador2 = 0;
       foreach ($model_est->servicios as $value) {

       		foreach ($value->pipelineServicios as $value2) {
       			 
       			 if ($value2->id_estado_servicio==10 || $value2->id_estado_servicio==11) {
       			 	 
       			 	 $acumulador2 = $acumulador2 + 1;
       			 }
       		}
       }

       $desviacion = $acumulador2-$acumulador;
?>
<div class="Audiorias">

  <div class="row">
    <div class="col-md-1 col-xs-3">
      <h4 class="titulo"><i class="fa fa-list-ul fa-4x" aria-hidden="true"></i></h4>
    </div>
    <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
      <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
    </div>
  </div>
 
    <br>
    
    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DEL CLIENTE</p>
            <hr>
        </div>
    </div>

    <div class="row">
       <?php $form = ActiveForm::begin(['id' => 'dynamic_lista-form']); ?>
        
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_est, 'empresa')->textInput(['disabled' => true, 'value' => $model_est->idEmpresa->nombre])->label('Empresa') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'nombre')->textInput(['value' => strtoupper($model_est->nombre), 'disabled' => true])->label('Estación') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'fecha')->textInput(['value' => date('Y-m-d'),'maxlength' => true, 'disabled' => true]) ?>
        </div>
      

           

            <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">REGISTROS</p>
            <hr>
        </div>
    </div>

      <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <?= Html::label('FUERA DE SERVICIO REGISTRADO', '', ['class' => 'control-label']) ?>
                <?= Html::textInput('', $acumulador,['class' => 'form-control', 'disabled' => true, ]) ?>
            </div>
        </div>
         <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <?= Html::label('FUERA DE SERVICIO EN ESTACIÓN', '', ['class' => 'control-label']) ?>
                <?= Html::textInput('', $acumulador2,['class' => 'form-control', 'disabled' => true, ]) ?>
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <?= Html::label('DESVIACIÓN', '', ['class' => 'control-label']) ?>
                <?= Html::textInput('', $desviacion,['class' => 'form-control', 'disabled' => true, ]) ?>
            </div>
        </div>
         <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'motivo')->textInput([ 'value'=> '', 'disabled' => false])->label('Motivo de desviación') ?>
        </div>
         <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'planaccion')->textInput([ 'value'=> '',  'disabled' => false])->label('Plan de Acción') ?>
        </div>
    <div class="col-xs-12">
            <p class="titulo-form">UNIDADES FUERA DE SERVICIO EN ESTACIÓN</p>
            <hr>
        </div>
         <br>
    
         <div class="col-md-12 col-xs-12" style="margin-button: 5px;">
          <?php if ($variable!='') {echo $variable;}?>
          </div>
          <br>
         <div class="col-md-12 col-xs-12">
         <?php if ($variable=='') {?>
             <div class="row">
        <div class="col-md-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
     
                'tableOptions' => ['class' => 'grid-blue table table-responsive table-bordered'],
                'summary' => '',
       
                'options' =>[
                    'class' => '',
                ],
                'columns' => [
                    [
                        'label' => 'Ficha/MVA',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:center;'],
                         'value' => function( $model ){
                               


                            return $model->idServicio->idVehiculo->mva;
                        },
                    ],
                     [
                        'label' => 'Modelo',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:center;'],
                         'value' => function( $model ){
                               


                            return $model->idServicio->idVehiculo->idModelo->nombre;
                        },
                    ],
    


                 [
                        'label' => 'Causa',
                        'format' => 'raw',
                        'filter' => false,
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:center;'],
                         'value' => function( $model ){
                            

                    $piezas = \app\models\PipelineServicio::find()->where(['id' => $model->id ])->all();
                            
                            foreach ($piezas as $key => $pieza) {
                                $vector_piezas[] = Html::input('text', 'causa[]', '', ['class'=> 'form-control causa', 'style' => 'font-size: 10pt; text-align: center;', 'disabled' => false, 'data' => ['id' => $pieza->id], ]);
                            }

                            $respuesta_piezas = implode('<br>', $vector_piezas);

                            return $respuesta_piezas;



                        },
                    ],
                                         [
                        'label' => 'Orden de compra/Estatus',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:center;'],
                         'value' => function( $model ){
                               $orden = '';
     foreach ($model->idServicio->solicitudServicioTallers as $value) {
                                                                    
          foreach ($value->cotizacionTallers as $value3) {
                                            
            foreach ($value3->ordenTrabajos as $value4) {

                                                
              foreach ($value4->solicitudCompraPiezas as $value5) {
                                                      
                foreach ($value5->solicitudCompraProveedors as $value6) {
                                                        
                  foreach ($value6->cotizacionProveedors as $value7) {
                                                            
                     foreach ($value7->ordenCompraPiezas as $value7) {
                            if ($value7) {
                              $orden = $value7->id;
                            }
                                        

                     }
                  }
                }
            }
          }
        }
      }
                  if ($orden == '') {
                        
                              $orden = 'No posee orden';
                            
                  }
                             return $orden.' / '. $model->idEstadoServicio->nombre;
                        },
                    ],
                          [
                        'label' => 'Observaciones',
                        'format' => 'raw',
                        'filter' => false,
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:center;'],
                         'value' => function( $model ){
                               
                    $piezas = \app\models\PipelineServicio::find()->where(['id' => $model->id ])->all();
                            
                            foreach ($piezas as $key => $pieza) {
                                $vector_piezas[] = Html::input('text', 'observacion[]', '', ['class'=> 'form-control observacion', 'style' => 'font-size: 10pt; text-align: center;', 'disabled' => false, 'data' => ['id' => $pieza->id], ]);
                            }

                            $respuesta_piezas = implode('<br>', $vector_piezas);

                            return $respuesta_piezas;

                     
                        },
                    ],


                  
                ],
              

                
            ]); ?>
        </div>
    </div>

        <?php } ?>
</div>
      <div class="col-xs-12">
            <p class="titulo-form">OTRAS SOLOCITUDES</p>
            <hr>
        </div>
             <div class="row">
  
      <?php DynamicFormWidget::begin([
        'widgetContainer' => 'dynamicform_wrapper_resultados',
        'widgetBody' => '.container-items',
        'widgetItem' => '.item',
        'limit' => 50,
        'min' => 1,
        'insertButton' => '.add-item',
        'deleteButton' => '.remove-item',
        'model' => $modelsSolicitudes[0],
        'formId' => 'dynamic_lista-form',
        'formFields' => [
            'nombre',
            
        ],
  ]); ?>
  <table class="table table-hover">
      <tbody class="container-items">
      <?php foreach ($modelsSolicitudes as $i => $modelmodel): ?>
          <tr class="item panel panel-default">
              <td>
              
                   <div class="col-md-12 col-xs-12">
   <div class="col-xs-12 col-md-1"><?= $form->field($modelmodel, "[{$i}]numero")->textInput(); ?></div>
   <div class="col-xs-12 col-md-4"><?= $form->field($modelmodel, "[{$i}]solicitud")->textInput(); ?></div>
   <div class="col-xs-12 col-md-1"><?= $form->field($modelmodel, "[{$i}]cantidad")->textInput(); ?></div>
  <div class="col-xs-12 col-md-4"><?= $form->field($modelmodel, "[{$i}]observacion")->textInput(); ?></div>
   <div class="col-xs-12 col-md-1 text-center">
                      <button type="button" class="remove-item btn btn-cancelar btn-xs" style="margin-top:28px;">
                          <i class="glyphicon glyphicon-minus"></i></button>
                  </div>
        </div>
              
           

                 
              </td>
          </tr>
      <?php endforeach; ?>
  <?php DynamicFormWidget::end(); ?>
      </tbody>
  </table>

  <button type="button" class="add-item btn btn-accion  btn-xs pull-right">Agregar solicitud</button>
  <br>
  <br>

     <div class="col-md-12 col-xs-12">
            <?= $form->field($model, 'observacion')->textInput(['disabled' => false])->textarea(['rows' => '6']) ->label('Observación') ?>
        </div>


        <div class="col-md-12 col-xs-12">
        <div class="form-group text-center">  
<?= Html::submitButton($model->isNewRecord ? 'GUARDAR' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-submit' : 'btn btn-primary']) ?>
        </div>
    </div>
        <?php ActiveForm::end(); ?>
