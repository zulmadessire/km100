<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\models\Caso;
use app\models\Empresa;
use app\models\Cliente;
use app\models\Estacion;
use app\models\Pieza;
use app\models\TipoPieza;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use yii\web\JsExpression;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Marca;


/* @var $this yii\web\View */
/* @var $model app\models\SolicitudCompraEstacion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="solicitud-compra-estacion-form">

<?php $form = ActiveForm::begin(['id' => 'dynamic_lista-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>
    <br>
        <div class="row" style="border-bottom: 1px solid #222d32;">
            <h4>DATOS DE LA EMPRESA</h4>
        </div>
    <br>
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <label class="control-label" for="id_estado">CLIENTE <span class="obligatorio">*&nbsp;</span></label>
                <?= Html::hiddenInput('SolicitudCompraEstacion[tipo_solicitud]', 3, ['id' => 'tipo_solicitud', ]); ?>  
                <?php if (\Yii::$app->user->can('estaciones todas') ): ?>
                    <?=  Select2::widget([
                            'name' => 'SolicitudCompraEstacion[id_cliente]',
                            'value' => ($model->id_estacion)?$model->idEstacion->idEmpresa->cliente->id:'',
                            'data' => ArrayHelper::map(Cliente::find()->orderBy('nombre_comercial')->where('id != 6')->all(), 'id', 'nombre_comercial'),
                            'options' => ['placeholder' => 'Selecciona el cliente', 'id' => 'id_cliente', 'initialize' => true,]
                        ]); 
                    ?>
                <?php else: ?>
                    <?= Html::textInput('SolicitudCompraEstacion[id_cliente]', $cliente->nombre_comercial, ['class' => 'form-control', 'id' => 'id_cliente', 'disabled' => true,]) ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <label class="control-label" for="id_estado">EMPRESA <span class="obligatorio">*&nbsp;</span></label>
            <?php if ( \Yii::$app->user->can('estaciones todas') ){ ?>
                <?=
                    DepDrop::widget([
                        'name' => 'SolicitudCompraEstacion[id_empresa]',
                        'data' => ($model->id_estacion)?ArrayHelper::map( Empresa::find()->where(['id_empresa'=>$model->idEstacion->id_empresa])->all(), 'id_empresa', 'nombre' ):[''=>''],
                        'type'=>DepDrop::TYPE_SELECT2,
                        'options'=>['id'=>'id_empresa', 'class' => 'form-control'],
                        'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                        'pluginOptions'=>[
                            'depends'=>['id_cliente'],
                            'placeholder'=>'Selecciona la empresa',
                            'url'=>Url::to(['/vehiculo/getempresas']),
                            'initialize' => true,
                        ]
                    ]);
                ?>
            <?php } elseif (\Yii::$app->user->can('estaciones cliente')) { ?>
                <?=  
                    Select2::widget([
                            'name' => 'SolicitudCompraEstacion[id_empresa]',
                            'value' => ($model->id_estacion)?$model->idEstacion->id_empresa:'',
                            'data' => ArrayHelper::map(Empresa::find()->where('cliente_id = ' . $cliente->id )->orderBy('nombre')->all(), 'id_empresa', 'nombre'),
                            'options' => ['placeholder' => 'Selecciona la empresa', 'id' => 'id_empresa', 'initialize' => true,]
                        ]); 
                    ?>
            <?php } elseif (\Yii::$app->user->can('estaciones asociadas')) { ?>
                <?= Html::textInput('SolicitudCompraEstacion[id_empresa]', $empresa->nombre, ['class' => 'form-control', 'id' => 'id_empresa', 'disabled' => true,]) ?>
            <?php } ?>
            
        </div>
        <div class="col-md-4 col-xs-12">
            <?php if (\Yii::$app->user->can('estaciones asociadas')) { ?>
                <?=
                    $form->field($model, 'id_estacion')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(Estacion::find()->where(['id_estacion' => $estaciones ])->all(), 'id_estacion', 'nombre'),
                        'options' => ['placeholder' => 'Selecciona la estación', 'id' => 'id_estacion'],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ])->label('ESTACIÓN <span class="obligatorio">*&nbsp;</span>');
                ?>
            <?php } else { ?>
                <?=
                    $form->field($model, 'id_estacion')->widget(DepDrop::classname(), [
                        'type'=>DepDrop::TYPE_SELECT2,
                        'data' => ($model->id_estacion)?ArrayHelper::map( Estacion::find()->where(['id_estacion'=>$model->id_estacion])->all(), 'id_estacion', 'nombre'):[''=>''],
                        'options'=>['id'=>'id_estacion', 'class' => 'form-control'],
                        'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                        'pluginOptions'=>[
                            'depends'=>['id_empresa'],
                            'placeholder'=>'Selecciona la estación',
                            'url'=>Url::to(['/vehiculo/getestaciones']),
                        ],
                    ])->label('ESTACIÓN <span class="obligatorio">*&nbsp;</span>');
                ?>
            <?php } ?>
        </div>
    </div>
    <div class="row" style="border-bottom: 1px solid #222d32;">
        <h4>PIEZAS SOLICITADAS</h4>
    </div>
    <br><br>
    <div class="row">
	    <div class="col-md-12 col-xs-12">
            <?php 
                DynamicFormWidget::begin([
                    'widgetContainer' => 'dynamicform_wrapper_resultados',
                    'widgetBody' => '.container-items',
                    'widgetItem' => '.item',
                    'limit' => 50,
                    'min' => 1,
                    'insertButton' => '.add-item',
                    'deleteButton' => '.remove-item',
                    'model' => $model_cotizacion_estacion_pieza[0],
                    'formId' => 'dynamic_lista-form',
                    'formFields' => [
                        'nombre',
                        'id_modelo',
                        'id_marca',
                    ],
                ]);            
            ?>
            <table class="table table-hover">
                <tbody class="container-items">
                    <?php foreach ($model_cotizacion_estacion_pieza as $i => $modelmodel): ?>
                        <tr class="item panel panel-default pieza">
                            <td>
                            <?php
                                $piezas = array();
                                $tiposdepieza = array();
                                if(!$modelmodel->isNewRecord){
                                    $piezas = ArrayHelper::map(Pieza::find()->where(['id_pieza'=>$modelmodel->id_pieza])->orderBy('nombre')->all(), 'id_pieza', 'nombre');
                                    //$tiposdepieza = ArrayHelper::map(TipoPieza::find()->where([ 'id' => $piezas->tipo_pieza_id ])->orderBy('nombre')->all(), 'id', 'nombre');

                                } 
                            ?>
                        <div class="col-xs-12 col-md-3">
                            <?php
                            if($modelmodel->isNewRecord){
                                $piezas = ArrayHelper::map(Pieza::find()->where(['tipo_pieza_id'=>6])->orderBy('nombre')->all(), 'id_pieza', 'nombre');
                            }
                            echo $form->field($modelmodel, "[{$i}]id_pieza")->dropDownList(
                                $piezas,
                                [
                                'prompt'=>'Seleccione',
                                'class' => 'form-control',
                                ])->label('PIEZA');
                                ?>
                            </div>
                            <div class="col-xs-12 col-md-2">
                                <?= $form->field($modelmodel, "[{$i}]cantidad")->label('CANTIDAD')->textInput(['maxlength' => true,'placeholder'=>'Cantidad', 'type' => 'number', 'class'=>'cotizaciontallerpieza form-control', 'min' => 0]) ?>
                            </div>
                            <div class="col-xs-12 col-md-1 text-center">
                            <button type="button" class="remove-item btn btn-cancelar btn-xs" style="margin-top:28px;" >
                                <i class="glyphi">
                                <i class="glyphicon glyphicon-minus"></i></button>
                            </div>
                        </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php DynamicFormWidget::end(); ?>
                </tbody>      
            </table>
            <button type="button" class="add-item btn btn-accion  btn-xs pull-right">Agregar Pieza</button>
        </div>
    </div>

    <br><br>
    <div class="form-group" align="center">

        <?= Html::submitButton($model->isNewRecord ? 'REGISTRAR SOLICITUD' : 'ACTUALIZAR SOLICITUD', ['class' => $model->isNewRecord ? 'btn  btn-primario' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>

$('#placa').on('change', function(event) {
    console.log('Entra');
    event.preventDefault();
    var id_vehiculo = $(this).val();
    $.ajax({
        url: "<?= Url::toRoute('caso/getvehiculo') ?>",
        type: 'post',
        data: {
            id_vehiculo: id_vehiculo,
        },
        success: function (response) {
            if (response) {
                $('#estacion-id_vehiculo').val(id_vehiculo);
                $('#estacion-id_modelo').val(response['id_modelo']);
                $('#modelo').val(response['modelo']);
                $('#marca').val(response['marca']);
            }
        }
    });
});

</script>

