<?php 

use yii\helpers\Html;

$subtotal = 0;
?>
<div class="content">
	<div class="row">
		<img src="/km100/img/logo.png"  width="70" style="float: left;">
		<div class="col-xs-6" style="font-size: 10pt;">
			<b>Inversiones Forteza, S.R.L.</b><br>
			Av. Republica de Colombia esq.<br>
			Av. Monumental<br>
			Tel.: 809-331-6009<br>
			RNC: 130213445<br>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-xs-12 text-center" style="font-size: 12pt;">
			<b>ORDEN DE COMPRA</b>
		</div>
	</div>
	<br><br>
	<div class="row">
		<div class=" col-xs-12" style="border: 1px solid black; border-radius: 3px; padding: 5px; padding-left: 10px;">
			<div style="width: 45%; float: left; padding: 5px; border-right: 1px solid black;">
				<span style="font-size: 8pt;"><b>DATOS SUPLIDOR</span></b><br>
				<span style="font-size: 8pt;"><?= $model_proveedor->nombre ?></span><br>
				<br>
				<span style="font-size: 8pt;"><b>Contribuyete:</b> <?= $model_proveedor->identificacion ?><br></span>
				<span style="font-size: 8pt;"><b>Teléfono:</b> <?= $model_proveedor->telefono ?><br></span>
				<span style="font-size: 8pt;"><b>Dirección:</b> <?= $model_proveedor->direccion ?><br></span>
			</div>
			<div style="width: 45%; float: left; padding: 5px;">
				<div style="border-bottom: 1px solid black; width: 100%; padding: 5px;">
					<span style="font-size: 8pt;"><b>Número: OC000<?= $model_orden_compra->id ?></b></span>
				</div>
				<div style="width: 100%; padding: 5px;">
					<span style="font-size: 8pt;"><b>Fecha Orden:</b> <?= date('d/m/Y', strtotime($model_orden_compra->fecha)) ?></span><br>
					<span style="font-size: 8pt;"><b>Condición de pago:</b> <?= ucfirst($model_proveedor->Tipo_pago).(($model_proveedor->Tipo_pago == 'credito')?' a '.$model_proveedor->plazo_pago.' Días':'') ?></span><br>
					<span style="font-size: 8pt;"><b>Cotización proveedor:</b> No. <?= $model_orden_compra->id_cotizacion_proveedor ?></span><br>
					<span style="font-size: 8pt;"><b>Moneda:</b> <?= ($model_proveedor->moneda == 0)?'RD':'USD' ?></span><br>
				</div>
			</div>
			<div style="width: 100%; border-top: 1px solid black; padding: 5px;">
				<span style="font-size: 8pt;">DIRECCIÓN DE ENVÍO: </span>
			</div>
		</div>
	</div>
	<br><br>
	<div class="row">
		<div class="col-xs-11">
			<table class="table">
				<thead class="text-center">
					<tr>
						<th style="text-align: center; font-size: 8pt;">#</th>
						<th style="text-align: center; font-size: 8pt;">CANTIDAD<br>ORDENADA</th>
						<th style="text-align: center; font-size: 8pt;">CÓDIGO</th>
						<th style="text-align: center; font-size: 8pt;">DESCRIPCIÓN</th>
						<th style="text-align: center; font-size: 8pt;">UNIDAD<br>COMPRA</th>
						<th style="text-align: center; font-size: 8pt;">PRECIO<br>UNITARIO</th>
						<th style="text-align: center; font-size: 8pt;">IMPORTE TOTAL</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($piezas as $i => $pieza) { ?>
						<tr>
							<td style="text-align: center; font-size: 7pt;"><?= $i+1 ?></td>
							<td style="text-align: center; font-size: 7pt;"><?= $pieza->cantidad_comprar ?></td>
							<td style="text-align: center; font-size: 7pt;"></td>
							<td style="text-align: left; font-size: 7pt;"><?= strtoupper($pieza->idSolicitudPiezaEstacion->idPieza->nombre) ?></td>
							<td style="text-align: center; font-size: 7pt;"><?= strtoupper($pieza->idSolicitudPiezaEstacion->idPieza->unidad) ?></td>
							<td style="text-align: right; font-size: 7pt;"><?= number_format(floatval($pieza->costo), 2, ',', '.') ?></td>
							<td style="text-align: right; font-size: 7pt;"><?= number_format(floatval($pieza->costo * $pieza->cantidad_comprar), 2, ',', '.') ?></td>
						</tr>
						<?php 
						$subtotal = $subtotal + ( $pieza->cantidad_comprar * $pieza->costo );
					} 
					?>
				</tbody>
			</table>
		</div>
	</div>
	<br><br>
	<br><br>
	<br><br>
	<br><br>
	<br><br>
	<br><br>
	<div class="row">
		<div class=" col-xs-12" style="border: 1px solid black; border-radius: 3px; padding: 5px; padding-left: 10px;">
			<div style="width: 45%; float: left; padding: 5px;">
				<span style="font-size: 9pt;"><b>Observaciones: </b></span><br>
				<span style="font-size: 8pt;"><b><?= ($model->id_vehiculo != NULL)? 'Vehículo: ' : 'Modelo Vehículo:' ?> </b></span><br>
				<div style="width: 35%; float: left; padding: 5px;">
				<?php if($model->tipo_solicitud != 3){ ?>
					<span style="font-size: 8pt;">&nbsp;&nbsp;&nbsp;&nbsp;Marca: <?= $model->idModelo->idMarca->nombre ?></span><br>
					<span style="font-size: 8pt;">&nbsp;&nbsp;&nbsp;&nbsp;Modelo: <?= $model->idModelo->nombre ?> </span><br>
				<?php }else { ?>
					<span style="font-size: 8pt;">&nbsp;&nbsp;&nbsp;&nbsp;Todos (Misceláneos) </span><br>
				<?php } ?>
				</div>
				<?php if( $model->id_vehiculo != NULL ): ?>
				<div style="width: 50%; float: left; padding: 5px;">
					<span style="font-size: 8pt;">Placa: <?= $model_vehiculo->placa ?></span><br>
					<span style="font-size: 8pt;">Chasis: <?= $model_vehiculo->chasis ?></span><br>
					<span style="font-size: 8pt;">Año: <?= $model_vehiculo->anho ?></span><br>
				</div>
				<?php endif ?>
				<span style="font-size: 8pt;"><b>Estacion: </b> <?= $model->idEstacion->nombre ?> - <?= $model->idEstacion->idEmpresa->nombre ?></span>
				<br>
			</div>
			<div style="width: 45%; float: left; padding: 5px; border-left: 1px solid black;">
				<div style="border-bottom: 1px solid black; width: 100%; padding: 5px;">
					<div style="width: 45%; text-align: right; float: left;">
						<span style="font-size: 8pt;"><b>Total Mercadería:</b> <br></span>
						<span style="font-size: 8pt;"><b>Descuento (<?= $model_proveedor->porcentaje ?>%):</b><br></span>
						<span style="font-size: 8pt;"><b>ITBIS:</b><br></span>
					</div>
					<div style="width: 45%; text-align: right;">
						<?php 
							$descuento = $subtotal - ( ($subtotal * $model_proveedor->porcentaje) / 100 );
							$itbis = ($descuento * 0.18);
						?>
						<span style="font-size: 8pt;"><b><?= number_format(floatval( $subtotal ), 2, ',', '.') ?></b></span><br>
						<span style="font-size: 8pt;"><b><?= '- '.number_format(floatval( ($subtotal * $model_proveedor->porcentaje) / 100  ), 2, ',', '.') ?></b></span><br>
						<span style="font-size: 8pt;"><b><?= '+ '.number_format(floatval( $itbis ), 2, ',', '.') ?></b></span>
					</div>
				</div>
				<div style="width: 100%; padding: 5px;">
					<div style="width: 45%; text-align: right; float: left;">
						<span style="font-size: 8pt;"><b>Total: <?= ($model_proveedor->moneda == 0)?'RD':'USD'?></b><br></span>
					</div>
					<div style="width: 45%; text-align: right;">
						<span style="font-size: 8pt;"><b><?= number_format((floatval( $descuento + $itbis )), 2, ',', '.') ?></b></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br><br>
	<div class="row">
		<div class="col-xs-12">
			<div style="text-align: center; font-size: 8pt;">
				<div class="col-xs-3 col-xs-offset-1">
					<span style="text-align: center;"><b>___________________________<br>
					HECHO POR</b></span>
				</div>
				<div class="col-xs-3">
					<span style="text-align: center;"><b>___________________________<br>
					REVISADO POR</b></span>
				</div>
				<div class="col-xs-3">
					<b>___________________________<br>
					<span style="text-align: center;">AUTORIZADO POR</b></span>
				</div>
			</div>
		</div>
	</div>
</div>