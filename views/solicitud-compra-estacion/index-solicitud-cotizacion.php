<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\db\Query;
use app\models\Servicio;
use app\models\SolicitudCompraEstacion;
use app\models\Vehiculo;
use app\models\CotizacionProveedor;
use app\models\SolicitudCompraProveedor;
use app\models\Marca;
use app\models\Modelo;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PartesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'LISTADO DE SOLICITUDES DE COTIZACIÓN ESTACIÓN';
?>
<div class="taller-index">

    <div class="row">
        <div class="col-md-1 col-xs-3">
            <i class="fa fa-list-ul fa-3x" aria-hidden="true"></i>
        </div>
        <div class="col-md-10 col-xs-9">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
        </div>
        <div class="col-xs-12">
            </br>
            <p> 
                <span style= "background-color: #c58b8b; width: 30px; height: 15px; display: inline-block;"></span> Fuera de tiempo para cotizar. 
            </p>
        </div>
    </div>
    </br>
    
    <div class="row">
        <div class="col-xs-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => ['class' => 'table table-responsive'],
                'summary' => '',
                'options' =>[
                    'class' => 'grid',
                ],
                'columns' => [
                    [
                        'attribute' => 'id_solicitud_compra_pieza',
                        'label' => 'No. Solicitud de compra',
                        'format' => 'text',
                        'enableSorting' => false,
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'id_solicitud_compra_pieza',
                            'data' => ArrayHelper::map(SolicitudCompraProveedor::find()->where([ 'id_proveedor' => $id_proveedor ])->all(), 'id_solicitud_compra_pieza', 'id_solicitud_compra_pieza'),
                            'options' => ['placeholder' => 'Selecciona',],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]),
                        'headerOptions' => ['width' => '150',],
                        'contentOptions' => function ($model, $key, $index, $column) {
                            $estado_cotizacion_proveedor = CotizacionProveedor::find()->where(['id_solicitud_compra_proveedor' => $model->id])->one();
                            if($estado_cotizacion_proveedor->estado == 3) {
                                $color = "#c58b8b";
                            }
                            // elseif($estado_cotizacion_proveedor->estado == 2){
                            //     $color = "#b9f3bc";
                            // }
                            else {
                                $color = '';
                            }
                            return  ['style' => 'background-color:' 
                                .       $color.''
                                    ];
                        },
                    ],
                    [
                        'label' => 'Marca',
                        'format' => 'text',
                        'enableSorting' => false,
                        'value' => function($model){
                            $model_solicitud_estacion = SolicitudCompraEstacion::findOne($model->idSolicitudCompraPieza->id_solicitud_compra_estacion);
                            if($model_solicitud_estacion->tipo_solicitud != 3){
                                $model_modelo = Modelo::findOne($model_solicitud_estacion->id_modelo);
                                if ($model_modelo) {
                                    return $model_modelo->idMarca->nombre;
                                } else{
                                    return '-';
                                }
                            }else {
                                return 'Todos (Misceláneos)';
                            }
                        },
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'id_marca',
                            'data' => ArrayHelper::map(Marca::find()->all(), 'id_marca', 'nombre'),
                            'options' => ['placeholder' => 'Selecciona',],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]),
                        'headerOptions' => ['width' => '150',],
                    ],
                    [
                        'label' => 'Modelo',
                        'format' => 'text',
                        'enableSorting' => false,
                        'value' => function($model){
                            $model_solicitud_estacion = SolicitudCompraEstacion::findOne($model->idSolicitudCompraPieza->id_solicitud_compra_estacion);
                            if($model_solicitud_estacion->tipo_solicitud != 3){
                                $model_modelo = Modelo::findOne($model_solicitud_estacion->id_modelo);
                                if ($model_modelo) {
                                    return $model_modelo->nombre;
                                } else{
                                    return '-';
                                }
                            }else {
                                return 'Todos (Misceláneos)';
                            }
                        },
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'id_modelo',
                            'data' => ArrayHelper::map(Modelo::find()->all(), 'id_modelo', 'nombre'),
                            'options' => ['placeholder' => 'Selecciona',],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]),
                        'headerOptions' => ['width' => '150',],
                    ],
                    [
                        'label' => 'Año',
                        'format' => 'raw',
                        'enableSorting' => false,
                        'value' => function($model){
                            $model_solicitud_estacion = SolicitudCompraEstacion::findOne($model->idSolicitudCompraPieza->id_solicitud_compra_estacion);                            
                            if(empty($model_solicitud_estacion->id_vehiculo == NULL)) {
                                $model_vehiculo = Vehiculo::findOne($model_solicitud_estacion->id_vehiculo);                                
                                $data = $model_vehiculo->anho;
                            }else{
                                $data = '-';
                            }
                            return $data;
                        },
                        'filter' => Html::input('text', 'SolicitudCompraProveedorSearch[chasis]', $searchModel->chasis, ['class' => 'form-control',]),
                    ],
                    [
                        'label' => 'Clase',
                        'format' => 'raw',
                        'enableSorting' => false,
                        'value' => function($model){
                            $model_solicitud_estacion = SolicitudCompraEstacion::findOne($model->idSolicitudCompraPieza->id_solicitud_compra_estacion);
                            if(empty($model_solicitud_estacion->id_vehiculo == NULL)) {
                                $model_vehiculo = Vehiculo::findOne($model_solicitud_estacion->id_vehiculo);                                
                                $data = $model_vehiculo->clase;
                            }else{
                                $data = '-';
                            }
                            return $data;
                        },
                        'filter' => Html::input('text', 'SolicitudCompraProveedorSearch[chasis]', $searchModel->chasis, ['class' => 'form-control',]),
                    ],
                    [
                        'label' => 'Chasis',
                        'format' => 'raw',
                        'enableSorting' => false,
                        'value' => function($model){
                            $model_solicitud_estacion = SolicitudCompraEstacion::findOne($model->idSolicitudCompraPieza->id_solicitud_compra_estacion);
                            if(empty($model_solicitud_estacion->id_vehiculo == NULL)) {
                                $model_vehiculo = Vehiculo::findOne($model_solicitud_estacion->id_vehiculo);                                
                                $data = $model_vehiculo->chasis;
                            }else{
                                $data = '-';
                            }
                            return $data;
                        },
                        'filter' => Html::input('text', 'SolicitudCompraProveedorSearch[chasis]', $searchModel->chasis, ['class' => 'form-control',]),
                    ],
                    [  
                        'class' => 'yii\grid\ActionColumn',
                        'header'=>'Acciones',
                        'template' => '{edit}',
                        'buttons' => [
                            'edit' => function($url, $model){
                            	$model_cotizacion_proveedor = CotizacionProveedor::find()->where(['id_solicitud_compra_proveedor' => $model->id])->one();
                                if($model_cotizacion_proveedor->estado == 3) {
                                    $link = "";
                                }
                                // elseif($model_cotizacion_proveedor->estado == 2){
                                //     $link = Html::a('<i class="fa fa-pencil" aria-hidden="true"></i>', ['/solicitud-compra-pieza/cotizacionproveedor', 'id' => $model_cotizacion_proveedor->id, 'origen' => 2 ], ['title' => 'Editar Solicitud', ]);
                                // }
                                else {
                                    $link = Html::a('<i class="fa fa-pencil" aria-hidden="true"></i>', ['/solicitud-compra-estacion/cotizacionproveedor', 'id' => $model_cotizacion_proveedor->id ], ['title' => 'Editar Solicitud', ]);
                                }
                                return $link;
                            },
                        ], 

                    ],
                ],
            ]);  ?>
        </div>
    </div>
</div>