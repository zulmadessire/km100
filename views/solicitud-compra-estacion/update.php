<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SolicitudCompraEstacion */

$this->title = 'Update Solicitud Compra Estacion: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Solicitud Compra Estacions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="solicitud-compra-estacion-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'model_cotizacion_estacion_pieza' => $model_cotizacion_estacion_pieza
    ]) ?>

</div>
