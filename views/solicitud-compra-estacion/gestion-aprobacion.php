<?php
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use app\models\Empresa;
use app\models\Estacion;
use app\models\PipelineCompraPieza;
use app\models\CotizacionTallerPiezaSearch;
use app\models\Variable;

$this->title = ' APROBACIÓN SOLICITUD COMPRA PIEZA';
?>

<div class="gestion-aprobacion">
    <div class="row">
        <div class="col-xs-12 col-md-6">
          <div class="row">
            <div class="col-md-2 col-xs-3">
                <i class="fa fa-check fa-5x" aria-hidden="true"></i>
            </div>
        	<div class="col-md-10 col-xs-9" style="padding-top: 30px;">
        		<h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
        	</div>
          </div>
        </div>
        <?php if (\Yii::$app->user->can('gerente-compras')) : ?>
        <div class="col-xs-12 col-md-6">
        <br>
        <br>
            <ul class="nav nav-pills pull-right">
                <li role="presentation">
                    <?= Html::a('<span><i class="fa fa-list-ul fa-sm" aria-hidden="true"></i></span> Ver cotizaciones',[ 'solicitud-compra-estacion/indexcotizacionesrecibidas', 'id' => $model->id ], ['class' => 'btn btn-small-gris']) ?>
                </li>
            </ul>
        </div>
        <?php endif ?>
    </div>

	</br>
	</br>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <?php
                $pipeline_compra_pieza = PipelineCompraPieza::find()->where([ 'id_solicitud_compra_pieza' => $model->id ])->orderby('id DESC')->all();

                // if ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo < 3 ) {
                //     $searchModel = new CotizacionTallerPiezaSearch(['id_cotizacion_taller' => $model->id_cotizacion_taller]);
                //     $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                // } else{
                //     $searchModel = new CotizacionProveedorPiezaSearch();
                //     $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                //     $dataProvider->query->joinWith('idCotizacionProveedor')->joinWith('idCotizacionProveedor.idSolicitudCompraProveedor')->where('solicitud_compra_proveedor.id_solicitud_compra_pieza = '. $model->id .' AND aprobacion = 2');
                // }

                echo Yii::$app->controller->renderPartial('_pipeline-solicitud-compra-pieza', ['pipeline_compra_pieza' => $pipeline_compra_pieza, /*'dataProvider' => $dataProvider,*/ 'model' => $model ]);
            ?>
        </div>
    </div>
    <?php $form = ActiveForm::begin(['id' => 'gestion-aprobacion-form', 'action' => ['aprobarsolicitud']]); ?>

    <?php 
        if($model_solicitud_compra_estacion->id_vehiculo != null):        
    ?>
    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DEL VEHÍCULO</p>
            <hr>
        </div>
    </div>


    <div class="row">

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_modelo')->textInput(['disabled' => true, 'value' => $model_vehiculo->idModelo->idMarca->nombre ])->label('Marca') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_modelo')->textInput(['value' => strtoupper($model_vehiculo->idModelo->nombre), 'disabled' => true])->label('Modelo') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'chasis')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>

    </div>
    <?php else: ?>
    
    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DEL MODELO DE VEHÍCULO</p>
            <hr>
        </div>
    </div>

    <div class="row">
        <?php if($model_solicitud_compra_estacion->tipo_solicitud != 3){ ?>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_solicitud_compra_estacion, 'id_modelo')->textInput(['disabled' => true, 'value' => $model_solicitud_compra_estacion->idModelo->idMarca->nombre ])->label('Marca') ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_solicitud_compra_estacion, 'id_modelo')->textInput(['disabled' => true, 'value' => $model_solicitud_compra_estacion->idModelo->nombre ])->label('Modelo') ?>
        </div>
        
        <?php }else{ ?>
            <div class="col-xs-12">
                <h4>Aplica para todos los modelos (Misceláneos)</h4>
            </div>
        <?php } ?>
    </div>

    <?php endif ?>
    </br>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DE LA ESTACIÓN</p>
            <hr>
        </div>
    </div>

    <div class="row">

        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <?= Html::label('Cliente', '', ['class' => 'control-label']) ?>
                <?= Html::textInput('', $model_solicitud_compra_estacion->idEstacion->idEmpresa->cliente->nombre_comercial ,['class' => 'form-control', 'disabled' => true,]) ?>
            </div>
        </div>

        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <?= Html::label('Empresa', '', ['class' => 'control-label']) ?>
                <?= Html::textInput('', Empresa::find()->where(['id_empresa' => $model_solicitud_compra_estacion->idEstacion->id_empresa ])->one()->nombre,['class' => 'form-control', 'disabled' => true,]) ?>
            </div>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_solicitud_compra_estacion, 'id_estacion')->textInput(['maxlength' => true, 'disabled' => true, 'value' => $model_solicitud_compra_estacion->idEstacion->nombre])->label('ESTACIÓN') ?>
        </div>

    </div>

    </br>
	</br>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">LISTADO DE PIEZAS</p>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <table class="table grid-blue table-responsive table-bordered">
				<thead>
					<tr>
						<th>Descripción</th>
						<th>Cantidad</th>
						<th>Proveedor</th>
						<th>Garantia</th>
						<th>Fecha entrega</th>
						<th>Costo</th>
					</tr>
				</thead>
				<tbody>
                    <?php $subtotal = 0; ?>    
					<?php foreach ($cotizaciones_piezas as $i => $cotizacion): ?>
						<tr class="row-pieza" data-pieza="<?= $cotizacion->idSolicitudPiezaEstacion->id_pieza ?>">
                            <td width="25%">
                                <?php if($cotizacion->imagen_pieza): ?>
                                <a href='<?= Yii::$app->request->baseUrl.$cotizacion->imagen_pieza ?>' data-lightbox='<?=  Yii::$app->request->baseUrl.$cotizacion->imagen_pieza ?>' data-title=' <?=  $cotizacion->observacion_pieza ?> ' title='<?=  $cotizacion->observacion_pieza ?>'>
                                    <img src= '<?=  Yii::$app->request->baseUrl.$cotizacion->imagen_pieza ?>' style='max-width:20%;' class='<?=  Yii::$app->request->baseUrl.$cotizacion->imagen_pieza ?>' />
                                </a>
                                <?php endif ?>
                                <br>
                                <?= ucfirst($cotizacion->idSolicitudPiezaEstacion->idPieza->nombre) ?>
                                <br>
                                <span style="font-size:10px;"><?=  $cotizacion->observacion_pieza ?></span>
                            </td>
							<td><?= $cotizacion->cantidad_comprar ?></td>
							<td><?= $cotizacion->idCotizacionProveedor->idSolicitudCompraProveedor->idProveedor->nombre ?></td>
                            <td><?= $cotizacion->garantia ?></td>
							<td><?= date('d/m/Y', strtotime($cotizacion->fecha_entrega)) ?></td>
							<td>
                                <?php  
                                    $costo = (($cotizacion->costo * $cotizacion->cantidad_comprar) - ((($cotizacion->costo * $cotizacion->cantidad_comprar) * $cotizacion->ahorro)/ 100));
                                    echo $costo.' RD$ ';
                                ?> 
                            <?= Html::hiddenInput('id[]', $cotizacion->id,[]) ?>
                            </td>
						</tr>
                        <?php $subtotal +=  $costo?>
					<?php endforeach ?>
                    <tr>
                        <td colspan="5" style="text-align: right; font-weight: bold;">SUBTOTAL</td>
                        <td><?= $subtotal.' RD$ ' ?></td>
                    </tr>
                    <tr>
                        <td colspan="5" style="text-align: right; font-weight: bold;">ITBIS</td>
                        <td><?php $itbis = ($subtotal * 0.18); echo $itbis.' RD$ '; ?></td>
                    </tr>
                    <tr>
                        <td colspan="5" style="text-align: right; font-weight: bold;">TOTAL</td>
                        <td><?php $total = ($subtotal + $itbis); echo $total.' RD$ '; ?></td>
                    </tr>
				</tbody>
		    </table>
        </div>
    </div>
	
	</br>
	</br>
    
    <!-- Comparar precio total de las piezas con la vaiable de configuración  -->
    <!-- Monto limite de piezas (mlb)   -->
    <?php
        $variable = Variable::find()->where(['key' => 'mlp'])->one();
        if($total < $variable->value) {
            $monto = 1;
        }else {
            $monto = 2;
            if(\Yii::$app->user->can('gerente-compras')) {
                echo '
                <div class="alert alert-info alert-dismissible">
                    <h4><i class="icon fa fa-info"></i> Alerta!</h4>
                    La orden de compra excede la cantidad máxima establecida en configuración. Pasar la solicitud al director comercial. 
                </div>
                ';
            }
        }
    ?>

    <div class="row">
        <?php if(\Yii::$app->user->can('director-comercial')): ?>
            <div class="col-xs-12">
                <p>
                    <strong>Observación: </strong> <?= $model->observacion_gerente ?>
                </p>
            </div>
        <?php endif ?>
        <div id="observacion" class="col-xs-12">        
            <?php if (\Yii::$app->user->can('gerente-compras') && $monto == 2) : ?>
        	    <?= $form->field($model, 'observacion_gerente')->textarea(['rows' => 3])->label('OBSERVACIÓN GERENTE') ?>
            <?php endif ?>
        </div>
        <div class="col-xs-12">
        	<?= $form->field($model, 'id')->hiddenInput()->label(false) ?>
        	<?= $form->field($model, 'mlp')->hiddenInput((['value'=> $monto]))->label(false) ?>
        </div>
        <div id="rechazar" class="col-xs-12" style="display: none;">
            <?= $form->field($model, 'motivo_rechazo')->textarea(['rows' => 3])->label('MOTIVO DE RECHAZO') ?>
        </div>
        <div class="col-md-3 col-xs-12">
            <?php if(\Yii::$app->user->can('gerente-compras')): ?>
            <?=
                $form->field($model, 'estado')->widget(Select2::classname(), [
                    'data' => ['' => 'Seleccione', '1' => 'Aprobar', '2' => 'Rechazar' ],
                    'options' => ['placeholder' => 'Seleccione', 'id' => 'op'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Aprobar o Rechazar');
            ?>
            <?php endif ?>
            <?php if(\Yii::$app->user->can('director-comercial')): ?>
            <?=
                $form->field($model, 'estado')->widget(Select2::classname(), [
                    'data' => ['' => 'Seleccione', '1' => 'Aprobar', '2' => 'Rechazar compra'],
                    'options' => ['placeholder' => 'Seleccione', 'id' => 'op'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Aprobar o Rechazar');
            ?>
            <?php endif ?>
        </div>
    </div>

    <br>
    <br>

	<div class="row">
        <div id="send" class="col-md-12 col-xs-12">
            <div class="form-group text-center">
                <?= Html::submitButton('Enviar', ['class' => 'btn btn-submit', 'id' => 'guardar-submit', 'value' => 'aprobar']) ?>
            </div>
        </div>
    </div>

	<?php ActiveForm::end(); ?>
	
</div>

<script>
    $('#op').on('change', function(event) {
        if($(this).val() == 2){
            $("#rechazar").show();                      
            $("#observacion").hide();                  
        }else if($(this).val() == 3) {    
            $("#rechazar").hide();        
            $("#observacion").hide();        
        }else {                         
            $("#rechazar").hide();        
            $("#observacion").show();  
        }
    });
</script>