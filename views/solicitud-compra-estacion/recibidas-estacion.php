<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\form\ActiveForm;
use kartik\grid\GridView;
use yii\db\Query;
use kartik\select2\Select2;
use app\models\CotizacionTallerPiezaSearch;
use app\models\CotizacionProveedorPiezaSearch;
use app\models\PipelineCompraPieza;
use app\models\EstadoCompraPieza;
use app\models\DespachoProveedorPieza;
use app\models\SolicitudCompraPieza;
use app\models\Taller;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SolicitudCompraPiezaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'RECEPCIÓN DE PIEZAS ESTACIÓN';
?>
    <?php $form = ActiveForm::begin([ 'id' => 'despacho-pieza-asignadas-form' ]); ?>
<div class="solicitud-compra-pieza-index">

    <div class="row">
        <div class="col-md-1 col-xs-3">
            <i class="fa fa-list-ul fa-3x" aria-hidden="true"></i>
        </div>
        <div class="col-md-10 col-xs-9">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
        </div>
    </div>
    </br>
    </br>
    <div class="row">
        <div class="col-xs-12">
            <h4>Piezas recibidas</h4>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= GridView::widget([
               'dataProvider' => $dataProviderRecibidas,
                // 'filterModel' => $searchModel,
                'tableOptions' => ['class' => 'table table-responsive'],
                'summary' => '',
                'options' =>[
                  'class' => 'grid',
                ],
                'columns' => [
                    [
                       
                        'label' => 'Traslado',
                        'format' => 'text',
                        'value' => function($model){
                            if ( $model->idDespacho )
                                return $model->idDespacho->traslado;
                            else
                                return '-';
                        },
                    ],
                    [                       
                        'label' => 'Fecha de Envío',
                        'format' => 'text',
                        'value' => function($model){
                            if ( $model->idDespacho )
                                return date( 'd/m/Y', strtotime($model->idDespacho->fecha) );
                            else
                                return '-';
                        },
                    ],
                    [
                         
                        'label' => 'Tipo de Pieza',
                        'format' => 'text',
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'filter' => false,
                        'enableSorting' => false,
                         'value' => function($model){

                            $query = new query();
                                $query->select('tp.nombre nombres')
                                    ->from('despacho de')
                                    ->join('INNER JOIN','despacho_proveedor_pieza dpp','de.id = dpp.id_despacho')
                                    ->join('INNER JOIN','cotizacion_proveedor_pieza cpp','cpp.id = dpp.id_cotizacion_proveedor_pieza')
                                    ->join('INNER JOIN','solicitud_pieza_estacion ctp','cpp.id_solicitud_pieza_estacion = ctp.id')
                                    ->join('INNER JOIN','pieza pi','pi.id_pieza = ctp.id_pieza')
                                    ->join('INNER JOIN','tipo_pieza tp','pi.tipo_pieza_id = tp.id')
                                    ->where('dpp.id = '. $model->id );
                               

                                $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                                $piezas = Yii::$app->db->createCommand($sql)->queryOne();
                                return $piezas['nombres'];
                        },
                    ],
                    [                         
                        'label' => 'Pieza',
                        'format' => 'text',
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'filter' => false,
                        'enableSorting' => false,
                         'value' => function($model){

                            $query = new query();
                                $query->select('pi.nombre nombres')
                                    ->from('despacho de')
                                    ->join('INNER JOIN','despacho_proveedor_pieza dpp','de.id = dpp.id_despacho')
                                    ->join('INNER JOIN','cotizacion_proveedor_pieza cpp','cpp.id = dpp.id_cotizacion_proveedor_pieza')
                                    ->join('INNER JOIN','solicitud_pieza_estacion ctp','cpp.id_solicitud_pieza_estacion = ctp.id')
                                    ->join('INNER JOIN','pieza pi','pi.id_pieza = ctp.id_pieza')
                                    ->where('dpp.id = '. $model->id );
                               

                                $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                                $piezas = Yii::$app->db->createCommand($sql)->queryOne();
                                return $piezas['nombres'];
                        },
                    ],
                    [                       
                        'label' => 'Estatus',
                        'format' => 'text',
                        'value' => function ($model) {
                            if ($model->idDespacho->estado == 0) {
                                return 'EN PROCESO';
                            } elseif ($model->idDespacho->estado == 1) {
                                return 'ENVIADO A MENSAJERO';
                            }elseif ($model->idDespacho->estado == 2) {
                                return 'RECIBIDA POR MENSAJERO';
                            }else {
                                return 'ENTREGADA';
                            }
                        },
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'filter' => false,
                        'enableSorting' => false,
                    ],
                    
                    [                         
                        'label' => 'Estación',
                        'format' => 'text',
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'filter' => false,
                        'enableSorting' => false,
                         'value' => function($model){
                            if ( $model->idDespacho->idEstacion)
                                return $model->idDespacho->idEstacion->nombre;
                            else
                                return '-';
                        },
                    ]
                ],
            ]); ?>
        </div>
    </div>
    <br>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <h4>Piezas por recibir</h4>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                // 'filterModel' => $searchModel,
                'tableOptions' => ['class' => 'table table-responsive'],
                'summary' => '',
                'options' =>[
                    'class' => 'grid',
                ],
                'columns' => [
                    [                       
                        'label' => 'Traslado',
                        'format' => 'text',
                        'value' => function($model){
                            if ( $model->idDespacho )
                                return $model->idDespacho->traslado;
                            else
                                return '-';
                        },
                    ],
                    [                       
                        'label' => 'Fecha de Envío',
                        'format' => 'text',
                        'value' => function($model){
                            if ( $model->idDespacho )
                                return date( 'd/m/Y', strtotime($model->idDespacho->fecha) );
                            else
                                return '-';
                        },
                    ],
                    [                         
                        'label' => 'Tipo de Pieza',
                        'format' => 'text',
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'filter' => false,
                        'enableSorting' => false,
                         'value' => function($model){

                            $query = new query();
                                $query->select('tp.nombre nombres')
                                    ->from('despacho de')
                                    ->join('INNER JOIN','despacho_proveedor_pieza dpp','de.id = dpp.id_despacho')
                                    ->join('INNER JOIN','cotizacion_proveedor_pieza cpp','cpp.id = dpp.id_cotizacion_proveedor_pieza')
                                    ->join('INNER JOIN','solicitud_pieza_estacion ctp','cpp.id_solicitud_pieza_estacion = ctp.id')
                                    ->join('INNER JOIN','pieza pi','pi.id_pieza = ctp.id_pieza')
                                    ->join('INNER JOIN','tipo_pieza tp','pi.tipo_pieza_id = tp.id')
                                    ->where('dpp.id = '. $model->id );
                               

                                $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                                $piezas = Yii::$app->db->createCommand($sql)->queryOne();
                                return $piezas['nombres'];
                        },
                    ],
                    [                         
                        'label' => 'Pieza',
                        'format' => 'text',
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'filter' => false,
                        'enableSorting' => false,
                         'value' => function($model){


                            $query = new query();
                                $query->select('pi.nombre nombres')
                                    ->from('despacho de')
                                    ->join('INNER JOIN','despacho_proveedor_pieza dpp','de.id = dpp.id_despacho')
                                    ->join('INNER JOIN','cotizacion_proveedor_pieza cpp','cpp.id = dpp.id_cotizacion_proveedor_pieza')
                                    ->join('INNER JOIN','solicitud_pieza_estacion ctp','cpp.id_solicitud_pieza_estacion = ctp.id')
                                    ->join('INNER JOIN','pieza pi','pi.id_pieza = ctp.id_pieza')
                                    ->where('dpp.id = '. $model->id );
                               

                                $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                                $piezas = Yii::$app->db->createCommand($sql)->queryOne();
                                return $piezas['nombres'];
                        },
                    ],
                    [
                       
                        'label' => 'Estatus',
                        'format' => 'text',
                        'value' => function ($model) {
                            if ($model->idDespacho->estado == 0) {
                                return 'EN PROCESO';
                            } elseif ($model->idDespacho->estado == 1) {
                                return 'ENVIADO A MENSAJERO';
                            }elseif ($model->idDespacho->estado == 2) {
                                return 'RECIBIDA POR MENSAJERO';
                            }else {
                                return 'ENTREGADA';
                            }
                        },
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'filter' => false,
                        'enableSorting' => false,
                    ],
                    [                         
                        'label' => 'Estación',
                        'format' => 'text',
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'filter' => false,
                        'enableSorting' => false,
                         'value' => function($model){
                            if ( $model->idDespacho->idEstacion)
                                return $model->idDespacho->idEstacion->nombre;
                            else
                                return '-';
                        },
                    ],
                    
                    // [
                    // 	'attribute' => 'cantidad_recibida',
                    //     'label' => 'Cantidad recibida',
                    //     'format' => 'raw',
                    //     'filter' => false,
                    //     'enableSorting' => false,
                    //     'value' => function( $model ){
                    //         return Html::textInput('recibido[]', $model->cantidad_recibida_taller,['class' => 'form-control', 'type' => 'text', 'style' => 'text-align:center; font-size: 10pt; float: left;', 'required' => true ]).
                    //         Html::hiddenInput('pieza_id[]', $model->id, ['class' => 'pieza_id', 'readonly' => true ]);;
                    //     },
                    //     'contentOptions'=>['style'=>'text-align: center;'],
                    //     'headerOptions' => ['width' => '150',],
                    // ],
                    [
                        'class' => 'yii\grid\CheckboxColumn',
                        'checkboxOptions'=>[
                            'style'=>'display: block;margin-right: auto;margin-left: auto;',
                        ],
                        'header' => Html::checkBox('selection_all', false, [
                            'class' => 'select-on-check-all hidden',
                            'label' => 'Recibidas',
                        ]),
                    ], 
              ],
          ]); ?>
        </div>
    </div>
    </br>
    </br>
    </br>
    </br>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="form-group text-center">
                <?= Html::a('REGRESAR', ['/solicitud-compra-estacion/listado' ], ['class' => 'btn btn-cancelar']) ?>&nbsp;
                <?= Html::submitButton('ENVIAR', ['class' => 'btn btn-aceptar', 'id' => 'pieza-submit']) ?>
            </div>
        </div>
    </div>
 <?php ActiveForm::end(); ?>
<style>
    .grid-blue .table > tbody > tr.active > td {
        background-color: #b9f3bc;
    }
</style>

<script>
    //Cambiando el background
    $('input[name="selection[]"]').on('change', function(event) {
        event.preventDefault();
        /* Act on the event */
        if ( this.checked ) {
            $('tr[data-key="'+$(this).val()+'"]').addClass('active');
  
        } else{
            $('tr[data-key="'+$(this).val()+'"]').removeClass('active');
            
        }
    });

    
    $('.table').removeClass( "table-bordered table-striped" );
</script>



 