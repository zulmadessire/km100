<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\form\ActiveForm;
use kartik\grid\GridView;
use kartik\select2\Select2;
use app\models\CotizacionTallerPiezaSearch;
use app\models\CotizacionProveedorPiezaSearch;
use app\models\PipelineCompraPieza;
use app\models\EstadoCompraPieza;
use app\models\DespachoProveedorPieza;
use app\models\SolicitudCompraPieza;
use app\models\Taller;
use app\models\Estacion;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SolicitudCompraPiezaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'LISTADO DE DESPACHOS ASIGNADOS';
?>
<div class="solicitud-compra-pieza-index">

    <div class="row">
        <div class="col-md-1 col-xs-3">
            <i class="fa fa-list-ul fa-3x" aria-hidden="true"></i>
        </div>
        <div class="col-md-10 col-xs-9">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
        </div>
    </div>
    </br>
    </br>
  <div class="row">
        <div class="col-md-12">
            <?= GridView::widget([
               'dataProvider' => $dataProvider,
              // 'filterModel' => $searchModel,
                     'tableOptions' => ['class' => 'table table-responsive'],
                     'summary' => '',
                          'options' =>[
                              'class' => 'grid',
                          ],
            'columns' => [
                    [
                        'attribute' => 'id_solicitud_compra_pieza',
                        'label' => 'No. Solicitud de compra',
                        'format' => 'text',
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'filter' => false,
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'estado',
                        'label' => 'Estatus',
                        'format' => 'text',
                        'value' => function ($model) {
                            if ($model->estado == 0) {
                                return 'EN PROCESO';
                            } elseif ($model->estado == 1) {
                                return 'ENVIADO A MENSAJERO';
                            }elseif ($model->estado == 2) {
                                return 'RECIBIDA POR MENSAJERO';
                            }else {
                                return 'ENTREGADA';
                            }
                        },
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'filter' => false,
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'fecha',
                        'label' => 'Fecha de Envío',
                        'format' => 'text',
                        'value' => function($model){
                            if ( $model->fecha )
                                return date( 'd/m/Y', strtotime($model->fecha) );
                            else
                                return '-';
                        },
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'enableSorting' => false,
                    ],
                    [
                        'label' => 'No. de Piezas',
                        'format' => 'text',
                        'value' => function($model){
                            $cant = count(DespachoProveedorPieza::find()->where(['id_despacho' => $model->id])->all());
                            return $cant;
                        },
                        'contentOptions'=>['style'=>'text-align:center;'],
                        //'headerOptions' => ['width' => '300',],
                    ],
                    [
                        'label' => 'Estacion',
                        'format' => 'text',
                        'value' => function($model){
                            $estacion = Estacion::find()->where(['id_estacion' => $model->id_estacion])->one();
                            $nombre = $estacion->nombre .' - '.$estacion->idEmpresa->nombre;
                            return $nombre;
                        },
                        'contentOptions'=>['style'=>'text-align:center;'],
                        //'headerOptions' => ['width' => '300',],
                    ],
                  [  
                                  'class' => 'yii\grid\ActionColumn',
                                  //'contentOptions' => ['style' => 'width:260px;'],
                                  'header'=>'Acciones',
                                  'template' => '{menu}',
                                  'buttons' => [

                                      //view button
                                      'menu' => function ($url, $model) {

                                           return  '<div class="dropdown">
                                                    <button class="btn btn-default dropdown-toggle btn-sm" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i>
                                                    <span class="caret"></span></button>
                                                    <ul class="dropdown-menu">'.
                                                    (( $model->estado == 1 ||  $model->estado ==2 )?
                                                        '<li>'.Html::a('<span><i class="fa fa-eye fa-sm" aria-hidden="true"></i></span> Piezas pendientes por recibir',[ '/solicitud-compra-estacion/asignadas', 'id' => $model->id ]).' </li>'
                                                        : '').
                                                    (( $model->estado == 2 )?
                                                       '<li>'.Html::a('<span><i class="fa fa-eye fa-sm" aria-hidden="true"></i></span> Checklist piezas estacion',[ '/solicitud-compra-estacion/piezas-recibidas', 'id' => $model->id ]).' </li>'
                                                    : '')                                                       
                                                       .'</ul>
                                                  </div>';


                                      },
                                  ],

                             ],

               

              ],
          ]); ?>
        </div>
    </div>




 