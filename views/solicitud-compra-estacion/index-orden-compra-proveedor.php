<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\db\Query;
use app\models\Vehiculo;
use app\models\Marca;
use app\models\Modelo;
use app\models\Servicio;
use app\models\SolicitudCompraPieza;
use app\models\OrdenCompraPieza;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
use kartik\date\DatePicker;
use app\models\CotizacionProveedor;
use app\models\SolicitudCompraProveedor;
use app\models\SolicitudCompraEstacion;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PartesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'LISTADO DE ORDENES DE COMPRA ESTACIÓN';
?>
<div class="index-orden-compra">

    <div class="row">
        <div class="col-md-1 col-xs-3">
            <i class="fa fa-list-ul fa-3x" aria-hidden="true"></i>
        </div>
        <div class="col-md-10 col-xs-9">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
        </div>
        <div class="col-xs-12">
        </br>
        </br>
          <p> <span style= "background-color: #c58b8b; width: 30px; height: 15px; display: inline-block;"></span> Ordenes de compras sin factura asociada </p>
        </div>
    </div>
    </br>
    
    <div class="row">
        <div class="col-xs-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => ['class' => 'table table-responsive'],
                'summary' => '',
                'options' =>[
                    'class' => 'grid',
                ],
                'columns' => [
                    [
                        'attribute' => 'id',
                        'label' => 'No. De Orden de compra',
                        'format' => 'text',
                        'enableSorting' => false,
                        /*'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'id_solicitud_compra_pieza',
                            'data' => ArrayHelper::map(SolicitudCompraProveedor::find()->where([ 'id_proveedor' => $id_proveedor ])->all(), 'id_solicitud_compra_pieza', 'id_solicitud_compra_pieza'),
                            'hideSearch' => true,
                            'options' => ['placeholder' => 'Selecciona',],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]),*/
                        'headerOptions' => ['width' => '150',],
                        'contentOptions' => function ($model, $key, $index, $column) {
                            return  ['style' => 'background-color:' 
                                .       ($model->estado == 1 ? '' : '#c58b8b')
                                    ];
                        },
                    ],
                    [
                        'label' => 'Marca',
                        'format' => 'text',
                        'enableSorting' => false,
                        'value' => function($model){
                            $id_solicitud_estacion = SolicitudCompraPieza::findOne($model->id_solicitud_compra_pieza)->id_solicitud_compra_estacion;
                            $model_solicitud_estacion = SolicitudCompraEstacion::findOne($id_solicitud_estacion);
                            if($model_solicitud_estacion->tipo_solicitud != 3){
                                $model_modelo = Modelo::findOne($model_solicitud_estacion->id_modelo);
                                return $model_modelo->nombre;
                            }else {
                                    return 'Todos (Misceláneos)';
                            }
                        },
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'id_marca',
                            'data' => ArrayHelper::map(Marca::find()->all(), 'id_marca', 'nombre'),
                            'options' => ['placeholder' => 'Selecciona',],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]),
                        'headerOptions' => ['width' => '150',],
                    ],
                    [
                        'label' => 'Modelo',
                        'format' => 'text',
                        'enableSorting' => false,
                        'value' => function($model){
                            $id_solicitud_estacion = SolicitudCompraPieza::findOne($model->id_solicitud_compra_pieza)->id_solicitud_compra_estacion;
                            $model_solicitud_estacion = SolicitudCompraEstacion::findOne($id_solicitud_estacion);
                            if($model_solicitud_estacion->tipo_solicitud != 3){
                                $model_modelo = Modelo::findOne($model_solicitud_estacion->id_modelo);
                                return $model_modelo->nombre;
                            }else {
                                    return 'Todos (Misceláneos)';
                            }
                        },
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'id_modelo',
                            'data' => ArrayHelper::map(Modelo::find()->all(), 'id_modelo', 'nombre'),
                            'options' => ['placeholder' => 'Selecciona',],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]),
                        'headerOptions' => ['width' => '150',],
                    ],
                    [
                        'label' => 'Año',
                        'format' => 'raw',
                        'enableSorting' => false,
                        'value' => function($model){
                            $id_solicitud_estacion = SolicitudCompraPieza::findOne($model->id_solicitud_compra_pieza)->id_solicitud_compra_estacion;
                            $model_solicitud_estacion = SolicitudCompraEstacion::findOne($id_solicitud_estacion);
                            if(empty($model_solicitud_estacion->id_vehiculo == NULL)) {
                                $model_vehiculo = Vehiculo::findOne($model_solicitud_estacion->id_vehiculo);                                
                                $data = $model_vehiculo->anho;
                            }else{
                                $data = '-';
                            }
                            return $data;
                        },
                        'filter' => Html::input('text', 'OrdenCompraPiezaSearch[anho]', $searchModel->anho, ['class' => 'form-control',]),
                    ],
                    [
                        'label' => 'Clase',
                        'format' => 'raw',
                        'enableSorting' => false,
                        'value' => function($model){
                            $id_solicitud_estacion = SolicitudCompraPieza::findOne($model->id_solicitud_compra_pieza)->id_solicitud_compra_estacion;
                            $model_solicitud_estacion = SolicitudCompraEstacion::findOne($id_solicitud_estacion);
                            if(empty($model_solicitud_estacion->id_vehiculo == NULL)) {
                                $model_vehiculo = Vehiculo::findOne($model_solicitud_estacion->id_vehiculo);                                
                                $data = $model_vehiculo->clase;
                            }else{
                                $data = '-';
                            }
                            return $data;
                        },
                        'filter' => Html::input('text', 'OrdenCompraPiezaSearch[clase]', $searchModel->clase, ['class' => 'form-control',]),
                    ],
                    [
                        'label' => 'Chasis',
                        'format' => 'raw',
                        'enableSorting' => false,
                        'value' => function($model){
                            $id_solicitud_estacion = SolicitudCompraPieza::findOne($model->id_solicitud_compra_pieza)->id_solicitud_compra_estacion;
                            $model_solicitud_estacion = SolicitudCompraEstacion::findOne($id_solicitud_estacion);
                            if(empty($model_solicitud_estacion->id_vehiculo == NULL)) {
                                $model_vehiculo = Vehiculo::findOne($model_solicitud_estacion->id_vehiculo);                                
                                $data = $model_vehiculo->chasis;
                            }else{
                                $data = '-';
                            }
                            return $data;
                        },
                        'filter' => Html::input('text', 'OrdenCompraPiezaSearch[chasis]', $searchModel->chasis, ['class' => 'form-control',]),
                    ],
                    [  
                            'class' => 'yii\grid\ActionColumn',
                            'header'=>'Acciones',
                            'template' => '{menu}',
                            'buttons' => [

                                //menu button
                                'menu' => function ($url, $model) {
                                    $model_orden_compra_pieza = OrdenCompraPieza::findOne($model->id);
                                    $monto = $model_orden_compra_pieza->total;
                                     return '<div class="dropdown">
                                                <button class="btn btn-default dropdown-toggle btn-sm" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i>
                                                <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li>'.Html::a('<span><i class="fa fa-eye fa-sm" aria-hidden="true"></i></span> Ver orden de compra',[ '/solicitud-compra-estacion/imprimirorden', 'id' => $model->id ], ['title' => 'Ver orden de compra', 'target' => '_blank']).' </li>
                                                    <li>'.Html::a('<span><i class="fa fa-pencil fa-sm" aria-hidden="true"></i></span> Ver cotización',[ '/solicitud-compra-estacion/cotizacionproveedor', 'id' => $model->id_cotizacion_proveedor, 'origen' => 2], ['title' => 'Ver cotización']).' </li>
                                                    '.(( $model->estado == 0 )?
                                                    '<li>'.Html::a('<span><i class="fa fa-file-text-o" aria-hidden="true"></i></span> Asociar Factura', '#', ['title' => 'Asociar Factura', 'data' => ['id' => $model->id, 'monto' => $monto,  'toggle' => 'modal', 'target' => '#facturaModal'] ]).' </li>'
                                                    : '')                                                    
                                                .'</ul>
                                            </div>';
                                },
                            ],

                        ],
                ],
            ]);  ?>
        </div>
    </div>
</div>

<!-- Modal Factura -->
<div id="facturaModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="title"><img src="<?= Yii::$app->request->baseUrl ?>/img/invoice_24.png" alt="" style="height: 40px;">&nbsp;&nbsp; ASOCIAR FACTURA A OC</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true" style="color: #000; font-weight: bold;"></i></button>
          </div>
        <div class="modal-body">
            </br>
            <?php $form = ActiveForm::begin([ 'id' => 'factura', 'action' => ['/solicitud-compra-estacion/asociarfactura',] ]); ?>
            <div class="row">
                <div class="col-xs-12 col-md-5 col-md-offset-1">
                    <?= $form->field($model_orden_compra_factura, 'numero_factura')->textInput(['maxlength' => true])->label('NÚMERO DE FACTURA'); ?>
                </div>
                <div class="col-xs-12 col-md-5">
                    <?= $form->field($model_orden_compra_factura, 'nic')->textInput(['maxlength' => true])->label('NCF'); ?>
                </div>
                <div class="clearfix"></div>
                <div class="col-xs-12 col-md-5 col-md-offset-1">
                    <?=
                        $form->field($model_orden_compra_factura, 'fecha_emision')->widget(DatePicker::classname(), [
                            'options' => ['placeholder' => 'Selecciona la fecha'],
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'yyyy-mm-dd',
                                'orientation' => 'bottom left',
                            ]
                        ])->label('FECHA DE EMISIÓN');
                    ?>
                </div>
                <div class="col-xs-12 col-md-5">
                    <?=
                        $form->field($model_orden_compra_factura, 'fecha_vencimiento')->widget(DatePicker::classname(), [
                            'options' => ['placeholder' => 'Selecciona la fecha'],
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'yyyy-mm-dd',
                                'orientation' => 'bottom left',
                            ]
                        ])->label('FECHA DE VENCIMIENTO');
                    ?>
                </div>
                <div class="clearfix"></div>
                <div class="col-xs-12 col-md-5 col-md-offset-1">
                    <?= $form->field($model_orden_compra_factura, 'condicion_pago')->textInput(['maxlength' => true])->label('CONDICIÓN DE PAGO'); ?>
                </div>
                <div class="col-xs-12 col-md-5">
                    <?= $form->field($model_orden_compra_factura, 'monto', ['addon' => ['prepend' => ['content'=>'<i class="fa fa-usd" aria-hidden="true"></i>']]] )->textInput(['disabled' => true])->label('MONTO') ?>
                </div>
            </div>
            </br>
            <?php ActiveForm::end(); ?>
            <?php
            echo '<div class="row" style="padding-top: 25px;">';
                echo '<div class="text-center">';
                    echo Html::button('CANCELAR', ['class' => 'btn btn-cancelar', 'data' => ['dismiss' => 'modal']]);
                    echo "&nbsp;";
                    echo Html::submitButton('REGISTRAR', ['class' => 'btn btn-aceptar', 'form' => 'factura', 'id' => 'factura-submit']);
                echo '</div>';
            echo '</div>';
            ?>
        </div>
    </div>
  </div>
</div>
<!-- Fin modal-->

<style>
    .modal-dialog {
        margin: 80px auto;
    }
</style>

<script>    
    var buttonAux =0;
    $('#facturaModal').on('show.bs.modal', function (event) {
        if(buttonAux==0){
            buttonAux = $(event.relatedTarget);
        } 
        var id = buttonAux.data('id');
        var monto = buttonAux.data('monto');
        $('#ordencomprafactura-monto').val(monto);
        $('#ordencomprafactura-monto').data('monto', monto);
        $('#factura-submit').data('id', id);
    });

    $('#facturaModal').on('hidden.bs.modal', function () {
        buttonAux =0;
    })

    $('#factura').on('submit', function(event) {
        event.preventDefault();
        event.stopImmediatePropagation();
        buttonAux =0;
        $('#facturaModal').modal('hide')
        var id = $('#factura-submit').data('id');
        var monto = $('#ordencomprafactura-monto').data('monto');

        var form = $(this);
        var data = form.serializeArray();      
        data.push({name: 'id', value: id});
        data.push({name: 'monto', value: monto});

        $.post( form.attr("action"), data ).done(function(response){
            console.log(response);
        });
    });
</script>