<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SolicitudCompraEstacion */

$this->title = 'Solicitud Compra Pieza';
if($type == 1) {
	$this->title = 'Solicitud Compra Stock Mantenimiento';
} else if($type == 2) {
    $this->title = 'Solicitud Compra MISCELÁNEOS';
} else if($type == 3) {
    $this->title = 'Solicitud Compra Neumáticos';
} else if($type == 4) {
    $this->title = 'Solicitud Compra BATERÍAS';
}
// $this->params['breadcrumbs'][] = ['label' => 'Solicitud Compra Estacions', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="solicitud-compra-estacion-create">

    <div class="row">
        <div class="col-md-1 col-xs-3">
            <h4 class="titulo"><i class="fa fa-file-text fa-4x" aria-hidden="true"></i></h4>
        </div>
        <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
            <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
        </div>
    </div>
    </br>

    <div class="row">
		<div class="col-xs-12">
			<div class="square" >
	            <ul>
	                <li>
	                    <a href="<?= Yii::$app->request->baseUrl ?>/solicitud-compra-estacion/create/" <?= ($type == 0)?'class="active"':'' ?>>
	                        <i class="fa fa-wrench" aria-hidden="true"></i>
	                        <p style="font-size:11px;">Compra pieza</p>
	                    </a>
	                </li>
	                <li>
	                    <a href="<?= Yii::$app->request->baseUrl ?>/solicitud-compra-estacion/create/?type=1" <?= ($type == 1)?'class="active"':'' ?>>
	                        <i class="fa fa-cog" aria-hidden="true"></i>
	                        <p style="font-size:11px;">Stock de mantenimiento</p>
	                    </a>
	                </li>
	                <li>
	                    <a href="<?= Yii::$app->request->baseUrl ?>/solicitud-compra-estacion/create/?type=2" <?= ($type == 2)?'class="active"':'' ?>>
	                        <i class="fa fa-fire-extinguisher" aria-hidden="true"></i>
	                        <p style="font-size:11px;">Misceláneos</p>
	                    </a>
	                </li>
	                <li>
	                    <a href="<?= Yii::$app->request->baseUrl ?>/solicitud-compra-estacion/create/?type=3" <?= ($type == 3)?'class="active"':'' ?>>
	                        <i class="fa fa-circle" aria-hidden="true"></i>
	                        <p style="font-size:11px;">Neumáticos</p>
	                    </a>
	                </li>  
	                <li>
	                    <a href="<?= Yii::$app->request->baseUrl ?>/solicitud-compra-estacion/create/?type=4" <?= ($type == 4)?'class="active"':'' ?>>
	                        <i class="fa fa-battery-half" aria-hidden="true"></i>
	                        <p style="font-size:11px;">Baterías</p>
	                    </a>
	                </li>
	            </ul>
        	</div>
		</div>
	</div>
	</br>
	</br>

    <div class="row">
		<div class="col-xs-12">
			<?php if ( $type == 0 ) { ?>
                <?= $this->render('_form', [
                    'model' => $model,
                    'model_cotizacion_estacion_pieza' => $model_cotizacion_estacion_pieza,
	                'cliente' => $cliente,
	                'empresa' => $empresa,
	                'estaciones' => $estaciones,
	                'vehiculos' => $vehiculos,
                ]) ?>
			<?php } else if( $type == 1 ){ ?>
                <?= $this->render('_form-stock', [
                    'model' => $model,
                    'model_cotizacion_estacion_pieza' => $model_cotizacion_estacion_pieza,
	                'cliente' => $cliente,
	                'empresa' => $empresa,
	                'estaciones' => $estaciones,
                ]) ?>
			<?php } else if( $type == 2 ){ ?>
                <?= $this->render('_form-miscelaneos', [
                    'model' => $model,
                    'model_cotizacion_estacion_pieza' => $model_cotizacion_estacion_pieza,
	                'cliente' => $cliente,
	                'empresa' => $empresa,
	                'estaciones' => $estaciones,
                ]) ?>
			<?php } else if( $type == 3 ){ ?>
                <?= $this->render('_form-neumaticos', [
                    'model' => $model,
                    'model_cotizacion_estacion_pieza' => $model_cotizacion_estacion_pieza,
	                'cliente' => $cliente,
	                'empresa' => $empresa,
	                'estaciones' => $estaciones,
	                'vehiculos' => $vehiculos,
                ]) ?>
			<?php } else if( $type == 4 ){ ?>
                <?= $this->render('_form-baterias', [
                    'model' => $model,
                    'model_cotizacion_estacion_pieza' => $model_cotizacion_estacion_pieza,
	                'cliente' => $cliente,
	                'empresa' => $empresa,
	                'estaciones' => $estaciones,
	                'vehiculos' => $vehiculos,
                ]) ?>
			<?php }?>
			
		</div>
	</div>

    <?php 
        // $this->render('_form', [
        // 'model' => $model,
        // 'model_cotizacion_estacion_pieza' => $model_cotizacion_estacion_pieza
        // ]) 
    ?>

</div>
