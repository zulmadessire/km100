<?php
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\select2\Select2;

$this->title = 'COTIZACIONES DE RECIBIDAS';
/*echo "<pre>";
print_r($cotizaciones_piezas);
echo "</pre>";*/
$id = Yii::$app->request->get('id');

?>

<div class="cotizaciones-recibidas">
	<div class="row">
		<div class="col-md-1 col-xs-3">
			<h4 class="titulo"><img src="<?= Yii::$app->request->baseUrl ?>/img/hand_money_64.png" alt=""></h4>
		</div>
		<div class="col-md-10 col-xs-9" style="padding-top: 30px;">
			<h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
		</div>
	</div>
	</br>
	<div class="row">
		<div class="col-md-4 col-xs-12">
			<div class="form-group">
                <label class="control-label" for="id_estado">FILTRAR POR PIEZA</label>
                <?=  
                    Select2::widget([
                        'name' => 'id_pieza',
                        'data' => $piezas,
                        'options' => ['placeholder' => 'Selecciona la pieza', 'id' => 'id-pieza'],
                        'pluginOptions' => [ 'allowClear'=>true ],
                    ]); 
                ?>
            </div>
		</div>
        <div class="col-md-8 col-xs-12">
            <div class="pull-right" style="margin-top: 30px;">
                <?= Html::a('Ver solicitud',[ '/solicitud-compra-estacion/gestionaprobacion/'.$id ], ['class' => 'btn btn-small-gris']) ?> 
			</div>
        </div>
    </div>
	</br>
	<?php $form = ActiveForm::begin(['id' => 'cotizaciones-recibidas-form']); ?>
	<div class="row">
		<div class="col-xs-12">
			<?php
				if($model->aprobacion == 5):
					echo Html::a('Ver cotizacion rechazada.','#', ['class' => 'btn btn-small-gris analista-mtto', 'data' => ['id' => $model->id, 'toggle' => 'modal', 'target' => '#cotizacionRechazada']]);
				endif;
			?>
			<br>
			<br>
		</div>
		<input type="text" id="count-piezas" value="<?= count($piezas); ?>" hidden="true">

        <?php 
            foreach ($cotizaciones_piezas as $key => $cotizacion): 
                if(isset($cotizacion->aprobacion)):
        ?>
            
			<div id="prove-<?= $cotizacion->idCotizacionProveedor->idSolicitudCompraProveedor->idProveedor->id ?>" class="col-md-6 col-xs-12 pieza-content"  data-pieza="<?= $cotizacion->idSolicitudPiezaEstacion->id_pieza ?>">
				<div class="panel panel-default">
				  	<div class="panel-heading text-center pieza-<?= $cotizacion->idSolicitudPiezaEstacion->id_pieza ?> pp-<?= $cotizacion->idCotizacionProveedor->idSolicitudCompraProveedor->idProveedor->id."-".$cotizacion->idSolicitudPiezaEstacion->id_pieza ?>"  data-id="<?= $cotizacion->id ?>" data-estado="<?= $cotizacion->idCotizacionProveedor->estado ?>" data-idpieza="<?= $cotizacion->idSolicitudPiezaEstacion->id_pieza ?>">
				  		<b> <?= $cotizacion->idCotizacionProveedor->idSolicitudCompraProveedor->idProveedor->nombre ?> </b>
						<input type="checkbox" id="check-<?= $cotizacion->id?>" name="selected[]" value="<?= $cotizacion->id ?>" hidden="true">
				  		<span class="pull-right" id="icon-<?= $cotizacion->id?>" hidden="true"><i class="fa fa-check-circle fa-lg" aria-hidden="true" style="color: #4b8723; background-color: #c5e0b4;"></i></span>
				  	</div>
		    		<table class="table table-bordered">
		    			<tbody>
		    				<tr>
		    					<td colspan="3"><b>Pieza: </b> <?= strtoupper($cotizacion->idSolicitudPiezaEstacion->idPieza->nombre) ?> </td>
		    				</tr>
		    				<tr>
		    					<td><b>% Descuento: </b> <?= ($cotizacion->ahorro)?$cotizacion->ahorro:'<span class="sin-respuesta">SIN RESPUESTA</span>' ?></td>
		    					<td><b>Precio: </b> <?= ($cotizacion->costo)?'RD$ '.$cotizacion->costo:'<span class="sin-respuesta">SIN RESPUESTA</span>' ?> </td>
		    					<td><b>Precio final: </b> <?= ($cotizacion->costo)?'RD$ '.(($cotizacion->costo) - (($cotizacion->costo * $cotizacion->ahorro)/ 100)).' RD$ ':'<span class="sin-respuesta">SIN RESPUESTA</span>' ?> </td>
		    				</tr>
		    				<tr>
		    					<td><b>Fecha de entrega: </b> <?= ($cotizacion->fecha_entrega)?date('d/m/Y', strtotime($cotizacion->fecha_entrega)):'<span class="sin-respuesta">SIN RESPUESTA</span>' ?> </td>
		    					<td><b>Garantía: </b> <?= ($cotizacion->garantia)?$cotizacion->garantia:'<span class="sin-respuesta">SIN RESPUESTA</span>' ?> </td>
		    					<td><b>Entrega a domicilio: </b> <?= $cotizacion->idCotizacionProveedor->idSolicitudCompraProveedor->idProveedor->domicilio ?> </td>
		    				</tr>
		    				<tr>
		    					<td><b>Cantidad disponible: </b> <?= ($cotizacion->cantidad_disponible)?$cotizacion->cantidad_disponible:0 ?> </td>
		    					<td colspan="2"><div class="col-xs-5"><b>Cantidad a comprar: </b></div> <div class="col-xs-4"><?= Html::textInput('comprar[]', $cotizacion->cantidad_solicitada,['class' => 'form-control pieza-'.$cotizacion->idSolicitudPiezaEstacion->id_pieza.'', 'type' => 'number', 'disabled' => true ]) ?></div></td>
		    				</tr>
		    			</tbody>
			    	</table>
				</div>
			</div>
        <?php 
                endif;
            endforeach; 
        ?>		
	</div>
	</br>

	
</div>

<!-- Fin modal-->
<?php ActiveForm::end(); ?>



<style>
	.panel{
		border-radius: 0px;
	}

	.panel .panel-heading{
		font-size: 12pt;
	}

	.sin-respuesta{
		color: #c51414;
		font-weight: bold;
	}

	.panel-heading{
	    padding: 5px 5px;
	}

	.panel-heading.selected {
		background-color: #c5e0b4 !important;
	}

	.panel-heading.sugerido {
		background-color: #33adff;
	}

	.modal-dialog {
        margin: 80px auto;
    }

</style>

<script>



	$('#guardar-submit').on('click', function(event) {
		event.preventDefault();
		/* Act on the event */
		var total = $("input[name='selected[]']:checked").length;
		var countpiezas = $('#count-piezas').val();
		if (total < countpiezas) {
			alert('Debes Selecionar todas las piezas');
		} else{
			$(this).submit();
		}
	});

	// $('#guardar-submit').on('click', function(event) {
	// 	event.preventDefault();
	// 	/* Act on the event */
	// 	var total = $("input[name='selected[]']:checked").length;

	// 	if (total < 3) {
	// 		$('#observacionModal').modal('show');
	// 	} else{
	// 		$(this).submit();
	// 	}
	// });

	$('#id-pieza').on('change', function(event) {
		event.preventDefault();
		/* Act on the event */
		var id_pieza = $(this).val();
		$('.pieza-content').hide();
		if ( id_pieza == '') {
			$('.pieza-content').show();
		} else{
			$('div[data-pieza='+id_pieza+']').show();
		}
	});


	$('#solicitarCotizacionModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var id = button.data('id');
        $('#submit-solicitar').data('id', id);
        $.ajax({
            url: "../getproveedores",
            type: 'post',
            data: {
                id: id,
            },
            success: function (response) {
                $('#response-proveedor').empty();
                for (var i = 0; i < response.length; i++) {
                    $('#response-proveedor').append(
                        '<div class="col-md-6 col-xs-12">'+
                            '<label class="checkbox-inline">'+
                                '<input type="checkbox" value="'+response[i]['id']+'" name="proveedor[]"> '+response[i]['nombre']+
                            '</label>'+
                        '</div>'
                    );
                }
            }
        });
    });

    $('#solicitar').on('submit', function(event) {
        event.preventDefault();
        event.stopImmediatePropagation();

        $('#submit-solicitar').attr({
            disabled: 'true'
        });

        $('#solicitarCotizacionModal').modal('hide');
        var id = $('#submit-solicitar').data('id');

        var form = $(this);
        var data = form.serializeArray();      
        data.push({name: 'id', value: id});

        $.post( form.attr("action"), data ).done(function(response){
            location.reload();
        });
    });
</script>

