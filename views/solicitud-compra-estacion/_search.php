<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SolicitudCompraEstacionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="solicitud-compra-estacion-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'tipo_solicitud') ?>

    <?= $form->field($model, 'id_estacion') ?>

    <?= $form->field($model, 'ficha') ?>

    <?= $form->field($model, 'placa') ?>

    <?php // echo $form->field($model, 'id_modelo') ?>

    <?php // echo $form->field($model, 'fecha_registro') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
