<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Marca;
use app\models\Modelo;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Mensajero */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mensajero-form">

    <?php $form = ActiveForm::begin(); ?>


            <div class="row">
   
        <div class="col-md-4">
            <?= $form->field($model, 'nombre')->textInput(['maxlength' => true])->label('NOMBRE <span class="obligatorio">*&nbsp;</span>');  ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'cedula')->textInput(['maxlength' => true])->label('CÉDULA ');  ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'telefono')->textInput(['maxlength' => true])->label('TELÉFONO');  ?>
        </div>
    </div>
    <div class="row">
           <div class="col-md-3">
            <?= $form->field($model, 'correo')->textInput(['maxlength' => true])->label('CORREO DE USUARIO<span class="obligatorio">*&nbsp;</span>');  ?>
        </div>
       <div class="col-md-3">
            <?= $form->field($model, 'user')->textInput(['maxlength' => true])->label('NOMBRE DE USUARIO<span class="obligatorio">*&nbsp;</span>');  ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'direccion')->textInput(['maxlength' => true])->label('DIRECCIÓN'); ?>
        </div>
    </div>
    <br>
    <div class="row" style="border-bottom: 1px solid #222d32;">
        <h4>ASOCIACIÓN DE VEHÍCULO</h4>
    </div>
    <br>
      <div class="row">
   
        <div class="col-md-3">
            <?= $form->field($model, 'placa')->textInput(['maxlength' => true])->label('PLACA ');  ?>
        </div>

            <div class="col-md-3">
                <?php 
                    $marcas = ArrayHelper::map(Marca::find()->orderBy('nombre')->all(), 'id_marca', 'nombre');
                    if(!$model->isNewRecord){
                        $model->marca = $model->idModelo->id_marca;
                    }
                    echo $form->field($model, "marca")->dropDownList(
                        $marcas,
                        [
                            'prompt'=>'Selecciona la marca',
                            'class' => 'form-control',
                            'onchange'=>'
                            var ident = $(this).attr("id");
                            var nom = ident.split("-");
                            $.get( "'.Url::toRoute('caso/listarmodelos').'", { id: $(this).val() } )
                                .done(function( data ) {
                                    $( "#nmodelo" ).html( data );
                                }
                            );                                              ',
                        ] )->label('MARCA'); 
                ?>
            </div>
            <div class="col-md-3">                                                         
            <?php
                $modelos=array(); //echo "trae: {$i} --- ";
                if(!$model->isNewRecord){
                    $modelos = ArrayHelper::map(Modelo::find()->where(['id_marca'=>$model->idModelo->id_marca])->orderBy('nombre')->all(), 'id_modelo', 'nombre');
                }
                echo $form->field($model, "id_modelo_id")->dropDownList(
                $modelos,
                    [
                    'prompt'=>'Selecciona el modelo',
                    'class' => 'form-control',
                    'id'    => 'nmodelo' ]
                )->label('MODELO');
                    ?>
            </div>



          <div class="col-md-3">

                                               <?php 
                          $anhos = ['1980'=>'1980', '1981'=>'1981','1982'=>'1982','1983'=>'1983',
                                                     '1984'=>'1984', '1985'=>'1985','1986'=>'1986','1987'=>'1987',
                                                     '1988'=>'1988', '1989'=>'1989','1990'=>'1990','1991'=>'1991',
                                                     '1992'=>'1992', '1993'=>'1993','1994'=>'1994','1995'=>'1995',
                                                     '1996'=>'1996', '1997'=>'1997','1998'=>'1998','1999'=>'1999',
                                                     '2000'=>'2000', '2001'=>'2001','2002'=>'2002','2003'=>'2003',
                                                     '2004'=>'2004', '2005'=>'2005','2006'=>'2006','2007'=>'2007',
                                                     '2008'=>'2008', '2009'=>'2009','2010'=>'2010','2011'=>'2011',
                                                     '2012'=>'2012', '2013'=>'2013','2014'=>'2014','2015'=>'2015',
                                                     '2016'=>'2016', '2017'=>'2017','2018'=>'2018','2019'=>'2019',
                                                      
                                                    ];
                                    echo $form->field($model, 'anho')->dropDownList(
                                            $anhos,
                                            [
                                                'prompt'=>'Seleccione...',
                                                'class' => 'form-control',
                                            ] );
                               

                                    ?>

                                    </div>  
    </div>

<br><br><br>

 <?php
      if(!$model->isNewRecord){
      ?>
          <div class="row" align="center">
              <div>
                  <?= Html::a('CANCELAR', ['index'], ['class' => 'btn btn-cancelar']) ?>
                  &nbsp;&nbsp;&nbsp;
                  <?= Html::submitButton('GUARDAR CAMBIOS', ['class' => 'btn btn-primario']) ?>
              </div>
          </div>
      <?php
      }else{
      ?>
          <div class="form-group" align="center">
              <?= Html::submitButton('GUARDAR', ['class' => 'btn btn-primario']) ?>
          </div>
      <?php
      }
  ?>
 

    <?php ActiveForm::end(); ?>

</div>
