<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Modelo;
use app\models\Marca;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MensajerosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'LISTADO DE REPRESENTANTE DE REPARTO';

?>
<div class="mensajero-index">

    <div class="row">
        <div class="col-md-1 col-xs-3">
            <h4 class="titulo"><img src="<?= Yii::$app->request->baseUrl ?>/img/driver_64.png" alt=""></h4>
        </div>
        <div class="col-md-10 col-xs-9" style="padding-top: 20px;">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
        </div>
    </div>
    </br>

    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                <?= Html::a('Registrar Representante de Reparto',['mensajero/create'], ['class' => 'btn btn-small-gris']) ?>   
            </div>
        </div>
        </br>
        </br>
    </div>

    <div class= "row">
        <div class="col-md-12">
            <?= GridView::widget([
                 'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'summary' => '',
                 'options' =>[
                                'class' => 'grid',
                            ],
                'columns' => [
                    [
                        'attribute' => 'nombre',
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'cedula',
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'telefono',
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'direccion',
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'placa',
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'id_modelo_id',
                        'label' => 'Marca',
                        'format' => 'html',
                        'value' => function ($model) {

         
                               
                                         return ($model->idModelo) ? $model->idModelo->idMarca->nombre.' -> '.$model->idModelo->nombre: "-";
                        },

                             'filter' => Select2::widget([
                            'name' => 'MensajerosSearch[id_marca]',
                            // 'data' => ArrayHelper::map(Marca::find()->joinWith('Modelo')->orderBy('nombre')->all(), 'id_marca', 'nombre'),
                            'data' => ArrayHelper::map(Marca::find()->orderBy('nombre')->all(), 'id_marca', 'nombre'),
                            'hideSearch' => true,
                            'options' => ['placeholder' => 'Marcas',],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]),
                        'enableSorting' => false,
                    ],
                    //'anho',
                                          
                 [  
                                    'class' => 'yii\grid\ActionColumn',
                                    //'contentOptions' => ['style' => 'width:260px;'],
                                    'header'=>'Acciones',
                                    'template' => '{menu}',
                                    'buttons' => [

                                        //view button
                                        'menu' => function ($url, $model) {

                                             return  '<div class="dropdown">
                                                      <button class="btn btn-default dropdown-toggle btn-sm" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i>
                                                      <span class="caret"></span></button>
                                                      <ul class="dropdown-menu">
                                                        
                                                        <li>'.Html::a('<span><i class="fa fa-pencil fa-sm" aria-hidden="true"></i></span> Modificar Datos',[ '/mensajero/update', 'id' => $model->id ]).' </li>
                                                        
                                                      </ul>
                                                    </div>';


                                        },
                                    ],

                               ],


          
                ],
            ]); ?>
        </div>
      </div>
 
</div>

 