<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Mensajero */

$this->title = 'REGISTRO DE REPRESENTANTE DE REPARTO';
?>
<div class="mensajero-create">

    <div class="row">
        <div class="col-md-1 col-xs-3">
            <h4 class="titulo"><img src="<?= Yii::$app->request->baseUrl ?>/img/driver_64.png" alt=""></h4>
        </div>
        <div class="col-md-10 col-xs-9" style="padding-top: 20px;">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
        </div>
    </div>
    </br>
    <div class="row" style="border-bottom: 1px solid #222d32;">
          <div class="col-xs-12 col-md-6">
              <h4>DATOS DEL REPRESENTANTE DE REPARTO</h4>
          </div>
          <div class="col-xs-12 col-md-6">
            <div align="right">
                  <?php
                  echo Html::a('Listado de Representantes de Reparto', ['index'], ['class' => 'btn btn-small-gris']);
                  ?>
            </div>
          </div>
      </div>
      <br>

    <?= $this->render('_form', [
        'model' => $model
    ]) ?>

</div>
 