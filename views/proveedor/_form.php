<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\proveedor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proveedor-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tipo_persona')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'identificacion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'direccion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'credito')->textInput() ?>

    <?= $form->field($model, 'nombre_representante')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'identificacion_representante')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'puesto_representante')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'correo_representante')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'celular_representante')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telefono_representante')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'garantia')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'domicilio')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'porcentaje')->textInput() ?>

    <?= $form->field($model, 'tipo_empresa_id')->textInput() ?>

    <?= $form->field($model, 'Tipo_pago')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'plazo_pago')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
