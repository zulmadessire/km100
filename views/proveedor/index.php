  <?php

  use yii\helpers\Html;
  use yii\grid\GridView;
  use app\models\Empresa;
  use app\models\Pieza;
  use app\models\Marca;
  use app\models\TipoEmpresa;
  use app\models\ProveedorPieza;
  use app\models\TipoPieza;
  use app\models\MarcaInsumo;
  use yii\helpers\ArrayHelper;
  use kartik\form\ActiveForm;
  use kartik\select2\Select2;

  /* @var $this yii\web\View */
  /* @var $searchModel app\models\ProveedorSearch */
  /* @var $dataProvider yii\data\ActiveDataProvider */
  $this->title = 'LISTADO DE PROVEEDORES';
  //$this->params['breadcrumbs'][] = $this->title;
  ?>
  <div class="proveedor-index">
      
  <div class="row">
      <div class="col-md-1 col-xs-3">
        <h4 class="titulo"><img src="<?= Yii::$app->request->baseUrl ?>/img/prov.png" alt="" width="40" height="40" ></h4>
      </div>
      <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
        <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
      </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                <?= Html::a('Registrar Proveedor',['proveedorpieza/create'], ['class' => 'btn btn-small-gris']) ?>
            </div>
        </div>
        </br>
        </br>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= GridView::widget([
               'dataProvider' => $dataProvider,
              'filterModel' => $searchModel,
                     'tableOptions' => ['class' => 'table table-responsive'],
                     'summary' => '',
                          'options' =>[
                              'class' => 'grid',
                          ],
            'columns' => [
                              [
                                  'attribute' => 'nombre',
                                  'label' => 'Nombre',
                                  'format' => 'text',
                                  'filter' => true,
                                  'enableSorting' => false,
                                  'value' => function( $model ){
                                      return $model->nombre;
                                  }
                                  //'headerOptions' => ['width' => '300',],
                              ],
                              
                                      [
                                  'label' => 'Marcas',
                                  'format' => 'raw',
                                  'filter' => true,
                                  'enableSorting' => false,
                                 'value' => function ($model, $key, $index, $column) {
                                  $marcas = "";
                                     
                                    foreach ($model->marcaProveedors as $value) {
                                        $marcas = $value->idMarca->nombre .'<br>'. $marcas;
                                    }
                                      return $marcas;
                                      },
                                
                                   'filter' => Select2::widget([
                                  'name' => 'ProveedorSearch[id_marca]',
                                  // 'data' => ArrayHelper::map(Marca::find()->joinWith('Modelo')->orderBy('nombre')->all(), 'id_marca', 'nombre'),
                                  'data' => ArrayHelper::map(Marca::find()->orderBy('nombre')->all(), 'id_marca', 'nombre'),
                                  'hideSearch' => true,
                                  'options' => ['placeholder' => 'Piezas',],
                                  'pluginOptions' => [
                                      'allowClear' => true
                                  ],
                              ]),
                               
                  ],  
 
                               [
                                  'attribute' => 'Tipo_pago',
                                  'label' => 'Tipo de pago',
                                  'format' => 'text',

                                  'filter' => ['credito'=>'Crédito','contado' => 'Contado'],
                                  'enableSorting' => false,
                                  'value' => function( $model ){
                                      return $model->Tipo_pago;
                                  }
                                  
                              ],
                               
                           
                                   [
                                  // 'attribute' => 'Tipo_pago',
                                  'label' => 'En Licitación',
                                  'format' => 'text',
                                  'filter' => false,
                                  'enableSorting' => false,
                                  'value' => function( $model ){
                                      return "Si";
                                  }
                                  //'headerOptions' => ['width' => '300',],
                              ],
                                [
                                
                                  'label' => 'Puntuación General',
                                  'format' => 'html',
                                  'filter' => true,
                                  'enableSorting' => false,
                                  'value' => function( $model ){
 

                                       return '<span class="label label-oval" style="background-color:'.'green'.'; ">'.'80'.'</span>';

                                      // return '80';
                                  }
                                  
                              ],
                                 [
                                
                                  'label' => 'Competencia',
                                  'format' => 'html',
                                  'filter' => true,
                                  'enableSorting' => false,
                                  'value' => function( $model ){
                                   
                                         return '<span class="label label-oval" style="background-color:'.'#0070C0'.'; ">'.'10%'.'</span>';
                                  }
                                  
                              ],
                  [  
                                  'class' => 'yii\grid\ActionColumn',
                                  //'contentOptions' => ['style' => 'width:260px;'],
                                  'header'=>'Acciones',
                                  'template' => '{menu}',
                                  'buttons' => [

                                      //view button
                                      'menu' => function ($url, $model) {

                                           return  '<div class="dropdown">
                                                    <button class="btn btn-default dropdown-toggle btn-sm" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i>
                                                    <span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                      <li>'.Html::a('<span><i class="fa fa-eye fa-sm" aria-hidden="true"></i></span> Ver Detalles',[ '/proveedorpieza/view', 'id' => $model->id ]).' </li>
                                                      <li>'.Html::a('<span><i class="fa fa-pencil fa-sm" aria-hidden="true"></i></span> Modificar Datos',[ '/proveedorpieza/update', 'id' => $model->id ]).' </li>
                                                      <li>'.Html::a('<span><i class="fa fa-file-archive-o fa-sm" aria-hidden="true"></i></span> Ficha Proveedor',[ '/proveedorpieza/ficha', 'id' => $model->id ]).' </li>
                                                      
                                                    </ul>
                                                  </div>';


                                      },
                                  ],

                             ],

               

              ],
          ]); ?>
        </div>
    </div>
</div>
