<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProveedorSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proveedor-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'tipo_persona') ?>

    <?= $form->field($model, 'identificacion') ?>

    <?= $form->field($model, 'direccion') ?>

    <?= $form->field($model, 'descripcion') ?>

    <?php // echo $form->field($model, 'telefono') ?>

    <?php // echo $form->field($model, 'credito') ?>

    <?php // echo $form->field($model, 'nombre_representante') ?>

    <?php // echo $form->field($model, 'identificacion_representante') ?>

    <?php // echo $form->field($model, 'puesto_representante') ?>

    <?php // echo $form->field($model, 'correo_representante') ?>

    <?php // echo $form->field($model, 'celular_representante') ?>

    <?php // echo $form->field($model, 'telefono_representante') ?>

    <?php // echo $form->field($model, 'garantia') ?>

    <?php // echo $form->field($model, 'domicilio') ?>

    <?php // echo $form->field($model, 'nombre') ?>

    <?php // echo $form->field($model, 'porcentaje') ?>

    <?php // echo $form->field($model, 'tipo_empresa_id') ?>

    <?php // echo $form->field($model, 'Tipo_pago') ?>

    <?php // echo $form->field($model, 'plazo_pago') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
