<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\proveedor */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Proveedors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proveedor-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'tipo_persona',
            'identificacion',
            'direccion',
            'descripcion',
            'telefono',
            'credito',
            'nombre_representante',
            'identificacion_representante',
            'puesto_representante',
            'correo_representante',
            'celular_representante',
            'telefono_representante',
            'garantia',
            'domicilio',
            'nombre',
            'porcentaje',
            'tipo_empresa_id',
            'Tipo_pago',
            'plazo_pago',
        ],
    ]) ?>

</div>
