<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AuditoriafsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Auditoriafs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auditoriafs-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Auditoriafs', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'fecha',
            'estado',
            'motivo',
            'desviacion',
            // 'veh_fuera_ser_est',
            // 'plan_accion',
            // 'observacion',
            // 'id_estacion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
