<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Auditoriafs */

$this->title = 'Update Auditoriafs: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Auditoriafs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="auditoriafs-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
