<?php


use yii\grid\GridView;
/*use yii\widgets\ActiveForm;*/
use app\models\OtrasFs;
use app\models\Flota;
use app\models\EstadoCompraPieza;
use app\models\SolicitudCompraPieza;
use app\models\SolicitudCompraEstacion;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\TipoMantenimiento;
use app\models\Modelo;
use kartik\select2\Select2;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use kartik\file\FileInput;
use wbraganca\dynamicform\DynamicFormWidget;


/* @var $this yii\web\View */
/* @var $model app\models\Vehiculo */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Registro de auditoría - Unidades fuera de servicio';
//$this->params['breadcrumbs'][] = $this->title;


$acumulador = 0;

$acumulador = $dataProvider->getTotalCount();
$acumulador2 = 0;
foreach ($model_est->servicios as $value) {

 foreach ($value->pipelineServicios as $value2) {

   if ($value2->id_estado_servicio==10 || $value2->id_estado_servicio==11) {

     $acumulador2 = $acumulador2 + 1;
   }
 }
}

$desviacion = $acumulador2-$acumulador;



?>
<div class="Audiorias">

  <div class="row">
    <div class="col-md-1 col-xs-3">
      <h4 class="titulo"><i class="fa fa-list-ul fa-4x" aria-hidden="true"></i></h4>
    </div>
    <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
      <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
    </div>
  </div>

  <br>

  <?php $form = ActiveForm::begin(['id' => 'dynamic_lista-form']); ?>
  <div class="row">
    <div class="col-xs-12">
      <p class="titulo-form">DATOS DEL CLIENTE</p>
    </div>
  </div>
  <div style = "margin-top: 1px !important;
  border-top: 1px solid #222d32 !important;"><br><br></div>
  <div class="row">

    <div class="col-md-4 col-xs-12">
      <?= $form->field($model_est, 'empresa')->textInput(['disabled' => true, 'value' => $model_est->idEmpresa->nombre])->label('Empresa') ?>
    </div>

    <div class="col-md-4 col-xs-12">
      <?= $form->field($model, 'nombre')->textInput(['value' => strtoupper($model_est->nombre), 'disabled' => true])->label('Estación') ?>
    </div>

    <div class="col-md-4 col-xs-12">
      <?= $form->field($model, 'fecha')->textInput(['value' => date('Y-m-d'),'maxlength' => true, 'disabled' => true]) ?>
    </div>

  </div>



    <div class="row">
      <div class="col-xs-12">
        <p class="titulo-form">REGISTROS</p>
        <hr>
      </div>
    </div>

    <div class="col-md-4 col-xs-12">
      <div class="form-group">

        <?= $form->field($model, 'veh_fuera_serv_reg')->textInput(['disabled' => false,'maxlength' => true,'id'=>'fsregistrado' ])->label('FUERA DE SERVICIO REGISTRADO') ?>

      </div>
    </div>
    <div class="col-md-4 col-xs-12">
      <div class="form-group">

        <?= $form->field($model, 'veh_fuera_ser_est')->textInput(['disabled' => false,'maxlength' => true ])->label('FUERA DE SERVICIO EN ESTACIÓN') ?>


      </div>
    </div>
    <div class="col-md-4 col-xs-12">
      <div class="form-group">
        <?= $form->field($model, 'desviacion')->textInput(['disabled' => true,'maxlength' => true ])->label('DESVIACIÓN') ?>
        <?= $form->field($model, 'desviacion')->hiddenInput(['maxlength' => true ])->label(false) ?>

      </div>
    </div>
    <div class="col-md-4 col-xs-12">
      <?= $form->field($model, 'motivo')->textarea(['rows' => '3'])->label('Motivo de desviación') ?>
    </div>
    <div class="col-md-4 col-xs-12">
      <?= $form->field($model, 'planaccion')->textarea(['rows' => '3', 'value'=> '',  'disabled' => false])->label('Plan de Acción') ?>
    </div>
    <div class="col-xs-12">
      <p class="titulo-form">UNIDADES CON ORDEN DE COMPRA ABIERTA EN LA ESTACIÓN</p>
      <hr>
    </div>
    <br>
    
 
    <br>
 
     <div class="row">
      <div class="col-md-12">
        <?= GridView::widget([
          'dataProvider' => $dataProvider,

          'tableOptions' => ['class' => 'grid-blue table table-responsive table-bordered'],
          'summary' => '',

          'options' =>[
          'class' => '',
          ],
          'columns' => [
          [
          'label' => 'Ficha/MVA',
          'format' => 'text',
          'filter' => false,
          'enableSorting' => false,
          'contentOptions'=>['style'=>'text-align:center;'],
          'value' => function( $model ){



            return $model->mva;
          },
          ],
          [
          'label' => 'Modelo',
          'format' => 'text',
          'filter' => false,
          'enableSorting' => false,
          'contentOptions'=>['style'=>'text-align:center;'],
          'value' => function( $model ){



            return $model->idModelo->nombre;
          },
          ],

          [
          'label' => 'Orden de compra/Estatus',
          'format' => 'text',
          'filter' => false,
          'enableSorting' => false,
          'contentOptions'=>['style'=>'text-align:center;'],
          'value' => function( $model ){
            $estado = '';
            foreach ($model->solicitudCompraEstacions as $key => $value) {

              $estados = SolicitudCompraPieza::find()->where([ 'id_solicitud_compra_estacion' => $value->id ])->orderBy('codigo_estado DESC')->one();
              $edo = $estados['codigo_estado'];
              $estado = EstadoCompraPieza::find()->where( ['id' => $edo] )->one();

                              // $estado = $estado.$value->id.'/'.$estados->nombre;
            }
                            // return $estado[];
            return $estados['id'].'/'.$estado['nombre'];
          }
          ],
          [
          'label' => 'Fecha de Orden de Compra',
          'format' => 'text',
          'filter' => false,
          'enableSorting' => false,
          'contentOptions'=>['style'=>'text-align:center;'],
          'value' => function( $model ){
            $estado = '';
            foreach ($model->solicitudCompraEstacions as $key => $value) {


             return $value->fecha_registro;
                              // $estado = $estado.$value->id.'/'.$estados->nombre;
           }
                            // return $estado[];
         }
         ],


         ],



         ]); ?>
       </div>
     </div>
 

     <!-- INICIO DINAMICO UNO -->
 
   <div class="col-xs-12">
    <p class="titulo-form">UNIDADES FUERA DE SERVICIO EN ESTACIÓN</p>
    <hr>
  </div>

  <div class="row">

   <?php DynamicFormWidget::begin([
    'widgetContainer' => 'dynamicform_wrapper_resultados',
    'widgetBody' => '.container-items',
    'widgetItem' => '.item',
    'limit' => 50,
    'min' => 1,
    'insertButton' => '.add-item',
    'deleteButton' => '.remove-item',
    'model' => $modelsfs[0],
    'formId' => 'dynamic_lista-form',
    'formFields' => [
    'archive',
    ],
    ]); ?>

    <table class="table table-hover" id="anexos-table">
      <tbody class="container-items">
        <?php foreach ($modelsfs as $i => $modelmodel): ?>
        <tr class="item panel panel-default">
          <td>
           <?php if (!$modelmodel->isNewRecord): ?>
           <?= Html::activeHiddenInput($modelmodel, "[{$i}]id"); ?>
           <?= Html::activeHiddenInput($modelmodel, "[{$i}]imageFile"); ?>
           <?= Html::activeHiddenInput($modelmodel, "[{$i}]ficha"); ?>
           <?= Html::activeHiddenInput($modelmodel, "[{$i}]causa"); ?>
           <?= Html::activeHiddenInput($modelmodel, "[{$i}]responsable"); ?>
           <?= Html::activeHiddenInput($modelmodel, "[{$i}]id_modelo"); ?>
           <?= Html::activeHiddenInput($modelmodel, "[{$i}]observacion"); ?>

         <?php endif; ?>

         <div class="col-md-1">
          <label >FICHA</label>
          <?= $form->field($modelmodel, "[{$i}]ficha")->textInput(['maxlength' => true])->label(false) ?>
        </div>
        <div class="col-xs-12 col-md-2"> 
         <?= $form->field($modelmodel, "[{$i}]id_modelo")->dropDownList(
          ArrayHelper::map(Modelo::find()->all(), 'id_modelo' , 'nombre'),
          ['prompt' => 'Seleccione', 'class' => 'form-control'


          ]
          )->label('MODELO');   ?>
        </div>
        <div class="col-md-2">
          <label >CAUSA</label>
          <?= $form->field($modelmodel, "[{$i}]causa")->textInput(['maxlength' => true])->label(false) ?>
        </div>
        <div class="col-md-2">
          <label >RESPONSABLE</label>
          <?= $form->field($modelmodel, "[{$i}]responsable")->textInput(['maxlength' => true])->label(false) ?>
        </div>
        <div class="col-md-3">
          <label >OBSERVACIÓN</label>
          <?= $form->field($modelmodel, "[{$i}]observacion")->textInput(['maxlength' => true])->label(false) ?>
        </div>
        <div class="col-xs-12 col-md-4">
          <label >ANEXO 1</label><br>         

          <?php 
          echo "<div class='item-name'>".$modelmodel->anexo."</div>";  
          echo  $form->field($modelmodel, "[{$i}]imageFile")->fileInput()->label(false);
          ?> 


        </div>  
        <div class="col-xs-12 col-md-5">
          <label >ANEXO 2</label><br>         

          <?php 
          echo "<div class='item-name'>".$modelmodel->anexo2."</div>";  
          echo  $form->field($modelmodel, "[{$i}]imagefile2")->fileInput()->label(false);
          ?> 


        </div>
    <!--     <div class="col-xs-12 col-md-3">
          <label >ANEXO 3</label><br>         

          <?php 
          echo "<div class='item-name'>".$modelmodel->anexo3."</div>";  
          echo  $form->field($modelmodel, "[{$i}]imagefile3")->fileInput()->label(false);
          ?> 


        </div>
        <div class="col-xs-12 col-md-3">
          <label >ANEXO 4</label><br>         

          <?php 
          echo "<div class='item-name'>".$modelmodel->anexo4."</div>";  
          echo  $form->field($modelmodel, "[{$i}]imagefile4")->fileInput()->label(false);
          ?> 


        </div>    -->

        <div class="col-xs-12 col-md-1 text-center">
          <button type="button" class="remove-item btn btn-cancelar btn-xs" style="margin-top:28px;">
            <i class="glyphicon glyphicon-minus"></i></button>
          </div>
        </td>
      </tr>
    <?php endforeach; ?>
    <?php DynamicFormWidget::end(); ?>
  </tbody>
</table>

<button type="button" class="add-item btn btn-accion  btn-xs pull-right">Agregar unidades</button>
<br>
<br>
</div>
<!-- INICIO DINAMICO 2 -->

<!-- INICIO DINAMICO 2 -->
<div class="col-xs-12">
  <p class="titulo-form">OTRAS SOLICITUDES</p>
  <hr>
</div>
<div class="row">

  <?php DynamicFormWidget::begin([
    'widgetContainer' => 'dynamicform_wrapper_resultados2',
    'widgetBody' => '.container-items2',
    'widgetItem' => '.item2',
    'limit' => 50,
    'min' => 1,
    'insertButton' => '.add-item2',
    'deleteButton' => '.remove-item',
    'model' => $modelsadd[0],
    'formId' => 'dynamic_lista-form',
    'formFields' => [
    'nombre',

    ],
    ]); ?>
    <table class="table table-hover">
      <tbody class="container-items2">
        <?php foreach ($modelsadd as $i => $modelmodel): ?>
        <tr class="item2 panel panel-default">
          <td>
           <div class="col-xs-12 col-md-2"> 
            <label >MODELO</label>

            <?= $form->field($modelmodel, "[{$i}]id_modelo")->dropDownList(
              ArrayHelper::map(Modelo::find()->all(), 'id_modelo' , 'nombre'),
              ['prompt' => 'Seleccione', 'class' => 'form-control'


              ]
              )->label(false);   ?>
            </div>
            <div class="col-xs-12 col-md-2">
              <label >CAUSA</label>
              <?= $form->field($modelmodel, "[{$i}]causa")->textInput()->label(false); ?></div>
              <div class="col-xs-12 col-md-2">
                <label >FICHA/MVA</label>
                <?= $form->field($modelmodel, "[{$i}]ficha")->textInput()->label('Responsable')->label(false); ?></div>
                <div class="col-xs-12 col-md-3">
                 <label >OBSERVACIÓN</label>
                 <?= $form->field($modelmodel, "[{$i}]observacion")->textInput()->label('OBSERVACIÓN')->label(false); ?></div>

                 <div class="col-xs-12 col-md-1 text-center">
                  <button type="button" class="remove-item btn btn-cancelar btn-xs" style="margin-top:28px;">
                    <i class="glyphicon glyphicon-minus"></i></button>
                  </div>    


                </td>
              </tr>
            <?php endforeach; ?>
            <?php DynamicFormWidget::end(); ?>
          </tbody>
        </table>

        <button type="button" class="add-item2 btn btn-accion  btn-xs pull-right">Agregar solicitud</button>
        <br>
        <br>
      </div>
        <!-- INICIO DINAMICO tres -->
        
      <div class="col-xs-12">
        <p class="titulo-form">OTRAS SOLICITUDES PENDIENTES</p>
        <hr>
      </div>
      <div class="col-md-12">
       <?php DynamicFormWidget::begin([
        'widgetContainer' => 'dynamicform_wrapper_resultados3',
        'widgetBody' => '.container-items3',
        'widgetItem' => '.item3',
        'limit' => 50,
        'min' => 1,
        'insertButton' => '.add-item3',
        'deleteButton' => '.remove-item',
        'model' => $modelsotras[0],
        'formId' => 'dynamic_lista-form',
        'formFields' => [
        'archive',
        ],
        ]); ?>

        <table class="table table-hover">
          <tbody class="container-items3">
            <?php foreach ($modelsotras as $i => $modelmodel): ?>
            <tr class="item3 panel panel-default">
              <td>
 

             <div class="col-xs-12  col-md-2">
              <label >TIPO</label>
              <?= $form->field($modelmodel, "[{$i}]solicitud")->textInput(['maxlength' => true])->label(false) ?>
            </div>
            <div class="col-xs-12 col-md-2">
              <label >ITEM</label>
              <?= $form->field($modelmodel, "[{$i}]causa")->textInput(['maxlength' => true])->label(false) ?>
            </div>
            <div class="col-xs-12 col-md-2">
              <label >RESPONSABLE</label>
              <?= $form->field($modelmodel, "[{$i}]responsable")->textInput(['maxlength' => true])->label(false) ?>
            </div>
            <div class="col-xs-12 col-md-3">
              <label >OBSERVACIÓN</label>
              <?= $form->field($modelmodel, "[{$i}]observacion")->textInput(['maxlength' => true])->label(false) ?>
            </div>


            <div class="col-xs-12 col-md-1 text-center">
              <button type="button" class="remove-item btn btn-cancelar btn-xs" style="margin-top:28px;">
                <i class="glyphicon glyphicon-minus"></i></button>
              </div>
            </td>
          </tr>
        <?php endforeach; ?>
        <?php DynamicFormWidget::end(); ?>
      </tbody>
    </table>

    <button type="button" class="add-item3 btn btn-accion  btn-xs pull-right">Agregar unidades</button>

    <br>
    <br>
    </div>

            <!-- INICIO DINAMICO tres -->
        
      <div class="col-xs-12">
        <p class="titulo-form">IMAGENES ADICIONALES</p>
        <hr>
      </div>
      <div class="col-md-12">
       <?php DynamicFormWidget::begin([
        'widgetContainer' => 'dynamicform_wrapper_resultados6',
        'widgetBody' => '.container-items6',
        'widgetItem' => '.item6',
        'limit' => 50,
        'min' => 1,
        'insertButton' => '.add-item6',
        'deleteButton' => '.remove-item',
        'model' => $modelimagenes[0],
        'formId' => 'dynamic_lista-form',
        'formFields' => [
        'archive',
        ],
        ]); ?>

        <table class="table table-hover">
          <tbody class="container-items6">
            <?php foreach ($modelimagenes as $i => $modelmodel): ?>
            <tr class="item6 panel panel-default">
              <td>
               <?php if (!$modelmodel->isNewRecord): ?>
           <?= Html::activeHiddenInput($modelmodel, "[{$i}]id"); ?>
           <?= Html::activeHiddenInput($modelmodel, "[{$i}]imageFile"); ?>


         <?php endif; ?>

             <div class="col-xs-12  col-md-5">
              <label >DESCRIPCIÓN</label>
              <?= $form->field($modelmodel, "[{$i}]descripcion")->textInput(['maxlength' => true])->label(false) ?>
            </div>
              <div class="col-xs-12 col-md-4">
          <label >IMAGEN</label><br>         

          <?php 
          echo "<div class='item-name'>".$modelmodel->anexo."</div>";  
          echo  $form->field($modelmodel, "[{$i}]imageFile")->fileInput()->label(false);
          ?> 


        </div> 


            <div class="col-xs-12 col-md-1 text-center">
              <button type="button" class="remove-item btn btn-cancelar btn-xs" style="margin-top:28px;">
                <i class="glyphicon glyphicon-minus"></i></button>
              </div>
            </td>
          </tr>
        <?php endforeach; ?>
        <?php DynamicFormWidget::end(); ?>
      </tbody>
    </table>

    <button type="button" class="add-item6 btn btn-accion  btn-xs pull-right">Agregar unidades</button>

    <br>
    <br>
    </div>
 <div class="  col-xs-12">
        <p class="titulo-form">OBSERVACIÓN</p>
        <hr>
      </div>
      <br>
    <div class="row">
<div class="col-md-offset-1 col-md-10 col-xs-12">
      <?= $form->field($model, 'observacion')->textInput(['disabled' => false])->textarea(['rows' => '6']) ->label(false) ?>
    </div>
    </div>
    

  
  <div class="row">
  <div class="col-md-12 col-xs-12">
      <div class="form-group text-center">  
        <?= Html::submitButton($model->isNewRecord ? 'GUARDAR' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-submit' : 'btn btn-primary']) ?>
      </div>
    </div>
  </div>
    <?php ActiveForm::end(); ?>
    <script type="text/javascript">

    $(document.body).on('blur','#auditoriafs-veh_fuera_ser_est', function () {
      var registrado= $('#fsregistrado').val();
      var existencia= $('#auditoriafs-veh_fuera_ser_est').val();
      $('#auditoriafs-desviacion').val((registrado-existencia));
      $('#auditoriafs-desviacion').val((registrado-existencia));
    });


    </script>