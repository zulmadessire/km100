<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Auditoriafs */

$this->title = 'Create Auditoriafs';
$this->params['breadcrumbs'][] = ['label' => 'Auditoriafs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auditoriafs-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
