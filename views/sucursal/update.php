<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SucursalConcesionario */

$this->title = 'Update Sucursal Concesionario: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sucursal Concesionarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sucursal-concesionario-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
