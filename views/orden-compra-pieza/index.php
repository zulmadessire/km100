<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrdencomprapiezaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orden Compra Piezas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orden-compra-pieza-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Orden Compra Pieza', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'fecha',
            'estado',
            'observaciones',
            'id_cotizacion_proveedor',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
