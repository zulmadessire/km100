<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\db\Query;
use app\models\Empresa;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Modelo;
use app\models\Cliente;
use app\models\Estacion;
use app\models\PipelineServicio;
use app\models\CotizacionTallerActividadSearch;
use app\models\Estado;
use app\models\Marca;
use app\models\Vehiculo;


/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchVehiculo */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Listado de Vehículos Próximos a vencer';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vehiculo-index">
    
    <div class="row">
        <div class="col-md-6">
            <h4 class="titulo"><?= Html::encode($this->title) ?></h4>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <span>Total de Vehículos: <?= $dataProvider->getTotalCount() ?></span>
            <div class="pull-right">
             
                    <?= Html::a('Listado de vehiculos',['/vehiculo/index'], ['class' => 'btn btn-small-gris']) ?>
            
            </div>
        </div>
        </br>
        </br>
        <div class="col-md-12 col-xs-12">

          <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel'   => $searchModel,
                    'tableOptions' => ['class' => 'table'],
                    'summary' => '',
                    'options' =>[
                        'class' => 'grid table-responsive',
                    ],
                    'columns' => [
                     [
                       
                            'label' => 'Marca',
                            'format' => 'text',
                            'enableSorting' => true,
                            'value' => function( $model ){
                                return $model->idVehiculo->idModelo->idMarca->nombre;
                            },
                            'filter' => Select2::widget([
                                'name' => 'VehiculoSearch[id_marcas]',
                                'data' => ArrayHelper::map(Marca::find()->orderBy('nombre')->all(), 'id_marca', 'nombre'),
                          
                                'hideSearch' => true,
                                'options' => ['placeholder' => 'Marca',],
                                'pluginOptions' => [
                                      'allowClear' => true
                                  ],
                              ]),
                        ],
                             [
                       
                            'label' => 'Modelo',
                            'format' => 'text',
                            'enableSorting' => true,
                            'value' => function( $model ){
                                return $model->idVehiculo->idModelo->nombre;
                            },
                            'filter' => Select2::widget([
                                'name' => 'VehiculoSearch[id_modelos]',
                              
                                'data' => ArrayHelper::map(Modelo::find()->orderBy('nombre')->all(), 'id_modelo', 'nombre'),
                                'hideSearch' => true,
                                'options' => ['placeholder' => 'Modelo',],
                                'pluginOptions' => [
                                      'allowClear' => true
                                  ],
                              ]),
                        ],
                            [
 
                            'label' => 'MVA',
                            'format' => 'text',
                            'value' => function( $model ){
                                return $model->idVehiculo->mva;
                            },
                               'filter' => Html::input('text', 'VehiculoSearch[mvas]', $searchModel->mvas, ['class' => 'form-control',]),
                       

                            ],

                             [
                            'label' => 'Km Proyectado',
                            'format' => 'text',
                            'enableSorting' => true,

                            'value' => function( $model ){
 
                                return $model->idVehiculo->proyeccionMantenimientos->km_proyectado;
 
                            },
                         
                            
                            'filter' => Html::input('text', 'VehiculoSearch[proyectado]', $searchModel->proyectado, ['class' => 'form-control',]),
                       
                        ],
                          [
                            'label' => 'Km Actual',
                            'format' => 'text',
                            'enableSorting' => true,
                            'value' => function( $model ){
                                return  $model->idVehiculo->km;
                            },
                            
                            'filter' => Html::input('text', 'VehiculoSearch[actual]', $searchModel->actual, ['class' => 'form-control',]),
                        ],
                         [
                            'label' => 'Diferencia',
                            'format' => 'text',
                            'enableSorting' => true,
                            'value' => function( $model ){
 
                                return  ($model->idVehiculo->proyeccionMantenimientos->km_proyectado)-($model->idVehiculo->km);
 
                            },
                            // 'filter' => Select2::widget([
                            //     'name' => 'VehiculoSearch[cliente]',
                            //       // 'data' => ArrayHelper::map(Marca::find()->joinWith('Modelo')->orderBy('nombre')->all(), 'id_marca', 'nombre'),
                            //     'data' => ArrayHelper::map(Cliente::find()->orderBy('nombre_comercial')->all(), 'id', 'nombre_comercial'),
                            //     'hideSearch' => true,
                            //     'options' => ['placeholder' => 'Cliente',],
                            //     'pluginOptions' => [
                            //           'allowClear' => true
                            //       ],
                            //   ]),
                        ],
 
                            /*[
 
                            'label' => 'Estatus',
                            'format' => 'html',
                            'value' => function( $model ){
                                $query = new query();
                                $query->select('es.nombre, es.color')
                                    ->from('proyeccion_mantenimiento pm')
                                    ->join('INNER JOIN','vehiculo ve','ve.id = pm.id_vehiculo')
                                    ->join('INNER JOIN','estado_vehiculo ev','ev.id_vehiculo = ve.id')
                                    ->join('INNER JOIN','estado es','es.id = ev.id_estado')
                                    ->where('ve.id = '. $model->idVehiculo->id );

                                $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                                $estado = Yii::$app->db->createCommand($sql)->queryOne();

                                return '<span class="label label-oval" style="background-color:'.$estado['color'].'; ">'.$estado['nombre'].'</span>';
                            },
                                'filter' => Select2::widget([
                                'name' => 'VehiculoSearch[estado]',
                                  // 'data' => ArrayHelper::map(Marca::find()->joinWith('Modelo')->orderBy('nombre')->all(), 'id_marca', 'nombre'),
                                'data' => ArrayHelper::map(Estado::find()->orderBy('nombre')->all(), 'id', 'nombre'),
                                'hideSearch' => true,
                                'options' => ['placeholder' => 'Estado',],
                                'pluginOptions' => [
                                      'allowClear' => true
                                  ],
                              ]),
                        ],*/
                    ],
                ]); ?>
            
        </div>
    </div>
</div>
