<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Vehiculo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vehiculo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'color')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'transmision')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'chasis')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'placa')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mva')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_categoria')->textInput() ?>

    <?= $form->field($model, 'clase')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'combustible')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha_compra')->textInput() ?>

    <?= $form->field($model, 'fecha_inicial')->textInput() ?>

    <?= $form->field($model, 'tiempo_vida')->textInput() ?>

    <?= $form->field($model, 'fecha_renovacion_placa')->textInput() ?>

    <?= $form->field($model, 'fecha_emision_placa')->textInput() ?>

    <?= $form->field($model, 'wizard')->textInput() ?>

    <?= $form->field($model, 'exactus')->textInput() ?>

    <?= $form->field($model, 'id_modelo')->textInput() ?>

    <?= $form->field($model, 'id_concesionario')->textInput() ?>

    <?= $form->field($model, 'transito')->textInput() ?>

    <?= $form->field($model, 'anho')->textInput() ?>

    <?= $form->field($model, 'fecha_emision_seguro')->textInput() ?>

    <?= $form->field($model, 'fecha_venc_seguro')->textInput() ?>

    <?= $form->field($model, 'fecha_emision_marbete')->textInput() ?>

    <?= $form->field($model, 'fecha_venc_marbete')->textInput() ?>

    <?= $form->field($model, 'fecha_venta')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
