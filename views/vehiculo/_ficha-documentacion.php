<?php

use yii\helpers\Html;
use yii\db\Query;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Vehiculo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ficha-documentacion">
    
    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">RENOVACIÓN DE PLACA</p>
            <hr>
        </div>
    </div>

    <div class="row">
        <?php $form = ActiveForm::begin(); ?>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'fecha_emision_placa', ['addon' => ['prepend' => ['content'=>'<i class="fa fa-calendar" aria-hidden="true"></i>']] ] )->textInput(['disabled' => true, 'value' => date('d/m/Y', strtotime($model->fecha_emision_placa)) ])->label('Fecha de Emisión') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'fecha_renovacion_placa', ['addon' => ['prepend' => ['content'=>'<i class="fa fa-calendar" aria-hidden="true"></i>']] ] )->textInput(['disabled' => true, 'value' => date('d/m/Y', strtotime($model->fecha_renovacion_placa)) ])->label('Fecha de Renovación') ?>
        </div>
     
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">RENOVACIÓN DE SEGURO</p>
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'fecha_emision_seguro', ['addon' => ['prepend' => ['content'=>'<i class="fa fa-calendar" aria-hidden="true"></i>']] ] )->textInput(['disabled' => true, 'value' => (($model->fecha_emision_seguro)?date('d/m/Y', strtotime($model->fecha_emision_seguro)):'') ])->label('Fecha de Emisión') ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'fecha_venc_seguro', ['addon' => ['prepend' => ['content'=>'<i class="fa fa-calendar" aria-hidden="true"></i>']] ] )->textInput(['disabled' => true, 'value' => (($model->fecha_venc_seguro)?date('d/m/Y', strtotime($model->fecha_venc_seguro)):'') ])->label('Fecha de Renovación') ?>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">RENOVACIÓN DE MARBETE</p>
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'fecha_emision_marbete', ['addon' => ['prepend' => ['content'=>'<i class="fa fa-calendar" aria-hidden="true"></i>']] ] )->textInput(['disabled' => true, 'value' => (($model->fecha_emision_marbete)?date('d/m/Y', strtotime($model->fecha_emision_marbete)):'') ])->label('Fecha de Emisión') ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'fecha_venc_marbete', ['addon' => ['prepend' => ['content'=>'<i class="fa fa-calendar" aria-hidden="true"></i>']] ] )->textInput(['disabled' => true, 'value' => (($model->fecha_venc_marbete)?date('d/m/Y', strtotime($model->fecha_venc_marbete)):'') ])->label('Fecha de Renovación') ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
    <br>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">GARANTÍA DEL VEHÍCULO</p>
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right">
                <?= Html::a('Descargar',['/vehiculo/excelgarantia', 'id' => $model->id ], ['class' => 'btn btn-small-verde']) ?> 
            </div>
        </div>
        </br>
        </br>
        <div class="col-md-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider_gv,
                'id' => 'garantias',
                'tableOptions' => ['class' => 'grid-blue table table-responsive table-bordered'],
                'summary' => '',
                'pjax' => true,
                'options' =>[
                    'class' => '',
                ],
                'columns' => [
                    [
                        'label' => 'Descripción',
                        'attribute' => 'partes.nombre',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                        'headerOptions' => ['width' => '500',],
                        'contentOptions'=>['style'=>'text-align:left;'],
                    ],
                    [
                        'label' => 'Tiempo',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                        'value' => function( $model ){
                            if ( $model->tiempo ) {
                                $periodo = '';
                                if ( $model->periodo ==  0 ) {
                                    if ( $model->tiempo > 1 ) {
                                        $periodo = 'días';
                                    } else{
                                        $periodo = 'día';
                                    }
                                    
                                } elseif ( $model->periodo == 1 ) {
                                    if ( $model->tiempo > 1 ) {
                                        $periodo = 'meses';
                                    } else{
                                        $periodo = 'mes';
                                    }
                                    
                                } elseif ( $model->periodo == 2 ) {
                                    if ( $model->tiempo > 1 ) {
                                        $periodo = 'años';
                                    } else{
                                        $periodo = 'año';
                                    }
                                }
                                return $model->tiempo. ' ' .$periodo;
                            } else{
                                return '-';
                            }
                            
                        },
                    ],
                    [
                        'attribute' => 'km',
                        'label' => 'Kilometraje',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                        'value' => function( $model ){
                            if ( $model->km ) {
                                return number_format(floatval($model->km), 0, ',', '.') . ' km';
                            } else{
                                return '-';
                            }
                            
                        },
                    ],
                    [
                        'attribute' => 'partes.tipoGarantia.nombre',
                        'label' => 'Tipo Garantía',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                    ],
                    [  
                        'class' => 'yii\grid\ActionColumn',
                        //'contentOptions' => ['style' => 'width:260px;'],
                        'header'=>'Acción',
                        'template' => '{anular}',
                        'buttons' => [

                            //view button
                            'anular' => function ($url, $model) {

                                if ( $model->km || $model->tiempo ) {
                                    if ( $model->anular == 0 ) {
                                        return Html::a('Anular','#',['class' => 'btn btn-anular', 'data' => ['id' => $model->id, 'toggle' => 'modal', 'target' => '#anularGarantiaModal']]);
                                    } else{
                                        if ( $model->motivo == 1 ) {
                                            return 'Accidente';
                                        } else if ( $model->motivo == 2 ){
                                            return 'Cambio de piezas';
                                        } else if ( $model->motivo == 3 ) {
                                            return 'Expiración';
                                        } else{
                                            return '-';
                                        }
                                    }
                                } else{
                                    return '-';
                                }
                                
                            },
                        ],

                    ],
                ],
            ]); ?>
        </div>
    </div>

</div>

<!-- Modal Anular Garantia -->
<div id="anularGarantiaModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="title"><i class="fa fa-times-circle-o fa-lg" aria-hidden="true" style="color: #e4655b;"></i>&nbsp;&nbsp; ANULAR GARANTÍA</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true" style="color: #000; font-weight: bold;"></i></button>
          </div>
        <div class="modal-body">
            </br>
            <?php $form = ActiveForm::begin([ 'id' => 'anular', 'action' => ['/vehiculo/anulargarantia',] ]); ?>
                <div class="row">
                    <div class="text-center">
                        <div class="col-xs-12 col-md-8 col-md-offset-2">
                            <label class="control-label" for="garantiavehiculo-motivo">MOTIVO DE ANULACIÓN</label>
                            <?=
                                Select2::widget([
                                    'model' => $model_gv,
                                    'attribute' => 'motivo',
                                    'data' => [1 => "Accidente", 2 => "Cambio de piezas",],
                                    'options' => ['placeholder' => 'Selecciona el motivo de anulación'],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ]);
                            ?>
                        </div>
                    </div>
                </div>

            <?php ActiveForm::end(); ?>
            </br>
            <?php
            echo '<div class="row" style="padding-top: 25px;">';
                echo '<div class="text-center">';
                    echo Html::button('CANCELAR', ['class' => 'btn btn-cancelar', 'data' => ['dismiss' => 'modal']]);
                    echo "&nbsp;";
                    echo Html::submitButton('ANULAR', ['class' => 'btn btn-aceptar', 'form' => 'anular', 'id' => 'submit-anular' ]);
                echo '</div>';
            echo '</div>';
            ?>
        </div>
    </div>
  </div>
</div>
<!-- Fin modal-->

<script>
    $('#anularGarantiaModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var id = button.data('id');
        $('#submit-anular').data('id', id);
    });

    $('#anular').on('submit', function(event) {
        event.preventDefault();
        event.stopImmediatePropagation();

        $('#anularGarantiaModal').modal('hide')
        var id = $('#submit-anular').data('id');

        var form = $(this);
        var data = form.serializeArray();      
        data.push({name: 'id', value: id});

        $.post( form.attr("action"), data ).done(function(response){
            $.pjax.reload({container:"#garantias"});
        });
    });
</script>