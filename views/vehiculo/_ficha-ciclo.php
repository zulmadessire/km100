<?php

use yii\helpers\Html;
use yii\grid\GridView;
/*use yii\widgets\ActiveForm;*/
use yii\db\Query;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Vehiculo */
/* @var $form yii\widgets\ActiveForm */

$seguimiento_km = $model->idCategoria->seguimiento_km;
$declive_km = $model->idCategoria->declive_km;
$porcentaje_seguimiento_km = (floatval($model->km) * 100) / floatval($seguimiento_km);
$porcentaje_declive_km = ((floatval($model->km) - floatval($seguimiento_km) ) * 100) / (floatval($declive_km) - floatval($seguimiento_km));


$date1 = new DateTime($model->fecha_compra);
$date2 = new DateTime("now");
$interval = $date1->diff($date2);
$anos = number_format(($interval->days / 365), 2, '.', '');

$seguimiento_fecha = $model->idCategoria->seguimiento_fecha;
$declive_fecha = $model->idCategoria->declive_fecha;
$porcentaje_seguimiento_fecha = ($anos * 100) / floatval($seguimiento_fecha);
$porcentaje_declive_fecha = ( ($anos - $seguimiento_fecha) * 100) / floatval($declive_fecha);


if ( $porcentaje_declive_km > 0 ){
    $ciclo = 'Declive por Km';
} elseif ( $porcentaje_declive_fecha > 0 ) {
    $ciclo = 'Declive po tiempo de vida';
} else {
    $ciclo = 'Seguimiento';
}

$costoacumulado = 0;
foreach ($model->servicios as $value) {
    foreach ($value->solicitudServicioTallers as $value1) {
  
        foreach ($value1->cotizacionTallers as $value2) {
            if ($value2->activo ==1) {
                foreach ($value2->cotizacionTallerActividads as $value3) {
                    $costoacumulado = $costoacumulado+($value3->cantidad * $value3->precio);
            }  
            }
           
        }
    }
}

?>

 
<div class="ficha-datos">
    
    <div class="row">
        <div class="col-xs-12 text-center">
            <div class="col-xs-3" style="background-color: #deebf7; left:0; margin:0 auto; float: none; padding: 5px;">
                Prioridad de Remarketing: 
                <?php if ( $porcentaje_declive_km > 0 ){
                    echo 1;
                } elseif ( $porcentaje_declive_fecha > 0 ) {
                    echo 2;
                } else {
                    echo 3;
                }
                ?>
            </div>
            <!--<div class="col-xs-3" style="background-color: #deebf7; left:0; margin:0 auto; float: none; padding: 5px;">
                Sugerencia: <b>NEGOCIAR</b>
            </div>-->
        </div>
    </div>
     <?php $form = ActiveForm::begin([ 'id' => 'analisis-viabilidad-form']); ?>
     <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">ANÁLISIS DE VIABILIDAD</p>
            <hr>
        </div>
    </div>
  
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <label class="control-label">HISTÓRICO DE COSTO</label>
                <div class="input-group">
                  <div class="input-group-addon">RD$</div>
                   <?= Html::textInput('', $costoacumulado,['class' => 'form-control', 'disabled' => true, ]) ?>
                </div>
             </div>
        </div>

        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <?= Html::label('ETAPA DE CICLO DE VIDA', '', ['class' => 'control-label']) ?>
                <?= Html::textInput('', $ciclo,['class' => 'form-control', 'disabled' => true, ]) ?>
            </div>
        </div>

        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <label class="control-label">ÚLTIMO AVALÚO</label>
                <div class="input-group">
                  <div class="input-group-addon">RD$</div>
                  <input type="text" class="form-control" disabled="true">
                </div>
             </div>
        </div>

        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <?= Html::label('FACTOR DE SINIESTRALIDAD', '', ['class' => 'control-label']) ?>
                <?= Html::textInput('', '',['class' => 'form-control',]) ?>
            </div>
        </div>

        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <?= Html::label('UTILIZACIÓN PROMEDIO', '', ['class' => 'control-label']) ?>
                <?= Html::textInput('', '',['class' => 'form-control',]) ?>
            </div>
        </div>

        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <?= Html::label('UTILIZACIÓN NECESARIA', '', ['class' => 'control-label']) ?>
                <?= Html::textInput('', '',['class' => 'form-control',]) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
    </br>
    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">ESTATUS DEL CICLO DE VIDA POR KM</p>
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-md-3">
            <div style="padding-left: 40px;">
                <p>Kilometraje: <?= number_format(floatval($model->km), 0, ',', '.') ?>Km</p>
                <p>
                    Estatus: &nbsp;&nbsp;
                    <span class="label label-oval" style="background-color: <?= ($porcentaje_declive_km > 0) ?'#ffc000': '#548235'; ?>; <?= ($porcentaje_declive_km > 0) ?'color: #484848;': ''; ?>">
                        <?= ($porcentaje_declive_km > 0) ?'Declive': 'Seguimiento'; ?>
                    </span>
                </p>
            </div>
        </div>
        <div class="col-xs-12 col-md-8">
            <div style="border-left: 1px solid #9e9e9e; height: 80px; position: relative;">
                <div style="height: 50px; top: 20px; position: relative;">
                    <div style="border-right: 1px solid #9e9e9e; border-top: 1px solid #9e9e9e; border-bottom: 1px solid #9e9e9e; height: 40px; top: 20px; left: 0;">
                    </div>
                    <div style="border-right: 1px solid #9e9e9e; width: 70%; float: left; height: 100%; position: absolute; left: 0; top: 0; ">
                        <div style="border-top: 1px solid #9e9e9e; border-bottom: 1px solid #9e9e9e; background-color: #548235; height: 40px; width: <?= (($porcentaje_seguimiento_km <= 100)?$porcentaje_seguimiento_km:100); ?>%; ">
                        </div>
                        <?php if ( $porcentaje_seguimiento_km <= 100 ): ?>
                            <div style="border-right: 1px solid #9e9e9e; float: left; height: 70px; position: absolute; top: 0; left: <?= $porcentaje_seguimiento_km ?>%;">
                            </div>
                            <div style="top: 30px; font-weight: bold; position: relative; left: <?= $porcentaje_seguimiento_km - 5 ?>%; font-size: 11px; width: 80px;">
                                <i class="fa fa-map-marker" aria-hidden="true"></i> <?= number_format($model->km, 0, ',', '.') ?>km
                            </div>
                        <?php endif ?>
                    </div>
                    <div style="top: 15px; position: relative; left: 65%; font-size: 11px; width: 80px;">
                        <= <?= number_format($seguimiento_km, 0, ',', '.') ?>km
                    </div>
                    <div style="border-right: 1px solid #9e9e9e; width: 15%; float: left; height: 100%; position: absolute; top: 0; left: 70%;">
                        <div style="border-top: 1px solid #9e9e9e; border-bottom: 1px solid #9e9e9e; background-color: #ffc000; height: 40px; 
                            width: <?php if ( $porcentaje_declive_km > 0 && $porcentaje_declive_km <= 100) {
                                echo $porcentaje_declive_km;
                            } elseif ( $porcentaje_declive_km > 0 && $porcentaje_declive_km > 100) {
                                echo "100";
                            } else {
                                echo "0";
                            }?>%; ">
                        </div>
                        <?php if ( $porcentaje_declive_km > 0 ): ?>
                            <div style="border-right: 1px solid #9e9e9e; float: left; height: 70px; position: absolute; top: 0; left: <?= $porcentaje_declive_km ?>%;">
                            </div>
                            <div style="top: 30px; font-weight: bold; position: relative; left: <?= $porcentaje_declive_km - 10 ?>%; font-size: 11px; width: 80px;">
                                <i class="fa fa-map-marker" aria-hidden="true"></i> <?= number_format($model->km, 0, ',', '.') ?>km
                            </div>
                        <?php endif ?>
                    </div>
                    <div style="position: relative; left: 80%; font-size: 11px; width: 80px;">
                        < <?= number_format($declive_km, 0, ',', '.') ?>km
                    </div>
                </div>
            </div>
        </div>
    </div>
    </br>
    </br>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">ESTATUS DEL CICLO DE VIDA POR TIEMPO DE SERVICIO</p>
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-md-3">
            <div style="padding-left: 40px;">
                <p>Tiempo de Vida: <?= $anos ?> años </p>
                <p>
                    Estatus: &nbsp;&nbsp;
                    <span class="label label-oval" style="background-color: <?= ($porcentaje_declive_fecha > 0) ?'#ffc000': '#548235'; ?>; <?= ($porcentaje_declive_fecha > 0) ?'color: #484848;': ''; ?>">
                        <?= ($porcentaje_declive_fecha > 0) ?'Declive': 'Seguimiento'; ?>
                    </span>
                </p>
                <?php 
                     
                $fecha2=date("d-m-Y",strtotime($model->fecha_compra));
                 ?>
                <p>Fecha de compra: <?= $fecha2?> </p>   
            </div>
        </div>
        <div class="col-xs-12 col-md-8">
            <div style="border-left: 1px solid #9e9e9e; height: 80px; position: relative;">
                <div style="height: 50px; top: 20px; position: relative;">
                    <div style="border-right: 1px solid #9e9e9e; border-top: 1px solid #9e9e9e; border-bottom: 1px solid #9e9e9e; height: 40px; top: 20px; left: 0;">
                    </div>
                    <div style="border-right: 1px solid #9e9e9e; width: 50%; float: left; height: 100%; position: absolute; left: 0; top: 0; ">
                        <div style="border-top: 1px solid #9e9e9e; border-bottom: 1px solid #9e9e9e; background-color: #548235; height: 40px; width: <?= (($porcentaje_seguimiento_fecha <= 100)?$porcentaje_seguimiento_fecha:100); ?>%; ">
                        </div>
                        <?php if ( $porcentaje_seguimiento_fecha <= 100 ): ?>
                            <div style="border-right: 1px solid #9e9e9e; float: left; height: 70px; position: absolute; top: 0; left: <?= $porcentaje_seguimiento_fecha ?>%;">
                            </div>
                            <div style="top: 30px; font-weight: bold; position: relative; left: <?= $porcentaje_seguimiento_fecha - 5 ?>%; font-size: 11px; width: 80px;">
                                <i class="fa fa-map-marker" aria-hidden="true"></i> <?= $anos ?> años
                            </div>
                        <?php endif ?>
                    </div>
                    <div style="top: 15px; position: relative; left: 46%; font-size: 11px; width: 80px;">
                        <= <?= $seguimiento_fecha ?>años
                    </div>
                    <div style="border-right: 1px solid #9e9e9e; width: 50%; float: left; height: 100%; position: absolute; top: 0; left: 50%;">
                       <div style="border-top: 1px solid #9e9e9e; border-bottom: 1px solid #9e9e9e; background-color: #ffc000; height: 40px; 
                            width: <?php if ( $porcentaje_declive_fecha > 0 && $porcentaje_declive_fecha <= 100) {
                                echo $porcentaje_declive_fecha;
                            } elseif ( $porcentaje_declive_fecha > 0 && $porcentaje_declive_fecha > 100) {
                                echo "100";
                            } else {
                                echo "0";
                            }?>%; ">
                        </div>
                        <?php if ( $porcentaje_declive_fecha > 0 && $porcentaje_declive_fecha <= 100 ): ?>
                            <div style="border-right: 1px solid #9e9e9e; float: left; height: 70px; position: absolute; top: 0; left: <?= $porcentaje_declive_fecha ?>%;">
                            </div>
                            <div style="top: 30px; font-weight: bold; position: relative; left: <?= $porcentaje_declive_fecha - 10 ?>%; font-size: 11px; width: 80px;">
                                <i class="fa fa-map-marker" aria-hidden="true"></i> <?= $anos ?> años
                            </div>
                        <?php elseif ( $porcentaje_declive_fecha > 100 ): ?>
                            <div style="border-right: 1px solid #9e9e9e; float: left; height: 70px; position: absolute; top: 0; left: 100%;">
                            </div>
                            <div style="top: 30px; font-weight: bold; position: relative; left: 90%; font-size: 11px; width: 80px;">
                                <i class="fa fa-map-marker" aria-hidden="true"></i> <?= $anos ?> años
                            </div>
                        <?php endif ?>
                    </div>
                    <div style="position: relative; left: 96%; font-size: 11px; width: 82px; ">
                        <= 10 años
                    </div>
                </div>
            </div>
         
                    
        </div>
    </div>
    <br>

</div>
