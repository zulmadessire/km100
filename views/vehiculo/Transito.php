<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\db\Query;
use app\models\Marca;
use app\models\Modelo;
use app\models\vehiculo;
use app\models\Concesionario;



/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchVehiculo */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vehículos en tránsito';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vehiculo-index">
    
    <div class="row">
    <div class="col-md-1 col-xs-3">
      <h4 class="titulo"><i class="fa fa-car fa-4x" aria-hidden="true"></i></h4>
    </div>
    <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
      <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
    </div>
  </div>
         <div class="col-md-12">
            <div class="pull-right">
                <?= Html::a('Registrar Compra',['vehiculo/registratransito'], ['class' => 'btn btn-accion']) ?>
                
            </div>
        </div>
          </br>
        </br>
    <div class="row">
        <div class="col-md-12">
            <span>Total de Vehículos: <?= $dataProvider->getTotalCount() ?></span>
            <!--<div class="pull-right">
                <?php // Html::a('Registrar Compra',['/vehiculo/create'], ['class' => 'btn btn-filtro']) ?>&nbsp;
            </div> -->
        </div>
        </br>
        </br>
        <div class="col-md-12">

            
                <?= GridView::widget([
                    'dataProvider' => $dataProvider_mp,
                    'tableOptions' => ['class' => 'table table-responsive'],
                    'summary' => '',
                    'options' =>[
                        'class' => 'grid',
                    ],
                    'columns' => [
                           [
                            'label' => 'Marca',
                            'format' => 'text',
                            'filter' => false,
                            'enableSorting' => false,
                            'value' => function( $model ){
                                $var = $model->id_modelo;
                                if ($var) {
                                $var2 = Modelo::findOne($var)->id_marca;
                                $marca = Marca::findOne($var2)->nombre;

                                return $marca;
                                
                                }
                                else{return "Sin Marca";}
                                 
                            }
                        ],
                        [
                            'attribute' => 'id_modelo',
                            'label' => 'Modelo',
                            'format' => 'text',
                            'filter' => false,
                            'enableSorting' => false,
                            'value' => function( $model ){
                                
                                // return $model->idModelo->nombre;
                                     $var = $model->id_modelo;
                                     if ($var) {
                                         # code...
                                return Modelo::findOne($var)->nombre;
                                     }
                                     else{return "Sin Modelo";}
                                   
                             
                            }

                        ],
                        [
                            'label' => 'Concesionario',
                            'format' => 'text',
                            'filter' => true,
                            'enableSorting' => false,
                            'value' => function( $model ){
                                // $var = Concesionario::findOne($model->idConcesionario);
                               // return Concesionario::findOne($model->id_concesionario)->nombre;
                                $var = $model->id_concesionario;
                                if($var){
                                    return Concesionario::findOne($var)->nombre;
                                }
                                else{
                                return "No posee Concesionario";
                                }


                            }
                        ],
                             [
                            'label' => 'Fecha de Compra',
                            'format' => 'text',
                            'filter' => false,
                            'enableSorting' => false,
                            'value' => function( $model ){
                                // $var = Concesionario::findOne($model->idConcesionario);
                               // return Concesionario::findOne($model->id_concesionario)->nombre;
                               return $model->fecha_compra;


                            }
                        ],
                        [
                            'label' => 'Cantidad',
                            'format' => 'text',
                            'filter' => false,
                            'enableSorting' => false,
                            'value' => function( $model ){
                                // $var = Concesionario::findOne($model->idConcesionario);
                               // return Concesionario::findOne($model->id_concesionario)->nombre;
 
                               

                  return Vehiculo::find()->where(['id_modelo' => $model->id_modelo])->count();



                                                             


                            }
                        ],
                            [
                            // 'label' => 'Fecha de Compra',
                            'format' => 'html',
                            'filter' => false,
                            'enableSorting' => false,
                            'value' => function( $model ){
                               $estado = "Recibido";
      return '<span class="label label-oval" style="background-color:#0070C0; ">'.$estado.'</span>';







                            }
                        ],
                    ],
                         
                ]); ?>
   
            
        </div>
    </div>
</div>
