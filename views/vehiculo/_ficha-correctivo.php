<?php

use yii\helpers\Html;
use yii\db\Query;
use kartik\grid\GridView;
use app\models\PipelineServicio;
use app\models\EstadoServicio;
use app\models\Empresa;
use app\models\Cliente;
use yii\helpers\ArrayHelper;
use app\models\Estacion;
use yii\data\Pagination;
use yii\widgets\LinkPager;
use kartik\select2\Select2;
use app\models\CotizacionTallerActividadSearch;

/* @var $this yii\web\View */
/* @var $model app\models\Vehiculo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ficha-correctivo">

    <div class="row">
        <div class="col-xs-12">
            <p style="font-weight: bold; margin-left: 20px; font-size: 11pt;">LISTADO DE MTTOS. CORRECTIVOS</p>
        </div>
    </div>
    <br>
     <?php
       $estaciones = ArrayHelper::map(Estacion::find()->asArray()->all(), 'id_estacion', function($model, $defaultValue) {
                                                                                            $emp = Empresa::findOne($model['id_empresa']);
                                                                                            return $model['nombre'].' ('.$emp->nombre.')';
                                                                                        });
     ?>

    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right">
                <?= Html::a('Histórico de piezas',[ '/vehiculo/historicopiezas', 'id' => $model->id ], ['class' => 'btn btn-small-gris']) ?>&nbsp;
                <?= Html::a('Exportar',['/vehiculo/exportar', 'id' => $model->id], ['class' => 'btn btn-small-verde']) ?> 
            </div>
        </div>
        </br>
        </br>
        <div class="col-xs-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider_servicio,
                'filterModel' => $searchModel_servicio,
                'tableOptions' => ['class' => 'table table-responsive'],
                'options' =>[
                    'class' => 'grid',
                ],
                'columns' => [
                    [
                        'class'=>'kartik\grid\ExpandRowColumn',
                        'width'=>'50px',
                        'allowBatchToggle' => false,
                        'expandIcon' => '<i class="fa fa-caret-right" aria-hidden="true" style="color:#d9d9d9;"></i>',
                        'collapseIcon' => '<i class="fa fa-caret-down" aria-hidden="true" style="color:#d9d9d9;"></i>',
                        'value'=>function ($model, $key, $index, $column) {
                            $query = new query();
                            $query->select('es.nombre, es.color, es.codigo')
                                ->from('pipeline_servicio p')
                                ->join('INNER JOIN','estado_servicio es','es.id = p.id_estado_servicio')
                                ->where('p.id_servicio = '. $model->id )
                                ->orderBy('p.id desc');

                            $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                            $estado = Yii::$app->db->createCommand($sql)->queryOne();

                            if ( $estado['codigo'] == 10 || $estado['codigo'] == 11 ) {
                                return '<i></i>';
                            } else{
                                return GridView::ROW_COLLAPSED;
                            }
                            
                        },
                        'detail'=>function ($model, $key, $index, $column) {
                            $pipeline_servicio = PipelineServicio::find()->where([ 'id_servicio' => $model->id ])->andWhere('id_estado_servicio != 10 AND id_estado_servicio != 11')->orderby('id DESC')->all();
                            $pipeline_servicio_first = PipelineServicio::find()->where([ 'id_servicio' => $model->id ])->orderby('id DESC')->one();
                            
                            if ( $pipeline_servicio_first->idEstadoServicio->codigo != 10 || $pipeline_servicio_first->idEstadoServicio->codigo != 11 ) {
                                if ( $pipeline_servicio_first->idEstadoServicio->codigo >= 5 && $pipeline_servicio_first->idEstadoServicio->codigo <= 9 ) {
                                    $query = new query();
                                    $query->select('c.id')
                                        ->from('solicitud_servicio_taller sst')
                                        ->join('INNER JOIN','cotizacion_taller c','c.id_solicitud_servicio_taller = sst.id AND c.estado = 1 AND c.activo = 1')
                                        ->where('sst.id_servicio = '. $model->id)
                                        ->orderBy('c.id desc');

                                    $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                                    $id_cotizacion = Yii::$app->db->createCommand($sql)->queryOne();

                                    $searchModel_cotizacion_actividad = new CotizacionTallerActividadSearch([ 'id_cotizacion_taller' => $id_cotizacion['id'] ]);
                                    $dataProvider_cotizacion_actividad = $searchModel_cotizacion_actividad->search(Yii::$app->request->queryParams);

                                    return Yii::$app->controller->renderPartial('_expand-ficha-correctivo', ['pipeline_servicio' => $pipeline_servicio, 'dataProvider_cotizacion_actividad' => $dataProvider_cotizacion_actividad, ]);
                                } else{
                                    return Yii::$app->controller->renderPartial('_expand-ficha-correctivo', ['pipeline_servicio' => $pipeline_servicio]);
                                }
                            } else{
                               return '';
                            }
                           
                        },
                        'headerOptions'=>['class'=>'kartik-sheet-style'],
                        'expandOneOnly'=>true,
                        'enableRowClick' => true,
                    ],
                    [
                        'attribute' => 'id',
                        'label' => 'Número de solicitud',
                        'format' => 'text',
                        'filter' => true,
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'servicio',
                        'label' => 'Tipo de solicitud',
                        'format' => 'raw',
                        'filterType'=> GridView::FILTER_SELECT2,
                        'filter'=> ['0'=>'Mecánica Ligera','1'=>'Mecánica Especializada','2'=>'Aire Acondicionado','3'=>'Desabolladura y pintura','4'=>'Accesorios'],
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                        ],
                        'filterInputOptions'=>['placeholder'=>'Selecciona'],
                        'format'=>'raw',                   
                        'enableSorting' => false,
                        'value' => function( $model ){
                            if ( $model->servicio ) {
                                $servicios = explode("*", $model->servicio);

                                foreach ($servicios as $key => $servicio) {
                                    switch ($servicio) {
                                        case '0':
                                            $vector_servicios[] = 'Mecánica Ligera';
                                            break;

                                        case '1':
                                            $vector_servicios[] = 'Mecánica Especializada';
                                            break;

                                        case '2':
                                            $vector_servicios[] = 'Aire Acondicionado';
                                            break;

                                        case '3':
                                            $vector_servicios[] = 'Desabolladura y pintura';
                                            break;

                                        case '4':
                                            $vector_servicios[] = 'Accesorios';
                                            break;
                                        
                                        default:
                                            # code...
                                            break;
                                    }
                                    
                                }

                                $respuesta_servicios = implode('<br>', $vector_servicios);

                                return $respuesta_servicios;
                            } else{
                                return '-';
                            }
                        }
                    ],
                    [
                        'attribute' => 'id_cliente',
                        'label' => 'Cliente',
                        'filterType'=> GridView::FILTER_SELECT2,
                        'filter'=> ArrayHelper::map(Cliente::find()->orderBy('nombre_comercial')->all(), 'id', 'nombre_comercial'),
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                        ],
                        'filterInputOptions'=>['placeholder'=>'Selecciona'],
                        'format'=>'raw',
                        'enableSorting' => false,
                        'value' => function( $model ){
                            if ( $model->id_estacion_actual ) {
                                 return $model->idEstacionActual->idEmpresa->cliente->nombre_comercial;
                            } else{
                                return '-';
                            }
                        }
                    ],
                    [
                        'attribute' => 'id_company',
                        'label' => 'Empresa',
                        'format' => 'raw',
                        'filter' => true,
                        'enableSorting' => false,
                        'value' => function ($model, $key, $index, $column) {
                          return $model->idEstacionActual->idEmpresa->nombre;
                              },
                        'filterType'=> GridView::FILTER_SELECT2,
                        'filter'=> ArrayHelper::map(Empresa::find()->orderBy('nombre')->all(), 'id_empresa', 'nombre'),
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                        ],
                        'filterInputOptions'=>['placeholder'=>'Selecciona'],
                        'format'=>'raw',
                    ],
                    [
                        'attribute' => 'id_estacion_actual',
                        'label' => 'Estacion',
                        'format' => 'raw',
                        'filter' => true,
                        'enableSorting' => false,
                        'value' => function ($model, $key, $index, $column) {
                            return $model->idEstacionActual->nombre;
                        },
                        'filterType'=> GridView::FILTER_SELECT2,
                        'filter'=> ArrayHelper::map(Estacion::find()->orderBy('nombre')->all(), 'id_estacion', 'nombre'),
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                        ],
                        'filterInputOptions'=>['placeholder'=>'Selecciona'],
                    ], 
                    [
                        'attribute' => 'dias',
                        'format' => 'text',
                        'filter' => true,
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'estatus',
                        'label' => 'Estatus',
                        'format' => 'html',
                        'enableSorting' => true,
                        'filterType'=> GridView::FILTER_SELECT2,
                        'filter'=>ArrayHelper::map(EstadoServicio::find()->orderBy('nombre')->all(), 'id', 'nombre'),
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                        ],
                        'filterInputOptions'=>['placeholder'=>'Selecciona'],
                        'format'=>'raw',
                    ],  
                ],
            ]); ?>
        </div>
    </div>

</div>
<script>
    $('.table').removeClass( "table-bordered table-striped" );
</script>