<?php

use yii\helpers\Html;
use app\models\Marca;

/* @var $this yii\web\View */
/* @var $model app\models\Vehiculo */

$this->title = 'Ficha Vehicular';

$marca = Marca::findOne($model->idModelo->id_marca)->nombre;
?>
<div>
	<div class="row">
		<div class="col-md-1 col-xs-3">
			<h4 class="titulo"><i class="fa fa-clipboard fa-4x" aria-hidden="true"></i></h4>
		</div>
		<div class="col-md-10 col-xs-9" style="padding-top: 30px;">
			<h4 class="titulo"><span> <?= Html::encode( $this->title .' de '. $marca .' '. $model->idModelo->nombre ) ?></span></h4>
		</div>
	</div>
	</br>

	<div class="row">
        <div class="col-md-12 col-xs-12">
            <p style="font-size: 11pt;"><b><?= Html::encode('HISTÓRICO DE PIEZAS') ?></b></p>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="pull-right">
            <?= Html::a('Listado de Mttos. Correctivos',[ '/vehiculo/ficha', 'id' => $model->id, 'type' => 4 ], ['class' => 'btn btn-small-gris']) ?>
        </div>
    </div>

</div>