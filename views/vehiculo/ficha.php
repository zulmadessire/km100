<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Vehiculo */

$this->title = 'Ficha Vehicular';
/*$this->params['breadcrumbs'][] = ['label' => 'Vehiculos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';*/

$modelo = $model->idModelo->nombre;
$marca = $model->idModelo->idMarca->nombre;
?>
<div class="ficha-vehiculo">
	
	<div class="row">
		<div class="col-md-1 col-xs-3">
			<h4 class="titulo"><i class="fa fa-clipboard fa-4x" aria-hidden="true"></i></h4>
		</div>
		<div class="col-md-10 col-xs-9" style="padding-top: 30px;">
			<h4 class="titulo"><span> <?= Html::encode( $this->title .' de '. $marca .' '. $modelo ) ?></span></h4>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<div class="square" >
	            <ul>
	                <li>
	                    <a href="<?= Yii::$app->request->baseUrl ?>/vehiculo/ficha/<?= $model->id ?>" <?= ($type == 0)?'class="active"':'' ?>>
	                        <i class="fa fa-car" aria-hidden="true"></i>
	                        <p>Datos del Vehículo</p>
	                    </a>
	                </li>
	                <li>
	                    <a href="<?= Yii::$app->request->baseUrl ?>/vehiculo/ficha/<?= $model->id ?>?type=1" <?= ($type == 1)?'class="active"':'' ?>>
	                        <i class="fa fa-file-archive-o" aria-hidden="true"></i>
	                        <p>Documentación</p>
	                    </a>
	                </li>
	                <li>
	                    <a href="<?= Yii::$app->request->baseUrl ?>/vehiculo/ficha/<?= $model->id ?>?type=2" <?= ($type == 2)?'class="active"':'' ?>>
	                        <i class="fa fa-line-chart" aria-hidden="true"></i>
	                        <p>Ciclo de Vida</p>
	                    </a>
	                </li>
	                <li>
	                    <a href="<?= Yii::$app->request->baseUrl ?>/vehiculo/ficha/<?= $model->id ?>?type=3" <?= ($type == 3)?'class="active"':'' ?>>
	                        <i class="fa fa-dashboard" aria-hidden="true"></i>
	                        <p>Mttos. Preventivos</p>
	                    </a>
	                </li>  
	                <li>
	                    <a href="<?= Yii::$app->request->baseUrl ?>/vehiculo/ficha/<?= $model->id ?>?type=4" <?= ($type == 4)?'class="active"':'' ?>>
	                        <i class="fa fa-wrench" aria-hidden="true"></i>
	                        <p>Mttos. Correctivos</p>
	                    </a>
	                </li>
	            </ul>
        	</div>
		</div>
	</div>
	</br>
	</br>

	<div class="row">
		<div class="col-xs-12">
			<?php if ( $type == 0 ) { ?>
				<?= $this->render('_ficha-datos', [
			        'model' => $model,
			        'dataProvider_flota' => $dataProvider_flota
			    ]) ?>
			<?php } else if( $type == 1 ){ ?>
				<?= $this->render('_ficha-documentacion', [
			        'model' => $model,
			        'dataProvider_gv' => $dataProvider_gv,
			        'model_gv' => $model_gv
			    ]) ?>
			<?php } else if( $type == 2 ){ ?>
				<?= $this->render('_ficha-ciclo', [
			        'model' => $model,
			    ]) ?>
			<?php } else if( $type == 3 ){ ?>
				<?= $this->render('_ficha-preventivo', [
			        'model' => $model,
                	'proyecciones' => $proyecciones,
                	'dataProvider_mp' => $dataProvider_mp,
                	'searchModel_mp' => $searchModel_mp,
			    ]) ?>
			<?php } else if( $type == 4 ){ ?>
				<?= $this->render('_ficha-correctivo', [
					'model' => $model,
                	'dataProvider_servicio' => $dataProvider_servicio,
                	'searchModel_servicio' => $searchModel_servicio,
			    ]) ?>
			<?php }?>
			
		</div>
	</div>
    

    


    
</div>
