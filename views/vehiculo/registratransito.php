<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\db\Query;
use app\models\Marca;
use app\models\Modelo;
use app\models\vehiculo;
use app\models\Concesionario;
use yii\widgets\ActiveForm; 
use kartik\file\FileInput;
use yii\helpers\Url;
 

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchVehiculo */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vehículos en tránsito';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vehiculo-index">
    
    <div class="row">
    <div class="col-md-1 col-xs-3">
      <h4 class="titulo"><i class="fa fa-car fa-4x" aria-hidden="true"></i></h4>
    </div>
    <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
      <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
    </div>
  </div>
 
        </br>

    <h4 class="titulo">CARGAR LISTADO DE VEHICULOS EN TRÁNSITO</span></h4>


    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>


<div class="row">
  <div class="col-md-6">
 <?php 
  echo $form->field($model, 'file')->widget(FileInput::classname(), [
    'options'=>['accept'=>'image/*'],
    'pluginOptions'=>['allowedFileExtensions'=>['xlsx']
]])->label(false);
 ?>
  </div> 
    <div class="col-md-3">
            <div class="pull-right">
                <?= Html::a('Vehículos en Tránsito',['vehiculo/transito'], ['class' => 'btn btn-accion']) ?>
                
            </div>
        </div>
  <div class="col-md-3" style="float: right">
         <?php if (Yii::$app->session->hasFlash('errordownload')): ?>
<strong class="label label-danger">¡Ha ocurrido un error al descargar el archivo!</strong>

<?php else: ?>
<a href="<?= Url::toRoute(['download', 'file' => 'Flotaoperativa.xlsx']) ?>">Descargar archivo ejemplo</a>
 

<?php endif; ?>
        </div>
      
             
</div>



<br><br><br>
  <div class="form-group" align="center">
                <?= Html::submitButton('CARGAR', ['class' => 'btn btn-primario']) ?>
            </div>
 

<?php ActiveForm::end() ?>



 