<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm; 
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\Vehiculo */

$this->title = 'Carga de Vehículos';

?>
<div class="cargar-vehiculos">
	<div class="row">
        <div class="col-md-1 col-xs-3">
            <i class="fa fa-upload fa-4x" aria-hidden="true"></i>
        </div>
        <div class="col-md-10 col-xs-9" style="padding-top: 10px;">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
        </div>
    </div>
    </br>
    </br>
	
	<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
    
    <div class="row">
    	<div class="col-xs-12">
    		<?=
    			$form->field($upload_vehiculos, 'file')->widget(FileInput::classname(), [
	    			'options'=>['accept'=>'xls/*'],
	    			'pluginOptions'=>['allowedFileExtensions'=>['xlsx']
				]])->label(false);
 			?>
    	</div>
    </div>

    <?php ActiveForm::end() ?>
</div>