<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
/*use yii\widgets\ActiveForm;*/
use yii\db\Query;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Vehiculo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ficha-datos">
    
    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DEL VEHÍCULO</p>
            <hr>
        </div>
    </div>

    <div class="row">
        <?php $form = ActiveForm::begin(); ?>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'id_modelo')->textInput(['disabled' => true, 'value' => $model->idModelo->idMarca->nombre ])->label('Marca') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'id_modelo')->textInput(['value' => strtoupper($model->idModelo->nombre), 'disabled' => true])->label('Modelo') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'color')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'transmision')->textInput(['maxlength' => true, 'disabled' => true])->label('Transmisión') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'chasis')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>


        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'placa')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'mva')->textInput(['maxlength' => true, 'disabled' => true])->label('Ficha o MVA') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'categoria')->textInput(['maxlength' => true, 'disabled' => true, 'value' => $model->idCategoria->nombre])->label('Categoría') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'clase')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'combustible')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'fecha_compra', ['addon' => ['prepend' => ['content'=>'<i class="fa fa-calendar" aria-hidden="true"></i>']] ] )->textInput(['disabled' => true, 'value' => date('d/m/Y', strtotime($model->fecha_compra)) ])->label('Fecha de compra') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'fecha_inicial', ['addon' => ['prepend' => ['content'=>'<i class="fa fa-calendar" aria-hidden="true"></i>']] ] )->textInput(['disabled' => true, 'value' => date('d/m/Y', strtotime($model->fecha_inicial)) ])->label('Fecha de inicio de servicio') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'id_concesionario')->textInput(['disabled' => true, 'value' => $model->idConcesionario->nombre ])->label('Concesionario') ?>
        </div>
        <?php if ($model->anho): ?>
                  <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'anho')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>
        <?php endif ?>
    <?php if (!$model->anho): ?>
    <div class="col-md-4 col-xs-4">
         <?= $form->field($model, 'anho')->textInput(['maxlength' => true, 'disabled' => true, 'value'=>'No posee']) ?>
           </div> 
    <?php endif ?>
  
        <?php if ($model->fecha_venta): ?>
            <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'fecha_venta')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div> 
        <?php endif ?>
          <?php if (!$model->fecha_venta): ?>
            <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'fecha_venta')->textInput(['maxlength' => true, 'disabled' => true, 'value'=>'No posee']) ?>
        </div> 
        <?php endif ?>
       
        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <label class="control-label" for="qr">CÓDIGO QR</label>
                <div class="clearfix"></div>
                <img id="qr" src="<?= Url::to(['vehiculo/qr', 'chasis' => $model->chasis ]) ?>" width="160" height="160" />
            </div>
        </div>


        <?php ActiveForm::end(); ?>
    </div>
    <br>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">TRAZABILIDAD DEL VEHÍCULO</p>
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">ESTATUS</p>

            <?php 
                $query = new query();
                $query->select('es.nombre, es.color')
                    ->from('estado_vehiculo ev')
                    ->join('INNER JOIN','estado es','es.id = ev.id_estado AND es.id_tipo_estado = 1')
                    ->where('ev.id_vehiculo = '. $model->id )
                    ->orderBy('ev.id desc');

                $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                $estado = Yii::$app->db->createCommand($sql)->queryOne();
            ?>

            <h4><span class="label label-oval" style="background-color: <?= $estado['color'] ?>; text-transform: uppercase;">
                <?= $estado['nombre'] ?>
            </span></h4>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-xs-12">
            <h5 style="font-weight: bold; margin-left: 20px;">TRAZABILIDAD POR AGENCIAS</h5>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider_flota,
                'tableOptions' => ['class' => 'table table-responsive'],
                'summary' => '',
                'options' =>[
                    'class' => 'grid',
                ],
                'columns' => [
                    [
                        'label' => 'Cliente',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                        'value' => function( $model ){
                            $query = new query();
                            $query->select('cli.nombre_comercial')
                                ->from('estacion es')
                                ->join('INNER JOIN','empresa em','es.id_empresa = em.id_empresa')
                                ->join('INNER JOIN','cliente cli','em.cliente_id = cli.id')
                                ->where('es.id_estacion = '. $model->id_estacion );

                            $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                            $agencia = Yii::$app->db->createCommand($sql)->queryOne();

                            return $agencia['nombre_comercial'];
                        }
                    ],
                         [
                        'label' => 'Empresa',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                        'value' => function( $model ){
                            $query = new query();
                            $query->select('em.nombre')
                                ->from('estacion es')
                                ->join('INNER JOIN','empresa em','es.id_empresa = em.id_empresa')
                                ->join('INNER JOIN','cliente cli','em.cliente_id = cli.id')
                                ->where('es.id_estacion = '. $model->id_estacion );

                            $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                            $agencia = Yii::$app->db->createCommand($sql)->queryOne();

                            return $agencia['nombre'];
                        }
                    ],
                    [
                        'label' => 'Estación',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                        'value' => function( $model ){
                            return $model->idEstacion->nombre;
                        }
                    ],
                    [
                        'attribute' => 'fecha_inicio',
                        'label' => 'Última Verificación',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                        'value' => function( $model ){
                            return date( 'd/m/Y', strtotime($model->fecha_inicio) );
                        }
                    ],
                ],
            ]); ?>
        </div>
    </div>


</div>
