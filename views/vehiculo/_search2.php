<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use app\models\Modelo;
use app\models\Marca;
use app\models\Estacion;
use app\models\Estado;
use app\models\Empresa;
use app\models\Cliente;
use app\models\Concesionario;
use app\models\TipoMantenimiento;
use kartik\date\DatePicker;
use kartik\field\FieldRange;
/* @var $this yii\web\View */
/* @var $model app\models\SearchVehiculo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vehiculo-search">

    <div class="row">
        <?php $form = ActiveForm::begin([
            'action' => ['indexmantenimiento'],
            'method' => 'get',
        ]); ?>
        <div class="col-xs-12">
            <p class="titulo-form">FILTROS DE BÚSQUEDA</p>
            <hr>
        </div>
        <div class="col-xs-12 col-md-12">
            <div class="pull-right">
                <?= Html::a('Limpiar filtros', '#', ['id' => 'reset', 'class' => 'reset-filtros']) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <label class="control-label" for="id_marca">MARCA</label>
                <?=  Select2::widget([
                        'name' => 'VehiculoSearch[id_marca]',
                        'data' => ArrayHelper::map(Marca::find()->orderBy('nombre')->all(), 'id_marca', 'nombre'),
                        'options' => ['placeholder' => 'Selecciona la marca', 'id' => 'id_marca']
                    ]); 
                ?>
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <?=
                $form->field($model, 'id_modelo')->widget(DepDrop::classname(), [
                    'type'=>DepDrop::TYPE_SELECT2,
                    'options'=>[
                            'id'=>'id_modelos',
                            'name' => 'VehiculoSearch[id_modelo]',
                            ],
                    'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                    'pluginOptions'=>[
                        'depends'=>['id_marca'],
                        'placeholder'=>'Selecciona un modelo',
                        'url'=>Url::to(['/vehiculo/getmodelo'])
                    ]
                ])->label('MODELO');
            ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'mva') ?>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <label class="control-label" for="id_cliente">CLIENTE</label>
                <?=  Select2::widget([
                        'name' => 'VehiculoSearch[id_cliente]',
                        'data' => ArrayHelper::map(Cliente::find()->where('id != 6')->orderBy('nombre_comercial')->all(), 'id', 'nombre_comercial'),
                        'options' => ['placeholder' => 'Selecciona el cliente', 'id' => 'id_cliente']
                    ]); 
                ?>
            </div>
        </div>

        <div class="col-md-4 col-xs-12">
            <label class="control-label" for="id_empresa">EMPRESA</label>
            <?=
                DepDrop::widget([
                    'name' => 'VehiculoSearch[id_empresa]',
                    'type'=>DepDrop::TYPE_SELECT2,
                    'options'=>['id'=>'id_empresa', 'class' => 'form-control'],
                    'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                    'pluginOptions'=>[
                        'depends'=>['id_cliente'],
                        'placeholder'=>'Selecciona la empresa',
                        'url'=>Url::to(['/vehiculo/getempresas'])
                    ]
                ]);
            ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <label class="control-label" for="id_estado">ESTACIÓN</label>
                <?=
                    DepDrop::widget([
                        'name' => 'VehiculoSearch[id_estacion]',
                        'type'=>DepDrop::TYPE_SELECT2,
                        'options'=>['id'=>'id_estacion'],
                        'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                        'pluginOptions'=>[
                            'depends'=>['id_empresa'],
                            'placeholder'=>'Selecciona la estación',
                            'url'=>Url::to(['/vehiculo/getestaciones'])
                        ]
                    ]);
                ?>
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <label class="control-label" for="id_estado">ESTATUS</label>
                <?= 
                    Select2::widget([
                        'name' => 'VehiculoSearch[estado]',
                        'data' => ArrayHelper::map(Estado::find()->where(['id_tipo_estado' => '4'])->orderBy('nombre')->all(), 'id', 'nombre'),
                        'options' => ['placeholder' => 'Selecciona el estatus',]
                    ]);
                ?>
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <label class="control-label" for="id_tipo_mantenimiento">TIPO DE MANTENIMIENTO</label>
                <?= 
                    Select2::widget([
                        'name' => 'VehiculoSearch[id_tipo_mantenimiento]',
                        'data' => ArrayHelper::map(TipoMantenimiento::find()->orderBy('nombre')->all(), 'id_tipo_mnto', 'nombre'),
                        'options' => ['placeholder' => 'Selecciona el tipo',]
                    ]);
                ?>
            </div>
        </div> 
         
    <!-- 
        <div class="col-md-4 col-xs-12" style="padding-top: 25px;">
            <div class="form-group">
                <label class="control-label" for="id_estado">PRÓXIMO A MTTO.</label>&nbsp;
                <?php // Html::checkbox('VehiculoSearch[mantenimiento]', false, ['class' => '' ]); ?>
            </div>
        </div>
         <div class="col-md-4 col-xs-12" style="padding-top: 25px;">
            <div class="form-group">
                <label class="control-label" for="id_estado">MTTOS ATRASADOS.</label>&nbsp;
                <?php // Html::checkbox('VehiculoSearch[atrasado]', false, ['class' => '']); ?>
            </div>
        </div>
    </div>  
    <div class="row">
        <div class="col-md-4 col-xs-12" style="padding-top: 25px;">
        <?php /*
            echo '<label class="control-label">Filtrar por rango de fechas</label>';
                echo DatePicker::widget([
                    'name' => 'VehiculoSearch[fecha1]',
                    'type' => DatePicker::TYPE_RANGE,
                    'name2' => 'VehiculoSearch[fecha2]', 
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-m-dd'
                    ]
                ]);*/
         ?>
         </div>
         <br>
    </div>-->
        <br>
        <div class="col-md-12 col-xs-12">
            <div class="form-group text-center">
                <?= Html::submitButton('BUSCAR', ['class' => 'btn btn-submit']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

</div>

<script>
    $("a#reset").on("click", function(event){
        event.preventDefault();
        var myForm = $(this).closest('form').get(0);
        myForm.reset();
        $("select", myForm).each(
            function () {
                $(this).select2('val','All');
            }
        );
        $("input", myForm).each(
            function () {
                $(this).val('');
            }
        );
    });
</script>
