<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\db\Query;
use app\models\Marca;
use app\models\Modelo;
use app\models\Empresa;
use app\models\Estacion;
use app\models\servicio;
use app\models\PipelineServicio;
use app\models\Flota;
use app\models\vehiculo;
use app\models\Concesionario;



/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchVehiculo */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Resumen de administración de flota';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="flota-index">
    
    <div class="row">
    <div class="col-md-1 col-xs-3">
      <h4 class="titulo"><i class="fa fa-car fa-4x" aria-hidden="true"></i></h4>
    </div>
    <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
      <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
    </div>
  </div>
    <div class="row">
        <div class="col-md-12">
 
            
        </div>
        </br>
        </br>
        <div class="col-md-12">

            
                <?= GridView::widget([
                    'dataProvider' => $dataProviderResumen,
                    'tableOptions' => ['class' => 'table table-responsive'],
                    'summary' => '',
                    'options' =>[
                        'class' => 'grid',
                    ],
                    'columns' => [
                           [
                            'label' => 'Empresa',
                            'format' => 'text',
                            'filter' => false,
                            'enableSorting' => false,
                            'value' => function( $model ){
                            	  $var = $model->id_estacion;
                               if ($var) {
                              $var2 = Estacion::findOne($var)->id_empresa;
                               $empresa = Empresa::findOne($var2)->nombre;

                                return $empresa;
                                 
                               }
                        }
                        ],
                             [
                            'label' => 'Flota',
                            'format' => 'text',
                            'filter' => false,
                            'enableSorting' => false,
                            'value' => function( $model ){
                            	 $var = $model->id_estacion;
                               if ($var) {
                              $var2 = Estacion::findOne($var)->id_empresa;
                               $empresa = Empresa::findOne($var2)->id_empresa;

                                return Flota::find()->joinWith('idEstacion')->where(['estacion.id_empresa' => $empresa])->count();
           
                                 
                               }
                        }
                        ],
                        [
                            'label' => 'Fuera de servicio',
                            'format' => 'text',
                            'filter' => false,
                            'enableSorting' => false,
                            'value' => function( $model ){
                            	 
                            	 $var = $model->id_estacion;
                               if ($var) {
                              $var2 = Estacion::findOne($var)->id_empresa;
                               $empresa = Empresa::findOne($var2)->id_empresa;

                            $vehiculos = Flota::find()->joinWith('idEstacion')->where(['estacion.id_empresa' => $empresa])->all();
                            // $vehiculos_servicio = PipelineServicio::find()
                            //                     ->joinWith('idServicio')
                            //                     ->joinWith('idEstacionActual')
                            //                     ->joinWith('servicios')
                            //                     ->where(['estacion.id_empresa' => $empresa])
                            //                     ->where(['pipeline_servicio.id_estado_servicio' => 9])
                            //                     ->count();


                            
                            

                                return $vehiculos[0]->id;
           
                           
                        }
                    }
                        ],
                         [
                            'label' => 'Mantenimiento',
                            'format' => 'text',
                            'filter' => false,
                            'enableSorting' => false,
                            'value' => function( $model ){
                            	 
                             
                                return '-';
           
                           
                        }
                        ],
                              [
                            'label' => 'Ciclo de vida',
                            'format' => 'text',
                            'filter' => false,
                            'enableSorting' => false,
                            'value' => function( $model ){
                            	 
                             
                                return '-';
           
                           
                        }
                        ],
                           [
                            'label' => 'Inconsistencias',
                            'format' => 'text',
                            'filter' => false,
                            'enableSorting' => false,
                            'value' => function( $model ){
                            	 
                             
                                return '-';
           
                           
                        }
                        ],
         
         
         
                  
                    ],
                         
                ]); ?>
   
            
        </div>
    </div>
</div>
