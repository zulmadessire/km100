<?php

use yii\helpers\Html;
use yii\db\Query;
use kartik\grid\GridView;
use app\models\Pieza;

/* @var $this yii\web\View */
/* @var $model app\models\Vehiculo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="expand-ficha-preventivo">
    
    <div class="row">
        <div class="col-md-3 col-xs-6">
            <p><b>KM DE MTTO. PROYECTADA</b></p>
            <p><?= number_format(floatval($model->idProyeccionMantenimiento->km_proyectado), 0, ',', '.') . ' Km' ?></p>
        </div>
        <div class="col-md-3 col-xs-6">
            <p><b>KM DE REALIZACIÓN MTTO. </b></p>
            <p><?= number_format(floatval($model->km), 0, ',', '.') . ' Km' ?></p>
        </div>            
        <div class="col-md-3 col-xs-6">
            <p><b>KM DE DIFERENCIA </b></p>
            <p><?= number_format((floatval($model->km) - floatval($model->idProyeccionMantenimiento->km_proyectado)), 0, ',', '.') . ' Km' ?></p>
        </div>    
        <div class="col-md-3 col-xs-6">
            <p><b>RESPONSABLE</b></p>
            <p><?= $model->responsable ?></p>
        </div>   
    </div>
        <?php 
            if ($model->observacion_adicional) {
         ?>        
        <div class="row">
          <div class="col-md-12 col-xs-6">
            <p><b>OBSERVACIONES ADICIONALES</b></p>
            <p><?= $model->observacion_adicional ?></p>
        </div>  
        </div>
        <?php 
            }
         ?>
    </br>

    <div class="row">
        <div class="col-xs-12">
            <p><b>TABLA DE CONSUMOS</b></p>
        </div>
        </br>
        <div class="col-md-10 col-md-offset-1 col-xs-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => ['class' => 'grid-blue table table-responsive', 'id' => 'grid-actividades-piezas',],
                'summary' => '',
                'pjax' => true,
                'columns' => [
                    [
                        'label' => 'Pieza consumida',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:left;'],
                        'value' => function( $model ){
                            return Pieza::findOne($model->idActividadPieza->id_pieza)->nombre;
                        }
                    ],
                    /*[
                        'label' => 'Tamaño/Modelo',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:left;'],
                        'value' => function( $model ){
                            $tamano = Pieza::findOne($model->idActividadPieza->id_pieza)->tamano;
                            if ($tamano) {
                                return $tamano;
                            } else{
                                return '-';
                            }
                        },
                    ],*/
                    [
                        'label' => 'Unidad de medición',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:left;'],
                        'value' => function( $model ){
                            $unidad = Pieza::findOne($model->idActividadPieza->id_pieza)->unidad;
                            if ($unidad) {
                                return $unidad;
                            } else{
                                return '-';
                            }
                        },
                    ],
                    [
                        'attribute' => 'cantidad',
                        'label' => 'Cantidad',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:left;'],
                    ],
                    [
                        'label' => 'Costo',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                        'value' => function( $model ){
                            return '-';
                        },
                    ],
                ],
            ]); ?>
        </div>
    </div>
    </br>
 <?php 
            if ($model->observaciones) {
         ?>        
         <div class="row">
        <div class="col-xs-12">
            <p><b>OBSERVACIONES</b></p>
            <ul><li><?= $model->observaciones ?></li></ul>
        </div>
    </div>
        <?php 
            }
         ?>
   

</div>

<style>
    .expand-ficha-preventivo{
        padding: 10px 30px 20px 30px;
        text-align: left;
    }
</style>

<script>
</script>