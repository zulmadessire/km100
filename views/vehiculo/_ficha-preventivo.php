<?php

use yii\helpers\Html;
use yii\db\Query;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use kartik\grid\GridView;
use app\models\Empresa;
use app\models\MantenimientoPreventivo;
use app\models\ProyeccionMantenimiento;
use app\models\ConsumoSearch;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use app\models\TipoMantenimiento;
use app\models\Estacion;
use app\models\Cliente;

/* @var $this yii\web\View */
/* @var $model app\models\Vehiculo */
/* @var $form yii\widgets\ActiveForm */

$query = new query();
$query->select('es.nombre, es.id, es.color')
    ->from('estado_vehiculo ev')
    ->join('INNER JOIN','estado es','es.id = ev.id_estado AND es.id_tipo_estado = 4')
    ->where('ev.id_vehiculo = '. $model->id )
    ->orderBy('ev.id desc');

$sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
$estado = Yii::$app->db->createCommand($sql)->queryOne();

/*echo "<pre>";
print_r($proyecciones);
echo "</pre>";*/

$count_proyecciones = count($proyecciones) - 1;

?>

<div class="ficha-preventivo">
    
    <div class="row">

        <div class="col-xs-12">
            <div class="content-pipeline">
                <p style="margin-top: 10px; font-size: 10pt; margin-left: 15px;">CRONOLOGÍA DE TIPOS DE MTTO. PREVENTIVOS</p>
                <div class="col-xs-10" style="margin-left: 60px;">
                    <div class="bs-wizard" style="border-bottom:0;">
                        <?php for ($i=0; $i < 6 ; $i++) : ?>
                            <div class="col-xs-2 bs-wizard-step">
                                <?php if ( $proyecciones[0]->serie == $i+1 ): ?>
                                    <div class="bs-wizard-marker active text-center">
                                        <i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>
                                    </div>
                                <?php else: ?>
                                    <div class="bs-wizard-marker text-center">
                                    </div>
                                <?php endif ?>
                                <div class="progress"><div class="progress-bar"></div></div>
                                <div class="bs-wizard-dot"></div>
                                <?php
                                    switch ($i) {
                                        case 0:
                                            echo '<div class="bs-wizard-stepnum text-center">General</div>';
                                            break;

                                        case 1:
                                            echo '<div class="bs-wizard-stepnum text-center">General</div>';
                                            break;

                                        case 2:
                                            echo '<div class="bs-wizard-stepnum text-center">Intermedio</div>';
                                            break;

                                        case 3:
                                            echo '<div class="bs-wizard-stepnum text-center">General</div>';
                                            break;

                                        case 4:
                                            echo '<div class="bs-wizard-stepnum text-center">General</div>';
                                            break;

                                        case 5:
                                            echo '<div class="bs-wizard-stepnum text-center">Detallado</div>';
                                            break;
                                         
                                        default:
                                            # code...
                                            break;
                                     } 
                                ?>
                                
                                
                                <?php if ( $proyecciones[0]->serie != $i+1 ): ?>
                                    <?php if ( $i <= $count_proyecciones ): ?>
                                        <?php for ($j=0; $j <= $count_proyecciones; $j++) : ?>
                                            <?php if ( $proyecciones[$j]->serie == $i+1 ): ?>
                                                <?php $actual = $j; break;?>
                                            <?php endif ?>
                                        <?php endfor ?>

                                        <?php if ( isset($proyecciones[$actual]) && $proyecciones[$actual]->realizado == 1){ ?>
                                            <div class="bs-wizard-info text-center"><?= number_format(floatval($proyecciones[$actual]->km_proyectado), 0, ',', '.') ?>Km</div>
                                            <div class="bs-wizard-status text-center">
                                                <i class="fa fa-check fa-2x" aria-hidden="true" style="color: #4a8621;"></i>
                                            </div>
                                        <?php } else if ( isset($proyecciones[$actual]) && $proyecciones[$actual]->realizado == 0 ){ ?>
                                            <div class="bs-wizard-info text-center"><?= number_format(floatval($proyecciones[$actual]->km_proyectado), 0, ',', '.') ?>Km</div>
                                            <div class="bs-wizard-status text-center">
                                                <i class="fa fa-times fa-2x" aria-hidden="true" style="color: #b34b4b;"></i>
                                            </div>
                                        <?php } else { ?>
                                            <div class="bs-wizard-info text-center"><?= number_format(floatval($proyecciones[$actual]->km_proyectado * ($i+1) +  5000), 0, ',', '.') ?>Km</div>
                                            <div class="bs-wizard-status text-center">
                                            </div>
                                        <?php } ?>
                                    <?php else : ?>
                                        <div class="bs-wizard-info text-center"><?= number_format(floatval($proyecciones[0]->km_proyectado + (5000 * ($i) ) )  , 0, ',', '.') ?>Km</div>
                                            <div class="bs-wizard-status text-center">
                                        </div>
                                    <?php endif ?>
                                <?php else : ?>
                                    <div class="bs-wizard-info text-center"><?= number_format(floatval($proyecciones[0]->km_proyectado), 0, ',', '.') ?>Km</div>
                                        <div class="bs-wizard-status text-center">
                                    </div>
                                <?php endif ?>
                            </div>
                        <?php endfor ?>
                    </div>
                </div>
            </div>
        </div>        
    </div>
    </br>

    <div class="row">
        <div class="col-xs-12">
            <!--<div class="arrow-before"></div><div class="arrow-after"></div>-->
            <div class="notificacion">
                <div class="col-md-2 col-xs-4 text-center bell">
                    <h5><i class="fa fa-bell fa-5x" aria-hidden="true"></i></h5>
                </div>
                <div class="col-md-10 col-xs-8 text-left info">
                    <p><b>Km Actual: </b> <?= number_format(floatval($model->km), 0, ',', '.') . ' km' ?></p>
                    <p><b>Km Próximo Mtto. Preventivo: </b><?= number_format(floatval($proyecciones[0]->km_proyectado), 0, ',', '.') . ' km' ?></p>
                    <p><b>Tipo de Próximo Mtto. Preventivo: </b><?= ucfirst(strtolower($proyecciones[0]->idProximoTipoMantenimiento->nombre)) ?></p>
                    <p><b>Estatus: </b> <span class="label label-oval" style="background-color: <?= $estado['color'] ?>; "><?= $estado['nombre'] ?></span></p>
                    <!--<p><b>Fecha proyectada: </b> 01/01/2017</p>-->
                </div>
            </div>
        </div>
    </div>
    </br>
    </br>

    <?php 
 
    
    $mantenimiento_preventivo = MantenimientoPreventivo::find()->joinWith('idProyeccionMantenimiento')->where('proyeccion_mantenimiento.id_vehiculo = '.$model->id )->orderBy('id DESC')->one();

    if (isset($mantenimiento_preventivo->observacion_adicional) && $mantenimiento_preventivo->observacion_adicional != '' ) {?>
        <div class="row">
            <div class="col-xs-12">
                <div class="notificacion">
                    <div class="col-md-2 col-xs-4 text-center bell">
                        <h5><i class="fa fa-exclamation-circle fa-5x" aria-hidden="true"></i></h5>
                    </div>
                    <div class="col-md-10 col-xs-8 text-left info">
                        <p><b>En su último mantenimiento tiene observaciones adicionales importantes </b><br><br> <?php  print_r($mantenimiento_preventivo['observacion_adicional']); ?> </p>
                        
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
            
    <br><br>

    <div class="row">
        <div class="col-xs-12">
            <p style="font-weight: bold; margin-left: 20px; font-size: 11pt;">LISTADO DE MTTOS. PREVENTIVOS</p>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right">
                <?= Html::a('Registrar Mtto. Preventivo',['/mantenimiento-preventivo/create', 'id' => $model->id ], ['class' => 'btn btn-small-gris']) ?>&nbsp;
                <?= Html::a('Exportar',['/vehiculo/exportarpreventivo', 'id' => $model->id], ['class' => 'btn btn-small-verde']) ?> 
            </div>
        </div>
        </br>
        </br>
        <div class="col-xs-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider_mp,
                'filterModel'   => $searchModel_mp,
                'tableOptions' => ['class' => 'table table-responsive', 'id' => 'grid-preventivos',],
                'summary' => '',
                'options' =>[
                    'class' => 'grid',
                ],
                'resizableColumns' => false,
                'pjax' => false,
                'columns' => [
                    [
                        'class'=>'kartik\grid\ExpandRowColumn',
                        'width'=>'50px',
                        'allowBatchToggle' => false,
                        'expandIcon' => '<i class="fa fa-caret-right" aria-hidden="true" style="color:#d9d9d9;"></i>',
                        'collapseIcon' => '<i class="fa fa-caret-down" aria-hidden="true" style="color:#d9d9d9;"></i>',
                        'value'=>function ($model, $key, $index, $column) {
                    // $mant_prev = MantenimientoPreventivo::find()->where([ 'id' => $model->id ])->orderby('id DESC')->one();
                    //     if ( !$mant_prev->observacion_adicional) {
                    //            return GridView::ROW_COLLAPSED;
                    //         } else if ( $mant_prev->observacion_adicional ){
                    //              return '<i class="fa fa-info-circle fa-1x" aria-hidden="true" style="color:#e79e2d;"></i>';
                    //         } else if ( $mant_prev->observacion_adicional ){
                    //             return GridView::ROW_COLLAPSED;
                    //         }
                                return GridView::ROW_COLLAPSED;
                             
                                
                             
                        },
                        'detail'=>function ($model, $key, $index, $column) {
                            $searchModel = new ConsumoSearch([ 'id_mantenimiento_preventivo' => $model->id ]);
                            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                            return Yii::$app->controller->renderPartial('_expand-ficha-preventivo', ['model'=>$model, 'dataProvider' => $dataProvider ]);
                        },
                        'headerOptions'=>['class'=>'kartik-sheet-style'],
                        'expandOneOnly'=>true,
                        'enableRowClick' => true,
                    ],
                
                    [
                        // 'attribute' => 'fecha_realizacion',
                        'label' => 'Fecha Mtto.',
                        'format' => 'raw',
                        
                        'enableSorting' => true,
                         'value' => function ($model) {
                                   
                                    return $model->fecha_realizacion;
                        },
                         
       
                         'filter' =>  DatePicker::widget([
                        'name' => 'MantenimientoPreventivoSearch[fechas]',
                        'type' => DatePicker::TYPE_INPUT,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'yyyy-m-dd'
                        ]
                    ]),
                    ],
                    [
                        'attribute' => 'km',
                        'label' => 'Km de Mtto.',
                        'format' => 'text',
                        'filter' => true,

                    ],
                    [
                        'attribute' => 'id_tipo_mantenimiento',
                        'label' => 'Tipo',
                        'format' => 'text',
                        'filter' => true,
                        'enableSorting' => true,
                        'value' => function( $model ){
                            if ( $model->km ) {
                                return ucfirst(strtolower($model->idTipoMantenimiento->nombre));
                            } else{
                                return '-';
                            }
                        },
                            'filter' => Select2::widget([
                                'name' => 'MantenimientoPreventivoSearch[id_tipo]',
                                  // 'data' => ArrayHelper::map(Marca::find()->joinWith('Modelo')->orderBy('nombre')->all(), 'id_marca', 'nombre'),
                                'data' => ArrayHelper::map(TipoMantenimiento::find()->orderBy('nombre')->all(), 'id_tipo_mnto', 'nombre'),
                                'hideSearch' => true,
                                'options' => ['placeholder' => 'Tipo Mnto',],
                                'pluginOptions' => [
                                      'allowClear' => true
                                  ],
                              ]),

                    ],
                    [
                        'label' => 'Cliente',
                        'format' => 'text',
                        'filter' => true,
                        'enableSorting' => true,
                        'value' => function( $model ){
                            if ( $model->id_estacion ) {
                                return $model->idEstacion->idEmpresa->cliente->nombre_comercial;
                            } else{
                                return '-';
                            }

                            
                        },
                         'filter' => Select2::widget([
                                'name' => 'MantenimientoPreventivoSearch[id_cliente]',
                                  // 'data' => ArrayHelper::map(Marca::find()->joinWith('Modelo')->orderBy('nombre')->all(), 'id_marca', 'nombre'),
                                'data' => ArrayHelper::map(Cliente::find()->orderBy('nombre_comercial')->all(), 'id', 'nombre_comercial'),
                                'hideSearch' => true,
                                'options' => ['placeholder' => 'Cliente',],
                                'pluginOptions' => [
                                      'allowClear' => true
                                  ],
                              ]),
                    ],
                    [
                        'attribute' => 'id_estacion',
                        'label' => 'Estación',
                        'format' => 'text',
                        'filter' => true,
                        'enableSorting' => true,
                        'value' => function( $model ){
                            if ( $model->id_estacion ) {
                                return $model->idEstacion->nombre;
                            } else{
                                return '-';
                            }
                        },
                           'filter' => Select2::widget([
                                'name' => 'MantenimientoPreventivoSearch[id_estacion]',
                                  // 'data' => ArrayHelper::map(Marca::find()->joinWith('Modelo')->orderBy('nombre')->all(), 'id_marca', 'nombre'),
                                'data' => ArrayHelper::map(estacion::find()->orderBy('nombre')->all(), 'id_estacion', 'nombre'),
                                'hideSearch' => true,
                                'options' => ['placeholder' => 'Estación',],
                                'pluginOptions' => [
                                      'allowClear' => true
                                  ],
                              ]),
                    ],
                ],
            ]); ?>
        </div>
    </div>

</div>

<style>
    .bs-wizard {}

    /*Form Wizard*/
    .bs-wizard {border-bottom: solid 1px #e0e0e0; padding: 0 0 10px 0;}
    .bs-wizard > .bs-wizard-step {padding: 0; position: relative;}
    /*.bs-wizard > .bs-wizard-step + .bs-wizard-step {}*/
    .bs-wizard > .bs-wizard-step .bs-wizard-stepnum {color: #000; font-size: 10pt; font-weight: bold;}
    .bs-wizard > .bs-wizard-step .bs-wizard-info {color: #000; font-size: 10pt;}
    .bs-wizard > .bs-wizard-step > .bs-wizard-dot {position: absolute; width: 30px; height: 30px; display: block; background: #767171; top: 50px; left: 50%; margin-top: -15px; margin-left: -15px; border-radius: 50%;} 
    .bs-wizard > .bs-wizard-step > .bs-wizard-dot:after {content: ' '; width: 16px; height: 16px; background: #fff; border-radius: 50px; position: absolute; top: 7px; left: 7px; } 
    .bs-wizard > .bs-wizard-step > .progress {position: relative; border-radius: 0px; height: 6px; box-shadow: none; margin: 20px 0; background: #767171;}
    /*.bs-wizard > .bs-wizard-step > .progress > .progress-bar {width:0px; box-shadow: none;}
    .bs-wizard > .bs-wizard-step.complete > .progress > .progress-bar {width:100%; background: #767171;}
    .bs-wizard > .bs-wizard-step.active > .progress > .progress-bar {width:50%;}
    .bs-wizard > .bs-wizard-step:first-child.active > .progress > .progress-bar {width:0%;}
    .bs-wizard > .bs-wizard-step:last-child.active > .progress > .progress-bar {width: 100%;}
    .bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot {background-color: #f5f5f5;}
    .bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot:after {opacity: 0;}
    .bs-wizard > .bs-wizard-step:first-child  > .progress {left: 50%; width: 50%;}
    .bs-wizard > .bs-wizard-step:last-child  > .progress {width: 50%;}
    .bs-wizard > .bs-wizard-step.disabled a.bs-wizard-dot{ pointer-events: none; }*/
    .bs-wizard > .bs-wizard-step > .bs-wizard-marker { margin-bottom: 47px;}
    .bs-wizard > .bs-wizard-step > .bs-wizard-marker.active{ color: #b34b4b; content: ' '; margin-bottom: 0px;}
    /*END Form Wizard*/

    .content-pipeline{
        border: 1px solid #dedede;
        height: 200px;
    }

    .arrow-before {
        /*width:0px;
        height:0px;
        border-left: 6px solid transparent;
        border-right: 6px solid transparent;
        border-bottom: 6px solid #dedede;
        font-size:0px;
        line-height:0px;
        top: 6px;
        left: 3px;
        position: relative;*/
        right: 100%;
    top: 50%;
    border: solid transparent;
    content: " ";
    height: 0;
    width: 0;
    position: absolute;
    pointer-events: none;
    border-color: rgba(194, 225, 245, 0);
    border-right-color: #c2e1f5;
    border-width: 36px;
    margin-top: -36px;
    }
    .arrow-after {
        /*width:0px;
        height:0px;
        border-left: 6px solid transparent;
        border-right: 6px solid transparent; 
        border-bottom: 6px solid white; 
        font-size:0px;
        line-height:0px;
        top: 1px;
        left: 3px;
        position: relative;*/
        border-color: rgba(136, 183, 213, 0);
    border-right-color: #88b7d5;
    border-width: 30px;
    margin-top: -30px;
    right: 100%;
    top: 50%;
    border: solid transparent;
    content: " ";
    height: 0;
    width: 0;
    position: absolute;
    pointer-events: none;
    }
    .notificacion {
       /*width: 80px;*/
        text-align: center;
        font-family: verdana;
        box-shadow: 3px 3px 3px #888888;
        border: 1px solid #dedede;
        height: 150px;
    }

    .notificacion > .info > p {
        margin: 0 0 3px;
    }

    .notificacion > .info {
        color:black; padding-top: 15px;
    }

    .notificacion > .bell {
        background-color: #fee599; height: 100%; border-right: 1px solid #dedede;
    }

    .notificacion > .bell > h5 {
        margin-top: 25%; font-weight: bold;
    }
</style>

<script>
    $('.table').removeClass( "table-bordered table-striped" );
</script>