<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\db\Query;
use app\models\Empresa;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Modelo;
use app\models\MantenimientoPreventivo;
use app\models\Cliente;
use app\models\Estacion;
use app\models\PipelineServicio;
use app\models\CotizacionTallerActividadSearch;
use app\models\Estado;


/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchVehiculo */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Listado de Vehículos';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vehiculo-index">
    
    <div class="row">
        <div class="col-md-6">
            <h4 class="titulo"><?= Html::encode($this->title) ?></h4>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <span>Total de Vehículos: <?= $dataProvider->getTotalCount() ?></span>
            <div class="pull-right">
                <?php if ($type != 1): ?>
 
                    <?= Html::a('Filtros',['/vehiculo/index', 'type' => 1], ['class' => 'btn btn-filtro']) ?>&nbsp;
                    
                      <?php if (!$searchModel['id_marca']) { 
                        $searchModel['id_marca'] = 0;
                     } ?>
                       <?php if (!$searchModel['mva']) { 
                        $searchModel['mva'] = 0;
                     } ?>
                       <?php if (!$searchModel['id_modelo']) { 
                        $searchModel['id_modelo'] = 0;
                     } ?>
                       <?php if (!$searchModel['id_cliente']) { 
                        $searchModel['id_cliente'] = 0;
                     } ?>
                       <?php if (!$searchModel['id_empresa']) { 
                        $searchModel['id_empresa'] = 0;
                     } ?>
                       <?php if (!$searchModel['id_estacion']) { 
                        $searchModel['id_estacion'] = 0;
                     } ?>
                       <?php if (!$searchModel['id_estado']) { 
                        $searchModel['id_estado'] = 0;
                     } ?>
                       <?php if (!$searchModel['id_concesionario']) { 
                        $searchModel['id_concesionario'] = 0;
                     } ?>

                    <?= Html::a('Descargar',['/vehiculo/excelvehiculos',
                     
                                            'id_marca'=>$searchModel['id_marca'],
                      
                                            'id_modelo'=>$searchModel['id_modelo'],
                                            'mva'=>$searchModel['mva'],
                                            'id_cliente'=>$searchModel['id_cliente'],
                                            'id_empresa'=>$searchModel['id_empresa'],
                                            'id_estacion'=>$searchModel['id_estacion'],
                                            'id_estado'=>$searchModel['id_estado'],
                                            'id_concesionario'=>$searchModel['id_concesionario'],
                                            ],
                                             ['class' => 'btn btn-small-verde']) ?> 
                <?php else: ?>
                    <?= Html::a('Listado de vehiculos',['/vehiculo/index'], ['class' => 'btn btn-small-gris']) ?>
                <?php endif ?>
            </div>
        </div>
        </br>
        </br>
        <div class="col-md-12 col-xs-12">

            <?php if ( $type == 0 ) { ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    // 'filterModel'   => $searchModel,
                    'tableOptions' => ['class' => 'table'],
                    'summary' => '',
                    'options' =>[
                        'class' => 'grid table-responsive',
                    ],
                    'columns' => [
                         [
                       
                            'label' => '',
                            'format' => 'raw',
                            'enableSorting' => true,
                            'value' => function( $model ){
                                $mantenimiento_preventivo = MantenimientoPreventivo::find()->joinWith('idProyeccionMantenimiento')->where('proyeccion_mantenimiento.id_vehiculo = '.$model->id )->orderBy('id DESC')->one();
                                if ($mantenimiento_preventivo) {
                                    if ($mantenimiento_preventivo->observacion_adicional && $mantenimiento_preventivo->observacion_adicional != '') {
                                        return  Html::a('<span class="fa-stack fa-lg">
                                            <i class="fa fa-circle fa-stack-2x" style="color:#e79e2d;"></i>
                                            <i class="fa fa-exclamation fa-stack-1x fa-inverse"></i>
                                          </span>',
                                          ['/vehiculo/ficha', 'id' => $model->id, 'type' => 3 ], ['title' => 'Tiene observaciones adicionales pendientes']);
                                    }else{
                                        return '';
                                    }
                                }
                                else{
                                        return '';
                                }
                             
                            },
                    
                        ],
                        [
                            'attribute' => 'km',
                            'label' => 'Mantenimiento',
                            'format' => 'html',
                            'filter' => true,
                            'enableSorting' => true,
                            'value' => function( $model ){
                                $query = new query();
                                $query->select('es.color')
                                    ->from('estado_vehiculo ev')
                                    ->join('INNER JOIN','estado es','es.id = ev.id_estado AND es.id_tipo_estado = 4')
                                    ->where('ev.id_vehiculo = '. $model->id )
                                    ->orderBy('ev.id desc');

                                $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                                $estado = Yii::$app->db->createCommand($sql)->queryOne();

                                return '<span style="color:'.$estado['color'].';"><i class="fa fa-circle fa" aria-hidden="true"></i> &nbsp;' . number_format(floatval($model->km), 0, ',', '.'). ' Km</span>';
                            }
                        ],
                        [
                       
                            'label' => 'Modelo',
                            'format' => 'text',
                            'enableSorting' => true,
                            'value' => function( $model ){
                                return $model->idModelo->nombre;
                            },
                            'filter' => Select2::widget([
                                'name' => 'VehiculoSearch[id_modelo]',
                                  // 'data' => ArrayHelper::map(Marca::find()->joinWith('Modelo')->orderBy('nombre')->all(), 'id_marca', 'nombre'),
                                'data' => ArrayHelper::map(Modelo::find()->orderBy('nombre')->all(), 'id_modelo', 'nombre'),
                                'hideSearch' => true,
                                'options' => ['placeholder' => 'Modelo',],
                                'pluginOptions' => [
                                      'allowClear' => true
                                  ],
                              ]),
                        ],
                        [
                            'attribute' => 'mva',
                            'label' => 'MVA/Ficha',
                            'format' => 'text',
                            'filter' => true,
                            'enableSorting' => true,
                        ],
                        [
         
                            'label' => 'Propietario',
                            'format' => 'text',
                            'enableSorting' => true,
                            'value' => function( $model ){
                                if ($model->id_propietario) {
                                    return Empresa::findOne($model->id_propietario)->nombre;
                                } else{
                                    return '-';
                                }
                            },
                             'filter' => Select2::widget([
                                'name' => 'VehiculoSearch[empresa]',
                                  // 'data' => ArrayHelper::map(Marca::find()->joinWith('Modelo')->orderBy('nombre')->all(), 'id_marca', 'nombre'),
                                'data' => ArrayHelper::map(Empresa::find()->orderBy('nombre')->all(), 'id_empresa', 'nombre'),
                                'hideSearch' => true,
                                'options' => ['placeholder' => 'Propietario',],
                                'pluginOptions' => [
                                      'allowClear' => true
                                  ],
                              ]),
                        ],
                        [
                            'label' => 'Cliente',
                            'format' => 'text',
                            'enableSorting' => true,
                            'value' => function( $model ){
                                $query = new query();
                                $query->select('cli.nombre_comercial')
                                    ->from('flota f')
                                    ->join('INNER JOIN','estacion es','es.id_estacion = f.id_estacion')
                                    ->join('INNER JOIN','empresa em','es.id_empresa = em.id_empresa')
                                    ->join('INNER JOIN','cliente cli','em.cliente_id = cli.id')
                                    ->where('f.id_vehiculo = '. $model->id )
                                    ->orderBy('f.id desc');

                                $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                                $agencia = Yii::$app->db->createCommand($sql)->queryOne();

                                return $agencia['nombre_comercial'];
                            },
                            'filter' => Select2::widget([
                                'name' => 'VehiculoSearch[cliente]',
                                  // 'data' => ArrayHelper::map(Marca::find()->joinWith('Modelo')->orderBy('nombre')->all(), 'id_marca', 'nombre'),
                                'data' => ArrayHelper::map(Cliente::find()->orderBy('nombre_comercial')->all(), 'id', 'nombre_comercial'),
                                'hideSearch' => true,
                                'options' => ['placeholder' => 'Cliente',],
                                'pluginOptions' => [
                                      'allowClear' => true
                                  ],
                              ]),
                        ],
                        [
                            'label' => 'Estación',
                            'format' => 'text',
                            'enableSorting' => true,
                            'value' => function( $model ){
                                $query = new query();
                                $query->select('em.nombre nombre_empresa, es.nombre nombre_estacion')
                                    ->from('flota f')
                                    ->join('INNER JOIN','estacion es','es.id_estacion = f.id_estacion')
                                    ->join('INNER JOIN','empresa em','es.id_empresa = em.id_empresa')
                                    ->where('f.id_vehiculo = '. $model->id )
                                    ->orderBy('f.id desc');

                                $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                                $agencia = Yii::$app->db->createCommand($sql)->queryOne();

                                return $agencia['nombre_empresa']. ' - ' . $agencia['nombre_estacion'];
                            },
                            'filter' => Select2::widget([
                                'name' => 'VehiculoSearch[id_estacion]',
                                  // 'data' => ArrayHelper::map(Marca::find()->joinWith('Modelo')->orderBy('nombre')->all(), 'id_marca', 'nombre'),
                                'data' => ArrayHelper::map(Estacion::find()->orderBy('nombre')->distinct()->all(), 'id_estacion', 'nombre'),
                                'hideSearch' => true,
                                'options' => ['placeholder' => 'Estación',],
                                'pluginOptions' => [
                                      'allowClear' => true
                                  ],
                              ]),
                        ],
                        [
                            'attribute' => 'wizard',
                            'label' => 'TSD/Wizard',
                            'format' => 'html',
                            'filter' => true,
                            'enableSorting' => true,
                            'value' => function($model){
                                if ($model->wizard == 1) {
                                    return '<span><i class="fa fa-check-circle fa-lg" aria-hidden="true" style="color: #4b8723; background-color: #fff;"></i></span>';
                                } else{
                                    return '<span><i class="fa fa-times-circle fa-lg" aria-hidden="true" style="color: #d80027; background-color: #fff;"></i></span>';
                                }
                            }
                        ],
                        [
                            'attribute' => 'exactus',
                            'label' => 'Exactus',
                            'format' => 'html',
                            'filter' => true,
                            'enableSorting' => true,
                            'value' => function($model){
                                if ($model->exactus == 1) {
                                    return '<span><i class="fa fa-check-circle fa-lg" aria-hidden="true" style="color: #4b8723; background-color: #fff;"></i></span>';
                                } else{
                                    return '<span><i class="fa fa-times-circle fa-lg" aria-hidden="true" style="color: #d80027; background-color: #fff;"></i></span>';
                                }
                            }
                        ],
                        [
                            'label' => 'Estatus',
                            'format' => 'html',
                            'filter' => false,
                            'enableSorting' => false,
                            'value' => function( $model ){
                                $query = new query();
                                $query->select('es.nombre, es.color')
                                    ->from('estado_vehiculo ev')
                                    ->join('INNER JOIN','estado es','es.id = ev.id_estado AND es.id_tipo_estado = 1')
                                    ->where('ev.id_vehiculo = '. $model->id )
                                    ->orderBy('ev.id desc');

                                $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                                $estado = Yii::$app->db->createCommand($sql)->queryOne();

                                return '<span class="label label-oval" style="background-color:'.$estado['color'].'; ">'.$estado['nombre'].'</span>';
                            },
                                'filter' => Select2::widget([
                                'name' => 'VehiculoSearch[id_estado]',
                                  // 'data' => ArrayHelper::map(Marca::find()->joinWith('Modelo')->orderBy('nombre')->all(), 'id_marca', 'nombre'),
                                'data' => ArrayHelper::map(Estado::find()->orderBy('nombre')->all(), 'id', 'nombre'),
                                'hideSearch' => true,
                                'options' => ['placeholder' => 'Estado',],
                                'pluginOptions' => [
                                      'allowClear' => true
                                  ],
                              ]),
                        ],
                        [  
                            'class' => 'yii\grid\ActionColumn',
                            'header'=>'Acciones',
                            'template' => '{menu}',
                            'buttons' => [

                                //menu button
                                'menu' => function ($url, $model) {

                                     return  '<div class="dropdown">
                                              <button class="btn btn-default dropdown-toggle btn-sm" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i>
                                              <span class="caret"></span></button>
                                              <ul class="dropdown-menu">
                                                <li>'.Html::a('<span><i class="fa fa-eye fa-sm" aria-hidden="true"></i></span> Ver ficha',[ '/vehiculo/ficha', 'id' => $model->id ]).' </li>
                                                <li>'.Html::a('<span><i class="fa fa-file-archive-o fa-sm" aria-hidden="true"></i></span> Registrar Mtto. Preventivo',['/mantenimiento-preventivo/create', 'id' => $model->id ]).' </li>
                                                <li>'.Html::a('<span><i class="fa fa-clock-o fa-sm" aria-hidden="true"></i></span> Historial Mtto. Correctivo',['/vehiculo/ficha', 'id' => $model->id, 'type' => 4 ]).' </li>
                                              </ul>
                                            </div>';
                                },
                            ],

                        ],
                    ],
                ]); ?>
            <?php } else{ ?>
                <?= $this->render('_search', [
                    'model' => $model_search,
                ]) ?>
            <?php }?>
            
        </div>
    </div>
</div>
