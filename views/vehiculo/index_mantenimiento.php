<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\db\Query;
use app\models\Empresa;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Modelo;
use app\models\Cliente;
use app\models\Estacion;
use app\models\PipelineServicio;
use app\models\CotizacionTallerActividadSearch;
use app\models\Estado;
use app\models\Marca;
use app\models\Vehiculo;
use app\models\ProyeccionMantenimiento;
use app\models\MantenimientoPreventivo;


/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchVehiculo */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Listado de Vehículos en Mantenimiento';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vehiculo-index">
    
    <div class="row">
        <div class="col-md-6">
            <h4 class="titulo"><?= Html::encode($this->title) ?></h4>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <span>Total de Vehículos: <?= $dataProvider->getTotalCount() ?></span>
            <div class="pull-right">
                <?php if ($type != 1): ?>
                    <?= Html::a('Filtros',['/vehiculo/indexmantenimiento', 'type' => 1], ['class' => 'btn btn-filtro']) ?>&nbsp;
                    <?= Html::a('Descargar',['/vehiculo/excelvehiculos'], ['class' => 'btn btn-small-verde']) ?> 
                <?php else: ?>
                    <?= Html::a('Listado de vehiculos',['/vehiculo/indexmantenimiento'], ['class' => 'btn btn-small-gris']) ?>
                <?php endif ?>
            </div>
        </div>
        </br>
        </br>
        <div class="col-md-12 col-xs-12">

            <?php if ( $type == 0 ) { ?>
             <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    // 'filterModel'   => $searchModel,
                    'tableOptions' => ['class' => 'table'],
                    'summary' => '',
                    'options' =>[
                        'class' => 'grid table-responsive',
                    ],
                    'columns' => [
                    [
                       
                            'label' => 'Marca',
                            'format' => 'text',
                            'enableSorting' => true,
                            'value' => function( $model ){
                                return $model->idModelo->idMarca->nombre;
                            },
    
                        ],
                             [
                       
                            'label' => 'Modelo',
                            'format' => 'text',
                            'enableSorting' => true,
                            'value' => function( $model ){
                                return $model->idModelo->nombre;
                            },
 
                        ],
                        [
 
                            'label' => 'MVA',
                            'format' => 'text',
                            'value' => function( $model ){
                                return $model->mva;
                            },
                        ],
                        [
                            'label' => 'Estación',
                            'format' => 'text',
                            'enableSorting' => true,
                            'value' => function( $model ){
                                $query = new query();
                                $query->select('em.nombre nombre_empresa, es.nombre nombre_estacion')
                                    ->from('flota f')
                                    ->join('INNER JOIN','estacion es','es.id_estacion = f.id_estacion')
                                    ->join('INNER JOIN','empresa em','es.id_empresa = em.id_empresa')
                                    ->where('f.id_vehiculo = '. $model->id )
                                    ->orderBy('f.id desc');

                                $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                                $agencia = Yii::$app->db->createCommand($sql)->queryOne();

                                return $agencia['nombre_empresa']. ' - ' . $agencia['nombre_estacion'];
                            },
                            'filter' => Select2::widget([
                                'name' => 'VehiculoSearch[id_estacion]',
                                  // 'data' => ArrayHelper::map(Marca::find()->joinWith('Modelo')->orderBy('nombre')->all(), 'id_marca', 'nombre'),
                                'data' => ArrayHelper::map(Estacion::find()->orderBy('nombre')->distinct()->all(), 'id_estacion', 'nombre'),
                                'hideSearch' => true,
                                'options' => ['placeholder' => 'Estación',],
                                'pluginOptions' => [
                                      'allowClear' => true
                                  ],
                              ]),
                        ],

                        [
                            'label' => 'Km Actual',
                            'format' => 'text',
                            'enableSorting' => true,
                            'value' => function( $model ){
                               return  number_format(floatval($model->km), 0, ',', '.').' Km';
                            },
                             
                        ],
                        [
                            'label' => 'Km Proyectado',
                            'format' => 'text',
                            'enableSorting' => true,

                            'value' => function( $model ){
                                if (isset($model)) {
                                    $km_proyectado = ProyeccionMantenimiento::find()->where(['id_vehiculo'=>$model->id])->orderBy('id DESC')->one()->km_proyectado;
                                    return  number_format(floatval($km_proyectado), 0, ',', '.').' Km';
                                }
                            },
    
                        ],
                        [
                            'label' => 'Diferencia',
                            'format' => 'text',
                            'enableSorting' => true,
                            'value' => function( $model ){
                                if (isset($model)) {
                                    $km_proyectado = ProyeccionMantenimiento::find()->where(['id_vehiculo'=>$model->id])->orderBy('id DESC')->one()->km_proyectado;
                                    return  number_format(floatval( ($km_proyectado)-($model->km)), 0, ',', '.').' Km';
                                }
                            },
 
                        ],
                        [
                            'label' => 'Tipo Mantenimiento',
                            'format' => 'text',
                            'enableSorting' => true,

                            'value' => function( $model ){
                                if (isset($model)) {
                                    $proyectado = ProyeccionMantenimiento::find()->where(['id_vehiculo'=>$model->id])->orderBy('id DESC')->one()->idProximoTipoMantenimiento->nombre;
                                    return  $proyectado;
                                }
                            },
    
                        ],
                        [
                            'attribute' => 'estatusMantenimiento',
                            'label' => 'Estatus',
                            'format' => 'html',
                            /*'value' => function( $model ){
                                $query = new query();
                                $query->select('es.nombre, es.color')
                                    ->from('vehiculo ve')
                                    ->join('INNER JOIN','estado_vehiculo ev','ev.id_vehiculo = ve.id')
                                    ->join('INNER JOIN','estado es','es.id = ev.id_estado')
                                    ->where('ve.id = '. $model->id )
                                    ->andWhere('es.id = 8')
                                    ->orWhere('es.id = 9')
                                    ->orWhere('es.id = 10');

                                $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                                $estado = Yii::$app->db->createCommand($sql)->queryOne();

                                return '<span class="label label-oval" style="background-color:'.$estado['color'].'; ">'.$estado['nombre'].'</span>';
                            },*/
                              
                             
                        ],
                       
                    ],
                ]); ?>
            <?php } else{ ?>
                <?= $this->render('_search2', [
                    'model' => $model_search,
                ]) ?>
            <?php }?>
            
        </div>
    </div>
</div>
