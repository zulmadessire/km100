<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use app\models\Modelo;
use app\models\Marca;
use app\models\Estacion;
use app\models\Estado;
use app\models\Empresa;
use app\models\Cliente;
use app\models\Concesionario;

/* @var $this yii\web\View */
/* @var $model app\models\SearchVehiculo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vehiculo-search">

    <div class="row">
        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>
        <div class="col-xs-12">
            <p class="titulo-form">FILTROS DE BÚSQUEDA</p>
            <hr>
        </div>
        <div class="col-xs-12 col-md-12">
            <div class="pull-right">
                <?= Html::a('Limpiar filtros', '#', ['id' => 'reset', 'class' => 'reset-filtros']) ?>
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <label class="control-label" for="id_marca">MARCA</label>
                <?=  Select2::widget([
                        'name' => 'VehiculoSearch[id_marca]',
                        'data' => ArrayHelper::map(Marca::find()->orderBy('nombre')->all(), 'id_marca', 'nombre'),
                        'options' => ['placeholder' => 'Selecciona la marca', 'id' => 'id_marca']
                    ]); 
                ?>
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <?=
                $form->field($model, 'id_modelo')->widget(DepDrop::classname(), [
                    'type'=>DepDrop::TYPE_SELECT2,
                    'options'=>['id'=>'id_modelo'],
                    'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                    'pluginOptions'=>[
                        'depends'=>['id_marca'],
                        'placeholder'=>'Selecciona un modelo',
                        'url'=>Url::to(['/vehiculo/getmodelo'])
                    ]
                ])->label('MODELO');
            ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'mva') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <label class="control-label" for="id_cliente">CLIENTE</label>
                <?=  Select2::widget([
                        'name' => 'VehiculoSearch[id_cliente]',
                        'data' => ArrayHelper::map(Cliente::find()->where('id != 6')->orderBy('nombre_comercial')->all(), 'id', 'nombre_comercial'),
                        'options' => ['placeholder' => 'Selecciona el cliente', 'id' => 'id_cliente']
                    ]); 
                ?>
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <label class="control-label" for="id_empresa">EMPRESA</label>
            <?=
                DepDrop::widget([
                    'name' => 'VehiculoSearch[id_empresa]',
                    'type'=>DepDrop::TYPE_SELECT2,
                    'options'=>['id'=>'id_empresa', 'class' => 'form-control'],
                    'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                    'pluginOptions'=>[
                        'depends'=>['id_cliente'],
                        'placeholder'=>'Selecciona la empresa',
                        'url'=>Url::to(['/vehiculo/getempresas'])
                    ]
                ]);
            ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <label class="control-label" for="id_estado">ESTACIÓN</label>
                <?=
                    DepDrop::widget([
                        'name' => 'VehiculoSearch[id_estacion]',
                        'type'=>DepDrop::TYPE_SELECT2,
                        'options'=>['id'=>'id_estacion'],
                        'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                        'pluginOptions'=>[
                            'depends'=>['id_empresa'],
                            'placeholder'=>'Selecciona la estación',
                            'url'=>Url::to(['/vehiculo/getestaciones'])
                        ]
                    ]);
                ?>
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <label class="control-label" for="id_estado">ESTATUS</label>
                <?= 
                    Select2::widget([
                        'name' => 'VehiculoSearch[id_estado]',
                        'data' => ArrayHelper::map(Estado::find()->where(['id_tipo_estado' => '1'])->orderBy('nombre')->all(), 'id', 'nombre'),
                        'options' => ['placeholder' => 'Selecciona el estatus',]
                    ]);
                ?>
            </div>
        </div>

        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <label class="control-label" for="id_estado">CONCESIONARIO</label>
                <?= 
                    Select2::widget([
                        'name' => 'VehiculoSearch[id_concesionario]',
                        'data' => ArrayHelper::map(Concesionario::find()->orderBy('nombre')->all(), 'id', 'nombre'),
                        'options' => ['placeholder' => 'Selecciona el concesionario',]
                    ]);
                ?>
            </div>
        </div>

      
        <div class="col-md-12 col-xs-12">
            <div class="form-group text-center">
                <?= Html::submitButton('BUSCAR', ['class' => 'btn btn-submit']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

</div>

<script>
    $("a#reset").on("click", function(event){
        event.preventDefault();
        var myForm = $(this).closest('form').get(0);
        myForm.reset();
        $("select", myForm).each(
            function () {
                $(this).select2('val','All');
            }
        );
        $("input", myForm).each(
            function () {
                $(this).val('');
            }
        );
    });
</script>
