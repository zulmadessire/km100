<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CobroCaso */

$this->title = 'REGISTRO DE COBROS';

?>
<div class="cobro-caso-create">

    <div class="row">
		<div class="col-md-1 col-xs-3">
			<h4 class="titulo"><img src="<?= Yii::$app->request->baseUrl ?>/img/give_money_64.png" alt=""></h4>
		</div>
		<div class="col-md-10 col-xs-9" style="padding-top: 30px;">
			<h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
		</div>
	</div>
	</br>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
