<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\form\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CobroCasoSerach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'REPORTE DE COBROS';
?>
<div class="cobro-caso-index">
    
    <div class="row">
        <div class="col-md-1 col-xs-3">
            <h4 class="titulo"><img src="<?= Yii::$app->request->baseUrl ?>/img/excel_64.png" alt=""></h4>
        </div>
        <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
        </div>
    </div>
    </br>
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <?php $form = ActiveForm::begin(); ?>
                <label class="control-label">MES</label>
                <?= Select2::widget([
                    'name' => 'CobroCasoSerach[MES]',
                    'data' => ['1' => 'Enero', '2' => 'Febrero', '3' => 'Marzo', '4' => 'Abril', '5' => 'Mayo', '6' => 'Junio', '7' => 'Julio', '8' => 'Agosto', '9' => 'Septiembre', '10' => 'Octubre', '11' => 'Noviembre', '12' => 'Diciembre' ],
                    'options' => [
                        'placeholder' => 'Selecciona el mes',
                        'allowClear' => true,
                        'id' => 'mes',
                    ],
                ]); ?>
            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-md-8 col-xs-12">
            <div class="pull-right" style="margin-top: 30px;">
                <?= Html::a('Exportar',['#'], ['class' => 'btn btn-small-verde']) ?> 
            </div>
        </div>
        <div class="clearfix"></div>
        </br>
        <div class="col-xs-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'summary' => '',
                'options' =>[
                    'class' => 'grid-blue',
                ],
                'showPageSummary' => true,
                'columns' => [
                    [
                        'attribute' => 'fecha',
                        'label' => 'Fecha',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                        'pageSummary' => false,
                        'value' => function( $model ){
                            if ($model->fecha) {
                                return date( 'd/m/Y', strtotime($model->fecha) );
                            } else{
                                return '-';
                            }
                        }
                    ],
                    [
                        'attribute' => 'id_caso',
                        'label' => 'Caso',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                        'pageSummary' => 'Total',
                        'headerOptions' => ['width' => '300',],
                    ],
                    [
                        'attribute' => 'monto',
                        'label' => 'Cobrado RD$',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                        'pageSummary' => true,
                        'footer'=>true,
                        'format'=>['decimal', 2],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
<script>
    $('#mes').on('change', function(event) {
        event.preventDefault();
        /* Act on the event */
        console.log('cambio');
    });
</script>
