<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use app\models\Caso;
//use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\CobroCaso */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cobro-caso-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">INFORMACIÓN DE COBRO</p>
            <hr>
        </div>
    </div>

    <div class="row">
    	<div class="col-md-4 col-xs-12">
    		<?=
                $form->field($model, 'id_caso')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Caso::find()
                    ->select('`caso`.`id_caso`')
                    ->leftJoin('cobro_caso', '`cobro_caso`.`id_caso` = `caso`.`id_caso`')
                    ->where(['cobro_caso.id_caso' => NULL])
                    ->all(), 'id_caso', 'id_caso'),
                    'options' => ['placeholder' => 'Selecciona el caso',],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('CASO ASOCIADO');
            ?>
    	</div>
    	<div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'monto', ['addon' => ['prepend' => ['content'=>'<i class="fa fa-usd" aria-hidden="true"></i>']] ] )->textInput()->label('MONTO COBRADO') ?>
    	</div>
    	<div class="col-md-4 col-xs-12">
    		<?=
                $form->field($model, 'forma_pago')->widget(Select2::classname(), [
                    'data' => ['0' => 'Efectivo', '1' => 'Tarjeta de crédito', '2' => 'Crédito', '3' => 'CLAIM', '4' => 'Compañía privada de cobranza' ],
                    'options' => ['placeholder' => 'Selecciona la forma de pago'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('FORMA DE PAGO');
            ?>
    	</div>
    	<div class="col-md-4 col-xs-12">
    		<?=
                $form->field($model, 'motivo_cobro')->widget(Select2::classname(), [
                    'data' => ['0' => 'No presentó acta policial', '1' => 'Declinación de cobertura', '2' => 'Deducible', '3' => 'Daños con objetos fijos', '4' => 'Deslizamiento', '5' => 'Negligencia', '6' => 'Daños maliciosos', '7' => 'Imprudencia', '8' => 'Motín', '9' => 'Actividades políticas', '10' => 'Manejo en terrenos rocosos', '11' => 'Exceso de velocidad', '12' => 'Vandalismo', '13' => 'No fue cargado por daños' ],
                    'options' => ['placeholder' => 'Selecciona el motivo'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('MOTIVO DE COBRO');
            ?>
    	</div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'numero_factura')->textInput()->label('NÚMERO DE FACTURA ASOCIADA') ?>
        </div>
    	<div class="col-md-4 col-xs-12">
            <?php //$form->field($model, 'fecha', ['addon' => ['prepend' => ['content'=>'<i class="fa fa-calendar" aria-hidden="true"></i>']] ] )->textInput()->label('FECHA DE COBRO') ?>
    		<?=
                $form->field($model, 'fecha')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'Selecciona la fecha'],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd',
                        'orientation' => 'bottom left',
                    ]
                ]);
            ?>
    	</div>
    </div>
	<br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>

    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="form-group text-center">
                <?= Html::submitButton($model->isNewRecord ? 'ENVIAR' : 'ACTUALIZAR', ['class' => $model->isNewRecord ? 'btn btn-submit' : 'btn btn-primary']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
