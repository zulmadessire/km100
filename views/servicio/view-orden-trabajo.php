<?php 
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\grid\GridView;
use app\models\CotizacionTallerActividad;
use app\models\Empresa;

$this->title = 'ORDEN DE TRABAJO';

if ( $model_cotizacion_taller->tipo_pago == 0 ) {
	$tipo_pago = 'Pronto Pago';
} elseif ( $model_cotizacion_taller->tipo_pago == 1 ) {
	$tipo_pago = 'Crédito';
} elseif ( $model_cotizacion_taller->tipo_pago == 2 ) {
	$tipo_pago = 'Contado';
}
?>
<div class="analisis-viabilidad">

    <div class="row">
        <div class="col-md-1 col-xs-3">
        	<h4 class="titulo">
        		<i class="fa fa-bar-chart fa-4x" aria-hidden="true"></i>
        	</h4>
        </div>
        <div class="col-md-10 col-xs-9" style="padding-top: 20px;">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
        </div>
    </div>
    </br>
    </br>

    <?php $form = ActiveForm::begin([ 'id' => 'orden-trabajo-form', 'action' => ['/servicio/guardarordentrabajo', 'id' => $model_orden_trabajo->id ] ]); ?>

    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right">
                <?= Html::a('Listado de órdenes de trabajo',['/servicio/indexordentrabajo' ], ['class' => 'btn btn-small-gris']) ?> 
            </div>
        </div>
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DEL VEHÍCULO</p>
            <hr>
        </div>
    </div>
    <div class="row">
    	<div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_modelo')->textInput(['disabled' => true, 'value' => $model_vehiculo->idModelo->idMarca->nombre ])->label('Marca') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_modelo')->textInput(['value' => strtoupper($model_vehiculo->idModelo->nombre), 'disabled' => true])->label('Modelo') ?>
        </div>

    	<div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'mva')->textInput(['maxlength' => true, 'disabled' => true])->label('Ficha o MVA') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'chasis')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'placa')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>
    </div>

    <br>
    <br>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DE LA ORDEN DE TRABAJO</p>
            <hr>
        </div>
    </div>

    <div class="row">

    	<div class="col-md-4 col-xs-12">
            <?= $form->field($model_orden_trabajo, 'id')->textInput(['maxlength' => true, 'disabled' => true,])->label('NÚMERO DE ORDEN') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_orden_trabajo, 'fecha', ['addon' => ['prepend' => ['content'=>'<i class="fa fa-calendar" aria-hidden="true"></i>']] ] )->textInput(['disabled' => true, 'value' => date('d/m/Y', strtotime($model_orden_trabajo->fecha)) ])->label('FECHA') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_cotizacion_taller, 'tipo_pago')->textInput(['maxlength' => true, 'disabled' => true, 'value' => $tipo_pago,])->label('FORMA DE PAGO') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'id')->textInput(['maxlength' => true, 'disabled' => true,])->label('NÚMERO DE SOLICITUD') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <?= Html::label('CLIENTE', '', ['class' => 'control-label']) ?>
                <?= Html::textInput('', Empresa::find()->where(['id_empresa' => $model->idEstacionActual->id_empresa ])->one()->nombre,['class' => 'form-control', 'disabled' => true,]) ?>
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'id_estacion_actual')->textInput(['maxlength' => true, 'disabled' => true, 'value' => $model->idEstacionActual->nombre])->label('ESTACIÓN') ?>
        </div>

        <div class="col-md-8 col-xs-12">
            <div class="form-group">
                <?= Html::label('TALLER', '', ['class' => 'control-label']) ?>
                <?= Html::textInput('', $model_cotizacion_taller->idSolicitudServicioTaller->idTaller->nombre,['class' => 'form-control', 'disabled' => true,]) ?>
            </div>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'linea')->textInput(['maxlength' => true, 'disabled' => true, 'value' => ($model->linea == 0)?'Quick Line':'Normal Line',])->label('LÍNEA') ?>
        </div>


    </div>
    </br>
    </br>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DETALLE DE SERVICIO</p>
            <hr>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider_cotizacion_actividad,
                'summary' => '',
                'id' =>'cotizacion-actividad-grid',
                'pjax' => true,
                'showFooter' => true,
                'tableOptions' => ['class' => 'table table-responsive', 'id' =>'cotizacion-actividad-table'],
                'options' =>[
                    'class' => 'grid-blue',
                ],
                'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                'afterFooter' => '<tr class="kv-table-footer"><td></td><td></td><td></td><td></td><td>ITBIS</td><td style="text-align: center;">'.number_format(floatval( CotizacionTallerActividad::getItbis2($dataProvider_cotizacion_actividad->models)), 2, ',', '.') . ' RD$'.'</td></tr><tr class="kv-table-footer"><td></td><td></td><td></td><td></td><td>TOTAL</td><td style="text-align: center;">'.number_format(floatval( CotizacionTallerActividad::getTotal2($dataProvider_cotizacion_actividad->models)), 2, ',', '.') . ' RD$'.'</td></tr>',
                'columns' => [
                   [
                         
                        'label' => 'Servicio',
                        'format' => 'text',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:left;'],
                        //'headerOptions' => ['style' => 'width:80%'],
                        'footer' => '',
                        'value' => function($model){
                            if ($model->id_pieza_servicio_taller) {
                                return $model->piezaServicioTaller->servicioTaller->nombre;
                            }
                            else{
                                return '-';
                            }
                        },
                    ],
                    [
                         
                        'label' => 'Descripción de la Actividad',
                        'format' => 'text',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:left;'],
                        //'headerOptions' => ['style' => 'width:80%'],
                        'footer' => '',
                        'value' => function($model){
                            if ($model->id_pieza_servicio_taller) {
                                return $model->piezaServicioTaller->nombre;
                            }
                            else{
                                return '-';
                            }
                        },
                    ],
                    [
                        'label' => 'Labor',
                        'format' => 'text',
                        'enableSorting' => false,
                        'footer' => '',
                        'value' => function($model){
                            if ($model->labor == 0) {
                                return 'Reparación';
                            } elseif ( $model->labor == 1 ){
                                return 'Salvado Leve';
                            } elseif ( $model->labor == 2 ){
                                return 'Salvado Fuerte';
                            } else{
                                return '-';
                            }
                        },
                    ],
                    [
                        'attribute' => 'precio',
                        'label' => 'Precio',
                        'format' => 'text',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'value' => function($model){
                            if ($model->precio) {
                                return number_format(floatval($model->precio), 2, ',', '.') . ' RD$';
                            } else{
                                return '-';
                            }
                        },
                        'footer' => '',
                    ],
                    [
                        'label' => 'Decimal',
                        'format' => 'text',
                        'enableSorting' => false,
                        'footer' => 'SUBTOTAL',
                        'value' => function($model){
                            if ($model->decimal) {
                                return $model->decimal;
                            } else{
                                return '-';
                            }
                        },
                    ],
                    [
                        'label' => 'Total',
                        'format' => 'text',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'value' => function($model){
                            if ($model->total_servicio ) {
                                return number_format(floatval(  $model->total_servicio ), 2, ',', '.') . ' RD$';
                            } else{
                                return '0';
                            }
                        },
                        'footer' => number_format(floatval( CotizacionTallerActividad::getSubTotal2($dataProvider_cotizacion_actividad->models)), 2, ',', '.') . ' RD$',
                    ],
                ],
            ]); ?>
 
        </div>

    </div>
    </br>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">PIEZAS SOLICITADAS</p>
            <hr>
        </div>
    </div>
    <div class="row">
        <?php if ($dataProvider_cotizacion_pieza->getTotalCount() > 0): ?>
            <div class="col-md-12 col-xs-12">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider_cotizacion_pieza,
                    'summary' => '',
                    'id' =>'cotizacion-pieza-grid',
                    'pjax' => true,
                    'tableOptions' => ['class' => 'table table-responsive', 'id' =>'cotizacion-pieza-table'],
                    'options' =>[
                        'class' => 'grid-blue',
                    ],
                    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                    'columns' => [
                        [
                            'label' => 'Tipo',
                            'format' => 'text',
                            'enableSorting' => false,
                            'contentOptions'=>['style'=>'text-align:center;'],
                            'value' => function($model){
                                if ($model->id_pieza) {
                                    return $model->idPieza->tipoPieza->nombre;
                                } else{
                                    return '-';
                                }
                            },
                        ],
                        [
                            'attribute' => 'idPieza.nombre',
                            'label' => 'Descripcion',
                            'format' => 'text',
                            'enableSorting' => false,
                            'contentOptions'=>['style'=>'text-align:center;'],
                        ],
                        [
                            'attribute' => 'cantidad',
                            'label' => 'Cantidad',
                            'format' => 'text',
                            'enableSorting' => false,
                            'contentOptions'=>['style'=>'text-align:center;'],
                        ],
                    ],
                ]); ?>
            </div>
        <?php else: ?>
            <div class="col-md-12 col-xs-12">
                <div class="alert alert-info alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-info"></i> Información!</h4>
                    El Taller no solicito piezas.
                </div>
            </div>
        <?php endif ?>
    </div>

    </br>
    </br>
    
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <?= $form->field($model_orden_trabajo, 'observaciones')->textarea(['rows' => 3, 'disabled' => true ]) ?>
        </div>
    </div>
    </br>
    </br>
    </br>
    </br>

    <?php ActiveForm::end(); ?>

</div>

<style>
	#cotizacion-actividad-grid tfoot > tr > td {
		text-align: right;
	}

	#cotizacion-actividad-grid tfoot > tr > td:nth-child(6) {
	    text-align: center;
	    font-weight: initial;
	}
</style>

<script>
    $('.table').removeClass( "table-bordered table-striped" );
</script>