<?php
use yii\helpers\Html;
use yii\db\Query;
use app\models\CotizacionTallerActividad;
use kartik\grid\GridView;

$estado = count($pipeline_servicio) -1;
?>

<div class="expand-ficha-correctivo">
    
    <div class="row">
        <?php if ($type == 1 && $pipeline_servicio[0]->idEstadoServicio->codigo == 2): ?>
        <div class="col-xs-12">
            <div class="pull-right">
                <div class="col-xs-2">
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle btn-sm" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i>
                        <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li> <?php // Html::a('<span><i class="fa fa-eye fa-sm" aria-hidden="true"></i></span> Ver cotización',['/servicio/viewcotizaciontaller', 'id' => $id_cotizacion['id'] ]) ?></li>
                            <li> <?= Html::a('<span><i class="fa fa-eye fa-sm" aria-hidden="true"></i></span> Ver cotización',['/servicio/viewcotizacionanalista', 'id' => $id_servicio ]) ?></li>
                            <li> <?= Html::a('<span><i class="fa fa-bar-chart fa-sm" aria-hidden="true"></i></span> Análisis de viabilidad',['/servicio/analisisviabilidad', 'id' => $id_cotizacion['id'] ]) ?></li>
                            <?php if ($count_asignacion == 0): ?>
                                <li> <?= Html::a('<span><i class="fa fa-user fa-sm" aria-hidden="true"></i></span> Asignar analista de mtto.','#', ['data' => ['id' => $id_cotizacion['id'], 'toggle' => 'modal', 'target' => '#addAnalistaModal']]) ?></li>
                            <?php endif ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <?php endif ?>
        <?php if($pipeline_servicio[0]->idEstadoServicio->codigo == 12){ ?>
            <h3 class="text-center">Servicio Cancelado</h3>
        <?php }else{ ?>
        <div class="col-xs-12">
            <div class="bs-wizard" style="border-bottom:0;">
                <div class="bs-wizard-step complete">
                    <div class="bs-wizard-marker <?= (0 <= $estado && $pipeline_servicio[0]->idEstadoServicio->codigo == 0)?'active':''?> text-center">
                        <?= (0 <= $estado && $pipeline_servicio[0]->idEstadoServicio->codigo == 0)?'<i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>':''?>
                    </div>
                    <div class="bs-wizard-days <?= ( 0 <= $estado )?'active':'no-days' ?> <?php // (0 <= $estado && $pipeline_servicio[0]->idEstadoServicio->codigo == 0)?'active':''?> text-center">
                        <?php // (0 <= $estado && $pipeline_servicio[0]->idEstadoServicio->codigo == 0)?date_diff(new DateTime("now"), new DateTime($pipeline_servicio[0]->fecha))->days.' DÍAS':''?>
                        <?php 
                            if ( $pipeline_servicio[0]->idEstadoServicio->codigo == 0 ) {
                                echo date_diff(new DateTime("now"), new DateTime($pipeline_servicio[0]->fecha))->days.' DÍAS';
                            } else{
                                if ( 0 <= $estado ) {
                                    for ( $i = 1;  $i <= $estado;  $i++ ) { 
                                        if ( $pipeline_servicio[$i]->idEstadoServicio->codigo == 0 ) {
                                            echo date_diff(new DateTime($pipeline_servicio[$i-1]->fecha), new DateTime($pipeline_servicio[$i]->fecha))->days.' DÍAS';
                                            break;
                                        }
                                    }
                                }
                            }
                        ?>
                    </div>
                    <div class="progress"><div class="progress-bar"></div></div>
                    <?php 
                        echo '<div class="bs-wizard-dot-int '; 
                        if ( 0 < $estado ) {
                            for ( $i = 1;  $i <= $estado;  $i++ ) { 
                                if ( $pipeline_servicio[$i]->idEstadoServicio->codigo == 0 ) {
                                    if (date_diff(new DateTime($pipeline_servicio[$i-1]->fecha), new DateTime($pipeline_servicio[$i]->fecha))->days > $pipeline_servicio[$i]->idEstadoServicio->dias_optimos) {
                                        echo 'active-red';
                                    } else{
                                        echo 'active-green';
                                    }
                                    break;
                                }
                            }
                        }
                        echo '"></div>';
                    ?>
                    <div class="bs-wizard-stepnum text-center">Envío a taller</div>
                </div>
                <div class="bs-wizard-step complete">
                    <div class="bs-wizard-marker <?= (1 <= $estado && $pipeline_servicio[0]->idEstadoServicio->codigo == 1)?'active':''?> text-center">
                        <?= (1 <= $estado && $pipeline_servicio[0]->idEstadoServicio->codigo == 1)?'<i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>':''?>
                    </div>
                    <div class="bs-wizard-days <?= ( 1 <= $estado )?'active':'no-days' ?> <?php // (1 <= $estado && $pipeline_servicio[0]->idEstadoServicio->codigo == 1)?'active':''?> text-center">
                        <?php (1 <= $estado && $pipeline_servicio[0]->idEstadoServicio->codigo == 1)?date_diff(new DateTime("now"), new DateTime($pipeline_servicio[0]->fecha))->days.' DÍAS':''?>
                        <?php 
                            if ( $pipeline_servicio[0]->idEstadoServicio->codigo == 1 ) {
                                echo date_diff(new DateTime("now"), new DateTime($pipeline_servicio[0]->fecha))->days.' DÍAS';
                            } else{
                                if ( 1 <= $estado ) {
                                    for ( $i = 1;  $i <= $estado;  $i++ ) { 
                                        if ( $pipeline_servicio[$i]->idEstadoServicio->codigo == 1 ) {
                                            echo date_diff(new DateTime($pipeline_servicio[$i-1]->fecha), new DateTime($pipeline_servicio[$i]->fecha))->days.' DÍAS';
                                            break;
                                        }
                                    }
                                }
                            }
                        ?>
                    </div>
                    <div class="progress"><div class="progress-bar"></div></div>
                    <?php 
                        echo '<div class="bs-wizard-dot-int '; 
                        if ( 1 < $estado ) {
                            for ( $i = 1;  $i <= $estado;  $i++ ) { 
                                if ( $pipeline_servicio[$i]->idEstadoServicio->codigo == 1 ) {
                                    if (date_diff(new DateTime($pipeline_servicio[$i-1]->fecha), new DateTime($pipeline_servicio[$i]->fecha))->days > $pipeline_servicio[$i]->idEstadoServicio->dias_optimos) {
                                        echo 'active-red';
                                    } else{
                                        echo 'active-green';
                                    }
                                    break;
                                }
                            }
                        }
                        echo '"></div>';
                    ?>
                    <div class="bs-wizard-stepnum text-center">Evaluación en taller</div>
                </div>
                <div class="bs-wizard-step complete">
                    <div class="bs-wizard-marker <?= (2 <= $estado && $pipeline_servicio[0]->idEstadoServicio->codigo == 2)?'active':''?> text-center">
                        <?= (2 <= $estado && $pipeline_servicio[0]->idEstadoServicio->codigo == 2)?'<i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>':''?>
                    </div>
                    <div class="bs-wizard-days <?= ( 2 <= $estado )?'active':'no-days' ?><?php //(2 <= $estado && $pipeline_servicio[0]->idEstadoServicio->codigo == 2)?'active':''?> text-center">
                        <?php // (2 <= $estado && $pipeline_servicio[0]->idEstadoServicio->codigo == 2)?date_diff(new DateTime("now"), new DateTime($pipeline_servicio[0]->fecha))->days.' DÍAS':''?>
                        <?php 
                            if ( $pipeline_servicio[0]->idEstadoServicio->codigo == 2 ) {
                                echo date_diff(new DateTime("now"), new DateTime($pipeline_servicio[0]->fecha))->days.' DÍAS';
                            } else{
                                if ( 2 <= $estado ) {
                                    for ( $i = 1;  $i <= $estado;  $i++ ) { 
                                        if ( $pipeline_servicio[$i]->idEstadoServicio->codigo == 2 ) {
                                            echo date_diff(new DateTime($pipeline_servicio[$i-1]->fecha), new DateTime($pipeline_servicio[$i]->fecha))->days.' DÍAS';
                                            break;
                                        }
                                    }
                                }
                            }
                        ?>
                    </div>
                    <div class="progress"><div class="progress-bar"></div></div>
                    <?php 
                        echo '<div class="bs-wizard-dot-int '; 
                        if ( 2 < $estado ) {
                            for ( $i = 1;  $i <= $estado;  $i++ ) { 
                                if ( $pipeline_servicio[$i]->idEstadoServicio->codigo == 2 ) {
                                    if (date_diff(new DateTime($pipeline_servicio[$i-1]->fecha), new DateTime($pipeline_servicio[$i]->fecha))->days > $pipeline_servicio[$i]->idEstadoServicio->dias_optimos) {
                                        echo 'active-red';
                                    } else{
                                        echo 'active-green';
                                    }
                                    break;
                                }
                            }
                        }
                        echo '"></div>';
                    ?>
                    <div class="bs-wizard-stepnum text-center">Aprobación</div>
                </div>
                <div class="bs-wizard-step complete">
                    <div class="bs-wizard-marker <?= (3 <= $estado && $pipeline_servicio[0]->idEstadoServicio->codigo == 3)?'active':''?> text-center">
                        <?= (3 <= $estado && $pipeline_servicio[0]->idEstadoServicio->codigo == 3)?'<i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>':''?>
                    </div>
                    <div class="bs-wizard-days <?= ( 3 <= $estado )?'active':'no-days' ?> <?php// (3 <= $estado && $pipeline_servicio[0]->idEstadoServicio->codigo == 3)?'active':''?> text-center">
                        <?php //(3 <= $estado && $pipeline_servicio[0]->idEstadoServicio->codigo == 3)?date_diff(new DateTime("now"), new DateTime($pipeline_servicio[0]->fecha))->days.' DÍAS':''?>
                        <?php 
                            if ( $pipeline_servicio[0]->idEstadoServicio->codigo == 3 ) {
                                echo date_diff(new DateTime("now"), new DateTime($pipeline_servicio[0]->fecha))->days.' DÍAS';
                            } else{
                                if ( 3 <= $estado ) {
                                    for ( $i = 1;  $i <= $estado;  $i++ ) { 
                                        if ( $pipeline_servicio[$i]->idEstadoServicio->codigo == 3 ) {
                                            echo date_diff(new DateTime($pipeline_servicio[$i-1]->fecha), new DateTime($pipeline_servicio[$i]->fecha))->days.' DÍAS';
                                            break;
                                        }
                                    }
                                }
                            }
                        ?>
                    </div>
                    <div class="progress"><div class="progress-bar"></div></div>
                    <?php 
                        echo '<div class="bs-wizard-dot-int ';
                        if ( 3 < $estado ) {
                            for ( $i = 1;  $i <= $estado;  $i++ ) { 
                                if ( $pipeline_servicio[$i]->idEstadoServicio->codigo == 3 ) {
                                    if (date_diff(new DateTime($pipeline_servicio[$i-1]->fecha), new DateTime($pipeline_servicio[$i]->fecha))->days > $pipeline_servicio[$i]->idEstadoServicio->dias_optimos) {
                                        echo 'active-red';
                                    } else{
                                        echo 'active-green';
                                    }
                                    break;
                                }
                            }
                        }
                        echo '"></div>';
                    ?>
                    <div class="bs-wizard-stepnum text-center">Pendiente por pieza</div>
                </div>
                <div class="bs-wizard-step complete">
                    <div class="bs-wizard-marker <?= (4 <= $estado && $pipeline_servicio[0]->idEstadoServicio->codigo == 4)?'active':''?> text-center">
                        <?= (4 <= $estado && $pipeline_servicio[0]->idEstadoServicio->codigo == 4)?'<i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>':''?>
                    </div>
                    <div class="bs-wizard-days <?= ( 4 <= $estado )?'active':'no-days' ?><?php //(4 <= $estado && $pipeline_servicio[0]->idEstadoServicio->codigo == 4)?'active':''?> text-center">
                        <?php // (4 <= $estado && $pipeline_servicio[0]->idEstadoServicio->codigo == 4)?date_diff(new DateTime("now"), new DateTime($pipeline_servicio[0]->fecha))->days.' DÍAS':''?>
                        <?php 
                            if ( $pipeline_servicio[0]->idEstadoServicio->codigo == 4 ) {
                                echo date_diff(new DateTime("now"), new DateTime($pipeline_servicio[0]->fecha))->days.' DÍAS';
                            } else{
                                if ( 4 <= $estado ) {
                                    for ( $i = 1;  $i <= $estado;  $i++ ) { 
                                        if ( $pipeline_servicio[$i]->idEstadoServicio->codigo == 4 ) {
                                            echo date_diff(new DateTime($pipeline_servicio[$i-1]->fecha), new DateTime($pipeline_servicio[$i]->fecha))->days.' DÍAS';
                                            break;
                                        }
                                    }
                                }
                            }
                        ?>
                    </div>
                    <div class="progress"><div class="progress-bar"></div></div>
                    <?php 
                        echo '<div class="bs-wizard-dot-int '; 
                        if ( 4 < $estado ) {
                            for ( $i = 1;  $i <= $estado;  $i++ ) { 
                                if ( $pipeline_servicio[$i]->idEstadoServicio->codigo == 4 ) {
                                    if (date_diff(new DateTime($pipeline_servicio[$i-1]->fecha), new DateTime($pipeline_servicio[$i]->fecha))->days > $pipeline_servicio[$i]->idEstadoServicio->dias_optimos) {
                                        echo 'active-red';
                                    } else{
                                        echo 'active-green';
                                    }
                                    break;
                                }
                            }
                        }
                        echo '"></div>';
                    ?>
                    <div class="bs-wizard-stepnum text-center">En proceso</div>
                </div>
                <div class="bs-wizard-step complete">
                    <div class="bs-wizard-marker <?= (5 <= $estado && $pipeline_servicio[0]->idEstadoServicio->codigo == 5)?'active':''?> text-center">
                        <?= (5 <= $estado && $pipeline_servicio[0]->idEstadoServicio->codigo == 5)?'<i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>':''?>
                    </div>
                    <div class="bs-wizard-days <?= ( 5 <= $estado )?'active':'no-days' ?> <?php// (5 <= $estado && $pipeline_servicio[0]->idEstadoServicio->codigo == 5)?'active':''?> text-center">
                        <?php // (5 <= $estado && $pipeline_servicio[0]->idEstadoServicio->codigo == 5)?date_diff(new DateTime("now"), new DateTime($pipeline_servicio[0]->fecha))->days.' DÍAS':''?>
                        <?php 
                            if ( $pipeline_servicio[0]->idEstadoServicio->codigo == 5 ) {
                                echo date_diff(new DateTime("now"), new DateTime($pipeline_servicio[0]->fecha))->days.' DÍAS';
                            } else{
                                if ( 5 <= $estado ) {
                                    for ( $i = 1;  $i <= $estado;  $i++ ) { 
                                        if ( $pipeline_servicio[$i]->idEstadoServicio->codigo == 5 ) {
                                            echo date_diff(new DateTime($pipeline_servicio[$i-1]->fecha), new DateTime($pipeline_servicio[$i]->fecha))->days.' DÍAS';
                                            break;
                                        }
                                    }
                                }
                            }
                        ?>
                    </div>
                    <div class="progress"><div class="progress-bar"></div></div>
                    <?php 
                        echo '<div class="bs-wizard-dot-int '; 
                        if ( 5 < $estado ) {
                            for ( $i = 1;  $i <= $estado;  $i++ ) { 
                                if ( $pipeline_servicio[$i]->idEstadoServicio->codigo == 5 ) {
                                    if (date_diff(new DateTime($pipeline_servicio[$i-1]->fecha), new DateTime($pipeline_servicio[$i]->fecha))->days > $pipeline_servicio[$i]->idEstadoServicio->dias_optimos) {
                                        echo 'active-red';
                                    } else{
                                        echo 'active-green';
                                    }
                                    break;
                                }
                            }
                        }
                        echo '"></div>';
                    ?>
                    <div class="bs-wizard-stepnum text-center">Finalizado</div>
                </div>
                <div class="bs-wizard-step complete">
                    <div class="bs-wizard-marker <?= (6 <= $estado && $pipeline_servicio[0]->idEstadoServicio->codigo == 6)?'active':''?> text-center">
                        <?= (6 <= $estado && $pipeline_servicio[0]->idEstadoServicio->codigo == 6)?'<i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>':''?>
                    </div>
                    <div class="bs-wizard-days <?= ( 6 <= $estado )?'active':'no-days' ?><?php // (6 <= $estado && $pipeline_servicio[0]->idEstadoServicio->codigo == 6)?'active':''?> text-center">
                        <?php // (6 <= $estado && $pipeline_servicio[0]->idEstadoServicio->codigo == 6)?date_diff(new DateTime("now"), new DateTime($pipeline_servicio[0]->fecha))->days.' DÍAS':''?>
                        <?php 
                            if ( $pipeline_servicio[0]->idEstadoServicio->codigo == 6 ) {
                                echo date_diff(new DateTime("now"), new DateTime($pipeline_servicio[0]->fecha))->days.' DÍAS';
                            } else{
                                if ( 6 <= $estado ) {
                                    for ( $i = 1;  $i <= $estado;  $i++ ) { 
                                        if ( $pipeline_servicio[$i]->idEstadoServicio->codigo == 6 ) {
                                            echo date_diff(new DateTime($pipeline_servicio[$i-1]->fecha), new DateTime($pipeline_servicio[$i]->fecha))->days.' DÍAS';
                                            break;
                                        }
                                    }
                                }
                            }
                        ?>
                    </div>
                    <div class="progress"><div class="progress-bar"></div></div>
                    <?php 
                        echo '<div class="bs-wizard-dot-int '; 
                        if ( 6 < $estado ) {
                            for ( $i = 1;  $i <= $estado;  $i++ ) { 
                                if ( $pipeline_servicio[$i]->idEstadoServicio->codigo == 6 ) {
                                    if (date_diff(new DateTime($pipeline_servicio[$i-1]->fecha), new DateTime($pipeline_servicio[$i]->fecha))->days > $pipeline_servicio[$i]->idEstadoServicio->dias_optimos) {
                                        echo 'active-red';
                                    } else{
                                        echo 'active-green';
                                    }
                                    break;
                                }
                            }
                        }
                        echo '"></div>';
                    ?>
                    <div class="bs-wizard-stepnum text-center">Entregado</div>
                </div>
                <div class="bs-wizard-step complete">
                    <div class="bs-wizard-marker <?= (7  <= $estado && $pipeline_servicio[0]->idEstadoServicio->codigo == 7 )?'active':''?> text-center">
                        <?= (7  <= $estado && $pipeline_servicio[0]->idEstadoServicio->codigo == 7 )?'<i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>':''?>
                    </div>
                    <div class="bs-wizard-days <?= ( 7 <= $estado )?'active':'no-days' ?> <?php // (7  <= $estado && $pipeline_servicio[0]->idEstadoServicio->codigo == 7 )?'active':''?> text-center">
                        <?php // (7 <= $estado && $pipeline_servicio[0]->idEstadoServicio->codigo == 7 )?date_diff(new DateTime("now"), new DateTime($pipeline_servicio[0]->fecha))->days.' DÍAS':''?>
                        <?php 
                            if ( $pipeline_servicio[0]->idEstadoServicio->codigo == 7 ) {
                                echo date_diff(new DateTime("now"), new DateTime($pipeline_servicio[0]->fecha))->days.' DÍAS';
                            } else{
                                if ( 7 <= $estado ) {
                                    for ( $i = 1;  $i <= $estado;  $i++ ) { 
                                        if ( $pipeline_servicio[$i]->idEstadoServicio->codigo == 7 ) {
                                            echo date_diff(new DateTime($pipeline_servicio[$i-1]->fecha), new DateTime($pipeline_servicio[$i]->fecha))->days.' DÍAS';
                                            break;
                                        }
                                    }
                                }
                            }
                        ?>
                    </div>
                    <div class="progress"><div class="progress-bar"></div></div>
                    <?php 
                        echo '<div class="bs-wizard-dot-int '; 
                        if ( 7 < $estado ) {
                            for ( $i = 1;  $i <= $estado;  $i++ ) { 
                                if ( $pipeline_servicio[$i]->idEstadoServicio->codigo == 7 ) {
                                    if (date_diff(new DateTime($pipeline_servicio[$i-1]->fecha), new DateTime($pipeline_servicio[$i]->fecha))->days > $pipeline_servicio[$i]->idEstadoServicio->dias_optimos) {
                                        echo 'active-red';
                                    } else{
                                        echo 'active-green';
                                    }
                                    break;
                                }
                            }
                        }
                        echo '"></div>';
                    ?>
                    <div class="bs-wizard-stepnum text-center">Garantía</div>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="clearfix"></div>
        </br>
        </br>
        <?php if ( ($type == 1 || $type == 2) && $pipeline_servicio[0]->idEstadoServicio->codigo >= 5 && $pipeline_servicio[0]->idEstadoServicio->codigo <= 9 ): ?>
            <div class="col-md-12 col-xs-12">
                <div class="col-xs-12">
                    <p><b>DETALLE DE SERVICIO</b></p>
                </div>
                <div class="clearfix"></div>
                </br>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider_cotizacion_actividad,
                    'summary' => '',
                    'id' =>'cotizacion-actividad-grid',
                    'pjax' => true,
                    'showFooter' => true,
                    'tableOptions' => ['class' => 'table table-responsive', 'id' =>'cotizacion-actividad-table'],
                    'options' =>[
                        'class' => 'grid-blue',
                    ],
                    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                    'afterFooter' => '<tr class="kv-table-footer"><td></td><td></td><td></td><td></td><td>ITBIS</td><td style="text-align: center;">'.number_format(floatval( CotizacionTallerActividad::getItbis2($dataProvider_cotizacion_actividad->models)), 2, ',', '.') . ' RD$'.'</td></tr><tr class="kv-table-footer"><td></td><td></td><td></td><td></td><td>TOTAL</td><td style="text-align: center;">'.number_format(floatval( CotizacionTallerActividad::getTotal2($dataProvider_cotizacion_actividad->models)), 2, ',', '.') . ' RD$'.'</td></tr>',
                    'columns' => [
                        [
                            'label' => 'Servicio',
                            'format' => 'text',
                            'enableSorting' => false,
                            'contentOptions'=>['style'=>'text-align:left;'],
                            //'headerOptions' => ['style' => 'width:80%'],
                            'footer' => '',
                            'value' => function($model){
                                if ($model->id_pieza_servicio_taller) {
                                    return $model->piezaServicioTaller->servicioTaller->nombre;
                                }
                                else{
                                    return '-';
                                }
                            },
                        ],
                        [
                            'label' => 'Descripción de la Actividad',
                            'format' => 'text',
                            'enableSorting' => false,
                            'contentOptions'=>['style'=>'text-align:left;'],
                            //'headerOptions' => ['style' => 'width:80%'],
                            'footer' => '',
                            'value' => function($model){
                                if ($model->id_pieza_servicio_taller) {
                                    return $model->piezaServicioTaller->nombre;
                                }
                                else{
                                    return '-';
                                }
                            },
                        ],
                        [
                            'label' => 'Labor',
                            'format' => 'text',
                            'enableSorting' => false,
                            'footer' => '',
                            'value' => function($model){
                                if ($model->labor == 0) {
                                    return 'Reparación';
                                } elseif ( $model->labor == 1 ){
                                    return 'Salvado Leve';
                                } elseif ( $model->labor == 2 ){
                                    return 'Salvado Fuerte';
                                } else{
                                    return '-';
                                }
                            },
                        ],
                        [
                            'attribute' => 'precio',
                            'label' => 'Precio',
                            'format' => 'text',
                            'enableSorting' => false,
                            'contentOptions'=>['style'=>'text-align:center;'],
                            'value' => function($model){
                                if ($model->precio) {
                                    return number_format(floatval($model->precio), 2, ',', '.') . ' RD$';
                                } else{
                                    return '-';
                                }
                            },
                        ],
                        [
                            'label' => 'Decimal',
                            'format' => 'text',
                            'enableSorting' => false,
                            'footer' => 'SUBTOTAL',
                            'value' => function($model){
                                if ($model->decimal) {
                                    return $model->decimal;
                                } else{
                                    return '-';
                                }
                            },
                        ],
                        [
                            'label' => 'Total',
                            'format' => 'text',
                            'enableSorting' => false,
                            'contentOptions'=>['style'=>'text-align:center;'],
                            'value' => function($model){
                                if ($model->total_servicio ) {
                                    return number_format(floatval(  $model->total_servicio ), 2, ',', '.') . ' RD$';
                                } else{
                                    return '0';
                                }
                            },
                            'footer' => number_format(floatval( CotizacionTallerActividad::getSubTotal2($dataProvider_cotizacion_actividad->models)), 2, ',', '.') . ' RD$',
                        ],
                    ],
                ]); ?>
            </div>
        <?php endif ?>
        <div class="clearfix"></div>
        </br>
        <?php if ($type == 1 && $pipeline_servicio[0]->idEstadoServicio->codigo == 5): ?>
            <div class="row">
                <div class="col-xs-12">
                    <div class="pull-right">
                        <?= Html::a('Check-list de recepción',['servicio/checklistrecepcion', 'id' =>$pipeline_servicio[0]->id_servicio], ['class' => 'btn btn-small-gris', ]) ?> 
                    </div>
                </div>
            </div>
        <?php endif ?>
    </div>
    </br>
</div>

<style>
    .bs-wizard {}

    /*Form Wizard*/
    .bs-wizard {border-bottom: solid 1px #e0e0e0; padding: 0 0 10px 0;}
    .bs-wizard > .bs-wizard-step {padding: 0; position: relative; width: 12.5%; float: left;}
    .bs-wizard > .bs-wizard-step .bs-wizard-stepnum {color: #000; font-size: 8pt; font-weight: bold;}
    .bs-wizard > .bs-wizard-step .bs-wizard-info {color: #000; font-size: 8pt;}
    .bs-wizard > .bs-wizard-step > .bs-wizard-dot-int {position: absolute; width: 30px; height: 30px; display: block; background: #767171; top: 70px; left: 50%; margin-top: -15px; margin-left: -15px; border-radius: 50%;} 
    .bs-wizard > .bs-wizard-step > .bs-wizard-dot-int:after {content: ' '; width: 16px; height: 16px; background: #fff; border-radius: 50px; position: absolute; top: 7px; left: 7px; }
    .bs-wizard > .bs-wizard-step > .bs-wizard-dot-int.active-green:after { background: #a9d18e; }
    .bs-wizard > .bs-wizard-step > .bs-wizard-dot-int.active-red:after { background: #e08282; }
    .bs-wizard > .bs-wizard-step > .progress {position: relative; border-radius: 0px; height: 6px; box-shadow: none; margin: 20px 0; background: #767171;}
    .bs-wizard > .bs-wizard-step > .bs-wizard-marker { margin-bottom: 30px;}
    .bs-wizard > .bs-wizard-step > .bs-wizard-marker.active{ color: #b34b4b; content: ' '; margin-bottom: 0px;}
    .bs-wizard > .bs-wizard-step > .bs-wizard-days { margin-bottom: 0px;}
    .bs-wizard > .bs-wizard-step > .bs-wizard-days.no-days{ padding-top: 15px; }
    .bs-wizard > .bs-wizard-step > .bs-wizard-days.active{ color: #757972; content: ' '; margin-bottom: 0px; font-size: 8pt; font-weight: bold;}
    /*END Form Wizard*/

    .content-pipeline{
        border: 1px solid #dedede;
        height: 200px;
    }
    .expand-ficha-correctivo{
        padding: 10px 30px 20px 30px;
        text-align: left;
    }

    .grid .dropdown-menu {
        right: -60px;
        left: unset;
    }
</style>

<script>
</script>