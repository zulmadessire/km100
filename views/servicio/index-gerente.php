<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\db\Query;
use app\models\PipelineServicio;
use app\models\Empresa;
use yii\helpers\ArrayHelper;
use app\models\Estacion;
use kartik\select2\Select2;
use app\models\EstadoServicio;
use app\models\CotizacionTaller;
use app\models\User;


/* @var $this yii\web\View */
/* @var $searchModel app\models\PartesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'LISTADO DE SOLICITUDES EN APROBACIÓN';
?>
<div class="gerente-index">

    <div class="row">
        <div class="col-md-1 col-xs-3">
            <i class="fa fa-list-ul fa-3x" aria-hidden="true"></i>
        </div>
        <div class="col-md-10 col-xs-9">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
        </div>
        <div class="col-xs-12">
            </br>
            </br>
            <p> <span style= "background-color: #61f898; width: 30px; height: 15px; display: inline-block;"></span> Negociación con taller</p>
        </div>
    </div>
    </br>
    </br>
    
    <div class="row">
        <div class="col-xs-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel'   => $searchModel,
                'tableOptions' => ['class' => 'table table-responsive'],
                'summary' => '',
                'options' =>[
                    'class' => 'grid',
                ],
                'columns' => [
                    [
                        'attribute' => 'id',
                        'label' => 'Número de solicitud',
                        'format' => 'text',
                        'filter' => true,
                        'enableSorting' => false,
                        'headerOptions' => ['width' => '100',],
                        'contentOptions' => function ($model, $key, $index, $column) {
                            $model_cotizacion = CotizacionTaller::find()->joinWith('idSolicitudServicioTaller')->where(['solicitud_servicio_taller.id_servicio' => $model->id ])->one();
                            if ( isset($model_cotizacion) && $model_cotizacion->estado == 3 ) {
                                return  ['style' => 'background-color:#61f898'];
                            }
                        },
                    ],
                    [
                        'attribute' => 'servicio',
                        'label' => 'Tipo de solicitud',
                        'format' => 'raw',
                        //'filter' => false,
                        'filter' => ['0'=>'Mecánica Ligera','1'=>'Mecánica Especializada','2'=>'Aire Acondicionado','3'=>'Desabolladura y pintura','4'=>'Accesorios'],                        
                        'enableSorting' => false,
                        'value' => function( $model ){
                            if ( $model->servicio ) {
                                $servicios = explode("*", $model->servicio);

                                foreach ($servicios as $key => $servicio) {
                                    switch ($servicio) {
                                        case '0':
                                            $vector_servicios[] = 'Mecánica Ligera';
                                            break;

                                        case '1':
                                            $vector_servicios[] = 'Mecánica Especializada';
                                            break;

                                        case '2':
                                            $vector_servicios[] = 'Aire Acondicionado';
                                            break;

                                        case '3':
                                            $vector_servicios[] = 'Desabolladura y pintura';
                                            break;

                                        case '4':
                                            $vector_servicios[] = 'Accesorios';
                                            break;
                                        
                                        default:
                                            # code...
                                            break;
                                    }
                                    
                                }

                                $respuesta_servicios = implode('<br>', $vector_servicios);

                                return $respuesta_servicios;
                            } else{
                                return '-';
                            }
                        }
                    ],
                    [
                        'label' => 'Empresa',
                        'format' => 'raw',
                        'filter' => true,
                        'enableSorting' => false,
                        'value' => function( $model ){
                            if ( $model ) {
                                return Empresa::findOne($model->idEstacionActual->id_empresa)->nombre;
                            } else{
                                return '-';
                            }
                        },
                        
                        'filter' => Select2::widget([
                        'name' => 'ServicioSearch[id_company]',
                        'data' => ArrayHelper::map(Empresa::find()->orderBy('nombre')->all(), 'id_empresa', 'nombre'),
                        'hideSearch' => true,
                        'options' => ['placeholder' => 'Empresa',],
                        'pluginOptions' => [
                              'allowClear' => true
                          ],
                      ]),
                       
                    ],
                    [
                        'label' => 'Estacion',
                        'format' => 'raw',
                        'filter' => true,
                        'enableSorting' => false,
                        'value' => function ($model, $key, $index, $column) {
                            return $model->idEstacionActual->nombre;
                              },                        
                        'filter' => Select2::widget([
                        'name' => 'ServicioSearch[id_estacion]',
                        'data' => ArrayHelper::map(Estacion::find()->orderBy('nombre')->all(), 'id_estacion', 'nombre'),
                        'hideSearch' => true,
                        'options' => ['placeholder' => 'Estacion',],
                        'pluginOptions' => [
                              'allowClear' => true
                          ],
                      ]),
                       
                    ],
                    [
                        'label' => 'Nivel de Aprobación',
                        'format' => 'raw',
                        'filter' => true,
                        'enableSorting' => false,
                        'value' => function( $model ){
                            $model_cotizacion = CotizacionTaller::find()->joinWith('idSolicitudServicioTaller')->where('solicitud_servicio_taller.id_servicio = '. $model->id )->one();
                            if ($model_cotizacion->activo == 0) {
                                return 'Nivel 1';
                            } elseif ($model_cotizacion->activo == 1) {
                                return 'Nivel 2';
                            } elseif ($model_cotizacion->activo == 2) {
                                return 'Nivel 3';
                            }
                        },
                        'filter' => Select2::widget([
                            'name' => 'ServicioSearch[nivel]',
                            'data' => [ 0 => 'Nivel 1', 1 => 'Nivel 2', 2 => 'Nivel 3' ],
                            'hideSearch' => true,
                            'options' => ['placeholder' => 'Nivel',],
                            'pluginOptions' => [
                                  'allowClear' => true,
                            ],
                        ]),
                    ],
                    [
                        'attribute' => 'id_analista',
                        'label' => 'Analista',
                        'format' => 'raw',
                        'filter' => true,
                        'enableSorting' => false,
                        'value' => function ($model, $key, $index, $column) {
                            if ($model->id_analista) {
                                $nombre_usuario = User::findOne($model->id_analista)->username;
                                return $nombre_usuario;
                            } else {
                                return '-';
                            }
                            
                        },
                        'filterType'=> GridView::FILTER_SELECT2,
                        'filter'=> ArrayHelper::map(User::find()->joinWith('authAssignments')->where('auth_assignment.item_name = "analista"')->all(), 'id', 'username'),
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                        ],
                        'filterInputOptions'=>['placeholder'=>'Analista'],
                       
                    ],
                    [  
                        'class' => 'yii\grid\ActionColumn',
                        'header'=>'Acciones',
                        'template' => '{menu}',
                        'buttons' => [

                            //menu button
                            'menu' => function ($url, $model) {

                                $model_cotizacion = CotizacionTaller::find()->joinWith('idSolicitudServicioTaller')->where('solicitud_servicio_taller.id_servicio = '. $model->id )->one();
                                return  '<div class="dropdown">
                                          <button class="btn btn-default dropdown-toggle btn-sm" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i>
                                          <span class="caret"></span></button>
                                          <ul class="dropdown-menu">'.
                                            ( (Yii::$app->user->can('gerente-tecnico'))?
                                                (( $model_cotizacion->estado == 2 && $model_cotizacion->activo == 1 )?
                                                    '<li>'.Html::a('<span><i class="fa fa-bar-chart fa-sm" aria-hidden="true"></i></span> Análisis de viabilidad',[ '/servicio/analisisviabilidad', 'id' => $model_cotizacion->id ]).' </li>'
                                                : '') 
                                            :'').
                                            ( (Yii::$app->user->can('director-comercial'))?
                                                (( $model_cotizacion->estado == 2 && $model_cotizacion->activo == 2 )?
                                                    '<li>'.Html::a('<span><i class="fa fa-bar-chart fa-sm" aria-hidden="true"></i></span> Análisis de viabilidad',[ '/servicio/analisisviabilidad', 'id' => $model_cotizacion->id ]).' </li>'
                                                : '') 
                                            :'')
                                            .'</ul>
                                        </div>';
                            },
                        ],

                    ],
                ],
            ]); ?>
        </div>
        <?php


        ?>
    </div>
</div>