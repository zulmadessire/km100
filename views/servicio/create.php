<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Servicio */

$this->title = 'REGISTRO DE SOLICITUD DE SERVICIO';
?>
<div class="servicio-create">

	<div class="row">
		<div class="col-md-1 col-xs-3">
			<h4 class="titulo"><i class="fa fa-file-text fa-4x" aria-hidden="true"></i></h4>
		</div>
		<div class="col-md-10 col-xs-9" style="padding-top: 30px;">
			<h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
		</div>
	</div>
	<br>
    <br>

	<div class="row">
		<div class="col-xs-12">
			<?= $this->render('_form', [
		        'model' => $model,
				'model_caso' => $model_caso,
				'acta'	=> $acta,
				'reporte'	=> $reporte,
				'cliente' => $cliente,
                'empresa' => $empresa,
                'estaciones' => $estaciones,
                'vehiculos' => $vehiculos,
		    ]) ?>
		</div>
	</div>
</div>
