<?php 

use yii\helpers\Html;
use yii\db\Query;
use kartik\grid\GridView;
use app\models\PipelineServicio;
use app\models\Empresa;
use app\models\Estacion;
use app\models\SucursalConcesionario;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\AnalistaMantenimiento;
use app\models\CotizacionTallerAnalista;
use app\models\CotizacionTallerActividadSearch;
use app\models\User;
use app\models\SolicitudServicioTaller;
use app\models\Concesionario;
use app\models\Taller;
use app\models\Proveedor;

$this->title = 'ASIGNACIÓN DE TALLER';

$servicios = explode("*", $model->servicio);

foreach ($servicios as $key => $servicio) {
    switch ($servicio) {
        case '0':
            $vector_servicios[] = 'Mecánica Ligera';
            break;

        case '1':
            $vector_servicios[] = 'Mecánica Especializada';
            break;

        case '2':
            $vector_servicios[] = 'Aire Acondicionado';
            break;

        case '3':
            $vector_servicios[] = 'Desabolladura y pintura';
            break;

        case '4':
            $vector_servicios[] = 'Accesorios';
            break;
        
        default:
            # code...
            break;
    }
    
}

$respuesta_servicios = implode(' / ', $vector_servicios);

?>
<div>
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-xs-12 col-md-6">
          <div class="row">
            <div class="col-md-2 col-xs-3">
			    <h4 class="titulo"><img src="<?= Yii::$app->request->baseUrl ?>/img/car_repair.png" alt=""></h4>
            </div>
        	<div class="col-md-10 col-xs-9" style="padding-top: 30px;">
        		<h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
        	</div>
          </div>
        </div>
        <div class="col-xs-12 col-md-6">
        <br>
        <br>
            <ul class="nav nav-pills pull-right">
                <li role="presentation">
                    <?= Html::a('Listado de solicitudes',['/servicio/indexasignacion'], ['class' => 'btn btn-small-gris']); ?>   
                </li>
            </ul>
        </div>
    </div>
	</br>

       <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DEL VEHÍCULO</p>
            <hr>
        </div>
    </div>


    <div class="row">

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_modelo')->textInput(['disabled' => true, 'value' => $model_vehiculo->idModelo->idMarca->nombre ])->label('Marca') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_modelo')->textInput(['value' => strtoupper($model_vehiculo->idModelo->nombre), 'disabled' => true])->label('Modelo') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'anho')->textInput(['maxlength' => true, 'disabled' => true])->label('Año') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'mva')->textInput(['maxlength' => true, 'disabled' => true])->label('Ficha o MVA') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'chasis')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'placa')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_categoria')->textInput(['maxlength' => true, 'disabled' => true, 'value' => $model_vehiculo->idCategoria->nombre ])->label('Categoria') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'km')->textInput(['maxlength' => true, 'disabled' => true, 'value' => number_format(floatval($model_vehiculo->km), 0, ',', '.'). ' Km', ])->label('Km Actual') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= Html::a('<i class="fa fa-wrench fa-lg" aria-hidden="true"></i>&nbsp; Mttos. Realizados','#',['style' => 'float: right; margin-top: 30px;', 'class' => 'btn btn-small-gris', 'data' => ['toggle' => 'modal', 'target' => '#serviciosModal']]); ?>
        </div>
        <div class="clearfix"></div>
        <br>
        
    
        <div class="col-xs-12">
            <h5><strong>GARANTÍAS ACTIVAS</strong></h5>
            <br>
            <?= GridView::widget([
                'dataProvider' => $dataProvider_gv,
                'id' => 'garantias',
                'tableOptions' => ['class' => 'grid-blue table table-responsive table-bordered'],
                'summary' => '',
                'pjax' => true,
                'options' =>[
                    'class' => '',
                ],
                'columns' => [
                    [
                        'label' => 'Descripción',
                        'attribute' => 'partes.nombre',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                        'headerOptions' => ['width' => '500',],
                        'contentOptions'=>['style'=>'text-align:left;'],
                    ],
                    [
                        'attribute' => 'partes.tipoGarantia.nombre',
                        'label' => 'Tipo Garantía',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                    ],
                    [
                        'label' => 'Responsable',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                        'value' => function($model){
                            if ( $model->partes->id_tipo_garantia == 1 ) {
                                $concesionario = Concesionario::findOne($model->id_concesionario)->nombre;
                                return $concesionario;
                            } elseif ( $model->partes->id_tipo_garantia == 2  ) {
                                $proveedor = Proveedor::findOne($model->id_proveedor)->nombre;
                                return $proveedor;
                            } elseif ( $model->partes->id_tipo_garantia == 3 ) {
                                $taller = Taller::findOne($model->taller)->nombre;
                                return $taller;
                            } else{
                                return '-';
                            }
                        }
                    ],
                ],
            ]); ?>
        </div>

    </div>
    <br>
    </br>
    
    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DE LA SOLICITUD</p>
            <hr>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'id')->textInput(['maxlength' => true, 'disabled' => true,])->label('NÚMERO DE SOLICITUD') ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'servicio')->textInput(['maxlength' => true, 'disabled' => true, 'value' => $respuesta_servicios ])->label('TIPO DE SOLICITUD') ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'fecha')->textInput(['maxlength' => true, 'disabled' => true, ])->label('FECHA DE SOLICITUD') ?>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <?= Html::label('Cliente', '', ['class' => 'control-label']) ?>
                <?= Html::textInput('', $model->idEstacionActual->idEmpresa->cliente->nombre_comercial ,['class' => 'form-control', 'disabled' => true,]) ?>
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <?= Html::label('Empresa', '', ['class' => 'control-label']) ?>
                <?= Html::textInput('', Empresa::find()->where(['id_empresa' => $model->idEstacionActual->id_empresa ])->one()->nombre,['class' => 'form-control', 'disabled' => true,]) ?>
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'id_estacion_actual')->textInput(['maxlength' => true, 'disabled' => true, 'value' => $model->idEstacionActual->nombre])->label('ESTACIÓN') ?>
        </div>
        <div class="col-xs-12 col-md-4">
        
        <?php
        if(isset($tipo_taller)){
            $tipo_taller = $tipo_taller;
        }else {
            $tipo_taller = '';
        }
        ?>

    </div>
    <div class="col-xs-12">
        <div class="row">  
            <div class="col-md-4 col-xs-12">
                <?= Html::label('ENVIAR POR', 'send', ['class' => 'control-label']) ?>
                <?= Html::dropDownList('enviarx', $selection = ($tipo_taller!= '')?1:null, $items = 
                                        [
                                            '0' => 'Seleccione', 
                                            '1' => 'Taller', 
                                            '2' => 'Taller Estación', 
                                            '3' => 'Concesionario', 
                                            '4' => 'Cancelado', 
                                        ], 
                                        ['option' => 'value', 'class' => 'form-control', 'id' => 'op']); ?>
                
            </div>
            <div id="motivo" class="col-md-4 col-xs-12" style="display: none;">
                <?= $form->field($model, 'observacion_cancelado')->textArea(['maxlength' => true])->label('Motivo') ?>
            </div>

            <div id="tipo-t" class="col-md-4 col-xs-12" <?php if($tipo_taller == '') { echo 'style="display: none;"'; }?> >
            <?= Html::label('Tipo Taller', 'tt', ['class' => 'control-label']) ?>
            <?= 
                Html::dropDownList('linea', $selection = $tipo_taller, $items = 
                [
                    '' => 'Seleccione', 
                    '0' => 'Quick Line', 
                    '1' => 'Normal Line', 
                    '2' => 'Taller km100', 
                ], 
                ['option' => 'value', 'class' => 'form-control tipo-taller']); 
            ?>
                
            </div>
            <div id="asctaller" class="col-md-4 col-xs-12" style="display: none;">
                <?= Html::label('ESTACIÓN', 'send', ['class' => 'control-label']) ?>
                <?= 
                    Html::dropDownList('tipo_taller', $selection = null, $items = 
                    ArrayHelper::map(Estacion::find()->where('asoc_taller != 0')->all(), 'id_estacion', 
                    function($model) {
                        return $model->nombre.' -> '.$model->idEmpresa->nombre;
                    }
                    ),
                    ['prompt' => 'Seleccione', 'option' => 'value', 'class' => 'form-control']); 
                ?>

            </div>
            <div id="sucursales" class="col-md-4 col-xs-12" style="display: none;">
                <?php 
                    //Para uso de pruebas se uso el concesonario 168
                    //$id_concesionario = 168;  
                    $id_concesionario = $model->idVehiculo->idConcesionario->id;  
                    $concesionarios = ArrayHelper::map(SucursalConcesionario::find()->where('concesionario_id =' . $id_concesionario )->all(), 'id', 'nombre');
                    if(!empty($concesionarios)) 
                    {        
                ?>        
                        <?= Html::label('Sucursal', 'send', ['class' => 'control-label']) ?>
                        <?= Html::dropDownList('sucursal', $selection = null, $items = $concesionarios, 
                                            ['prompt' => 'Seleccione', 'option' => 'value', 'class' => 'form-control']); 
                        ?>
                <?php
                    }else {
                        echo "<strong>No tiene sucursales asociadas</strong>";
                    }
                ?>
            </div>
        </div>
    </br>
    </br>
    </div>
    
    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">CASOS ASOCIADOS</p>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= GridView::widget([
                'dataProvider' => $dataProvider_caso,
                'summary' => '',
                'tableOptions' => ['class' => 'table table-responsive'],
                'options' =>[
                    'class' => 'grid',
                ],
                'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                'columns' => [
                    [
                        'attribute' => 'id_caso',
                        'label' => 'Número de caso',
                        'format' => 'text',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:center;'],
                    ],
                    [
                        'attribute' => 'tipo_caso',
                        'label' => 'Tipo de caso',
                        'format' => 'text',
                        'value' => function($model){
                            if ( $model->idCaso->tipo_caso ){
                                if ( $model->idCaso->tipo_caso == 0) {
                                    return 'Accidente / Incidente';
                                } elseif ( $model->idCaso->tipo_caso == 1 ) {
                                    return 'Aire acondicionado';
                                } elseif ( $model->idCaso->tipo_caso == 2 ) {
                                    return 'Falla mecánica';
                                }
                            }
                            else
                                return '';
                        },
                        'contentOptions'=>['style'=>'text-align:center;'],
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view}',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a('<i class="fa fa-eye" aria-hidden="true"></i>', ['/caso/view', 'id' => $model->id_caso], ['title' => 'Ver Caso']);
                            },
                        ],
                        'contentOptions'=>['style'=>'text-align:center;'],
                    ],
                ],
            ]); ?>
        </div>
    </div>
    </br>
    </br>
    <?php $est = Estacion::find()->where(['id_estacion'=>$model->idEstacionActual->id_estacion])->one()->coord_google_maps; ?>
    <div id= "table-ast" <?php if($tipo_taller == '') { echo 'style="display: none;"'; }?>>
        <div class="row">
            <div class="col-xs-12">
                <p class="titulo-form">TALLERES DISPONIBLES</p>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-xs-12">
            <?php Pjax::begin(['id' => 'list-taller']); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider_taller,
                    //'filterModel' => $searchModel_taller,
                    'filterSelector' => '#tipo-t .tipo-taller',
                    'summary' => '',
                    'tableOptions' => ['class' => 'table table-responsive'],
                    'options' =>[
                        'class' => 'grid-blue'
                    ],
                    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                    'columns' => [
                        [
                            'attribute' => 'nombre',
                            'label' => 'Taller',
                            'format' => 'text',
                            'filter'   => false, 
                            'enableSorting' => false,
                            'contentOptions'=>['style'=>'text-align:center;'],
                        ],
                        [
                            'attribute' => 'espacios',
                            'label' => 'Limite de Disponibilidad',
                            'format' => 'text',
                            'enableSorting' => false,
                            'filter'   => false, 
                            'contentOptions'=>['style'=>'text-align:center;'],
                        ],
                        [
                            'label' => 'Disponibilidad Actual',
                            'format' => 'text',
                            'enableSorting' => false,
                            'filter'   => false, 
                            'contentOptions'=>['style'=>'text-align:center;'],
                            'value' => function($model){
                                $count_usados = SolicitudServicioTaller::find()->where(['id_taller' => $model->id_taller])->count();
                                if ( ($model->espacios - $count_usados) >= 0 ) {
                                    return ($model->espacios - $count_usados);
                                } else{
                                    return 0;
                                }
                            }
                        ],
                        /*[
                            'label' => 'Costo',
                            'format' => 'text',
                            'enableSorting' => false,
                            'contentOptions'=>['style'=>'text-align:center;'],
                            'value' => function($model){
                                return '-';
                            }
                        ],
                        [
                            'label' => 'Cumplimiento',
                            'format' => 'text',
                            'enableSorting' => false,
                            'contentOptions'=>['style'=>'text-align:center;'],
                            'value' => function($model){
                                return '-';
                            }
                        ],*/
                        [
                            'attribute' => 'garantia_global',
                            'label' => 'Días de Garantía',
                            'format' => 'text',
                            'enableSorting' => false,
                            'contentOptions'=>['style'=>'text-align:center;'],
                            'value' => function($model){
                                if ($model->garantia_global) {
                                    return $model->garantia_global. ' Días';
                                } else {
                                    return 'NO';
                                }
                            }
                        ],
                        [
                            'label' => 'Distancia',
                            'contentOptions'=>['style'=>'text-align:center;'],
                            'value' => function ($model) use ($est) {
                                if( $est && $model->coord_google_maps ){
                                    $km = 111.13384;
                                    $punto1 = $est;
                                    $punto2 = $model->coord_google_maps;
                                    $coo1 = explode(',',$punto1);
                                    $coo2 = explode(',',$punto2);
                                    $degrees = rad2deg(acos((sin(deg2rad($coo1[1]))*sin(deg2rad($coo2[1]))) + (cos(deg2rad($coo1[1]))*cos(deg2rad($coo2[1]))*cos(deg2rad($coo1[0]-$coo2[0])))));
                                    $dist = $degrees*$km;

                                    return round($dist, 3).'  Km' ;
                                }
                                else{
                                    return '-';
                                }
                            }
                        ],
                        [
                            'label' => 'Línea',
                            'format' => 'text',
                            'contentOptions'=>['style'=>'text-align:center;'],
                            'value' => function($model){
                                if ( $model->tipo_taller == 0 ) {
                                    return 'Quick Line';
                                } else if ( $model->tipo_taller == 1 ) {
                                    return 'Normal Line';
                                } else if ( $model->tipo_taller == 2 ) {
                                    return 'Taller Km100';
                                } else {
                                    return '-';
                                }
                            }
                        ],
                        [
                            'class'=>'kartik\grid\RadioColumn',
                            'width'=>'36px',
                            'name' => 'id_taller',
                            'headerOptions'=>['class'=>'kartik-sheet-style'],
                            /*'radioOptions' => function($model, $key, $index, $column) {
                                $count_usados = SolicitudServicioTaller::find()->where(['id_taller' => $model->id_taller])->count();
                                return ['hidden' => (($model->espacios - $count_usados > 0)?'false':'true'),];
                            }*/
                        ],
                    ],
                ]); ?>
            <?php Pjax::end(); ?>
            </div>
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>

    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="form-group text-center">
                <?= Html::submitButton('ASIGNAR', ['class' => 'btn btn-submit']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>

<!-- Modal Servicios Realizados -->
<div id="serviciosModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="title"><i class="fa fa-wrench fa-lg" aria-hidden="true"></i>&nbsp;&nbsp; MANTENIMIENTOS REALIZADOS</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true" style="color: #000; font-weight: bold;"></i></button>
        </div>
        <div class="modal-body">
            </br>
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider_servicio,
                        //'filterModel'   => $searchModel,
                        'tableOptions' => ['class' => 'table table-responsive',],
                        'layout' => "{items}\n<div align='center'>{pager}</div>",
                        'summary' => '',
                        'options' =>[
                            'class' => 'grid',
                        ],
                        'resizableColumns' => false,
                        'columns' => [
                            [
                                'class'=>'kartik\grid\ExpandRowColumn',
                                'width'=>'50px',
                                'allowBatchToggle' => false,
                                'expandIcon' => '<i class="fa fa-caret-right" aria-hidden="true" style="color:#d9d9d9;"></i>',
                                'collapseIcon' => '<i class="fa fa-caret-down" aria-hidden="true" style="color:#d9d9d9;"></i>',
                                'value'=>function ($model, $key, $index, $column) {
                                    $query = new query();
                                    $query->select('es.nombre, es.color, es.codigo')
                                        ->from('pipeline_servicio p')
                                        ->join('INNER JOIN','estado_servicio es','es.id = p.id_estado_servicio')
                                        ->where('p.id_servicio = '. $model->id )
                                        ->orderBy('p.id desc');

                                    $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                                    $estado = Yii::$app->db->createCommand($sql)->queryOne();

                                    if ( $estado['codigo'] == 10 || $estado['codigo'] == 11 ) {
                                        return '<i></i>';
                                    } else {
                                        return GridView::ROW_COLLAPSED;
                                    }
                                },
                                'detail'=>function ($model, $key, $index, $column) {
                                    $pipeline_servicio = PipelineServicio::find()->where([ 'id_servicio' => $model->id ])->andWhere('id_estado_servicio != 10 AND id_estado_servicio != 11')->orderby('id DESC')->all();
                                    $pipeline_servicio_first = PipelineServicio::find()->where([ 'id_servicio' => $model->id ])->orderby('id DESC')->one();
                                    
                                    if ( $pipeline_servicio_first->idEstadoServicio->codigo != 10 || $pipeline_servicio_first->idEstadoServicio->codigo != 11 ) {
                                        if ( $pipeline_servicio_first->idEstadoServicio->codigo >= 5 && $pipeline_servicio_first->idEstadoServicio->codigo <= 9 ) {
                                            $query = new query();
                                            $query->select('c.id')
                                                ->from('solicitud_servicio_taller sst')
                                                ->join('INNER JOIN','cotizacion_taller c','c.id_solicitud_servicio_taller = sst.id AND c.estado = 1 AND c.activo = 1')
                                                ->where('sst.id_servicio = '. $model->id)
                                                ->orderBy('c.id desc');

                                            $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                                            $id_cotizacion = Yii::$app->db->createCommand($sql)->queryOne();

                                            $searchModel_cotizacion_actividad = new CotizacionTallerActividadSearch([ 'id_cotizacion_taller' => $id_cotizacion['id'] ]);
                                            $dataProvider_cotizacion_actividad = $searchModel_cotizacion_actividad->search(Yii::$app->request->queryParams);

                                            return Yii::$app->controller->renderPartial('_expand-solicitud-servicio', ['pipeline_servicio' => $pipeline_servicio, 'type' => 2, 'dataProvider_cotizacion_actividad' => $dataProvider_cotizacion_actividad, ]);
                                        } else {
                                            return Yii::$app->controller->renderPartial('_expand-solicitud-servicio', ['pipeline_servicio' => $pipeline_servicio, 'type' => 2 ]);
                                        }
                                    } else{
                                       return '';
                                    }
                                },
                                'headerOptions'=>['class'=>'kartik-sheet-style'],
                                'expandOneOnly'=>true,
                                'enableRowClick' => true,
                            ],
                            [
                                'attribute' => 'id',
                                'label' => 'Número de solicitud',
                                'format' => 'text',
                                'filter' => true,
                                'enableSorting' => false,
                                'headerOptions' => ['width' => '100',],
                            ],
                            [
                                'attribute' => 'ficha',
                                'label' => 'MVA',
                                'format' => 'text',
                                'filter' => true,
                                'enableSorting' => true,
                            ],
                            [
                                'attribute' => 'servicio',
                                'label' => 'Tipo de solicitud',
                                'format' => 'raw',
                                'filterType'=> GridView::FILTER_SELECT2,
                                'filter'=> ['0'=>'Mecánica Ligera','1'=>'Mecánica Especializada','2'=>'Aire Acondicionado','3'=>'Desabolladura y pintura','4'=>'Accesorios'],
                                'filterWidgetOptions'=>[
                                    'pluginOptions'=>['allowClear'=>true],
                                ],
                                'filterInputOptions'=>['placeholder'=>'Selecciona'],
                                'format'=>'raw',                   
                                'enableSorting' => false,
                                'value' => function( $model ){
                                    if ( $model->servicio ) {
                                        $servicios = explode("*", $model->servicio);

                                        foreach ($servicios as $key => $servicio) {
                                            switch ($servicio) {
                                                case '0':
                                                    $vector_servicios[] = 'Mecánica Ligera';
                                                    break;

                                                case '1':
                                                    $vector_servicios[] = 'Mecánica Especializada';
                                                    break;

                                                case '2':
                                                    $vector_servicios[] = 'Aire Acondicionado';
                                                    break;

                                                case '3':
                                                    $vector_servicios[] = 'Desabolladura y pintura';
                                                    break;

                                                case '4':
                                                    $vector_servicios[] = 'Accesorios';
                                                    break;
                                                
                                                default:
                                                    # code...
                                                    break;
                                            }
                                            
                                        }

                                        $respuesta_servicios = implode('<br>', $vector_servicios);

                                        return $respuesta_servicios;
                                    } else{
                                        return '-';
                                    }
                                }
                            ],
                            [
                                'attribute' => 'dias',
                                'format' => 'text',
                                'filter' => true,
                                'enableSorting' => false,
                            ],
                            [
                                'attribute' => 'taller',
                                'label' => 'Taller Asignado',
                                'format' => 'text',
                                'enableSorting' => true,
                                //'filterType'=> GridView::FILTER_SELECT2,
                                //'filter'=> $talleres,
                                /*'filterWidgetOptions'=>[
                                    'pluginOptions'=>['allowClear'=>true],
                                ],
                                'filterInputOptions'=>['placeholder'=>'Selecciona'],*/
                            ],
                            [
                                'attribute' => 'estatus',
                                'label' => 'Estatus',
                                'format' => 'html',
                                'enableSorting' => true,
                                /*'filterType'=> GridView::FILTER_SELECT2,
                                'filter'=>ArrayHelper::map(EstadoServicio::find()->orderBy('nombre')->all(), 'id', 'nombre'),
                                'filterWidgetOptions'=>[
                                    'pluginOptions'=>['allowClear'=>true],
                                ],
                                'filterInputOptions'=>['placeholder'=>'Selecciona'],*/
                                'format'=>'raw',
                            ],
                            [  
                                'class' => 'yii\grid\ActionColumn',
                                'header'=>'Acciones',
                                'template' => '{view}',
                                'buttons' => [

                                    //view button
                                    'view' => function ($url, $model){
                                        return Html::a('<span><i class="fa fa-eye fa-sm" aria-hidden="true"></i></span>',[ '/servicio/view', 'id' => $model->id, 'origen' => 1]);
                                    },
                                ],

                           ],
                        ],
                    ]); ?>
                </div>
            </div>
            </br>
            </br>
            </br>
            </br>
            </br>
            <?php
            echo '<div class="row" style="padding-top: 25px;">';
                echo '<div class="text-center">';
                    echo Html::button('REGRESAR', ['class' => 'btn btn-cancelar', 'data' => ['dismiss' => 'modal']]);
                echo '</div>';
            echo '</div>';
            ?>
        </div>
    </div>
  </div>
</div>
<!-- Fin modal-->

<script>
    $('.table').removeClass( "table-bordered table-striped" );
    
    $('#op').on('change', function(event) {
        if($(this).val() == 1){
                $("#tipo-t").show();
                $("#table-ast").show();   
                $("#asctaller").hide();                     
                $("#sucursales").hide();                     
                $("#motivo").hide();                     
            }else if($(this).val() == 2){
                $("#asctaller").show();    
                $("#tipo-t").hide();                    
                $("#table-ast").hide();    
                $("#sucursales").hide();                     
                $("#motivo").hide();                     
                                
            }else if($(this).val() == 3){            
                $("#asctaller").hide();    
                $("#tipo-t").hide();                    
                $("#table-ast").hide();  
                $("#motivo").hide();   
                $("#sucursales").show();      
            }else if($(this).val() == 4){            
                $("#asctaller").hide();    
                $("#tipo-t").hide();                    
                $("#table-ast").hide();  
                $("#sucursales").hide();             
                $("#motivo").show();      
            }else {            
                $("#asctaller").hide();    
                $("#tipo-t").hide();                    
                $("#table-ast").hide();  
                $("#sucursales").hide();      
                $("#motivo").hide();      
            }
    });

    $('#addAnalistaModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var id = button.data('id');
        $('#submit-analista').data('id', id);
    });

    $('#analista').on('submit', function(event) {
        event.preventDefault();
        event.stopImmediatePropagation();

        $('#addAnalistaModal').modal('hide')
        var id = $('#submit-analista').data('id');

        var form = $(this);
        var data = form.serializeArray();      
        data.push({name: 'id', value: id});

        $.post( form.attr("action"), data ).done(function(response){
            alert('Analista agregado con éxito');
        });
    });
</script>

<style>
    .modal-dialog {
        margin: 80px auto;
    }
</style>