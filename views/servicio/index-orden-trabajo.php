<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\db\Query;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use app\models\CotizacionTallerActividad;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PartesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'LISTADO DE ÓRDENES DE TRABAJO';

/*$query = new query();
$query->select('es.nombre, es.color, es.codigo')
    ->from('pipeline_servicio p')
    ->join('INNER JOIN','estado_servicio es','es.id = p.id_estado_servicio')
    ->where('p.id_servicio = '. $id_servicio )
    ->orderBy('p.id desc');

$sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
$estado = Yii::$app->db->createCommand($sql)->queryOne();*/

?>
<div class="taller-index">

    <div class="row">
        <div class="col-md-1 col-xs-3">
            <i class="fa fa-list-ul fa-3x" aria-hidden="true"></i>
        </div>
        <div class="col-md-10 col-xs-9">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
        </div>
    </div>
    </br>
    </br>
    
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                //'filterModel' => $searchModel,
                'tableOptions' => ['class' => 'table table-responsive'],
                'summary' => '',
                'options' =>[
                    'class' => 'grid',
                ],
                'id' => 'orden-trabajo-grid',
                'columns' => [
                    [
                        'attribute' => 'id',
                        'label' => 'Número de Orden',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'idCotizacionTaller.idSolicitudServicioTaller.id_servicio',
                        'label' => 'Número de solicitud',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'idCotizacionTaller.idSolicitudServicioTaller.idServicio.idVehiculo.mva',
                        'label' => 'MVA',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'idCotizacionTaller.idSolicitudServicioTaller.idTaller.nombre',
                        'label' => 'Taller',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                    ],
                    /*[
                        'attribute' => 'estado',
                        'label' => 'Estado',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                    ],*/
                    [
                        'label' => 'Costo',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                        'value' => function($model){
                            $model_cotizacion_taller_actividad = CotizacionTallerActividad::find()->where(['id_cotizacion_taller' => $model->id_cotizacion_taller])->all();
                            return number_format(floatval( CotizacionTallerActividad::getTotal2($model_cotizacion_taller_actividad)), 2, ',', '.') . ' RD$';
                        }
                    ],
                    [  
                        'class' => 'yii\grid\ActionColumn',
                        'header'=>'Acciones',
                        'template' => '{menu}',
                        'buttons' => [

                            //menu button
                            'menu' => function ($url, $model) {

                                 return  '<div class="dropdown">
                                          <button class="btn btn-default dropdown-toggle btn-sm" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i>
                                          <span class="caret"></span></button>
                                          <ul class="dropdown-menu">
                                            <li>'.Html::a('<span><i class="fa fa-eye fa-sm" aria-hidden="true"></i></span>&nbsp;&nbsp;Ver orden de trabajo',[ '/servicio/viewordentrabajo', 'id' => $model->id ]).' </li>'.
                                            '<li>'.Html::a('<span><i class="fa fa-file-pdf-o fa-sm" aria-hidden="true"></i></span>&nbsp;&nbsp; Imprimir Orden',[ '/servicio/imprimirorden', 'id' => $model->id, ], ['target' => '_blank']).' </li>'.
                                            (( $model->estado == 1 && Yii::$app->user->can('admin') )?
                                                '<li>'.Html::a('<span><i class="fa fa-times-circle-o fa-sm" aria-hidden="true" style="color: #e4655b;"></i></span>&nbsp;&nbsp;Cancelar orden de trabajo','#', ['data' => ['id' => $model->id, 'toggle' => 'modal', 'target' => '#cancelarOrdenModal']]).' </li>'
                                            : '').
                                            (( $model->estado == 2 && Yii::$app->user->can('gerente-compras') )?
                                                '<li>'.Html::a('<span><i class="fa fa-times-circle-o fa-sm" aria-hidden="true" style="color: #e4655b;"></i></span>&nbsp;&nbsp;Ver motivo de cancelación','#', ['data' => ['id' => $model->id, 'toggle' => 'modal', 'target' => '#aprobacionCancelacionOrdenModal']]).' </li>'
                                            : '').
                                          '</ul>
                                        </div>';
                            },
                        ],

                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<?php if (Yii::$app->user->can('admin')): ?>
<!-- Modal Cancelar Orden de Trabajo -->
<div id="cancelarOrdenModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="title"><i class="fa fa-times-circle-o fa-lg" aria-hidden="true" style="color: #e4655b;"></i>&nbsp;&nbsp; CANCELAR ORDEN DE TRABAJO</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true" style="color: #000; font-weight: bold;"></i></button>
          </div>
        <div class="modal-body">
            </br>
            <?php $form = ActiveForm::begin([ 'id' => 'cancelar', 'action' => ['/servicio/cancelarordentrabajo',] ]); ?>
                <div class="row">
                    <div class="text-center">
                        <div class="col-xs-12 col-md-8 col-md-offset-2">
                            <label class="control-label" for="garantiavehiculo-motivo">MOTIVO DE CANCELACIÓN</label>
                            <?=
                                Select2::widget([
                                    'model' => $model_orden_trabajo,
                                    'attribute' => 'motivo_cancelacion',
                                    'data' => [1 => "Elevado costo de mano de pieza cotizada",],
                                    'options' => ['placeholder' => 'Selecciona el motivo de cancelación'],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ]);
                            ?>
                        </div>
                    </div>
                </div>

            <?php ActiveForm::end(); ?>
            </br>
            </br>
            </br>
            </br>
            <?php
            echo '<div class="row" style="padding-top: 25px;">';
                echo '<div class="text-center">';
                    echo Html::submitButton('REGISTRAR', ['class' => 'btn btn-aceptar', 'form' => 'cancelar', 'id' => 'submit-cancelar' ]);
                echo '</div>';
            echo '</div>';
            ?>
        </div>
    </div>
  </div>
</div>
<!-- Fin modal-->
<?php endif ?>

<?php if (Yii::$app->user->can('gerente-compras')): ?>
<!-- Modal Aprobacion de cancelacion de Orden de Trabajo -->
<div id="aprobacionCancelacionOrdenModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="title"><i class="fa fa-times-circle-o fa-lg" aria-hidden="true" style="color: #e4655b;"></i>&nbsp;&nbsp; APROBACIÓN DE CANCELACIÓN</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true" style="color: #000; font-weight: bold;"></i></button>
          </div>
        <div class="modal-body">
            </br>
            <?php $form = ActiveForm::begin([ 'id' => 'aprobar', 'action' => ['/servicio/aprobarcancelarordentrabajo',] ]); ?>
                <div class="row">
                    <div class="text-center">
                        <div class="col-xs-12 col-md-8 col-md-offset-2">
                            <?= $form->field($model_orden_trabajo, 'motivo_cancelacion')->textInput(['disabled' => true, 'id' => 'motivo-cancelacion'])->label('MOTIVO DE CANCELACIÓN') ?>
                        </div>
                    </div>
                </div>

            <?php ActiveForm::end(); ?>
            </br>
            </br>
            </br>
            </br>
            <?php
            echo '<div class="row" style="padding-top: 25px;">';
                echo '<div class="text-center">';
                    echo Html::submitButton('APROBAR', ['class' => 'btn btn-aceptar', 'form' => 'aprobar', 'id' => 'submit-aprobar' ]);
                echo '</div>';
            echo '</div>';
            ?>
        </div>
    </div>
  </div>
</div>
<!-- Fin modal-->
<?php endif ?>

<script>
    $('.table').removeClass( "table-bordered table-striped" );
	/*$('#id_servicio').on('change', function(event) {
		event.preventDefault();
		/* Act on the event 
		var id = $(this).val();
		$('#index-cotizacion-taller').attr('action', '/km100/servicio/indexcotizaciontaller/'+ id);
		$('#index-cotizacion-taller').submit();
	});*/
    $('#cancelarOrdenModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var id = button.data('id');
        $('#submit-cancelar').data('id', id);
    });

    $('#cancelar').on('submit', function(event) {
        event.preventDefault();
        event.stopImmediatePropagation();

        $('#cancelarOrdenModal').modal('hide')
        var id = $('#submit-cancelar').data('id');

        var form = $(this);
        var data = form.serializeArray();      
        data.push({name: 'id', value: id});

        $.post( form.attr("action"), data ).done(function(response){
            location.reload();
        });
    });

    $('#aprobacionCancelacionOrdenModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var id = button.data('id');
        $('#submit-aprobar').data('id', id);

        $.ajax({
           url: '../servicio/getmotivocancelacion',
           type: 'post',
           data: {
                id: id,
            },
            success: function (response) {
                $('#motivo-cancelacion').val(response);
            }
        });
    });

    $('#aprobar').on('submit', function(event) {
        event.preventDefault();
        event.stopImmediatePropagation();

        $('#cancelarOrdenModal').modal('hide')
        var id = $('#submit-cancelar').data('id');

        var form = $(this);
        var data = form.serializeArray();      
        data.push({name: 'id', value: id});

        $.post( form.attr("action"), data ).done(function(response){
            location.reload();
        });
    });
</script>