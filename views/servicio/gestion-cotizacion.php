<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use yii\db\Query;
use app\models\TipoPieza;
use app\models\CotizacionTallerActividad;
use app\models\AnalistaMantenimiento;

$this->title = 'COTIZACIÓN DE TALLER';
$servicios = explode("*", $model->servicio);

foreach ($servicios as $key => $servicio) {
    switch ($servicio) {
        case '0':
            $vector_servicios[] = 'Mecánica Ligera';
            break;

        case '1':
            $vector_servicios[] = 'Mecánica Especializada';
            break;

        case '2':
            $vector_servicios[] = 'Aire Acondicionado';
            break;

        case '3':
            $vector_servicios[] = 'Desabolladura y pintura';
            break;

        case '4':
            $vector_servicios[] = 'Accesorios';
            break;
        
        default:
            # code...
            break;
    }
    
}

if ( $model_taller->tipo_pago == 0 ) {
	$tipo_pago = 'Pronto Pago';
} elseif ( $model_taller->tipo_pago == 1 ) {
	$tipo_pago = 'Crédito';
} elseif ( $model_taller->tipo_pago == 2 ) {
	$tipo_pago = 'Contado';
}

?>

<div class="view-cotizacion-taller">

    <div class="row">
		<div class="col-md-1 col-xs-3">
			<h4 class="titulo"><img src="<?= Yii::$app->request->baseUrl ?>/img/car_repair.png" alt=""></h4>
		</div>
		<div class="col-md-10 col-xs-9" style="padding-top: 30px;">
			<h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
		</div>
	</div>
	</br>
    </br>

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <?php if (Yii::$app->user->can('admin')): ?>
            <div class="col-xs-12">
                <div class="pull-right">
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle btn-sm" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i>
                        <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <?php if ($count_asignacion == 0): ?>
                                <li> <?= Html::a('<span><i class="fa fa-user fa-sm" aria-hidden="true"></i></span> Asignar analista de mtto.','#', ['data' => ['id' => $model_cotizacion_taller->id , 'toggle' => 'modal', 'target' => '#addAnalistaModal']]) ?></li>
                            <?php endif ?>
                        </ul>
                    </div>
                </div>
            </div>
        <?php endif ?>
        <div class="col-md-4 col-xs-12">
            <?php if (Yii::$app->user->can('taller')): ?>
            
                <div class="pull-left" style="margin-top: 30px;">
                    <?= Html::a('Listado de cotizaciones',['/servicio/indexcotizaciontaller', 'id' => $model_cotizacion_taller->id_solicitud_servicio_taller ], ['class' => 'btn btn-small-gris']) ?> 
                </div>
            
            <?php endif ?>
            <?php if (Yii::$app->user->can('admin')): ?>
            
                <div class="pull-left" style="margin-top: 30px;">
                    <?= Html::a('Listado de solicitudes',['/servicio/gestionservicio'], ['class' => 'btn btn-small-gris']) ?> 
                </div>
            
            <?php endif ?>
        </div>
        <div class="col-md-4 col-md-offset-4 col-xs-12">
        	<?= $form->field($model_cotizacion_taller, 'id')->textInput(['disabled' => true, ])->label('NÚMERO DE COTIZACIÓN') ?>
        </div>
        </br>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DEL VEHÍCULO</p>
            <hr>
        </div>
    </div>


    <div class="row">

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_modelo')->textInput(['disabled' => true, 'value' => $model_vehiculo->idModelo->idMarca->nombre ])->label('Marca') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_modelo')->textInput(['value' => strtoupper($model_vehiculo->idModelo->nombre), 'disabled' => true])->label('Modelo') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'mva')->textInput(['maxlength' => true, 'disabled' => true])->label('Ficha o MVA') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'chasis')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'placa')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'km')->textInput(['maxlength' => true, 'disabled' => true, 'value' => number_format(floatval($model_vehiculo->km), 0, ',', '.'). ' Km', ])->label('Km Actual') ?>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DE LA SOLICITUD</p>
            <hr>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'id')->textInput(['maxlength' => true, 'disabled' => true,])->label('NÚMERO DE SOLICITUD') ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'prioridad')->textInput(['maxlength' => true, 'disabled' => true, 'value' => (($model->prioridad == 0)?'Normal':'Urgente') ])->label('PRIORIDAD') ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'linea')->textInput(['maxlength' => true, 'disabled' => true, 'value' => ($model->linea == 0)?'Quick Line':'Normal Line',])->label('LÍNEA') ?>
        </div>
        <div class="col-md-4 col-xs-12">
        	<?= $form->field($model, 'evaluacion_tecnica')->textarea(['rows' => 3, 'disabled' => true,])->label('EVALUACIÓN TÉCNICA PRELIMINAR') ?>
        </div>
		<div class="col-md-4 col-xs-12">
			<label class="control-label" for="servicio-servicio">SERVICIOS SOLICITADO</label>
			<ul class="list-group">
				<?php foreach ($vector_servicios as $servicio) {
					echo '<li class="list-group-item">'.$servicio.'</li>';
				} ?>
			</ul>
		</div>
    </div>
    </br>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">MANO DE OBRA PROPUESTA</p>
            <hr>
        </div>
    </div>
	
    <div class="row">
    	<div class="col-md-4 col-xs-12">
    		<?= $form->field($model_taller, 'tipo_pago')->textInput(['maxlength' => true, 'disabled' => true, 'value' => $tipo_pago,])->label('FORMA DE PAGO') ?>
    		<?= $form->field($model_cotizacion_taller, 'tipo_pago')->hiddenInput(['value'=> $model_taller->tipo_pago])->label(false); ?>
    	</div>
    	<div class="col-md-12 col-xs-12">
    		<?= GridView::widget([
                'dataProvider' => $dataProvider_cotizacion_actividad,
                'summary' => '',
                'id' =>'cotizacion-actividad-grid',
                'pjax' => true,
                'showFooter' => true,
                'tableOptions' => ['class' => 'table table-responsive', 'id' =>'cotizacion-actividad-table'],
                'options' =>[
                    'class' => 'grid-blue',
                ],
                'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                'afterFooter' => '<tr class="kv-table-footer"><td></td><td></td><td>ITBIS</td><td style="text-align: center;">'.number_format(floatval( CotizacionTallerActividad::getItbis($dataProvider_cotizacion_actividad->models)), 2, ',', '.') . ' RD$'.'</td></tr><tr class="kv-table-footer"><td></td><td></td><td>TOTAL</td><td style="text-align: center;">'.number_format(floatval( CotizacionTallerActividad::getTotal($dataProvider_cotizacion_actividad->models)), 2, ',', '.') . ' RD$'.'</td></tr>',
                'columns' => [
                    [
                    	'attribute' => 'descripcion',
                        'label' => 'Descripción',
                        'format' => 'text',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'footer' => '',
                    ],
                    [
                        'attribute' => 'cantidad',
                        'label' => 'Cantidad',
                        'format' => 'text',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'footer' => '',
                    ],
                    [
                        'attribute' => 'precio',
                        'label' => 'Precio',
                        'format' => 'text',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'value' => function($model){
                        	if ($model->precio) {
                        		return number_format(floatval($model->precio), 2, ',', '.') . ' RD$';
                        	} else{
                        		return '-';
                        	}
                        },
                        'footer' => 'SUBTOTAL',
                    ],
                    [
                        'label' => 'Total',
                        'format' => 'text',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'value' => function($model){
                        	if ($model->precio && $model->cantidad) {
                        		return number_format(floatval( $model->precio * $model->cantidad ), 2, ',', '.') . ' RD$';
                        	} else{
                        		return '0';
                        	}
                        },
                        'footer' => number_format(floatval( CotizacionTallerActividad::getSubTotal($dataProvider_cotizacion_actividad->models)), 2, ',', '.') . ' RD$',
                    ],
                ],
            ]); ?>
    	</div>
    </div>
    </br>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">PIEZAS SOLICITADAS</p>
            <hr>
        </div>
    </div>
    <div class="row">
    	<div class="col-md-12 col-xs-12">
    		<?= GridView::widget([
                'dataProvider' => $dataProvider_cotizacion_pieza,
                'summary' => '',
                'id' =>'cotizacion-pieza-grid',
                'pjax' => true,
                'tableOptions' => ['class' => 'table table-responsive', 'id' =>'cotizacion-pieza-table'],
                'options' =>[
                    'class' => 'grid-blue',
                ],
                'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                'columns' => [
                    [
                        'label' => 'Tipo',
                        'format' => 'text',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'value' => function($model){
                        	if ($model->id_pieza) {
                        		return $model->idPieza->tipoPieza->nombre;
                        	} else{
                        		return '-';
                        	}
                        },
                    ],
                    [
                        'attribute' => 'idPieza.nombre',
                        'label' => 'Descripcion',
                        'format' => 'text',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:center;'],
                    ],
                    [
                        'attribute' => 'cantidad',
                        'label' => 'Cantidad',
                        'format' => 'text',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:center;'],
                    ],
                ],
            ]); ?>
    	</div>
    </div>
    </br>
    </br>
    </br>
    </br>
    </br>
	
	<div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">IMÁGENES DE SOPORTE</p>
            <hr>
        </div>
    </div>
    <div class="row">
    	<div class="col-md-4 col-xs-12">
            <?php
 
                if (empty($model_cotizacion_taller->imagen_soporte)) {
                    echo 'No hay imágen cargada.';
                }
                else {
                    $image = Html::img(Yii::$app->urlManager->baseUrl . '/' . $model_cotizacion_taller->imagen_soporte, [
                        'title'=>Yii::t('app', 'Click remove button below to remove this image'),
                        'class'=>'file-preview-image',
                        'style' => 'height: 180px; display: block;',
                    ]);

                    echo '<div class="thumbnail file-preview-frame">';
                        echo $image;
                    echo '</div>';
                }
            ?>
        </div>
    </div>
    </br>
    </br>
    </br>
    </br>
    <?php ActiveForm::end(); ?>

</div>

<?php if (Yii::$app->user->can('admin')): ?>
<!-- Modal Asociar Analista -->
<div id="addAnalistaModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="title"><i class="fa fa-user fa-lg" aria-hidden="true"></i>&nbsp;&nbsp; ASIGNAR ANALISTA DE MTTO.</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true" style="color: #000; font-weight: bold;"></i></button>
          </div>
        <div class="modal-body">
            </br>
            <?php $form = ActiveForm::begin([ 'id' => 'analista', 'action' => ['/servicio/asignaranalista'] ]); ?>
                <div class="row">
                    <div class="text-center">
                        <div class="col-xs-12 col-md-8 col-md-offset-2">
                            <label class="control-label" for="garantiavehiculo-motivo">ANALISTA A ASIGNAR</label>
                            <?=
                                Select2::widget([
                                    'model' => $model_cotizacion_taller_analista,
                                    'attribute' => 'id_analista',
                                    'data' => ArrayHelper::map(AnalistaMantenimiento::find()->orderBy('nombre')->all(), 'id', 'nombre'),
                                    'options' => ['placeholder' => 'Selecciona el analista'],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ]);
                            ?>
                        </div>
                    </div>
                </div>

            <?php ActiveForm::end(); ?>
            </br>
            </br>
            </br>
            </br>
            </br>
            <?php
            echo '<div class="row" style="padding-top: 25px;">';
                echo '<div class="text-center">';
                    echo Html::button('CANCELAR', ['class' => 'btn btn-cancelar', 'data' => ['dismiss' => 'modal']]);
                    echo "&nbsp;";
                    echo Html::submitButton('REGISTRAR', ['class' => 'btn btn-aceptar', 'form' => 'analista', 'id' => 'submit-analista' ]);
                echo '</div>';
            echo '</div>';
            ?>
        </div>
    </div>
  </div>
</div>
<!-- Fin modal-->
<?php endif ?>

<style>
	#cotizacion-actividad-grid tfoot > tr > td {
		text-align: right;
	}

	#cotizacion-actividad-grid tfoot > tr > td:nth-child(4) {
	    text-align: center;
	    font-weight: initial;
	}
    .view-cotizacion-taller .dropdown-menu {
        right: 0;
        left: unset;
    }
</style>

<script>
    $('#addAnalistaModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var id = button.data('id');
        $('#submit-analista').data('id', id);
    });

    $('#analista').on('submit', function(event) {
        event.preventDefault();
        event.stopImmediatePropagation();

        $('#addAnalistaModal').modal('hide')
        var id = $('#submit-analista').data('id');

        var form = $(this);
        var data = form.serializeArray();      
        data.push({name: 'id', value: id});

        $.post( form.attr("action"), data ).done(function(response){
            location.reload();
        });
    });
    $('.table').removeClass( "table-bordered table-striped" );
</script>