<?php 
use yii\helpers\Html;
use kartik\grid\GridView;

$this->title = 'RECEPCIÓN DE VEHÍCULOS';

?>

<div class="recepcion-vehiculo">

    <div class="row">
        <div class="col-md-1 col-xs-3">
            <h4 class="titulo"><img src="<?= Yii::$app->request->baseUrl ?>/img/car_repair.png" alt=""></h4>
        </div>
        <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
            <h4 class="titulo"><span> <?= Html::encode( 'Gestión de Servicios' ) ?></span></h4>
        </div>
    </div>
    </br>

	<div class="row">
        <div class="col-md-12 col-xs-12">
            <p style="font-size: 11pt;"><b><?= Html::encode( $this->title ) ?></b></p>
        </div>
    </div>
    </br>
    <div class="row">
        <div class="col-xs-3">
            <?= Html::input('text', '', '', ['id' => 'codigo-vehiculo', 'class'=> 'form-control', 'placeholder' => 'Placa',] ) ?>
        </div>
    </div>
    </br>
    <div class="row">
    	<div class="col-md-8 col-xs-12">
    		<p style="font-size: 11pt;"><b><?= Html::encode( 'LISTADO DE VEHÍCULOS ESCANEADOS' ) ?></b></p>
    	</div>
    	<div class="col-md-4 col-xs-12">
    		<div class="pull-right">
	            <?= Html::a('Listado de Vehículos',[ '/servicio/gestionservicio' ], ['class' => 'btn btn-small-gris']) ?>
	        </div>
    	</div>
    	<div class="clearfix"></div>
    	</br>
    	<div class="col-md-12 col-xs-12">
    		<?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => ['class' => 'table table-responsive', 'id' => 'grid-vehiculos-escaneados',],
                'summary' => '',
                'options' =>[
                    'class' => 'grid',
                ],
                'resizableColumns' => false,
                'pjax' => true,
                'columns' => [
                	[
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{remove}',
                        'buttons' => [
                           'remove' => function ($url, $model) {
                                return Html::a('<i class="fa fa-times" aria-hidden="true" style="color:#d9d9d9;"></i>', ['servicio/eliminarvehiculoescaneado', 'id' => $model->id ], [ 'title' => 'Eliminar Solicitud', ]);
                           },
                        ],
                    ],
                    /*[
                        'class'=>'kartik\grid\ExpandRowColumn',
                        'width'=>'50px',
                        'allowBatchToggle' => false,
                        'expandIcon' => '<i class="fa fa-caret-right" aria-hidden="true" style="color:#d9d9d9;"></i>',
                        'collapseIcon' => '<i class="fa fa-caret-down" aria-hidden="true" style="color:#d9d9d9;"></i>',
                        'value'=>function ($model, $key, $index, $column) {
                            return GridView::ROW_COLLAPSED;
                        },
                        'detail'=>function ($model, $key, $index, $column) {
                            $pipeline_servicio = PipelineServicio::find()->where([ 'id_servicio' => $model->id ])->orderby('id DESC')->all();

                            return Yii::$app->controller->renderPartial('_expand-ficha-correctivo', ['pipeline_servicio' => $pipeline_servicio]);
                        },
                        'headerOptions'=>['class'=>'kartik-sheet-style'],
                        'expandOneOnly'=>true,
                        'enableRowClick' => true,
                    ],*/
                    [
                        'attribute' => 'ficha',
                        'label' => 'MVA',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'modelo',
                        'label' => 'Modelo',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'empresa',
                        'label' => 'Cliente',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'id_servicio',
                        'label' => 'Núm. Solicitud',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'tipo_solicitud',
                        'label' => 'Tipo de solicitud',
                        'format' => 'raw',
                        'filter' => false,
                        'enableSorting' => false,
                        'value' => function( $model ){
                            if ( $model->tipo_solicitud ) {
                                $servicios = explode("*", $model->tipo_solicitud);

                                foreach ($servicios as $key => $servicio) {
                                    switch ($servicio) {
                                        case '0':
                                            $vector_servicios[] = 'Mecánica Ligera';
                                            break;

                                        case '1':
                                            $vector_servicios[] = 'Mecánica Especializada';
                                            break;

                                        case '2':
                                            $vector_servicios[] = 'Aire Acondicionado';
                                            break;

                                        case '3':
                                            $vector_servicios[] = 'Desabolladura y pintura';
                                            break;

                                        case '4':
                                            $vector_servicios[] = 'Accesorios';
                                            break;
                                        
                                        default:
                                            # code...
                                            break;
                                    }
                                    
                                }

                                $respuesta_servicios = implode('<br>', $vector_servicios);

                                return $respuesta_servicios;
                            } else{
                                return '-';
                            }
                        }
                    ],
                ],
            ]); ?>
    	</div>
    </div>
    <br>
    <br>
    <br>
    <br>

    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="form-group text-center">
                <?= Html::a('CARGAR', ['servicio/cargarvehiculoescaneado'], [ 'class' => 'btn btn-submit', 'title' => 'Cargar Vehículos', ]); ?>
            </div>
        </div>
    </div>
</div>

<style>
	#codigo-vehiculo{
		font-size: 10pt;
		text-align: center;
		background-color: #fff2cc;
		border: none;
		color: black;
	}
</style>

<script>
	$('#codigo-vehiculo').on('keyup', function(event) {
        if ( event.keyCode == 13 ) {
            //event.stopImmediatePropagation();
            var input = $(this);
            var codigo = input.val();

            $.ajax({
                url: 'escanearvehiculo',
                type: 'post',
                data: {
                    codigo: codigo,
                },
                success: function (response) {
                    event.preventDefault();

                    if ( response == 1 ) {
                    	$.pjax.reload({container:"#grid-vehiculos-escaneados"});
                    } else{
                    	console.log('Algun Error');
                    }

                    input.val('');
                    input.focus();
                }
            });
        }
    });
    $('#codigo-vehiculo').focus();
    $('.table').removeClass( "table-bordered table-striped" );
</script>