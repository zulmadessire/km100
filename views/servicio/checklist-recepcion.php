<?php 
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\grid\GridView;
use app\models\CotizacionTallerActividad;
use app\models\Preguntas;


$this->title = 'CHECKLIST DE RECEPCIÓN';

if ( $model_cotizacion_taller->tipo_pago == 0 ) {
	$tipo_pago = 'Pronto Pago';
} elseif ( $model_cotizacion_taller->tipo_pago == 1 ) {
	$tipo_pago = 'Crédito';
} elseif ( $model_cotizacion_taller->tipo_pago == 2 ) {
	$tipo_pago = 'Contado';
}

?>
<div class="checklist-recepcion">
	<div class="row">
		<div class="col-md-1 col-xs-3">
			<h4 class="titulo"><img src="<?= Yii::$app->request->baseUrl ?>/img/list_64.png" alt=""></h4>
		</div>
		<div class="col-md-10 col-xs-9" style="padding-top: 30px;">
			<h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
		</div>
	</div>
	</br>
	
	<?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DEL VEHÍCULO</p>
            <hr>
        </div>
    </div>


    <div class="row">
		
		<div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'mva')->textInput(['maxlength' => true, 'disabled' => true])->label('Ficha o MVA') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_modelo')->textInput(['disabled' => true, 'value' => $model_vehiculo->idModelo->idMarca->nombre ])->label('Marca') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_modelo')->textInput(['value' => strtoupper($model_vehiculo->idModelo->nombre), 'disabled' => true])->label('Modelo') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'chasis')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'placa')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>

    </div>
    <br>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DE LA ORDEN DE TRABAJO</p>
            <hr>
        </div>
    </div>

    <div class="row">
    	<div class="col-md-4 col-xs-12">
            <?= $form->field($model_orden_trabajo, 'id')->textInput(['maxlength' => true, 'disabled' => true])->label('NÚMERO DE ORDEN') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_orden_trabajo, 'fecha', ['addon' => ['prepend' => ['content'=>'<i class="fa fa-calendar" aria-hidden="true"></i>']] ] )->textInput(['disabled' => true, 'value' => date('d/m/Y', strtotime($model_orden_trabajo->fecha)) ])->label('Fecha') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_cotizacion_taller, 'tipo_pago')->textInput(['maxlength' => true, 'disabled' => true, 'value' => $tipo_pago ])->label('FORMA DE PAGO') ?>
        </div>
      </div>
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'id')->textInput(['maxlength' => true, 'disabled' => true,])->label('NÚMERO DE SOLICITUD') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <?= Html::label('CLIENTE', '', ['class' => 'control-label']) ?>
 
                <?= Html::textInput('', $model->idEstacionActual->idEmpresa->cliente->nombre_comercial,['class' => 'form-control', 'disabled' => true,]) ?>
 
            </div>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'id_estacion_actual')->textInput(['maxlength' => true, 'disabled' => true, 'value' => $model->idEstacionActual->nombre])->label('ESTACIÓN') ?>
        </div>

        <div class="col-md-8 col-xs-12">
            <?= $form->field($model_solicitud_servicio_taller, 'id_taller')->textInput(['maxlength' => true, 'disabled' => true, 'value' => $model_solicitud_servicio_taller->idTaller->nombre,])->label('TALLER') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'linea')->textInput(['maxlength' => true, 'disabled' => true, 'value' => ($model->linea == 0)?'Quick Line':'Normal Line',])->label('LÍNEA') ?>
        </div>

    </div>
	</br>




    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DETALLE DE SERVICIO</p>
            <hr>
        </div>
    </div>


    <?= $form->field($model, 'temporal')->label(false)->hiddenInput([  'value'=>$dataProvider_cotizacion_actividad->getTotalCount(), 'class'=>'temporal']) ?>
 
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider_cotizacion_actividad,
                'summary' => '',
                'id' =>'cotizacion-actividad-grid',
                'pjax' => true,
                'showFooter' => true,
                'tableOptions' => ['class' => 'table table-responsive', 'id' =>'cotizacion-actividad-table'],
                'options' =>[
                    'class' => 'grid-blue',
                ],
                'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                // 'afterFooter' => '<tr class="kv-table-footer"><td></td><td></td><td>ITBIS</td><td style="text-align: center;">'.number_format(floatval( CotizacionTallerActividad::getItbis($dataProvider_cotizacion_actividad->models)), 2, ',', '.') . ' RD$'.'</td></tr><tr class="kv-table-footer"><td></td><td></td><td>TOTAL</td><td style="text-align: center;">'.number_format(floatval( CotizacionTallerActividad::getTotal($dataProvider_cotizacion_actividad->models)), 2, ',', '.') . ' RD$'.'</td></tr>',
                'columns' => [
                    [
                         
                        'label' => 'Servicio',
                        'format' => 'text',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'footer' => '',
                        'value' => function($model){
                            if ($model->id_pieza_servicio_taller) {
                                return $model->piezaServicioTaller->nombre;
                            }
                            else{
                                return '-';
                            }
                        },
                    ],
                    // [
                    //     'attribute' => 'cantidad',
                    //     'label' => 'Cantidad',
                    //     'format' => 'text',
                    //     'enableSorting' => false,
                    //     'contentOptions'=>['style'=>'text-align:center;'],
                    //     'footer' => '',
                    // ],
                    // [
                    //     'attribute' => 'precio',
                    //     'label' => 'Precio',
                    //     'format' => 'text',
                    //     'enableSorting' => false,
                    //     'contentOptions'=>['style'=>'text-align:center;'],
                    //     'value' => function($model){
                    //      if ($model->precio) {
                    //          return number_format(floatval($model->precio), 2, ',', '.') . ' RD$';
                    //      } else{
                    //          return '-';
                    //      }
                    //     },
                    //     'footer' => 'SUBTOTAL',
                    // ],
                    // [
                    //     'label' => 'Total',
                    //     'format' => 'text',
                    //     'enableSorting' => false,
                    //     'contentOptions'=>['style'=>'text-align:center;'],
                    //     'value' => function($model){
                    //      if ($model->precio && $model->cantidad) {
                    //          return number_format(floatval( $model->precio * $model->cantidad ), 2, ',', '.') . ' RD$';
                    //      } else{
                    //          return '0';
                    //      }
                    //     },
                    //     'footer' => number_format(floatval( CotizacionTallerActividad::getSubTotal($dataProvider_cotizacion_actividad->models)), 2, ',', '.') . ' RD$',
                    // ],
                    [
                        'class' => 'yii\grid\CheckboxColumn',
                        'name' => 'conforme',
                        'checkboxOptions'=>[
                                        'style'=>'display: block;
                                                  margin-right: auto;
                                                  margin-left: auto;',
                                         ],//center checkboxes
                        'header' => Html::checkBox('conforme', false, [
                            'class' => 'select-on-check-all hidden',//pull right the checkbox
                            'label' => 'Conforme',//pull left the label
                             
                        ]),
                    ],
                     [
                        'class' => 'yii\grid\CheckboxColumn',
                        'name' => 'inconforme',
                        'checkboxOptions'=>[
                                        'style'=>'display: block;
                                                  margin-right: auto;
                                                  margin-left: auto;',
                                        ],//center checkboxes
                        'header' => Html::checkBox('inconforme', false, [
                            'class' => 'select-on-check-all hidden',//pull right the checkbox
                            'label' => 'No Conforme',//pull left the label
                            
                        ]),
                    ],
                ],
            ]); ?>
        </div>
    </div>
    </br>
      <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">ENCUESTA EVALUACIÓN TALLER</p>
            <hr>
        </div>
    </div>
      <br>
   <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form"><?php echo Preguntas::findOne(['id'=>4])->pregunta ?></p>
             
        </div>
                 <div class="col-xs-12">
        <?= $form->field($model_serv_encuesta, 'respuesta')->radio(['name'=> 'cuatro','label' => 'Excelente', 'value' => 0, 'uncheck' => null, 'id' => 'check10']) ?>
        <?= $form->field($model_serv_encuesta, 'respuesta')->radio(['name'=> 'cuatro','label' => 'Regular', 'value' => 1, 'uncheck' => null, 'id' => 'check11']) ?>
        <?= $form->field($model_serv_encuesta, 'respuesta')->radio(['name'=> 'cuatro','label' => 'Malo', 'value' => 2, 'uncheck' => null, 'id' => 'check12']) ?>
        </div>
    </div>

         <br>
   <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form"><?php echo Preguntas::findOne(['id'=>5])->pregunta ?></p>
             
        </div>
  
            <div class="col-xs-12">
        <?= $form->field($model_serv_encuesta, 'respuesta')->radio(['name'=> 'cinco','label' => 'Excelente', 'value' => 0, 'uncheck' => null, 'id' => 'check13']) ?>
        <?= $form->field($model_serv_encuesta, 'respuesta')->radio(['name'=> 'cinco','label' => 'Regular', 'value' => 1, 'uncheck' => null, 'id' => 'check14']) ?>
        <?= $form->field($model_serv_encuesta, 'respuesta')->radio(['name'=> 'cinco','label' => 'Malo', 'value' => 2, 'uncheck' => null, 'id' => 'check15']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">ENCUESTA EVALUACIÓN KM100</p>
            <hr class ="hhr">
        </div>
    </div>

    <br>
       <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form"><?php echo Preguntas::findOne(['id'=>1])->pregunta ?></p>
        </div>
        <div class="col-xs-12">
        <?= $form->field($model_serv_encuesta, 'respuesta')->radio(['name'=> 'uno','label' => 'Excelente', 'value' => 0, 'uncheck' => null, 'id' => 'check1']) ?>
        <?= $form->field($model_serv_encuesta, 'respuesta')->radio(['name'=> 'uno','label' => 'Regular', 'value' => 1, 'uncheck' => null, 'id' => 'check2']) ?>
        <?= $form->field($model_serv_encuesta, 'respuesta')->radio(['name'=> 'uno','label' => 'Malo', 'value' => 2, 'uncheck' => null, 'id' => 'check3']) ?>

        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form"><?php echo Preguntas::findOne(['id'=>2])->pregunta ?></p>
          
        </div>
            <div class="col-xs-12">
        <?= $form->field($model_serv_encuesta, 'respuesta')->radio(['name'=> 'dos','label' => 'Excelente', 'value' => 0, 'uncheck' => null, 'id' => 'check4']) ?>
        <?= $form->field($model_serv_encuesta, 'respuesta')->radio(['name'=> 'dos','label' => 'Regular', 'value' => 1, 'uncheck' => null, 'id' => 'check5']) ?>
        <?= $form->field($model_serv_encuesta, 'respuesta')->radio(['name'=> 'dos','label' => 'Malo', 'value' => 2, 'uncheck' => null, 'id' => 'check6']) ?>
        </div>
    </div>
      <br>
   <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form"><?php echo Preguntas::findOne(['id'=>3])->pregunta ?></p>
             
        </div>
        <div class="col-xs-12">
        <?= $form->field($model_serv_encuesta, 'respuesta')->radio(['name'=> 'tres','label' => 'Excelente', 'value' => 0, 'uncheck' => null, 'id' => 'check7']) ?>
        <?= $form->field($model_serv_encuesta, 'respuesta')->radio(['name'=> 'tres','label' => 'Regular', 'value' => 1, 'uncheck' => null, 'id' => 'check8']) ?>
        <?= $form->field($model_serv_encuesta, 'respuesta')->radio(['name'=> 'tres','label' => 'Malo', 'value' => 2, 'uncheck' => null, 'id' => 'check9']) ?>
        </div>
    </div>


 <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">OBSERVACIÓN</p>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <?= $form->field($model, 'observacion_recepcion')->textarea(['rows' => 3])->label(false) ?>
        </div>
     
    </div>
    </br>
    </br>
    </br>
    </br>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="form-group text-center">
                <?= Html::submitButton('ENVIAR', ['class' => 'btn btn-submit','disabled'=>true,'id'=>'boton']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<style>
	#cotizacion-actividad-grid tfoot > tr > td {
		text-align: right;
	}

	#cotizacion-actividad-grid tfoot > tr > td:nth-child(4) {
	    text-align: center;
	    font-weight: initial;
	}
</style>
 
<script type="text/javascript">
	$('.table').removeClass( "table-bordered table-striped" );

 

 
        var acum = 0;
        var band1=0;
        var band2=0;
        var band3=0;
        var band=0;
        var bandera1 = 0;
        var bandera2 = 0;
        var bandera3 = 0;
        var bandera4 = 0;
        var bandera5 = 0;
        var temporal= $('.temporal').val();
    $('input[name="conforme[]"]').on('change', function(event) {
        event.preventDefault();
        /* Act on the event */
        if ( this.checked ) {
            acum++;
            if (acum>=temporal && bandera1==1 && bandera2==1 && bandera3==1 && bandera4==1 && bandera5==1) {
            boton.disabled = false;
             
        }
        if (acum<temporal && (bandera1==0 || bandera2==0 || bandera3==0 || bandera4==0 || bandera5==0)){
            boton.disabled = true;
        }
        
        } else{
            acum--;
         
             if (acum>=temporal && bandera1==1 && bandera2==1 && bandera3==1 && bandera4==1 && bandera5==1) {
             boton.disabled = false;
        }
        if (acum<temporal && (bandera1==0 || bandera2==0 || bandera3==0 || bandera4==0 || bandera5==0)){
            boton.disabled = true;
        }
        } 
       
    
    });
            $('input[name="inconforme[]"]').on('change', function(event) {
        event.preventDefault();
        /* Act on the event */
        if ( this.checked ) {
            acum++;
              if (acum>=temporal && bandera1==1 && bandera2==1 && bandera3==1 && bandera4==1 && bandera5==1) {
            
             boton.disabled = false;
        } 
        else {
             boton.disabled = true;
        }
         }
        else{
            acum--;
          
              if (acum>=temporal && bandera1==1 && bandera2==1 && bandera3==1 && bandera4==1 && bandera5==1) {
             boton.disabled = false;
        } 
        else {
             boton.disabled = true;
        }
        } 
      
         
    
    });
       
         $('input[name="uno"]').on('change', function(event) {
            bandera1 = 1;
            if (acum>=temporal && bandera1==1 && bandera2==1 && bandera3==1 && bandera4==1 && bandera5==1) {
             boton.disabled = false;
        } 
            });
          $('input[name="dos"]').on('change', function(event) {
            bandera2 = 1;
            if (acum>=temporal && bandera1==1 && bandera2==1 && bandera3==1 && bandera4==1 && bandera5==1) {
             boton.disabled = false;
        } 
            });
           $('input[name="tres"]').on('change', function(event) {
            bandera3 = 1;
            if (acum>=temporal && bandera1==1 && bandera2==1 && bandera3==1 && bandera4==1 && bandera5==1) {
             boton.disabled = false;
        } 
            });
            $('input[name="cuatro"]').on('change', function(event) {
            bandera4 = 1;
            if (acum>=temporal && bandera1==1 && bandera2==1 && bandera3==1 && bandera4==1 && bandera5==1) {
             boton.disabled = false;
        } 
            });
             $('input[name="cinco"]').on('change', function(event) {
            bandera5 = 1;
            if (acum>=temporal && bandera1==1 && bandera2==1 && bandera3==1 && bandera4==1 && bandera5==1) {
             boton.disabled = false;
        } 
            });


 
</script>
