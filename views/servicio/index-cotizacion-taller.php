<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\db\Query;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use app\models\SolicitudServicioTaller;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PartesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'LISTADO DE COTIZACIONES';

$query = new query();
$query->select('es.nombre, es.color, es.codigo')
    ->from('pipeline_servicio p')
    ->join('INNER JOIN','estado_servicio es','es.id = p.id_estado_servicio')
    ->where('p.id_servicio = '. $id_servicio )
    ->orderBy('p.id desc');

$sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
$estado = Yii::$app->db->createCommand($sql)->queryOne();

?>
<div class="taller-index">

    <div class="row">
        <div class="col-md-1 col-xs-3">
            <i class="fa fa-list-ul fa-3x" aria-hidden="true"></i>
        </div>
        <div class="col-md-10 col-xs-9">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
        </div>
    </div>
    </br>
    </br>
    
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <?php $form = ActiveForm::begin([ 'id' => 'index-cotizacion-taller', ]); ?>
                <label class="control-label">NÚMERO DE SOLICITUD</label>
                <?= Select2::widget([
                    'name' => 'id_servicio',
                    'data' => ArrayHelper::map(SolicitudServicioTaller::find()->joinWith(['cotizacionTallers' => function ($query) {
                                            $query->onCondition(['cotizacion_taller.id_solicitud_servicio_taller' => 'solicitud_servicio_taller.id']);
                                }, ])->all(), 'id', 'id_servicio'),
                    'options' => [
                        'placeholder' => 'Selecciona la solicitud',
                        'allowClear' => true,
                        'id' => 'id_servicio',
                    ],
                    'value' => $id,
                ]); ?>
            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-md-8 col-xs-12">
            <div class="pull-right" style="margin-top: 30px;">
                <?= Html::a('Listado de solicitudes',['/servicio/indextaller'], ['class' => 'btn btn-small-gris']) ?>
                <?php if ( $estado['codigo'] == 1 || $estado['codigo'] == 2 ): ?>
                    &nbsp;
                    <?= Html::a('Registrar cotización',['/servicio/crearcotizaciontaller', 'id' => $id], ['class' => 'btn btn-small-gris']) ?>
                <?php endif ?>
            </div>
        </div>
        <div class="clearfix"></div>
        </br>
        <div class="col-md-12 col-xs-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => ['class' => 'table table-responsive'],
                'summary' => '',
                'options' =>[
                    'class' => 'grid',
                ],
                'columns' => [
                    [
                        'attribute' => 'idSolicitudServicioTaller.id_servicio',
                        'label' => 'Número de solicitud',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'id',
                        'label' => 'Número de cotización',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'fecha',
                        'label' => 'Fecha de cotización',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                        'value' => function($model){
                            if ($model->fecha) {
                                return date('d/m/Y', strtotime($model->fecha));
                            } else{
                                return '-';
                            }
                        }
                    ],
                    [  
                        'class' => 'yii\grid\ActionColumn',
                        'header'=>'Acciones',
                        'template' => '{menu}',
                        'buttons' => [

                            //menu button
                            'menu' => function ($url, $model) use ($estado) {

                                 return  '<div class="dropdown">
                                          <button class="btn btn-default dropdown-toggle btn-sm" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i>
                                          <span class="caret"></span></button>
                                          <ul class="dropdown-menu">
                                          	<li>'.Html::a('<span><i class="fa fa-eye fa-sm" aria-hidden="true"></i></span> Ver cotización',['/servicio/viewcotizaciontaller', 'id' => $model->id ]).' </li>'.
                                            (( $model->estado == 0 || $model->estado == 1 || $model->estado == 3 )?
                                                '<li>'.Html::a('<span><i class="fa fa-pencil fa-sm" aria-hidden="true"></i></span> Modificar cotización',['/servicio/cotizaciontaller', 'id' => $model->id ]).' </li>'
                                            : '')
                                          .'</ul>
                                        </div>';
                            },
                        ],

                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<script>
    $('#id_servicio').on('change', function(event) {
        event.preventDefault();
        /* Act on the event */
        var id = $(this).val();
        $('#index-cotizacion-taller').attr('action', '<?= Yii::$app->request->baseUrl ?>/servicio/indexcotizaciontaller/'+ id);
        $('#index-cotizacion-taller').submit();
    });
</script>