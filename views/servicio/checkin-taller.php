<?php 
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\form\ActiveForm;
use yii\db\Query;

$this->title = 'REGISTRAR CHECK-IN';
$servicios = explode("*", $model->servicio);

foreach ($servicios as $key => $servicio) {
    switch ($servicio) {
        case '0':
            $vector_servicios[] = 'Mecánica Ligera';
            break;

        case '1':
            $vector_servicios[] = 'Mecánica Especializada';
            break;

        case '2':
            $vector_servicios[] = 'Aire Acondicionado';
            break;

        case '3':
            $vector_servicios[] = 'Desabolladura y pintura';
            break;

        case '4':
            $vector_servicios[] = 'Accesorios';
            break;
        
        default:
            # code...
            break;
    }
    
}

$respuesta_servicios = implode(' / ', $vector_servicios);

$query = new query();
$query->select('es.nombre, es.color, es.codigo')
    ->from('pipeline_servicio p')
    ->join('INNER JOIN','estado_servicio es','es.id = p.id_estado_servicio')
    ->where('p.id_servicio = '. $model->id )
    ->orderBy('p.id desc');

$sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
$estado = Yii::$app->db->createCommand($sql)->queryOne();

?>

<div class="checkin-taller">

    <div class="row">
        <div class="col-md-1 col-xs-3">
            <h4 class="titulo"><i class="fa fa-qrcode fa-4x" aria-hidden="true"></i></h4>
        </div>
        <div class="col-md-10 col-xs-9" style="padding-top: 10px;">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?> </br> NÚMERO DE SOLICITUD <?= $model->id ?></span></h4>
        </div>
    </div>
    </br>

    <div class="row">
        <div class="col-md-3 col-xs-12">
            <p style="font-size: 11pt;"><b><?= Html::encode( 'ESCANEAR VEHÍCULO' ) ?></b></p>
            <?= Html::input('text', '', '', ['id' => 'codigo-vehiculo', 'class'=> 'form-control', 'placeholder' => 'Placa',] ) ?>
        </div>
        <div class="col-md-3 col-xs-12">
            <p style="font-size: 11pt;"><b><?= Html::encode( 'ESTATUS DE LA SOLICITUD' ) ?></b></p>
            <h4 style="margin-top: 15px;"><span class="label label-oval" style="background-color: <?= $estado['color'] ?>; text-transform: uppercase;">
                <?= $estado['nombre'] ?>
            </span></h4>
        </div>
    </div>
    </br>

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right">
                <?= Html::a('Listado de solicitudes',['/servicio/indextaller'], ['class' => 'btn btn-small-gris']) ?>
            </div>
        </div>
        </br>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DEL VEHÍCULO</p>
            <hr>
        </div>
    </div>


    <div class="row">

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_modelo')->textInput(['disabled' => true, 'value' => $model_vehiculo->idModelo->idMarca->nombre ])->label('Marca') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_modelo')->textInput(['value' => strtoupper($model_vehiculo->idModelo->nombre), 'disabled' => true])->label('Modelo') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'mva')->textInput(['maxlength' => true, 'disabled' => true])->label('Ficha o MVA') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'chasis')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'placa')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'km')->textInput(['maxlength' => true, 'disabled' => true, 'value' => number_format(floatval($model_vehiculo->km), 0, ',', '.'). ' Km', ])->label('Km Actual') ?>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DE LA SOLICITUD</p>
            <hr>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'id')->textInput(['maxlength' => true, 'disabled' => true,])->label('NÚMERO DE SOLICITUD') ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'servicio')->textInput(['maxlength' => true, 'disabled' => true, 'value' => $respuesta_servicios ])->label('TIPO DE SOLICITUD') ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <?= Html::label('GARANTÍA DE CONCESIONARIO', '', ['class' => 'control-label']) ?>
                <?= Html::textInput('', 'No',['class' => 'form-control', 'disabled' => true,]) ?>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <?= Html::label('CLIENTE', '', ['class' => 'control-label']) ?>
                <?= Html::textInput('', $model->idEstacionActual->idEmpresa->nombre,['class' => 'form-control', 'disabled' => true,]) ?>
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'id_estacion_actual')->textInput(['maxlength' => true, 'disabled' => true, 'value' => $model->idEstacionActual->nombre])->label('ESTACIÓN') ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'linea')->textInput(['maxlength' => true, 'disabled' => true, 'value' => ($model->linea == 0)?'Quick Line':'Normal Line',])->label('LÍNEA') ?>
        </div>
    </div>
    </br>

    <?php ActiveForm::end(); ?>

</div>

<style>
	#codigo-vehiculo{
		font-size: 10pt;
		text-align: center;
		background-color: #fff2cc;
		border: none;
		color: black;
	}
</style>

<script>
	$('#codigo-vehiculo').on('keyup', function(event) {
        if ( event.keyCode == 13 ) {
            //event.stopImmediatePropagation();
            var input = $(this);
            var codigo = input.val();
            var id_servicio = <?= $model->id ?>;

            $.ajax({
                url: '../escanearvehiculotaller',
                type: 'post',
                data: {
                    codigo: codigo,
                    id_servicio: id_servicio,
                },
                success: function (response) {
                    event.preventDefault();

                    if ( response == 1 ) {
                    	location.reload();
                    } else if ( response == 0 ) {
                    	console.log('No se encuentra en estado de checkin');
                    } else if ( response == 2 ){
                        console.log('El codigo del vehiculo no coincide con la solicitud');
                    } else if ( response == 3 ) {
                        console.log('El codigo del vehiculo es incorrecto');
                    } else{
                        console.log('Otro error');
                    }

                    input.val('');
                    input.focus();
                }
            });
        }
    });
    $('#codigo-vehiculo').focus();
</script>