<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Caso;
use app\models\Empresa;
use app\models\Cliente;
use app\models\Estacion;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\models\Servicio */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="servicio-form">
    <?php $form = ActiveForm::begin(['id' => 'form-servicio']); ?>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DEL VEHÍCULO</p>
            <hr>
        </div>
    </div>

    <div class="row">
        
        <div class="col-md-4 col-xs-12">
            <?=
                $form->field($model, 'ficha')->widget(Select2::classname(), [
                    'data' => $vehiculos,
                    'options' => ['placeholder' => 'Selecciona el MVA', 'id' => 'ficha'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ])->label('MVA O FICHA <span class="obligatorio">*&nbsp;</span>');
            ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?=
                $form->field($model, 'placa')->widget(DepDrop::classname(), [
                    'type'=>DepDrop::TYPE_SELECT2,
                    'options'=>['id'=>'placa', 'class' => 'form-control'],
                    'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                    'pluginOptions'=>[
                        'depends'=>['ficha'],
                        'placeholder'=>'Selecciona la placa',
                        'url'=>Url::to(['/servicio/getplacas']),
                        'initialize' => true,
                    ]
                ])->label('PLACA <span class="obligatorio">*&nbsp;</span>');
            ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <?= Html::label('MODELO', 'Servicio[modelo]', ['class' => 'control-label']) ?>
                <?= Html::textInput('Servicio[modelo]', '',['class' => 'form-control', 'id' => 'modelo', 'disabled' => true ]) ?>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <?= Html::label('MARCA', 'Servicio[marca]', ['class' => 'control-label']) ?>
                <?= Html::textInput('Servicio[marca]', '',['class' => 'form-control', 'id' => 'marca', 'disabled' => true ]) ?>
            </div>
        </div>
        <?= Html::hiddenInput('Servicio[id_vehiculo]', '', ['id' => 'servicio-id_vehiculo', ]); ?>
    </div>
    <br>
    <br>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DE LA SOLICITUD</p>
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 col-xs-12">
            <?=
                $form->field($model, 'tipo_caso')->widget(Select2::classname(), [
                    'data' => ['0' => 'Accidente / Incidente', '3' => 'Aire acondicionado', '2' => 'Falla mecánica', ],
                    'options' => ['placeholder' => 'Selecciona el tipo de caso', 'id' => 'tipo_caso'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('TIPO DE CASO <span class="obligatorio">*&nbsp;</span>');
            ?>
        </div>
        <div class="col-md-8 col-xs-12">
            <div class="form-group">
                <?=
                    $form->field($model_caso, 'id_caso')->widget(DepDrop::classname(), [
                        'type'=>DepDrop::TYPE_SELECT2,
                        'options'=>['id'=>'casos', 'class' => 'form-control', 'multiple'=>true],
                        'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                        'pluginOptions'=>[
                            'depends'=>['placa', 'tipo_caso'],
                            'placeholder'=>'Selecciona los casos asociados',
                            'url'=>Url::to(['/servicio/getcasos']),
                            'initialize' => true,
                        ],
                        'pluginEvents' => [
                            "depdrop:change"=>"function(event, id, value, count) {  $('#placa').trigger('change'); }",
                        ]
                    ])->label('CASOS ASOCIADOS <span class="obligatorio">*&nbsp;</span>');
                ?>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-4 col-xs-12">
            <?=
                $form->field($model, 'prioridad')->widget(Select2::classname(), [
                    'data' => ['0' => 'Normal', '1' => 'Urgente' ],
                    'options' => ['placeholder' => 'Selecciona la prioridad',],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('PRIORIDAD <span class="obligatorio">*&nbsp;</span>');
            ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?=
                $form->field($model, 'dano')->widget(Select2::classname(), [
                    'data' => ['0' => 'Bajo', '1' => 'Medio', '2' => 'Alto' ],
                    'options' => ['placeholder' => 'Selecciona el nivel de daño',],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('NIVEL DE DAÑO <span class="obligatorio">*&nbsp;</span>');
            ?>
        </div>
        <div class="col-md-4 col-xs-12">
        <?=
            $form->field($reporte, 'imageFiles[]')->fileInput(['multiple' => false, 'accept' => 'image/*', 'class' =>"form-control btn btn-default btn-md"]);
        ?>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-4 col-xs-12" id = "img-acta" style="display: none;">
        <?=
            $form->field($acta, 'imageFiles[]')->fileInput(['multiple' => false, /*'disabled' => true,*/ 'accept' => 'image/*', 'class' =>"form-control btn btn-default btn-md",]);
        ?>
        </div>
    </div>
    <br>
    <br>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">SERVICIO DE TALLER</p>
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="form-group field-servicio-servicio required">
                <label class="control-label" for="servicio-servicio">SERVICIO SOLICITADO <span class="obligatorio">*&nbsp;</span></label>
                <div class="col-xs-12 col-md-11 col-md-offset-1 checkbox">
                    <label class="col-xs-2 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox1" value="0" name="tipo_servicio[]"> Mecánica Ligera
                    </label>
                    <label class="col-md-2 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox2" value="1" name="tipo_servicio[]"> Mecánica Especializada
                    </label>
                    <label class="col-md-2 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox3" value="2" name="tipo_servicio[]"> Aire Acondicionado
                    </label>
                    <label class="col-md-3 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox3" value="3" name="tipo_servicio[]"> Desabolladura y pintura
                    </label>
                    <label class="col-md-2 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox3" value="4" name="tipo_servicio[]"> Accesorios
                    </label>
                </div>

                <div class="help-block"></div>
            </div>
        </div>
        <div class="clearfix"></div>
        <br>
        <div class="col-md-12 col-xs-12">
            <?= $form->field($model, 'evaluacion_tecnica')->textarea(['rows' => 1])->label('EVALUACIÓN TÉCNICA PRELIMINAR <span class="obligatorio">*&nbsp;</span>') ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <label class="control-label" for="id_estado">UBICACIÓN ACTUAL - CLIENTE <span class="obligatorio">*&nbsp;</span></label>
                <?php if (\Yii::$app->user->can('registro servicio todos') ): ?>
                    <?=  Select2::widget([
                            'name' => 'Servicio[id_cliente]',
                            'data' => ArrayHelper::map(Cliente::find()->where('id != 6')->orderBy('nombre_comercial')->all(), 'id', 'nombre_comercial'),
                            'options' => ['placeholder' => 'Selecciona el cliente', 'id' => 'id_cliente', 'initialize' => true,]
                        ]); 
                    ?>
                <?php else: ?>
                    <?= Html::textInput('Servicio[id_cliente]', $cliente->nombre_comercial, ['class' => 'form-control', 'id' => 'id_cliente', 'disabled' => true,]) ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <label class="control-label" for="id_estado">UBICACIÓN ACTUAL - EMPRESA <span class="obligatorio">*&nbsp;</span></label>
            <?php if ( \Yii::$app->user->can('registro servicio todos') ){ ?>
                <?= DepDrop::widget([
                    'name' => 'Servicio[id_empresa]',
                    'data' => ($model->id_estacion_actual)?ArrayHelper::map( Empresa::find()->where(['id_empresa'=>$model->idEstacion->id_empresa])->all(), 'id_empresa', 'nombre' ):[''=>''],
                    'type'=>DepDrop::TYPE_SELECT2,
                    'options' => ['id'=>'id_empresa'],
                    'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                    'pluginOptions' => [
                       'depends'  => ['id_cliente'],
                       'placeholder' => 'Selecciona la empresa',
                       'url' => Url::to(['/vehiculo/getempresas']),
                       'initialize' => true,
                    ]
                ]);  ?>
            <?php } elseif (\Yii::$app->user->can('registro servicio cliente')) { ?>
                <?=  
                    Select2::widget([
                            'name' => 'Servicio[id_empresa]',
                            'value' => ($model->id_estacion_actual)?$model->idEstacion->id_empresa:'',
                            'data' => ArrayHelper::map(Empresa::find()->where('cliente_id = ' . $cliente->id )->orderBy('nombre')->all(), 'id_empresa', 'nombre'),
                            'options' => ['placeholder' => 'Selecciona la empresa', 'id' => 'id_empresa', 'initialize' => true,]
                        ]); 
                    ?>
            <?php } elseif (\Yii::$app->user->can('registro servicio estaciones asociadas')) { ?>
                <?= Html::textInput('Servicio[id_empresa]', $empresa->nombre, ['class' => 'form-control', 'id' => 'id_empresa', 'disabled' => true,]) ?>
            <?php } ?>
            
        </div>
        <div class="col-md-4 col-xs-12">
            <?php if (\Yii::$app->user->can('registro servicio estaciones asociadas')) { ?>
                <?=
                    $form->field($model, 'id_estacion_actual')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(Estacion::find()->where(['id_estacion' => $estaciones ])->all(), 'id_estacion', 'nombre'),
                        'options' => ['placeholder' => 'Selecciona la estacion', 'id' => 'id_estacion'],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ])->label('UBICACIÓN ACTUAL - ESTACIÓN <span class="obligatorio">*&nbsp;</span>');
                ?>
            <?php } else { ?>
                <?=
                    $form->field($model, 'id_estacion_actual')->widget(DepDrop::classname(), [
                        'type'=>DepDrop::TYPE_SELECT2,
                        'data' => ($model->id_estacion_actual)?ArrayHelper::map( Estacion::find()->where(['id_estacion'=>$model->id_estacion_actual])->all(), 'id_estacion', 'nombre'):[''=>''],
                        'options'=>['id'=>'id_estacion', 'class' => 'form-control'],
                        'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                        'pluginOptions'=>[
                            'depends'=>['id_empresa'],
                            'placeholder'=>'Selecciona la estación',
                            'url'=>Url::to(['/vehiculo/getestaciones']),
                            'initialize' => true,
                        ]
                    ])->label('UBICACIÓN ACTUAL - ESTACIÓN <span class="obligatorio">*&nbsp;</span>');
                ?>
            <?php } ?>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'responsable')->textInput(['maxlength' => true])->label('NOMBRE DEL TÉCNICO <span class="obligatorio">*&nbsp;</span>') ?>
        </div>
        <!--<div class="col-md-4 col-xs-12">
            <?php
                /*$form->field($model, 'linea')->widget(Select2::classname(), [
                    'data' => ['0' => 'Quick Line', '1' => 'Normal Line' ],
                    'options' => ['placeholder' => 'Selecciona la línea',],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('LÍNEA <span class="obligatorio">*&nbsp;</span>');*/
            ?>
        </div>-->
    </div>
    <!-- <br>
    <br>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">COBRO A CLIENTE</p>
            <hr>
        </div>
    </div> -->
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <?php
            echo  $form->field($model, 'cobro_cliente')->hiddenInput(['value'=> 0])->label(false);
            /*    $form->field($model, 'cobro_cliente')->widget(Select2::classname(), [
                    'data' => ['0' => 'No', '1' => 'Si',],
                    'options' => ['placeholder' => 'Selecciona la respuesta'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('¿SE REALIZÓ COBRO A CLIENTE FINAL? <span class="obligatorio">*&nbsp;</span>');*/
            ?>
        </div>
    </div>

    <br>
    <br>
    <br>
    <br>

    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="form-group text-center">
                <?= Html::submitButton($model->isNewRecord ? 'ENVIAR' : 'ACTUALIZAR', ['class' => $model->isNewRecord ? 'btn btn-submit' : 'btn btn-primary', 'id' => 'submit-servicio']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    <?php // $form->field($model, 'fecha')->textInput() ?>

</div>
<div class="cargando"></div>
<style>
</style>
<script>

    $('#placa').on('change', function(event) {
        event.preventDefault();
        var id_vehiculo = $(this).val();
        $.ajax({
            url: 'getvehiculo',
            type: 'post',
            data: {
                id_vehiculo: id_vehiculo,
            },
            success: function (response) {
                if (response) {
                    $('#servicio-id_vehiculo').val(id_vehiculo);
                    $('#modelo').val(response['modelo']);
                    $('#marca').val(response['marca']);
                }
            }
        });
    });

    $('#tipo_caso').on('change', function(event) {
        event.preventDefault();
        if ( $(this).val() == 0 ) {
            $( '#img-acta' ).show();
        } else{
            $( '#img-acta' ).hide();
        }
    });

    /*$('#form-servicio').on('submit', function(event) {
        event.preventDefault();
        event.stopImmediatePropagation();


        $('#form-servicio').attr({
            disabled: 'true',
        });
        $("body").addClass("loading");

        $.ajax({
            url: $(this).attr("action"),
            type: 'post',
            success: function (response) {
                $("body").removeClass("loading");
            }
        });
        
    });*/
    
</script>
