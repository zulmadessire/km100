<?php 
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\grid\GridView;
use kartik\select2\Select2;
use app\models\CotizacionTallerActividad;

$this->title = 'Solicitud de servicio';

$servicios = explode("*", $model->servicio);

foreach ($servicios as $key => $servicio) {
    switch ($servicio) {
        case '0':
            $vector_servicios[] = 'Mecánica Ligera';
            break;

        case '1':
            $vector_servicios[] = 'Mecánica Especializada';
            break;

        case '2':
            $vector_servicios[] = 'Aire Acondicionado';
            break;

        case '3':
            $vector_servicios[] = 'Desabolladura y pintura';
            break;

        case '4':
            $vector_servicios[] = 'Accesorios';
            break;
        
        default:
            # code...
            break;
    }
    
}

$respuesta_servicios = implode(' / ', $vector_servicios);

?>
<div class="view-solicitud">

    <div class="row">
        <div class="col-md-1 col-xs-3">
        	<h4 class="titulo">
                <img src="<?= Yii::$app->request->baseUrl ?>/img/car_repair.png" alt="">
        	</h4>
        </div>
        <div class="col-md-10 col-xs-9" style="padding-top: 20px;">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
        </div>
    </div>
    </br>
    </br>
    </br>

    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right">
                <?php
                    if($origen == 1) {
                        echo Html::a('Listado de solicitudes',['/servicio'], ['class' => 'btn btn-small-gris']);
                    }else{
                        echo Html::a('Gestion de servicios',['/servicio/gestionservicio'], ['class' => 'btn btn-small-gris']);
                    } 
                ?>
            </div>
        </div>
        </br>
    </div>

    <?php $form = ActiveForm::begin([ 'id' => 'analisis-viabilidad-form', 'action' => ['/servicio/guardaranalisisviabilidad',] ]); ?>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DEL VEHÍCULO</p>
            <hr>
        </div>
    </div>
    <div class="row">
    	<div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'mva')->textInput(['maxlength' => true, 'disabled' => true])->label('Ficha o MVA') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'placa')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_modelo')->textInput(['value' => strtoupper($model_vehiculo->idModelo->nombre), 'disabled' => true])->label('Modelo') ?>
        </div>
    </div>

    <br>
    <br>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DE LA SOLICITUD</p>
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'id')->textInput(['maxlength' => true, 'disabled' => true,])->label('NÚMERO DE SOLICITUD') ?>
        </div>
	
		<div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'servicio')->textInput(['maxlength' => true, 'disabled' => true, 'value' => $respuesta_servicios ])->label('TIPO DE SOLICITUD') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'linea')->textInput(['maxlength' => true, 'disabled' => true, 'value' => ($model->linea == 0)?'Quick Line':'Normal Line',])->label('LÍNEA') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'responsable')->textInput(['maxlength' => true, 'disabled' => true,])->label('TÉNICO ENCARGADO') ?>
        </div>

        <div class="col-md-8 col-xs-12">
        	<?= $form->field($model, 'evaluacion_tecnica')->textarea(['rows' => 3, 'disabled' => true,])->label('EVALUACIÓN TÉCNICA PRELIMINAR') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">CASOS ASOCIADOS</p>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= GridView::widget([
                'dataProvider' => $dataProvider_caso,
                'summary' => '',
                'tableOptions' => ['class' => 'table table-responsive'],
                'options' =>[
                    'class' => 'grid',
                ],
                'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                'columns' => [
                    [
                        'attribute' => 'id_caso',
                        'label' => 'Número de caso',
                        'format' => 'text',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:center;'],
                    ],
                    [
                        'attribute' => 'tipo_caso',
                        'label' => 'Tipo de caso',
                        'format' => 'text',
                        'value' => function($model){
                            if ( $model->idCaso->tipo_caso ){
                                if ( $model->idCaso->tipo_caso == 0) {
                                    return 'Accidente / Incidente';
                                } elseif ( $model->idCaso->tipo_caso == 1 ) {
                                    return 'Aire acondicionado';
                                } elseif ( $model->idCaso->tipo_caso == 2 ) {
                                    return 'Falla mecánica';
                                }
                            }
                            else
                                return '';
                        },
                        'contentOptions'=>['style'=>'text-align:center;'],
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view}',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a('<i class="fa fa-eye" aria-hidden="true"></i>', ['/caso/view', 'id' => $model->id_caso], ['title' => 'Ver Caso']);
                            },
                        ],
                        'contentOptions'=>['style'=>'text-align:center;'],
                    ],
                ],
            ]); ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    $('.table').removeClass( "table-bordered table-striped" );
</script>