<?php 
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\form\ActiveForm;
use kartik\select2\Select2;

$this->title = 'DETALLE DE LA ASIGNACIÓN';
$servicios = explode("*", $model->servicio);

foreach ($servicios as $key => $servicio) {
    switch ($servicio) {
        case '0':
            $vector_servicios[] = 'Mecánica Ligera';
            break;

        case '1':
            $vector_servicios[] = 'Mecánica Especializada';
            break;

        case '2':
            $vector_servicios[] = 'Aire Acondicionado';
            break;

        case '3':
            $vector_servicios[] = 'Desabolladura y pintura';
            break;

        case '4':
            $vector_servicios[] = 'Accesorios';
            break;
        
        default:
            # code...
            break;
    }
    
}
?>

<div class="envio-taller">

    <div class="row">
        <div class="col-md-1 col-xs-3">
            <h4 class="titulo"><img src="<?= Yii::$app->request->baseUrl ?>/img/car_repair.png" alt=""></h4>
        </div>
        <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
        </div>
    </div>
    </br>
    </br>

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-4 col-md-offset-8 col-xs-12">
            <?= $form->field($model, 'id')->textInput(['disabled' => true, ])->label('NÚMERO DE SOLICITUD') ?>
        </div>
        </br>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DEL VEHÍCULO</p>
            <hr>
        </div>
    </div>


    <div class="row">

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_modelo')->textInput(['disabled' => true, 'value' => $model_vehiculo->idModelo->idMarca->nombre ])->label('Marca') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_modelo')->textInput(['value' => strtoupper($model_vehiculo->idModelo->nombre), 'disabled' => true])->label('Modelo') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'mva')->textInput(['maxlength' => true, 'disabled' => true])->label('Ficha o MVA') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'chasis')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'placa')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_categoria')->textInput(['maxlength' => true, 'disabled' => true, 'value' => $model_vehiculo->idCategoria->nombre ])->label('Categoria') ?>
        </div>

    </div>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DE LA SOLICITUD</p>
            <hr>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'id')->textInput(['maxlength' => true, 'disabled' => true,])->label('NÚMERO DE SOLICITUD') ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'prioridad')->textInput(['maxlength' => true, 'disabled' => true, 'value' => (($model->prioridad == 0)?'Normal':'Urgente') ])->label('PRIORIDAD') ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'linea')->textInput(['maxlength' => true, 'disabled' => true, 'value' => ($model->linea == 0)?'Quick Line':'Normal Line',])->label('LÍNEA') ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'evaluacion_tecnica')->textarea(['rows' => 3, 'disabled' => true,])->label('EVALUACIÓN TÉCNICA PRELIMINAR') ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <label class="control-label" for="servicio-servicio">SERVICIOS SOLICITADO</label>
            <ul class="list-group">
                <?php foreach ($vector_servicios as $servicio) {
                    echo '<li class="list-group-item">'.$servicio.'</li>';
                } ?>
            </ul>
        </div>
    </div>
    </br>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DEL TALLER</p>
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_taller, 'nombre')->textInput(['maxlength' => true, 'disabled' => true,])->label('TALLER') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_taller, 'telefono')->textInput(['maxlength' => true, 'disabled' => true,])->label('TELÉFONO') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_taller, 'direccion')->textInput(['maxlength' => true, 'disabled' => true,])->label('DIRECIÓN') ?>
        </div>
    </div>
    </br>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">INFORMACIÓN DE LA ENTREGA</p>
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="alert alert-info" role="alert">
                <strong><i class="icon fa fa-info"></i></span> Info!</strong> 
                Indique la siguiente información sobre el estatus actual del vehículo.
            </div>
        </div>

        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <?=
                    $form->field($model, 'accion')->widget(Select2::classname(), [
                        'data' => [ 0 => 'Enviar a Taller', 1 => 'Cancelar Servicio',],
                        'options' => ['value' => '','placeholder' => 'Selecciona la acción', 'id' => 'accion'],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ])->label('ACCIÓN <span class="obligatorio">*&nbsp;</span>');
                ?>
            </div>
        </div>
        
        <div id = "taller" style = "display: none">
            <div class="col-md-4 col-xs-12">
                <?= $form->field($model, 'combustible_estacion')->textInput(['type' => 'number',])->label('COMBUSTIBLE ACTUAL') ?>
            </div>

            <div class="col-md-4 col-xs-12">
                <?= $form->field($model, 'km_estacion')->textInput(['maxlength' => true,])->label('KM ACTUAL') ?>
            </div>
        </div>
        <div id = "cancelar" style = "display: none">
            <div class="col-md-8 col-xs-12">
                <?= $form->field($model, 'observacion_cancelado')->textArea(['maxlength' => true, 'value' => ''])->label('Motivo de Cancelación') ?>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    </br>
    </br>
    </br>
    </br>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="form-group text-center">
                <?= Html::submitButton('ENVIAR', ['class' => 'btn btn-submit']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    $('#accion').on('change', function(event) {
        event.preventDefault();
        /* Act on the event */

        if ( $(this).val() == 0 ){
            $('#taller').show();
            $('#cancelar').hide();
        } else if ( $(this).val() == 1 ){
            $('#taller').hide();
            $('#cancelar').show();
            $('#servicio-combustible_estacion').val(null);
            $('#servicio-km_estacion').val(null);
        } else{
            $('#taller').hide();
            $('#cancelar').hide();
        }
    });                 
</script>