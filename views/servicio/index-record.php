<?php

use yii\helpers\Html;
use yii\db\Query;
use kartik\grid\GridView;
use app\models\PipelineServicio;
use app\models\Empresa;
use app\models\CotizacionTallerActividadSearch;
use yii\helpers\ArrayHelper;
use app\models\Estacion;
use app\models\EstadoServicio;
use app\models\Modelo;
use app\models\Cliente;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ServicioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'HISTORIAL DE SOLICITUDES';
?>
<div class="servicio-index">

    <div class="row">
        <div class="col-md-1 col-xs-3">
            <i class="fa fa-list-ul fa-3x" aria-hidden="true"></i>
        </div>
        <div class="col-md-10 col-xs-9">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
        </div>
    </div>
    </br>
    </br>

    <div class="row">
        <div class="col-md-12 col-xs-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel'   => $searchModel,
                'tableOptions' => ['class' => 'table table-responsive', 'id' => 'grid-preventivos',],
                'summary' => '',
                'options' =>[
                    'class' => 'grid',
                ],
                'resizableColumns' => false,
                'pjax' => true,
                'columns' => [
                    [
                        'class'=>'kartik\grid\ExpandRowColumn',
                        'width'=>'50px',
                        'allowBatchToggle' => false,
                        'expandIcon' => '<i class="fa fa-caret-right" aria-hidden="true" style="color:#d9d9d9;"></i>',
                        'collapseIcon' => '<i class="fa fa-caret-down" aria-hidden="true" style="color:#d9d9d9;"></i>',
                        'value'=>function ($model, $key, $index, $column) {
                            $query = new query();
                            $query->select('es.nombre, es.color, es.codigo')
                                ->from('pipeline_servicio p')
                                ->join('INNER JOIN','estado_servicio es','es.id = p.id_estado_servicio')
                                ->where('p.id_servicio = '. $model->id )
                                ->orderBy('p.id desc');

                            $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                            $estado = Yii::$app->db->createCommand($sql)->queryOne();

                            if ( $estado['codigo'] == 10 || $estado['codigo'] == 11 ) {
                                return '<i></i>';
                            } else {
                                return GridView::ROW_COLLAPSED;
                            }
                        },
                        'detail'=>function ($model, $key, $index, $column) {
                            $pipeline_servicio = PipelineServicio::find()->where([ 'id_servicio' => $model->id ])->andWhere('id_estado_servicio != 10 AND id_estado_servicio != 11')->orderby('id DESC')->all();
                            $pipeline_servicio_first = PipelineServicio::find()->where([ 'id_servicio' => $model->id ])->orderby('id DESC')->one();
                            
                            if ( $pipeline_servicio_first->idEstadoServicio->codigo != 10 || $pipeline_servicio_first->idEstadoServicio->codigo != 11 ) {
                                if ( $pipeline_servicio_first->idEstadoServicio->codigo >= 5 && $pipeline_servicio_first->idEstadoServicio->codigo <= 9 ) {
                                    $query = new query();
                                    $query->select('c.id')
                                        ->from('solicitud_servicio_taller sst')
                                        ->join('INNER JOIN','cotizacion_taller c','c.id_solicitud_servicio_taller = sst.id AND c.estado = 1 AND c.activo = 1')
                                        ->where('sst.id_servicio = '. $model->id)
                                        ->orderBy('c.id desc');

                                    $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                                    $id_cotizacion = Yii::$app->db->createCommand($sql)->queryOne();

                                    $searchModel_cotizacion_actividad = new CotizacionTallerActividadSearch([ 'id_cotizacion_taller' => $id_cotizacion['id'] ]);
                                    $dataProvider_cotizacion_actividad = $searchModel_cotizacion_actividad->search(Yii::$app->request->queryParams);

                                    return Yii::$app->controller->renderPartial('_expand-solicitud-servicio', ['pipeline_servicio' => $pipeline_servicio, 'type' => 0, 'dataProvider_cotizacion_actividad' => $dataProvider_cotizacion_actividad, ]);
                                } else {
                                    return Yii::$app->controller->renderPartial('_expand-solicitud-servicio', ['pipeline_servicio' => $pipeline_servicio, 'type' => 0 ]);
                                }
                            } else{
                               return '';
                            }
                        },
                        'headerOptions'=>['class'=>'kartik-sheet-style'],
                        'expandOneOnly'=>true,
                        'enableRowClick' => true,
                    ],
                    [
                        'attribute' => 'id',
                        'label' => 'Número de solicitud',
                        'format' => 'text',
                        'filter' => true,
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'ficha',
                        'label' => 'MVA',
                        'format' => 'text',
                        'filter' => true,
                        'enableSorting' => true,
                    ],
                    [
                        'attribute' => 'id_modelo',
                        'label' => 'Modelo',
                        'format' => 'raw',
                        'filter' => true,
                        'enableSorting' => false,
                        'value' => function ($model, $key, $index, $column) {
                            return $model->idVehiculo->idModelo->nombre;
                        },
                        'filterType'=> GridView::FILTER_SELECT2,
                        'filter'=> ArrayHelper::map(Modelo::find()->orderBy('nombre')->all(), 'id_modelo', 'nombre'),
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                        ],
                        'filterInputOptions'=>['placeholder'=>'Selecciona'],    
                    ],
                    [
                        'attribute' => 'servicio',
                        'label' => 'Tipo de solicitud',
                        'format' => 'raw',
                        'filterType'=> GridView::FILTER_SELECT2,
                        'filter'=> ['0'=>'Mecánica Ligera','1'=>'Mecánica Especializada','2'=>'Aire Acondicionado','3'=>'Desabolladura y pintura','4'=>'Accesorios'],
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                        ],
                        'filterInputOptions'=>['placeholder'=>'Selecciona'],
                        'format'=>'raw',                   
                        'enableSorting' => false,
                        'value' => function( $model ){
                            if ( $model->servicio ) {
                                $servicios = explode("*", $model->servicio);

                                foreach ($servicios as $key => $servicio) {
                                    switch ($servicio) {
                                        case '0':
                                            $vector_servicios[] = 'Mecánica Ligera';
                                            break;

                                        case '1':
                                            $vector_servicios[] = 'Mecánica Especializada';
                                            break;

                                        case '2':
                                            $vector_servicios[] = 'Aire Acondicionado';
                                            break;

                                        case '3':
                                            $vector_servicios[] = 'Desabolladura y pintura';
                                            break;

                                        case '4':
                                            $vector_servicios[] = 'Accesorios';
                                            break;
                                        
                                        default:
                                            # code...
                                            break;
                                    }
                                    
                                }

                                $respuesta_servicios = implode('<br>', $vector_servicios);

                                return $respuesta_servicios;
                            } else{
                                return '-';
                            }
                        }
                    ],
                    [
                        'attribute' => 'id_company',
                        'label' => 'Empresa',
                        'format' => 'raw',
                        'filter' => true,
                        'enableSorting' => false,
                        'value' => function ($model, $key, $index, $column) {
                          return $model->idEstacionActual->idEmpresa->nombre;
                              },
                        'filterType'=> GridView::FILTER_SELECT2,
                        'filter'=> ArrayHelper::map(Empresa::find()->orderBy('nombre')->all(), 'id_empresa', 'nombre'),
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                        ],
                        'filterInputOptions'=>['placeholder'=>'Selecciona'],
                        'format'=>'raw',
                    ],
                    [
                        'attribute' => 'id_estacion_actual',
                        'label' => 'Estacion',
                        'format' => 'raw',
                        'filter' => true,
                        'enableSorting' => false,
                        'value' => function ($model, $key, $index, $column) {
                            return $model->idEstacionActual->nombre;
                        },
                        'filterType'=> GridView::FILTER_SELECT2,
                        'filter'=> ArrayHelper::map(Estacion::find()->orderBy('nombre')->all(), 'id_estacion', 'nombre'),
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                        ],
                        'filterInputOptions'=>['placeholder'=>'Selecciona'],
                    ], 
                    [
                        'attribute' => 'dias',
                        'format' => 'text',
                        'filter' => true,
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'estatus',
                        'label' => 'Estatus',
                        'format' => 'html',
                        'enableSorting' => true,
                        'filterType'=> GridView::FILTER_SELECT2,
                        'filter'=>ArrayHelper::map(EstadoServicio::find()->orderBy('nombre')->where('codigo = 12 OR codigo = 8')->all(), 'id', 'nombre'),
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                        ],
                        'filterInputOptions'=>['placeholder'=>'Selecciona'],
                        'format'=>'raw',
                    ],
                    [  
                        'class' => 'yii\grid\ActionColumn',
                        //'contentOptions' => ['style' => 'width:260px;'],
                        'header'=>'Acciones',
                        'template' => '{view}',
                        'buttons' => [

                            //view button
                            'view' => function ($url, $model){
                                return Html::a('<span><i class="fa fa-eye fa-sm" aria-hidden="true"></i></span>',[ '/servicio/view', 'id' => $model->id, 'origen' => 1]);
                            },
                        ],

                   ],
                ],
            ]); ?>
        </div>
    </div>

</div>

<script>
    $('.table').removeClass( "table-bordered table-striped" );
</script>
