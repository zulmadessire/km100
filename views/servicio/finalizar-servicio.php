<?php 
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\grid\GridView;
use kartik\select2\Select2;
use app\models\CotizacionTallerActividad;

$this->title = 'Solicitud de servicio';

$servicios = explode("*", $model->servicio);

foreach ($servicios as $key => $servicio) {
    switch ($servicio) {
        case '0':
            $vector_servicios[] = 'Mecánica Ligera';
            break;

        case '1':
            $vector_servicios[] = 'Mecánica Especializada';
            break;

        case '2':
            $vector_servicios[] = 'Aire Acondicionado';
            break;

        case '3':
            $vector_servicios[] = 'Desabolladura y pintura';
            break;

        case '4':
            $vector_servicios[] = 'Accesorios';
            break;
        
        default:
            # code...
            break;
    }
    
}

$respuesta_servicios = implode(' / ', $vector_servicios);

?>
<div class="view-solicitud">

    <div class="row">
        <div class="col-md-1 col-xs-3">
        	<h4 class="titulo">
                <img src="<?= Yii::$app->request->baseUrl ?>/img/car_repair.png" alt="">
        	</h4>
        </div>
        <div class="col-md-10 col-xs-9" style="padding-top: 20px;">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
        </div>
    </div>
    </br>
    </br>
    </br>

    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right">
            </div>
        </div>
        </br>
    </div>

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DEL VEHÍCULO</p>
            <hr>
        </div>
    </div>
    <div class="row">
    	<div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'mva')->textInput(['maxlength' => true, 'disabled' => true])->label('Ficha o MVA') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'placa')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_modelo')->textInput(['value' => strtoupper($model_vehiculo->idModelo->nombre), 'disabled' => true])->label('Modelo') ?>
        </div>
    </div>

    <br>
    <br>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DE LA SOLICITUD</p>
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'id')->textInput(['maxlength' => true, 'disabled' => true,])->label('NÚMERO DE SOLICITUD') ?>
        </div>
	
		<div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'servicio')->textInput(['maxlength' => true, 'disabled' => true, 'value' => $respuesta_servicios ])->label('TIPO DE SOLICITUD') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'responsable')->textInput(['maxlength' => true, 'disabled' => true,])->label('TÉNICO ENCARGADO') ?>
        </div>

        <div class="col-md-8 col-xs-12">
        	<?= $form->field($model, 'evaluacion_tecnica')->textarea(['rows' => 3, 'disabled' => true,])->label('EVALUACIÓN TÉCNICA PRELIMINAR') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DETALLES DE LA REPARACIÓN</p>
            <hr>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'combustible_taller')->textInput(['type' => 'number',])->label('COMBUSTIBLE ACTUAL') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'km_taller')->textInput(['maxlength' => true,])->label('KM ACTUAL') ?>
        </div>
        <div class="col-md-4 col-xs-12">


            <?=
                $form->field($model, 'entrega_estacion')->widget(Select2::classname(), [
                    'data' => ['' => 'Seleccione', '1' => 'Si', '2' => 'No' ],
                    'options' => ['placeholder' => 'Seleccione', 'id' => 'entrega_estacion'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Entrega a estación');
            ?>
        </div>
        <div class="col-xs-12">
             <div>
                <?= $form->field($model, 'observacion_finalizado')->textArea(['maxlength' => true, 'class' => 'form-control'])->label('Observaciones') ?>
            </div>
        </div>
    </div>  
    <br>
    <br>    
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="form-group text-center">
                <?= Html::submitButton('FINALIZAR', ['class' => 'btn btn-submit']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
<script>
    $('.table').removeClass( "table-bordered table-striped" );
</script>