<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\db\Query;
use app\models\PipelineServicio;
use app\models\Empresa;
use yii\helpers\ArrayHelper;
use app\models\Estacion;
use kartik\select2\Select2;
use app\models\EstadoServicio;
use app\models\CotizacionTaller;


/* @var $this yii\web\View */
/* @var $searchModel app\models\PartesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'LISTADO DE SOLICITUDES';
?>
<div class="gerente-index">

    <div class="row">
        <div class="col-md-1 col-xs-3">
            <i class="fa fa-list-ul fa-3x" aria-hidden="true"></i>
        </div>
        <div class="col-md-10 col-xs-9">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
        </div>
    </div>
    </br>
    </br>
    
    <div class="row">
        <div class="col-xs-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel'   => $searchModel,
                'tableOptions' => ['class' => 'table table-responsive'],
                'summary' => '',
                'options' =>[
                    'class' => 'grid',
                ],
                'columns' => [
                    [
                        'attribute' => 'id',
                        'label' => 'Número de solicitud',
                        'format' => 'text',
                        'filter' => true,
                        'enableSorting' => false,
                        'headerOptions' => ['width' => '100',],
                    ],
                    [
                        'attribute' => 'servicio',
                        'label' => 'Tipo de solicitud',
                        'format' => 'raw',
                        //'filter' => false,
                        'filter' => ['0'=>'Mecánica Ligera','1'=>'Mecánica Especializada','2'=>'Aire Acondicionado','3'=>'Desabolladura y pintura','4'=>'Accesorios'],                        
                        'enableSorting' => false,
                        'value' => function( $model ){
                            if ( $model->servicio ) {
                                $servicios = explode("*", $model->servicio);

                                foreach ($servicios as $key => $servicio) {
                                    switch ($servicio) {
                                        case '0':
                                            $vector_servicios[] = 'Mecánica Ligera';
                                            break;

                                        case '1':
                                            $vector_servicios[] = 'Mecánica Especializada';
                                            break;

                                        case '2':
                                            $vector_servicios[] = 'Aire Acondicionado';
                                            break;

                                        case '3':
                                            $vector_servicios[] = 'Desabolladura y pintura';
                                            break;

                                        case '4':
                                            $vector_servicios[] = 'Accesorios';
                                            break;
                                        
                                        default:
                                            # code...
                                            break;
                                    }
                                    
                                }

                                $respuesta_servicios = implode('<br>', $vector_servicios);

                                return $respuesta_servicios;
                            } else{
                                return '-';
                            }
                        }
                    ],
                    [
                        'label' => 'Empresa',
                        'format' => 'raw',
                        'filter' => true,
                        'enableSorting' => false,
                        'value' => function( $model ){
                            if ( $model ) {
                                return Empresa::findOne($model->idEstacionActual->id_empresa)->nombre;
                            } else{
                                return '-';
                            }
                        },
                        
                        'filter' => Select2::widget([
                        'name' => 'ServicioSearch[id_company]',
                        'data' => ArrayHelper::map(Empresa::find()->orderBy('nombre')->all(), 'id_empresa', 'nombre'),
                        'hideSearch' => true,
                        'options' => ['placeholder' => 'Empresa',],
                        'pluginOptions' => [
                              'allowClear' => true
                          ],
                      ]),
                       
                    ],
                    [
                        'label' => 'Estacion',
                        'format' => 'raw',
                        'filter' => true,
                        'enableSorting' => false,
                        'value' => function ($model, $key, $index, $column) {
                            return $model->idEstacionActual->nombre;
                              },                        
                        'filter' => Select2::widget([
                        'name' => 'ServicioSearch[id_estacion]',
                        'data' => ArrayHelper::map(Estacion::find()->orderBy('nombre')->all(), 'id_estacion', 'nombre'),
                        'hideSearch' => true,
                        'options' => ['placeholder' => 'Estacion',],
                        'pluginOptions' => [
                              'allowClear' => true
                          ],
                      ]),
                       
                    ],
                    [
                        'attribute' => 'estatus',
                        'label' => 'Estatus',
                        'format' => 'html',
                        'enableSorting' => true,
                        'filterType'=> GridView::FILTER_SELECT2,
                        'filter'=>ArrayHelper::map(EstadoServicio::find()->orderBy('nombre')->all(), 'id', 'nombre'),
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                        ],
                        'filterInputOptions'=>['placeholder'=>'Selecciona'],
                        'format'=>'raw',
                    ],
                    [  
                        'class' => 'yii\grid\ActionColumn',
                        'header'=>'Acciones',
                        'template' => '{asignar}',
                        'buttons' => [
                            //asignar button
                            'asignar' => function ($url, $model) {
                                return  (( $model->aprobacion_estacion == -1 )?Html::a('<span class="fa-stack fa-lg">
                                        <i class="fa fa-circle fa-stack-2x" style="color:#e79e2d;"></i>
                                        <i class="fa fa-wrench fa-stack-1x fa-inverse"></i>
                                      </span>',
                                      ['/servicio/asignartaller', 'id' => $model->id ], ['title' => 'Asignar taller a esta solicitud']):'');
                            },
                        ],

                    ],
                ],
            ]); ?>
        </div>
        <?php


        ?>
    </div>
</div>


<script>
    $('.table').removeClass( "table-bordered table-striped" );
</script>