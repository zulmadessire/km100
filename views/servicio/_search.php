<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ServicioSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="servicio-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'prioridad') ?>

    <?= $form->field($model, 'dano') ?>

    <?= $form->field($model, 'img_reporte') ?>

    <?= $form->field($model, 'img_acta') ?>

    <?php // echo $form->field($model, 'servicio') ?>

    <?php // echo $form->field($model, 'evaluacion_tecnica') ?>

    <?php // echo $form->field($model, 'responsable') ?>

    <?php // echo $form->field($model, 'fecha') ?>

    <?php // echo $form->field($model, 'ficha') ?>

    <?php // echo $form->field($model, 'placa') ?>

    <?php // echo $form->field($model, 'id_estacion_actual') ?>

    <?php // echo $form->field($model, 'id_vehiculo') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
