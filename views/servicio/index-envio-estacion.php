<?php

use yii\helpers\Html;
use yii\db\Query;
use kartik\grid\GridView;
use app\models\PipelineServicio;
use app\models\Empresa;
use app\models\CotizacionTallerActividadSearch;
use app\models\Cliente;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Estacion;
use app\models\EstadoServicio;
use app\models\Modelo;
use app\models\Marca;

$this->title = 'LISTADO DE VEHÍCULOS POR ENVIAR A TALLER';
?>
<div class="envio-taller-index">

    <div class="row">
        <div class="col-md-1 col-xs-3">
            <i class="fa fa-list-ul fa-3x" aria-hidden="true"></i>
        </div>
        <div class="col-md-10 col-xs-9">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
        </div>
    </div>
    </br>
    </br>

    <div class="row">
        <div class="col-md-12 col-xs-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel'   => $searchModel,
                'tableOptions' => ['class' => 'table table-responsive', 'id' => 'grid-preventivos',],
                'layout' => "{items}\n<div align='center'>{pager}</div>",
                'summary' => '',
                'options' =>[
                    'class' => 'grid',
                ],
                'resizableColumns' => false,
                'columns' => [
                    [
                        'attribute' => 'id',
                        'label' => 'Número de solicitud',
                        'format' => 'text',
                        'filter' => true,
                        'enableSorting' => false,
                        'headerOptions' => ['width' => '150',],
                    ],
                    [
                        'attribute' => 'ficha',
                        'label' => 'MVA',
                        'format' => 'text',
                        'filter' => true,
                        'enableSorting' => true,
                        'headerOptions' => ['width' => '100',],
                    ],
                    [
                        'attribute' => 'id_marca',
                        'label' => 'Marca',
                        'format' => 'raw',
                        'filter' => true,
                        'enableSorting' => false,
                        'value' => function ($model, $key, $index, $column) {
                            return $model->idVehiculo->idModelo->idMarca->nombre;
                        },
                        'filterType'=> GridView::FILTER_SELECT2,
                        'filter'=> ArrayHelper::map(Marca::find()->orderBy('nombre')->all(), 'id_marca', 'nombre'),
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                        ],
                        'filterInputOptions'=>['placeholder'=>'Selecciona'],    
                    ],
                    [
                        'attribute' => 'id_modelo',
                        'label' => 'Modelo',
                        'format' => 'raw',
                        'filter' => true,
                        'enableSorting' => false,
                        'value' => function ($model, $key, $index, $column) {
                            return $model->idVehiculo->idModelo->nombre;
                        },
                        'filterType'=> GridView::FILTER_SELECT2,
                        'filter'=> ArrayHelper::map(Modelo::find()->orderBy('nombre')->all(), 'id_modelo', 'nombre'),
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                        ],
                        'filterInputOptions'=>['placeholder'=>'Selecciona'],    
                    ],
                    [
                        'attribute' => 'servicio',
                        'label' => 'Tipo de solicitud',
                        'format' => 'raw',
                        'filterType'=> GridView::FILTER_SELECT2,
                        'filter'=> ['0'=>'Mecánica Ligera','1'=>'Mecánica Especializada','2'=>'Aire Acondicionado','3'=>'Desabolladura y pintura','4'=>'Accesorios'],
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                        ],
                        'filterInputOptions'=>['placeholder'=>'Selecciona'],
                        'format'=>'raw',                   
                        'enableSorting' => false,
                        'value' => function( $model ){
                            if ( $model->servicio ) {
                                $servicios = explode("*", $model->servicio);

                                foreach ($servicios as $key => $servicio) {
                                    switch ($servicio) {
                                        case '0':
                                            $vector_servicios[] = 'Mecánica Ligera';
                                            break;

                                        case '1':
                                            $vector_servicios[] = 'Mecánica Especializada';
                                            break;

                                        case '2':
                                            $vector_servicios[] = 'Aire Acondicionado';
                                            break;

                                        case '3':
                                            $vector_servicios[] = 'Desabolladura y pintura';
                                            break;

                                        case '4':
                                            $vector_servicios[] = 'Accesorios';
                                            break;
                                        
                                        default:
                                            # code...
                                            break;
                                    }
                                    
                                }

                                $respuesta_servicios = implode('<br>', $vector_servicios);

                                return $respuesta_servicios;
                            } else{
                                return '-';
                            }
                        }
                    ],
                    [
                        'attribute' => 'taller',
                        'label' => 'Taller Asignado',
                        'format' => 'text',
                        'enableSorting' => true,
                        'filterType'=> GridView::FILTER_SELECT2,
                        'filter'=> $talleres,
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                        ],
                        'filterInputOptions'=>['placeholder'=>'Selecciona'],
                    ],
                    [  
                        'class' => 'yii\grid\ActionColumn',
                        'header'=>'Acciones',
                        'template' => '{view}',
                        'buttons' => [

                            //view button
                            'view' => function ($url, $model){
                                return Html::a('<span><i class="fa fa-eye fa-sm" aria-hidden="true"></i></span>',[ '/servicio/enviotaller', 'id' => $model->id]);
                            },
                        ],

                   ],
                ],
            ]); ?>
        </div>
    </div>

</div>

<script>
    $('.table').removeClass( "table-bordered table-striped" );
</script>