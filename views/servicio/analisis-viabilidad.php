<?php 
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\form\ActiveForm;
use kartik\grid\GridView;
use kartik\select2\Select2;
use app\models\CotizacionTallerActividad;
use app\models\User;
use app\models\Taller;

$this->title = 'ANÁLISIS DE VIABILIDAD';

$servicios = explode("*", $model->servicio);

foreach ($servicios as $key => $servicio) {
    switch ($servicio) {
        case '0':
            $vector_servicios[] = 'Mecánica Ligera';
            break;

        case '1':
            $vector_servicios[] = 'Mecánica Especializada';
            break;

        case '2':
            $vector_servicios[] = 'Aire Acondicionado';
            break;

        case '3':
            $vector_servicios[] = 'Desabolladura y pintura';
            break;

        case '4':
            $vector_servicios[] = 'Accesorios';
            break;
        
        default:
            # code...
            break;
    }
    
}

$respuesta_servicios = implode(' / ', $vector_servicios);

/********* Ciclo de vida ****************/

$seguimiento_km = $model_vehiculo->idCategoria->seguimiento_km;
$declive_km = $model_vehiculo->idCategoria->declive_km;
$porcentaje_declive_km = ((floatval($model_vehiculo->km) - floatval($seguimiento_km) ) * 100) / (floatval($declive_km) - floatval($seguimiento_km));


$date1 = new DateTime($model_vehiculo->fecha_inicial);
$interval = $date1->diff(new DateTime("now"));
$anos = number_format(($interval->days / 365), 2, '.', '');

$seguimiento_fecha = $model_vehiculo->idCategoria->seguimiento_fecha;
$declive_fecha = $model_vehiculo->idCategoria->declive_fecha;
$porcentaje_declive_fecha = ( ($anos - $seguimiento_fecha) * 100) / floatval($declive_fecha);

if ( $porcentaje_declive_km > 0 ){
    $ciclo = 'Declive por Km';
} elseif ( $porcentaje_declive_fecha > 0 ) {
    $ciclo = 'Declive po tiempo de vida';
} else {
    $ciclo = 'Seguimiento';
}

if ( $model_taller->tipo_servicio_taller == 0 ) {
    $tipo_servicio_taller = 'Desabolladura y Pintura';
} elseif ( $model_taller->tipo_servicio_taller == 1 ) {
    $tipo_servicio_taller = 'Mecánica';
} elseif ( $model_taller->tipo_servicio_taller == 2 ) {
    $tipo_servicio_taller = 'Desabolladura y Pintura / Mecánica';
}

/********* Fin ciclo de vida ****************/

?>
<div class="analisis-viabilidad">

    <div class="row">
        <div class="col-md-1 col-xs-3">
        	<h4 class="titulo">
        		<i class="fa fa-bar-chart fa-4x" aria-hidden="true"></i>
        	</h4>
        </div>
        <div class="col-md-10 col-xs-9" style="padding-top: 20px;">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
        </div>
    </div>
    </br>
    </br>

    <div class="row">
        <div class="col-xs-12 text-center">
            <div class="col-xs-3" style="background-color: #deebf7; left:0; margin:0 auto; float: none; padding: 5px;">
                Sugerencia: <b>NEGOCIAR</b>
            </div>
        </div>
    </div>
    </br>

    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right">
                <?php 
                    if (Yii::$app->user->can('admin')){
                        echo Html::a('Listado de solicitudes',['/servicio/gestionservicio'], ['class' => 'btn btn-small-gris']);
                    } elseif (Yii::$app->user->can('analista')){
                        echo Html::a('Listado de solicitudes',['/servicio/indexanalista'], ['class' => 'btn btn-small-gris']);
                    } elseif (Yii::$app->user->can('gerente-tecnico')){
                        echo Html::a('Asignar Analista',['#'], ['class' => 'btn btn-small-gris', 'data' => ['id' => $model->id, 'toggle' => 'modal', 'target' => '#addAnalistaModal']]);
                        echo "&nbsp;";
                        echo Html::a('Listado de solicitudes',['/servicio/indexgerente'], ['class' => 'btn btn-small-gris']);
                    } elseif (Yii::$app->user->can('director-comercial')){
                        echo Html::a('Listado de solicitudes',['/servicio/indexgerente'], ['class' => 'btn btn-small-gris']);
                    }
                ?>
            </div>
        </div>
        </br>
    </div>

    <?php $form = ActiveForm::begin([ 'id' => 'analisis-viabilidad-form', 'action' => ['/servicio/guardaranalisisviabilidad',] ]); ?>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DEL VEHÍCULO</p>
            <hr>
        </div>
    </div>
    <div class="row">
    	<div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_modelo')->textInput(['disabled' => true, 'value' => $model_vehiculo->idModelo->idMarca->nombre ])->label('Marca') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_modelo')->textInput(['value' => strtoupper($model_vehiculo->idModelo->nombre), 'disabled' => true])->label('Modelo') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'mva')->textInput(['maxlength' => true, 'disabled' => true])->label('Ficha o MVA') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'chasis')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'placa')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_categoria')->textInput(['maxlength' => true, 'disabled' => true, 'value' => $model_vehiculo->idCategoria->nombre ])->label('Categoria') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'km')->textInput(['maxlength' => true, 'disabled' => true, 'value' => number_format(floatval($model_vehiculo->km), 0, ',', '.'). ' Km', ])->label('Km Actual') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <div class="form-group">
			    <label class="control-label">HISTÓRICO DE COSTO</label>
			    <div class="input-group">
			      <div class="input-group-addon">RD$</div>
			      <input type="text" class="form-control" disabled="true">
			    </div>
			 </div>
        </div>

        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <?= Html::label('ETAPA DE CICLO DE VIDA', '', ['class' => 'control-label']) ?>
                <?= Html::textInput('', $ciclo,['class' => 'form-control', 'disabled' => true, ]) ?>
            </div>
        </div>

        <div class="col-md-4 col-xs-12">
            <div class="form-group">
			    <label class="control-label">ÚLTIMO AVALÚO</label>
			    <div class="input-group">
			      <div class="input-group-addon">RD$</div>
			      <input type="text" class="form-control" disabled="true">
			    </div>
			 </div>
        </div>

        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <?= Html::label('FACTOR DE SINIESTRALIDAD', '', ['class' => 'control-label']) ?>
                <?= Html::textInput('', '',['class' => 'form-control',]) ?>
            </div>
        </div>

        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <?= Html::label('UTILIZACIÓN PROMEDIO', '', ['class' => 'control-label']) ?>
                <?= Html::textInput('', '',['class' => 'form-control',]) ?>
            </div>
        </div>

        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <?= Html::label('UTILIZACIÓN NECESARIA', '', ['class' => 'control-label']) ?>
                <?= Html::textInput('', '',['class' => 'form-control',]) ?>
            </div>
        </div>
    </div>

    <br>
    <br>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DE LA SOLICITUD</p>
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'id')->textInput(['maxlength' => true, 'disabled' => true,])->label('NÚMERO DE SOLICITUD') ?>
        </div>
	
		<div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'servicio')->textInput(['maxlength' => true, 'disabled' => true, 'value' => $respuesta_servicios ])->label('TIPO DE SOLICITUD') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'linea')->textInput(['maxlength' => true, 'disabled' => true, 'value' => ($model->linea == 0)?'Quick Line':'Normal Line',])->label('LÍNEA') ?>
        </div>

        <div class="col-md-4 col-xs-12">
        	<?= $form->field($model, 'evaluacion_tecnica')->textarea(['rows' => 3, 'disabled' => true,])->label('EVALUACIÓN TÉCNICA PRELIMINAR') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DEL TALLER</p>
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_taller, 'nombre')->textInput(['maxlength' => true, 'disabled' => true,])->label('TALLER') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_taller, 'telefono')->textInput(['maxlength' => true, 'disabled' => true,])->label('TELÉFONO') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_taller, 'direccion')->textInput(['maxlength' => true, 'disabled' => true,])->label('DIRECIÓN') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_taller, 'tipo_servicio_taller')->textInput(['maxlength' => true, 'disabled' => true, 'value' => $tipo_servicio_taller,])->label('SERVICIOS TALLER') ?>
        </div>
    </div>

    </br>
    </br>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">CASOS ASOCIADOS</p>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= GridView::widget([
                'dataProvider' => $dataProvider_caso,
                'summary' => '',
                'tableOptions' => ['class' => 'table table-responsive'],
                'options' =>[
                    'class' => 'grid',
                ],
                'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                'columns' => [
                    [
                        'attribute' => 'id_caso',
                        'label' => 'Número de caso',
                        'format' => 'text',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:center;'],
                    ],
                    [
                        'attribute' => 'tipo_caso',
                        'label' => 'Tipo de caso',
                        'format' => 'text',
                        'value' => function($model){
                            if ( $model->idCaso->tipo_caso ){
                                if ( $model->idCaso->tipo_caso == 0) {
                                    return 'Accidente / Incidente';
                                } elseif ( $model->idCaso->tipo_caso == 1 ) {
                                    return 'Aire acondicionado';
                                } elseif ( $model->idCaso->tipo_caso == 2 ) {
                                    return 'Falla mecánica';
                                }
                            }
                            else
                                return '';
                        },
                        'contentOptions'=>['style'=>'text-align:center;'],
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view}',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a('<i class="fa fa-eye" aria-hidden="true"></i>', ['/caso/view', 'id' => $model->id_caso], ['title' => 'Ver Caso']);
                            },
                        ],
                        'contentOptions'=>['style'=>'text-align:center;'],
                    ],
                ],
            ]); ?>
        </div>
    </div>
    </br>
    </br>
    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">CHECKLIST AVALUO</p>
            <hr>
            <?php
                if($checklist_avaluo) {
                    if($checklist_avaluo->id_analista == null){
                        echo '
                        <div class="alert alert-info alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-info"></i> Alerta!</h4>
                            No se ha asignado analista.
                        </div>
                        ';
                    }else {
                        if($checklist_avaluo->diagnostico == null) {
                            echo '
                            <div class="alert alert-info alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h4><i class="icon fa fa-info"></i> Alerta!</h4>
                                El Analista no ha completado el checklist de avaluo.
                            </div>
                            ';  
                        }else {
                            echo 'Usuario que realizó el avaluo:  ' . $checklist_avaluo->idUser->username; 
                            echo '<br><br>';
                            echo Html::a('<i class="fa fa-eye" aria-hidden="true"></i> checklist avaluo', ['analista-mantenimiento/verchecklist', 'id' => $checklist_avaluo->id,], ['class' => 'btn btn-small-gris',]);
                            echo '<br><br>';
                        }
                    }
                }else {
                    echo '
                    <div class="alert alert-info alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-info"></i> Alerta!</h4>
                        No se ha asignado analista.
                    </div>
                    ';
                }
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">COSTO DE MANTENIMIENTO</p>
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-xs-12">
            <p style="font-size: 11pt;"><b><?= Html::encode('COSTO DE MANO DE OBRA') ?></b></p>
        </div>
    	<div class="col-md-12 col-xs-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider_cotizacion_actividad,
                'summary' => '',
                'id' =>'cotizacion-actividad-grid',
                'pjax' => true,
                'showFooter' => true,
                'tableOptions' => ['class' => 'table table-responsive', 'id' =>'cotizacion-actividad-table'],
                'options' =>[
                    'class' => 'grid-blue',
                ],
                'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                'afterFooter' => '<tr class="kv-table-footer"><td></td><td></td><td></td><td></td><td>ITBIS</td><td style="text-align: center;">'.number_format(floatval( CotizacionTallerActividad::getItbis2($dataProvider_cotizacion_actividad->models)), 2, ',', '.') . ' RD$'.'</td></tr><tr class="kv-table-footer"><td></td><td></td><td></td><td></td><td>TOTAL</td><td style="text-align: center;">'.number_format(floatval( CotizacionTallerActividad::getTotal2($dataProvider_cotizacion_actividad->models)), 2, ',', '.') . ' RD$'.'</td></tr>',
                'columns' => [
                    [
                         
                        'label' => 'Servicio',
                        'format' => 'text',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:left;'],
                        //'headerOptions' => ['style' => 'width:80%'],
                        'footer' => '',
                        'value' => function($model){
                            if ($model->id_pieza_servicio_taller) {
                                return $model->piezaServicioTaller->servicioTaller->nombre;
                            }
                            else{
                                return '-';
                            }
                        },
                    ],
                    [
                         
                        'label' => 'Descripción de la Actividad',
                        'format' => 'text',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:left;'],
                        //'headerOptions' => ['style' => 'width:80%'],
                        'footer' => '',
                        'value' => function($model){
                            if ($model->id_pieza_servicio_taller) {
                                return $model->piezaServicioTaller->nombre;
                            }
                            else{
                                return '-';
                            }
                        },
                    ],
                    [
                        'label' => 'Labor',
                        'format' => 'text',
                        'enableSorting' => false,
                        'footer' => '',
                        'value' => function($model){
                            if ($model->labor == 0) {
                                return 'Reparación';
                            } elseif ( $model->labor == 1 ){
                                return 'Salvado Leve';
                            } elseif ( $model->labor == 2 ){
                                return 'Salvado Fuerte';
                            } else{
                                return '-';
                            }
                        },
                    ],
                    [
                        'attribute' => 'precio',
                        'label' => 'Precio',
                        'format' => 'text',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'value' => function($model){
                            if ($model->precio) {
                                return number_format(floatval($model->precio), 2, ',', '.') . ' RD$';
                            } else{
                                return '-';
                            }
                        },
                        'footer' => '',
                    ],
                    [
                        'label' => 'Decimal',
                        'format' => 'text',
                        'enableSorting' => false,
                        'footer' => 'SUBTOTAL',
                        'value' => function($model){
                            if ($model->decimal) {
                                return $model->decimal;
                            } else{
                                return '-';
                            }
                        },
                    ],
                    [
                        'label' => 'Total',
                        'format' => 'text',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'value' => function($model){
                            if ($model->total_servicio ) {
                                return number_format(floatval(  $model->total_servicio ), 2, ',', '.') . ' RD$';
                            } else{
                                return '0';
                            }
                        },
                        'footer' => number_format(floatval( CotizacionTallerActividad::getSubTotal2($dataProvider_cotizacion_actividad->models)), 2, ',', '.') . ' RD$',
                    ],
                ],
            ]); ?>
        </div>
        </br>
        </br>

        <div class="col-md-12 col-xs-12">
            <p style="font-size: 11pt;"><b><?= Html::encode('COSTO DE PIEZA') ?></b></p>
        </div>
        
        <?php if ($dataProvider_cotizacion_pieza->getTotalCount() > 0): ?>
            <div class="col-md-12 col-xs-12">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider_cotizacion_pieza,
                    'summary' => '',
                    'id' =>'cotizacion-pieza-grid',
                    'pjax' => true,
                    'showFooter' => true,
                    'tableOptions' => ['class' => 'table table-responsive', 'id' =>'cotizacion-pieza-table'],
                    'options' =>[
                        'class' => 'grid-blue',
                    ],
                    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                    'afterFooter' => '<tr class="kv-table-footer"><td></td><td></td><td>ITBIS</td><td style="text-align: center;">'.number_format(floatval( 0 ), 2, ',', '.') . ' RD$'.'</td></tr><tr class="kv-table-footer"><td></td><td></td><td>TOTAL</td><td style="text-align: center;">'.number_format(floatval( 0 ), 2, ',', '.') . ' RD$'.'</td></tr>',
                    'columns' => [
                        [
                            'attribute' => 'idPieza.nombre',
                            'label' => 'Descripcion',
                            'format' => 'text',
                            'enableSorting' => false,
                            'contentOptions'=>['style'=>'text-align:left;'],
                            'headerOptions' => ['width' => '510',],
                        ],
                        [
                            'attribute' => 'cantidad',
                            'label' => 'Cantidad',
                            'format' => 'text',
                            'enableSorting' => false,
                            'contentOptions'=>['style'=>'text-align:center;'],
                        ],
                        [
                            'attribute' => 'precio',
                            'label' => 'Precio',
                            'format' => 'text',
                            'enableSorting' => false,
                            'contentOptions'=>['style'=>'text-align:center;'],
                            'value' => function($model){
                                return 0 . ' RD$';
                            },
                            'footer' => 'SUBTOTAL',
                        ],
                        [
                            'label' => 'Total',
                            'format' => 'text',
                            'enableSorting' => false,
                            'contentOptions'=>['style'=>'text-align:center;'],
                            'value' => function($model){
                                return 0 . ' RD$';
                            },
                            'footer' => number_format(floatval( 0 ), 2, ',', '.') . ' RD$',
                        ],
                    ],
                ]); ?>
            </div>
        <?php else: ?>
            <div class="col-md-12 col-xs-12">
                <div class="alert alert-info alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-info"></i> Información!</h4>
                    El Taller no solicito piezas.
                </div>
            </div>
        <?php endif ?>
    	
        <!--Link de solicitud de cotización de pieza-->
        <?php if( $model->solicita_cotizacion != NULL || $model->solicita_cotizacion != 1 ){ ?>
            <div class="clearfix"></div>
            </br>
            </br>
            <div class="col-md-12 col-xs-12" style="display: none;">
                <div class="pull-right">
                    <?= Html::a('Solicitar cotización de piezas','#', ['class' => 'btn btn-small-gris', 'data' => ['toggle' => 'modal', 'target' => '#solicitarCotizacionModal']]) ?>
                </div>
            </div>
        <?php } ?>
    </div>
    </br>
    </br>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DECISIÓN / OBSERVACIONES</p>
            <hr>
        </div>
    </div>

    <?php if ($model_cotizacion_taller->activo == 2 && $model->decision == 4 ): ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="alert alert-info alert-dismissible">
                    <h4><i class="icon fa fa-info"></i> KM100!</h4>
                    <p>La Solicitud ha sido enviada a Salvamento por el Gerente Técnico</p>
                    <?= $model->observacion_gerente ?>
                </div>
            </div>
        </div>
        </br>
    <?php endif ?>
    <div class="row">
        <?php foreach ($mensajes as $key => $mensaje): ?>
            <div class="col-xs-12">
                <div class="media">
                    <div class="media-body">
                        <h5 class="media-heading"><strong> <?= ( isset($mensaje->user->id_taller) && $mensaje->user->id_taller != 0 )?ucfirst(Taller::findOne($mensaje->user->id_taller)->nombre):'KM100' ?> </strong> (<?= $mensaje->user->username ?>)</h5>
                        <p> <?= $mensaje->mensaje ?> </p>
                        <small> <?= $mensaje->fecha ?> </small>
                    </div>
                </div>
                </br>
            </div>
        <?php endforeach ?>
    </div>
    </br>
    </br>
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <?php
            $user = '';
                if (Yii::$app->user->can('analista')) {
                    $user = 0;
                    $data = ['1' => 'Reparar', '2' => 'Renegociar con taller', /*'3' => 'Reasignar Taller'*/];
                } elseif ( Yii::$app->user->can('gerente-tecnico') ) {
                    $user = 1;
                    $data = ['1' => 'Reparar', '2' => 'Renegociar con taller', /*'3' => 'Reasignar Taller',*/ '4' => 'Salvamento' ];
                } elseif ( Yii::$app->user->can('director-comercial') ){
                    $user = 2;
                    $data = ['1' => 'Reparar', '2' => 'Renegociar con taller', /*'3' => 'Reasignar Taller',*/ '4' => 'Salvamento', /*'5' => 'Cancelar Solicitud'*/];
                }
            ?>
            <?=
                $form->field($model, 'decision')->widget(Select2::classname(), [
                    'data' => $data,
                    'options' => ['value' => '','placeholder' => 'Selecciona la acción', 'id' => 'op'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('ACCIÓN');
            ?>
        </div>
        <div class="col-md-8 col-xs-12" >
            <?= $form->field($model_servicio_mensaje, 'mensaje')->textArea(['maxlength' => true, 'rows' => 3])->label('OBSERVACIÓN') ?>
        </div>
        <div id="rechazar" class="col-md-8 col-xs-12" style="display: none;" >
            <?= $form->field($model_cotizacion_taller, 'observacion_cancela')->textArea(['maxlength' => true, 'value' => ''])->label('Motivo de Renegociación') ?>
        </div>
        <div id="observacion-gerente" class="col-md-8 col-xs-12" style="display: none;" >
            <?= $form->field($model, 'observacion_gerente')->textArea(['maxlength' => true,])->label('OBSERVACIÓN') ?>
        </div>
        <div id="observacion-director" class="col-md-8 col-xs-12" style="display: none;" >
            <?= $form->field($model, 'observacion_director')->textArea(['maxlength' => true,])->label('OBSERVACIÓN') ?>
        </div>
    </div>
    </br>
    </br>
    </br>
    </br>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="form-group text-center">
                <?= Html::submitButton('ENVIAR', ['class' => 'btn btn-submit']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php if (Yii::$app->user->can('admin') || Yii::$app->user->can('gerente-tecnico')): ?>
<!-- Modal Asociar Analista -->
<div id="addAnalistaModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="title"><i class="fa fa-user fa-lg" aria-hidden="true"></i>&nbsp;&nbsp; ASIGNAR ANALISTA DE MTTO.</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true" style="color: #000; font-weight: bold;"></i></button>
          </div>
        <div class="modal-body">
            </br>
            <?php $form = ActiveForm::begin([ 'id' => 'analista', 'action' => ['/servicio/asignaranalista'] ]); ?>
                <div class="row">
                    <div class="text-center">
                        <div class="col-xs-12 col-md-8 col-md-offset-2">
                            <label class="control-label" for="garantiavehiculo-motivo">ANALISTA A ASIGNAR</label>
                            <?=
                                Select2::widget([
                                    'model' => $add_analista_checklist_avaluo,
                                    'attribute' => 'id_analista',
                                    'data' => ArrayHelper::map(User::find()->joinWith('authAssignments')->where('auth_assignment.item_name = "analista"')->all(), 'id', 'username'),
                                    'options' => ['placeholder' => 'Selecciona el analista'],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ]);
                            ?>
                        </div>
                    </div>
                </div>

            <?php ActiveForm::end(); ?>
            </br>
            </br>
            </br>
            </br>
            </br>
            <?php
            echo '<div class="row" style="padding-top: 25px;">';
                echo '<div class="text-center">';
                    echo Html::button('CANCELAR', ['class' => 'btn btn-cancelar', 'data' => ['dismiss' => 'modal']]);
                    echo "&nbsp;";
                    echo Html::submitButton('REGISTRAR', ['class' => 'btn btn-aceptar', 'form' => 'analista', 'id' => 'submit-analista' ]);
                echo '</div>';
            echo '</div>';
            ?>
        </div>
    </div>
  </div>
</div>
<!-- Fin modal-->
<?php endif ?>

<!-- Modal Solicitar Cotizacion -->
<div id="solicitarCotizacionModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="title"><i class="fa fa-gears fa-lg" aria-hidden="true"></i>&nbsp;&nbsp; SOLICITAR COIZACIÓN DE PIEZAS</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true" style="color: #000; font-weight: bold;"></i></button>
          </div>
        <div class="modal-body">
            </br>
            <?php $form = ActiveForm::begin([ 'id' => 'solicita-cotizacion', 'action' => ['/servicio/solicitudcotizacionpiezas', 'id' => $model->id ] ]); ?>
                <div class="row">
                    <div class="text-center">
                        <div class="col-xs-12 col-md-8 col-md-offset-2">
                            <h4>¿DESEA SOLICITAR LA COTIZACIÓN DE LAS PIEZAS REQUERIDAS POR EL TALLER AL DEPARTAMENTO DE COMPRAS DE KM100?</h4>
                        </div>
                    </div>
                </div>

            <?php ActiveForm::end(); ?>
            </br>
            <?php
            echo '<div class="row" style="padding-top: 25px;">';
                echo '<div class="text-center">';
                    echo Html::button('CANCELAR', ['class' => 'btn btn-cancelar', 'data' => ['dismiss' => 'modal']]);
                    echo "&nbsp;";
                    echo Html::submitButton('SOLICITAR', ['class' => 'btn btn-aceptar', 'form' => 'solicita-cotizacion',]);
                echo '</div>';
            echo '</div>';
            ?>
        </div>
    </div>
  </div>
</div>

<?= Html::hiddenInput('name', $user, ['id' =>'user']);   ?>
<!-- Fin modal-->

<style>
	#cotizacion-actividad-grid tfoot > tr > td, #cotizacion-pieza-grid tfoot > tr > td {
		text-align: right;
	}

	#cotizacion-actividad-grid tfoot > tr > td:nth-child(6), #cotizacion-pieza-grid tfoot > tr > td:nth-child(4){
	    text-align: center;
	    font-weight: initial;
	}
    .view-cotizacion-taller .dropdown-menu {
        right: 0;
        left: unset;
    }
</style>

<script>
    /*$('#op').on('change', function(event) {

        var usuario = $('#user').val();
        console.log(usuario);

        if($(this).val() == 1 || $(this).val() == 3){
            //$("#send").show();            
            $("#rechazar").hide();
            $("#observacion-gerente").hide();                    
            $("#observacion-director").hide();                    
        }else if($(this).val() == 2 ){   
            //$("#send").show();            
            $("#rechazar").show();
            $("#observacion-gerente").hide();   
            $("#observacion-director").hide();   
        } else if ( $(this).val() == 4 ){
            $("#rechazar").hide();
            
            if ( usuario == 1 ) {
                $("#observacion-gerente").show();
                $("#observacion-director").hide(); 
            } else if( usuario == 2 ){
                $("#observacion-gerente").hide();
                $("#observacion-director").show(); 
            }
        }
    });*/

    $('#analisis-viabilidad-form').on('submit', function(event) {
        event.preventDefault();
        event.stopImmediatePropagation();

        if ($('#servicio-decision').val() == '') {
            $('.field-servicio-decision').addClass('required has-error');
            $('.field-servicio-decision').focus();
            $('.field-servicio-decision>.help-block').show();
        } else{
            var id_servicio = <?= $model->id ?>;
            var id_cotizacion = <?= $model_cotizacion_taller->id ?>;
            var form = $(this);
            var data = form.serializeArray();      
            data.push({ name: 'id', value: id_servicio });
            data.push({ name: 'id_cotizacion', value: id_cotizacion });

            $.post( form.attr("action"), data ).done(function(response){
                console.log(response);
            });
        }
    });

    $('#addAnalistaModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var id = button.data('id');
        $('#submit-analista').data('id', id);
    });

    $('#analista').on('submit', function(event) {
        event.preventDefault();
        event.stopImmediatePropagation();

        $('#addAnalistaModal').modal('hide')
        var id = $('#submit-analista').data('id');

        var form = $(this);
        var data = form.serializeArray();      
        data.push({name: 'id', value: id});

        $.post( form.attr("action"), data ).done(function(response){
            location.reload();
            //alert('Analista agregado con éxito');
        });
    });

    $('.table').removeClass( "table-bordered table-striped" );
</script>