<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\db\Query;
use app\models\PipelineServicio;
use app\models\Empresa;
use yii\helpers\ArrayHelper;
use app\models\Estacion;
use kartik\select2\Select2;
use app\models\EstadoServicio;
use app\models\CotizacionTaller;


/* @var $this yii\web\View */
/* @var $searchModel app\models\PartesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'LISTADO DE SOLICITUDES';
?>
<div class="taller-index">

    <div class="row">
        <div class="col-md-1 col-xs-3">
            <i class="fa fa-list-ul fa-3x" aria-hidden="true"></i>
        </div>
        <div class="col-md-10 col-xs-9">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
        </div>
        <div class="col-xs-12">
            </br>
            </br>
            <p> <span style= "background-color: #61f898; width: 30px; height: 15px; display: inline-block;"></span> Negociación con taller</p>
        </div>
    </div>
    </br>
    </br>
    
    <div class="row">
        <div class="col-xs-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel'   => $searchModel,
                'tableOptions' => ['class' => 'table table-responsive'],
                'summary' => '',
                'options' =>[
                    'class' => 'grid',
                ],
                'columns' => [
                    [
                        'attribute' => 'id_servicio',
                        'label' => 'Número de solicitud',
                        'format' => 'text',
                        'filter' => true,
                        'enableSorting' => false,
                        'headerOptions' => ['width' => '100',],
                        'contentOptions' => function ($model, $key, $index, $column) {
                            $model_cotizacion = CotizacionTaller::find()->where(['id_solicitud_servicio_taller' => $model->id ])->one();
                            if ( isset($model_cotizacion) && $model_cotizacion->estado == 3 ) {
                                return  ['style' => 'background-color:#61f898'];
                            }
                        },
                    ],
                    [
                        'attribute' => 'idServicio.servicio',
                        'label' => 'Tipo de solicitud',
                        'format' => 'raw',
                        'filter' => true,
                        'filter' => Select2::widget([
                            'name' => 'ServicioSearch[id_servicio]',
                            'data' => ['0'=>'Mecánica Ligera','1'=>'Mecánica Especializada','2'=>'Aire Acondicionado','3'=>'Desabolladura y pintura','4'=>'Accesorios'],                        
                            'hideSearch' => true,
                            'options' => ['placeholder' => 'Seleccione',],
                            'pluginOptions' => [
                                  'allowClear' => true
                              ],
                          ]),
                        'enableSorting' => false,
                        'value' => function( $model ){
                            if ( $model->idServicio->servicio ) {
                                $servicios = explode("*", $model->idServicio->servicio);

                                foreach ($servicios as $key => $servicio) {
                                    switch ($servicio) {
                                        case '0':
                                            $vector_servicios[] = 'Mecánica Ligera';
                                            break;

                                        case '1':
                                            $vector_servicios[] = 'Mecánica Especializada';
                                            break;

                                        case '2':
                                            $vector_servicios[] = 'Aire Acondicionado';
                                            break;

                                        case '3':
                                            $vector_servicios[] = 'Desabolladura y pintura';
                                            break;

                                        case '4':
                                            $vector_servicios[] = 'Accesorios';
                                            break;
                                        
                                        default:
                                            # code...
                                            break;
                                    }
                                    
                                }

                                $respuesta_servicios = implode('<br>', $vector_servicios);

                                return $respuesta_servicios;
                            } else{
                                return '-';
                            }
                        }
                    ],
                    [
                        'label' => 'Empresa',
                        'format' => 'raw',
                        'filter' => true,
                        'enableSorting' => false,
                        'value' => function( $model ){
                            if ( $model->idServicio ) {
                                 return Empresa::findOne($model->idServicio->idEstacionActual->id_empresa)->nombre;
                            } else{
                                return '-';
                            }
                        },
                        
                        'filter' => Select2::widget([
                        'name' => 'ServicioSearch[id_company]',
                        'data' => ArrayHelper::map(Empresa::find()->orderBy('nombre')->all(), 'id_empresa', 'nombre'),
                        'hideSearch' => true,
                        'options' => ['placeholder' => 'Empresa',],
                        'pluginOptions' => [
                              'allowClear' => true
                          ],
                      ]),
                       
                    ], 
                    /*[
                        'label' => 'Cliente',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                        'value' => function( $model ){
                            if ( $model->idServicio ) {
                                 return Empresa::findOne($model->idServicio->idEstacionActual->id_empresa)->nombre;
                            } else{
                                return '-';
                            }
                        }
                    ],*/
                    /*[
                        'attribute' => 'idEstacionActual.nombre',
                        'label' => 'Estación',
                        'format' => 'text',
                        'filter' => true,
                        'enableSorting' => false,
                        'value' => function($model){
                            if ( $model->idServicio->id_estacion_actual ) {
                                 return $model->idServicio->idEstacionActual->nombre;
                            } else{
                                return '-';
                            }
                        }
                    ],*/
                    [
                        'label' => 'Estacion',
                        'format' => 'raw',
                        'filter' => true,
                        'enableSorting' => false,
                        'value' => function ($model, $key, $index, $column) {
                            return $model->idServicio->idEstacionActual->nombre;
                              },                        
                        'filter' => Select2::widget([
                        'name' => 'ServicioSearch[id_estacion]',
                        'data' => ArrayHelper::map(Estacion::find()->orderBy('nombre')->all(), 'id_estacion', 'nombre'),
                        'hideSearch' => true,
                        'options' => ['placeholder' => 'Estacion',],
                        'pluginOptions' => [
                              'allowClear' => true
                          ],
                      ]),
                       
                    ],
                    /*[
                        'attribute' => 'id_servicio',
                        'label' => 'Estatus',
                        'format' => 'html',
                        'enableSorting' => false,
                        'value' => function( $model ){
                            foreach ($model->idServicio->pipelineServicios as $value) {
                                $estado = $value->idEstadoServicio->nombre;
                                $color = $value->idEstadoServicio->color;
                            }
                            return '<span class="label label-oval" style="background-color:'.$color.'; ">'.$estado.'</span>';
                            // return $estado;
                        },
                        'filterType'=> GridView::FILTER_SELECT2,
                        'filter'=>ArrayHelper::map(EstadoServicio::find()->orderBy('nombre')->all(), 'id', 'nombre'),
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                            'name' => 'ServicioSearch[id_servicio]',
                        ],
                        'filterInputOptions'=>['placeholder'=>'Selecciona'],
                        'format'=>'raw',
                    ],*/
                    [
                        'label' => 'Nivel de Aprobación',
                        'format' => 'raw',
                        'filter' => true,
                        'enableSorting' => false,
                        'value' => function( $model ){
                            $model_cotizacion = CotizacionTaller::findOne(['id_solicitud_servicio_taller' => $model->id]);
                            if ($model_cotizacion->activo == 0) {
                                return 'Nivel 1';
                            } elseif ($model_cotizacion->activo == 1) {
                                return 'Nivel 2';
                            } elseif ($model_cotizacion->activo == 2) {
                                return 'Nivel 3';
                            }
                        },
                        'filter' => Select2::widget([
                            'name' => 'ServicioSearch[nivel]',
                            'data' => [ 0 => 'Nivel 1', 1 => 'Nivel 2', 2 => 'Nivel 3' ],
                            'hideSearch' => true,
                            'options' => ['placeholder' => 'Nivel',],
                            'pluginOptions' => [
                                  'allowClear' => true,
                            ],
                        ]),
                    ],
                    [
                        'label' => 'Estatus',
                        'format' => 'html',
                        'filter' => true,
                        'enableSorting' => false,
                        'value' => function( $model ){
                            $query = new query();
                            $query->select('es.nombre, es.color, es.codigo')
                                ->from('pipeline_servicio p')
                                ->join('INNER JOIN','estado_servicio es','es.id = p.id_estado_servicio')
                                ->where('p.id_servicio = '. $model->id_servicio )
                                ->orderBy('p.id desc');

                            $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                            $estado = Yii::$app->db->createCommand($sql)->queryOne();

                            if ( $estado['codigo'] == 0 ) {
                                return 'Nueva';
                            } else{
                                return '<span class="label label-oval" style="background-color:'.$estado['color'].'; ">'.$estado['nombre'].'</span>';
                            }
                        },
                        'filter' => Select2::widget([
                            'name' => 'ServicioSearch[id_estatus]',
                            'data' => ArrayHelper::map(EstadoServicio::find()->orderBy('nombre')->all(), 'id', 'nombre'),
                            'hideSearch' => true,
                            'options' => ['placeholder' => 'Estatus',],
                            'pluginOptions' => [
                                  'allowClear' => true
                              ],
                          ]),
                    ],
                    [  
                        'class' => 'yii\grid\ActionColumn',
                        'header'=>'Acciones',
                        'template' => '{menu}',
                        'buttons' => [

                            //menu button
                            'menu' => function ($url, $model) {

                                $model_cotizacion = CotizacionTaller::findOne(['id_solicitud_servicio_taller' => $model->id]);

                                return  '<div class="dropdown">
                                          <button class="btn btn-default dropdown-toggle btn-sm" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i>
                                          <span class="caret"></span></button>
                                          <ul class="dropdown-menu">'.
                                            '<li>'.Html::a('<span><i class="fa fa-eye fa-sm" aria-hidden="true"></i></span> Ver cotización',[ '/servicio/viewcotizacionanalista', 'id' => $model->id]).' </li>'.
                                            (( $model_cotizacion->estado == 1 && $model_cotizacion->activo == 0)?
                                                '<li>'.Html::a('<span><i class="fa fa-bar-chart fa-sm" aria-hidden="true"></i></span> Análisis de viabilidad',[ '/servicio/analisisviabilidad', 'id' => $model_cotizacion->id ]).' </li>'
                                            : '')
                                            .'</ul>
                                        </div>';
                            },
                        ],

                    ],
                ],
            ]); ?>
        </div>
        <?php


        ?>
    </div>
</div>