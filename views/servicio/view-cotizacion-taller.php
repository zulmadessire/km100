<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use yii\db\Query;
use app\models\TipoPieza;
use app\models\CotizacionTallerActividad;
use app\models\AnalistaMantenimiento;
use app\models\Taller;

$this->title = 'COTIZACIÓN DE TALLER';
$servicios = explode("*", $model->servicio);

foreach ($servicios as $key => $servicio) {
    switch ($servicio) {
        case '0':
            $vector_servicios[] = 'Mecánica Ligera';
            break;

        case '1':
            $vector_servicios[] = 'Mecánica Especializada';
            break;

        case '2':
            $vector_servicios[] = 'Aire Acondicionado';
            break;

        case '3':
            $vector_servicios[] = 'Desabolladura y pintura';
            break;

        case '4':
            $vector_servicios[] = 'Accesorios';
            break;
        
        default:
            # code...
            break;
    }
    
}

if ( $model_taller->tipo_pago == 0 ) {
	$tipo_pago = 'Pronto Pago';
} elseif ( $model_taller->tipo_pago == 1 ) {
	$tipo_pago = 'Crédito';
} elseif ( $model_taller->tipo_pago == 2 ) {
	$tipo_pago = 'Contado';
}

?>

<div class="view-cotizacion-taller">

    <div class="row">
		<div class="col-md-1 col-xs-3">
			<h4 class="titulo"><img src="<?= Yii::$app->request->baseUrl ?>/img/car_repair.png" alt=""></h4>
		</div>
		<div class="col-md-10 col-xs-9" style="padding-top: 30px;">
			<h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
		</div>
	</div>
	</br>
    </br>

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <?php if (Yii::$app->user->can('taller')): ?>
            
                <div class="pull-left" style="margin-top: 30px;">
                    <?= Html::a('Listado de cotizaciones',['/servicio/indextaller'], ['class' => 'btn btn-small-gris']) ?> 
                </div>
            
            <?php endif ?>
            <?php if (Yii::$app->user->can('admin')): ?>
            
                <div class="pull-left" style="margin-top: 30px;">
                    <?= Html::a('Listado de solicitudes',['/servicio/gestionservicio'], ['class' => 'btn btn-small-gris']) ?> 
                </div>
            
            <?php endif ?>
        </div>
        <div class="col-md-4 col-md-offset-4 col-xs-12">
        	<?= $form->field($model_cotizacion_taller, 'id')->textInput(['disabled' => true, ])->label('NÚMERO DE COTIZACIÓN') ?>
        </div>
        </br>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DEL VEHÍCULO</p>
            <hr>
        </div>
    </div>


    <div class="row">

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_modelo')->textInput(['disabled' => true, 'value' => $model_vehiculo->idModelo->idMarca->nombre ])->label('Marca') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_modelo')->textInput(['value' => strtoupper($model_vehiculo->idModelo->nombre), 'disabled' => true])->label('Modelo') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'mva')->textInput(['maxlength' => true, 'disabled' => true])->label('Ficha o MVA') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'chasis')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'placa')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_categoria')->textInput(['maxlength' => true, 'disabled' => true, 'value' => $model_vehiculo->idCategoria->nombre ])->label('Categoria') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'km')->textInput(['maxlength' => true, 'disabled' => true, 'value' => number_format(floatval($model_vehiculo->km), 0, ',', '.'). ' Km', ])->label('Km Actual') ?>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DE LA SOLICITUD</p>
            <hr>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'id')->textInput(['maxlength' => true, 'disabled' => true,])->label('NÚMERO DE SOLICITUD') ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'prioridad')->textInput(['maxlength' => true, 'disabled' => true, 'value' => (($model->prioridad == 0)?'Normal':'Urgente') ])->label('PRIORIDAD') ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'linea')->textInput(['maxlength' => true, 'disabled' => true, 'value' => ($model->linea == 0)?'Quick Line':'Normal Line',])->label('LÍNEA') ?>
        </div>
        <div class="col-md-4 col-xs-12">
        	<?= $form->field($model, 'evaluacion_tecnica')->textarea(['rows' => 3, 'disabled' => true,])->label('EVALUACIÓN TÉCNICA PRELIMINAR') ?>
        </div>
		<div class="col-md-4 col-xs-12">
			<label class="control-label" for="servicio-servicio">SERVICIOS SOLICITADO</label>
			<ul class="list-group">
				<?php foreach ($vector_servicios as $servicio) {
					echo '<li class="list-group-item">'.$servicio.'</li>';
				} ?>
			</ul>
		</div>
    </div>
    </br>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">MANO DE OBRA PROPUESTA</p>
            <hr>
        </div>
    </div>
	
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_taller, 'tipo_servicio_taller')->textInput(['maxlength' => true, 'disabled' => true, 'value' => $tipo_pago,])->label('SERVICIOS TALLER') ?>
        </div>
    	<div class="col-md-4 col-xs-12">
    		<?= $form->field($model_taller, 'tipo_pago')->textInput(['maxlength' => true, 'disabled' => true, 'value' => $tipo_pago,])->label('FORMA DE PAGO') ?>
    		<?= $form->field($model_cotizacion_taller, 'tipo_pago')->hiddenInput(['value'=> $model_taller->tipo_pago])->label(false); ?>
    	</div>
    	<div class="col-md-12 col-xs-12">
    		<?= GridView::widget([
                'dataProvider' => $dataProvider_cotizacion_actividad,
                'summary' => '',
                'id' =>'cotizacion-actividad-grid',
                'pjax' => true,
                'showFooter' => true,
                'tableOptions' => ['class' => 'table table-responsive', 'id' =>'cotizacion-actividad-table'],
                'options' =>[
                    'class' => 'grid-blue',
                ],
                'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                'afterFooter' => '<tr class="kv-table-footer"><td></td><td></td><td></td><td></td><td>ITBIS</td><td style="text-align: center;">'.number_format(floatval( CotizacionTallerActividad::getItbis2($dataProvider_cotizacion_actividad->models)), 2, ',', '.') . ' RD$'.'</td></tr><tr class="kv-table-footer"><td></td><td></td><td></td><td></td><td>TOTAL</td><td style="text-align: center;">'.number_format(floatval( CotizacionTallerActividad::getTotal2($dataProvider_cotizacion_actividad->models)), 2, ',', '.') . ' RD$'.'</td></tr>',
                'columns' => [
                    [
                        'label' => 'Servicio',
                        'format' => 'text',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:left;'],
                        //'headerOptions' => ['style' => 'width:80%'],
                        'footer' => '',
                        'value' => function($model){
                            if ($model->id_pieza_servicio_taller) {
                                return $model->piezaServicioTaller->servicioTaller->nombre;
                            }
                            else{
                                return '-';
                            }
                        },
                    ],
                    [
                        'label' => 'Descripción de la Actividad',
                        'format' => 'text',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:left;'],
                        //'headerOptions' => ['style' => 'width:80%'],
                        'footer' => '',
                        'value' => function($model){
                            if ($model->id_pieza_servicio_taller) {
                                return $model->piezaServicioTaller->nombre;
                            }
                            else{
                                return '-';
                            }
                        },
                    ],
                    [
                        'label' => 'Labor',
                        'format' => 'text',
                        'enableSorting' => false,
                        'footer' => '',
                        'value' => function($model){
                            if ($model->labor == 0) {
                                return 'Reparación';
                            } elseif ( $model->labor == 1 ){
                                return 'Salvamento Leve';
                            } elseif ( $model->labor == 2 ){
                                return 'Salvamento Fuerte';
                            } else{
                                return '-';
                            }
                        },
                    ],
                    [
                        'attribute' => 'precio',
                        'label' => 'Precio',
                        'format' => 'text',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'value' => function($model){
                        	if ($model->precio) {
                        		return number_format(floatval($model->precio), 2, ',', '.') . ' RD$';
                        	} else{
                        		return '-';
                        	}
                        },
                        'footer' => '',
                    ],
                    [
                        'label' => 'Decimal',
                        'format' => 'text',
                        'enableSorting' => false,
                        'footer' => 'SUBTOTAL',
                        'value' => function($model){
                            if ($model->decimal) {
                                return $model->decimal;
                            } else{
                                return '-';
                            }
                        },
                    ],
                    [
                        'label' => 'Total',
                        'format' => 'text',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'value' => function($model){
                        	if ($model->total_servicio ) {
                                return number_format(floatval(  $model->total_servicio ), 2, ',', '.') . ' RD$';
                            } else{
                                return '0';
                            }
                        },
                        'footer' => number_format(floatval( CotizacionTallerActividad::getSubTotal2($dataProvider_cotizacion_actividad->models)), 2, ',', '.') . ' RD$',
                    ],
                ],
            ]); ?>
    	</div>
    </div>
    </br>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">PIEZAS SOLICITADAS</p>
            <hr>
        </div>
    </div>
    <div class="row">
        <?php if ($dataProvider_cotizacion_pieza->getTotalCount() > 0): ?>
        	<div class="col-md-12 col-xs-12">
        		<?= GridView::widget([
                    'dataProvider' => $dataProvider_cotizacion_pieza,
                    'summary' => '',
                    'id' =>'cotizacion-pieza-grid',
                    'pjax' => true,
                    'tableOptions' => ['class' => 'table table-responsive', 'id' =>'cotizacion-pieza-table'],
                    'options' =>[
                        'class' => 'grid-blue',
                    ],
                    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                    'columns' => [
                        [
                            'label' => 'Tipo',
                            'format' => 'text',
                            'enableSorting' => false,
                            'contentOptions'=>['style'=>'text-align:center;'],
                            'value' => function($model){
                            	if ($model->id_pieza) {
                            		return $model->idPieza->tipoPieza->nombre;
                            	} else{
                            		return '-';
                            	}
                            },
                        ],
                        [
                            'attribute' => 'idPieza.nombre',
                            'label' => 'Descripcion',
                            'format' => 'text',
                            'enableSorting' => false,
                            'contentOptions'=>['style'=>'text-align:center;'],
                        ],
                        [
                            'attribute' => 'cantidad',
                            'label' => 'Cantidad',
                            'format' => 'text',
                            'enableSorting' => false,
                            'contentOptions'=>['style'=>'text-align:center;'],
                        ],
                    ],
                ]); ?>
        	</div>
        <?php else: ?>
            <div class="col-md-12 col-xs-12">
                <div class="alert alert-info alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-info"></i> Información!</h4>
                    No solicito piezas.
                </div>
            </div>
        <?php endif ?>
    </div>
    </br>
    </br>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">OBSERVACIONES</p>
            <hr>
        </div>
    </div>

    <?php if ($model_cotizacion_taller->estado == 3 ): ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="alert alert-warning alert-dismissible">
                    <h4><i class="icon fa fa-info"></i> KM100!</h4>
                    <?= $model_cotizacion_taller->observacion_cancela ?>
                </div>
            </div>
        </div>
        </br>
    <?php endif ?>

    <div class="row">
        <?php foreach ($model_servicio_mensaje as $key => $mensaje): ?>
            <div class="col-xs-12">
                <div class="media">
                    <div class="media-body">
                        <h5 class="media-heading"><strong> <?= ( isset($mensaje->user->id_taller) && $mensaje->user->id_taller != 0 )?ucfirst(Taller::findOne($mensaje->user->id_taller)->nombre):'KM100' ?> </strong></h5>
                        <p> <?= $mensaje->mensaje ?> </p>
                        <small> <?= $mensaje->fecha ?> </small>
                    </div>
                </div>
                </br>
            </div>
        <?php endforeach ?>
    </div>
    </br>
    </br>
	
	<div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">IMÁGENES DE SOPORTE</p>
            <hr>
        </div>
    </div>
    <div class="row">
        <?php
            $image = Html::img(Yii::$app->urlManager->baseUrl . '/' . $model_cotizacion_taller->imagen_soporte, [
                'class'=>'file-preview-image',
                'style' => 'height: 180px; display: block;',
            ]);
            $image1 = Html::img(Yii::$app->urlManager->baseUrl . '/' . $model_cotizacion_taller->imagen_soporte1, [
                'class'=>'file-preview-image',
                'style' => 'height: 180px; display: block;',
            ]);
            $image2 = Html::img(Yii::$app->urlManager->baseUrl . '/' . $model_cotizacion_taller->imagen_soporte2, [
                'class'=>'file-preview-image',
                'style' => 'height: 180px; display: block;',
            ]);
            $image3 = Html::img(Yii::$app->urlManager->baseUrl . '/' . $model_cotizacion_taller->imagen_soporte3, [
                'class'=>'file-preview-image',
                'style' => 'height: 180px; display: block;',
            ]);
            $image4 = Html::img(Yii::$app->urlManager->baseUrl . '/' . $model_cotizacion_taller->imagen_soporte4, [
                'class'=>'file-preview-image',
                'style' => 'height: 180px; display: block;',
            ]);
        ?>
        <div class="col-md-4 col-xs-12">
            <div class="thumbnail file-preview-frame">
                <?= (empty($model_cotizacion_taller->imagen_soporte))?'No hay imágen cargada.':$image ?> 
            </div>
        </div>
        <div class="col-md-4 col-xs-12" style = "<?= (empty($model_cotizacion_taller->imagen_soporte1))?'display: none;':'display: block;' ?>">
            <div class="thumbnail file-preview-frame">
                <?= $image1 ?> 
            </div>
        </div>
        <div class="col-md-4 col-xs-12" style = "<?= (empty($model_cotizacion_taller->imagen_soporte2))?'display: none;':'display: block;' ?>">
            <div class="thumbnail file-preview-frame">
                <?= $image2 ?>  
            </div>
        </div>
        <div class="col-md-4 col-xs-12" style = "<?= (empty($model_cotizacion_taller->imagen_soporte3))?'display: none;':'display: block;' ?>">
            <div class="thumbnail file-preview-frame">
                <?= $image3 ?>  
            </div>
        </div>
        <div class="col-md-4 col-xs-12" style = "<?= (empty($model_cotizacion_taller->imagen_soporte4))?'display: none;':'display: block;' ?>">
            <div class="thumbnail file-preview-frame">
                <?= $image4 ?> 
            </div>
        </div>
    </div>
    </br>
    </br>
    </br>
    </br>
    <?php ActiveForm::end(); ?>

</div>

<style>
	#cotizacion-actividad-grid tfoot > tr > td {
		text-align: right;
	}

	#cotizacion-actividad-grid tfoot > tr > td:nth-child(6) {
	    text-align: center;
	    font-weight: initial;
	}
    .view-cotizacion-taller .dropdown-menu {
        right: 0;
        left: unset;
    }
</style>

<script>
    $('#addAnalistaModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var id = button.data('id');
        $('#submit-analista').data('id', id);
    });

    $('#analista').on('submit', function(event) {
        event.preventDefault();
        event.stopImmediatePropagation();

        $('#addAnalistaModal').modal('hide')
        var id = $('#submit-analista').data('id');

        var form = $(this);
        var data = form.serializeArray();      
        data.push({name: 'id', value: id});

        $.post( form.attr("action"), data ).done(function(response){
            location.reload();
        });
    });
    $('.table').removeClass( "table-bordered table-striped" );
</script>