<?php 

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\db\Query;
use kartik\grid\GridView;
use app\models\PipelineServicio;
use app\models\Empresa;
use app\models\SolicitudServicioTaller;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use app\models\AnalistaMantenimiento;
use app\models\CotizacionTaller;
use app\models\CotizacionTallerAnalista;
use app\models\CotizacionTallerActividadSearch;
use app\models\Modelo;
use app\models\EstadoServicio;
use app\models\Cliente;
use app\models\Estacion;


$this->title = 'Gestión de Servicios';
?>
<div>
    <div class="row">
        <div class="col-md-1 col-xs-3">
            <h4 class="titulo"><img src="<?= Yii::$app->request->baseUrl ?>/img/car_repair.png" alt=""></h4>
        </div>
        <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
        </div>
    </div>
    </br>

    <div class="row">
        <div class="col-xs-12">
            <div class="content-pipeline">
                <p style="margin-top: 10px; font-size: 10pt; margin-left: 15px;">ESTATUS GENERAL DE VEHÍCULOS GESTIONADOS</p>
                <div class="col-xs-12 col-md-offset-1 col-md-10">
                    <div class="bs-wizard" style="border-bottom:0;">
                        <?php for ($i=0; $i < 8 ; $i++) : ?> 
                            <div class="bs-wizard-step complete">
                                <div class="progress"><div class="progress-bar"></div></div>
                                <div class="bs-wizard-dot"></div>
                                <div class="bs-wizard-stepnum text-center">
                                    <?php
                                        switch ($i) {
                                            case 0:
                                                echo "Envío a taller";
                                                break;

                                            case 1:
                                                echo "Evaluación en taller";
                                                break;

                                            case 2:
                                                echo "Aprobación";
                                                break;

                                            case 3:
                                                echo "Pendiente por pieza";
                                                break;

                                            case 4:
                                                echo "En proceso";
                                                break;

                                            case 5:
                                                echo "Finalizado";
                                                break;

                                            case 6:
                                                echo "Entregado";
                                                break;

                                            case 7:
                                                echo "Garantía";
                                                break;
                                             
                                            default:
                                                # code...
                                                break;
                                         } 
                                    ?>
                                </div>
                            </div>
                        <?php endfor ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </br>
    </br>
    <div class="row">
        <div class="col-xs-12">
            <div style="margin-left: 20px;">
                <p style="font-weight: bold; font-size: 11pt;">LISTADO DE VEHÍCULOS EN SERVICIO</p>
                <span>Total de Vehículos: <?= $dataProvider->getTotalCount() ?></span>
            </div>
        </div>
        <!--<div class="col-xs-12">
            <div class="pull-right">
                <?php // Html::a('Recepción de vehículos',['/servicio/gestionservicio', 'type' => 1], ['class' => 'btn btn-filtro']) ?>
            </div>
        </div>-->
    </div>
    </br>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel'   => $searchModel,
                'layout' => "{items}\n<div align='center'>{pager}</div>",            
                'tableOptions' => ['class' => 'table table-responsive',],
                'summary' => '',
                'options' =>[
                    'class' => 'grid',
                ],
                'resizableColumns' => false,
                //'pjax' => true,
                'columns' => [
                    [
                        'class'=>'kartik\grid\ExpandRowColumn',
                        'width'=>'50px',
                        'allowBatchToggle' => false,
                        'expandIcon' => '<i class="fa fa-caret-right" aria-hidden="true" style="color:#d9d9d9;"></i>',
                        'collapseIcon' => '<i class="fa fa-caret-down" aria-hidden="true" style="color:#d9d9d9;"></i>',
                        'value'=>function ($model, $key, $index, $column) {
                            $pipeline_servicio_first = PipelineServicio::find()->where([ 'id_servicio' => $model->id ])->orderby('id DESC')->one();
                            if ( $pipeline_servicio_first->idEstadoServicio->codigo == 10 ) {
                                return '<i></i>';
                            } else if ( $pipeline_servicio_first->idEstadoServicio->codigo == 11  ){
                                
                                /*if ( $model->aprobacion_estacion == -1 ) {
                                    if(\Yii::$app->user->can('planner') || \Yii::$app->user->can('admin')) {
                                        return  Html::a('<span class="fa-stack fa-lg">
                                        <i class="fa fa-circle fa-stack-2x" style="color:#e79e2d;"></i>
                                        <i class="fa fa-wrench fa-stack-1x fa-inverse"></i>
                                      </span>',
                                      ['/servicio/asignartaller', 'id' => $model->id ], ['title' => 'Asignar taller a esta solicitud']);
                                    }else {
                                        return " ";
                                    }
                                } else{*/
                                    return " ";
                                //}
                            } else {
                                return GridView::ROW_COLLAPSED;
                            }
                        },
                        'detail'=>function ($model, $key, $index, $column) {
                            $pipeline_servicio = PipelineServicio::find()->where([ 'id_servicio' => $model->id ])->andWhere('id_estado_servicio != 10 AND id_estado_servicio != 11')->orderby('id DESC')->all();
                            $pipeline_servicio_first = PipelineServicio::find()->where([ 'id_servicio' => $model->id ])->orderby('id DESC')->one();

                            if ( $pipeline_servicio_first->idEstadoServicio->codigo != 10 && $pipeline_servicio_first->idEstadoServicio->codigo != 11 ) {
                                if ( $pipeline_servicio_first->idEstadoServicio->codigo == 2 ) {

                                    $query = new query();
                                    $query->select('c.id')
                                        ->from('solicitud_servicio_taller sst')
                                        ->join('INNER JOIN','cotizacion_taller c','c.id_solicitud_servicio_taller = sst.id AND c.estado = 1')
                                        ->where('sst.id_servicio = '. $model->id)
                                        ->orderBy('c.id desc');

                                    $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                                    $id_cotizacion = Yii::$app->db->createCommand($sql)->queryOne();

                                    //$model_cotizacion = CotizacionTaller::find()->joinWith('idSolicitudServicioTaller')->where('solicitud_servicio_taller.id_servicio = ' . $model->id . ' AND cotizacion_taller.estado = 2')->orderBy('cotizacion_taller.id DESC')->one();

                                    $count_asignacion = CotizacionTallerAnalista::find()->where(['id_cotizacion_taller' => $id_cotizacion ])->count();

                                    return Yii::$app->controller->renderPartial('_expand-solicitud-servicio', ['pipeline_servicio' => $pipeline_servicio, 'id_cotizacion' => $id_cotizacion, 'type' => 1, 'count_asignacion' => $count_asignacion, 'id_servicio' => $model->id ]);
                                } else if ( $pipeline_servicio_first->idEstadoServicio->codigo >= 5 && $pipeline_servicio_first->idEstadoServicio->codigo <= 9 ) {

                                    $model_cotizacion = CotizacionTaller::find()->joinWith('idSolicitudServicioTaller')->where('solicitud_servicio_taller.id_servicio = ' . $model->id )->one();

                                    $searchModel_cotizacion_actividad = new CotizacionTallerActividadSearch([ 'id_cotizacion_taller' => $model_cotizacion->id ]);
                                    $dataProvider_cotizacion_actividad = $searchModel_cotizacion_actividad->search(Yii::$app->request->queryParams);

                                    return Yii::$app->controller->renderPartial('_expand-solicitud-servicio', ['pipeline_servicio' => $pipeline_servicio, 'type' => 1, 'dataProvider_cotizacion_actividad' => $dataProvider_cotizacion_actividad, ]);
                                } else {
                                    return Yii::$app->controller->renderPartial('_expand-solicitud-servicio', ['pipeline_servicio' => $pipeline_servicio, 'type' => 1 ]);
                                }
                            } else{
                               return '';
                            }
                        },
                        'headerOptions'=>['class'=>'kartik-sheet-style'],
                        'expandOneOnly'=>true,
                        'enableRowClick' => true,
                    ],
                    [
                        'attribute' => 'id',
                        'label' => 'Núm. Solicitud',
                        'format' => 'text',
                        'filter' => true,
                        'enableSorting' => false,
                        'headerOptions' => ['width' => '80',],
                    ],
                    [
                        'attribute' => 'ficha',
                        'label' => 'MVA',
                        'format' => 'text',
                        'filter' => true,
                        'enableSorting' => false,
                        'headerOptions' => ['width' => '80',],
                    ],
                    [
                        'attribute' => 'id_modelo',
                        'label' => 'Modelo',
                        'format' => 'raw',
                        'filter' => true,
                        'enableSorting' => false,
                        'value' => function ($model, $key, $index, $column) {
                            return $model->idVehiculo->idModelo->nombre;
                        },
                        'filterType'=> GridView::FILTER_SELECT2,
                        'filter'=> ArrayHelper::map(Modelo::find()->orderBy('nombre')->all(), 'id_modelo', 'nombre'),
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                        ],
                        'filterInputOptions'=>['placeholder'=>'Selecciona'],
                        'headerOptions' => ['width' => '80',], 
                    ],
                    [
                        'attribute' => 'servicio',
                        'label' => 'Tipo de solicitud',
                        'format' => 'raw',
                        'filterType'=> GridView::FILTER_SELECT2,
                        'filter'=> ['0'=>'Mecánica Ligera','1'=>'Mecánica Especializada','2'=>'Aire Acondicionado','3'=>'Desabolladura y pintura','4'=>'Accesorios'],
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                        ],
                        'filterInputOptions'=>['placeholder'=>'Selecciona'],
                        'format'=>'raw',                   
                        'enableSorting' => false,
                        'headerOptions' => ['width' => '200',],
                        'value' => function( $model ){
                            if ( $model->servicio ) {
                                $servicios = explode("*", $model->servicio);

                                foreach ($servicios as $key => $servicio) {
                                    switch ($servicio) {
                                        case '0':
                                            $vector_servicios[] = 'Mecánica Ligera';
                                            break;

                                        case '1':
                                            $vector_servicios[] = 'Mecánica Especializada';
                                            break;

                                        case '2':
                                            $vector_servicios[] = 'Aire Acondicionado';
                                            break;

                                        case '3':
                                            $vector_servicios[] = 'Desabolladura y pintura';
                                            break;

                                        case '4':
                                            $vector_servicios[] = 'Accesorios';
                                            break;
                                        
                                        default:
                                            # code...
                                            break;
                                    }
                                    
                                }

                                $respuesta_servicios = implode('<br>', $vector_servicios);

                                return $respuesta_servicios;
                            } else{
                                return '-';
                            }
                        }
                    ],
                    /*[
                        'attribute' => 'id_cliente',
                        'label' => 'Cliente',
                        'filterType'=> GridView::FILTER_SELECT2,
                        'filter'=> ArrayHelper::map(Cliente::find()->orderBy('nombre_comercial')->all(), 'id', 'nombre_comercial'),
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                        ],
                        'filterInputOptions'=>['placeholder'=>'Selecciona'],
                        'format'=>'raw',
                        'enableSorting' => false,
                        'value' => function( $model ){
                            if ( $model->id_estacion_actual ) {
                                 return $model->idEstacionActual->idEmpresa->cliente->nombre_comercial;
                            } else{
                                return '-';
                            }
                        }
                    ],
                    [
                        'attribute' => 'id_company',
                        'label' => 'Empresa',
                        'format' => 'raw',
                        'filter' => true,
                        'enableSorting' => false,
                        'value' => function ($model, $key, $index, $column) {
                          return $model->idEstacionActual->idEmpresa->nombre;
                              },
                        'filterType'=> GridView::FILTER_SELECT2,
                        'filter'=> ArrayHelper::map(Empresa::find()->orderBy('nombre')->all(), 'id_empresa', 'nombre'),
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                        ],
                        'filterInputOptions'=>['placeholder'=>'Selecciona'],
                        'format'=>'raw',
                    ],
                    [
                        'attribute' => 'id_estacion_actual',
                        'label' => 'Estacion',
                        'format' => 'raw',
                        'filter' => true,
                        'enableSorting' => false,
                        'value' => function ($model, $key, $index, $column) {
                            return $model->idEstacionActual->nombre;
                        },
                        'filterType'=> GridView::FILTER_SELECT2,
                        'filter'=> ArrayHelper::map(Estacion::find()->orderBy('nombre')->all(), 'id_estacion', 'nombre'),
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                        ],
                        'filterInputOptions'=>['placeholder'=>'Selecciona'],
                        'headerOptions' => ['width' => '100',],
                    ], */
                    [
                        'attribute' => 'taller',
                        'label' => 'Taller Asignado',
                        'format' => 'text',
                        'enableSorting' => true,
                        'filterType'=> GridView::FILTER_SELECT2,
                        'filter'=> $talleres,
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                        ],
                        'filterInputOptions'=>['placeholder'=>'Selecciona'],
                        'headerOptions' => ['width' => '200',],
                    ],
                    [
                        'attribute' => 'dias',
                        'format' => 'text',
                        'filter' => true,
                        'enableSorting' => false,
                        'headerOptions' => ['width' => '100',],
                    ],
                    [
                        'attribute' => 'estatus',
                        'label' => 'Estatus',
                        'format' => 'html',
                        'enableSorting' => true,
                        'filterType'=> GridView::FILTER_SELECT2,
                        'filter'=>ArrayHelper::map(EstadoServicio::find()->orderBy('nombre')->all(), 'id', 'nombre'),
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                        ],
                        'filterInputOptions'=>['placeholder'=>'Selecciona'],
                        'format'=>'raw',
                        'headerOptions' => ['width' => '150',],
                    ],
                    [  
                        'class' => 'yii\grid\ActionColumn',
                        'header'=>'',
                        'template' => '{view}',
                        'buttons' => [
                            
                            'view' => function ($url, $model){
                                return Html::a('<span><i class="fa fa-eye fa-sm" aria-hidden="true"></i></span>',[ '/servicio/view', 'id' => $model->id, 'origen' => 2]);
                            },
                            /*'menu' => function ($url, $model) {

                                 return  '<div class="dropdown">
                                          <button class="btn btn-default dropdown-toggle btn-sm" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i>
                                          <span class="caret"></span></button>
                                          <ul class="dropdown-menu">
                                            <li>'.Html::a('<span><i class="fa fa-eye fa-sm" aria-hidden="true"></i></span> Ver <br> Solicitud',[ '/servicio/view', 'id' => $model->id, 'origen' => 2 ]).' </li>                                            
                                          </ul>
                                        </div>';


                            },*/
                        ],

                   ],
                ],
            ]); ?>
        </div>
    </div>

    

</div>

<?php if (Yii::$app->user->can('admin')): ?>
<!-- Modal Asociar Analista -->
<div id="addAnalistaModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="title"><i class="fa fa-user fa-lg" aria-hidden="true"></i>&nbsp;&nbsp; ASIGNAR ANALISTA DE MTTO.</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true" style="color: #000; font-weight: bold;"></i></button>
          </div>
        <div class="modal-body">
            </br>
            <?php $form = ActiveForm::begin([ 'id' => 'analista', 'action' => ['/servicio/asignaranalista'] ]); ?>
                <div class="row">
                    <div class="text-center">
                        <div class="col-xs-12 col-md-8 col-md-offset-2">
                            <label class="control-label" for="garantiavehiculo-motivo">ANALISTA A ASIGNAR</label>
                            <?=
                                Select2::widget([
                                    'model' => $model_cotizacion_taller_analista,
                                    'attribute' => 'id_analista',
                                    'data' => ArrayHelper::map(AnalistaMantenimiento::find()->orderBy('nombre')->all(), 'id', 'nombre'),
                                    'options' => ['placeholder' => 'Selecciona el analista'],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ]);
                            ?>
                        </div>
                    </div>
                </div>

            <?php ActiveForm::end(); ?>
            </br>
            </br>
            </br>
            </br>
            </br>
            <?php
            echo '<div class="row" style="padding-top: 25px;">';
                echo '<div class="text-center">';
                    echo Html::button('CANCELAR', ['class' => 'btn btn-cancelar', 'data' => ['dismiss' => 'modal']]);
                    echo "&nbsp;";
                    echo Html::submitButton('REGISTRAR', ['class' => 'btn btn-aceptar', 'form' => 'analista', 'id' => 'submit-analista' ]);
                echo '</div>';
            echo '</div>';
            ?>
        </div>
    </div>
  </div>
</div>
<!-- Fin modal-->
<?php endif ?>

<style type="text/css" media="screen">
    .bs-wizard {}

    /*Form Wizard*/
    .bs-wizard {border-bottom: solid 1px #e0e0e0; padding: 10px 0 10px 0;}
    .bs-wizard > .bs-wizard-step {padding: 0; position: relative; width: 12.5%; float: left;}
    .bs-wizard > .bs-wizard-step .bs-wizard-stepnum {color: #000; font-size: 8pt; font-weight: bold;}
    .bs-wizard > .bs-wizard-step .bs-wizard-info {color: #000; font-size: 8pt;}
    .bs-wizard > .bs-wizard-step > .bs-wizard-dot {position: absolute; width: 30px; height: 30px; display: block; background: #767171; top: 22px; left: 50%; margin-top: -15px; margin-left: -15px; border-radius: 50%;} 
    .bs-wizard > .bs-wizard-step > .bs-wizard-dot:after {content: ' '; width: 16px; height: 16px; background: #fff; border-radius: 50px; position: absolute; top: 7px; left: 7px; }
    .bs-wizard > .bs-wizard-step > .bs-wizard-dot.active:after { background: #a9d18e; }  
    .bs-wizard > .bs-wizard-step > .progress {position: relative; border-radius: 0px; height: 6px; box-shadow: none; margin: 20px 0; background: #767171;}
    /*END Form Wizard*/

    .content-pipeline{
        border: 1px solid #dedede;
        height: 150px;
    }
</style>
<script>
    $('.table').removeClass( "table-bordered table-striped" );

    $('#addAnalistaModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var id = button.data('id');
        $('#submit-analista').data('id', id);
    });

    $('#analista').on('submit', function(event) {
        event.preventDefault();
        event.stopImmediatePropagation();

        $('#addAnalistaModal').modal('hide')
        var id = $('#submit-analista').data('id');

        var form = $(this);
        var data = form.serializeArray();      
        data.push({name: 'id', value: id});

        $.post( form.attr("action"), data ).done(function(response){
            location.reload();
        });
    });
</script>