<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use yii\db\Query;
use app\models\Pieza;
use app\models\ServicioTaller;
use app\models\PiezaServicioTaller;
use app\models\TipoPieza;
use app\models\CotizacionTallerActividad;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Taller;

$this->title = 'REGISTRO DE COTIZACIÓN';
$servicios = explode("*", $model->servicio);

foreach ($servicios as $key => $servicio) {
    switch ($servicio) {
        case '0':
        $vector_servicios[] = 'Mecánica Ligera';
        break;

        case '1':
        $vector_servicios[] = 'Mecánica Especializada';
        break;

        case '2':
        $vector_servicios[] = 'Aire Acondicionado';
        break;

        case '3':
        $vector_servicios[] = 'Desabolladura y pintura';
        break;

        case '4':
        $vector_servicios[] = 'Accesorios';
        break;

        default:
        # code...
        break;
    }
}

if ( $model_taller->tipo_pago == 0 ) {
    $tipo_pago = 'Pronto Pago';
} elseif ( $model_taller->tipo_pago == 1 ) {
    $tipo_pago = 'Crédito';
} elseif ( $model_taller->tipo_pago == 2 ) {
    $tipo_pago = 'Contado';
}

if ( $model_taller->tipo_servicio_taller == 0 ) {
    $tipo_servicio_taller = 'Desabolladura y Pintura';
    $servicios_taller = [ 1 => 'Desabolladura y Pintura', ];
} elseif ( $model_taller->tipo_servicio_taller == 1 ) {
    $tipo_servicio_taller = 'Mecánica';
    $servicios_taller = [ 2 => 'Mecánica', ];
} elseif ( $model_taller->tipo_servicio_taller == 2 ) {
    $tipo_servicio_taller = 'Desabolladura y Pintura / Mecánica';
    $servicios_taller = [ 1 => 'Desabolladura y Pintura', 2 => 'Mecánica', ];
}

$lineas = [0 => 'Reparación', 1 => 'Salvado Leve', 2 => 'Salvado Fuerte'];

?>

<div class="cotizacion-taller">

    <div class="row">
       <div class="col-md-1 col-xs-3">
          <h4 class="titulo"><i class="fa fa-file-text fa-4x" aria-hidden="true"></i></h4>
      </div>
      <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
          <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
      </div>
  </div>
  <br>
  <br>


    <?php $form = ActiveForm::begin(['id' => 'dynamic_lista-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <div class="pull-rigth" style="margin-top: 30px;">
                <?= Html::a('Listado de solicitudes',['/servicio/indextaller'], ['class' => 'btn btn-small-gris']) ?>
            </div>
        </div>
        <div class="col-md-4 col-md-offset-4 col-xs-12">
        	<?= $form->field($model_cotizacion_taller, 'id')->textInput(['disabled' => true, ])->label('NÚMERO DE COTIZACIÓN') ?>
        </div>
        </br>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DEL VEHÍCULO</p>
            <hr>
        </div>
    </div>


    <div class="row">

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_modelo')->textInput(['disabled' => true, 'value' => $model_vehiculo->idModelo->idMarca->nombre ])->label('Marca') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_modelo')->textInput(['value' => strtoupper($model_vehiculo->idModelo->nombre), 'disabled' => true])->label('Modelo') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'anho')->textInput(['maxlength' => true, 'disabled' => true])->label('Año') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'mva')->textInput(['maxlength' => true, 'disabled' => true])->label('Ficha o MVA') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'chasis')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'placa')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_categoria')->textInput(['maxlength' => true, 'disabled' => true, 'value' => $model_vehiculo->idCategoria->nombre ])->label('Categoria') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'km')->textInput(['maxlength' => true, 'disabled' => true, 'value' => number_format(floatval($model_vehiculo->km), 0, ',', '.'). ' Km', ])->label('Km Actual') ?>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DE LA SOLICITUD</p>
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'id')->textInput(['maxlength' => true, 'disabled' => true,])->label('NÚMERO DE SOLICITUD') ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'prioridad')->textInput(['maxlength' => true, 'disabled' => true, 'value' => (($model->prioridad == 0)?'Normal':'Urgente') ])->label('PRIORIDAD') ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'linea')->textInput(['maxlength' => true, 'disabled' => true, 'value' => ($model->linea == 0)?'Quick Line':'Normal Line',])->label('LÍNEA') ?>
        </div>
        <div class="col-md-4 col-xs-12">
        	<?= $form->field($model, 'evaluacion_tecnica')->textarea(['rows' => 3, 'disabled' => true,])->label('EVALUACIÓN TÉCNICA PRELIMINAR') ?>
        </div>
        <div class="col-md-4 col-xs-12">
          <label class="control-label" for="servicio-servicio">SERVICIOS SOLICITADO</label>
          <ul class="list-group">
             <?php foreach ($vector_servicios as $servicio) {
                echo '<li class="list-group-item">'.$servicio.'</li>';
            } ?>
        </ul>
    </div>
    </div>
    </br>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">MANO DE OBRA PROPUESTA</p>
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_taller, 'tipo_servicio_taller')->textInput(['maxlength' => true, 'disabled' => true, 'value' => $tipo_servicio_taller,])->label('SERVICIOS TALLER') ?>
        </div>
    	<div class="col-md-4 col-xs-12">
    		<?= $form->field($model_taller, 'tipo_pago')->textInput(['maxlength' => true, 'disabled' => true, 'value' => $tipo_pago,])->label('FORMA DE PAGO') ?>
    		<?= $form->field($model_cotizacion_taller, 'tipo_pago')->hiddenInput(['value'=> $model_taller->tipo_pago])->label(false); ?>
    	</div>
        <div class="clearfix"></div>
    </br>
    <div class="col-md-12 col-xs-12">
        <?php DynamicFormWidget::begin([
            'widgetContainer' => 'dynamicform_wrapper_resultados_actividad',
            'widgetBody' => '.container-items-actividad',
            'widgetItem' => '.actividad',
            'limit' => 50,
            'min' => 1,
            'insertButton' => '.add-actividad',
            'deleteButton' => '.remove-actividad',
            'model' => $model_cotizacion_taller_actividad[0],
            'formId' => 'dynamic_lista-form',
            'formFields' => [
            'descripcion',
            'cantidad',
            'precio',
            'total',
            ],
            ]); ?>


            <table class="table table-hover cotizacion-actividad-grid">
                <tbody class="container-items-actividad">
                    <?php foreach ($model_cotizacion_taller_actividad as $i => $actividad): ?>
                        <?php
                            if ( isset($actividad->tipo_servicio_taller) ) {
                                $piezas = ArrayHelper::map(PiezaServicioTaller::find()->where([ 'id_servicio_taller' => $actividad->tipo_servicio_taller ])->all(), 'id', 'nombre');
                                if ( $actividad->tipo_servicio_taller == 1 ) {
                                    $precio = $model_taller->precio_pieza;
                                } elseif ( $actividad->tipo_servicio_taller == 2 ) {
                                    $precio = $model_taller->precio_pieza_mecanica;
                                } else{
                                    $precio = 0;
                                }
                            } else{
                                $piezas = [ '' => 'Selecciona'];
                                $precio = 0;
                            }
                            
                        ?>
                    <tr class="actividad panel panel-default">
                        <td>
                            <?= $form->field($actividad, "[{$i}]tipo_servicio_taller")->dropDownList($servicios_taller,
                                [
                                    'prompt'=>'Selecciona',
                                    'class' => 'form-control tipo-servicio',
                                ] )->label('SERVICIO'); 
                            ?>
                        </td>
                        <td>
                            <?= $form->field($actividad, "[{$i}]id_pieza_servicio_taller")->dropDownList($piezas,
                                [
                                    'class' => 'form-control servicio-pieza',
                                ]);
                            ?>
                        </td>
                        <td>
                            <?= $form->field($actividad, "[{$i}]labor")->dropDownList( $lineas,
                                [
                                    'prompt'=>'Selecciona una labor',
                                    'class' => 'labor-actividad',
                                ]
                              );
                            ?>
                        </td>
                        <td>
                            <?= $form->field($model_taller, "[{$i}]precio_pieza")->label('PRECIO')->textInput([  'class' => 'precio-actividad', 'oninput'=>'obtenerPrecios()', 'value' => $precio, 'readonly' => true]) ?>
                            <?= $form->field($model_taller, "[{$i}]precio_pieza")->label(false)->hiddenInput([ 'placeholder'=>'Precio', 'value' => $precio]) ?>
                            <?= $form->field($actividad, "[{$i}]precio")->label(false)->hiddenInput([ 'placeholder'=>'Precio', 'value' => $precio]) ?>
                            <?= $form->field($model_taller, "[{$i}]id_taller")->label(false)->hiddenInput([ 'placeholder'=>'taller', 'value'=>$model_taller->id_taller, 'class'=>'id_tallers']) ?>
                        </td>
                        <td>
                            <?= $form->field($model_taller, "[{$i}]precio_temp")->textInput([ 'placeholder'=>'Precio', 'value'=>0, 'class'=>'precio-temp', 'readonly' => true ])->label('Decimal') ?>
                        </td>
                        <td>
                            <?= $form->field($actividad, "[{$i}]total_servicio")->label('TOTAL')->textInput([ 'placeholder'=>'Total', 'class' => 'precio-total', 'disabled' => true ]) ?>
                            <?= $form->field($actividad, "[{$i}]total_servicio")->label(false)->hiddenInput([ 'placeholder'=>'Total', 'class' => 'precio-total', 'disabled' => false ]) ?>
                        </td>
                          
                        <td>
                            <div class="col-xs-12 col-md-12 text-center">
                                <button type="button" class="remove-actividad btn btn-cancelar btn-xs" style="margin-top:28px;" onclick="setTimeout(function(){ obtenerPrecios(); }, 500);">
                                    <i class="glyphicon glyphicon-minus"></i></button>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php DynamicFormWidget::end(); ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td style="text-align: right; font-weight: bold;">
                            SUBTOTAL
                        </td>
                        <td id = "sub-actividad" style="text-align: center;">
                            0 RD$
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td style="text-align: right; font-weight: bold;">
                            ITBIS
                        </td>
                        <td id = "itbis-actividad" style="text-align: center;">
                            0 RD$
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td style="text-align: right; font-weight: bold;">
                            TOTAL
                        </td>
                        <td id = "total-actividad" style="text-align: center;">
                            0 RD$
                        </td>
                    </tr>
                </tfoot>
            </table>
            <button type="button" class="add-actividad btn btn-accion  btn-xs pull-right">Agregar Actividad</button>
        </div>
    </div>
    </br>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">PIEZAS SOLICITADAS</p>
            <hr>
        </div>
    </div>
    <div class="row">
    	<div class="col-md-12 col-xs-12">
            <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper_resultados',
                'widgetBody' => '.container-items',
                'widgetItem' => '.item',
                'limit' => 50,
                'min' => 1,
                'insertButton' => '.add-item',
                'deleteButton' => '.remove-item',
                'model' => $model_cotizacion_taller_pieza[0],
                'formId' => 'dynamic_lista-form',
                'formFields' => [
                'nombre',
                'id_modelo',
                'id_marca',
                ],
                ]); ?>


                <table class="table table-hover">
                    <tbody class="container-items">
                        <?php foreach ($model_cotizacion_taller_pieza as $i => $modelmodel): ?>
                        <tr class="item panel panel-default pieza">
                            <td>
                               <div class="col-xs-12 col-md-2"><?php
                               $piezas = array();
                               $tiposdepieza = array();
                               if(!$modelmodel->isNewRecord){
                                $piezas = ArrayHelper::map(Pieza::find()->where(['id_pieza'=>$modelmodel->id_pieza])->orderBy('nombre')->all(), 'id_pieza', 'nombre');
                                        //$tiposdepieza = ArrayHelper::map(TipoPieza::find()->where([ 'id' => $piezas->tipo_pieza_id ])->orderBy('nombre')->all(), 'id', 'nombre');

                            }
                            $tiposdepieza = ArrayHelper::map(TipoPieza::find()->where(['id'=>[1,4,5]])->orderBy('nombre')->all(), 'id', 'nombre');
                            echo $form->field($modelmodel, "[{$i}]tipo")->dropDownList(
                                $tiposdepieza,
                                [
                                'prompt'=>'Selecciona...',
                                'class' => 'form-control',
                                'options' => [
                                (!$modelmodel->isNewRecord)?Pieza::findOne($modelmodel->id_pieza)->tipo_pieza_id:'' => [ 'selected' => true ],
                                ],
                                'onchange'=>'
                                var ident = $(this).attr("id");
                                var nom = ident.split("-");
                                var nmodelo = "cotizaciontallerpieza-"+nom[1]+"-id_pieza";
                                $.get( "'.Url::toRoute('analista-mantenimiento/listar').'", { id: $(this).val() } )
                                .done(function( data ) {
                                  $( "#"+nmodelo ).html( null );
                                  $( "#"+nmodelo ).html( data );
                              }
                              );
                            ',
                            ] )->label('TIPO DE PIEZA'); ?>
                        </div>
                        <div class="col-xs-12 col-md-3">
                            <?php
                            echo $form->field($modelmodel, "[{$i}]id_pieza")->dropDownList(
                                $piezas,
                                [
                                'prompt'=>'Selecciona el modelo',
                                'class' => 'form-control',
                                ])->label('PIEZA');
                                ?>
                            </div>
                            <div class="col-xs-12 col-md-2">
                                <?= $form->field($modelmodel, "[{$i}]cantidad")->label('CANTIDAD')->textInput(['maxlength' => true,'placeholder'=>'Cantidad', 'type' => 'number', 'min' => 1,'class'=>'cotizaciontallerpieza', 'oninput'=>'obtenerTotalPiezas()' ]) ?>
                            </div>

                            <div class="col-xs-12 col-md-1">

                                <?= $form->field($modelmodel, "[{$i}]cotiza")->checkbox(['label'=>'','onchange'=>'habilitarPrecio(this,this.checked);', 'class' => 'cotiza-pieza'])->label('COTIZA'); ?>
                                <?= $form->field($modelmodel, "[{$i}]posicion")->label(false)->hiddenInput([ 'class' => 'posicion', 'value'=>0]) ?>
                            </div>

                            <div class="col-xs-12 col-md-2">
                                <?= $form->field($modelmodel, "[{$i}]precio")->label('PRECIO')->textInput(['maxlength' => true,'disabled'=>true,'oninput'=>'obtenerTotalPiezas()','class'=>'precio-pieza']) ?>
                                <?= $form->field($modelmodel, "[{$i}]precio")->label(false)->hiddenInput(['maxlength' => true,'disabled'=>false,'oninput'=>'obtenerTotalPiezas()','class'=>'precio-pieza']) ?>
                            
                            </div>


                            <div class="col-xs-12 col-md-1 text-center">
                              <button type="button" class="remove-item btn btn-cancelar btn-xs" style="margin-top:28px;" onclick="setTimeout(function(){ obtenerTotalPiezas(); }, 500);
    ">
                                  <i class="glyphi">
                                  <i class="glyphicon glyphicon-minus"></i></button>
                              </div>
                          </td>
                      </tr>
                  <?php endforeach; ?>
                  <?php DynamicFormWidget::end(); ?>
              </tbody>
                 <tfoot>
                    <tr>
                         
                        
                        <td style="text-align: right; font-weight: bold;">
                            SUBTOTAL
                        </td>
                        <td id = "total-pieza" style="text-align: center;">
                            0 RD$
                        </td>
                    </tr>
                    <tr>
                         
                        
                        <td style="text-align: right; font-weight: bold;">
                            ITBIS
                        </td>
                        <td id = "itbis-pieza" style="text-align: center;">
                            0 RD$
                        </td>
                    </tr>
                    <tr>
                        
                     
                         <td style="text-align: right; font-weight: bold;">
                        TOTAL
                    </td>
                    <td id = "total2-pieza" style="text-align: center;">
                        0 RD$
                    </td>
                    </tr>
                </tfoot>
             
        </table>
        <button type="button" class="add-item btn btn-accion  btn-xs pull-right">Agregar Pieza</button>
    </div>
    </div>
    </br>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">OBSERVACIONES</p>
            <hr>
        </div>
    </div>

    <?php if ($model_cotizacion_taller->estado == 3 ): ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="alert alert-warning alert-dismissible">
                    <h4><i class="icon fa fa-info"></i> KM100!</h4>
                    <?= $model_cotizacion_taller->observacion_cancela ?>
                </div>
            </div>
        </div>
        </br>
    <?php endif ?>

    <div class="row">
        <?php foreach ($mensajes as $key => $mensaje): ?>
            <div class="col-xs-12">
                <div class="media">
                    <div class="media-body">
                        <h5 class="media-heading"><strong> <?= ( isset($mensaje->user->id_taller) && $mensaje->user->id_taller != 0 )?ucfirst(Taller::findOne($mensaje->user->id_taller)->nombre):'KM100' ?> </strong></h5>
                        <p> <?= $mensaje->mensaje ?> </p>
                        <small> <?= $mensaje->fecha ?> </small>
                    </div>
                </div>
                </br>
            </div>
        <?php endforeach ?>
    </div>

    <div class="row">
        <div class="col-xs-12 col-md-12">
            <?= $form->field($model_servicio_mensaje, 'mensaje')->textarea(['rows' => 3])->label('OBSERVACIÓN') ?>
        </div>
    </div>
    </br>
 
    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">IMÁGENES DE SOPORTE</p>
            <hr>
        </div>
    </div>
    <div class="row">
    	<div class="col-md-4 col-xs-12">
            <?php

            if (empty($model_cotizacion_taller->imagen_soporte)) {
                echo $form->field($soporte_cotizacion_taller, 'imageFiles[]')->fileInput(['multiple' => true, 'accept' => 'image/*', 'class' =>"form-control btn btn-default btn-md"])->label('Imágen Placa <span class="obligatorio">*&nbsp;</span>');
            }
            else {
                $image = Html::img(Yii::$app->urlManager->baseUrl . '/' . $model_cotizacion_taller->imagen_soporte, [
                    'title'=>Yii::t('app', 'Click remove button below to remove this image'),
                    'class'=>'file-preview-image',
                    'style' => 'height: 180px; display: block;',
                    ]);

                echo '<div class="thumbnail file-preview-frame">';
                echo $image;
                echo '</div>';
                echo Html::a(
                    Yii::t('app', '<i class="fa fa-trash"></i> Eliminar'), 
                    Url::to(['/servicio/deleteimage', 'id'=>$model_cotizacion_taller->id]),
                    ['class' => 'btn btn-default']
                    );
            }
            ?>
        </div>
          <div class="col-md-4 col-xs-12">
            <?php

            if (empty($model_cotizacion_taller->imagen_soporte1)) {
                echo $form->field($soporte_cotizacion_taller, 'imageFiles1[]')->fileInput(['multiple' => true, 'accept' => 'image/*', 'class' =>"form-control btn btn-default btn-md"]);
            }
            else {
                $image = Html::img(Yii::$app->urlManager->baseUrl . '/' . $model_cotizacion_taller->imagen_soporte1, [
                    'title'=>Yii::t('app', 'Click remove button below to remove this image'),
                    'class'=>'file-preview-image',
                    'style' => 'height: 180px; display: block;',
                    ]);

                echo '<div class="thumbnail file-preview-frame">';
                echo $image;
                echo '</div>';
                echo Html::a(
                    Yii::t('app', '<i class="fa fa-trash"></i> Eliminar'), 
                    Url::to(['/servicio/deleteimage', 'id'=>$model_cotizacion_taller->id]),
                    ['class' => 'btn btn-default']
                    );
            }
            ?>
        </div>
          <div class="col-md-4 col-xs-12">
            <?php

            if (empty($model_cotizacion_taller->imagen_soporte2)) {
                echo $form->field($soporte_cotizacion_taller, 'imageFiles2[]')->fileInput(['multiple' => true, 'accept' => 'image/*', 'class' =>"form-control btn btn-default btn-md"]);
            }
            else {
                $image = Html::img(Yii::$app->urlManager->baseUrl . '/' . $model_cotizacion_taller->imagen_soporte2, [
                    'title'=>Yii::t('app', 'Click remove button below to remove this image'),
                    'class'=>'file-preview-image',
                    'style' => 'height: 180px; display: block;',
                    ]);

                echo '<div class="thumbnail file-preview-frame">';
                echo $image;
                echo '</div>';
                echo Html::a(
                    Yii::t('app', '<i class="fa fa-trash"></i> Eliminar'), 
                    Url::to(['/servicio/deleteimage', 'id'=>$model_cotizacion_taller->id]),
                    ['class' => 'btn btn-default']
                    );
            }
            ?>
        </div>
        <div class="clearfix"></div><br>
            <div class="col-md-4 col-xs-12">
                <?php

                if (empty($model_cotizacion_taller->imagen_soporte3)) {
                    echo $form->field($soporte_cotizacion_taller, 'imageFiles3[]')->fileInput(['multiple' => true, 'accept' => 'image/*', 'class' =>"form-control btn btn-default btn-md"]);
                }
                else {
                    $image = Html::img(Yii::$app->urlManager->baseUrl . '/' . $model_cotizacion_taller->imagen_soporte3, [
                        'title'=>Yii::t('app', 'Click remove button below to remove this image'),
                        'class'=>'file-preview-image',
                        'style' => 'height: 180px; display: block;',
                        ]);

                    echo '<div class="thumbnail file-preview-frame">';
                    echo $image;
                    echo '</div>';
                    echo Html::a(
                        Yii::t('app', '<i class="fa fa-trash"></i> Eliminar'), 
                        Url::to(['/servicio/deleteimage', 'id'=>$model_cotizacion_taller->id]),
                        ['class' => 'btn btn-default']
                        );
                }
                ?>
            </div>
            <div class="col-md-4 col-xs-12">
                <?php

                if (empty($model_cotizacion_taller->imagen_soporte4)) {
                    echo $form->field($soporte_cotizacion_taller, 'imageFiles4[]')->fileInput(['multiple' => true, 'accept' => 'image/*', 'class' =>"form-control btn btn-default btn-md"]);
                }
                else {
                    $image = Html::img(Yii::$app->urlManager->baseUrl . '/' . $model_cotizacion_taller->imagen_soporte4, [
                        'title'=>Yii::t('app', 'Click remove button below to remove this image'),
                        'class'=>'file-preview-image',
                        'style' => 'height: 180px; display: block;',
                        ]);

                    echo '<div class="thumbnail file-preview-frame">';
                    echo $image;
                    echo '</div>';
                    echo Html::a(
                        Yii::t('app', '<i class="fa fa-trash"></i> Eliminar'), 
                        Url::to(['/servicio/deleteimage', 'id'=>$model_cotizacion_taller->id]),
                        ['class' => 'btn btn-default']
                        );
                }
                ?>
            </div>
    </div>

    </br>
    </br>
    </br>
    </br>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="form-group text-center">
                <?= Html::submitButton('ENVIAR', ['class' => 'btn btn-submit']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>

<script>

    //Funcion referente al cambio de servicio
    function obtenerServicioPrecio(){

        $('.tipo-servicio').off().on('change', function(event) {
            event.preventDefault();
            var servicio = $(this);
            
            var tall= $('#taller-0-id_taller').val();
            
            var aux_id = servicio.attr('id');
            aux_id = aux_id.split('-');
            aux_id = aux_id[1];

            //Actualiza los precios del servicio
            $("input#taller-"+aux_id+"-precio_pieza").val( (servicio.val() == 1)? <?= $model_taller->precio_pieza ?>: <?= $model_taller->precio_pieza_mecanica ?> );
            $("input#cotizaciontalleractividad-"+aux_id+"-precio").val($("input#taller-"+aux_id+"-precio_pieza").val());
            $('#taller-'+aux_id+'-id_taller').val((tall));

            //Actualiza el listado de piezas para el servicio seleccionado
            $.get( "../getpiezas", { id: servicio.val() } )

                .done(function( data ) {
                    $("select#cotizaciontalleractividad-"+aux_id+"-id_pieza_servicio_taller").html( null );
                    $("select#cotizaciontalleractividad-"+aux_id+"-labor").val('');
                    $("select#cotizaciontalleractividad-"+aux_id+"-labor").trigger('change');
                    $("select#cotizaciontalleractividad-"+aux_id+"-id_pieza_servicio_taller").html( data );
                }
            );
        });
    }

    $(document).ready(function($) {
        obtenerPrecios();
        obtenerServicioPrecio();
        $(".labor-actividad").trigger('change');
        //$('.tipo-servicio').trigger('change');
    });

    $(document.body).on('click','.add-actividad', function () {
        obtenerServicioPrecio();
    });

    $(document.body).on('change','.labor-actividad', function () {
        var labor = $(this).val();
        var servicio = $(this).parent().parent().parent().find('select.tipo-servicio').val();
        var pieza = $(this).parent().parent().parent().find('select.servicio-pieza').val();
        var taller = $(this).parent().parent().parent().find('input.id_tallers').val();
        var categoria_vehiculo = <?= $model_vehiculo->id_categoria ?>;
        var precio = 0;
        var decimal = 0;
        var t = $(this);

        if ( labor != "") {
            if ( pieza != "") {

                $.ajax({
                    url: '../getprecio',
                    type: 'post',
                    data: {
                        servicio: servicio,
                        pieza: pieza,
                        labor: labor,
                        taller: taller,
                        categoria_vehiculo: categoria_vehiculo,
                    },
                    success: function (response) {
                        if(response){
                            switch( categoria_vehiculo ){
                                case 1:
                                    decimal = response['carro'];
                                    break;
                                case 2:
                                    decimal = response['camion'];
                                    break;
                                case 3:
                                    decimal = response['camioneta'];
                                    break;
                                case 4:
                                    decimal = response['furgoneta'];
                                    break;
                                case 5:
                                    decimal = response['suv'];
                                    break;
                                case 6:
                                    decimal = response['van'];
                                    break;
                                case 7:
                                    decimal = response['jeepeta'];
                                    break;
                                default:
                                    break;
                            }

                            switch(labor) {
                                case '0':
                                    precio = decimal;
                                    break;
                                case '1':
                                    if (decimal != 0 ) {
                                        precio = decimal + response['salvado_leve'];
                                    }
                                    break;
                                case '2':
                                    if (decimal != 0 ) {
                                        precio = decimal + response['salvado_fuerte'];
                                    }
                                    break;
                                default:
                                    break;
                            }
                            t.parent().parent().parent().find('input.precio-temp').val(precio);
                            obtenerPrecios();
                        }else{
                            alert("Labor seleccionada no tiene precio registrado.");
                        }
                    }
                });
            } else{
                alert("Debe seleccionar una pieza.");
            }
        } else{
            t.parent().parent().parent().find('input.precio-temp').val(0);
            obtenerPrecios();
        }    
    });

    $(document.body).on('change','.servicio-pieza', function () {
        var aux_id = $(this).attr('id');
        aux_id = aux_id.split('-');
        aux_id = aux_id[1];
        $("select#cotizaciontalleractividad-"+aux_id+"-labor").val('');
        $("select#cotizaciontalleractividad-"+aux_id+"-labor").trigger('change');
    });
    
    function obtenerPrecios(){
        var subtotal=0;
        $('.container-items-actividad .actividad').each(function(index, el){
            var precio_labor = $(this).find('input.precio-temp').val();
            var precio = $(this).find('input.precio-actividad').val();
            var calcularTotal = parseFloat(precio_labor) * parseFloat(precio);
            $(this).find('input.precio-total').val(calcularTotal);
        
            subtotal+=calcularTotal;

        });
        var itbis = (parseFloat(subtotal) * 0.18);
        var total = parseFloat(subtotal) + itbis;
        $('#sub-actividad').html(subtotal.toFixed(2)+' RD$');
        $('#itbis-actividad').html(itbis.toFixed(2)+' RD$');
        $('#total-actividad').html(total.toFixed(2)+' RD$');
    }

    function habilitarPrecio(field, checked){
        var checkField= $(field);   
        checkField.parent().parent().parent().parent().parent().find('input.precio-pieza').attr("disabled",!checked);
    }

    function obtenerTotalPiezas(){
        var total=0;
        $('.container-items .pieza').each(function(index, el){
            var cantidad = $(this).find('input.cotizaciontallerpieza').val();
            var precio = $(this).find('input.precio-pieza').val();
            if(precio!='' && cantidad!=''){
                var calcularTotal = parseFloat(cantidad) * parseFloat(precio);        
                total+=calcularTotal;
            }
        });
        var itbis = (parseFloat(total) * 0.18);
        var total2 = parseFloat(total) + itbis;
        $('#total-pieza').html(total.toFixed(2)+' RD$');
        $('#itbis-pieza').html(itbis.toFixed(2)+' RD$');
        $('#total2-pieza').html(total2.toFixed(2)+' RD$');
    }  

    $('.table').removeClass( "table-bordered table-striped" );
</script>


