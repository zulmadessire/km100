<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PiezaServicioTallerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Servicios de Taller';

?>
<div class="pieza-servicio-taller-index">

    <div class="row">
        <div class="col-md-1 col-xs-3">
            <i class="fa fa-suitcase fa-4x" aria-hidden="true"></i>
        </div>
        <div class="col-md-10 col-xs-9" style="padding-top: 10px;">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
        </div>
    </div>
    </br>

    <div class="row">
        <div class="col-md-12">
            <?= Html::a('Registrar Piezas', ['create'], ['class' => 'btn btn-small-gris', 'style' => 'float:right;']); ?>
        </div>
    </div>
    </br>
    </br>

    <div class="row">
        <div class="col-xs-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel'   => $searchModel,
                'tableOptions' => ['class' => 'table table-responsive', 'id' => 'grid-preventivos',],
                'summary' => '',
                'options' =>[
                    'class' => 'grid',
                ],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'id_servicio_taller',
                        'label' => 'Servicio',
                        'value' => function($model){
                            return $model->servicioTaller->nombre;
                        },
                        'filterType'=> GridView::FILTER_SELECT2,
                        'filter'=> $servicio_taller,
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                        ],
                        'filterInputOptions'=>['placeholder'=>'Selecciona'],  
                    ],
                    [
                        'attribute' => 'nombre',
                        'label' => 'Pieza',
                        'headerOptions' => ['width' => '180',],
                        'contentOptions' => ['style'=>'text-align:left;'],
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'header' => 'Automovil',
                        'attribute' => 'carro',
                        'editableOptions' => [
                            'header' => 'Decimal',
                            'placement' => 'left',
                            'inputType' => \kartik\editable\Editable::INPUT_SPIN,
                            'options'=>[
                                'pluginOptions'=>['min'=>0, 'max'=>5000,'step' => 0.1,'decimals' => 1,],
                            ],
                        ],
                        
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'headerOptions' => ['width' => '60',],
                    ],
                    /*[
                        'class' => 'kartik\grid\EditableColumn',
                        'header' => 'Jeepeta',
                        'attribute' => 'jeepeta',
                        'editableOptions' => [
                            'header' => 'Decimal',
                            'placement' => 'left',
                            'inputType' => \kartik\editable\Editable::INPUT_SPIN,
                            'options'=>[
                                'pluginOptions'=>['min'=>0, 'max'=>5000,'step' => 0.1,'decimals' => 1,],
                            ],
                        ],
                        
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'headerOptions' => ['width' => '60',],
                    ],*/
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'camioneta',
                        'editableOptions' => [
                            'header' => 'Decimal',
                            'placement' => 'left',
                            'inputType' => \kartik\editable\Editable::INPUT_SPIN,
                            'options'=>[
                                'pluginOptions'=>['min'=>0, 'max'=>5000,'step' => 0.1,'decimals' => 1,],
                            ],
                        ],
                        
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'headerOptions' => ['width' => '60',],
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'van',
                        'editableOptions' => [
                            'header' => 'Decimal',
                            'placement' => 'left',
                            'inputType' => \kartik\editable\Editable::INPUT_SPIN,
                            'options'=>[
                                'pluginOptions'=>['min'=>0, 'max'=>5000,'step' => 0.1,'decimals' => 1,],
                            ],
                        ],
                        
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'headerOptions' => ['width' => '60',],
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'camion',
                        'editableOptions' => [
                            'header' => 'Decimal',
                            'placement' => 'left',
                            'inputType' => \kartik\editable\Editable::INPUT_SPIN,
                            'options'=>[
                                'pluginOptions'=>['min'=>0, 'max'=>5000,'step' => 0.1,'decimals' => 1,],
                            ],
                        ],
                        
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'headerOptions' => ['width' => '60',],
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'furgoneta',
                        'editableOptions' => [
                            'header' => 'Decimal',
                            'placement' => 'left',
                            'inputType' => \kartik\editable\Editable::INPUT_SPIN,
                            'options'=>[
                                'pluginOptions'=>['min'=>0, 'max'=>5000,'step' => 0.1,'decimals' => 1,],
                            ],
                        ],
                        
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'headerOptions' => ['width' => '60',],
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'suv',
                        'editableOptions' => [
                            'header' => 'Decimal',
                            'placement' => 'left',
                            'inputType' => \kartik\editable\Editable::INPUT_SPIN,
                            'options'=>[
                                'pluginOptions'=>['min'=>0, 'max'=>5000,'step' => 0.1,'decimals' => 1,],
                            ],
                        ],
                        
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'headerOptions' => ['width' => '60',],
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'salvado_leve',
                        'label' => 'Salvado Leve (+)',
                        'editableOptions' => [
                            'header' => 'Decimal',
                            'placement' => 'left',
                            'inputType' => \kartik\editable\Editable::INPUT_SPIN,
                            'options'=>[
                                'pluginOptions'=>['min'=>0, 'max'=>5000,'step' => 0.1,'decimals' => 1,],
                            ],
                        ],
                        
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'headerOptions' => ['width' => '80',],
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'salvado_fuerte',
                        'label' => 'Salvado Fuerte (+)',
                        'editableOptions' => [
                            'header' => 'Decimal',
                            'placement' => 'left',
                            'inputType' => \kartik\editable\Editable::INPUT_SPIN,
                            'options'=>[
                                'pluginOptions'=>['min'=>0, 'max'=>5000,'step' => 0.1,'decimals' => 1,],
                            ],
                        ],
                        
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'headerOptions' => ['width' => '80',],
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'header' => 'Estatus',
                        'attribute' => 'estado',
                        'editableOptions' => [
                            'header' => 'Estatus',
                            'data' => [0 => 'DESHABILITADO', 1 => 'HABILITADO',],
                            'placement' => 'left',
                            'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                            'options'=>[
                                'class'=>'form-control','prompt'=>'SELECCIONA UN ESTADO'
                            ],
                            'displayValueConfig'=> [
                                                    '1' => 'HABILITADO',
                                                    '0' => 'DESHABILITADO',
                                                ],
                        ],
                        
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'headerOptions' => ['width' => '80',],
                    ],
                ],
            ]); ?>

        </div>
    </div>

</div>

<script>
    $('.table').removeClass( "table-bordered table-striped" );
</script>
