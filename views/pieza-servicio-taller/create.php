<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PiezaServicioTaller */

$this->title = 'Crear Pieza en Servicio';

?>
<div class="pieza-servicio-taller-create">

	<div class="row">
        <div class="col-md-1 col-xs-3">
            <i class="fa fa-wrench fa-4x" aria-hidden="true"></i>
        </div>
        <div class="col-md-10 col-xs-9" style="padding-top: 10px;">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
        </div>
    </div>
    </br>
    </br>

    <?= $this->render('_form', [
        'model' => $model,
        //'model_pieza' => $model_pieza,
        'servicio_taller' => $servicio_taller,
    ]) ?>

</div>
