<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PiezaServicioTaller */

$this->title = 'Update Pieza Servicio Taller: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Pieza Servicio Tallers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pieza-servicio-taller-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
