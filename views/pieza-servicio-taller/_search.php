<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PiezaServicioTallerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pieza-servicio-taller-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nombre') ?>

    <?= $form->field($model, 'carro') ?>

    <?= $form->field($model, 'camion') ?>

    <?= $form->field($model, 'camioneta') ?>

    <?php // echo $form->field($model, 'furgoneta') ?>

    <?php // echo $form->field($model, 'suv') ?>

    <?php // echo $form->field($model, 'van') ?>

    <?php // echo $form->field($model, 'jeepeta') ?>

    <?php // echo $form->field($model, 'salvado_leve') ?>

    <?php // echo $form->field($model, 'salvado_fuerte') ?>

    <?php // echo $form->field($model, 'id_servicio_taller') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
