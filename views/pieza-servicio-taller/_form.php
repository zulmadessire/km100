<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model app\models\PiezaServicioTaller */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pieza-servicio-taller-form">

    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right">
                <?= Html::a('Listado de Servicios',['index'], ['class' => 'btn btn-small-gris']); ?>
            </div>
        </div>
        </br>
    </div>
    <div class="clrearfix"></div>
    </br>

    <?php $form = ActiveForm::begin(['id' => 'dynamic_pieza-form', 'options' => ['enctype' => 'multipart/form-data'], ]); ?>

    <div class="row">
        <div class="col-md-12 col-xs-12">
            <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper_resultados',
                'widgetBody' => '.container-items',
                'widgetItem' => '.item',
                'limit' => 50,
                'min' => 1,
                'insertButton' => '.add-item',
                'deleteButton' => '.remove-item',
                'model' => $model[0],
                'formId' => 'dynamic_pieza-form',
                'formFields' => [
                    'nombre',
                    'carro',
                    'camion',
                    'camioneta',
                    'furgoneta',
                    'suv',
                    'van',
                    'salvado_leve',
                    'salvado_fuerte',
                    'id_servicio_taller',
                ],
            ]); ?>

 
            <table class="table table-hover">
                <tbody class="container-items">
                    <?php foreach ($model as $i => $pieza): ?>
                        <tr class="item panel panel-default">
                            <td>
                             <div class="col-xs-12 col-md-4">
                                <?=
                                
                                    $form->field($pieza, "[{$i}]id_servicio_taller", ['enableAjaxValidation' => true ])->dropDownList(
                                        $servicio_taller,
                                        [
                                            'prompt'=>'Selecciona el servicio',
                                            'class' => 'form-control',
                                        ] )->label('SERVICIO');
                                ?>
                            </div>
                            <div class="col-xs-12 col-md-7">
                                <?=
                                    $form->field($pieza, "[{$i}]nombre", ['enableAjaxValidation' => true])->textInput(['placeholder'=>'Pieza',])->label('PIEZA');
                                ?>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-xs-12 col-md-1">
                                <?=
                                    $form->field($pieza, "[{$i}]carro", ['enableAjaxValidation' => true])->label('Automovil');
                                ?>
                            </div>
                            <div class="col-xs-12 col-md-1">
                                <?=
                                    $form->field($pieza, "[{$i}]camioneta", ['enableAjaxValidation' => true])->label('Camioneta');
                                ?>
                            </div>
                            <div class="col-xs-12 col-md-1">
                                <?=
                                    $form->field($pieza, "[{$i}]van", ['enableAjaxValidation' => true])->label('Microbus');
                                ?>
                            </div>
                            <div class="col-xs-12 col-md-1">
                                <?=
                                    $form->field($pieza, "[{$i}]camion", ['enableAjaxValidation' => true])->label('Camion');
                                ?>
                            </div>
                            <div class="col-xs-12 col-md-1">
                                <?=
                                    $form->field($pieza, "[{$i}]furgoneta", ['enableAjaxValidation' => true])->label('Furgoneta');
                                ?>
                            </div>
                            <div class="col-xs-12 col-md-1">
                                <?=
                                    $form->field($pieza, "[{$i}]suv", ['enableAjaxValidation' => true])->label('Suv');
                                ?>
                            </div>
                            <div class="col-xs-12 col-md-2">
                                <?=
                                    $form->field($pieza, "[{$i}]salvado_leve", ['enableAjaxValidation' => true])->label('Salvado Leve (+)');
                                ?>
                            </div>
                            <div class="col-xs-12 col-md-2">
                                <?=
                                    $form->field($pieza, "[{$i}]salvado_fuerte", ['enableAjaxValidation' => true])->label('Salvado Fuerte (+)');
                                ?>
                            </div>
                            <div class="col-xs-12 col-md-1 text-center">
                                <button type="button" class="remove-item btn btn-cancelar btn-xs" style="margin-top:28px;">
                                <i class="glyphicon glyphicon-minus"></i></button>
                            </div>
                          </td>
                        </tr>
                    <?php endforeach; ?>
                    
                </tbody>
            </table>
            <div class="clearfix"></div>
            <br>
            <button type="button" class="add-item btn btn-accion  btn-xs pull-right">Agregar Pieza</button>
            <?php DynamicFormWidget::end(); ?> 
        </div>
    </div>
    <div class="clearfix"></div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div class="form-group" align="center">
        <?= Html::a('CANCELAR', ['index'], ['class' => 'btn btn-cancelar']) ?>
            &nbsp;&nbsp;&nbsp;
        <?= Html::submitButton('GUARDAR', ['class' => 'btn btn-primario']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
