<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Proveedor;
use app\models\CotizacionProveedorPieza;

$this->title = 'LISTADO DE PIEZAS DEVUELTAS';
?>
<div class="piezas-devueltas">
	<div class="row">
        <div class="col-md-1 col-xs-3">
            <i class="fa fa-list-ul fa-3x" aria-hidden="true"></i>
        </div>
        <div class="col-md-10 col-xs-9">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
        </div>
    </div>
        <div class="row">
        <div class="col-md-12">
            <?php
              echo Html::a('Listado de Solicitudes', ['/solicitud-compra-pieza/index'], ['class' => 'btn btn-small-gris', 'style' => 'float:right;']);
            ?>
        </div>
    </div>
    </br>
    </br>

    <div class="row">
    	<div class="col-xs-12">
    		<?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel'   => $searchModel,
                'summary' => '',
                'id' =>'index-devolucion-grid',
                'options' =>[
                    'class' => 'grid table-responsive',
                ],
                'columns' => [
                    ['class' => 'kartik\grid\SerialColumn'],
                    [
                        'attribute' => 'id_solicitud_compra_pieza',
                        'label' => 'No. Solicitud de compra',
                        'format' => 'text',
                        'enableSorting' => false,
                        /*'value' => function($model){
                            return OrdenCompraPieza::find()->where([ 'id_cotizacion_proveedor' => $model->id_cotizacion_proveedor ])->one()->id;
                        },
                        'filter' => Select2::widget([
                        'name' => 'CotizacionProveedorPiezaSearch[pieza]',
                        'data' => ArrayHelper::map(Pieza::find()->orderBy('nombre')->all(), 'id_pieza', 'nombre'),
                        'hideSearch' => true,
                        'options' => ['placeholder' => 'Descripción',],
                        'pluginOptions' => [
                              'allowClear' => true
                          ],
                           ]),*/
                        'headerOptions' => ['width' => '150',],
                    ],
                    [
                        'attribute' => 'id',
                        'label' => 'No. Orden de compra',
                        'format' => 'text',
                        'enableSorting' => false,
                        /*'value' => function($model){
                            return OrdenCompraPieza::find()->where([ 'id_cotizacion_proveedor' => $model->id_cotizacion_proveedor ])->one()->id;
                        },
                        'filter' => Select2::widget([
                        'name' => 'CotizacionProveedorPiezaSearch[pieza]',
                        'data' => ArrayHelper::map(Pieza::find()->orderBy('nombre')->all(), 'id_pieza', 'nombre'),
                        'hideSearch' => true,
                        'options' => ['placeholder' => 'Descripción',],
                        'pluginOptions' => [
                              'allowClear' => true
                          ],
                           ]),*/
                        'headerOptions' => ['width' => '150',],
                    ],
                    [
                        'attribute' => 'id_proveedor',
                        'label' => 'Proveedor',
                        'format' => 'text',
                        'enableSorting' => false,
                        'value' => function($model){
                            return Proveedor::findOne($model->id_proveedor)->nombre;
                        },
                        'filterType'=> GridView::FILTER_SELECT2,
                        'filter'=> $proveedores,
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                        ],
                        'filterInputOptions'=>['placeholder'=>'Selecciona'],
                        /*'filter' => Select2::widget([
                        'name' => 'CotizacionProveedorPiezaSearch[pieza]',
                        'data' => ArrayHelper::map(Pieza::find()->orderBy('nombre')->all(), 'id_pieza', 'nombre'),
                        'hideSearch' => true,
                        'options' => ['placeholder' => 'Descripción',],
                        'pluginOptions' => [
                              'allowClear' => true
                          ],
                           ]),*/
                        'headerOptions' => ['width' => '400',],
                    ],
                    [
                        'attribute' => 'fecha_devolucion',
                        'label' => 'Fecha Devolución',
                        'format' => 'text',
                        'enableSorting' => false,
                        'value' => function($model){
                            return date('d/m/Y', strtotime(CotizacionProveedorPieza::find()->where(['id_cotizacion_proveedor' => $model->id_cotizacion_proveedor ])->one()->fecha_devolucion));
                        },
                        'filterType'=> GridView::FILTER_DATE,
                        'filter' => true,
                        /*'filter' => Select2::widget([
                        'name' => 'CotizacionProveedorPiezaSearch[pieza]',
                        'data' => ArrayHelper::map(Pieza::find()->orderBy('nombre')->all(), 'id_pieza', 'nombre'),
                        'hideSearch' => true,
                        'options' => ['placeholder' => 'Descripción',],
                        'pluginOptions' => [
                              'allowClear' => true
                          ],
                           ]),*/
                        //'headerOptions' => ['width' => '100',],
                    ],
                ],
            ]); ?>
    	</div>
    </div>
</div>

<script>
	$('.table').removeClass( "table-bordered table-striped" );
</script>