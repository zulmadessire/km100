<!-- Modal Solciitar Cotizacion a Proveedores -->
<div id="solicitarCotizacionModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="title"><img src="<?= Yii::$app->request->baseUrl ?>/img/give_money_64.png" alt="" style="height: 40px;">&nbsp;&nbsp; SOLICITUD DE COTIZACIÓN DE PIEZA</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true" style="color: #000; font-weight: bold;"></i></button>
          </div>
        <div class="modal-body">
            </br>
            <?php $form = ActiveForm::begin([ 'id' => 'solicitar', 'action' => ['/solicitud-compra-pieza/solicitudcotizacion',] ]); ?>
                <div class="row">
                    <div class="col-xs-12 col-md-11 col-md-offset-1">
                        <label class="control-label" for="garantiavehiculo-motivo">PROVEEDORES</label>
                        <div id="response-proveedor" class="checkbox">
                        </div>
                    </div>
                </div>

            <?php ActiveForm::end(); ?>
            </br>
            <?php
            echo '<div class="row" style="padding-top: 25px;">';
                echo '<div class="text-center">';
                    echo Html::button('CANCELAR', ['class' => 'btn btn-cancelar', 'data' => ['dismiss' => 'modal']]);
                    echo "&nbsp;";
                    echo Html::submitButton('CONTINUAR', ['class' => 'btn btn-aceptar', 'form' => 'solicitar', 'id' => 'submit-solicitar' ]);
                echo '</div>';
            echo '</div>';
            ?>
        </div>
    </div>
  </div>
</div>
<!-- Fin modal-->