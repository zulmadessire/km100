<?php 
use kartik\grid\GridView;
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\Url;

$count_pipeline = count($pipeline_compra_pieza);
?>

<div class="expand-compra-pieza">
	<div class="row">
        <?php if ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 1 || $pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 4 ): ?>
            <div class="col-xs-12">
                <div class="pull-right">
                    <div class="col-xs-2">
                        <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle btn-sm" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i>
                            <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <?php if ( $pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 1 ) { ?>
                                    <li> <?= Html::a('<span><i class="fa fa-eye fa-sm" aria-hidden="true"></i></span> Ver cotizaciones recibidas',[ 'solicitud-compra-pieza/cotizacionesrecibidas', 'id' => $model->id ]) ?></li>
                                    <?php if ( $model->aprobacion == 1 ) { ?>
                                        <li> <?= Html::a('<span><i class="fa fa-check fa-sm" aria-hidden="true"></i></span> Solicitar probación',[ 'solicitud-compra-pieza/solicitudprobacion', 'id' => $model->id ]) ?></li>
                                    <?php } ?>
                                <?php } elseif ( $pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 4 ) { ?>
                                    <li> <?= Html::a('<span><i class="fa fa-truck fa-sm" aria-hidden="true"></i></span> Despachar piezas',[ 'solicitud-compra-pieza/despacho', 'id' => $model->id ]) ?></li>
                                <?php } ?>
                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif ?>
        <div class="col-xs-10 col-xs-offset-1">
            <div class="bs-wizard" style="border-bottom:0;">
                <div class="bs-wizard-step complete">
                    <div class="bs-wizard-marker <?= ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 0)?'active':''?> text-center">
                        <?= ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 0)?'<i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>':''?>
                    </div>
                    <div class="bs-wizard-days <?= ( 0 < $count_pipeline )?'active':'no-days' ?><?php // ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 0)?'active':''?> text-center">
                        <?php //($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 0)?date_diff(new DateTime("now"), new DateTime($pipeline_compra_pieza[0]->fecha))->days.' DÍAS':''?>
                        <?php
                            if ( $pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 0 ) {
                                echo date_diff(new DateTime("now"), new DateTime($pipeline_compra_pieza[0]->fecha))->days.' DÍAS';
                            } else{
                                if ( 0 < $count_pipeline ) {
                                    for ( $i = 1;  $i < $count_pipeline;  $i++ ) { 
                                        if ( $pipeline_compra_pieza[$i]->idEstadoCompraPieza->codigo == 0 ) {
                                            echo date_diff(new DateTime($pipeline_compra_pieza[$i-1]->fecha), new DateTime($pipeline_compra_pieza[$i]->fecha))->days.' DÍAS';
                                            break;
                                        }
                                    }
                                }
                            }
                        ?>
                    </div>
                    <div class="progress"><div class="progress-bar"></div></div>
                    <?php 
                        echo '<div class="bs-wizard-dot '; 
                        if ( 0 < $count_pipeline ) {
                            for ( $i = 1;  $i < $count_pipeline;  $i++ ) { 
                                if ( $pipeline_compra_pieza[$i]->idEstadoCompraPieza->codigo == 0 ) {
                                    if (date_diff(new DateTime($pipeline_compra_pieza[$i-1]->fecha), new DateTime($pipeline_compra_pieza[$i]->fecha))->days > $pipeline_compra_pieza[$i]->idEstadoCompraPieza->dias_optimos) {
                                        echo 'active-red';
                                    } else{
                                        echo 'active-green';
                                    }
                                    break;
                                }
                            }
                        }
                        echo '"></div>';
                    ?>
                    <div class="bs-wizard-stepnum text-center">Asignado</div>
                </div>
                <div class="bs-wizard-step complete">
                    <div class="bs-wizard-marker <?= ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 1)?'active':''?> text-center">
                        <?= ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 1)?'<i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>':''?>
                    </div>
                    <div class="bs-wizard-days <?= ( 1 < $count_pipeline )?'active':'no-days' ?><?php // ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 1)?'active':''?> text-center">
                        <?php //($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 1)?date_diff(new DateTime("now"), new DateTime($pipeline_compra_pieza[0]->fecha))->days.' DÍAS':''?>
                        <?php
                            if ( $pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 1 ) {
                                echo date_diff(new DateTime("now"), new DateTime($pipeline_compra_pieza[0]->fecha))->days.' DÍAS';
                            } else{
                                if ( 1 < $count_pipeline ) {
                                    for ( $i = 1;  $i < $count_pipeline;  $i++ ) { 
                                        if ( $pipeline_compra_pieza[$i]->idEstadoCompraPieza->codigo == 1 ) {
                                            echo date_diff(new DateTime($pipeline_compra_pieza[$i-1]->fecha), new DateTime($pipeline_compra_pieza[$i]->fecha))->days.' DÍAS';
                                            break;
                                        }
                                    }
                                }
                            }
                        ?>
                    </div>
                    <div class="progress"><div class="progress-bar"></div></div>
                    <?php 
                        echo '<div class="bs-wizard-dot '; 
                        if ( 1 < $count_pipeline ) {
                            for ( $i = 1;  $i < $count_pipeline;  $i++ ) { 
                                if ( $pipeline_compra_pieza[$i]->idEstadoCompraPieza->codigo == 1 ) {
                                    if (date_diff(new DateTime($pipeline_compra_pieza[$i-1]->fecha), new DateTime($pipeline_compra_pieza[$i]->fecha))->days > $pipeline_compra_pieza[$i]->idEstadoCompraPieza->dias_optimos) {
                                        echo 'active-red';
                                    } else{
                                        echo 'active-green';
                                    }
                                    break;
                                }
                            }
                        }
                        echo '"></div>';
                    ?>
                    <div class="bs-wizard-stepnum text-center">Espera de cotización</div>
                </div>
                <div class="bs-wizard-step complete">
                    <div class="bs-wizard-marker <?= ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 2)?'active':''?> text-center">
                        <?= ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 2)?'<i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>':''?>
                    </div>
                    <div class="bs-wizard-days <?= ( 2 < $count_pipeline )?'active':'no-days' ?><?php // ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 2)?'active':''?> text-center">
                        <?php // ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 2)?date_diff(new DateTime("now"), new DateTime($pipeline_compra_pieza[0]->fecha))->days.' DÍAS':''?>
                        <?php
                            if ( $pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 2 ) {
                                echo date_diff(new DateTime("now"), new DateTime($pipeline_compra_pieza[0]->fecha))->days.' DÍAS';
                            } else{
                                if ( 2 < $count_pipeline ) {
                                    for ( $i = 1;  $i < $count_pipeline;  $i++ ) { 
                                        if ( $pipeline_compra_pieza[$i]->idEstadoCompraPieza->codigo == 2 ) {
                                            echo date_diff(new DateTime($pipeline_compra_pieza[$i-1]->fecha), new DateTime($pipeline_compra_pieza[$i]->fecha))->days.' DÍAS';
                                            break;
                                        }
                                    }
                                }
                            }
                        ?>
                    </div>
                    <div class="progress"><div class="progress-bar"></div></div>
                    <?php 
                        echo '<div class="bs-wizard-dot '; 
                        if ( 2 < $count_pipeline ) {
                            for ( $i = 1;  $i < $count_pipeline;  $i++ ) { 
                                if ( $pipeline_compra_pieza[$i]->idEstadoCompraPieza->codigo == 2 ) {
                                    if (date_diff(new DateTime($pipeline_compra_pieza[$i-1]->fecha), new DateTime($pipeline_compra_pieza[$i]->fecha))->days > $pipeline_compra_pieza[$i]->idEstadoCompraPieza->dias_optimos) {
                                        echo 'active-red';
                                    } else{
                                        echo 'active-green';
                                    }
                                    break;
                                }
                            }
                        }
                        echo '"></div>';
                    ?>
                    <div class="bs-wizard-stepnum text-center">Aprobación</div>
                </div>
                <div class="bs-wizard-step complete">
                    <div class="bs-wizard-marker <?= ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 3)?'active':''?> text-center">
                        <?= ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 3)?'<i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>':''?>
                    </div>
                    <div class="bs-wizard-days <?= ( 3 < $count_pipeline )?'active':'no-days' ?><?php //($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 3)?'active':''?> text-center">
                        <?php // ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 3)?date_diff(new DateTime("now"), new DateTime($pipeline_compra_pieza[0]->fecha))->days.' DÍAS':''?>
                        <?php
                            if ( $pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 3 ) {
                                echo date_diff(new DateTime("now"), new DateTime($pipeline_compra_pieza[0]->fecha))->days.' DÍAS';
                            } else{
                                if ( 3 < $count_pipeline ) {
                                    for ( $i = 1;  $i < $count_pipeline;  $i++ ) { 
                                        if ( $pipeline_compra_pieza[$i]->idEstadoCompraPieza->codigo == 3 ) {
                                            echo date_diff(new DateTime($pipeline_compra_pieza[$i-1]->fecha), new DateTime($pipeline_compra_pieza[$i]->fecha))->days.' DÍAS';
                                            break;
                                        }
                                    }
                                }
                            }
                        ?>
                    </div>
                    <div class="progress"><div class="progress-bar"></div></div>
                    <?php 
                        echo '<div class="bs-wizard-dot '; 
                        if ( 3 < $count_pipeline ) {
                            for ( $i = 1;  $i < $count_pipeline;  $i++ ) { 
                                if ( $pipeline_compra_pieza[$i]->idEstadoCompraPieza->codigo == 3 ) {
                                    if (date_diff(new DateTime($pipeline_compra_pieza[$i-1]->fecha), new DateTime($pipeline_compra_pieza[$i]->fecha))->days > $pipeline_compra_pieza[$i]->idEstadoCompraPieza->dias_optimos) {
                                        echo 'active-red';
                                    } else{
                                        echo 'active-green';
                                    }
                                    break;
                                }
                            }
                        }
                        echo '"></div>';
                    ?>
                    <div class="bs-wizard-stepnum text-center">Por recibir pieza</div>
                </div>
                <div class="bs-wizard-step complete">
                    <div class="bs-wizard-marker <?= ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 4)?'active':''?> text-center">
                        <?= ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 4)?'<i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>':''?>
                    </div>
                    <div class="bs-wizard-days <?= ( 4 < $count_pipeline )?'active':'no-days' ?><?php // ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 4)?'active':''?> text-center">
                        <?php // ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 4)?date_diff(new DateTime("now"), new DateTime($pipeline_compra_pieza[0]->fecha))->days.' DÍAS':''?>
                        <?php
                            if ( $pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 4 ) {
                                echo date_diff(new DateTime("now"), new DateTime($pipeline_compra_pieza[0]->fecha))->days.' DÍAS';
                            } else{
                                if ( 4 < $count_pipeline ) {
                                    for ( $i = 1;  $i < $count_pipeline;  $i++ ) { 
                                        if ( $pipeline_compra_pieza[$i]->idEstadoCompraPieza->codigo == 4 ) {
                                            echo date_diff(new DateTime($pipeline_compra_pieza[$i-1]->fecha), new DateTime($pipeline_compra_pieza[$i]->fecha))->days.' DÍAS';
                                            break;
                                        }
                                    }
                                }
                            }
                        ?>
                    </div>
                    <div class="progress"><div class="progress-bar"></div></div>
                    <?php 
                        echo '<div class="bs-wizard-dot '; 
                        if ( 4 < $count_pipeline ) {
                            for ( $i = 1;  $i < $count_pipeline;  $i++ ) { 
                                if ( $pipeline_compra_pieza[$i]->idEstadoCompraPieza->codigo == 4 ) {
                                    if (date_diff(new DateTime($pipeline_compra_pieza[$i-1]->fecha), new DateTime($pipeline_compra_pieza[$i]->fecha))->days > $pipeline_compra_pieza[$i]->idEstadoCompraPieza->dias_optimos) {
                                        echo 'active-red';
                                    } else{
                                        echo 'active-green';
                                    }
                                    break;
                                }
                            }
                        }
                        echo '"></div>';
                    ?>
                    <div class="bs-wizard-stepnum text-center">Asignación de ruta</div>
                </div>
                <div class="bs-wizard-step complete">
                    <div class="bs-wizard-marker <?= ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 5)?'active':''?> text-center">
                        <?= ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 5)?'<i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>':''?>
                    </div>
                    <div class="bs-wizard-days <?= ( 5 < $count_pipeline )?'active':'no-days' ?><?php // ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 5)?'active':''?> text-center">
                        <?php // ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 5)?date_diff(new DateTime("now"), new DateTime($pipeline_compra_pieza[0]->fecha))->days.' DÍAS':''?>
                        <?php
                            if ( $pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 5 ) {
                                echo date_diff(new DateTime("now"), new DateTime($pipeline_compra_pieza[0]->fecha))->days.' DÍAS';
                            } else{
                                if ( 5 < $count_pipeline ) {
                                    for ( $i = 1;  $i < $count_pipeline;  $i++ ) { 
                                        if ( $pipeline_compra_pieza[$i]->idEstadoCompraPieza->codigo == 5 ) {
                                            echo date_diff(new DateTime($pipeline_compra_pieza[$i-1]->fecha), new DateTime($pipeline_compra_pieza[$i]->fecha))->days.' DÍAS';
                                            break;
                                        }
                                    }
                                }
                            }
                        ?>

                    </div>
                    <div class="progress"><div class="progress-bar"></div></div>
                    <?php 
                        echo '<div class="bs-wizard-dot '; 
                        if ( 5 < $count_pipeline ) {
                            for ( $i = 1;  $i < $count_pipeline;  $i++ ) { 
                                if ( $pipeline_compra_pieza[$i]->idEstadoCompraPieza->codigo == 5 ) {
                                    if (date_diff(new DateTime($pipeline_compra_pieza[$i-1]->fecha), new DateTime($pipeline_compra_pieza[$i]->fecha))->days > $pipeline_compra_pieza[$i]->idEstadoCompraPieza->dias_optimos) {
                                        echo 'active-red';
                                    } else{
                                        echo 'active-green';
                                    }
                                    break;
                                }
                            }
                        }
                        echo '"></div>';
                    ?>
                    <div class="bs-wizard-stepnum text-center">Pieza entregada</div>
                </div>
            </div>
        </div>
    </div>
    </br></br>
	<div class="row">
		<div class="col-xs-12">
            <p><b>FECHA DE ASIGNACIÓN: </b> <?= date('d/m/Y', strtotime($model->fecha)) ?></p>
        </div>
	</div>
</div>
<style>
	.bs-wizard {}

    /*Form Wizard*/
    .bs-wizard {border-bottom: solid 1px #e0e0e0; padding: 0 0 10px 0;}
    .bs-wizard > .bs-wizard-step {padding: 0; position: relative; width: 16.6%; float: left;}
    .bs-wizard > .bs-wizard-step .bs-wizard-stepnum {color: #000; font-size: 8pt; font-weight: bold;}
    .bs-wizard > .bs-wizard-step .bs-wizard-info {color: #000; font-size: 8pt;}
    .bs-wizard > .bs-wizard-step > .bs-wizard-dot {position: absolute; width: 30px; height: 30px; display: block; background: #767171; top: 65px; left: 50%; margin-top: -15px; margin-left: -15px; border-radius: 50%;} 
    .bs-wizard > .bs-wizard-step > .bs-wizard-dot:after {content: ' '; width: 16px; height: 16px; background: #fff; border-radius: 50px; position: absolute; top: 7px; left: 7px; }
    .bs-wizard > .bs-wizard-step > .bs-wizard-dot.active-green:after { background: #a9d18e; }
    .bs-wizard > .bs-wizard-step > .bs-wizard-dot.active-red:after { background: #e08282; }
    .bs-wizard > .bs-wizard-step > .progress {position: relative; border-radius: 0px; height: 6px; box-shadow: none; margin: 20px 0; background: #767171;}
    .bs-wizard > .bs-wizard-step > .bs-wizard-marker { margin-bottom: 27px;}
    .bs-wizard > .bs-wizard-step > .bs-wizard-marker.active{ color: #b34b4b; content: ' '; margin-bottom: 0px;}
    .bs-wizard > .bs-wizard-step > .bs-wizard-days.no-days{ padding-top: 15px; }
    .bs-wizard > .bs-wizard-step > .bs-wizard-days { margin-bottom: 0px;}
    .bs-wizard > .bs-wizard-step > .bs-wizard-days.active{ color: #757972; content: ' '; margin-bottom: 0px; font-size: 8pt; font-weight: bold;}
    /*END Form Wizard*/

    .content-pipeline{
        border: 1px solid #dedede;
        height: 200px;
    }
	.expand-compra-pieza{
        padding: 10px 30px 10px 30px;
        text-align: left;        
        border: 1px solid #dedede;
        margin-bottom: 30px;
    }
    .grid .dropdown-menu {
        right: -60px;
        left: unset;
    }

</style>