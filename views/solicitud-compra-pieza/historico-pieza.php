<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\db\Query;
use app\models\SolicitudCompraPieza;
use app\models\OrdenCompraPieza;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
use kartik\date\DatePicker;
use app\models\CotizacionProveedor;
use app\models\SolicitudCompraProveedor;
use app\models\Pieza;
use app\models\Proveedor;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PartesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'HISTORICO DE PIEZA';
?>
<div class="index-orden-compra">

    <div class="row">
        <div class="col-md-1 col-xs-3">
            <i class="fa fa-list-ul fa-3x" aria-hidden="true"></i>
        </div>
        <div class="col-md-10 col-xs-9">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?> - <?= $pieza->nombre ?></span></h4>
        </div>
    </div>
    </br>

    <div class="row">
        <br>
        <div class="col-xs-12">
        <?php 
            if (!empty($dataProvider->getModels())) :
                $costo = 0;
                $total = 0;
                foreach ($dataProvider->getModels() as $key => $val) :  
                    $total += (($val->cantidad_comprar * $val->costo) - ((($val->cantidad_comprar * $val->costo) * $val->ahorro)/ 100));
                    $costo += $val->costo;
                endforeach;
                echo '<h4><b>Costo promedio:</b> ' . $costo / ($key + 1) . ' RD$</h4>';
                echo '<h4><b>Gasto total:</b> ' . $total . ' RD$</h4>';
             endif;
        ?> 
        </div>
    </div>    
    <div class="row">    
        <br>
        <div class="col-xs-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                //'filterModel' => $searchModel,
                'tableOptions' => ['class' => 'table table-responsive'],
                'summary' => '',
                'options' =>[
                    'class' => 'grid',
                ],
                'columns' => [
                    // [
                    //     'attribute' => 'proveedor',
                    //     'label' => 'Proveedor',
                    //     'format' => 'text',
                    //     'enableSorting' => false,
                    //     'filter' => false,
                    //     'value' => function($model){
                    //         $model_orden_compra_pieza = OrdenCompraPieza::findOne(['id_cotizacion_proveedor' => $model->id_cotizacion_proveedor]);
                    //         $model_proveedor = Proveedor::findOne(['id' => $model_orden_compra_pieza->id_proveedor]);
                    //         return $model_proveedor->nombre;
                    //     }
                    // ], 
                    [
                        'attribute' => 'fecha',
                        'label' => 'Fecha',
                        'format' => 'text',
                        'enableSorting' => false,
                        'filter' => false,
                        'value' => function($model){
                            $model_orden_compra_pieza = OrdenCompraPieza::findOne(['id_cotizacion_proveedor' => $model->id_cotizacion_proveedor]);
                            return date( 'd/m/Y', strtotime($model_orden_compra_pieza->fecha) );
                        }
                    ],                    
                    [
                        'attribute' => 'cantidad_comprar',
                        'label' => 'Cantidad',
                        'format' => 'text',
                        'enableSorting' => false,
                        'filter' => false,
                    ],
                    [
                        'attribute' => 'costo',
                        'label' => 'Costo por pieza',
                        'format' => 'text',
                        'enableSorting' => false,
                        'filter' => false,                        
                        'value' => function($model){
                            return $model->costo. ' RD$';
                        }
                    ],
                    [
                        'attribute' => 'ahorro',
                        'label' => 'Descuento',
                        'format' => 'text',
                        'enableSorting' => false,
                        'filter' => false,
                        'value' => function($model){
                            return $model->ahorro. '%';
                        }
                    ],
                    [
                        'label' => 'Costo total',
                        'format' => 'text',
                        'enableSorting' => false,
                        'value' => function($model){
                            return (($model->cantidad_comprar * $model->costo) - ((($model->cantidad_comprar * $model->costo) * $model->ahorro)/ 100)) . ' RD$';
                        }
                    ],
                ],
            ]);  ?>
        </div>
    </div>
</div>