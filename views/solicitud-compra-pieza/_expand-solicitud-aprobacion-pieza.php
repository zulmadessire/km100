<?php 
use yii\helpers\Html;
use kartik\select2\Select2;
use kartik\form\ActiveForm;

?>

<div class="expand-aprobacion-pieza">
	<div class="row">
		<div class="col-md-4 col-md-offset-1 col-xs-12">
			<div class="form-group">
                <label class="control-label" for="id_estado"><b>LISTADO DE PIEZAS</b></label>
                <?=  
                    Select2::widget([
                        'name' => 'id_pieza[]',
                        'data' => $piezas,
                        'options' => ['placeholder' => 'Selecciona la pieza', 'class' =>'id-pieza'],
                        'pluginOptions' => [ 'allowClear'=>true ],
                    ]); 
                ?>
            </div>
		</div>
		<div class="clearfix"></div>
		<div class="col-xs-10 col-xs-offset-1">
			<?php $form = ActiveForm::begin(['id' => 'aprobacion-form-'.$model->id, 'action' => ['solicitud-compra-pieza/aprobarsolicitud'] ] ); ?>
			<table class="table grid-blue table-responsive table-bordered">
				<thead>
					<tr>
						<th>Descripción</th>
						<th>Cantidad</th>
						<th>Proveedor</th>
						<th>Fecha entrega</th>
						<th>Costo</th>
						<th>Aprobar</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($cotizaciones_piezas as $i => $cotizacion): ?>
						<tr class="row-pieza" data-pieza="<?= $cotizacion->idCotizacionTallerPieza->id_pieza ?>">
							<td><?= ucfirst($cotizacion->idCotizacionTallerPieza->idPieza->nombre) ?></td>
							<td><?= $cotizacion->cantidad_comprar ?></td>
							<td><?= $cotizacion->idCotizacionProveedor->idSolicitudCompraProveedor->idProveedor->nombre ?></td>
							<td><?= date('d/m/Y', strtotime($cotizacion->fecha_entrega)) ?></td>
							<td><?= 'RD$ '.$cotizacion->costo ?> </td>
							<td><?= Html::checkbox('piezas[]', false, ['style' => 'display: block;margin-right: auto;margin-left: auto;', 'value' => $cotizacion->id, 'class' => 'cotizacion-pieza']); ?></td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
	</br>
	</br>
	<div class="row">
		<div class="col-xs-11">
			<div class="pull-right">
				<?= Html::a('Rechazar compra',[ '/solicitud-compra-pieza/index' ], ['class' => 'btn btn-cancelar']) ?>
				&nbsp;
				<?= Html::a('Aprobar compra','#', ['class' => 'btn btn-aceptar aprobar-compra', 'data' => ['id' => $model->id, ]]) ?>
			</div>
		</div>
	</div>
</div>
<style>
	.expand-aprobacion-pieza{
        padding: 10px 30px 20px 30px;
        text-align: left;
    }
</style>
<script>
	$('.id-pieza').on('change', function(event) {
		event.preventDefault();
		/* Act on the event */

		var id_pieza = $(this).val();
		$('tr.row-pieza').hide();
		if ( id_pieza == '') {
			$('.row-pieza').show();
		} else{
			$('tr[data-pieza='+id_pieza+']').show();
		}
	});
	$('.cotizacion-pieza').on('change', function(event) {
		event.preventDefault();
		/* Act on the event */
		var id_pieza = $(this).closest( "tr" ).data('pieza');

		if ( $(this).is(':checked') ) {
			$('tr[data-pieza='+id_pieza+'] .cotizacion-pieza').prop({
				disabled: true,
			});

			$(this).prop({
				disabled: false,
			});
		} else{
			$('tr[data-pieza='+id_pieza+'] .cotizacion-pieza').prop({
				disabled: false,
			});
		}
	});
	$('.aprobar-compra').on('click', function(event) {
		event.preventDefault();
        event.stopImmediatePropagation();

        var id = $(this).data('id');
        var form = $('#aprobacion-form-'+id);
        var data = form.serializeArray();
        data.push({name: 'id', value: id});

        $.post( form.attr("action"), data ).done(function(response){
            location.reload();
        });
	});
</script>