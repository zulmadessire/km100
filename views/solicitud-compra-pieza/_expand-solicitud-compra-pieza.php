<?php 
use kartik\grid\GridView;
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\Url;
use kartik\form\ActiveForm;

$count_pipeline = count($pipeline_compra_pieza);
?>

<div class="expand-compra-pieza">
	<div class="row">
        <?php if ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 1 || $model->aprobacion == 5 || $pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 4 ): ?>
            <div class="col-xs-12">
                <div class="pull-right">
                    <?php if ($model->aprobacion == 5 && \Yii::$app->user->can('analista-compras')): ?>
                        <div class="col-xs-9">                    
                            <?php
                                if($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 2 && $model->aprobacion == 5 && \Yii::$app->user->can('analista-compras')) {
                                    echo "
                                    <div class='callout callout-danger'>                
                                        <p>
                                            La cotización fue rechazada </br>
                                            <strong>Motivo:</strong> ".$model->motivo_rechazo."

                                        </p>
                                    </div>
                                    ";
                                }
                            ?>
                        </div>
                    <?php endif; ?>
                    <div class="col-xs-2">
                        <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle btn-sm" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i>
                            <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <?php if ( $pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 1 && \Yii::$app->user->can('analista-compras')) { ?>
                                    <li> <?= Html::a('<span><i class="fa fa-list-ul fa-sm" aria-hidden="true"></i></span> Ver cotizaciones recibidas',[ 'solicitud-compra-pieza/cotizacionesrecibidas', 'id' => $model->id ]) ?></li>
                                    <?php if ( $model->aprobacion == 1 && \Yii::$app->user->can('analista-compras')) { ?>
                                        <li> <?= Html::a('<span><i class="fa fa-check fa-sm" aria-hidden="true"></i></span> Solicitar probación',[ 'solicitud-compra-pieza/solicitudprobacion', 'id' => $model->id ]) ?></li>
                                    <?php } ?>
                                <?php } elseif ( $pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 2 && $model->aprobacion == 5 && \Yii::$app->user->can('analista-compras')) { ?>
                                    <li> 
                                        <?= Html::a('<span><i class="fa fa-list-ul fa-sm" aria-hidden="true"></i></span> Ver cotizaciones',[ 'solicitud-compra-pieza/cotizacionesrecibidas', 'id' => $model->id ]) ?>
                                    </li>
                                <?php } elseif ( $pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 4 && \Yii::$app->user->can('admin-logistica')) { ?>
                                    <li> <?= Html::a('<span><i class="fa fa-truck fa-sm" aria-hidden="true"></i></span> Despachar piezas',[ 'solicitud-compra-pieza/despacho', 'id' => $model->id ]) ?></li>
                                <?php } ?>
                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif ?>
        <?php if($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 7){ ?>
            <h3 class="text-center">Servicio Cancelado</h3>
        <?php }else{ ?>
        <div class="col-xs-10 col-xs-offset-1">
            <div class="bs-wizard" style="border-bottom:0;">
                <div class="bs-wizard-step complete">
                    <div class="bs-wizard-marker <?= ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 0)?'active':''?> text-center">
                        <?= ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 0)?'<i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>':''?>
                    </div>
                    <div class="bs-wizard-days <?= ( 0 < $count_pipeline )?'active':'no-days' ?><?php // ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 0)?'active':''?> text-center">
                        <?php //($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 0)?date_diff(new DateTime("now"), new DateTime($pipeline_compra_pieza[0]->fecha))->days.' DÍAS':''?>
                        <?php
                            if ( $pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 0 ) {
                                echo date_diff(new DateTime("now"), new DateTime($pipeline_compra_pieza[0]->fecha))->days.' DÍAS';
                            } else{
                                if ( 0 < $count_pipeline ) {
                                    for ( $i = 1;  $i < $count_pipeline;  $i++ ) { 
                                        if ( $pipeline_compra_pieza[$i]->idEstadoCompraPieza->codigo == 0 ) {
                                            echo date_diff(new DateTime($pipeline_compra_pieza[$i-1]->fecha), new DateTime($pipeline_compra_pieza[$i]->fecha))->days.' DÍAS';
                                            break;
                                        }
                                    }
                                }
                            }
                        ?>
                    </div>
                    <div class="progress"><div class="progress-bar"></div></div>
                    <?php 
                        echo '<div class="bs-wizard-dot '; 
                        if ( 0 < $count_pipeline ) {
                            for ( $i = 1;  $i < $count_pipeline;  $i++ ) { 
                                if ( $pipeline_compra_pieza[$i]->idEstadoCompraPieza->codigo == 0 ) {
                                    if (date_diff(new DateTime($pipeline_compra_pieza[$i-1]->fecha), new DateTime($pipeline_compra_pieza[$i]->fecha))->days > $pipeline_compra_pieza[$i]->idEstadoCompraPieza->dias_optimos) {
                                        echo 'active-red';
                                    } else{
                                        echo 'active-green';
                                    }
                                    break;
                                }
                            }
                        }
                        echo '"></div>';
                    ?>
                    <div class="bs-wizard-stepnum text-center">Asignado</div>
                </div>
                <div class="bs-wizard-step complete">
                    <div class="bs-wizard-marker <?= ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 1)?'active':''?> text-center">
                        <?= ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 1)?'<i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>':''?>
                    </div>
                    <div class="bs-wizard-days <?= ( 1 < $count_pipeline )?'active':'no-days' ?><?php // ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 1)?'active':''?> text-center">
                        <?php //($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 1)?date_diff(new DateTime("now"), new DateTime($pipeline_compra_pieza[0]->fecha))->days.' DÍAS':''?>
                        <?php
                            if ( $pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 1 ) {
                                echo date_diff(new DateTime("now"), new DateTime($pipeline_compra_pieza[0]->fecha))->days.' DÍAS';
                            } else{
                                if ( 1 < $count_pipeline ) {
                                    for ( $i = 1;  $i < $count_pipeline;  $i++ ) { 
                                        if ( $pipeline_compra_pieza[$i]->idEstadoCompraPieza->codigo == 1 ) {
                                            echo date_diff(new DateTime($pipeline_compra_pieza[$i-1]->fecha), new DateTime($pipeline_compra_pieza[$i]->fecha))->days.' DÍAS';
                                            break;
                                        }
                                    }
                                }
                            }
                        ?>
                    </div>
                    <div class="progress"><div class="progress-bar"></div></div>
                    <?php 
                        echo '<div class="bs-wizard-dot '; 
                        if ( 1 < $count_pipeline ) {
                            for ( $i = 1;  $i < $count_pipeline;  $i++ ) { 
                                if ( $pipeline_compra_pieza[$i]->idEstadoCompraPieza->codigo == 1 ) {
                                    if (date_diff(new DateTime($pipeline_compra_pieza[$i-1]->fecha), new DateTime($pipeline_compra_pieza[$i]->fecha))->days > $pipeline_compra_pieza[$i]->idEstadoCompraPieza->dias_optimos) {
                                        echo 'active-red';
                                    } else{
                                        echo 'active-green';
                                    }
                                    break;
                                }
                            }
                        }
                        echo '"></div>';
                    ?>
                    <div class="bs-wizard-stepnum text-center">Espera de cotización</div>
                </div>
                <div class="bs-wizard-step complete">
                    <div class="bs-wizard-marker <?= ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 2)?'active':''?> text-center">
                        <?= ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 2)?'<i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>':''?>
                    </div>
                    <div class="bs-wizard-days <?= ( 2 < $count_pipeline )?'active':'no-days' ?><?php // ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 2)?'active':''?> text-center">
                        <?php // ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 2)?date_diff(new DateTime("now"), new DateTime($pipeline_compra_pieza[0]->fecha))->days.' DÍAS':''?>
                        <?php
                            if ( $pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 2 ) {
                                echo date_diff(new DateTime("now"), new DateTime($pipeline_compra_pieza[0]->fecha))->days.' DÍAS';
                            } else{
                                if ( 2 < $count_pipeline ) {
                                    for ( $i = 1;  $i < $count_pipeline;  $i++ ) { 
                                        if ( $pipeline_compra_pieza[$i]->idEstadoCompraPieza->codigo == 2 ) {
                                            echo date_diff(new DateTime($pipeline_compra_pieza[$i-1]->fecha), new DateTime($pipeline_compra_pieza[$i]->fecha))->days.' DÍAS';
                                            break;
                                        }
                                    }
                                }
                            }
                        ?>
                    </div>
                    <div class="progress"><div class="progress-bar"></div></div>
                    <?php 
                        echo '<div class="bs-wizard-dot '; 
                        if ( 2 < $count_pipeline ) {
                            for ( $i = 1;  $i < $count_pipeline;  $i++ ) { 
                                if ( $pipeline_compra_pieza[$i]->idEstadoCompraPieza->codigo == 2 ) {
                                    if (date_diff(new DateTime($pipeline_compra_pieza[$i-1]->fecha), new DateTime($pipeline_compra_pieza[$i]->fecha))->days > $pipeline_compra_pieza[$i]->idEstadoCompraPieza->dias_optimos) {
                                        echo 'active-red';
                                    } else{
                                        echo 'active-green';
                                    }
                                    break;
                                }
                            }
                        }
                        echo '"></div>';
                    ?>
                    <div class="bs-wizard-stepnum text-center">Aprobación</div>
                </div>
                <div class="bs-wizard-step complete">
                    <div class="bs-wizard-marker <?= ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 3)?'active':''?> text-center">
                        <?= ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 3)?'<i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>':''?>
                    </div>
                    <div class="bs-wizard-days <?= ( 3 < $count_pipeline )?'active':'no-days' ?><?php //($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 3)?'active':''?> text-center">
                        <?php // ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 3)?date_diff(new DateTime("now"), new DateTime($pipeline_compra_pieza[0]->fecha))->days.' DÍAS':''?>
                        <?php
                            if ( $pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 3 ) {
                                echo date_diff(new DateTime("now"), new DateTime($pipeline_compra_pieza[0]->fecha))->days.' DÍAS';
                            } else{
                                if ( 3 < $count_pipeline ) {
                                    for ( $i = 1;  $i < $count_pipeline;  $i++ ) { 
                                        if ( $pipeline_compra_pieza[$i]->idEstadoCompraPieza->codigo == 3 ) {
                                            echo date_diff(new DateTime($pipeline_compra_pieza[$i-1]->fecha), new DateTime($pipeline_compra_pieza[$i]->fecha))->days.' DÍAS';
                                            break;
                                        }
                                    }
                                }
                            }
                        ?>
                    </div>
                    <div class="progress"><div class="progress-bar"></div></div>
                    <?php 
                        echo '<div class="bs-wizard-dot '; 
                        if ( 3 < $count_pipeline ) {
                            for ( $i = 1;  $i < $count_pipeline;  $i++ ) { 
                                if ( $pipeline_compra_pieza[$i]->idEstadoCompraPieza->codigo == 3 ) {
                                    if (date_diff(new DateTime($pipeline_compra_pieza[$i-1]->fecha), new DateTime($pipeline_compra_pieza[$i]->fecha))->days > $pipeline_compra_pieza[$i]->idEstadoCompraPieza->dias_optimos) {
                                        echo 'active-red';
                                    } else{
                                        echo 'active-green';
                                    }
                                    break;
                                }
                            }
                        }
                        echo '"></div>';
                    ?>
                    <div class="bs-wizard-stepnum text-center">Por recibir pieza</div>
                </div>
                <div class="bs-wizard-step complete">
                    <div class="bs-wizard-marker <?= ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 4)?'active':''?> text-center">
                        <?= ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 4)?'<i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>':''?>
                    </div>
                    <div class="bs-wizard-days <?= ( 4 < $count_pipeline )?'active':'no-days' ?><?php // ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 4)?'active':''?> text-center">
                        <?php // ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 4)?date_diff(new DateTime("now"), new DateTime($pipeline_compra_pieza[0]->fecha))->days.' DÍAS':''?>
                        <?php
                            if ( $pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 4 ) {
                                echo date_diff(new DateTime("now"), new DateTime($pipeline_compra_pieza[0]->fecha))->days.' DÍAS';
                            } else{
                                if ( 4 < $count_pipeline ) {
                                    for ( $i = 1;  $i < $count_pipeline;  $i++ ) { 
                                        if ( $pipeline_compra_pieza[$i]->idEstadoCompraPieza->codigo == 4 ) {
                                            echo date_diff(new DateTime($pipeline_compra_pieza[$i-1]->fecha), new DateTime($pipeline_compra_pieza[$i]->fecha))->days.' DÍAS';
                                            break;
                                        }
                                    }
                                }
                            }
                        ?>
                    </div>
                    <div class="progress"><div class="progress-bar"></div></div>
                    <?php 
                        echo '<div class="bs-wizard-dot '; 
                        if ( 4 < $count_pipeline ) {
                            for ( $i = 1;  $i < $count_pipeline;  $i++ ) { 
                                if ( $pipeline_compra_pieza[$i]->idEstadoCompraPieza->codigo == 4 ) {
                                    if (date_diff(new DateTime($pipeline_compra_pieza[$i-1]->fecha), new DateTime($pipeline_compra_pieza[$i]->fecha))->days > $pipeline_compra_pieza[$i]->idEstadoCompraPieza->dias_optimos) {
                                        echo 'active-red';
                                    } else{
                                        echo 'active-green';
                                    }
                                    break;
                                }
                            }
                        }
                        echo '"></div>';
                    ?>
                    <div class="bs-wizard-stepnum text-center">Asignación de ruta</div>
                </div>
                <div class="bs-wizard-step complete">
                    <div class="bs-wizard-marker <?= ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 5)?'active':''?> text-center">
                        <?= ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 5)?'<i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>':''?>
                    </div>
                    <div class="bs-wizard-days <?= ( 5 < $count_pipeline )?'active':'no-days' ?><?php // ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 5)?'active':''?> text-center">
                        <?php // ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 5)?date_diff(new DateTime("now"), new DateTime($pipeline_compra_pieza[0]->fecha))->days.' DÍAS':''?>
                        <?php
                            if ( $pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 5 ) {
                                echo date_diff(new DateTime("now"), new DateTime($pipeline_compra_pieza[0]->fecha))->days.' DÍAS';
                            } else{
                                if ( 5 < $count_pipeline ) {
                                    for ( $i = 1;  $i < $count_pipeline;  $i++ ) { 
                                        if ( $pipeline_compra_pieza[$i]->idEstadoCompraPieza->codigo == 5 ) {
                                            echo date_diff(new DateTime($pipeline_compra_pieza[$i-1]->fecha), new DateTime($pipeline_compra_pieza[$i]->fecha))->days.' DÍAS';
                                            break;
                                        }
                                    }
                                }
                            }
                        ?>

                    </div>
                    <div class="progress"><div class="progress-bar"></div></div>
                    <?php 
                        echo '<div class="bs-wizard-dot '; 
                        if ( 5 < $count_pipeline ) {
                            for ( $i = 1;  $i < $count_pipeline;  $i++ ) { 
                                if ( $pipeline_compra_pieza[$i]->idEstadoCompraPieza->codigo == 5 ) {
                                    if (date_diff(new DateTime($pipeline_compra_pieza[$i-1]->fecha), new DateTime($pipeline_compra_pieza[$i]->fecha))->days > $pipeline_compra_pieza[$i]->idEstadoCompraPieza->dias_optimos) {
                                        echo 'active-red';
                                    } else{
                                        echo 'active-green';
                                    }
                                    break;
                                }
                            }
                        }
                        echo '"></div>';
                    ?>
                    <div class="bs-wizard-stepnum text-center">Pieza entregada</div>
                </div>
            </div>
        </div>           
        <?php } ?>
    </div>
    </br></br>
	<div class="row">
		<div class="col-xs-12">
            <p><b>FECHA DE ASIGNACIÓN: </b> <?= date('d/m/Y', strtotime($model->fecha)) ?></p>
        </div>
	</div>
	</br></br>
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1">
            <p><b>LISTADO DE PIEZAS</b></p>
        </div>
        </br>
        <?php if ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo < 3): ?>
            <div class="col-xs-10 col-xs-offset-1">
            <?php $form = ActiveForm::begin([ 'id' => 'cotizacion-proveedor-form', 'action' => ['/solicitud-compra-pieza/guardarimagenpieza'], 'options' => ['enctype' => 'multipart/form-data'] ]); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'summary' => '',
                    'id' =>'cotizacion-pieza-grid',
                    'pjax' => true,
                    'tableOptions' => ['class' => 'table table-responsive',],
                    'options' =>[
                        'class' => 'grid-blue',
                    ],
                    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                    'columns' => [
                        [
                            'label' => 'Tipo',
                            'format' => 'text',
                            'enableSorting' => false,
                            'contentOptions'=>['style'=>'text-align:center;'],
                            'value' => function($model){
                                if ($model->id_pieza) {
                                    return $model->idPieza->tipoPieza->nombre;
                                } else{
                                    return '-';
                                }
                            },
                        ],
                        [
                            'attribute' => 'id_pieza',
                            'label' => 'Descripcion',
                            'format' => 'raw',
                            'enableSorting' => false,
                            'contentOptions'=>['style'=>'text-align:center;'],
                            'value' => function($model) {
                                if ($model->id_pieza) {
                                    $link = Html::a('<span><i class="fa fa-list-ul fa-sm" aria-hidden="true"></i></span>',[ 'solicitud-compra-pieza/historicopieza', 'id' => $model->id_pieza], ['title' => 'Ver historico', 'target' => '_blank']);
                                    return $link.' '.$model->idPieza->nombre;
                                }
                            }
                        ],
                        [
                            'attribute' => 'cantidad',
                            'label' => 'Cantidad',
                            'format' => 'text',
                            'enableSorting' => false,
                            'contentOptions'=>['style'=>'text-align:center;'],
                        ],
                        [
                            'attribute' => 'id_pieza',
                            'label' => 'Observación Pieza',
                            'format' => 'raw',
                            'enableSorting' => false,
                            'contentOptions'=>['style'=>'text-align:center;'],
                            'value' => function($model) {
                                return Html::hiddenInput('id[]', $model->id) . Html::input('text', 'observacion_pieza[]', (($model->observacion_pieza)?$model->observacion_pieza:""), ['class'=> 'form-control', 'style' => 'font-size: 10pt; text-align: center;']);
                            }
                        ],
                        [
                            'attribute' => 'id_pieza',
                            'label' => 'Imagen Pieza',
                            'format' => 'raw',
                            'enableSorting' => false,
                            'contentOptions'=>['style'=>'text-align:center; width:30%;'],
                            'value' => function($model) use ($form) {
                                $ip = $form->field($model, 'imageFiles[]')->fileInput(['accept' => 'image/*', 'class' =>"form-control", 'style' => 'border:0px;'])->label('');
                                $imagen = " <a href='".Yii::$app->request->baseUrl.$model->imagen_pieza."' data-lightbox='".Yii::$app->request->baseUrl.$model->imagen_pieza."'>
                                                <img src= '".Yii::$app->request->baseUrl.$model->imagen_pieza."' style='max-width:40%;' class='".Yii::$app->request->baseUrl.$model->imagen_pieza."' />
                                            </a></br>";
                                return ($model->imagen_pieza)? $imagen . $ip : 'Sin imagen' . $ip;
                            }
                        ],
                    ],
                ]); ?>
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="form-group text-center">
                        <a href=""></a>
                            <?= Html::submitButton('ENVIAR', ['class' => 'btn btn-submit']) ?>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        <?php else: ?>
            <div class="col-xs-10 col-xs-offset-1">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'summary' => '',
                    'id' =>'cotizacion-pieza-grid',
                    'pjax' => true,
                    'tableOptions' => ['class' => 'table table-responsive', 'id' =>'cotizacion-pieza-table'],
                    'options' =>[
                        'class' => 'grid-blue',
                    ],
                    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                    'columns' => [
                        /*[
                            'label' => 'Tipo',
                            'format' => 'text',
                            'enableSorting' => false,
                            'contentOptions'=>['style'=>'text-align:center;'],
                            'value' => function($model){
                                if ($model->id_pieza) {
                                    return $model->idPieza->tipoPieza->nombre;
                                } else{
                                    return '-';
                                }
                            },
                        ],*/
                        [
                            'attribute' => 'idCotizacionTallerPieza.idPieza.nombre',
                            'label' => 'Descripcion',
                            'format' => 'text',
                            'enableSorting' => false,
                            'contentOptions'=>['style'=>'text-align:left;'],
                        ],
                        [
                            'attribute' => 'cantidad_comprar',
                            'label' => 'Cantidad',
                            'format' => 'text',
                            'enableSorting' => false,
                            'contentOptions'=>['style'=>'text-align:center;'],
                        ],
                        [
                            'attribute' => 'idCotizacionProveedor.idSolicitudCompraProveedor.idProveedor.nombre',
                            'label' => 'Proveedor',
                            'format' => 'text',
                            'enableSorting' => false,
                            'contentOptions'=>['style'=>'text-align:left;'],
                        ],
                        [
                            'attribute' => 'fecha_entrega',
                            'label' => 'Fecha entrega',
                            'format' => 'text',
                            'enableSorting' => false,
                            'contentOptions'=>['style'=>'text-align:center;'],
                            'value' => function( $model ){
                                return date( 'd/m/Y', strtotime($model->fecha_entrega) );
                            },
                        ],
                        [
                            'attribute' => 'costo',
                            'label' => 'Costo',
                            'format' => 'text',
                            'enableSorting' => false,
                            'contentOptions'=>['style'=>'text-align:right;'],
                            'value' => function( $model ){
                                return number_format(floatval($model->costo), 2, ',', '.').' RD$';
                            },
                        ],
                        [
                            'attribute' => 'cantidad_recibida',
                            'label' => 'Cantidad recibida',
                            'format' => 'text',
                            'enableSorting' => false,
                            'contentOptions'=>['style'=>'text-align:center;'],
                            'value' => function($model){
                                if ($model->cantidad_recibida) {
                                    return $model->cantidad_recibida;
                                } else{
                                    return 'En espera';
                                }
                            },
                        ],
                        [  
                            'class' => 'yii\grid\ActionColumn',
                            'header'=>'Acciones',
                            'template' => '{check} {devolucion}',
                            'buttons' => [
                                'check' => function($url, $model) use ( $pipeline_compra_pieza){
                                    return ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 3 && \Yii::$app->user->can('admin-logistica'))?($model->cantidad_recibida)?'<span><i class="fa fa-check-circle fa-lg" aria-hidden="true" style="color: #4b8723; background-color: #fff;"></i></span>':Html::a('<i class="fa fa-check-square-o" aria-hidden="true"></i>',Url::to(['/solicitud-compra-pieza/checklistrecepcion', 'id' => $model->id_cotizacion_proveedor ]), ['title' => 'Checklist de recepción', ]):'';
                                    //return (\Yii::$app->user->can('admin-logistica'))?($model->cantidad_recibida )?'<span><i class="fa fa-check-circle fa-lg" aria-hidden="true" style="color: #4b8723; background-color: #fff;"></i></span>':Html::a('<i class="fa fa-check-square-o" aria-hidden="true"></i>',Url::to(['/solicitud-compra-pieza/checklistrecepcion', 'id' => $model->id_cotizacion_proveedor ]), ['title' => 'Checklist de recepción', ]) : '';
                                },
                                'devolucion' => function($url, $model) use ( $pipeline_compra_pieza){
                                    return ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 4 && \Yii::$app->user->can('admin-logistica'))?Html::a('<i class="fa fa-undo" aria-hidden="true"></i>',Url::to(['/solicitud-compra-pieza/devolucion', 'id' => $model->id_cotizacion_proveedor ]), ['title' => 'Devolución de piezas', ]):'';
                                },
                            ], 

                        ],
                    ],
                ]); ?>
            </div>
        <?php endif ?>
	</div>
	</br></br>
    <?php if ( $pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 3 ): ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="pull-right">
                <?php                
                    $piezas_checklist = 0;
                    if (!empty($dataProvider->getModels())) :
                        $piezas_checklist = 0;
                        foreach ($dataProvider->getModels() as $key => $val) :
                            $piezas_checklist += ($val->inventario == 1)? count($key) :0;
                        endforeach;
                    endif;
                    if($cantidad_piezas == $piezas_checklist && \Yii::$app->user->can('admin-logistica')):
                        echo Html::a('Finalizar recepción','#', ['class' => 'btn btn-small-gris', 'data' => [ 'id' => $model->id, 'toggle' => 'modal', 'target' => '#recepcionModal' ] ]); 
                    endif;
                ?> 
                </div>
            </div>
        </div>
    <?php endif ?>
    <?php if ( $pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo == 0 ): ?>
        <div class="row">
            <div class="col-xs-12">
                <?php if(\Yii::$app->user->can('analista-compras')): ?>
                <div class="pull-right">
                    <?= Html::a('Solicitar cotización','#', ['class' => 'btn btn-small-gris', 'data' => [ 'id' => $model->id, 'toggle' => 'modal', 'target' => '#solicitarCotizacionModal' ] ]) ?> 
                </div>
                <?php endif ?>
            </div>
        </div>
    <?php endif ?>
</div>
<style>
	.bs-wizard {}

    /*Form Wizard*/
    .bs-wizard {border-bottom: solid 1px #e0e0e0; padding: 0 0 10px 0;}
    .bs-wizard > .bs-wizard-step {padding: 0; position: relative; width: 16.6%; float: left;}
    .bs-wizard > .bs-wizard-step .bs-wizard-stepnum {color: #000; font-size: 8pt; font-weight: bold;}
    .bs-wizard > .bs-wizard-step .bs-wizard-info {color: #000; font-size: 8pt;}
    .bs-wizard > .bs-wizard-step > .bs-wizard-dot {position: absolute; width: 30px; height: 30px; display: block; background: #767171; top: 70px; left: 50%; margin-top: -15px; margin-left: -15px; border-radius: 50%;} 
    .bs-wizard > .bs-wizard-step > .bs-wizard-dot:after {content: ' '; width: 16px; height: 16px; background: #fff; border-radius: 50px; position: absolute; top: 7px; left: 7px; }
    .bs-wizard > .bs-wizard-step > .bs-wizard-dot.active-green:after { background: #a9d18e; }
    .bs-wizard > .bs-wizard-step > .bs-wizard-dot.active-red:after { background: #e08282; }
    .bs-wizard > .bs-wizard-step > .progress {position: relative; border-radius: 0px; height: 6px; box-shadow: none; margin: 20px 0; background: #767171;}
    .bs-wizard > .bs-wizard-step > .bs-wizard-marker { margin-bottom: 30px;}
    .bs-wizard > .bs-wizard-step > .bs-wizard-marker.active{ color: #b34b4b; content: ' '; margin-bottom: 0px;}
    .bs-wizard > .bs-wizard-step > .bs-wizard-days.no-days{ padding-top: 15px; }
    .bs-wizard > .bs-wizard-step > .bs-wizard-days { margin-bottom: 0px;}
    .bs-wizard > .bs-wizard-step > .bs-wizard-days.active{ color: #757972; content: ' '; margin-bottom: 0px; font-size: 8pt; font-weight: bold;}
    /*END Form Wizard*/

    .content-pipeline{
        border: 1px solid #dedede;
        height: 200px;
    }
	.expand-compra-pieza{
        padding: 10px 30px 20px 30px;
        text-align: left;
    }
    .grid .dropdown-menu {
        right: -60px;
        left: unset;
    }
</style>