<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\db\Query;
use app\models\CotizacionProveedorPieza;
use app\models\CotizacionTallerPieza;
use app\models\CotizacionTallerAnalista;
use app\models\SolicitudCompraPieza;
use app\models\AnalistaMantenimiento;
use app\models\Vehiculo;
use app\models\Servicio;
use app\models\Modelo;
use yii\helpers\ArrayHelper;

$this->title = 'LISTADO DE SOLICITUDES DE COMPRAS CERRADAS';
?>
<div class="piezas-recibidas">
	<div class="row">
        <div class="col-md-1 col-xs-3">
            <i class="fa fa-list-ul fa-3x" aria-hidden="true"></i>
        </div>
        <div class="col-md-10 col-xs-9">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
        </div>
    </div>
           <div class="row">
        <div class="col-md-12">
            <?php
              echo Html::a('Listado de Solicitudes', ['/solicitud-compra-pieza/index'], ['class' => 'btn btn-small-gris', 'style' => 'float:right;']);
            ?>
        </div>
    </div>
    </br>
    </br>

    <div class="row">
    	<div class="col-xs-12">
    		<?= GridView::widget([
                'dataProvider' => $dataProvider,
                // 'filterModel'   => $searchModel,
                'summary' => '',
                'id' =>'cotizacion-pieza-grid',
                'tableOptions' => ['class' => 'table table-responsive', 'id' =>'cotizacion-pieza-table'],
                   'tableOptions' => ['class' => 'table table-responsive', 'id' =>'cotizacion-pieza-table'],
                   'options' =>[
                        'class' => 'grid table-responsive',
                    ],
                'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                'columns' => [
                   [
                        'attribute' => 'id_servicio',
                        'label' => 'Solicitud N°',
                        'format' => 'text',
                        'filter' => true,
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:left;'],
                    ],
                    [
                        // 'attribute' => 'fecha',
                        'label' => 'Fecha',
                        'format' => 'text',
                        'filter' => true,
                        'enableSorting' => true,
                        'contentOptions'=>['style'=>'text-align:left;'],
                         'value' => function( $model ){
                            return date('d/m/Y', strtotime($model->fecha));

                         }
                    ],
                    [
                        'attribute' => 'observacion',
                        'label' => 'Observación',
                        'format' => 'text',
                        'filter' => true,
                        'enableSorting' => true,
                        'contentOptions'=>['style'=>'text-align:left;'],
                    ],
           
               
                   
                ],
            ]); ?>
    	</div>
    </div>
</div>

<script>
	$('.table').removeClass( "table-bordered table-striped" );
</script>