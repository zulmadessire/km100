<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\db\Query;
use app\models\Servicio;
use app\models\Vehiculo;
use app\models\CotizacionProveedor;
use app\models\SolicitudCompraProveedor;
use app\models\Marca;
use app\models\Modelo;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PartesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'LISTADO DE SOLICITUDES DE COTIZACIÓN';
?>
<div class="taller-index">

    <div class="row">
        <div class="col-md-1 col-xs-3">
            <i class="fa fa-list-ul fa-3x" aria-hidden="true"></i>
        </div>
        <div class="col-md-10 col-xs-9">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
        </div>
        <div class="col-xs-12">
            </br>
            <p> 
                <span style= "background-color: #c58b8b; width: 30px; height: 15px; display: inline-block;"></span> Fuera de tiempo para cotizar. 
            </p>
        </div>
    </div>
    </br>
    
    <div class="row">
        <div class="col-xs-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => ['class' => 'table table-responsive'],
                'summary' => '',
                'options' =>[
                    'class' => 'grid',
                ],
                'columns' => [
                    [
                        'attribute' => 'id_solicitud_compra_pieza',
                        'label' => 'No. Solicitud de compra',
                        'format' => 'text',
                        'enableSorting' => false,
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'id_solicitud_compra_pieza',
                            'data' => ArrayHelper::map(SolicitudCompraProveedor::find()->where([ 'id_proveedor' => $id_proveedor ])->all(), 'id_solicitud_compra_pieza', 'id_solicitud_compra_pieza'),
                            'options' => ['placeholder' => 'Selecciona',],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]),
                        'headerOptions' => ['width' => '150',],
                        'contentOptions' => function ($model, $key, $index, $column) {
                            $estado_cotizacion_proveedor = CotizacionProveedor::find()->where(['id_solicitud_compra_proveedor' => $model->id])->one();
                            if($estado_cotizacion_proveedor->estado == 3) {
                                $color = "#c58b8b";
                            }
                            // elseif($estado_cotizacion_proveedor->estado == 2){
                            //     $color = "#b9f3bc";
                            // }
                            else {
                                $color = '';
                            }
                            return  ['style' => 'background-color:' 
                                .       $color.''
                                    ];
                        },
                    ],
                    [
                        'label' => 'Marca',
                        'format' => 'text',
                        'enableSorting' => false,
                        'value' => function($model){
                        	$model_servicio = Servicio::findOne($model->idSolicitudCompraPieza->id_servicio);
                        	$model_vehiculo = Vehiculo::findOne($model_servicio->id_vehiculo);
                        	return $model_vehiculo->idModelo->idMarca->nombre;
                        },
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'id_marca',
                            'data' => ArrayHelper::map(Marca::find()->all(), 'id_marca', 'nombre'),
                            'options' => ['placeholder' => 'Selecciona',],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]),
                        'headerOptions' => ['width' => '150',],
                    ],
                    [
                        'label' => 'Modelo',
                        'format' => 'text',
                        'enableSorting' => false,
                        'value' => function($model){
                        	$model_servicio = Servicio::findOne($model->idSolicitudCompraPieza->id_servicio);
                        	$model_vehiculo = Vehiculo::findOne($model_servicio->id_vehiculo);
                        	return $model_vehiculo->idModelo->nombre;
                        },
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'id_modelo',
                            'data' => ArrayHelper::map(Modelo::find()->all(), 'id_modelo', 'nombre'),
                            'options' => ['placeholder' => 'Selecciona',],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]),
                        'headerOptions' => ['width' => '150',],
                    ],
                    [
                        'label' => 'Año',
                        'format' => 'raw',
                        'enableSorting' => false,
                        'value' => function($model){
                            $model_servicio = Servicio::findOne($model->idSolicitudCompraPieza->id_servicio);
                            $model_vehiculo = Vehiculo::findOne($model_servicio->id_vehiculo);
                            return $model_vehiculo->anho;
                        },
                        'filter' => Html::input('text', 'SolicitudCompraProveedorSearch[anho]', $searchModel->anho, ['class' => 'form-control',]),
                    ],
                    [
                        'label' => 'Clase',
                        'format' => 'raw',
                        'enableSorting' => false,
                        'value' => function($model){
                            $model_servicio = Servicio::findOne($model->idSolicitudCompraPieza->id_servicio);
                            $model_vehiculo = Vehiculo::findOne($model_servicio->id_vehiculo);
                            return $model_vehiculo->clase;
                        },
                        'filter' => Html::input('text', 'SolicitudCompraProveedorSearch[clase]', $searchModel->clase, ['class' => 'form-control',]),
                    ],
                    [
                        'label' => 'Chasis',
                        'format' => 'raw',
                        'enableSorting' => false,
                        'value' => function($model){
                        	$model_servicio = Servicio::findOne($model->idSolicitudCompraPieza->id_servicio);
                        	$model_vehiculo = Vehiculo::findOne($model_servicio->id_vehiculo);
                        	return $model_vehiculo->chasis;
                        },
                        'filter' => Html::input('text', 'SolicitudCompraProveedorSearch[chasis]', $searchModel->chasis, ['class' => 'form-control',]),
                    ],
                    [  
                        'class' => 'yii\grid\ActionColumn',
                        'header'=>'Acciones',
                        'template' => '{edit}',
                        'buttons' => [
                            'edit' => function($url, $model){
                            	$model_cotizacion_proveedor = CotizacionProveedor::find()->where(['id_solicitud_compra_proveedor' => $model->id])->one();
                                if($model_cotizacion_proveedor->estado == 3) {
                                    $link = "";
                                }
                                // elseif($model_cotizacion_proveedor->estado == 2){
                                //     $link = Html::a('<i class="fa fa-pencil" aria-hidden="true"></i>', ['/solicitud-compra-pieza/cotizacionproveedor', 'id' => $model_cotizacion_proveedor->id, 'origen' => 2 ], ['title' => 'Editar Solicitud', ]);
                                // }
                                else {
                                    $link = Html::a('<i class="fa fa-pencil" aria-hidden="true"></i>', ['/solicitud-compra-pieza/cotizacionproveedor', 'id' => $model_cotizacion_proveedor->id ], ['title' => 'Editar Solicitud', ]);
                                }
                                return $link;
                            },
                        ], 

                    ],
                ],
            ]);  ?>
        </div>
    </div>
</div>