<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use app\models\DespachoProveedorPieza;

$this->title = 'DESPACHAR PIEZAS';
?>

<div class="despacho">
	<div class="row">
		<div class="col-md-1 col-xs-3">
			<h4 class="titulo"><i class="fa fa-truck fa-4x" aria-hidden="true"></i></h4>
		</div>
		<div class="col-md-10 col-xs-9" style="padding-top: 20px;">
			<h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
		</div>
	</div>
    </br>
    </br>
    <div class="row">
        <div class="col-xs-12 col-md-8 col-md-offset-2">
                <div class="pull-left">
                    <?php // Html::a('+ Nuevo Envío', '#', ['class' => 'btn btn-small-gris', 'data' => ['toggle' => 'modal', 'target' => '#alertAddEnvioBodegaModal']]) ?>
                    <?= Html::a('Listado de solicitudes', ['solicitud-compra-pieza/index', 'id' => $model->id ], ['class' => 'btn btn-small-gris',]) ?>
                </div>
                <div class="pull-right">
                    <?php // Html::a('+ Nuevo Envío', '#', ['class' => 'btn btn-small-gris', 'data' => ['toggle' => 'modal', 'target' => '#alertAddEnvioBodegaModal']]) ?>
                    <?= ($cantidad_piezas != $cantidad_piezas_despacho)? Html::a('+ Nuevo Envío', ['solicitud-compra-pieza/crearenvio', 'id' => $model->id ], ['class' => 'btn btn-small-verde',]): '' ?>
                </div>
        </div>
    </div>
    <div class="clearfix"></div>
    </br>
    <div class="row">
        <div class="col-xs-12 col-md-8 col-md-offset-2">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => ['class' => 'table table-responsive'],
                'summary' => '',
                'options' =>[
                    'class' => 'grid',
                ],
                'columns' => [
                    [
                        'attribute' => 'numero',
                        'label' => 'No. Envío',
                        'format' => 'text',
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'filter' => false,
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'estado',
                        'label' => 'Estatus',
                        'format' => 'text',
                        'value' => function ($model) {
                            if ($model->estado == 0) {
                                return 'EN PROCESO';
                            } elseif ($model->estado == 1){
                                return 'ENVIADO A MENSAJERO';
                            } elseif ($model->estado == 2){
                                return 'RECIBIDA POR MENSAJERO';
                            } else {
                                return 'ENTREGADA';
                            }
                        },
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'filter' => false,
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'fecha',
                        'label' => 'Fecha de Envío',
                        'format' => 'text',
                        'value' => function($model){
                            if ( $model->fecha )
                                return date( 'd/m/Y', strtotime($model->fecha) );
                            else
                                return '-';
                        },
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'enableSorting' => false,
                    ],
                    [
                        'label' => 'No. de Piezas',
                        'format' => 'text',
                        'value' => function($model){
                            $cant = count(DespachoProveedorPieza::find()->where(['id_despacho' => $model->id])->all());
                            return $cant;
                        },
                        'contentOptions'=>['style'=>'text-align:center;'],
                        //'headerOptions' => ['width' => '300',],
                    ],
                    [
                        'attribute' => 'traslado',
                        'label' => 'Traslado',
                        'format' => 'text',
                        'value' => function($model){
                            if ( $model->traslado )
                                return $model->traslado;
                            else
                                return '-';
                        },
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'idMensajero.nombre',
                        'label' => 'Representante de Reparto',
                        'format' => 'text',
                        'value' => function($model){
                            if ( $model->id_mensajero )
                                return $model->idMensajero->nombre;
                            else
                                return '-';
                        },
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'enableSorting' => false,
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => 'Acciones',
                        'template' => '{update}',
                        'buttons' => [
                            'update' => function ($url, $model) {
                                return ($model->estado == 0 )?Html::a('<i class="fa fa-pencil" aria-hidden="true"></i>', ['/solicitud-compra-pieza/despachopiezas', 'id' => $model->id], ['title' => 'Editar Envío', ]):Html::a('<i class="fa fa-file" aria-hidden="true"></i>', ['/solicitud-compra-pieza/imprimirtranslado', 'id' => $model->id], ['title' => 'PDF Envío', ]);
                            },
                        ],
                        'contentOptions'=>['style'=>'text-align:center;'],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<script>
    $('.table').removeClass( "table-bordered table-striped" );
</script>