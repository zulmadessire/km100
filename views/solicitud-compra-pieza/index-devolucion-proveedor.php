<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Proveedor;
use app\models\CotizacionProveedorPieza;
use app\models\Marca;
use app\models\Modelo;
use app\models\Servicio;
use app\models\Vehiculo;
use app\models\SolicitudCompraPieza;

$this->title = 'LISTADO DE PIEZAS DEVUELTAS';
?>
<div class="piezas-devueltas">
	<div class="row">
        <div class="col-md-1 col-xs-3">
            <i class="fa fa-list-ul fa-3x" aria-hidden="true"></i>
        </div>
        <div class="col-md-10 col-xs-9">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
        </div>
    </div>
    </br>
    </br>

    <div class="row">
    	<div class="col-xs-12">
    		<?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel'   => $searchModel,
                'summary' => '',
                'id' =>'index-devolucion-grid',
                'options' =>[
                    'class' => 'grid table-responsive',
                ],
                'columns' => [
                    ['class' => 'kartik\grid\SerialColumn'],
                    [
                        'attribute' => 'id',
                        'label' => 'No. Orden de compra',
                        'format' => 'text',
                        'enableSorting' => false,
                        /*'value' => function($model){
                            return OrdenCompraPieza::find()->where([ 'id_cotizacion_proveedor' => $model->id_cotizacion_proveedor ])->one()->id;
                        },
                        'filter' => Select2::widget([
                        'name' => 'CotizacionProveedorPiezaSearch[pieza]',
                        'data' => ArrayHelper::map(Pieza::find()->orderBy('nombre')->all(), 'id_pieza', 'nombre'),
                        'hideSearch' => true,
                        'options' => ['placeholder' => 'Descripción',],
                        'pluginOptions' => [
                              'allowClear' => true
                          ],
                           ]),*/
                        'headerOptions' => ['width' => '100',],
                    ],
                    [
                        'attribute' => 'fecha_devolucion',
                        'label' => 'Fecha Devolución',
                        'format' => 'text',
                        'enableSorting' => false,
                        'value' => function($model){
                            return date('d-m-Y', strtotime(CotizacionProveedorPieza::find()->where(['id_cotizacion_proveedor' => $model->id_cotizacion_proveedor ])->one()->fecha_devolucion));
                        },
                        'filterType'=> GridView::FILTER_DATE,
                        'filter' => true,
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>[
                                'format' => 'dd-mm-yyyy',
                            ],
                        ],
                        /*'filter' => Select2::widget([
                        'name' => 'CotizacionProveedorPiezaSearch[pieza]',
                        'data' => ArrayHelper::map(Pieza::find()->orderBy('nombre')->all(), 'id_pieza', 'nombre'),
                        'hideSearch' => true,
                        'options' => ['placeholder' => 'Descripción',],
                        'pluginOptions' => [
                              'allowClear' => true
                          ],
                           ]),*/
                        //'headerOptions' => ['width' => '100',],
                    ],
                    [
                        'label' => 'Marca',
                        'format' => 'text',
                        'enableSorting' => false,
                        'value' => function($model){
                            $id_servicio = SolicitudCompraPieza::findOne($model->id_solicitud_compra_pieza)->id_servicio;
                            $model_servicio = Servicio::findOne($id_servicio);
                            $model_vehiculo = Vehiculo::findOne($model_servicio->id_vehiculo);
                            return $model_vehiculo->idModelo->idMarca->nombre;
                        },
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'id_marca',
                            'data' => ArrayHelper::map(Marca::find()->all(), 'id_marca', 'nombre'),
                            'options' => ['placeholder' => 'Selecciona',],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]),
                        'headerOptions' => ['width' => '150',],
                    ],
                    [
                        'label' => 'Modelo',
                        'format' => 'text',
                        'enableSorting' => false,
                        'value' => function($model){
                            $id_servicio = SolicitudCompraPieza::findOne($model->id_solicitud_compra_pieza)->id_servicio;
                            $model_servicio = Servicio::findOne($id_servicio);
                            $model_vehiculo = Vehiculo::findOne($model_servicio->id_vehiculo);
                            return $model_vehiculo->idModelo->nombre;
                        },
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'id_modelo',
                            'data' => ArrayHelper::map(Modelo::find()->all(), 'id_modelo', 'nombre'),
                            'options' => ['placeholder' => 'Selecciona',],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]),
                        'headerOptions' => ['width' => '150',],
                    ],
                    [
                        'label' => 'Año',
                        'format' => 'raw',
                        'enableSorting' => false,
                        'value' => function($model){
                            $id_servicio = SolicitudCompraPieza::findOne($model->id_solicitud_compra_pieza)->id_servicio;
                            $model_servicio = Servicio::findOne($id_servicio);
                            $model_vehiculo = Vehiculo::findOne($model_servicio->id_vehiculo);
                            return $model_vehiculo->anho;
                        },
                        'filter' => Html::input('text', 'OrdenCompraPiezaSearch[chasis]', $searchModel->chasis, ['class' => 'form-control',]),
                    ],
                    [
                        'label' => 'Chasis',
                        'format' => 'raw',
                        'enableSorting' => false,
                        'value' => function($model){
                            $id_servicio = SolicitudCompraPieza::findOne($model->id_solicitud_compra_pieza)->id_servicio;
                            $model_servicio = Servicio::findOne($id_servicio);
                            $model_vehiculo = Vehiculo::findOne($model_servicio->id_vehiculo);
                            return $model_vehiculo->chasis;
                        },
                        'filter' => Html::input('text', 'OrdenCompraPiezaSearch[chasis]', $searchModel->chasis, ['class' => 'form-control',]),
                    ],
                    [  
                        'class' => 'yii\grid\ActionColumn',
                        'header'=>'Acciones',
                        'template' => '{view}',
                        'buttons' => [
                            //view button
                            'view' => function ($url, $model){
                                return Html::a('<span><i class="fa fa-eye fa-sm" aria-hidden="true"></i></span>',[ 'viewdevolucionproveedor', 'id' => $model->id ]);
                            },
                        ],
                    ],
                ],
            ]); ?>
    	</div>
    </div>
</div>

<script>
	$('.table').removeClass( "table-bordered table-striped" );
</script>