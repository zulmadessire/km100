<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\db\Query;
use app\models\Vehiculo;
use app\models\Marca;
use app\models\Modelo;
use app\models\Servicio;
use app\models\SolicitudCompraPieza;
use app\models\OrdenCompraPieza;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
use kartik\date\DatePicker;
use app\models\CotizacionProveedor;
use app\models\SolicitudCompraProveedor;
use app\models\Proveedor;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PartesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'LISTADO DE ORDENES DE COMPRA';
?>
<div class="index-orden-compra">

    <div class="row">
        <div class="col-md-1 col-xs-3">
            <i class="fa fa-list-ul fa-3x" aria-hidden="true"></i>
        </div>
        <div class="col-md-10 col-xs-9">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
        </div>
        <div class="col-xs-12">
        </br>
        </br>
          <p> <span style= "background-color: #c58b8b; width: 30px; height: 15px; display: inline-block;"></span> Ordenes de compras sin factura asociada </p>
        </div>
    </div>
    </br>
    
    <div class="row">
        <div class="col-xs-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => ['class' => 'table table-responsive'],
                'summary' => '',
                'options' =>[
                    'class' => 'grid',
                ],
                'columns' => [
                    [
                        'attribute' => 'id',
                        'label' => 'No. De Orden de compra',
                        'format' => 'text',
                        'enableSorting' => false,
                        /*'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'id_solicitud_compra_pieza',
                            'data' => ArrayHelper::map(SolicitudCompraProveedor::find()->where([ 'id_proveedor' => $id_proveedor ])->all(), 'id_solicitud_compra_pieza', 'id_solicitud_compra_pieza'),
                            'hideSearch' => true,
                            'options' => ['placeholder' => 'Selecciona',],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]),*/
                        'headerOptions' => ['width' => '150',],
                        'contentOptions' => function ($model, $key, $index, $column) {
                            return  ['style' => 'background-color:' 
                                .       ($model->estado == 1 ? '' : '#c58b8b')
                                    ];
                        },
                    ],
                    [
                        'label' => 'Proveedor',
                        'format' => 'text',
                        'enableSorting' => false,
                        'value' => function($model){
                            $proveedor = Proveedor::findOne($model->id_proveedor)->nombre;
                            return $proveedor;
                        },
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'id_proveedor',
                            'data' => ArrayHelper::map(Proveedor::find()->all(), 'id', 'nombre'),
                            'options' => ['placeholder' => 'Selecciona',],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]),
                        'headerOptions' => ['width' => '150',],
                    ],
                    [
                        'label' => 'Marca',
                        'format' => 'text',
                        'enableSorting' => false,
                        'value' => function($model){
                            $id_servicio = SolicitudCompraPieza::findOne($model->id_solicitud_compra_pieza)->id_servicio;
                            $model_servicio = Servicio::findOne($id_servicio);
                            $model_vehiculo = Vehiculo::findOne($model_servicio->id_vehiculo);
                            return $model_vehiculo->idModelo->idMarca->nombre;
                        },
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'id_marca',
                            'data' => ArrayHelper::map(Marca::find()->all(), 'id_marca', 'nombre'),
                            'options' => ['placeholder' => 'Selecciona',],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]),
                        'headerOptions' => ['width' => '150',],
                    ],
                    [
                        'label' => 'Modelo',
                        'format' => 'text',
                        'enableSorting' => false,
                        'value' => function($model){
                            $id_servicio = SolicitudCompraPieza::findOne($model->id_solicitud_compra_pieza)->id_servicio;
                            $model_servicio = Servicio::findOne($id_servicio);
                            $model_vehiculo = Vehiculo::findOne($model_servicio->id_vehiculo);
                            return $model_vehiculo->idModelo->nombre;
                        },
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'id_modelo',
                            'data' => ArrayHelper::map(Modelo::find()->all(), 'id_modelo', 'nombre'),
                            'options' => ['placeholder' => 'Selecciona',],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]),
                        'headerOptions' => ['width' => '150',],
                    ],
                    [
                        'label' => 'Año',
                        'format' => 'raw',
                        'enableSorting' => false,
                        'value' => function($model){
                            $id_servicio = SolicitudCompraPieza::findOne($model->id_solicitud_compra_pieza)->id_servicio;
                            $model_servicio = Servicio::findOne($id_servicio);
                            $model_vehiculo = Vehiculo::findOne($model_servicio->id_vehiculo);
                            return $model_vehiculo->anho;
                        },
                        'filter' => Html::input('text', 'OrdenCompraPiezaSearch[anho]', $searchModel->anho, ['class' => 'form-control',]),
                    ],
                    [
                        'label' => 'Clase',
                        'format' => 'raw',
                        'enableSorting' => false,
                        'value' => function($model){
                            $id_servicio = SolicitudCompraPieza::findOne($model->id_solicitud_compra_pieza)->id_servicio;
                            $model_servicio = Servicio::findOne($id_servicio);
                            $model_vehiculo = Vehiculo::findOne($model_servicio->id_vehiculo);
                            return $model_vehiculo->clase;
                        },
                        'filter' => Html::input('text', 'OrdenCompraPiezaSearch[clase]', $searchModel->clase, ['class' => 'form-control',]),
                    ],
                    [
                        'label' => 'Chasis',
                        'format' => 'raw',
                        'enableSorting' => false,
                        'value' => function($model){
                            $id_servicio = SolicitudCompraPieza::findOne($model->id_solicitud_compra_pieza)->id_servicio;
                            $model_servicio = Servicio::findOne($id_servicio);
                            $model_vehiculo = Vehiculo::findOne($model_servicio->id_vehiculo);
                            return $model_vehiculo->chasis;
                        },
                        'filter' => Html::input('text', 'OrdenCompraPiezaSearch[chasis]', $searchModel->chasis, ['class' => 'form-control',]),
                    ],
                    [  
                            'class' => 'yii\grid\ActionColumn',
                            'header'=>'Acciones',
                            'template' => '{menu}',
                            'buttons' => [

                                //menu button
                                'menu' => function ($url, $model) {
                                    $model_orden_compra_pieza = OrdenCompraPieza::findOne($model->id);
                                    $monto = $model_orden_compra_pieza->total;
                                     return '<div class="dropdown">
                                                <button class="btn btn-default dropdown-toggle btn-sm" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i>
                                                <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li>'.Html::a('<span><i class="fa fa-eye fa-sm" aria-hidden="true"></i></span> Ver orden de compra',[ '/solicitud-compra-pieza/imprimirorden', 'id' => $model->id ], ['title' => 'Ver orden de compra', 'target' => '_blank']).' </li>'                                                                                                         
                                                .'</ul>
                                            </div>';
                                },
                            ],

                        ],
                ],
            ]);  ?>
        </div>
    </div>
</div>