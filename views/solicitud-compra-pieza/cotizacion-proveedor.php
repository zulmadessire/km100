<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use yii\db\Query;
use app\models\Pieza;
use app\models\User;
use app\models\Proveedor;
use app\models\TipoPieza;
use app\models\CotizacionTallerActividad;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\date\DatePicker;


$this->title = ($origen == 2 ? 'COTIZACIÓN':'REGISTRO DE COTIZACIÓN');
$disabled = ($origen == 2 ? true:false);
?>

<div class="cotizacion-taller">

    <div class="row">
		<div class="col-md-1 col-xs-3">
			<h4 class="titulo"><i class="fa fa-file-text fa-4x" aria-hidden="true"></i></h4>
		</div>
		<div class="col-md-10 col-xs-9" style="padding-top: 30px;">
			<h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
		</div>
	</div>
	<br>
    <br>


    <?php $form = ActiveForm::begin([ 'id' => 'cotizacion-proveedor-form', 'action' => ['/solicitud-compra-pieza/guardarcotizacionproveedor', 'id' => $model_cotizacion_proveedor->id ] ]); ?>
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <div class="pull-rigth" style="margin-top: 30px;">
                <?= Html::a('Listado de solicitudes',[ '/solicitud-compra-pieza/indexsolicitudcotizacion' ], ['class' => 'btn btn-small-gris']) ?> 
            </div>
        </div>
        <div class="col-md-4 col-md-offset-4 col-xs-12">
        	<?= $form->field($model_cotizacion_proveedor, 'id')->textInput(['disabled' => true, 'value' => $model_cotizacion_proveedor->idSolicitudCompraProveedor->id_solicitud_compra_pieza ])->label('NÚMERO DE ORDEN DE SOLICITUD') ?>
        </div>
        </br>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DEL VEHÍCULO</p>
            <hr>
        </div>
    </div>


    <div class="row">

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_modelo')->textInput(['disabled' => true, 'value' => $model_vehiculo->idModelo->idMarca->nombre ])->label('Marca') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_modelo')->textInput(['value' => strtoupper($model_vehiculo->idModelo->nombre), 'disabled' => true])->label('Modelo') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'chasis')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>

    </div>
    </br>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DEL TRASLADO</p>
            <hr>
        </div>
    </div>
    </br>
    <DIV class="row">
        <div class="col-md-4 col-xs-12">
            <?php
                $id_proveedor = User::findOne(Yii::$app->user->id)->id_proveedor;
                $proveedor = Proveedor::find()->where(['id' => $id_proveedor])->one();
                $datos = [0 => 'Domicilio a Km100', 2 => 'No entrega a Domicilio'];
                if($proveedor->domicilio == 'si') :
                    $domicilio = 0;
                    else :
                    $domicilio = 2;
                    endif;
                $model_cotizacion_proveedor->tipo_envio = $domicilio;
            ?>

            <?=
                $form->field($model_cotizacion_proveedor, 'tipo_envio')->widget(Select2::classname(), [
                    'data' => $datos,
                    'value' => $domicilio,
                    'options' => ['placeholder' => 'Selecciona el tipo de envio', 'disabled' => $disabled],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('TIPO DE ENVIO <span class="obligatorio">*&nbsp;</span>');
            ?>
        </div>
    </DIV>
    </br>
    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DEFINICIÓN DE PRECIOS</p>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <?= Html::a('Agregar Imagenes','#', ['class' => 'btn btn-small-gris', 'data' => ['toggle' => 'modal', 'target' => '#agreagrImagen' ] ]) ?> 
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'id' => 'cotizacion-proveedor-piezas-grid',
                'tableOptions' => ['class' => 'grid-blue table table-responsive table-bordered'],
                'summary' => '',
                'showFooter' => true,
                'pjax' => true,
                'afterFooter' => '<tr class="kv-table-footer"><td></td><td></td><td></td><td></td><td></td><td></td><td>ITBIS</td><td id="itbis"></td></tr>'.
                                 '<tr class="kv-table-footer"><td></td><td></td><td></td><td></td><td></td><td></td><td>TOTAL</td><td id="total"></td></tr>',
                'options' =>[
                    'class' => '',
                ],
                'columns' => [
                    [
                        'label' => 'Descripción',
                        'attribute' => '',
                        'format' => 'raw',
                        'filter' => false,
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:left;'],
                        'headerOptions' => ['width' => '200',],
                        'footer' => '',
                        'value' => function($model){
                            $imagen = " <a href='".Yii::$app->request->baseUrl.$model->idCotizacionTallerPieza->imagen_pieza."' data-lightbox='".Yii::$app->request->baseUrl.$model->idCotizacionTallerPieza->imagen_pieza."' data-title='".$model->idCotizacionTallerPieza->observacion_pieza."' title='".$model->idCotizacionTallerPieza->observacion_pieza."'>
                                                <img src= '".Yii::$app->request->baseUrl.$model->idCotizacionTallerPieza->imagen_pieza."' style='max-width:40%;' class='".Yii::$app->request->baseUrl.$model->idCotizacionTallerPieza->imagen_pieza."' />
                                            </a></br>";
                            $nombre_pieza = '<b>' . $model->idCotizacionTallerPieza->idPieza->nombre .'</b>' . '<br>';
                            $observacion = 'Observación: '.$model->idCotizacionTallerPieza->observacion_pieza;
                            return ($model->idCotizacionTallerPieza->imagen_pieza)? $imagen . $nombre_pieza . $observacion : '' . $nombre_pieza . $observacion;
                        },
                    ],
                    [
                        'label' => 'Cantidad Solicitada',
                        'attribute' => 'cantidad_solicitada',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'headerOptions' => ['width' => '80',],
                        'footer' => '',
                    ],
                    [
                        'label' => 'Cantidad Disponible',
                        'attribute' => 'cantidad_disponible',
                        'format' => 'raw',
                        'filter' => false,
                        'enableSorting' => false,
                        'value' => function($model) use ($disabled){
                            return Html::input('number', 'cantidad_disponible[]', (($model->cantidad_disponible)?$model->cantidad_disponible:0), ['class'=> 'form-control cantidad-disponible', 'style' => 'font-size: 10pt; text-align: center;', 'required' => 'true', 'disabled' => $disabled ]);
                        },
                        'footer' => '',
                        'headerOptions' => ['width' => '90',],
                    ],
                    [
                        'label' => 'Fecha Entrega',
                        'attribute' => 'fecha_entrega',
                        'format' => 'raw',
                        'filter' => false,
                        'enableSorting' => false,
                        'value' => function($model) use ($disabled){
                            return DatePicker::widget([
                                'name' => 'fecha_entrega[]', 
                                'value' => ($model->fecha_entrega)?date('d-M-Y', strtotime($model->fecha_entrega)):'',
                                'options' => ['placeholder' => 'Fecha', 'required' => 'true', 'disabled' => $disabled],
                                'pluginOptions' => [
                                    'format' => 'dd-mm-yyyy',
                                    'todayHighlight' => true,
                                ]
                            ]);
                        },
                        'headerOptions' => ['width' => '240',],
                        'footer' => '',
                    ],
                    [
                        'label' => 'Garantía',
                        'attribute' => 'garantia',
                        'format' => 'raw',
                        'filter' => false,
                        'enableSorting' => false,
                        'value' => function($model) use ($disabled){
                            return Select2::widget([
                                'name' => 'garantia[]',
                                'data' => ['10 días'=>'10 días', '15 días'=>'15 días', '30 días'=>'30 días', '60 días'=>'60 días', '3 meses'=>'3 meses', '6 meses'=>'6 meses', '12 meses'=>'12 meses'],
                                'options' => ['placeholder' => 'Selecciona', 'required' => 'true', 'disabled' => $disabled],
                                'value' => (($model->garantia)?$model->garantia:''),
                            ]);
                        },
                        'footer' => '',
                    ],
                    [
                        'label' => '% de Descuento',
                        'attribute' => 'ahorro',
                        'format' => 'raw',
                        'filter' => false,
                        'enableSorting' => false,
                        'value' => function($model, $key, $index) use ($disabled){
                            return Html::input('number', 'descuento[]', (($model->ahorro)?$model->ahorro:0), ['class'=> 'form-control descuento', 'style' => 'font-size: 10pt; text-align: center;', 'min' => 0, 'max' => 100, 'required' => 'true', 'data' => [ 'key' => $key, 'index' => $index, 'cantidad' => $model->cantidad_solicitada ], 'disabled' => $disabled ]);
                        },
                        'footer' => '',
                        'headerOptions' => ['width' => '90',],
                    ],
                    [
                        'label' => 'Costo',
                        'attribute' => 'costo',
                        'format' => 'raw',
                        'filter' => false,
                        'enableSorting' => false,
                        'value' => function($model, $key, $index) use ($disabled){
                            return Html::hiddenInput('id_pieza[]', $model->id) . Html::input('text', 'costo[]', (($model->costo)?$model->costo:0), ['class'=> 'form-control costo', 'style' => 'font-size: 10pt; text-align: center;', 'required' => 'true', 'data' => [ 'key' => $key, 'index' => $index, 'cantidad' => $model->cantidad_solicitada ], 'disabled' => $disabled ]);
                        },
                        'footer' => 'TOTAL DESCUENTO',
                        'headerOptions' => ['width' => '200',],
                    ],
                    [
                        'label' => 'SUBTOTAL',
                        'format' => 'raw',
                        'filter' => false,
                        'enableSorting' => false,
                        'value' => function($model, $key, $index){
                            return '<span id="sub-'.$key.'">0</span>';
                        },
                        'footer' => '<span id="total-descuento">0</span>',
                    ],
                ],
            ]); ?>
        </div>
    </div>


    </br>
    </br>
    </br>
    </br>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="form-group text-center">
                <?= Html::submitButton('ENVIAR', ['class' => 'btn btn-submit', 'disabled' => $disabled]) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

    <!-- Modal Solciitar Cotizacion a Proveedores -->
<div id="agreagrImagen" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="title">OBSERVACIONES Y IMAGANES DE PIEZAS</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true" style="color: #000; font-weight: bold;"></i></button>
          </div>
        <div class="modal-body">
            </br>
            <?php $form = ActiveForm::begin([ 'id' => 'solicitar', 'action' => ['/solicitud-compra-pieza/guardarimagenpiezaproveedor',] ]); ?>
                <div class="row">

                <div class="col-md-12">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'id' => 'modal-imagenes',
                    'tableOptions' => ['class' => 'grid-blue table table-responsive table-bordered'],
                    'summary' => '',
                    'pjax' => true,
                    'options' =>[
                        'class' => '',
                    ],
                    'columns' => [
                        [
                            'label' => 'Descripción',
                            'attribute' => 'idCotizacionTallerPieza.idPieza.nombre',
                            'format' => 'raw',
                            'filter' => false,
                            'enableSorting' => false,
                            'contentOptions'=>['style'=>'text-align:left;'],
                            'headerOptions' => ['width' => '100',],
                            'footer' => '',
                        ],
                        [
                            'attribute' => 'id_pieza',
                            'label' => 'Observación Pieza',
                            'format' => 'raw',
                            'enableSorting' => false,
                            'contentOptions'=>['style'=>'text-align:center;'],
                            'headerOptions' => ['width' => '130',],
                            'value' => function($model) {
                                return Html::hiddenInput('id[]', $model->id) . Html::input('text', 'observacion_pieza[]', (($model->observacion_pieza)?$model->observacion_pieza:""), ['class'=> 'form-control', 'style' => 'font-size: 10pt; text-align: center;']);
                            }
                        ],
                        [
                            'label' => 'Cantidad Disponible',
                            'attribute' => 'cantidad_disponible',
                            'format' => 'raw',
                            'filter' => false,
                            'enableSorting' => false,
                            'value' => function($model) use ($form) {
                                $ip = $form->field($model, 'imageFiles[]')->fileInput(['accept' => 'image/*', 'class' =>"form-control", 'style' => 'border:0;'])->label('');
                                $imagen = " <a href='".Yii::$app->request->baseUrl.$model->imagen_pieza."' data-lightbox='".Yii::$app->request->baseUrl.$model->imagen_pieza."'>
                                                <img src= '".Yii::$app->request->baseUrl.$model->imagen_pieza."' style='max-width:30%;' class='".Yii::$app->request->baseUrl.$model->imagen_pieza."' />
                                            </a></br>";
                                return ($model->imagen_pieza)? $imagen . $ip : 'Sin imagen' . $ip;
                            },
                            'footer' => '',
                            'headerOptions' => ['width' => '90',],
                        ],
                    ],
                ]); ?>
            </div>
                </div>

            <?php ActiveForm::end(); ?>
            </br>
            <?php
            echo '<div class="row" style="padding-top: 25px;">';
                echo '<div class="text-center">';
                    echo Html::button('CANCELAR', ['class' => 'btn btn-cancelar', 'data' => ['dismiss' => 'modal']]);
                    echo "&nbsp;";
                    echo Html::submitButton('CONTINUAR', ['class' => 'btn btn-aceptar', 'form' => 'solicitar', 'id' => 'submit-solicitar' ]);
                echo '</div>';
            echo '</div>';
            ?>
        </div>
    </div>
  </div>
</div>
<!-- Fin modal-->

</div>

<style>
    #cotizacion-proveedor-piezas-grid tfoot > tr > td {
        text-align: right;
    }

    #cotizacion-proveedor-piezas-grid tfoot > tr > td:nth-child(8) {
        text-align: center;
        font-weight: initial;
    }
</style>

<script>

    //Calcula los totales al inicio
    $( document ).ready(function() {
        calcularTotales();
    });

    //CAlcula los totales al cambiar un valor
    $(document.body).on('change','input.costo, input.descuento', function () {
        calcularTotales( );
    });

    //Calculando el subtotal por pieza quitandole el descuento
    function calcularSubTotales( costo, descuento, cantidad, key ){
        var subtotal = (costo * cantidad) - (((costo * cantidad) * descuento)/100);
        $("tr[data-key='" + key + "']").find('span#sub-' +key).html( subtotal + ' RD$' );
        return subtotal;
    }

    //Calculando los totales generales
    function calcularTotales( ){
        //Se recalculan los totales
        var total_descuento = 0;
        var itbis = 0;
        var total = 0;

        $('input.costo').each(function(index, el) {
            var key = $(this).data('key');

            var costo = ( $("tr[data-key='" + key + "']").find('input.costo').val() != '' )?parseFloat( $("tr[data-key='" + key + "']").find('input.costo').val() ) : 0;
            var descuento = ( $("tr[data-key='" + key + "']").find('input.descuento').val() != '' )?parseFloat( $("tr[data-key='" + key + "']").find('input.descuento').val() ) : 0;
            var cantidad = $(this).data('cantidad');

            total_descuento += calcularSubTotales( costo, descuento, cantidad, key );
        });

        itbis = (total_descuento * 0.18);
        total = total_descuento + itbis;

        $('#total-descuento').html(total_descuento +' RD$');
        $('#itbis').html(itbis+' RD$');
        $('#total').html(total+' RD$');
    }

    $('.table').removeClass( "table-bordered table-striped" );
</script>