<?php 
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\grid\GridView;
use kartik\select2\Select2;
use kartik\date\DatePicker;

$this->title = 'DEVOLUCIÓN DE PIEZAS';
$disabled = (false);
?>
<div class="devolucion">
	<div class="row">
		<div class="col-md-1 col-xs-3">
			<h3 class="titulo"><i class="fa fa-undo fa-4x" aria-hidden="true"></i></h3>
		</div>
		<div class="col-md-10 col-xs-9" style="padding-top: 20px;">
			<h2 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h2>
		</div>
	</div>
	</br>
    <?php $form = ActiveForm::begin(['id' => 'devolucion-form']); ?>

    <div class="row">
        <div class="col-md-4 col-xs-12">
            <div class="pull-rigth" style="margin-top: 30px;">
                <?= Html::a('Listado de piezas devueltas',[ '/solicitud-compra-pieza/indexdevolucionproveedor' ], ['class' => 'btn btn-small-gris']) ?> 
            </div>
        </div>
        <div class="col-md-3 col-md-offset-5 col-xs-12">
            <?= $form->field($model_orden_compra, 'id')->textInput(['disabled' => true, ])->label('NÚMERO DE ORDEN DE COMPRA') ?>
        </div>
        </br>
    </div>
    <br>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DEL VEHÍCULO</p>
            <hr>
        </div>
    </div>


    <div class="row">

        <div class="col-md-3 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_modelo')->textInput(['disabled' => true, 'value' => $model_vehiculo->idModelo->idMarca->nombre ])->label('Marca') ?>
        </div>

        <div class="col-md-3 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_modelo')->textInput(['value' => strtoupper($model_vehiculo->idModelo->nombre), 'disabled' => true])->label('Modelo') ?>
        </div>

        <div class="col-md-3 col-xs-12">
            <?= $form->field($model_vehiculo, 'chasis')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>

        <div class="col-md-3 col-xs-12">
            <?= $form->field($model_vehiculo, 'anho')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>

    </div>
    <br>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">LISTADO DE PIEZAS DEVUELTAS</p>
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'summary' => '',
                'id' =>'devolucion-grid',
                'tableOptions' => ['class' => 'table table-responsive', 'id' =>'cotizacion-pieza-table'],
                'options' =>[
                    'class' => 'grid-blue',
                ],
                'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                'columns' => [
                   [
                        'attribute' => 'idCotizacionTallerPieza.idPieza.tipoPieza.nombre',
                        'label' => 'Tipo',
                        'format' => 'text',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:left;'],
                    ],
                    [
                        'attribute' => 'idCotizacionTallerPieza.idPieza.nombre',
                        'label' => 'Descripcion',
                        'format' => 'text',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:left;'],
                    ],
                    [
                        'attribute' => 'cantidad_devolucion',
                        'label' => 'Cantidad Devuleta',
                        'format' => 'raw',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'headerOptions' => ['width' => '150',],
                    ],
                    [
                        'label' => 'Motivo de devolución',
                        'attribute' => 'motivoDevolucion.nombre',
                        'format' => 'raw',
                        'filter' => false,
                        'enableSorting' => false,
                    ],
                    [
                        'label' => 'Nueva Fecha Entrega',
                        'attribute' => 'fecha_entrega_nueva',
                        'format' => 'raw',
                        'filter' => false,
                        'enableSorting' => false,
                        'value' => function($model) use ($disabled){
                            return DatePicker::widget([
                                'name' => 'fecha_entrega[]', 
                                'value' => ($model->fecha_entrega_nueva)?date('d-M-Y', strtotime($model->fecha_entrega_nueva)):'',
                                'options' => ['placeholder' => 'Fecha', 'required' => 'true', 'disabled' => $disabled],
                                'pluginOptions' => [
                                    'format' => 'dd-mm-yyyy',
                                    'todayHighlight' => true,
                                ]
                            ]);
                        },
                        'headerOptions' => ['width' => '240',],
                        'footer' => '',
                    ],
                    [
                        'attribute' => 'costo',
                        'label' => 'Costo',
                        'format' => 'text',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:right;'],
                        'value' => function( $model ){
                            return number_format(floatval($model->costo), 2, ',', '.').' RD$';
                        },
                    ],
                    [
                        'class' => 'yii\grid\CheckboxColumn',
                        'checkboxOptions'=>['style'=>'display: block;margin-right: auto;margin-left: auto;'],//center checkboxes
                        'header' => Html::checkBox('selection_all', false, [
                            'class' => 'select-on-check-all hidden',//pull right the checkbox
                            'label' => 'Devolver Pieza',//pull left the label
                        ]),
                    ],
                ],
            ]); ?>
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="form-group text-center">
                <?= Html::submitButton('ACEPTAR', ['class' => 'btn btn-submit']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<style>
    .grid-blue .table > tbody > tr.active > td {
        background-color: #b9f3bc;
    }
</style>

<script>
    /*$('input[name="selection[]"]').on('change', function(event) {
        event.preventDefault();
        if ( this.checked ) {
            $('tr[data-key="'+$(this).val()+'"]').addClass('active');
            $('tr[data-key="'+$(this).val()+'"]>td>input.cantidad').prop({
                disabled: 'true',
            });
            $('tr[data-key="'+$(this).val()+'"]>td>input.pieza_id').prop({
                disabled: 'true',
            });
            $('tr[data-key="'+$(this).val()+'"]>td>select.motivo-devolucion').prop({
                disabled: 'true',
            });
        } else{
            $('tr[data-key="'+$(this).val()+'"]').removeClass('active');
            $('tr[data-key="'+$(this).val()+'"]>td>input.cantidad').removeAttr("disabled");
            $('tr[data-key="'+$(this).val()+'"]>td>input.pieza_id').removeAttr("disabled");
            $('tr[data-key="'+$(this).val()+'"]>td>select.motivo-devolucion').removeAttr("disabled");
            
        }
    });

    $('#devolucion-form').on('submit', function(event) {
        //$('#despacho-pieza-submit').prop('disabled', 'true');
        event.preventDefault();
        event.stopImmediatePropagation();
        console.log('entro');

        var keys = $('#devolucion-grid').yiiGridView('getSelectedRows');
        var cantidad_devolucion = [];
        var id_cotizacion_proveedor_piezas = [];
        var motivo_devolucion = [];
        //var id_despacho = $(this).data('id-pedido');

        $.each(keys, function (ind, elem) {
            cantidad_devolucion.push($('tr[data-key="'+elem+'"]>td>input.cantidad').val());
            motivo_devolucion.push($('tr[data-key="'+elem+'"]>td>select.motivo-devolucion').val());
        });

        var form = $(this);
        var data = form.serializeArray();      
        data.push({name: 'id_cotizacion_proveedor_piezas', value: keys});
        data.push({name: 'cantidades_devolucion', value: cantidad_devolucion});
        data.push({name: 'motivo_devolucion', value: motivo_devolucion});

        $.post( form.attr("action"), data ).done(function(response){
            //console.log(response);
            location.reload();
        });
    });*/
</script>