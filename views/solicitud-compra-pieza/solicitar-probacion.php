<?php
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\select2\Select2;

$this->title = 'SOLICITUD DE APROBACIÓN';

?>

<div class="solicitud-aprobacion">
	<div class="row">
        <div class="col-md-1 col-xs-3">
            <i class="fa fa-check fa-5x" aria-hidden="true"></i>
        </div>
        <div class="col-md-10 col-xs-9" style="padding-top: 10px;">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
        </div>
    </div>
	</br>
	<div class="row">
		<div class="col-md-4 col-xs-12">
			<div class="form-group">
                <label class="control-label" for="id_estado">FILTRAR POR PIEZA</label>
                <?=  
                    Select2::widget([
                        'name' => 'id_pieza',
                        'data' => $piezas,
                        'options' => ['placeholder' => 'Selecciona la pieza', 'id' => 'id-pieza'],
                        'pluginOptions' => [ 'allowClear'=>true ],
                    ]); 
                ?>
            </div>
		</div>
        <div class="col-md-8 col-xs-12">
            <div class="pull-right" style="margin-top: 30px;">
                <?= Html::a('Listado de solicitudes',[ '/solicitud-compra-pieza/index' ], ['class' => 'btn btn-small-gris']) ?> 
            </div>
        </div>
    </div>
	</br>
	<?php $form = ActiveForm::begin(['id' => 'solicitud-aprobacion-form']); ?>
	<div class="row">
		<?php foreach ($cotizaciones_piezas as $key => $cotizacion): ?>
			<div class="col-md-6 col-xs-12">
				<div class="panel panel-default" data-pieza="<?= $cotizacion->idCotizacionTallerPieza->id_pieza ?>">
				  	<div class="panel-heading text-center" data-id="<?= $cotizacion->id ?>" >
				  		<b> <?= $cotizacion->idCotizacionProveedor->idSolicitudCompraProveedor->idProveedor->nombre ?> </b>
				  	</div>
		    		<table class="table table-bordered">
		    			<tbody>
		    				<tr>
		    					<td><b>Pieza: </b> <?= strtoupper($cotizacion->idCotizacionTallerPieza->idPieza->nombre) ?> </td>
		    				</tr>
		    				<tr>
		    					<td><b>Precio Final: </b> <?= ($cotizacion->costo)?'RD$ '.$cotizacion->costo:'<span class="sin-respuesta">SIN RESPUESTA</span>' ?> </td>
		    				</tr>
		    				<tr>
		    					<td><b>Fecha de entrega: </b> <?= ($cotizacion->fecha_entrega)?date('d/m/Y', strtotime($cotizacion->fecha_entrega)):'<span class="sin-respuesta">SIN RESPUESTA</span>' ?> </td>
		    				</tr>
		    			</tbody>
			    	</table>
				</div>
			</div>
		<?php endforeach ?>
	</div>
	</br>
	</br>
	</br>
	</br>
	<div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="form-group text-center">
                <?= Html::submitButton('Guardar', ['class' => 'btn btn-submit', 'id' => 'guardar-submit']) ?>
            </div>
        </div>
    </div>
	<?php ActiveForm::end(); ?>
	
</div>

<style>
	.panel{
		border-radius: 0px;
	}

	.panel .panel-heading{
		font-size: 12pt;
	}

	.sin-respuesta{
		color: #c51414;
		font-weight: bold;
	}

	.panel-heading {
	    padding: 5px 5px;
	}

	.panel-heading.selected {
		background-color: #c5e0b4;
	}

	.modal-dialog {
        margin: 80px auto;
    }
</style>
<script>
	$('#id-pieza').on('change', function(event) {
		event.preventDefault();
		/* Act on the event */
		var id_pieza = $(this).val();
		$('.panel').hide();
		if ( id_pieza == '') {
			$('.panel').show();
		} else{
			$('div[data-pieza='+id_pieza+']').show();
		}
	});
</script>