<?php
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use app\models\SolicitudCompraProveedor;

$this->title = 'COTIZACIONES DE PIEZAS';
/*echo "<pre>";
print_r($cotizaciones_piezas);
echo "</pre>";*/
$id = Yii::$app->request->get('id');
?>

<div class="cotizaciones-recibidas">
	<div class="row">
		<div class="col-md-1 col-xs-3">
			<h4 class="titulo"><img src="<?= Yii::$app->request->baseUrl ?>/img/hand_money_64.png" alt=""></h4>
		</div>
		<div class="col-md-10 col-xs-9" style="padding-top: 30px;">
			<h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
		</div>
	</div>
	</br>
	<div class="row">
		<div class="col-md-4 col-xs-12">
			<div class="form-group">
                <label class="control-label" for="id_estado">FILTRAR POR PIEZA</label>
                <?=  
                    Select2::widget([
                        'name' => 'id_pieza',
                        'data' => $piezas,
                        'options' => ['placeholder' => 'Selecciona la pieza', 'id' => 'id-pieza'],
                        'pluginOptions' => [ 'allowClear'=>true ],
                    ]); 
                ?>
            </div>
		</div>
        <div class="col-md-8 col-xs-12">
            <div class="pull-right" style="margin-top: 30px;">
                <?= Html::a('Listado de solicitudes',[ '/solicitud-compra-pieza/index' ], ['class' => 'btn btn-small-gris']) ?> 
				<?= Html::a('Invitar proveedores','#', ['class' => 'btn btn-small-gris', 'data' => [ 'id' => $model->id, 'toggle' => 'modal', 'target' => '#solicitarCotizacionModal' ] ]) ?> 
			</div>
        </div>
    </div>
	</br>
	<?php $form = ActiveForm::begin(['id' => 'cotizaciones-recibidas-form']); ?>
	<div class="row">
		<div class="col-xs-12">
			<?php
				if($model->aprobacion == 5):
					echo Html::a('Ver cotizacion rechazada.','#', ['class' => 'btn btn-small-gris analista-mtto', 'data' => ['id' => $model->id, 'toggle' => 'modal', 'target' => '#cotizacionRechazada']]);
				endif;
			?>
			<br>
			<br>
		</div>
		<input type="text" id="count-piezas" value="<?= count($piezas); ?>" hidden="true">
	<?php
		$piezaPrecio = [];
		$piezaID = [];
		$piezaProveedor = [];	
	?>
		<?php foreach ($cotizaciones_piezas as $key => $cotizacion): ?>
			<div id="prove-<?= $cotizacion->idCotizacionProveedor->idSolicitudCompraProveedor->idProveedor->id ?>" class="col-md-6 col-xs-12 pieza-content"  data-pieza="<?= $cotizacion->idCotizacionTallerPieza->id_pieza ?>">
				<div class="panel panel-default">
				  	<div class="panel-heading text-center pieza-<?= $cotizacion->idCotizacionTallerPieza->id_pieza ?> pp-<?= $cotizacion->idCotizacionProveedor->idSolicitudCompraProveedor->idProveedor->id."-".$cotizacion->idCotizacionTallerPieza->id_pieza ?>"  data-id="<?= $cotizacion->id ?>" data-estado="<?= $cotizacion->idCotizacionProveedor->estado ?>" data-idpieza="<?= $cotizacion->idCotizacionTallerPieza->id_pieza ?>">
				  		<b> <?= $cotizacion->idCotizacionProveedor->idSolicitudCompraProveedor->idProveedor->nombre ?> </b>
						<input type="checkbox" id="check-<?= $cotizacion->id?>" name="selected[]" value="<?= $cotizacion->id ?>" hidden="true">
				  		<span class="pull-right" id="icon-<?= $cotizacion->id?>" hidden="true"><i class="fa fa-check-circle fa-lg" aria-hidden="true" style="color: #4b8723; background-color: #c5e0b4;"></i></span>
				  	</div>
		    		<table class="table table-bordered">
		    			<tbody>
		    				<tr>
		    					<td colspan="3"><b>Pieza: </b> <?= strtoupper($cotizacion->idCotizacionTallerPieza->idPieza->nombre) ?> </td>
		    				</tr>
		    				<tr>
		    					<td><b>% Descuento: </b> <?= ($cotizacion->ahorro)?$cotizacion->ahorro:'<span class="sin-respuesta">SIN RESPUESTA</span>' ?></td>
		    					<td><b>Precio: </b> <?= ($cotizacion->costo)?'RD$ '.$cotizacion->costo:'<span class="sin-respuesta">SIN RESPUESTA</span>' ?> </td>
		    					<td><b>Precio final: </b> <?= ($cotizacion->costo)?'RD$ '.(($cotizacion->costo) - (($cotizacion->costo * $cotizacion->ahorro)/ 100)).' RD$ ':'<span class="sin-respuesta">SIN RESPUESTA</span>' ?> </td>
		    				</tr>
		    				<tr>
		    					<td><b>Fecha de entrega: </b> <?= ($cotizacion->fecha_entrega)?date('d/m/Y', strtotime($cotizacion->fecha_entrega)):'<span class="sin-respuesta">SIN RESPUESTA</span>' ?> </td>
		    					<td><b>Garantía: </b> <?= ($cotizacion->garantia)?$cotizacion->garantia:'<span class="sin-respuesta">SIN RESPUESTA</span>' ?> </td>
		    					<td><b>Entrega a domicilio: </b> <?= $cotizacion->idCotizacionProveedor->idSolicitudCompraProveedor->idProveedor->domicilio ?> </td>
		    				</tr>
		    				<tr>
		    					<td><b>Cantidad disponible: </b> <?= ($cotizacion->cantidad_disponible)?$cotizacion->cantidad_disponible:0 ?> </td>
		    					<td colspan="2"><div class="col-xs-5"><b>Cantidad a comprar: </b></div> <div class="col-xs-4"><?= Html::textInput('comprar[]', $cotizacion->cantidad_solicitada,['class' => 'form-control pieza-'.$cotizacion->idCotizacionTallerPieza->id_pieza.'', 'type' => 'number', 'disabled' => true ]) ?></div></td>
		    				</tr>
		    			</tbody>
			    	</table>

					<?php							
						if($key==0){
							array_push($piezaPrecio, $cotizacion->costo);
							array_push($piezaID, $cotizacion->idCotizacionTallerPieza->id_pieza);
							array_push($piezaProveedor,$cotizacion->idCotizacionProveedor->idSolicitudCompraProveedor->idProveedor->id);
						}else {
							if(in_array($cotizacion->idCotizacionTallerPieza->id_pieza ,$piezaID )) {
								$indexPosition  = array_search($cotizacion->idCotizacionTallerPieza->id_pieza , $piezaID);
								if( $cotizacion->costo!=null  &&  $cotizacion->costo < $piezaPrecio[$indexPosition] ){
									$piezaPrecio[$indexPosition] = $cotizacion->costo;
									$piezaProveedor[$indexPosition] = $cotizacion->idCotizacionProveedor->idSolicitudCompraProveedor->idProveedor->id; 
								}
							}else {
								array_push($piezaPrecio, $cotizacion->costo);
								array_push($piezaID, $cotizacion->idCotizacionTallerPieza->id_pieza);
								array_push($piezaProveedor,$cotizacion->idCotizacionProveedor->idSolicitudCompraProveedor->idProveedor->id);
							}
						}
					?>
				</div>
			</div>
		<?php endforeach ?>		
	</div>
	</br>
	</br>
	</br>
	</br>
	<div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="form-group text-center">
                <?= Html::submitButton('Guardar', ['class' => 'btn btn-submit', 'id' => 'guardar-submit']) ?>
            </div>
        </div>
    </div>

	
</div>
<!-- Modal Observación -->
<div id="observacionModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="title"><img src="<?= Yii::$app->request->baseUrl ?>/img/bubble_info_64.png" alt="" style="height: 40px;">&nbsp;&nbsp; INFORMACIÓN IMPORTANTE</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true" style="color: #000; font-weight: bold;"></i></button>
          </div>
        <div class="modal-body">
            </br>
            <div class="row">
            	<div class="col-xs-10 col-xs-offset-1">
            		<p><b>NOTA:</b> Se deben seleccionar 3 cotizaciones por pieza para poder continuar con el proceso, de lo contrario indique una justificación.</p>
            	</div>
            </div>
        	</br>
                <div class="row">
                    <div class="col-xs-12 col-md-10 col-md-offset-1">
                        <?= $form->field($model, 'observacion')->textarea(['rows' => 3])->label('OBSERVACIONES') ?>
                    </div>
                </div>
            </br>
            <?php
            echo '<div class="row" style="padding-top: 25px;">';
                echo '<div class="text-center">';
                    echo Html::button('CANCELAR', ['class' => 'btn btn-cancelar', 'data' => ['dismiss' => 'modal']]);
                    echo "&nbsp;";
                    echo Html::submitButton('CONTINUAR', ['class' => 'btn btn-aceptar', 'form' => 'cotizaciones-recibidas-form',]);
                echo '</div>';
            echo '</div>';
            ?>
        </div>
    </div>
  </div>
</div>
<!-- Fin modal-->
<!-- Modal Cotización Rechazada  -->
<div id="cotizacionRechazada" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="title"><i class="fa fa-user fa-lg" aria-hidden="true"></i>&nbsp;&nbsp; COTIZACIÓN RECHAZADA</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true" style="color: #000; font-weight: bold;"></i></button>
        </div>
        <div class="modal-body">
			<div class="row">
				<div class="col-xs-12">
					<p>
						<strong>Observación: </strong> <?= ($model->observacion_gerente)? $model->observacion_gerente : 'No se registro ninguna observación'; ?>
						<br>
						<br>
					</p>
				</div>
				<div class="col-xs-12">
					<table class="table grid-blue table-responsive table-bordered">
						<thead>
							<tr>
								<th>Descripción</th>
								<th>Cantidad</th>
								<th>Proveedor</th>
								<th>Garantia</th>
								<th>Fecha entrega</th>
								<th>Costo</th>
							</tr>
						</thead>
						<tbody>
							<?php $subtotal = 0; ?>    
							<?php foreach ($cotizaciones_piezas_rechazadas as $i => $cotizacion_rechazadas): ?>
								<tr class="row-pieza" data-pieza="<?= $cotizacion_rechazadas->idCotizacionTallerPieza->id_pieza ?>">
									<td><?= ucfirst($cotizacion_rechazadas->idCotizacionTallerPieza->idPieza->nombre) ?></td>
									<td><?= $cotizacion_rechazadas->cantidad_comprar ?></td>
									<td><?= $cotizacion_rechazadas->idCotizacionProveedor->idSolicitudCompraProveedor->idProveedor->nombre ?></td>
									<td><?= $cotizacion_rechazadas->garantia ?></td>
									<td><?= date('d/m/Y', strtotime($cotizacion_rechazadas->fecha_entrega)) ?></td>
									<td>
										<?php  
											$costo = (($cotizacion_rechazadas->costo * $cotizacion_rechazadas->cantidad_comprar) - ((($cotizacion_rechazadas->costo * $cotizacion_rechazadas->cantidad_comprar) * $cotizacion_rechazadas->ahorro)/ 100));
											echo $costo.' RD$ ';
										?> 
									<?= Html::hiddenInput('id[]', $cotizacion_rechazadas->id,[]) ?>
									</td>
								</tr>
								<?php $subtotal +=  $costo?>
							<?php endforeach ?>
							<tr>
								<td colspan="5" style="text-align: right; font-weight: bold;">SUBTOTAL</td>
								<td><?= $subtotal.' RD$ ' ?></td>
							</tr>
							<tr>
								<td colspan="5" style="text-align: right; font-weight: bold;">ITBIS</td>
								<td><?php $itbis = ($subtotal * 0.18); echo $itbis.' RD$ '; ?></td>
							</tr>
							<tr>
								<td colspan="5" style="text-align: right; font-weight: bold;">TOTAL</td>
								<td><?php $total = ($subtotal + $itbis); echo $total.' RD$ '; ?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
            </br>
            </br>
            <?php
            echo '<div class="row" style="padding-top: 25px;">';
                echo '<div class="text-center">';
                    echo Html::button('CANCELAR', ['class' => 'btn btn-cancelar', 'data' => ['dismiss' => 'modal']]);
                echo '</div>';
            echo '</div>';
            ?>
        </div>
    </div>
  </div>
</div>
<!-- Fin modal-->
<?php ActiveForm::end(); ?>

<?php if(\Yii::$app->user->can('analista-compras')): ?>
<!-- Modal Solciitar Cotizacion a Proveedores -->
<div id="solicitarCotizacionModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="title"><img src="<?= Yii::$app->request->baseUrl ?>/img/give_money_64.png" alt="" style="height: 40px;">&nbsp;&nbsp; SOLICITUD DE COTIZACIÓN DE PIEZA</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true" style="color: #000; font-weight: bold;"></i></button>
          </div>
        <div class="modal-body">
            </br>
            <?php $form = ActiveForm::begin([ 'id' => 'solicitar', 'action' => ['/solicitud-compra-pieza/solicitudcotizacioninvitacion',] ]); ?>
                <div class="row">
                    <div class="col-xs-12 col-md-11 col-md-offset-1">
                        <label class="control-label" for="garantiavehiculo-motivo">PROVEEDORES</label>
                        <div id="response-proveedor" class="checkbox">
                        </div>
                    </div>
                </div>

            <?php ActiveForm::end(); ?>
            </br>
            <?php
            echo '<div class="row" style="padding-top: 25px;">';
                echo '<div class="text-center">';
                    echo Html::button('CANCELAR', ['class' => 'btn btn-cancelar', 'data' => ['dismiss' => 'modal']]);
                    echo "&nbsp;";
                    echo Html::submitButton('CONTINUAR', ['class' => 'btn btn-aceptar', 'form' => 'solicitar', 'id' => 'submit-solicitar' ]);
                echo '</div>';
            echo '</div>';
            ?>
        </div>
    </div>
  </div>
</div>
<!-- Fin modal-->
<?php endif ?>


<style>
	.panel{
		border-radius: 0px;
	}

	.panel .panel-heading{
		font-size: 12pt;
	}

	.sin-respuesta{
		color: #c51414;
		font-weight: bold;
	}

	.panel-heading{
	    padding: 5px 5px;
	}

	.panel-heading.selected {
		background-color: #c5e0b4 !important;
	}

	.panel-heading.sugerido {
		background-color: #33adff;
	}

	.modal-dialog {
        margin: 80px auto;
    }

</style>

<script>

	

	$(function() {  
		var proveedor = <?php echo json_encode($piezaProveedor) ?>;
		var piezas =  <?php echo json_encode($piezaID) ?>;
		for(var i=0 ; i< proveedor.length ; i++) {
			$(".cotizaciones-recibidas").find('div.pp-'+proveedor[i]+'-'+piezas[i]).addClass('sugerido');				
		}
	});

	$('.panel-heading').on('click', function(event) {
		event.preventDefault();
		/* Act on the event */
		var id = $(this).data('id');
		var estado = $(this).data('estado');
		var idpieza = $(this).data('idpieza');
		var parent = $(this).parent().parent().attr('id');
		if (estado != 0) {
			if ( $('#check-'+id).is(':checked') ) {
				$('#check-'+id).prop({
					checked: false,
				});
				$(this).removeClass('selected');
				$('#icon-'+id).hide();			

				$('.pieza-'+idpieza+'').parent().parent().show();
				$('#'+parent+' input.pieza-'+idpieza+'').prop('disabled', true);
			} else{
				$('#check-'+id).prop({
					checked: true,
				});
				$(this).addClass('selected');
				$('#icon-'+id).show();

				$('.pieza-'+idpieza+'').parent().parent().hide();
				$('#'+parent+' .pieza-'+idpieza+'').parent().parent().show();
				$('#'+parent+' input.pieza-'+idpieza+'').prop('disabled', false);

			}
		}
	});

	$('#guardar-submit').on('click', function(event) {
		event.preventDefault();
		/* Act on the event */
		var total = $("input[name='selected[]']:checked").length;
		var countpiezas = $('#count-piezas').val();
		if (total < countpiezas) {
			alert('Debes Selecionar todas las piezas');
		} else{
			$(this).submit();
		}
	});

	// $('#guardar-submit').on('click', function(event) {
	// 	event.preventDefault();
	// 	/* Act on the event */
	// 	var total = $("input[name='selected[]']:checked").length;

	// 	if (total < 3) {
	// 		$('#observacionModal').modal('show');
	// 	} else{
	// 		$(this).submit();
	// 	}
	// });

	$('#id-pieza').on('change', function(event) {
		event.preventDefault();
		/* Act on the event */
		var id_pieza = $(this).val();
		$('.pieza-content').hide();
		if ( id_pieza == '') {
			$('.pieza-content').show();
		} else{
			$('div[data-pieza='+id_pieza+']').show();
		}
	});


	$('#solicitarCotizacionModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var id = button.data('id');
        $('#submit-solicitar').data('id', id);
        $.ajax({
            url: "../getproveedores",
            type: 'post',
            data: {
                id: id,
            },
            success: function (response) {
				console.log(response);
                $('#response-proveedor').empty();
                for (var i = 0; i < response.length; i++) {
                    $('#response-proveedor').append(
                        '<div class="col-md-6 col-xs-12">'+
                            '<label class="checkbox-inline">'+
                                '<input type="checkbox" value="'+response[i]['id']+'" name="proveedor[]"> '+response[i]['nombre']+
                            '</label>'+
                        '</div>'
                    );
                }
            }
        });
    });

    $('#solicitar').on('submit', function(event) {
        event.preventDefault();
        event.stopImmediatePropagation();

        $('#submit-solicitar').attr({
            disabled: 'true'
        });

        $('#solicitarCotizacionModal').modal('hide');
        var id = $('#submit-solicitar').data('id');

        var form = $(this);
        var data = form.serializeArray();      
        data.push({name: 'id', value: id});

        $.post( form.attr("action"), data ).done(function(response){
            location.reload();
        });
    });
</script>

