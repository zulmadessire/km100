<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\form\ActiveForm;
use kartik\grid\GridView;
use kartik\select2\Select2;
use app\models\DespachoProveedorPieza;
use app\models\Mensajero;

$this->title = 'ENVÍO DE PIEZAS - ENVÍO NÚMERO ';
/*echo "<pre>";
print_r($model);
echo "</pre>";*/
?>

<div class="despacho">
	<div class="row">
		<div class="col-md-1 col-xs-3">
			<h4 class="titulo"><i class="fa fa-truck fa-4x" aria-hidden="true"></i></h4>
		</div>
		<div class="col-md-10 col-xs-9" style="padding-top: 20px;">
			<h4 class="titulo"><span> <?= Html::encode( $this->title . $model_despacho->numero ) ?></span></h4>
		</div>
	</div>

    <?php $form = ActiveForm::begin([ 'id' => 'despacho-pieza-form' ]); ?>

    <div class="row">
        <div class="col-xs-6 col-xs-offset-5">
            <table class="table table-responsive table-bordered">
                <tr>
                    <th style="text-align: left;">NOMBRE TALLER</th>
                    <td style="text-align: center;"><?= $model_despacho->idTaller->nombre ?></td>
                </tr>
                <tr>
                    <th style="text-align: left;">DIRECCIÓN TALLER</th>
                    <td style="text-align: center;"><?= $model_despacho->idTaller->direccion ?></td>
                </tr>
                <tr>
                    <th style="text-align: left;">REPRESENTANTE DE REPARTO</th>
                    <td style="text-align: center;"><?=
                        $form->field($model_despacho, 'id_mensajero')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(Mensajero::find()->all(), 'id', 'nombre'),
                                'options' => ['placeholder' => 'Selecciona el representante', 'id' => 'mensajero'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ])->label(false);
                        ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="clearfix"></div>
    </br>
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'summary' => '',
                'id' =>'despacho-pieza-grid',
                'pjax' => true,
                'tableOptions' => ['class' => 'table table-responsive', 'id' =>'cotizacion-pieza-table'],
                'options' =>[
                    'class' => 'grid-blue',
                ],
                'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                'columns' => [
                    [
                        'attribute' => 'idCotizacionProveedor.idSolicitudCompraProveedor.idProveedor.nombre',
                        'label' => 'Proveedor',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:center; width:80px;'],
                        'headerOptions' => ['width' => '150',],
                    ],
                    [
                        'label' => 'Tipo',
                        'format' => 'text',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:center;'],
                        'value' => function($model){
                            if ($model->idCotizacionTallerPieza->idPieza->tipoPieza->nombre) {
                                return $model->idCotizacionTallerPieza->idPieza->tipoPieza->nombre;
                            } else{
                                return '-';
                            }
                        },
                    ],
                    [
                        'attribute' => 'idCotizacionTallerPieza.idPieza.nombre',
                        'label' => 'Descripcion',
                        'format' => 'text',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:left;'],
                    ],
                    [
                        'attribute' => 'cantidad_recibida',
                        'label' => 'Cantidad a Enviar',
                        'format' => 'raw',
                        'value' => function($model){
                            return Html::input('number', 'cantidad[]', $model->cantidad_recibida, ['class'=> 'form-control cantidad-enviar', 'style' => 'font-size: 10pt; text-align: center;', 'max' => $model->cantidad_recibida, 'min' => 1 ]);
                        },
                        'filter' => false,
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:center; width:80px;'],
                        'headerOptions' => ['width' => '150',],
                    ],
                    [
                        'class' => 'yii\grid\CheckboxColumn',
                        'checkboxOptions'=>['style'=>'display: block;margin-right: auto;margin-left: auto;'],
                        'header' => Html::checkBox('selection_all', false, [
                            'class' => 'select-on-check-all hidden',
                            'label' => 'Enviar',
                        ]),
                    ],                ],
            ]); ?>
        </div>
    </div>

    </br>
    </br>
    </br>
    </br>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="form-group text-center">
                <?= Html::a('REGRESAR', ['/solicitud-compra-pieza/despacho', 'id' => $model->id ], ['class' => 'btn btn-cancelar']) ?>&nbsp;
                <?= Html::submitButton('ENVIAR PIEZAS', ['class' => 'btn btn-aceptar', 'id' => 'despacho-pieza-submit']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<style>
    .grid-blue .table > tbody > tr.active > td {
        background-color: #b9f3bc;
    }
</style>

<script>
    //Cambiando el background
    $('input[name="selection[]"]').on('change', function(event) {
        event.preventDefault();
        /* Act on the event */
        if ( this.checked ) {
            $('tr[data-key="'+$(this).val()+'"]').addClass('active');
            $('tr[data-key="'+$(this).val()+'"]>td>input.cantidad-enviar').prop({
                disabled: 'true',
            });
        } else{
            $('tr[data-key="'+$(this).val()+'"]').removeClass('active');
            $('tr[data-key="'+$(this).val()+'"]>td>input.cantidad-enviar').removeAttr("disabled");
        }
    });

    $('#despacho-pieza-form').on('submit', function(event) {
        //$('#despacho-pieza-submit').prop('disabled', 'true');
        event.preventDefault();
        event.stopImmediatePropagation();

        var keys = $('#despacho-pieza-grid').yiiGridView('getSelectedRows');
        var cantidad_enviar = [];
        var id_cotizacion_proveedor_piezas = [];
        //var id_despacho = $(this).data('id-pedido');

        $.each(keys, function (ind, elem) {
            cantidad_enviar.push($('tr[data-key="'+elem+'"]>td>input.cantidad-enviar').val());
        });

        var form = $(this);
        var data = form.serializeArray();      
        data.push({name: 'id_cotizacion_proveedor_piezas', value: keys});
        data.push({name: 'cantidades_enviar', value: cantidad_enviar});

        $.post( form.attr("action"), data ).done(function(response){
            location.reload();
            //console.log(response);
        });
    });
    
    /*jQuery(document).ready(function($) {
        $('input[name="selection[]"]').on('change', function(event) {
            event.preventDefault();
            $('#pedido-despacho-pieza-submit').prop('disabled', !$('input[name="selection[]"]').filter(':checked').length);
        }).trigger('change');
    });*/

    $('.table').removeClass( "table-bordered table-striped" );
</script>