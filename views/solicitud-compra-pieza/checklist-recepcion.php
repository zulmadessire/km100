<?php 
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\grid\GridView;

$this->title = 'CHECKLIST DE RECEPCIÓN DE PIEZAS';
?>
<div class="checklist-recepcion">
	<div class="row">
		<div class="col-md-1 col-xs-3">
			<h4 class="titulo"><img src="<?= Yii::$app->request->baseUrl ?>/img/list_64.png" alt=""></h4>
		</div>
		<div class="col-md-10 col-xs-9" style="padding-top: 30px;">
			<h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
		</div>
	</div>
	</br>
	<?php $form = ActiveForm::begin(); ?>

	<div class="row">
        <div class="col-md-4 col-xs-12">
            <div class="pull-rigth" style="margin-top: 30px;">
                <?= Html::a('Listado de solicitudes',[ '/solicitud-compra-pieza/index' ], ['class' => 'btn btn-small-gris']) ?> 
            </div>
        </div>
        <div class="col-md-4 col-md-offset-4 col-xs-12">
        	<?= $form->field($model_orden_compra, 'id')->textInput(['disabled' => true, ])->label('NÚMERO DE ORDEN DE COMPRA') ?>
        </div>
        </br>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DEL VEHÍCULO</p>
            <hr>
        </div>
    </div>


    <div class="row">
		
		<div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'mva')->textInput(['maxlength' => true, 'disabled' => true])->label('Ficha o MVA') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_modelo')->textInput(['disabled' => true, 'value' => $model_vehiculo->idModelo->idMarca->nombre ])->label('Marca') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_modelo')->textInput(['value' => strtoupper($model_vehiculo->idModelo->nombre), 'disabled' => true])->label('Modelo') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'chasis')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_estacion2, 'nombre')->textInput(['value' => strtoupper($model_estacion2->nombre), 'disabled' => true])->label('ESTACIÓN') ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_solicitud_compra_pieza, 'observacion')->textInput(['value' => ($model_solicitud_compra_pieza->idOrdenTrabajo->idCotizacionTaller->idSolicitudServicioTaller->idTaller->nombre), 'disabled' => true])->label('TALLER DE ENTREGA') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'placa')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>
       
    </div>
    <br>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DEL PROVEEDOR</p>
            <hr>
        </div>
    </div>


    <div class="row">
		
		<div class="col-md-4 col-xs-12">
            <?= $form->field($model_proveedor, 'nombre')->textInput(['maxlength' => true, 'disabled' => true])->label('NOMBRE DEL PROVEEDOR') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_proveedor, 'identificacion')->textInput(['maxlength' => true, 'disabled' => true])->label('RNC') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_proveedor, 'telefono')->textInput(['maxlength' => true, 'disabled' => true])->label('TELÉFONO') ?>
        </div>

        <div class="col-md-8 col-xs-12">
            <?= $form->field($model_proveedor, 'direccion')->textInput(['maxlength' => true, 'disabled' => true])->label('DIRECCIÓN') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_proveedor, 'nombre_representante')->textInput(['maxlength' => true, 'disabled' => true])->label('CONTACTO') ?>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">PIEZAS RECIBIDAS</p>
            <hr>
        </div>
    </div>

    <div class="row">
    	<div class="col-xs-12">
    		<?= GridView::widget([
                'dataProvider' => $dataProvider,
                'summary' => '',
                'id' =>'cotizacion-pieza-grid',
                'tableOptions' => ['class' => 'table table-responsive', 'id' =>'cotizacion-pieza-table'],
                'options' =>[
                    'class' => 'grid-blue',
                ],
                'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                'columns' => [
                   [
                   		'attribute' => 'idCotizacionTallerPieza.idPieza.tipoPieza.nombre',
                        'label' => 'Tipo',
                        'format' => 'text',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:left;'],
                    ],
                    [
                        'attribute' => 'idCotizacionTallerPieza.idPieza.nombre',
                        'label' => 'Descripcion',
                        'format' => 'text',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:left;'],
                    ],
         
                            [
                        'attribute' => 'serial',
                        'label' => 'Serial',
                        'format' => 'raw',
                        'filter' => false,
                        'enableSorting' => false,
                        'value' => function( $model ){
                            return Html::textInput('serial[]', $model->serial,['class' => 'form-control', 'type' => 'text', 'style' => 'text-align:center; font-size: 10pt; float: left;', 'required' => true ]);
                        },
                        'contentOptions'=>['style'=>'text-align: center;'],
                        'headerOptions' => ['width' => '150',],
                    ],
                    [
                        'attribute' => 'cantidad_comprar',
                        'label' => 'Cantidad',
                        'format' => 'text',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:center;'],
                    ],
                    [
                        'attribute' => 'costo',
                        'label' => 'Costo',
                        'format' => 'text',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:right;'],
                        'value' => function( $model ){
                            return number_format(floatval($model->costo), 2, ',', '.').' RD$';
                        },
                    ],
                    [
                    	'attribute' => 'cantidad_recibida',
                        'label' => 'Cantidad recibida',
                        'format' => 'raw',
                        'filter' => false,
                        'enableSorting' => false,
                        'value' => function( $model ){
                            return Html::textInput('recibido[]', $model->cantidad_recibida,['class' => 'form-control', 'type' => 'text', 'style' => 'text-align:center; font-size: 10pt; float: left;', 'required' => true ]).
                            Html::hiddenInput('pieza_id[]', $model->id, ['class' => 'pieza_id', 'readonly' => true ]);;
                        },
                        'contentOptions'=>['style'=>'text-align: center;'],
                        'headerOptions' => ['width' => '150',],
                    ],
                ],
            ]); ?>
    	</div>
    </div>
	<br>
	<br>
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <?= $form->field($model_orden_compra, 'observacion_recepcion')->textarea(['rows' => 3])->label('OBSERVACIONES') ?>
        </div>
    </div>
    </br>
    </br>
    </br>
    </br>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="form-group text-center">
                <?= Html::submitButton('ACEPTAR', ['class' => 'btn btn-submit']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>