<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\select2\Select2;
use app\models\CotizacionProveedorPieza;
use app\models\CotizacionTallerPieza;
use app\models\CotizacionTallerAnalista;
use app\models\SolicitudCompraPieza;
use app\models\AnalistaMantenimiento;
use app\models\User;
use app\models\CotizacionTaller;
use app\models\SolicitudServicioTaller;
use app\models\Taller;
use app\models\Vehiculo;
use app\models\Servicio;
use app\models\Modelo;
use yii\helpers\ArrayHelper;
use app\models\EstadoCompraPieza;

$this->title = 'LISTADO DE SOLICITUDES DE APROBACIÓN DE COMPRA';
?>
<div class="solicitud-aprobacion">
	<div class="row">
        <div class="col-md-1 col-xs-3">
            <i class="fa fa-list-ul fa-3x" aria-hidden="true"></i>
        </div>
        <div class="col-md-10 col-xs-9">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
        </div>
    </div>
    </br>
    </br>

    <div class="row">
    	<div class="col-xs-12">
    		<?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => ['class' => 'table table-responsive'],
                'summary' => '',
                'options' =>[
                    'class' => 'grid',
                ],
                'columns' => [
                    [
                        'label' => '',
                        'format' => 'html',
                        'filter' => false,
                        'enableSorting' => false,
                        'value' => function($model){

                            if ( $model->aprobacion == 5 ) {
                                return '<i class="fa fa-info-circle fa-1x" aria-hidden="true" style="color:#e79e2d;" title="Cotización rechazada"></i>';
                            } else{
                                return '';
                            }
                        }
                    ],
                    [
                        'attribute' => 'id',
                        'label' => 'No. Solicitud de compra',
                        'format' => 'text',
                        'enableSorting' => false,
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'id',
                            'data' => ArrayHelper::map(SolicitudCompraPieza::find()->where([ 'codigo_estado' => 2 ])->all(), 'id', 'id'),
                            'hideSearch' => false,
                            'options' => ['placeholder' => 'Selecciona',],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]),
                    ],
                    [
                        'attribute' => 'id_servicio',
                        'label' => 'No. Solicitud de Servicio',
                        'format' => 'text',
                        'enableSorting' => false,
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'id_servicio',
                            'data' => ArrayHelper::map(SolicitudCompraPieza::find()->where([ 'codigo_estado' => 2 ])->all(), 'id_servicio', 'id_servicio'),
                            'hideSearch' => false,
                            'options' => ['placeholder' => 'Selecciona',],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]),
                    ],
                    [
                        'label' => 'Analista',
                        'format' => 'text',
                        'enableSorting' => false,
                        'value' => function($model){
                            $analista = User::find()->where(['id' => $model->id_analista])->one();
                        	if ($analista) {
                                return $analista->username;
                            } else{
                                return '-';
                            }
                        },
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'id_analista',
                            'data' => ArrayHelper::map(User::find()->joinWith('authAssignments')->where('auth_assignment.item_name = "analista"')->all(), 'id', 'username'),
                            'hideSearch' => false,
                            'options' => ['placeholder' => 'Selecciona',],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]),
                    ],
                    [
                        'label' => 'Taller',
                        'format' => 'text',
                        'enableSorting' => false,
                        'value' => function($model){
                        	$cotizacion = CotizacionTaller::findOne($model->id_cotizacion_taller);
                        	$solicitud_taller = SolicitudServicioTaller::findOne($cotizacion->id_solicitud_servicio_taller);
                        	$taller = Taller::findOne($solicitud_taller->id_taller);
                        	return $taller->nombre;
                        },
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'id_taller',
                            'data' => ArrayHelper::map(Taller::find()->all(), 'id_taller', 'nombre'),
                            'hideSearch' => false,
                            'options' => ['placeholder' => 'Selecciona',],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]),
                    ],
                    [
                        'label' => 'Modelo',
                        'format' => 'text',
                        'enableSorting' => false,
                        'value' => function($model){
                        	$model_servicio = Servicio::findOne($model->id_servicio);
                        	$model_vehiculo = Vehiculo::findOne($model_servicio->id_vehiculo);
                        	return $model_vehiculo->idModelo->nombre;
                        },
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'id_modelo',
                            'data' => ArrayHelper::map(Modelo::find()->all(), 'id_modelo', 'nombre'),
                            'hideSearch' => false,
                            'options' => ['placeholder' => 'Selecciona',],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]),
                    ],
                    [
                        'label' => 'Chasis',
                        'format' => 'raw',
                        'enableSorting' => false,
                        'value' => function($model){
                        	$model_servicio = Servicio::findOne($model->id_servicio);
                        	$model_vehiculo = Vehiculo::findOne($model_servicio->id_vehiculo);
                        	return $model_vehiculo->chasis;
                        },
                        'filter' => Html::input('text', 'SolicitudCompraPiezaSearch[chasis]', $searchModel->chasis, ['class' => 'form-control',]),
                    ],
                    [
                        'attribute' => 'dias',
                        'label' => 'Días en Gestión',
                        'enableSorting' => false,
                        'format'=>'raw',
                    ],                    
                    [
                        'attribute' => 'codigo_estado',
                        'label' => 'Estatus',
                        'format' => 'html',
                        'filter' => false,
                        'enableSorting' => false,
                        'value' => function( $model ){
                            $estado_compra_pieza = EstadoCompraPieza::find()->where([ 'codigo' => $model->codigo_estado ])->one();

                            return '<span class="label label-oval" style="background-color:'.$estado_compra_pieza->color.'; ">'.$estado_compra_pieza->nombre.'</span>';
                        }/*,
                        'filterType'=> GridView::FILTER_SELECT2,
                        'filter'=> ['0' => 'Asignado', '1' => 'En cotización', '2' => 'Aprobación', '3' => 'Por recibir', '4' => 'Asignación ruta', '5' => 'Entregada'],
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                            'name' => 'SolicitudCompraPiezaSearch[id_taller]',
                        ],
                        'filterInputOptions'=>['placeholder'=>'Selecciona'],
                        'format'=>'raw',*/
                    ],                    [  
                        'class' => 'yii\grid\ActionColumn',
                        'header'=>'Acciones',
                        'template' => '{view}',
                        'buttons' => [
                            //view button
                            'view' => function ($url, $model){
                                return ($model->aprobacion != 5)? Html::a('<span><i class="fa fa-eye fa-sm" aria-hidden="true"></i></span>',[ '/solicitud-compra-pieza/gestionaprobacion', 'id' => $model->id]): '';
                            }
                        ],

                   ],

                ],
            ]);  ?>
    	</div>
    </div>
</div>

<script>
	$('.table').removeClass( "table-bordered table-striped" );
</script>