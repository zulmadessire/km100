<?php
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\db\Query;
use app\models\OrdenCompraPieza;
use app\models\CotizacionProveedorPieza;
use app\models\CotizacionTallerPieza;
use app\models\CotizacionTallerAnalista;
use app\models\SolicitudCompraPieza;
use app\models\AnalistaMantenimiento;
use app\models\Vehiculo;
use app\models\Servicio;
use app\models\TipoPieza;
use app\models\Modelo;
use app\models\Pieza;

$this->title = 'LISTADO DE PIEZAS RECIBIDAS';
?>
<div class="piezas-recibidas">
	<div class="row">
        <div class="col-md-1 col-xs-3">
            <i class="fa fa-list-ul fa-3x" aria-hidden="true"></i>
        </div>
        <div class="col-md-10 col-xs-9">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
        </div>
    </div>
        <div class="row">
        <div class="col-md-12">
            <?php
              echo Html::a('Listado de Solicitudes', ['/solicitud-compra-pieza/index'], ['class' => 'btn btn-small-gris', 'style' => 'float:right;']);
            ?>
        </div>
    </div>
    </br>
    </br>

    <div class="row">
    	<div class="col-xs-12">
    		<?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel'   => $searchModel,
                'summary' => '',
                'id' =>'cotizacion-pieza-grid',
                'tableOptions' => ['class' => 'table table-responsive', 'id' =>'cotizacion-pieza-table'],
                'options' =>[
                    'class' => 'grid table-responsive',
                ],
                'columns' => [
                    ['class' => 'kartik\grid\SerialColumn'],
                    [
                        'attribute' => 'id_solicitud',
                        'label' => 'No. Solicitud de compra',
                        'format' => 'text',
                        'enableSorting' => false,
                        'value' =>function ($model){
                            return $model->idCotizacionProveedor->idSolicitudCompraProveedor->idSolicitudCompraPieza->id;
                        },
                        'filter' => Html::input('text', 'id_solicitud', $searchModel->id_solicitud, ['class' => 'form-control',]),
                        
                        'headerOptions' => ['width' => '100',],
                    ],
                    [
                        'attribute' => 'idCotizacionProveedor.ordenCompraPiezas.id',
                        'label' => 'No. Orden de compra',
                        'format' => 'text',
                        'enableSorting' => false,
                        'value' => function($model){
                            return OrdenCompraPieza::find()->where([ 'id_cotizacion_proveedor' => $model->id_cotizacion_proveedor ])->one()->id;
                        },
                        /*'filter' => Select2::widget([
                        'name' => 'CotizacionProveedorPiezaSearch[pieza]',
                        'data' => ArrayHelper::map(Pieza::find()->orderBy('nombre')->all(), 'id_pieza', 'nombre'),
                        'hideSearch' => true,
                        'options' => ['placeholder' => 'Descripción',],
                        'pluginOptions' => [
                              'allowClear' => true
                          ],
                           ]),*/
                        'headerOptions' => ['width' => '100',],
                    ],
                    [
                        'attribute' => 'idCotizacionProveedor.idSolicitudCompraProveedor.idProveedor.nombre',
                        'label' => 'Proveedor',
                        'format' => 'text',
                        'enableSorting' => false,
                        /*'filter' => Select2::widget([
                        'name' => 'CotizacionProveedorPiezaSearch[pieza]',
                        'data' => ArrayHelper::map(Pieza::find()->orderBy('nombre')->all(), 'id_pieza', 'nombre'),
                        'hideSearch' => true,
                        'options' => ['placeholder' => 'Descripción',],
                        'pluginOptions' => [
                              'allowClear' => true
                          ],
                           ]),*/
                        'headerOptions' => ['width' => '200',],
                    ],
                    [
                        'attribute' => 'idCotizacionTallerPieza.idPieza.tipoPieza.id',
                        'label' => 'Tipo',
                        'format' => 'raw',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:left;'],
                        
                        'filterType'=> GridView::FILTER_SELECT2,
                        'filter'=> ArrayHelper::map(TipoPieza::find()->orderBy('nombre')->all(), 'id', 'nombre'),

                        'value' => function( $model ){
                            return  $model->idCotizacionTallerPieza->idPieza->tipoPieza->nombre;

                        },



                        /*'filter' => Select2::widget([
                        'name' => 'CotizacionProveedorPiezaSearch[tipo_pieza]',
                        'data' => ArrayHelper::map(TipoPieza::find()->orderBy('nombre')->all(), 'id', 'nombre'),
                        'hideSearch' => true,
                        'options' => ['placeholder' => 'Tipo',],
                        'pluginOptions' => [
                              'allowClear' => true
                          ],
                           ]),*/
                    ],
                    [
                         
                        'label' => 'Descripción',
                        'format' => 'text',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:left;'],
                         'value' => function( $model ){
                            return  $model->idCotizacionTallerPieza->idPieza->nombre;

                         },
                        'filter' => Select2::widget([
                        'name' => 'CotizacionProveedorPiezaSearch[pieza]',
                        'data' => ArrayHelper::map(Pieza::find()->orderBy('nombre')->all(), 'id_pieza', 'nombre'),
                        'hideSearch' => true,
                        'options' => ['placeholder' => 'Descripción',],
                        'pluginOptions' => [
                              'allowClear' => true
                          ],
                           ]),
                    ],
                    [
                        'attribute' => 'serial',
                        'label' => 'Serial',
                        'format' => 'text',
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:left;'],
                        //'filter' => Html::input('text', 'CotizacionProveedorPiezaSearch[serial]', $searchModel->serial, ['class' => 'form-control',]),
                        'headerOptions' => ['width' => '100',],
                    ],
                    [
                        'attribute' => 'cantidad_recibida',
                        'label' => 'Cantidad Recibida',
                        'format' => 'text',
                        'enableSorting' => false,
                        'headerOptions' => ['width' => '80',],
                    ],
                   
                   
                ],
            ]); ?>
    	</div>
    </div>
</div>

<script>
	$('.table').removeClass( "table-bordered table-striped" );
</script>