<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\form\ActiveForm;
use kartik\grid\GridView;
use kartik\select2\Select2;
use app\models\CotizacionTallerPiezaSearch;
use app\models\CotizacionProveedorPiezaSearch;
use app\models\PipelineCompraPieza;
use app\models\EstadoCompraPieza;
use app\models\SolicitudCompraPieza;
use app\models\Taller;
use app\models\SolicitudPiezaEstacionSearch;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SolicitudCompraPiezaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'LISTADO DE SOLICITUDES DE COMPRA';
?>
<div class="solicitud-compra-pieza-index">

    <div class="row">
        <div class="col-md-1 col-xs-3">
            <i class="fa fa-list-ul fa-3x" aria-hidden="true"></i>
        </div>
        <div class="col-md-10 col-xs-9">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
        </div>
    </div>
    </br>
    </br>
    <div class="row">    
        <div class="col-xs-12">
            <?php
                if(\Yii::$app->user->can('analista-compras')) {
                echo "
                <p> <span style= 'background-color: #c58b8b; width: 30px; height: 15px; display: inline-block;'></span> Cotizaciones rechazadas </p>";
                }
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => ['class' => 'table table-responsive',],
                'summary' => '',
                'options' =>[
                    'class' => 'grid',
                ],
                'id' => 'solicitud-compra-pieza-grid',
                'resizableColumns' => false,
                //'pjax' => true,
                'columns' => [
                    [
                        'class'=>'kartik\grid\ExpandRowColumn',
                        'width'=>'50px',
                        'allowBatchToggle' => false,
                        'expandIcon' => '<i class="fa fa-caret-right" aria-hidden="true" style="color:#d9d9d9;"></i>',
                        'collapseIcon' => '<i class="fa fa-caret-down" aria-hidden="true" style="color:#d9d9d9;"></i>',
                        'value'=>function ($model, $key, $index, $column) {
                            return GridView::ROW_COLLAPSED;
                        },
                        'detail'=>function ($model, $key, $index, $column){
                            $pipeline_compra_pieza = PipelineCompraPieza::find()->where([ 'id_solicitud_compra_pieza' => $model->id ])->orderby('id DESC')->all();

                            if ($pipeline_compra_pieza[0]->idEstadoCompraPieza->codigo < 3 ) {
                                if($model->origen_solicitud ==  2) {
                                    $searchModel = new SolicitudPiezaEstacionSearch(['id_solicitud_compra_estacion' => $model->id_solicitud_compra_estacion]);
                                    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                                    $cantidad_piezas = 0;
                                }else {
                                    $searchModel = new CotizacionTallerPiezaSearch(['id_cotizacion_taller' => $model->id_cotizacion_taller]);
                                    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                                    $cantidad_piezas = 0;
                                }
                            } else{
                                $searchModel = new CotizacionProveedorPiezaSearch();
                                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                                $dataProvider->query->joinWith('idCotizacionProveedor')->joinWith('idCotizacionProveedor.idSolicitudCompraProveedor')->where('solicitud_compra_proveedor.id_solicitud_compra_pieza = '. $model->id .' AND aprobacion = 2');
                                $cantidad_piezas = $dataProvider->getTotalCount();
                            }

                            return Yii::$app->controller->renderPartial('_expand-solicitud-compra-pieza', ['pipeline_compra_pieza' => $pipeline_compra_pieza, 'dataProvider' => $dataProvider, 'model' => $model, 'cantidad_piezas' => $cantidad_piezas]);
                        },
                        'headerOptions'=>['class'=>'kartik-sheet-style'],
                        'expandOneOnly'=>true,
                        'enableRowClick' => true,
                        'filter' => false,
                    ],
                    [
                        'attribute' => 'id',
                        'label' => 'No. Solicitud de compra',
                        'enableSorting' => false,
                        'filterType'=> GridView::FILTER_SELECT2,
                        'filter'=> ArrayHelper::map(SolicitudCompraPieza::find()->orderBy('id')->all(), 'id', 'id'),
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                        ],
                        'filterInputOptions'=>['placeholder'=>'Selecciona'],
                        'format'=>'raw',
                        'contentOptions' => function ($model, $key, $index, $column) {
                            return  ['style' => 'background-color:' 
                                .       ($model->aprobacion == 5 ? '#c58b8b' : '')
                                    ];
                        },
                    ],
                    [
                        'attribute' => 'id_servicio',
                        'label' => 'Núm. Solicitud',
                        'enableSorting' => false,
                        'filterType'=> GridView::FILTER_SELECT2,
                        'filter'=> ArrayHelper::map(SolicitudCompraPieza::find()->orderBy('id_servicio')->all(), 'id_servicio', 'id_servicio'),
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                        ],
                        'filterInputOptions'=>['placeholder'=>'Selecciona'],
                        'format'=>'raw'
                    ],
                    [
                        'attribute' => 'idOrdenTrabajo.idCotizacionTaller.idSolicitudServicioTaller.idTaller.id_taller',
                        'label' => 'Taller',
                        'enableSorting' => false,
                        'format'=>'raw',
                        'filter' => Select2::widget([
                            'name' => 'SolicitudCompraPiezaSearch[id_taller]',
                            'data' => ArrayHelper::map(Taller::find()->orderBy('id_taller')->all(), 'id_taller', 'nombre'),
                            'options' => ['placeholder' => 'Selecciona',],
                            'pluginOptions' => [
                                'allowClear' => true
                            ]
                        ]),
                    ],
                    [
                        'attribute' => 'dias',
                        'label' => 'Días en Gestión',
                        'enableSorting' => false,
                        'format'=>'raw',
                    ],
                    [
                        'attribute' => 'codigo_estado',
                        'label' => 'Estatus',
                        'format' => 'html',
                        'filter' => false,
                        'enableSorting' => false,
                        'value' => function( $model ){
                            $estado_compra_pieza = EstadoCompraPieza::find()->where([ 'codigo' => $model->codigo_estado ])->one();

                            return '<span class="label label-oval" style="background-color:'.$estado_compra_pieza->color.'; ">'.$estado_compra_pieza->nombre.'</span>';
                        },
                        'filterType'=> GridView::FILTER_SELECT2,
                        'filter'=> ['0' => 'Asignado', '1' => 'En cotización', '2' => 'Aprobación', '3' => 'Por recibir', '4' => 'Asignación ruta', '5' => 'Entregada'],
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                        ],
                        'filterInputOptions'=>['placeholder'=>'Selecciona'],
                        'format'=>'raw',
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
<?php if(\Yii::$app->user->can('analista-compras')): ?>
<!-- Modal Solciitar Cotizacion a Proveedores -->
<div id="solicitarCotizacionModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="title"><img src="<?= Yii::$app->request->baseUrl ?>/img/give_money_64.png" alt="" style="height: 40px;">&nbsp;&nbsp; SOLICITUD DE COTIZACIÓN DE PIEZA</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true" style="color: #000; font-weight: bold;"></i></button>
          </div>
        <div class="modal-body">
            </br>
            <?php $form = ActiveForm::begin([ 'id' => 'solicitar', 'action' => ['/solicitud-compra-pieza/solicitudcotizacion',] ]); ?>
                <div class="row">
                    <div class="col-xs-12 col-md-11 col-md-offset-1">
                        <label class="control-label" for="garantiavehiculo-motivo">PROVEEDORES</label>
                        <div id="response-proveedor" class="checkbox">
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-11 col-md-offset-1">                    
                        <br>
                        <label class="control-label" for="garantiavehiculo-motivo">PROVEEDORES RECOMENDADOS</label>
                        <div>
                            <?php foreach($mejor_proveedor as $pro): ?>
                                <span class="label label-oval" style="background-color:#70b93e; "><?= $pro->nombre ?></span>    
                            <?php endforeach ?>
                        </div>
                    </div>
                </div>

            <?php ActiveForm::end(); ?>
            </br>
            <?php
            echo '<div class="row" style="padding-top: 25px;">';
                echo '<div class="text-center">';
                    echo Html::button('CANCELAR', ['class' => 'btn btn-cancelar', 'data' => ['dismiss' => 'modal']]);
                    echo "&nbsp;";
                    echo Html::submitButton('CONTINUAR', ['class' => 'btn btn-aceptar', 'form' => 'solicitar', 'id' => 'submit-solicitar' ]);
                echo '</div>';
            echo '</div>';
            ?>
        </div>
    </div>
  </div>
</div>
<!-- Fin modal-->
<?php endif ?>
<!-- Modal Checklist de Recepcion -->
<div id="recepcionModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="title"><img src="<?= Yii::$app->request->baseUrl ?>/img/bubble_info_64.png" alt="" style="height: 40px;">&nbsp;&nbsp; INFORMACIÓN IMPORTANTE</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true" style="color: #000; font-weight: bold;"></i></button>
          </div>
        <div class="modal-body">
            </br>
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1">
                    <p><b>Seguro que desea finalizar la recepción de piezas?</b></p>
                </div>
            </div>
            </br>
            <?php $form = ActiveForm::begin([ 'id' => 'recepcion-form', 'action' => ['/solicitud-compra-pieza/finalizarrecepcion',] ]); ?>
                <div class="row">
                    <div class="col-xs-12 col-md-10 col-md-offset-1">
                        <?= Html::label('OBSERVACIONES', 'observacion_recepcion', ['class' => 'control-label']) ?>
                        <?= Html::textArea('observacion_recepcion', '',['class' => 'form-control', 'rows' => 3]) ?>
                    </div>
                </div>
            </br>
            <?php ActiveForm::end(); ?>
            <?php
            echo '<div class="row" style="padding-top: 25px;">';
                echo '<div class="text-center">';
                    echo Html::button('CANCELAR', ['class' => 'btn btn-cancelar', 'data' => ['dismiss' => 'modal']]);
                    echo "&nbsp;";
                    echo Html::submitButton('CONTINUAR', ['class' => 'btn btn-aceptar', 'form' => 'recepcion-form', 'id' => 'submit-recepcion',]);
                echo '</div>';
            echo '</div>';
            ?>
        </div>
    </div>
  </div>
</div>
<!-- Fin modal-->
<style>
    .modal-dialog {
        margin: 80px auto;
    }
</style>
<script>
    $('#solicitarCotizacionModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var id = button.data('id');
        $('#submit-solicitar').data('id', id);
        $.ajax({
            url: "../solicitud-compra-pieza/getproveedores",
            type: 'post',
            data: {
                id: id,
            },
            success: function (response) {
                $('#response-proveedor').empty();
                for (var i = 0; i < response.length; i++) {
                    $('#response-proveedor').append(
                        '<div class="col-md-6 col-xs-12">'+
                            '<label class="checkbox-inline">'+
                                '<input type="checkbox" value="'+response[i]['id']+'" name="proveedor[]" checked="true"> '+response[i]['nombre']+
                            '</label>'+
                        '</div>'
                    );
                }
            }
        });
    });

    $('#solicitar').on('submit', function(event) {
        event.preventDefault();
        event.stopImmediatePropagation();

        $('#submit-solicitar').attr({
            disabled: 'true'
        });

        $('#solicitarCotizacionModal').modal('hide');
        var id = $('#submit-solicitar').data('id');

        var form = $(this);
        var data = form.serializeArray();      
        data.push({name: 'id', value: id});

        $.post( form.attr("action"), data ).done(function(response){
            location.reload();
        });
    });

    $('#recepcionModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var id = button.data('id');
        $('#submit-recepcion').data('id', id);
    });

    $('#recepcion-form').on('submit', function(event) {
        event.preventDefault();
        event.stopImmediatePropagation();

        $('#submit-recepcion').attr({
            disabled: 'true'
        });

        $('#recepcionModal').modal('hide');
        var id = $('#submit-recepcion').data('id');

        var form = $(this);
        var data = form.serializeArray();      
        data.push({name: 'id', value: id});

        $.post( form.attr("action"), data ).done(function(response){
            location.reload();
        });
    });

    $('.table').removeClass( "table-bordered table-striped" );
    //$('.kv-expand-header-cell').removeAttr('rowspan');
</script>
