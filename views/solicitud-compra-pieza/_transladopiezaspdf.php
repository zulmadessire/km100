<?php 

use yii\helpers\Html;

$subtotal = 0;
?>
<div class="content">
	<div class="row">
        <div class="col-xs-12">  
            <div style="width: 70%; float: left;">            
                <img src="/km100/img/logo.png"  width="70" style="float: left;">
                <div class="col-xs-6" style="font-size: 9pt;">
                    <b>Inversiones Carrozza, S.R.L.</b><br>
                    Av. Republica de Colombia esq.<br>
                    Av. Monumental<br>
                    Tel.: 809-331-6009<br>
                    RNC: 130512663<br>
                </div>
            </div> 
            <div style="width: 30%; float: left;">  
                <b>SALIDA DE PARTES</b>
            </div>
        </div>
	</div>
	<br>
    <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-5 col-xs-offset-2">
                <span style="font-size: 8pt;"><b>Cliente:</b> <?= $model->idOrdenTrabajo->idCotizacionTaller->idSolicitudServicioTaller->idServicio->idEstacionActual->idEmpresa->cliente->nombre_comercial ?> </span>
            </div>
            <div class="col-xs-3">
                <span style="font-size: 8pt;"><b>Fecha:</b> ____/____/______ </span>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
		<div class=" col-xs-12" style="border: 1px solid black; border-radius: 3px; padding: 5px; padding-left: 10px;">
			<div style="width: 45%; float: left; padding: 5px; border-right: 1px solid black;">
                <div style="width: 100%; padding: 3px;">
                    <span style="text-aling: center; font-size: 9.5pt;">SERVICIOS</span>
				</div>
                <div style="width: 40%; float: left; padding: 5px; border-right: 1px solid black;">
                    <span style="font-size: 8pt;"><b>Rsa:</b> ____ <br></span>
                    <span style="font-size: 8pt;"><b>Remarketing:</b> ______ <br></span>
                    <span style="font-size: 8pt;"><b>No. Expediente:</b> ______ <br></span>
                </div>
                <div style="width: 50%; float: left; padding: 5px 5px 5px 10px; ">
                    <span style="font-size: 8pt;"><b>Forma de envio:</b>  <br></span>
                    <span style="font-size: 8pt;"><b>Enviado con:</b> __________ <br></span>
                    <span style="font-size: 8pt;"><b>Mensajeria:</b> ____ <br></span>
                    <span style="font-size: 8pt;"><b>Recolección:</b> ____ <br></span>
                    <span style="font-size: 8pt;"><b>Transporte:</b> ____ <br></span>
                </div>
                <br>
                <span style="font-size: 8pt;"><b>Orden de servicio:</b> <?= $model->idOrdenTrabajo->id ?><br></span>
                <span style="font-size: 8pt;"><b>Solicitud orden de compra:</b> <?= $model_orden_compra->id;?> <br></span>
                <span style="font-size: 8pt;"><b>Taller:</b> <?= $model->idOrdenTrabajo->idCotizacionTaller->idSolicitudServicioTaller->idTaller->nombre; ?> <br></span>
			</div>
			<div style="width: 45%; float: left; padding: 5px;">
				<div style="width: 100%; padding: 3px;">
                    <span style="text-aling: center; font-size: 9.5pt;">DETALLES SOBRE CAUSA SERVICIO</span>
				</div>
				<div style="width: 100%; padding: 5px;">
					<span style="font-size: 8pt;"><b>Marca:</b> <?= $model_vehiculo->idModelo->idMarca->nombre ?> </span><br>
					<span style="font-size: 8pt;"><b>Estación:</b> <?= $model->idOrdenTrabajo->idCotizacionTaller->idSolicitudServicioTaller->idServicio->idEstacionActual->nombre ?></span><br>
					<!-- <span style="font-size: 8pt;"><b>Contrato:</b> No. </span><br> -->
					<span style="font-size: 8pt;"><b>Ficha/Mva:</b> <?= $model_vehiculo->mva ?> </span><br>
					<span style="font-size: 8pt;"><b>Placa:</b> <?= $model_vehiculo->placa ?> </span><br>
					<span style="font-size: 8pt;"><b>Modelo:</b> <?= $model_vehiculo->idModelo->nombre ?> </span><br>
					<span style="font-size: 8pt;"><b>Color:</b> <?= $model_vehiculo->color ?> </span><br>
					<span style="font-size: 8pt;"><b>Año:</b> <?= $model_vehiculo->anho ?> </span><br>
					<span style="font-size: 8pt;"><b>Chasis:</b> <?= $model_vehiculo->chasis ?> </span><br>
				</div>
			</div>
		</div>
	</div>
	<br><br>
	<div class="row">
		<div class="col-xs-11">
			<table class="table">
				<thead class="text-center">
					<tr>
						<th style="text-align: center; font-size: 8pt;">ITEM NO.</th>
						<th style="text-align: center; font-size: 8pt;">DESCRIPCIÓN DE PARTES</th>
						<th style="text-align: center; font-size: 8pt;">TIPO PIEZA</th>
					</tr>
				</thead>
				<tbody>
                    <?php //foreach ($piezas as $i => $pieza) 
                        foreach ($dataProvider->getModels() as $key => $pieza){ 
                    ?>
						<tr>
							<td style="text-align: center; font-size: 7pt;"><?= $pieza->cantidad_enviada_taller ?></td>
							<td style="text-align: left; font-size: 7pt;"><?= $pieza->idCotizacionProveedorPieza->idCotizacionTallerPieza->idPieza->nombre; ?></td>
							<td style="text-align: center; font-size: 7pt;"><?= $pieza->idCotizacionProveedorPieza->idCotizacionTallerPieza->idPieza->tipoPieza->nombre; ?></td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
    <div class="row">
		<div class=" col-xs-12" style="border: 1px solid black; border-radius: 3px; padding: 5px; padding-left: 10px;">
			<div style="width: 65%; float: left; padding: 5px;">
                <div style="width: 100%; padding: 3px;">
                    <span style="text-aling: center; font-size: 9.5pt;">USO EXCLUSIVO RSA O QUIEN REMITE</span>
				</div>
			</div>
			<div style="width: 25%; float: left; padding: 5px;  border-left: 1px solid black;">
				<div style="width: 40%; float: left; padding: 5px; text-align: right;">
					<span style="font-size: 8pt;"><b>Completo:</b> </span><br>
					<span style="font-size: 8pt;"><b>Cerrado:</b> </span><br>
					<span style="font-size: 8pt;"><b>Re-abierto:</b></span><br>
					<span style="font-size: 8pt;"><b>Por definir:</b> </span><br>
				</div>
                <div style="width: 40%; float: left; padding: 5px;">
                <span style="font-size: 8pt;">______</span><br>
                <span style="font-size: 8pt;">______</span><br>
                <span style="font-size: 8pt;">______</span><br>
                <span style="font-size: 8pt;">______</span><br>
                </div>
			</div>
		</div>
        <span style="font-size: 7pt;"><b>Por la presente recibo conforme las partes como bueno y válido</b></span><br>
        <span style="font-size: 7pt; text-align: justify; text-justify: inter-word;">
            Cualquier otra causa fortuita y de fuerza mayor. Reconozco que INVERSIONES FORTEZA tiene la facultad de contactarme para validación de este documento. Reconozco que al momento de la entrega de las partes he revisado completamente y se me hace entrega de lo solicitado, objeto o bien. Luego de 24 horas de habérseme entregado la parte y no recibir ninguna notificado por vía escrita, telefónica o personal, correo electrónico o NO SE ACEPTA devolución de las partes y queda bajo responsabilidad del que recibe.
        </span>
	</div>
	<br><br>
	<div class="row">
		<div class="col-xs-12">
			<div style="text-align: center; font-size: 8pt;">
				<div class="col-xs-3 col-xs-offset-1">
					<span style="text-align: center;"><b>___________________________<br>
					PIEZAS ENTREGADAS POR</b></span>
				</div>
				<div class="col-xs-3">
					<span style="text-align: center;"><b>___________________________<br>
					PIEZAS RECIBIDAS POR</b></span>
				</div>
				<div class="col-xs-3">
					<b>___________________________<br>
					<span style="text-align: center;">EN FECHA</b></span>
				</div>
			</div>
		</div>
	</div>
	<br><br>
	<div class="row">
		<div class="col-xs-12">
			<div style="text-align: center; font-size: 8pt;">
				<div class="col-xs-3 col-xs-offset-1">
					<span style="text-align: center;"><b>___________________________<br>
					CEDULA No.</b></span>
				</div>
				<div class="col-xs-3">
					<span style="text-align: center;"><b>___________________________<br>
					CEDULA No.</b></span>
				</div>
				<div class="col-xs-3">
					<b>___________________________<br>
					<span style="text-align: center;">HORA</b></span>
				</div>
			</div>
		</div>
	</div>
</div>