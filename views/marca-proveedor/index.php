<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MarcaProveedorsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Marca Proveedors';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marca-proveedor-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Marca Proveedor', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'id_proveedor',
            'id_marca',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
