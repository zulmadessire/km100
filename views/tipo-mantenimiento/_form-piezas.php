<?php

use yii\helpers\Html;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Pieza;
use yii\helpers\ArrayHelper;

?>

<?php DynamicFormWidget::begin([
    'widgetContainer' => 'dynamicform_inner',
    'widgetBody' => '.container-rooms',
    'widgetItem' => '.room-item',
    'limit' => 20,
    'min' => 1,
    'insertButton' => '.add-room',
    'deleteButton' => '.remove-room',
    'model' => $modelpiezas[0],
    'formId' => 'dynamic_lista-form',
    'formFields' => [
        'id_pieza'
    ],
]); ?>
<table class="table table-bordered" cellspacing="0" cellpadding="0" border="0">
    <tbody class="container-rooms">
    <?php foreach ($modelpiezas as $indexPieza => $modelPieza): ?>
        <tr class="room-item">
            <td class="vcenter">
                   <?php 
                        // necessary for update action.
                        if (! $modelPieza->isNewRecord) {
                      
                            // echo Html::activeHiddenInput($modelPieza, "[{$i}]id_activ_pieza");
                  
                             
                        }
                       
                 ?>
                <?php

                    echo $form->field($modelPieza, "[{$i}][{$indexPieza}]id_pieza")
  ->DropDownList( ArrayHelper::map(
    // \app\models\Pieza::find()->where(['tipo_pieza_id'=>1])->orderBy('nombre')->all(),
    \app\models\Pieza::find()->all(),
    'id_pieza',
    function($modelPieza, $defaultValue) {
       $mod = \app\models\Modelo::findOne(['id_modelo'=> $modelPieza->modelo_id_modelo]);
       if ($mod) {
           $models = $modelPieza['codigo'].' - '.$modelPieza['nombre'].' - '.$mod['nombre'].' - '.$modelPieza['anho'];
       }else{
        $models = $modelPieza['codigo'].' - '.$modelPieza['nombre'];
       }
        return $models;
    }
  ),
  [ 'prompt' => 'Seleccione la pieza' ])->label(false); 

                ?>

            </td>

            <?php /*if($indexPieza==1){ echo('SIIII'); ?>
            <?php }else{  echo('NOOOOO');?>
            <?php } */?>

            <td class="text-center vcenter" style="width: 40px;">
                <button type="button" class="remove-room btn btn-cancelar btn-xs"><span class="glyphicon glyphicon-minus"></span></button>
            </td>
        </tr>
     <?php endforeach; ?>
    </tbody>
</table>
<div align="right">
    <button type="button" class="add-room btn btn-accion"><span class="glyphicon glyphicon-plus"></span></button>
</div>
<?php DynamicFormWidget::end(); ?>
