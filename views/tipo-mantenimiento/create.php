<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TipoMantenimiento */

$this->title = 'REGISTRO DE MANTENIMIENTO';
//$this->params['breadcrumbs'][] = ['label' => 'Tipo Mantenimientos', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipo-mantenimiento-create">

    <div class="row">
      <div class="col-md-1 col-xs-3">
        <h4 class="titulo"><i class="fa fa-wrench fa-4x" aria-hidden="true"></i></h4>
      </div>
      <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
        <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
      </div>
    </div>
    <br>
    <?= $this->render('_form', [
        'model' => $model,
        'modelact' => $modelact,
        'modelpiezas' => $modelpiezas,
  
    ]) ?>

</div>
