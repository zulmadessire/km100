<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\ActividadMnto;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TipoMantenimientoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Listado de Mantenimientos';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipo-mantenimiento-index">

  <div class="row">
    <div class="col-md-1 col-xs-3">
      <h4 class="titulo"><i class="fa fa-wrench fa-4x" aria-hidden="true"></i></h4>
    </div>
    <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
      <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
    </div>
  </div>
    <div class="row">
        <div class="col-xs-12">
            <?php
                echo Html::a('Registrar Mantenimiento', ['create'], ['class' => 'btn btn-small-gris', 'style' => 'float:right; margin-top:20px;']);
                echo "<br><br>";
            ?>
        </div>
    </div>
    <br>
    <di class="row">
        <div class="col-md-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                       'tableOptions' => ['class' => 'table table-responsive'],
                       'summary' => '',
                            'options' =>[
                                'class' => 'grid',
                            ],
                'columns' => [
                   
                    [
                        'attribute' => 'nombre',
                        'label' => 'Nombre de Mantenimiento',
                        'value' => 'nombre',
                        'format' => 'text',
                        'filter' => true,
                        'enableSorting' => false,
                    ],
                    [
                        'label' => 'Actividades',
                        'format' => 'html',
                        'value' => function ($model) {
                                    $actividades = ActividadMnto::find()->where(['id_tipo_mnto' => $model->id_tipo_mnto])->orderBy('nombre')->All();
                                    $activs = '';
                                    foreach ($actividades as $actividad) {
                                        $activs.=$actividad->nombre.'<br>';
                                    }
                                    return $activs;
                        },
                         'filter' => Select2::widget([
                            'name' => 'TipoMantenimientoSearch[id_actividad_mnto]',
                            // 'data' => ArrayHelper::map(Marca::find()->joinWith('Modelo')->orderBy('nombre')->all(), 'id_marca', 'nombre'),
                            'data' => ArrayHelper::map(ActividadMnto::find()->orderBy('nombre')->all(), 'id_actividad_mnto', 'nombre'),
                            'hideSearch' => true,
                            'options' => ['placeholder' => 'Actividades',],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                             ]),
                          'enableSorting' => false,
                    ],


                       [
                        'class' => 'yii\grid\ActionColumn',
                        'header'=>'',
                        'template' => '{menu}',
                        'buttons' => [
                            'menu' => function ($url, $model) {
                                 return  '<div class="dropdown">
                                          <button class="btn btn-default dropdown-toggle btn-sm" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i>
                                          <span class="caret"></span></button>
                                          <ul class="dropdown-menu">
                                             <li>'.Html::a('<span><i class="fa fa-edit fa-sm" aria-hidden="true"></i></span> Modificar Mantenimiento',[ 'tipo-mantenimiento/update', 'id' => $model->id_tipo_mnto ]).' </li>
                                          </ul>
                                        </div>';


                            },
                        ],
                   ],

                    //['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </di>
</div>
