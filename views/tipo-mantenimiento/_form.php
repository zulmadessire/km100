<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use yii\web\view;
use app\models\ActividadPieza;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\TipoMantenimiento */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tipo-mantenimiento-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic_lista-form',]); ?>


    <div class="row" style="border-bottom: 1px solid #222d32;">
        <div class="col-xs-12 col-md-6">
            <h4>DETALLE DE MANTENIMIENTO</h4>
        </div>
        <div class="col-xs-12 col-md-6">
          <div align="right">
                <?php
                echo Html::a('Listado de Mantenimientos', ['tipo-mantenimiento/index/'], ['class' => 'btn btn-small-gris']);
                ?>
          </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12 col-md-4">
            <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <?php DynamicFormWidget::begin([
        'widgetContainer' => 'dynamicform_wrapper_resultados',
        'widgetBody' => '.container-items',
        'widgetItem' => '.item',
        'limit' => 50,
        'min' => 1,
        'insertButton' => '.add-item',
        'deleteButton' => '.remove-item',
        'model' => $modelact[0],
        'formId' => 'dynamic_lista-form',
        'formFields' => [
            'nombre',
            'tipo_actividad',
        ],
    ]); ?>
    <table class="table table-hover">
        <tbody class="container-items">
        <?php foreach ($modelact as $i => $modelact): ?>
            <tr class="item panel panel-default">
                 <td class="vcenter">
                <?php 
                        // necessary for update action.
                        if (! $modelact->isNewRecord) {
                           
                            // echo Html::activeHiddenInput($modelact, "[{$i}]id_actividad_mnto");
                           
                             
                        }
              
                 ?>
               
                    <div class="col-xs-12 col-md-3"><?php
                        $tipos_mnto = [0 => 'Cambio', 1 => 'Chequeo'];
                        echo $form->field($modelact, "[{$i}]tipo_actividad")->dropDownList(
                                $tipos_mnto,
                                [
                                    'prompt'=>'Selecciona el tipo',
                                    'class' => 'form-control',
                                ] ); ?>
                        </div>
                    <div class="col-xs-12 col-md-4"><?= $form->field($modelact, "[{$i}]nombre")->textInput(); ?></div>

                    <div class="col-xs-12 col-md-4"><?php echo '<label class="control-label">Piezas</label>'; ?>
                        <?= $this->render('_form-piezas', [
                            'form' => $form,
                            'i' => $i,
                            'modelpiezas' => $modelpiezas[$i],
                        ]) ?>
                    </div>
                    <div class="col-xs-12 col-md-1 text-center">
                        <button type="button" class="remove-item btn btn-cancelar btn-xs" style="margin-top:28px;">
                            <i class="glyphicon glyphicon-minus"></i></button>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
    <?php DynamicFormWidget::end(); ?>
        </tbody>
    </table>

    <button type="button" class="add-item btn btn-accion  btn-xs pull-right">Agregar Actividad</button>
    <br><br>







       <?php
        if(!$model->isNewRecord){
        ?>
            <div class="row" align="center">
                <div>
                    <?= Html::a('CANCELAR', ['index'], ['class' => 'btn btn-cancelar']) ?>
                    &nbsp;&nbsp;&nbsp;
                    <?= Html::submitButton('GUARDAR CAMBIOS', ['class' => 'btn btn-primario']) ?>
                </div>
            </div>
        <?php
        }else{
        ?>
            <div class="form-group" align="center">
                <?= Html::submitButton('GUARDAR', ['class' => 'btn btn-primario']) ?>
            </div>
        <?php
        }
    ?>

    <?php ActiveForm::end(); ?>

</div>







