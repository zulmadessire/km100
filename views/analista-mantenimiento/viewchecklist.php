<?php 
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\CotizacionTallerActividad;
use app\models\analistamantenimiento;
use app\models\TipoPieza;
use yii\helpers\Url;
use app\models\ChecklistAvaluo;
use app\models\ChecklistAvaluoPieza;
use app\models\Pieza;
use app\models\Empresa;
use wbraganca\dynamicform\DynamicFormWidget;



/* @var $this yii\web\View */
/* @var $model app\models\Taller */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'VER AVALÚO';
?>

<div class="analista-form">
	<div class="row">
		<div class="col-md-1 col-xs-3">
			<h4 class="titulo">
				<i class="fa fa-list fa-4x" aria-hidden="true"></i>
			</h4>
		</div>
		<div class="col-md-10 col-xs-9" style="padding-top: 20px;">
			<h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
		</div>
	</div>

	<?php $form = ActiveForm::begin(['id' => 'dynamic_lista-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>

	<!-- --DATOS DEL TALLER-- -->

	<div class="row" style="border-bottom: 1px solid #222d32;">
		<div class="col-xs-12 col-md-6">
			<h4>DATOS DEL VEHÍCULO</h4>
		</div>

	</div>
	<br>
	<br>
	<div class="row">
		<div class="col-md-4 col-xs-12">
			<?= $form->field($n_sol, 'id')->textInput(['maxlength' => true, 'disabled' => true]) ?>
		</div>
		<div class="col-md-4 col-xs-12">
			<?= $form->field($model_vehiculo, 'mva')->textInput(['maxlength' => true, 'disabled' => true])->label('Ficha o MVA') ?>
		</div>
		<div class="col-md-4 col-xs-12">
			<?= $form->field($model_vehiculo, 'id_modelo')->textInput(['disabled' => true, 'value' => $model_vehiculo->idModelo->idMarca->nombre ])->label('Marca') ?>
		</div>

		<div class="col-md-4 col-xs-12">
			<?= $form->field($model_vehiculo, 'id_modelo')->textInput(['value' => strtoupper($model_vehiculo->idModelo->nombre), 'disabled' => true])->label('Modelo') ?>
		</div>



		<div class="col-md-4 col-xs-12">
			<?= $form->field($model_vehiculo, 'placa')->textInput(['maxlength' => true, 'disabled' => true]) ?>
		</div>

		<div class="col-md-4 col-xs-12">
			<?= $form->field($model_vehiculo, 'anho')->textInput(['maxlength' => true, 'disabled' => true]) ?>
		</div>
	</div>
	<br>
	<div class="row" style="border-bottom: 1px solid #222d32;">
		<div class="col-xs-12 col-md-6">
			<h4>INFORMACIÓN DE DAÑOS</h4>
		</div>

	</div>

	<div class="row">
		<div class="col-md-4">
			<br><br>
			<?php echo Html::img(Yii::$app->request->BaseUrl.'/img/autopiezas.png') ?>
		</div>
		<div class="col-md-8">


			<br><br>
			<?php 

			$chec = explode('*',$clavaluo->danos);
			$v[]=0;
			for ($i=1; $i < 20; $i++) {
				foreach ($chec as $value) {
					if ($i==$value) {
						$v[$i] = 1;
						break;
					}
					else{
						$v[$i] = 0;
					}
				} 

			}


			?>
			<?php 
			for ($j=1; $j < 20; $j++) {  ?>

			<?php if ($j==6): ?>
			<br>
		<?php endif ?>
		<?php if ($j==11): ?>
		<br>
	<?php endif ?>
	<?php if ($j==16): ?>
	<br>
<?php endif ?>
<?php if ($v[$j]==0): ?>
	<label class="col-md-2 col-xs-12 checkbox-inline">
		<input type="checkbox" id="inlineCheckbox2" value="2" disabled> <?php echo $j; ?>
	</label>

<?php endif ?>
<?php if ($v[$j]==1): ?>
	<label class="col-xs-2 col-xs-12 checkbox-inline">

		<input type="checkbox" id="inlineCheckbox1" checked disabled > <?php echo $j; ?>
	</label> 
<?php endif ?>

<?php }
?>


</div>

</div>


<div class="row" style="border-bottom: 1px solid #222d32;">
	<div class="col-xs-12 col-md-6">
		<h4>PIEZAS AFECTADAS</h4>
	</div>

</div>
<br><br>
	<div class="row">
		<div class="col-xs-12">
			<?= GridView::widget([
                   	'dataProvider' => $dataProvider,
	                'summary' => '',
	                'id' =>'cotizacion-actividad-grid',
	                'pjax' => true,
	                'tableOptions' => ['class' => 'table table-responsive', 'id' =>'cotizacion-actividad-table'],
	                'options' =>[
	                    'class' => 'grid-blue',
	                ],
                    'columns' => [
                        [
                   			'label' => 'Tipo de Pieza',
                            'format' => 'text',
                            'filter' => false,
                            'enableSorting' => false,
                            'value' => function( $model ){
                            	 
                            	return $model->pieza->tipoPieza->nombre;
                        	}
                        ],
                        [
                   			'label' => 'Pieza',
                            'format' => 'text',
                            'filter' => false,
                            'enableSorting' => false,
                            'value' => function( $model ){
                            	 
                             	return $model->pieza->nombre;
                           
                        	}
                        ],
                        [
                            'label' => 'Acción',
                            'format' => 'text',
                            'filter' => false,
                            'enableSorting' => false,
                            'value' => function( $model ){
                             
                    	 		if ($model->accion==0) {
                    	 			return 'Reparación';
                    	 		} else if ($model->accion==1){
                    	 			return 'Recuperación';
                    	 		} else if ($model->accion==2){
                    	 			return 'Cambio de pieza';
                    	 		} else{
                    	 			return '-';
                    	 		}
                        	}
                        ],
                    ],
                         
                ]); ?>
		</div>
	</div>




            

	</br>

	<br>
  	<br>
    <div class="row" style="border-bottom: 1px solid #222d32;">
        <div class="col-xs-12 col-md-6">
            <h4>DIAGNÓSTICO TÉCNICO</h4>
        </div>
     
    </div>
    <br>

    <div class="row">
    	<div class="col-md-12 col-xs-12">
			<?= $form->field($clavaluo, 'diagnostico')->textarea(['rows' => '6'])->textInput(['value' => strtoupper($clavaluo->diagnostico), 'disabled' => true])->label('OBSERVACIONES') ?>
		</div>
    </div>





	<?php ActiveForm::end(); ?>

</div>





