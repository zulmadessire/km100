<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\db\Query;
use app\models\PipelineServicio;
use app\models\Empresa;
use app\models\CotizacionTallerAnalista;
use app\models\ChecklistAvaluo;
use app\models\ChecklistAvaluoPieza;


/* @var $this yii\web\View */
/* @var $searchModel app\models\PartesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'LISTADO DE SOLICITUDES';
?>
 
<div class="taller-index">

    <div class="row">
        <div class="col-md-1 col-xs-3">
            <i class="fa fa-list-ul fa-3x" aria-hidden="true"></i>
        </div>
        <div class="col-md-10 col-xs-9">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
        </div>
    </div>
    </br>
    </br>
    
    <div class="row">
        <div class="col-xs-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                // 'filterModel' => $searchModel,
                'tableOptions' => ['class' => 'table table-responsive'],
                'summary' => '',
                'options' =>[
                    'class' => 'grid',
                ],
                'columns' => [
                     
                    [
                        'attribute' => 'id_servicio',
                        'label' => 'Número de solicitud',
                        'format' => 'text',
                        'filter' => true,
                        'enableSorting' => true,
                    ],
                    [
                        'attribute' => 'servicio',
                        'label' => 'Tipo de solicitud',
                        'format' => 'raw',
                        'filter' => false,
                        'enableSorting' => false,
                        'value' => function( $model ){

                            if ($model->servicio->servicio ) {
                                $servicios = explode("*", $model->servicio->servicio);

                                foreach ($servicios as $key => $servicio) {
                                    switch ($servicio) {
                                        case '0':
                                            $vector_servicios[] = 'Mecánica Ligera';
                                            break;

                                        case '1':
                                            $vector_servicios[] = 'Mecánica Especializada';
                                            break;

                                        case '2':
                                            $vector_servicios[] = 'Aire Acondicionado';
                                            break;

                                        case '3':
                                            $vector_servicios[] = 'Desabolladura y pintura';
                                            break;

                                        case '4':
                                            $vector_servicios[] = 'Accesorios';
                                            break;
                                        
                                        default:
                                            # code...
                                            break;
                                    }
                                    
                                }

                                $respuesta_servicios = implode('<br>', $vector_servicios);

                                return $respuesta_servicios;
                            } else{
                                return '-';
                            }
                        }
                    ],
                    [
                        'attribute' => 'servicio.taller',
                        'label' => 'Taller',
                        'format' => 'text',
                        'filter' => true,
                        'enableSorting' => true,
                    ],
                    [  
                        'class' => 'yii\grid\ActionColumn',
                        //'contentOptions' => ['style' => 'width:260px;'],
                        'header'=>'Acciones',
                        'template' => '{menu}',
                        'buttons' => [

                            //view button
                            'menu' => function ($url, $model) {

                                return  '<div class="dropdown">
                                            <button class="btn btn-default dropdown-toggle btn-sm" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i>
                                            <span class="caret"></span></button>
                                            <ul class="dropdown-menu">'.
                                                (($model->danos === NULL )?
                                                    '<li>'.Html::a('<span><i class="fa fa-pencil fa-sm" aria-hidden="true"></i></span> Registrar Avalúo',[ '/analista-mantenimiento/checklist', 'id' => $model->id]).' </li>'
                                                :
                                                    '<li>'.Html::a('<span><i class="fa fa-eye fa-sm" aria-hidden="true"></i></span> Ver Avalúo',[ '/analista-mantenimiento/verchecklist', 'id' => $model->id,]).' </li>')
                                            .'</ul>
                                        </div>';
                            },
                        ],

                    ],
                  
                ],
            ]); ?>
        </div>
    </div>
</div>