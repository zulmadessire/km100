<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AnalistaMantenimiento */

$this->title = 'ANALISTA DE MANTENIMIENTO:  '.$model->nombre;
?>
<div class="analista-mantenimiento-view">
        <div class="row">
    <div class="col-md-1 col-xs-3">
      <h4 class="titulo"><i class="fa fa-cog fa-4x" aria-hidden="true"></i></h4>
    </div>
    <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
      <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
    </div>
  </div>
            <div class="row" style="border-bottom: 1px solid #222d32;">
            <div class="col-xs-12 col-md-6">
                <h4>DATOS DEL ANALISTA</h4>
            </div>
            <div class="col-xs-12 col-md-6">
              <div align="right">
                    <?php
                    echo Html::a('Listado de Analistas', ['index'], ['class' => 'btn btn-small-gris']);
                    ?>&nbsp;&nbsp;&nbsp;<?php
                    echo Html::a('Modificar Analista', ['update', 'id' => $model->id], ['class' => 'btn btn-small-gris']);
                    ?>
              </div>
            </div>
        </div>

<br>

        <table class ="tablacaso" border="1">
                <tr><th style="width: 10%;">Nombre: </th>           <td><?= $model->nombre ?></td></tr>
                <tr><th>Teléfono: </th>                               <td><?= $model->telefono ?></td></tr>
                <tr><th>Dirección: </th>                   <td><?= ($model->direccion) ?></td></tr>
                <tr><th>Correo: </th>                          <td><?= ($model->correo) ?></td></tr>
                <tr><th>Usuario: </th>                          <td><?= ($model->user) ?></td></tr>
                </tr>

        </table><br>

    

</div>
