<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\AnalistaMantenimiento */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="analista-mantenimiento-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
      <div class="col-md-2">
        <?= $form->field($model, 'user')->textInput(['maxlength' => true])->label('NOMBRE DE USUARIO <span class="obligatorio">*&nbsp;</span>');  ?>
      </div>
    	<div class="col-md-4">
    		<?= $form->field($model, 'nombre')->textInput(['maxlength' => true])->label('NOMBRE <span class="obligatorio">*&nbsp;</span>');  ?>
    	</div>
    	<div class="col-md-3">
    		<?= $form->field($model, 'correo')->textInput(['maxlength' => true])->label('CORREO<span class="obligatorio">*&nbsp;</span>');  ?>
    	</div>
    	<div class="col-md-3">
    		<?= $form->field($model, 'telefono')->textInput(['maxlength' => true])->label('TELÉFONO<span class="obligatorio">*&nbsp;</span>');  ?>
    	</div>
    </div>
    <div class="row">
    	<div class="col-md-12">
    		<?= $form->field($model, 'direccion')->textInput(['maxlength' => true])->label('DIRECCIÓN <span class="obligatorio">*&nbsp;</span>'); ?>
    	</div>
    </div>

  </br>
  </br>
  </br>

      

    
<?php
      if(!$model->isNewRecord){
      ?>
          <div class="row" align="center">
              <div>
                  <?= Html::a('CANCELAR', ['view', 'id' => $model->id], ['class' => 'btn btn-cancelar']) ?>
                  &nbsp;&nbsp;&nbsp;
                  <?= Html::submitButton('GUARDAR CAMBIOS', ['class' => 'btn btn-primario']) ?>
              </div>
          </div>
      <?php
      }else{
      ?>
          <div class="form-group" align="center">
              <?= Html::submitButton('GUARDAR', ['class' => 'btn btn-primario']) ?>
          </div>
      <?php
      }
  ?>
    

    <?php ActiveForm::end(); ?>

</div>
