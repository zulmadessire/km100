<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AnalistaMantenimiento */

$this->title = 'ANALISTA DE MANTENIMIENTO';
?>
 
<div class="analista-mantenimiento-create">
 <div class="row">
    <div class="col-md-1 col-xs-3">
      <h4 class="titulo"><i class="fa fa-cog fa-4x" aria-hidden="true"></i></h4>
    </div>
    <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
      <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
    </div>
  </div>
        <div class="row" style="border-bottom: 1px solid #222d32;">
              <div class="col-xs-12 col-md-6">
                  <h4>DATOS DEL ANALISTA DE MANTENIMIENTOS</h4>
              </div>
              <div class="col-xs-12 col-md-6">
                <div align="right">
                      <?php
                      echo Html::a('Listado de Analista de Mantenimientos', ['index'], ['class' => 'btn btn-accion']);
                      ?>
                </div>
              </div>
          </div>
          <br>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
