<?php 
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\CotizacionTallerActividad;
use app\models\analistamantenimiento;
use app\models\TipoPieza;
use yii\helpers\Url;
use app\models\ChecklistAvaluo;
use app\models\ChecklistAvaluoPieza;
use app\models\Pieza;
use app\models\Empresa;
use wbraganca\dynamicform\DynamicFormWidget;

 

/* @var $this yii\web\View */
/* @var $model app\models\Taller */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'CHECKLIST AVALÚO';
?>

<div class="analista-form">
	    <div class="row">
        <div class="col-md-1 col-xs-3">
        	<h4 class="titulo">
        		<i class="fa fa-list fa-4x" aria-hidden="true"></i>
        	</h4>
        </div>
        <div class="col-md-10 col-xs-9" style="padding-top: 20px;">
            <h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
        </div>
    </div>

    <?php $form = ActiveForm::begin(['id' => 'dynamic_lista-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>

    <!-- --DATOS DEL TALLER-- -->

    <div class="row" style="border-bottom: 1px solid #222d32;">
        <div class="col-xs-12 col-md-6">
            <h4>DATOS DEL VEHÍCULO</h4>
        </div>
     
    </div>
    <br>
    <br>
        <div class="row">
        <div class="col-md-4 col-xs-12">
            <?= $form->field($n_sol, 'id')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'mva')->textInput(['maxlength' => true, 'disabled' => true])->label('Ficha o MVA') ?>
        </div>
    	<div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_modelo')->textInput(['disabled' => true, 'value' => $model_vehiculo->idModelo->idMarca->nombre ])->label('Marca') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_modelo')->textInput(['value' => strtoupper($model_vehiculo->idModelo->nombre), 'disabled' => true])->label('Modelo') ?>
        </div>

    	

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'placa')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'anho')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>
    </div>
    <br>
     <div class="row" style="border-bottom: 1px solid #222d32;">
        <div class="col-xs-12 col-md-6">
            <h4>INFORMACIÓN DE DAÑOS</h4>
        </div>
     
    </div>
 	
 	<div class="row">
 		<div class="col-md-4">
 			<br><br>
 			 <?php echo Html::img(Yii::$app->request->BaseUrl.'/img/autopiezas.png') ?>
 		</div>
 		<div class="col-md-8">

 			     <div class="col-xs-12 col-md-11 col-md-offset-1 checkbox">
                    <label class="col-xs-2 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox1" value="1" name="pie[]"> 1
                    </label>
                    <label class="col-md-2 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox2" value="2" name="pie[]"> 2
                    </label>
                    <label class="col-md-2 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox3" value="3" name="pie[]"> 3
                    </label>
                    <label class="col-md-2 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox4" value="4" name="pie[]"> 4
                    </label>
                    <label class="col-md-2 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox5" value="5" name="pie[]"> 5
                    </label>
 
                </div>
                <br>
                  <div class="col-xs-12 col-md-11 col-md-offset-1 checkbox">
                    <label class="col-xs-2 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox6" value="6" name="pie[]"> 6
                    </label>
                    <label class="col-md-2 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox7" value="7" name="pie[]"> 7
                    </label>
                    <label class="col-md-2 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox8" value="8" name="pie[]"> 8
                    </label>
                    <label class="col-md-2 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox9" value="9" name="pie[]"> 9
                    </label>
                    <label class="col-md-2 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox10" value="10" name="pie[]"> 10
                    </label>
                   
                </div>
                            <br>
                  <div class="col-xs-12 col-md-11 col-md-offset-1 checkbox">
                    <label class="col-xs-2 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox11" value="11" name="pie[]"> 11
                    </label>
                    <label class="col-md-2 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox12" value="12" name="pie[]"> 12
                    </label>
                    <label class="col-md-2 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox13" value="13" name="pie[]"> 13
                    </label>
                    <label class="col-md-2 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox14" value="14" name="pie[]"> 14
                    </label>
                    <label class="col-md-2 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox15" value="15" name="pie[]"> 15
                    </label>
                   
                </div>
                            <br>
                  <div class="col-xs-12 col-md-11 col-md-offset-1 checkbox">
                    <label class="col-xs-2 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox16" value="16" name="pie[]"> 16
                    </label>
                    <label class="col-md-2 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox17" value="17" name="pie[]"> 17
                    </label>
                    <label class="col-md-2 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox18" value="18" name="pie[]"> 18
                    </label>
                    <label class="col-md-2 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox19" value="19" name="pie[]"> 19
                    </label>
 
                   
                </div>


 		</div>

 	</div>
 
 
     <div class="row" style="border-bottom: 1px solid #222d32;">
        <div class="col-xs-12 col-md-6">
            <h4>PIEZAS AFECTADAS</h4>
        </div>
     
    </div>

    </br>


 

 <br>
<?php DynamicFormWidget::begin([
        'widgetContainer' => 'dynamicform_wrapper_resultados',
        'widgetBody' => '.container-items',
        'widgetItem' => '.item',
        'limit' => 50,
        'min' => 1,
        'insertButton' => '.add-item',
        'deleteButton' => '.remove-item',
        'model' => $modelcheckavap[0],
        'formId' => 'dynamic_lista-form',
        'formFields' => [
            'nombre',
            'id_modelo',
            'id_marca',
            'pieza_id',
        ],
  ]); ?>
  <table class="table table-hover">
      <tbody class="container-items">
      <?php foreach ($modelcheckavap as $i => $modelmodel): ?>
          <tr class="item panel panel-default">
              <td>
             <div class="col-xs-12 col-md-3"><?php
                      $tiposdepieza = ArrayHelper::map(TipoPieza::find()->where(['id'=>[4,5]])->orderBy('nombre')->all(), 'id', 'nombre');
                      echo $form->field($modelmodel, "[{$i}]tipo")->dropDownList(
                              $tiposdepieza,
                              [
                                  'prompt'=>'Selecciona...',
                                  'class' => 'form-control',
                                  'onchange'=>'
                                      var ident = $(this).attr("id");
                                      var nom = ident.split("-");
                                      var nmodelo = "checklistavaluopieza-"+nom[1]+"-pieza_id";
                                      $.get( "'.Url::toRoute('analista-mantenimiento/listar').'", { id: $(this).val() } )
                                          .done(function( data ) {
                                              $( "#"+nmodelo ).html( null );
                                              $( "#"+nmodelo ).html( data );
                                          }
                                      );
                                  ',
                              ] )->label('TIPO'); ?>
                      </div>
                  <div class="col-xs-12 col-md-3">
                    <?php
                      $piezas=array(); //echo "trae: {$i} --- ";
                      if(!$modelmodel->isNewRecord){
                          $piezas = ArrayHelper::map(Pieza::find()->where(['tipo_pieza_id'=>$modelmodel->id])->orderBy('nombre')->all(), 'id_pieza', 'nombre');
                          // print_r($piezas);
                      }
                      echo $form->field($modelmodel, "[{$i}]pieza_id")->dropDownList(
                          $piezas,
                          [
                              'prompt'=>'Selecciona el modelo',
                              'class' => 'form-control',
                          ])->label('PIEZA');
                    ?>
                  </div>
                     <div class="col-xs-12 col-md-3">
            
                      <?php 
                $tip_clientes = ['0'=>'Recuperación', '1'=>'Reparación', '2' => 'Cambio'];
                echo $form->field($modelmodel, "[{$i}]accion")->dropDownList(
                        $tip_clientes,
                        [
                            'prompt'=>'Seleccione...',
                            'class' => 'form-control',
                        ])->label('ACCIÓN');
                        ?>
                  </div>
          

                  <div class="col-xs-12 col-md-1 text-center">
                      <button type="button" class="remove-item btn btn-cancelar btn-xs" style="margin-top:28px;">
                          <i class="glyphicon glyphicon-minus"></i></button>
                  </div>
              </td>
          </tr>
      <?php endforeach; ?>
  <?php DynamicFormWidget::end(); ?>
      </tbody>
  </table>

  <button type="button" class="add-item btn btn-accion  btn-xs pull-right">Agregar Pieza</button>
  <br>
  <br>
     <div class="row" style="border-bottom: 1px solid #222d32;">
        <div class="col-xs-12 col-md-6">
            <h4>DIAGNÓSTICO TÉCNICO</h4>
        </div>
     
    </div>
    <br>
 <div class="col-md-12 col-xs-12">
   <?= $form->field($clavaluo, 'diagnostico')->textarea(['rows' => '6'])->label(false) ?>
   
</div>
     <div class="form-group" align="center">
                <?= Html::submitButton('GUARDAR', ['class' => 'btn btn-primario']) ?>
            </div>
<div class="row">
 

    <?php ActiveForm::end(); ?>

</div>


 

 
 