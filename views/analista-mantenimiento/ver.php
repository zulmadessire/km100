<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AnalistaMantenimientosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'VER AVALÚOS';
?>

<div class="analista-mantenimiento-ver">

     <div class="row">
    <div class="col-md-1 col-xs-3">
      <h4 class="titulo"><i class="fa fa-search fa-4x" aria-hidden="true"></i></h4>
    </div>
    <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
      <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
    </div>
  </div>
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                <?= Html::a('Listado de Analistas',['analista-mantenimiento/index'], ['class' => 'btn btn-small-gris']) ?>
            </div>
        </div>
        </br>
        </br>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= GridView::widget([
        'dataProvider' => $dataProvider,
 
        'tableOptions' => ['class' => 'table table-responsive'],
        'summary' => '',
              
                    'options' =>[
                        'class' => 'grid-blue',
                    ],
        'columns' => [
            [
                        
                        'label' => 'Taller',
                        'format' => 'text',
                        'enableSorting' => true,
                        'value' => function ($model, $key, $index, $column) {
                               
                            return $model->idCotizacionTaller->idSolicitudServicioTaller->idTaller->nombre;
                               
                                },
                    ],
            [
                             
                            'label' => 'N° de cotización del analista en el taller',
                            'format' => 'text',
                            'value' => 'id',
                            'enableSorting' => true,
                    
            ],
         
             
         [  
                            'class' => 'yii\grid\ActionColumn',
                            //'contentOptions' => ['style' => 'width:260px;'],
                            'header'=>'Acciones',
                            'template' => '{menu}',
                            'buttons' => [

                                //view button
                                'menu' => function ($url, $model) {

                                     return  '<div class="dropdown">
                                              <button class="btn btn-default dropdown-toggle btn-sm" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i>
                                              <span class="caret"></span></button>
                                              <ul class="dropdown-menu">
                                                <li>'.Html::a('<span><i class="fa fa-eye fa-sm" aria-hidden="true"></i></span> Ver Avalúo',[ '/analista-mantenimiento/veravaluo', 'id' => $model->id, 'idcotizaciontaller' => $model->id_cotizacion_taller, 'idanalista' => $model->id_analista ]).' </li>
 
                                                
                                              </ul>
                                            </div>';


                                },
                            ],

                       ],


  
        ],
    ]); ?>
        </div>
    </div>
 </div>