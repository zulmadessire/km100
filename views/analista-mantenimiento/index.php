<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AnalistaMantenimientosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'LISTADO DE ANALISTAS DE MANTENIMIENTO';
?>
<div class="analista-mantenimiento-index">

    <div class="row">
        <div class="col-md-1 col-xs-3">
            <h4 class="titulo"><i class="fa fa-cog fa-4x" aria-hidden="true"></i></h4>
        </div>
        <div class="col-md-10 col-xs-9" style="padding-top: 20px;">
            <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                <?= Html::a('Registrar Analista de mantenimientos',['analista-mantenimiento/create'], ['class' => 'btn btn-small-gris']) ?>  
            </div>
        </div>
        </br>
        </br>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => ['class' => 'table table-responsive'],
                'summary' => '',
                'options' =>[
                    'class' => 'grid',
                ],
                'columns' => [

                    [
                        'attribute' => 'nombre',
                        'label' => 'Nombre',
                        'format' => 'text',
                        'filter' => true,
                        'enableSorting' => false,
                        'value' => function( $model ){
                            return $model->nombre;
                        }
                    ],
                    [
                        'attribute' => 'telefono',
                        'label' => 'Teléfono',
                        'enableSorting' => false,
                        'value' => function( $model ){
                            return $model->telefono;
                        }
                    ],
                    [
                        'attribute' => 'direccion',
                        'label' => 'Dirección',
                        'enableSorting' => false,
                        'value' => function( $model ){
                            return $model->direccion;
                        }
                    ],
                    [
                        'attribute' => 'correo',
                        'label' => 'Correo',
                        'enableSorting' => false,
                        'value' => function( $model ){
                            return $model->correo;
                        }
                    ],
                                          
                    [  
                        'class' => 'yii\grid\ActionColumn',
                        //'contentOptions' => ['style' => 'width:260px;'],
                        'header'=>'Acciones',
                        'template' => '{menu}',
                        'buttons' => [

                            //view button
                            'menu' => function ($url, $model) {

                                 return  '<div class="dropdown">
                                          <button class="btn btn-default dropdown-toggle btn-sm" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i>
                                          <span class="caret"></span></button>
                                          <ul class="dropdown-menu">
                                            <li>'.Html::a('<span><i class="fa fa-eye fa-sm" aria-hidden="true"></i></span> Ver Detalles',[ '/analista-mantenimiento/view', 'id' => $model->id ]).' </li>
                                            <li>'.Html::a('<span><i class="fa fa-pencil fa-sm" aria-hidden="true"></i></span> Modificar Datos',[ '/analista-mantenimiento/update', 'id' => $model->id ]).' </li>
                                             <li>'.Html::a('<span><i class="fa fa-search fa-sm" aria-hidden="true"></i></span> Ver Avalúos',[ '/analista-mantenimiento/ver', 'id' => $model->id ]).' </li>
                                            
                                          </ul>
                                        </div>';


                            },
                        ],

                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>


 
    