  <?php

  use yii\helpers\Html;
  use yii\grid\GridView;
  use yii\db\Query;
  use kartik\select2\Select2;
use yii\widgets\ActiveForm;
  use yii\helpers\ArrayHelper;
  use app\models\Empresa;
  use app\models\Estacion;
   use app\models\AuditoriaGeneral;
  use kartik\date\DatePicker;
use yii\helpers\Url;
use kartik\depdrop\DepDrop;
  /* @var $this yii\web\View */
  /* @var $searchModel app\models\AuditoriageneralSearch */
  /* @var $dataProvider yii\data\ActiveDataProvider */

  $this->title = 'HISTORICO DE AUDITORIAS';
  ?>
  <div class="auditoria-index">
    <div class="auditoria-index">
         <?php $form = ActiveForm::begin([
            'action' => ['historico'],
            'method' => 'get',
        ]); ?>
        <div class="row">
      <div class="col-md-1 col-xs-3">
        <h4 class="titulo"><i class="fa fa-list-ul fa-4x" aria-hidden="true"></i></h4>
      </div>
      <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
        <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
      </div>
    </div>
   <span>Total de auditorias realizadas: <?= $dataProvider->getTotalCount() ?></span>

   </br>
    <div class="row">
        <?php
            echo Html::a('Auditorias Mensuales Pendientes', ['index'], ['class' => 'btn btn-accion', 'style' => 'float:right;']);
        ?>
    </div>

               <div class="col-md-2 col-xs-12">
            <div class="form-group">
                <label class="control-label" for="id_empresa">CLIENTE</label>
                <?=  Select2::widget([
                        'name' =>'AuditoriageneralSearch[empresa]',
                        'data' => ArrayHelper::map(Empresa::find()->orderBy('nombre')->all(), 'id_empresa', 'nombre'),
                        'options' => ['placeholder' => 'Selecciona la Empresa', 'id' => 'id_empresa']
                    ]); 
                ?>
            </div>
        </div>
      
        <div class="col-md-2 col-xs-12">
            <div class="form-group">
                <label class="control-label" for="id_estado">ESTACIÓN</label>
                <?=
                    DepDrop::widget([
                        'name' => 'AuditoriageneralSearch[id_estacion]',
                        'type'=>DepDrop::TYPE_SELECT2,
                        'options'=>['id'=>'id_estacion'],
                        'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                        'pluginOptions'=>[
                            'depends'=>['id_empresa'],
                            'placeholder'=>'Selecciona la estación',
                            'url'=>Url::to(['/vehiculo/getestaciones'])
                        ]
                    ]);
                ?>
            </div>
        </div>
           <div class="col-md-4 col-xs-12">
            <div class="form-group">
                    <?php 
            echo '<label class="control-label">RANGO DE FECHAS</label>';
            echo DatePicker::widget([
    'name' => 'AuditoriageneralSearch[fecha1]',
    'type' => DatePicker::TYPE_RANGE,
    'name2' => 'AuditoriageneralSearch[fecha2]',
    'pluginOptions' => [
        'autoclose'=>true,
        'format' => 'yyyy-mm-dd'
    ]
]);
 
         ?>
        
            </div>
        </div>
        <br>
  <div class="col-md-1 col-xs-12" style="margin-top:3px">
            <div class="form-group text-center">
                <?= Html::submitButton('BUSCAR', ['class' => 'btn2']) ?>
            </div>
        </div>

    <br>
   
      <?= GridView::widget([
             'dataProvider' => $dataProvider,
          // 'filterModel' => $searchModel,
           'tableOptions' => ['class' => 'table table-responsive'],
                 'summary' => '',
                      'options' =>[
                          'class' => 'grid',
                      ],
          'columns' => [
                       [
                           
                              'label' => 'Empresa',
                              'format' => 'text',
                              'filter' => true,
                              'enableSorting' => false,
                              'value' => function( $model ){
                                  return $model->idEstacion->idEmpresa->nombre;
                              },
                              //    'filter' => Select2::widget([
                              //   'name' => 'AuditoriageneralSearch[empresa]',
                                   
                              //     'attribute' => 'id_estacion',
                              //     'data' => ArrayHelper::map(Empresa::find()->asArray()->all(), 'id_empresa', 'nombre'),
                              //     'options' => ['placeholder' => 'Seleccione Estación...'],
                              //     'pluginOptions' => [
                              //         'allowClear' => true
                              //     ],
                              // ])
                              
              ],
                         [
                              'attribute' => 'id_estacion',
                              'label' => 'Estación',
                              'format' => 'text',
                              // 'filter' => ArrayHelper::map(Estacion::find()->asArray()->all(), 'id_estacion', 'nombre'),
                              'enableSorting' => false,
                              'value' => function( $model ){
                                  return $model->idEstacion->nombre;

                              }
                              
              ],
                   [
                              'attribute' => 'fecha',
                              'label' => 'Fecha',
                              'format' => 'text',
                              'filter' => DatePicker::widget([
    'name' => 'from_date',
    'type' => DatePicker::TYPE_RANGE,
    'name2' => 'to_date',
    'pluginOptions' => [
        'autoclose'=>false,
        'format' => 'dd-M-yyyy'
    ]
]),
//                               'filter' =>  DatePicker::widget([
//   'name' => 'AuditoriageneralSearch[fecha]', 
//   'pluginOptions' => [
//     'format' => 'yyyy-mm-dd',
//   ]
// ]),
                              'enableSorting' => false,
                              'value' => function( $model ){
                                  return $model->fecha;

                              }
                              
              ],
               [
                          'attribute' => 'estado',
                          'format' => 'raw',
                          'filter' => ['1'=>'Incompleta', '3'=>'Realizado'],
                          'enableSorting' => false,
                          'value' => function( $model ){
                                  if ($model->estado==3) {
                                    $estado = 'Realizada';
                                    $color = '#00A65A';
                                     return '<span class="label label-oval" style="background-color:'.$color.'; ">'.$estado.'</span>';
                                  }
                                 

                              }
                      ],


                          [  
                        'class' => 'yii\grid\ActionColumn',
                        //'contentOptions' => ['style' => 'width:260px;'],
                        'header'=>'Acciones',
                        'template' => '{menu}',
                        'buttons' => [

                            //view button
                            'menu' => function ($url, $model) {

                                 return  '<div class="dropdown">
                                          <button class="btn btn-default dropdown-toggle btn-sm" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i>
                                          <span class="caret"></span></button>
                                          <ul class="dropdown-menu">
                                            
                                             <li>'.Html::a('<span><i class="fa fa-eye fa-sm" aria-hidden="true"></i></span> Ver detalle de auditoria',[ '/auditoriageneral/imprimirpdf', 
                                              'id' => $model->id_estacion,
                                              'fecha' => $model->fecha ] ).' </li>
                                             
                                            
                                          </ul>
                                        </div>';


                            },
                        ],

                   ],
                      ],
           
      ]); ?>
  <?php ActiveForm::end(); ?>
  </div>
