<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Auditoriageneral */

$this->title = 'Create Auditoriageneral';
$this->params['breadcrumbs'][] = ['label' => 'Auditoriagenerals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auditoriageneral-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
