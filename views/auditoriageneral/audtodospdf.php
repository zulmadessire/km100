<?php 

use app\models\Auditoriafs;
use yii\helpers\Html;
use app\models\Empresa;
use app\models\Vehiculo;
use app\models\Modelo;
use app\models\AddFs;
use app\models\Estacion;
use app\models\TipoMantenimiento;
use app\models\Aspectos;

$subtotal = 0;
$vehiculoss='';
$estacion = Estacion::find()->where(['id_estacion'=>$model_general['id_estacion']])->one();
$empresa = Empresa::find()->where(['id_empresa'=>$estacion['id_empresa']])->one();

?>
<div class="content">
	<div class="row">
		<img src="/km100/img/logo.png"  width="70" style="float: left;">
		<div class="col-xs-6" style="font-size: 10pt;">
			<b>Inversiones Forteza, S.R.L.</b><br>
			Av. Republica de Colombia esq.<br>
			Av. Monumental<br>
			Tel.: 809-331-6009<br>
			RNC: 130213445<br>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-xs-12 text-center" style="font-size: 12pt;">
			<b>AUDITORIAS</b>
		</div>
	</div>
	<br><br>
	<div class="row">

		<div class=" col-xs-12" style=" border-radius: 3px; padding: 5px; padding-left: 10px;">
			<div style="width: 35%; float: left; ">
				<span style="font-size: 8pt;"><b>NOMBRE DE LA EMPRESA</span></b><br>
				<span style="font-size: 8pt;"><?= $empresa['nombre'] ?></span><br>
				<br>

			</div>
			<div style="width: 35%; float: left; ">

				<span style="font-size: 8pt;"><b>NOMBRE DE LA ESTACIÓN </b></span><br>
				<span style="font-size: 8pt;"><?= $estacion['nombre']  ?></span><br>
				<br>

			</div>
			<div style="width: 30%; float: left; ">

				<span style="font-size: 8pt;"><b>FECHA:</b> <?= $model_general['fecha']  ?><br></span>

			</div>
		</div>
	</div> 
			<div class="row">
		<div class="col-xs-12 text-center" style="font-size: 12pt;">
			<b>AUDITORIAS FUERA DE SERVICIO</b>
			<hr>
		</div>
	</div>
	<br><br>
	<?php if ($modelfs != ''){?>
		<div class="row">
	

	<div style="width: 35%; float: left; ">
				<span style="font-size: 8pt;"><b>FECHA</span></b><br>
				<span style="font-size: 8pt;"><?= $modelfs['fecha'] ?></span><br>
				<br>

			</div>
		<div class=" col-xs-12" style=" border-radius: 3px; padding: 5px; padding-left: 10px;">
			<div style="width: 35%; float: left; ">
				<span style="font-size: 8pt;"><b>FUERA DE SERVICIO REGISTRADO</span></b><br>
				<span style="font-size: 8pt;"><?= $modelfs['veh_fuera_serv_reg'] ?></span><br>
				<br>

			</div>
			<div style="width: 35%; float: left; ">

				<span style="font-size: 8pt;"><b>FUERA DE SERVICIO EN ESTACIÓN</b></span><br>
				<span style="font-size: 8pt;"><?= $modelfs['veh_fuera_ser_est']  ?></span><br>
				<br>

			</div>
			<div style="width: 30%; float: left; ">
				<span style="font-size: 8pt;"><b>DESVIACIÓN:</b> <?= $modelfs['desviacion']  ?><br></span>
			</div>
			<div style="width: 35%; float: left; ">
				<span style="font-size: 8pt;"><b>MOTIVO DE DESVIACIÓN</span></b><br>
				<span style="font-size: 8pt;"><?= $modelfs['motivo'] ?></span><br>
				<br>

			</div>
			<div style="width: 35%; float: left; ">

				<span style="font-size: 8pt;"><b>PLAN DE ACCIÓN</b></span><br>
				<span style="font-size: 8pt;"><?= $modelfs['plan_accion']  ?></span><br>
				<br>

			</div>
		</div>
	</div> 

	<!-- fin del div row -->
	<div class="row">
		<div class="col-xs-12 text-center" style="font-size: 12pt;">
			<b>UNIDADES FUERA DE SERVICIO EN ESTACIÓN</b>
			 
		</div>
	</div>
		<div class="row">
			<br><br>
	<div class="row">
		<div class="col-xs-11">
			<table class="table">
				<thead class="text-center">
					<tr>
						<th style="text-align: center; font-size: 25pt;">FICHA</th>
						<th style="text-align: center; font-size: 25pt;">MODELO</th>
						<th style="text-align: center; font-size: 25pt;">CAUSA</th>
						<th style="text-align: center; font-size: 25pt;">RESPONSABLE</th>
						<th style="text-align: center; font-size: 25pt;">OBSERVACIÓN</th>
						<th style="text-align: center; font-size: 25pt;">ANEXO 1</th>
						<th style="text-align: center; font-size: 25pt;">ANEXO 2</th>



					</tr>
				</thead>
				<tbody>
					<?php foreach ($modelsolaud as $i => $sol) { ?>
		 		<?php 
        // $vehiculos = Vehiculo::find()->where(['id'=>$add['id_vehiculo']])->one();
        $modelos = Modelo::find()->where(['id_modelo'=>$sol['id_modelo']])->one();
					 ?>
						<tr>
							<td style="text-align: left; font-size: 20pt;"><?= strtoupper($sol->ficha) ?>
							<td style="text-align: center; font-size: 20pt;"><?= $modelos->nombre ?></td>
							<td style="text-align: center; font-size: 20pt;"><?= strtoupper($sol->causa) ?></td>
							<td style="text-align: center; font-size: 20pt;"><?= strtoupper($sol->responsable) ?></td>
							<td style="text-align: left; font-size: 20pt;"><?= strtoupper($sol->observacion) ?>
							<td style="text-align: left; font-size: 2pt;">
  								<?php $sop = $sol->base_url;?>
					 

      
                   <a href=<?= Yii::$app->request->baseUrl.'/files/auditorias/'.$sol->id_aud_fs.'/'.$sop ?> data-lightbox=<?= Yii::$app->request->baseUrl.'/files/auditorias/'.$sol->id_aud_fs.'/'.$sop ?>>
                   <img src=<?= Yii::$app->request->baseUrl.'/files/auditorias/'.$sol->id_aud_fs.'/'.$sop ?> style="max-width:60%;" class=<?= Yii::$app->request->baseUrl.'/files/auditorias/'.$sol->id_aud_fs.'/'.$sop ?> />
                   </a>

						 

                                                      

						</td>
				<td style="text-align: left; font-size: 2pt;">
  					 
  								<?php $sop2 = $sol->base_url2;?>
  				
                   <a href=<?= Yii::$app->request->baseUrl.'/files/auditorias/'.$sol->id_aud_fs.'/'.$sop2 ?> data-lightbox=<?= Yii::$app->request->baseUrl.'/files/auditorias/'.$sol->id_aud_fs.'/'.$sop2 ?>>
                   <img src=<?= Yii::$app->request->baseUrl.'/files/auditorias/'.$sol->id_aud_fs.'/'.$sop2 ?> style="max-width:60%;" class=<?= Yii::$app->request->baseUrl.'/files/auditorias/'.$sol->id_aud_fs.'/'.$sop2 ?> />
                   </a>
                                                                      
						</td>
							 

						</tr>
						<?php 
 
					} 
					?>
				</tbody>
			</table>
		</div>
	</div>
	</div>
	<br><br>
	<!-- fin del div row -->
	<!-- fin del div row -->
	<div class="row">
		<div class="col-xs-12 text-center" style="font-size: 12pt;">
			<b>OTRA SOLICITUDES</b>
		 
		</div>
	</div>
	<div class="row">
			<br><br>
	<div class="row">
		<div class="col-xs-11">
			<table class="table">
				<thead class="text-center">
					<tr>
						<th style="text-align: center; font-size: 8pt;">#</th>
						<th style="text-align: center; font-size: 8pt;">CAUSA</th>
						<th style="text-align: center; font-size: 8pt;">OBSERVACIONES<br>COMPRA</th>
						<th style="text-align: center; font-size: 8pt;">FICHA/MVA</th>
						<th style="text-align: center; font-size: 8pt;">MODELO</th>
						<th style="text-align: center; font-size: 8pt;">KM ACTUAL</th>
						<th style="text-align: center; font-size: 8pt;">TIPO DE MANTENIMIENTO</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($modeladd as $i => $add) { ?>
					<?php 



					// $vehiculos = Vehiculo::find()->where(['id'=>$add['id_vehiculo']])->one();
        $modelos = Modelo::find()->where(['id_modelo'=>$add['id_modelo']])->one();
        $tipos = TipoMantenimiento::find()->where(['id_tipo_mnto'=>$add['id_tipo_mnto']])->one();




					 ?>
						<tr>
							<td style="text-align: center; font-size: 7pt;"><?= $add->id ?></td>
							<td style="text-align: center; font-size: 7pt;"><?= strtoupper($add->causa)?></td>
							<td style="text-align: right; font-size: 7pt;"><?= strtoupper($add->observacion)?></td>
							<td style="text-align: center; font-size: 7pt;"><?= strtoupper($add->ficha) ?></td>
							<td style="text-align: center; font-size: 7pt;"><?= $modelos['nombre'] ?></td>
							<td style="text-align: left; font-size: 7pt;"><?= strtoupper($add->km_actual) ?></td>
							<td style="text-align: center; font-size: 7pt;"><?= $tipos['nombre'] ?></td>
						</tr>
						<?php 
 
					} 
					?>
				</tbody>
			</table>
		</div>
	</div>
	</div>
	<br><br>

<!-- fin del div row -->
	<div class="row">
		<div class="col-xs-12 text-center" style="font-size: 12pt;">
			<b>OTRA SOLICITUDES PENDIENTES</b>
		 
		</div>
	</div>
	<div class="row">
			<br><br>
	<div class="row">
		<div class="col-xs-11">
			<table class="table">
				<thead class="text-center">
					<tr>
						<th style="text-align: center; font-size: 8pt;">#</th>
						<th style="text-align: center; font-size: 8pt;">TIPO</th>
						<th style="text-align: center; font-size: 8pt;">ITEM</th>
						<th style="text-align: center; font-size: 8pt;">RESPONSABLE</th>
						<th style="text-align: center; font-size: 8pt;">OBSERVACIÓN</th>
						
					</tr>
				</thead>
				<tbody>
					<?php foreach ($modelsotras as $i => $otras) { ?>
					<?php 



				 ?>
						<tr>
							<td style="text-align: center; font-size: 7pt;"><?= $otras->id ?></td>
							<td style="text-align: center; font-size: 7pt;"><?= strtoupper($otras->solicitud)?></td>
							<td style="text-align: right; font-size: 7pt;"><?= strtoupper($otras->causa)?></td>
							<td style="text-align: center; font-size: 7pt;"><?= strtoupper($otras->responsable) ?></td>
							<td style="text-align: left; font-size: 7pt;"><?= strtoupper($otras->observacion) ?></td>
						</tr>
						<?php 
 
					} 
					?>
				</tbody>
			</table>
		</div>
	</div>
	</div>
	<br><br>
 	<!-- fin del div row -->
	<div class="row">
		<div class="col-xs-12 text-center" style="font-size: 12pt;">
			<b>UNIDADES FUERA DE SERVICIO EN ESTACIÓN</b>
			 
		</div>
	</div>
		<div class="row">
			<br><br>
	<div class="row">
		<div class="col-xs-11">
			<table class="table">
				<thead class="text-center">
					<tr>
						<th style="text-align: center; font-size: 8pt;">OBSERVACIÓN</th>
						<th style="text-align: center; font-size: 8pt;">IMAGEN</th>


					</tr>
				</thead>
				<tbody>
					<?php foreach ($modelsimagenes as $i => $sol) { ?>
		 		<?php 
					 ?>
						<tr>
							<td style="text-align: left; font-size: 7pt;"><?= strtoupper($sol->descripcion) ?>
							<td style="text-align: left; font-size: 2pt;">
  								<?php $sop = $sol->base_url;?>
					 

      
                   <a href=<?= Yii::$app->request->baseUrl.'/files/auditorias/'.$sol->id_auditoriafs.'/'.$sop ?> data-lightbox=<?= Yii::$app->request->baseUrl.'/files/auditorias/'.$sol->id_auditoriafs.'/'.$sop ?>>
                   <img src=<?= Yii::$app->request->baseUrl.'/files/auditorias/'.$sol->id_auditoriafs.'/'.$sop ?> style="max-width:60%;" class=<?= Yii::$app->request->baseUrl.'/files/auditorias/'.$sol->id_auditoriafs.'/'.$sop ?> />
                   </a>

						 

                                                      

						</td>

			

		

						</tr>
						<?php 
 
					} 
					?>
				</tbody>
			</table>
		</div>
	</div>
	</div>
	<br><br>
	<!-- fin del div row -->

		<div style="width: 35%; float: left; ">
				<span style="font-size: 8pt;"><b>OBSERVACIONES</span></b><br>
				<span style="font-size: 8pt;"><?= strtoupper($modelfs['observacion']) ?></span><br>
				<br>

			</div>

	</div>

	<?php } ?>
 

<?php if ($modelmnto != ''){?>
	
		<div class="row">
		<div class="col-xs-12 text-center" style="font-size: 12pt;">
			<b>AUDITORIAS DE MANTENIMIENTO</b>
			<hr>
		</div>
	</div>
	<div class="row">
		<div style="width: 35%; float: left; ">
				<span style="font-size: 8pt;"><b>FECHA</span></b><br>
				<span style="font-size: 8pt;"><?= $modelmnto['fecha'] ?></span><br>
				<br>

			</div>
	<div class=" col-xs-12" style=" border-radius: 3px; padding: 5px; padding-left: 10px;">
			<div style="width: 35%; float: left; ">
				<span style="font-size: 8pt;"><b>PROYECCIÓN DE MTTO. SEMANA ANTERIOR</span></b><br>
				<span style="font-size: 8pt;"><?= $modelmnto['mntos_smna_anterior'] ?></span><br>
				<br>

			</div>
			<div style="width: 35%; float: left; ">

				<span style="font-size: 8pt;"><b>MTTOS. REALIZADOS</b></span><br>
				<span style="font-size: 8pt;"><?= $modelmnto['mntos_realizados']  ?></span><br>
				<br>

			</div>
			<div style="width: 30%; float: left; ">
				<span style="font-size: 8pt;"><b>DESVIACIÓN</b></span><br>
				<span style="font-size: 8pt;"><?= $modelmnto['desviacion'] ?></span><br>
			 <br></span>
			</div>
	</div> 
	<!-- fin row -->
<!-- fin del div row -->



		<div class="row">
		<div class="col-xs-12 text-center" style="font-size: 12pt;">
			<b>VEHÍCULOS A LOS QUE NO SE LES REGISTRO EL MTTO. PROYECTADO

</b>
			<hr>
		</div>
	</div>

	<div class="row">
			<br><br>
	<div class="row">
		<div class="col-xs-11">
			<table class="table">
				<thead class="text-center">
					<tr>
						<th style="text-align: center; font-size: 8pt;">#</th>
						<th style="text-align: center; font-size: 8pt;">FICHA/MVA</th>
						<th style="text-align: center; font-size: 8pt;">MODELO</th>
						<th style="text-align: center; font-size: 8pt;">KM ACTUAL</th>
						<th style="text-align: center; font-size: 8pt;">OBSERVACIONES</th>
					
					</tr>
				</thead>
				<tbody>
					<?php foreach ($modeladdmnto as $i => $addmnto) { ?>
					<?php 
        // $vehiculos = Vehiculo::find()->where(['id'=>$add['id_vehiculo']])->one();
        $modelos = Modelo::find()->where(['id_modelo'=>$addmnto['id_modelo']])->one();



					 ?>
						<tr>
							<td style="text-align: center; font-size: 7pt;"><?= $addmnto->id ?></td>
							<td style="text-align: center; font-size: 7pt;"><?= strtoupper($addmnto->ficha)?></td>
							<td style="text-align: center; font-size: 7pt;"><?= $modelos['nombre'] ?></td>
							<td style="text-align: left; font-size: 7pt;"><?= strtoupper($addmnto->km_actual)?></td>
							<td style="text-align: right; font-size: 7pt;"><?= strtoupper($addmnto->observacion)?></td>
						</tr>
						<?php 
 
					} 
					?>
				</tbody>
			</table>
		</div>
	</div>
	</div>
	<br><br>


		<div class="row">
		<div class="col-xs-12 text-center" style="font-size: 12pt;">
			<b>UNIDADES FUERA DE SERVICIO</b>
			<hr>
		</div>
	</div>

	<div class="row">
			<br><br>
	<div class="row">
		<div class="col-xs-11">
			<table class="table">
				<thead class="text-center">
					<tr>
						<th style="text-align: center; font-size: 8pt;">NÚMERO</th>
						<th style="text-align: center; font-size: 8pt;">FICHA/MVA</th>
						<th style="text-align: center; font-size: 8pt;">MODELO</th>
						<th style="text-align: center; font-size: 8pt;">KM ACTUAL</th>
						<th style="text-align: center; font-size: 8pt;">TIPO DE MANTENIMIENTO</th>
						<th style="text-align: center; font-size: 8pt;">OBSERVACIÓN</th>

					</tr>
				</thead>
				<tbody>
					<?php foreach ($modelsolaudmnto as $i => $solmnto) { ?>
		 <?php 
        $modelos = Modelo::find()->where(['id_modelo'=>$solmnto['id_modelo']])->one();

        $tipos = TipoMantenimiento::find()->where(['id_tipo_mnto'=>$solmnto['id_tipo_mnto']])->one();

		  ?>
						<tr>
							<td style="text-align: center; font-size: 7pt;"><?= $solmnto->id ?></td>
							<td style="text-align: center; font-size: 7pt;"><?= $solmnto->ficha_mva ?></td>
							<td style="text-align: center; font-size: 7pt;"><?= $modelos['nombre']?></td>
							<td style="text-align: center; font-size: 7pt;"><?= $solmnto->km_actual ?></td>
							<td style="text-align: left; font-size: 7pt;"><?= strtoupper($solmnto->id_tipo_mnto) ?></td>
							<td style="text-align: left; font-size: 7pt;"><?= $tipos['nombre'] ?></td>


						</tr>
						<?php 
 
					} 
					?>
				</tbody>
			</table>
		</div>
	</div>
	</div>
	<br><br>
		<?php } ?>

	<!-- fin del div row --> 
	<?php if ($modelejec!= ''){ ?>
		<div class="row">
		<div class="col-xs-12 text-center" style="font-size: 12pt;">
			<b>AUDITORIAS DE EJECUCIÓN DE MANTENIMIENTO</b>
			<hr>
		</div>
	</div>
	<div class="row">
		<?php 
        $tipo = TipoMantenimiento::find()->where(['id_tipo_mnto'=>$modelejec['id_tipo_mnto']])->one();

		 ?>
		<div class=" col-xs-12" style=" border-radius: 3px; padding: 5px; padding-left: 10px;">
		 
			<div style="width: 35%; float: left; ">

				<span style="font-size: 8pt;"><b>TÉCNICO</b></span><br>
				<span style="font-size: 8pt;"><?= $modelejec['tecnico']  ?></span><br>
				<br>

			</div>
			<div style="width: 30%; float: left; ">
				<span style="font-size: 8pt;"><b>MVA DEL VEHÍCULO:</b> <?= $modelejec['mva_vehiculo']  ?><br></span>
			</div>
			<div style="width: 35%; float: left; ">
				<span style="font-size: 8pt;"><b>TIPO DE MANTENIMIENTO</span></b><br>
				<span style="font-size: 8pt;"><?= $tipo['nombre'] ?></span><br>
				<br>

			</div>

		</div>
	</div> 
	<!-- fin del row -->
	<div class="row">
		<div class="col-xs-12 text-center" style="font-size: 12pt;">
			<b>ASPECTOS A EVALUAR</b>
		 
		</div>
	</div>
<!-- fin del div row -->

	<div class="row">
			<br><br>
	<div class="row">
		<div class="col-xs-11">
			<table class="table">
				<thead class="text-center">
					<tr>
						<!-- <th style="text-align: center; font-size: 8pt;">#</th> -->
						<th style="text-align: center; font-size: 8pt;">DESCRIPCIÓN</th>
						<th style="text-align: center; font-size: 8pt;">RESPUESTA</th>
					
					</tr>
				</thead>
				<tbody>
					<?php foreach ($modelaspectos as $i => $evaluar) { ?>
					<?php 
 
        $aspecto = Aspectos::find()->joinWith('aspectosEvaluars')->where(['aspectos.id'=>$evaluar['id_aspecto']])->one();



					 ?>
						<tr>
							<!-- <td style="text-align: center; font-size: 7pt;"><?= $aspecto->id ?></td> -->
							<td style="text-align: center; font-size: 7pt;"><?=  strtoupper($aspecto->descripcion) ?></td>
							<td style="text-align: center; font-size: 7pt;"><?php if($evaluar->respuesta==1){echo 'Si';}else{echo 'No';} ?></td>
 
						</tr>
						<?php 
 
					} 
					?>
				</tbody>
			</table>
		</div>
	</div>
	</div>
	<br><br>

	<?php } ?>
 
			
</div>