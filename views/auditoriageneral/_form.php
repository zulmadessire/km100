<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Auditoriageneral */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="auditoriageneral-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fecha')->textInput() ?>

    <?= $form->field($model, 'estado')->textInput() ?>

    <?= $form->field($model, 'id_estacion')->textInput() ?>

    <?= $form->field($model, 'id_aud_fs')->textInput() ?>

    <?= $form->field($model, 'id_aud_mnto')->textInput() ?>

    <?= $form->field($model, 'id_aud_ejec')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
