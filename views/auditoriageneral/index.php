<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\AuditoriageneralSearch;
use app\models\Empresa;
use app\models\Vehiculo;
use app\models\FlotaSearch;
use app\models\VehiculoSearch;
use app\models\SolicitudCompraEstacion;
use yii\widgets\ActiveForm;
use app\models\Estacion;
use app\models\Cliente;
use kartik\select2\Select2;
use yii\helpers\Url;
use kartik\depdrop\DepDrop;
use yii\helpers\ArrayHelper;
use yii\db\Query;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AuditoriageneralSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'AUDITORIAS MENSUALES PENDIENTES';
?>
<div class="auditoria-index">
         <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>
      <div class="row">
    <div class="col-md-1 col-xs-3">
      <h4 class="titulo"><i class="fa fa-list-ul fa-4x" aria-hidden="true"></i></h4>
    </div>
    <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
      <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
    </div>
  </div>
 <span>Total de estaciones sin mantenimiento: <?= $dataProvider->getTotalCount() ?></span>

 </br>
  <div class="row">
      <?php
          echo Html::a('Historico de Auditorias', ['historico'], ['class' => 'btn btn-accion', 'style' => 'float:right;']);
      ?>
  </div>
               <div class="col-md-3 col-xs-12">
            <div class="form-group">
                <label class="control-label" for="id_empresa">CLIENTE</label>
                <?=  Select2::widget([
                        'name' =>'EstacionesSearch[id_empresa]',
                        'data' => ArrayHelper::map(Empresa::find()->orderBy('nombre')->all(), 'id_empresa', 'nombre'),
                        'options' => ['placeholder' => 'Selecciona la Empresa', 'id' => 'id_empresa']
                    ]); 
                ?>
            </div>
        </div>
      
        <div class="col-md-3 col-xs-12">
            <div class="form-group">
                <label class="control-label" for="id_estado">ESTACIÓN</label>
                <?=
                    DepDrop::widget([
                        'name' => 'EstacionesSearch[id_estacion]',
                        'type'=>DepDrop::TYPE_SELECT2,
                        'options'=>['id'=>'id_estacion'],
                        'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                        'pluginOptions'=>[
                            'depends'=>['id_empresa'],
                            'placeholder'=>'Selecciona la estación',
                            'url'=>Url::to(['/vehiculo/getestaciones'])
                        ]
                    ]);
                ?>
            </div>
        </div>
 <br>
         <div class="col-md-2 col-xs-12" style="margin-top:3px">
            <div class="form-group text-center">
                <?= Html::submitButton('BUSCAR', ['class' => 'btn2']) ?>
            </div>
        </div>

  <br>
 
       <?= GridView::widget([
       'dataProvider' => $dataProvider,
                // 'filterModel'   => $searchModel,                
                'tableOptions' => ['class' => 'table table-responsive',],
                'summary' => '',
                'options' =>[
                    'class' => 'grid',
                ],
        
        'columns' => [
               [
                            'attribute' => 'idEmpresa.nombre',
                            'label' => 'Empresa',
                            'format' => 'raw',
                            // 'filter' => ArrayHelper::map(Empresa::find()->orderBy('nombre')->all(), 'id_empresa', 'nombre'),
                            'enableSorting' => false,
            
                              
              ],
             [
                            'label' => 'Agencia',
                            'attribute' =>'nombre',
                            'format' => 'raw',
                            // 'filter' => ArrayHelper::map(Estacion::find()->orderBy('nombre')->all(), 'id_estacion', 'nombre'),
                            'filter' => false,
                            'enableSorting' => false,
                 
            ],

           [
                            'label' => 'OC. Abierta',
                            'format' => 'raw',
                            'filter' => false,
                            'enableSorting' => false,
                           'value' => function ($model, $key, $index, $column) {

                            $acumulador = 0;
     
               
                              $searchModel = new VehiculoSearch();
                       $dataProvider = $searchModel->fuerasearch(Yii::$app->request->queryParams,$model->id_estacion);
                              $acumulador = $dataProvider->getTotalCount();
                              return $acumulador;
                                },
            ],
              [
                            'label' => 'Vehículos en proyección. ',
                            'format' => 'raw',
                            'filter' => false,
                            'enableSorting' => false,
                           'value' => function ($model, $key, $index, $column) {
                             $searchModel = new FlotaSearch();
                       $dataProvider = $searchModel->searchauditoria(Yii::$app->request->queryParams,$model->id_estacion);
                              $acumulador = $dataProvider->getTotalCount();
                              return $acumulador;
                                },
            ],

     

                                [  
                        'class' => 'yii\grid\ActionColumn',
                        'header'=>'Acciones',
                        'template' => '{menu}',
                        'buttons' => [

                            //menu button
                            'menu' => function ($url, $model) {
                

                                 $query2 = new query();
                                 $query2->select('es.id_estacion')
                                ->from('estacion es')
                                ->join('INNER JOIN','auditoria_fs aud','es.id_estacion = aud.id_estacion')
                                ->where( 'MONTH(aud.fecha)=MONTH(CURDATE())')
                                ->andwhere( 'YEAR(aud.fecha)=YEAR(CURDATE())')
                                ->andwhere( 'aud.estado = 1')
                                ->andwhere('es.id_estacion ='.$model->id_estacion)
                                ->andWhere('aud.mensual = 1');

                   

                                $sql = $query2->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                                $fs = Yii::$app->db->createCommand($sql)->queryOne();

                                        $query3 = new query();
                                    $query3->select('es.id_estacion')
                                    ->from('estacion es')
                                    ->join('INNER JOIN','auditoria_mnto aud','es.id_estacion = aud.id_estacion')
                                    ->where( 'MONTH(aud.fecha)=MONTH(CURDATE())')
                                    ->andwhere( 'YEAR(aud.fecha)=YEAR(CURDATE())')
                                    ->andwhere( 'aud.estado = 1')
                                    ->andwhere('es.id_estacion ='.$model->id_estacion)
                                    ->andWhere('aud.mensual = 1');

                   

                                    $sql = $query3->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                                    $mnto = Yii::$app->db->createCommand($sql)->queryOne();
                                // var_dump($mnto);
                                // print_r($mnto);
                                // die();
                                
                                $query4 = new query();
                                $query4->select('es.id_estacion')
                                ->from('estacion es')
                                ->join('INNER JOIN','auditoria_ejec_mant aud','es.id_estacion = aud.id_estacion')
                                ->where( 'MONTH(aud.fecha)=MONTH(CURDATE())')
                                ->andwhere( 'YEAR(aud.fecha)=YEAR(CURDATE())')
                                ->andwhere( 'aud.estado = 1')
                                ->andwhere('es.id_estacion ='.$model->id_estacion)
                                ->andWhere('aud.mensual = 1');


                                           
                                        $sql = $query4->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                                        $ejec = Yii::$app->db->createCommand($sql)->queryOne();


                                return  '<div class="dropdown">
                                          <button class="btn btn-default dropdown-toggle btn-sm" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i>
                                          <span class="caret"></span></button>
                                          <ul class="dropdown-menu">'.
                                            (( !$fs && !$mnto && !$ejec)?
                                                '<li>'.Html::a('<span><i class="fa fa-warning fa-sm" aria-hidden="true"></i></span>&nbsp;&nbsp;Auditoria Fuera de Servicio',[ '/auditoriafs/auditoriafs', 'id' => $model->id_estacion , 'mensual'=> '1'] ).' </li>'
                                            : '').
                                            (( !$mnto && $fs)?
                                                '<li>'.Html::a('<span><i class="fa fa-wrench fa-sm" aria-hidden="true"></i></span>&nbsp;&nbsp;Auditoria de mantenimiento',[ '/auditoriamnto/auditoriamnto', 'id' => $model->id_estacion , 'mensual'=> '1']).' </li>'
                                            : '')
                                          .(( !$ejec && $mnto)?
                                                '<li>'.Html::a('<span><i class="fa fa-check-square-o fa-sm" aria-hidden="true"></i></span>&nbsp;&nbsp;Audit. ejecución de mantenimientos',[ '/auditoriaejecmant/auditoriaejec', 'id' => $model->id_estacion , 'mensual'=> '1']).' </li>'
                                            : '')
                                          // .(( true)?
                                          //       '<li>'.Html::a('<span><i class="fa fa-list-ul fa-sm" aria-hidden="true"></i></span>&nbsp;&nbsp;Historico de auditorias',[ 'historico']).' </li>'
                                          //   : '')
                                          // .
                                          .'</ul>
                                        </div>';


                                    
                            },
                        ],

                    ],

              
        ],
    ]); ?>
        <?php ActiveForm::end(); ?>

</div>
