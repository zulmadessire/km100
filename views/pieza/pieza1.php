<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\ModeloPieza;
use app\models\TipoPieza;
use app\models\Modelo;
use app\models\Marca;
use app\models\Pieza;
use yii\helpers\Url;
use wbraganca\dynamicform\DynamicFormWidget;


/* @var $this yii\web\View */
/* @var $model app\models\Pieza */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rubro-form">
  <?php $form = ActiveForm::begin(['id' => 'dynamic_lista-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>

  <br>
 

  <div class="row">
      <div class="col-xs-12 col-md-4">
          <?php
              echo $form->field($model_tipo, "id")->dropDownList(
                   ArrayHelper::map(TipoPieza::find()->all(), 'id' , 'nombre'),
                  [
                    'prompt'=>'Selecciona tipo de pieza',
                    'class' => 'form-control',
                    'onchange' => ' 
                                        if(($(this).val() == 1)){
                                          document.getElementById("nomb").style.display="block";
                                          document.getElementById("uni").style.display="block";
                                          document.getElementById("tam").style.display="block";
                                          document.getElementById("cod").style.display="block";
                                          document.getElementById("mins").style.display="block";
                                          document.getElementById("mecanica").style.display="none";
                             
                                        }
                                        if(($(this).val() == 4)){
                                          document.getElementById("mecanica").style.display="block";
                                            document.getElementById("nomb").style.display="block";
                                          document.getElementById("uni").style.display="block";
                                          document.getElementById("tam").style.display="block";
                                          document.getElementById("cod").style.display="block";
                                          document.getElementById("mins").style.display="none";
                             
                                        }
                                          if(($(this).val() == 5)){
                                          document.getElementById("mecanica").style.display="block";
                                            document.getElementById("nomb").style.display="block";
                                          document.getElementById("uni").style.display="block";
                                          document.getElementById("tam").style.display="block";
                                          document.getElementById("cod").style.display="block";
                                          document.getElementById("mins").style.display="none";
                               
                                        }',
                  ]
                  );
          ?>
      </div>
  </div>



<!--______________Esta parte es para insumos______________ -->


            <div class="row">
  <div class="nomb" id = "nomb" style="display: none;">
                <div class="col-xs-12 col-md-3">
                      <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
                </div>
  </div>
    <div class="cod" id = "cod" style="display: none;">
                <div class="col-xs-12 col-md-2">
                      <?= $form->field($model, 'codigo')->textInput(['maxlength' => true]) ?>
                </div>
  </div>
  <div class="uni" id = "uni" style="display: none;">
                <div class="col-xs-12 col-md-2">
                      <?php
                          $unidades = [0 => 'Unidades', 1 => 'Litros'];
                          echo $form->field($model, "unidad")->dropDownList(
                              $unidades,
                              [
                                  'prompt'=>'Selecciona la unidad',
                                  'class' => 'form-control',
                              ]
                              );
                      ?>
                </div>
  </div>
  <div class="tam" id = "tam" style="display: none;">
                <div class="col-xs-12 col-md-2">
                      <?= $form->field($model, 'tamano')->textInput(['maxlength' => true]) ?>
                </div>
  </div>
  <div class="mins" id = "mins" style="display: none;">
                <div class="col-xs-12 col-md-3">
                      <?= $form->field($model, 'marca_insumo')->textInput(['maxlength' => true]) ?>
                </div>
  </div>
            </div>
  

                      <br>

    <div class="mecanica" id = "mecanica" style="display: none;">
                    <?php DynamicFormWidget::begin([
                          'widgetContainer' => 'dynamicform_wrapper_resultados',
                          'widgetBody' => '.container-items',
                          'widgetItem' => '.item',
                          'limit' => 50,
                          'min' => 1,
                          'insertButton' => '.add-item',
                          'deleteButton' => '.remove-item',
                          'model' => $modelpieza[0],
                          'formId' => 'dynamic_lista-form',
                          'formFields' => [
                              'id_marca_rubro',
                              'id_modelo_rubro',
                          ],
                    ]); ?>
                    <table class="table table-hover">
                        <tbody class="container-items">
                        <?php foreach ($modelpieza as $i => $modelrub): ?>
                            <tr class="item panel panel-default">
                                <td>
                     
                                    <div class="col-md-3">
                                             <?php 
                                  $modelosList = [];
                                $marcas = ArrayHelper::map(Marca::find()->orderBy('nombre')->all(), 'id_marca', 'nombre');

                                echo  $form->field($modelrub,"[{$i}]marca")->dropDownList(
                                    $marcas,
                                    [
                                        'prompt'=>'Seleccione...',
                                        'class' => 'form-control marca','id' => 'marca',
                                        'onchange'=>'
                                                var object = $(this);
                                                    $.get( "'.Url::toRoute('proveedorpieza/listarmodelos').'", { id: $(this).val() } )

                                                        .done(function( data ) {
                                                           

                                                         object.parent().parent().parent().find("select.modelos").html( null );
                                                         object.parent().parent().parent().find("select.modelos").html( data );
                                                        }
                                                    );
                                                ',

                                        ]
                                    ); ?>

                                    </div>
                                     <div class="col-md-3">
                                                
                                             
                                                    <?= $form->field($modelrub,"[{$i}]modelo_id_modelo")->dropDownList(
                                                    $modelosList,
                                                    [
                                                        'prompt'=>'Seleccione...',
                                                        'class' => 'form-control modelos','id' => 'field_modelo_id']
                                                    )
                                                ?>


                                    </div>

                                    <div class="col-md-3">

                                               <?php 
                          $anhos = ['1980'=>'1980', '1981'=>'1981','1982'=>'1982','1983'=>'1983',
                                                     '1984'=>'1984', '1985'=>'1985','1986'=>'1986','1987'=>'1987',
                                                     '1988'=>'1988', '1989'=>'1989','1990'=>'1990','1991'=>'1991',
                                                     '1992'=>'1992', '1993'=>'1993','1994'=>'1994','1995'=>'1995',
                                                     '1996'=>'1996', '1997'=>'1997','1998'=>'1998','1999'=>'1999',
                                                     '2000'=>'2000', '2001'=>'2001','2002'=>'2002','2003'=>'2003',
                                                     '2004'=>'2004', '2005'=>'2005','2006'=>'2006','2007'=>'2007',
                                                     '2008'=>'2008', '2009'=>'2009','2010'=>'2010','2011'=>'2011',
                                                     '2012'=>'2012', '2013'=>'2013','2014'=>'2014','2015'=>'2015',
                                                     '2016'=>'2016', '2017'=>'2017','2018'=>'2018','2019'=>'2019',
                                                      
                                                    ];
                                    echo $form->field($modelrub, "[{$i}]anho")->dropDownList(
                                            $anhos,
                                            [
                                                'prompt'=>'Seleccione...',
                                                'class' => 'form-control',
                                            ] );
                               

                                    ?>

                                    </div>  
                                  <div class="col-xs-12 col-md-1 text-center">
                                      <button type="button" class="remove-item btn btn-cancelar btn-xs" style="margin-top:28px;">
                                          <i class="glyphicon glyphicon-minus"></i></button>
                                  </div>
                              </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php DynamicFormWidget::end(); ?>
                        </tbody>
                    </table>

    <button type="button" class="add-item btn btn-accion  btn-xs pull-right">Agregar vehiculo</button>
  </div>  
</div>

 
<br>
<br>





<br>
<br>
  <?php
      if(!$model->isNewRecord){
      ?>
          <div class="row" align="center">
              <div>
                  <?= Html::a('CANCELAR', ['view', 'id' => $model->id_rubro], ['class' => 'btn btn-cancelar']) ?>
                  &nbsp;&nbsp;&nbsp;
                  <?= Html::submitButton('GUARDAR CAMBIOS', ['class' => 'btn btn-primario']) ?>
              </div>
          </div>
      <?php
      }else{
      ?>
          <div class="form-group" align="center">
              <?= Html::submitButton('GUARDAR', ['class' => 'btn btn-primario']) ?>
          </div>
      <?php
      }
  ?>
<?php ActiveForm::end(); ?>
</div>
