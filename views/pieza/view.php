<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Pieza */

$this->title = $model->id_pieza;
$this->params['breadcrumbs'][] = ['label' => 'Piezas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pieza-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_pieza], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_pieza], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_pieza',
            'nombre',
            'tamano',
            'unidad',
            'tipo_pieza_id',
            'modelo_id_modelo',
            'anho',
        ],
    ]) ?>

</div>
