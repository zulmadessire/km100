<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\ModeloPieza;
use app\models\TipoPieza;
use app\models\Modelo;
use app\models\Marca;
use app\models\Pieza;
use yii\helpers\Url;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\select2\Select2;


/* @var $this yii\web\View */
/* @var $model app\models\Pieza */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rubro-form">
    <?php $form = ActiveForm::begin(['id' => 'dynamic_lista-form', 'options' => ['enctype' => 'multipart/form-data'], ]); ?>

    <div class="row">
        <?php if( $model->isNewRecord ){ ?>
            <div class="col-md-12 col-xs-12">
                <?php DynamicFormWidget::begin([
                    'widgetContainer' => 'dynamicform_wrapper_resultados',
                    'widgetBody' => '.container-items',
                    'widgetItem' => '.item',
                    'limit' => 50,
                    'min' => 1,
                    'insertButton' => '.add-item',
                    'deleteButton' => '.remove-item',
                    'model' => $model_pieza[0],
                    'formId' => 'dynamic_lista-form',
                    'formFields' => [
                        'nombre',
                        'tipo_pieza_id',
                        'unidad',
                    ],
                ]); ?>

     
                <table class="table table-hover">
                    <tbody class="container-items">
                        <?php foreach ($model_pieza as $i => $pieza): ?>
                            <tr class="item panel panel-default">
                                <td>
                                 <div class="col-xs-12 col-md-3"><?php
                                    $piezas = array();
                                    $tiposdepieza = array();
                                    if(!$pieza->isNewRecord){
                                        $piezas = ArrayHelper::map(Pieza::find()->where(['id_pieza'=>$pieza->id_pieza])->orderBy('nombre')->all(), 'id_pieza', 'nombre');
                                        //$tiposdepieza = ArrayHelper::map(TipoPieza::find()->where([ 'id' => $piezas->tipo_pieza_id ])->orderBy('nombre')->all(), 'id', 'nombre');

                                    }
                                    $tiposdepieza = ArrayHelper::map(TipoPieza::find()->orderBy('nombre')->all(), 'id', 'nombre');
                                    echo $form->field($pieza, "[{$i}]tipo_pieza_id", ['enableAjaxValidation' => true ])->dropDownList(
                                    $tiposdepieza,
                                    [
                                        'prompt'=>'Selecciona el tipo',
                                        'class' => 'form-control',
                                        'options' => [
                                            (!$pieza->isNewRecord)?Pieza::findOne($pieza->id_pieza)->tipo_pieza_id:'' => [ 'selected' => true ],
                                        ],
                                    ] )->label('TIPO'); ?>
                                </div>
                                <div class="col-xs-12 col-md-3">
                                    <?php
                                        echo $form->field($pieza, "[{$i}]nombre", ['enableAjaxValidation' => true])->textInput(['placeholder'=>'Nombre',])->label('NOMBRE');
                                    ?>
                                </div>
                                <div class="col-xs-12 col-md-3">
                                    <?php
                                        $unidades = [0 => 'Unidad', 1 => 'Litros'];
                                        echo $form->field($pieza, "[{$i}]unidad")->dropDownList(
                                            $unidades,
                                            [
                                                'prompt'=>'Selecciona la unidad',
                                                'class' => 'form-control',
                                            ]
                                        );
                                    ?>
                                </div>
                                    <div class="col-xs-12 col-md-1 text-center">
                                            <button type="button" class="remove-item btn btn-cancelar btn-xs" style="margin-top:28px;">
                                            <i class="glyphicon glyphicon-minus"></i></button>
                                    </div>
                              </td>
                            </tr>
                        <?php endforeach; ?>
                        
                    </tbody>
                </table>
                <button type="button" class="add-item btn btn-accion  btn-xs pull-right">Agregar Pieza</button>
                <?php DynamicFormWidget::end(); ?> 
            </div>
        <?php } else{ ?>
            <div class="col-md-4 col-xs-12">
                <?=
                    $form->field($model, 'tipo_pieza_id')->widget(Select2::classname(), [
                        'data' => $model_tipo,
                        'options' => ['placeholder' => 'Selecciona el tipo de pieza',],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ])->label('TIPO <span class="obligatorio">*&nbsp;</span>')
                ?>
            </div>
            <div class="col-md-4 col-xs-12">
                <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4 col-xs-12">
                <?=
                    $form->field($model, 'unidad')->widget(Select2::classname(), [
                        'data' =>  [0 => 'Unidad', 1 => 'Litros'],
                        'options' => ['placeholder' => 'Selecciona la unidad',],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ])->label('UNIDAD')
                ?>
            </div>
        <?php } ?>
    </div>
    <div class="clearfix"></div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <?php if(!$model->isNewRecord){ ?>
        <div class="row" align="center">
            <div>
                <?= Html::a('CANCELAR', ['index'], ['class' => 'btn btn-cancelar']) ?>
                &nbsp;&nbsp;&nbsp;
                <?= Html::submitButton('GUARDAR CAMBIOS', ['class' => 'btn btn-primario']) ?>
            </div>
        </div>
    <?php } else{ ?>
        <div class="form-group" align="center">
            <?= Html::submitButton('GUARDAR', ['class' => 'btn btn-primario']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>
</div>
