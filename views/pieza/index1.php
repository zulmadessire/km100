<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\RubroModeloAsignado;
use app\models\Modelo;
use app\models\MarcaRubro;
use app\models\ModeloPieza;
use app\models\PiezasSearch;
use yii\helpers\ArrayHelper;
use kartik\form\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RubroSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'LISTADO DE MATERIALES';
?>
<div class="rubro-index">

    <div class="row">
      <div class="col-md-1 col-xs-3">
        <h4 class="titulo"><i class="fa fa-tint fa-4x" aria-hidden="true"></i></h4>
      </div>
      <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
        <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
      </div>
    </div>
    </br>
    <div class="row">
        <?php
            echo Html::a('Registrar Material', ['create'], ['class' => 'btn btn-accion', 'style' => 'float:right;']);
        ?>
    </div>
     <br>
    <div class="table-responsive" style="padding-bottom:80px;">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table table-responsive'],
        'summary' => '',
        'options' =>[
                        'class' => 'grid',
                    ],
        'columns' => [
                  [
            
                'label' => 'Código',
                'format' => 'text',
                'filter' => true,
                'enableSorting' => true,
                'value' => function ($model) {
                        return ($model->codigo) ? $model->codigo : "-";
                },
 
            ],

                [
                'attribute' => 'nombre',
                'label' => 'Material',
                'value' => 'nombre',
                'format' => 'text',
                'filter' => true,
                'enableSorting' => true,
              ],
                  [


                'attribute' => 'tamano',
                'label' => 'Tamaño',
                'value' => 'nombre',
                'format' => 'text',
                'filter' => true,
                'enableSorting' => true,
                'value' => function ($model) {
                              return ($model->tamano) ? $model->tamano : "-";
                            },
            ],
                    [
               'attribute' => 'unidad',
                'label' => 'Unidad de medición',
                'format' => 'text',
                'filter' => ['0'=>'Unidades','1'=>'Litros'],
                'enableSorting' => true,
                'value' => function ($model) {
                              return ($model->unidad==0) ? "Unidades" : "Litros";
                            },
            ],
            [
                'attribute' => 'tipo_pieza_id',
                'label' => 'Tipo de Pieza',
                'format' => 'text',
                'filter' => ['1'=>'Insumos','4'=>'Mecánica','5'=>'Carrocería'],
                'enableSorting' => true,
                'value' => function ($model) {
                              return ($model->tipoPieza->nombre);
                            },
            ],
             [
                'label' => 'Marca -> Modelo',
                'format' => 'html',
                'value' => function ($model) {

                            $modelos = '';
                            $rma = ModeloPieza::find()->where(['pieza_id' => $model->id_pieza])->All();
                            foreach ($rma as $r) {
                             $modelos = $modelos.'<br>'.$r->modeloIdModelo->nombre.'->'.$r->modeloIdModelo->idMarca->nombre;
                            }
                              return ($modelos) ? $modelos : "-";
                },
                 'filter' => Select2::widget([
                    'name' => 'PiezasSearch[id_modelo]',
                    'data' => ArrayHelper::map(Modelo::find()->orderBy('nombre')->all(), 'id_modelo', 'nombre'),
                    'hideSearch' => true,
                    'options' => ['placeholder' => 'Modelo',],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                     ]),
            
            ],
         

       

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
