<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\RubroModeloAsignado;
use app\models\Modelo;
use app\models\Marca;
use app\models\MarcaRubro;
use app\models\ModeloPieza;
use app\models\PiezasSearch;
use yii\helpers\ArrayHelper;
use kartik\form\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RubroSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$dataProvider->pagination = ['pageSize'=>30];

$this->title = 'LISTADO DE PIEZAS';
?>
<div class="rubro-index">

    <div class="row">
      <div class="col-md-1 col-xs-3">
        <h4 class="titulo"><i class="fa fa-tint fa-4x" aria-hidden="true"></i></h4>
      </div>
      <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
        <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
      </div>
    </div>
    </br>
    <div class="row">
        <div class="col-md-12">
            <?php
                echo Html::a('Registrar Pieza', ['create'], ['class' => 'btn btn-small-gris', 'style' => 'float:right;']);
            ?>
        </div>
    </div>
    <br>
    <br>
    <div class="row">
        <div class="col-md-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => ['class' => 'table table-responsive'],
                'summary' => '',
                'options' =>[
                                'class' => 'grid',
                            ],
                'columns' => [
                    [
                    
                        'label' => 'Código',
                        'format' => 'text',
                        'filter' => true,
                        'enableSorting' => false,
                        'value' => function ($model) {
                                return ($model->codigo) ? $model->codigo : "-";
                        },
                    ],
                    [
                        'attribute' => 'nombre',
                        'label' => 'Pieza',
                        'value' => 'nombre',
                        'format' => 'text',
                        'filter' => true,
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'tamano',
                        'label' => 'Tamaño',
                        'value' => function ($model) {

         
                               
                                      return ($model->tamano) ? $model->tamano: "-";
                        },
                        'format' => 'text',
                        'filter' => true,
                        'enableSorting' => false,
                    ],
                    [
                       'attribute' => 'unidad',
                        'label' => 'Unidad de medición',
                        'format' => 'text',
                        'filter' => ['0'=>'Unidades','1'=>'Litros'],
                        'enableSorting' => true,
                        'value' => function ($model) {
                                      return ($model->unidad==0) ? "Unidades" : "Litros";
                                    },
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'tipo_pieza_id',
                        'label' => 'Tipo de Pieza',
                        'format' => 'text',
                        'filter' => ['1'=>'Insumos','4'=>'Mecánica','5'=>'Carrocería'],
                        'enableSorting' => false,
                        'value' => function ($model) {
                                      return ($model->tipoPieza->nombre);
                                    },
                    ],
                    [
                       
                        'label' => ' Modelo',
                        'format' => 'html',
                        'value' => function ($model) {

         
                               
                                      return ($model->modeloIdModelo) ? $model->modeloIdModelo->idMarca->nombre.' -> '.$model->modeloIdModelo->nombre: "-";
                        },
                             'filter' => Select2::widget([
                            'name' => 'PiezasSearch[id_modelo]',
                            // 'data' => ArrayHelper::map(Marca::find()->joinWith('Modelo')->orderBy('nombre')->all(), 'id_marca', 'nombre'),
                            'data' => ArrayHelper::map(Modelo::find()->orderBy('nombre')->all(), 'id_modelo', 'nombre'),
                            'hideSearch' => true,
                            'options' => ['placeholder' => 'Modelos',],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]),
                        'enableSorting' => false,
                         
                    
                    ],
                 
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header'=>'Acciones',
                        'template' => '{menu}',
                        'buttons' => [
                            'menu' => function ($url, $model) {
                                 return  '<div class="dropdown">
                                          <button class="btn btn-default dropdown-toggle btn-sm" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i>
                                          <span class="caret"></span></button>
                                          <ul class="dropdown-menu">
         
                                            <li>'.Html::a('<span><i class="fa fa-pencil fa-sm" aria-hidden="true"></i></span> Modificar datos',[ 'pieza/update', 'id' => $model->id_pieza]).' </li>
         
                                          </ul>
                                        </div>';


                            },
                        ],
                   ],
               

                     
                ],
            ]); ?>
        </div>
    </div>

</div>
