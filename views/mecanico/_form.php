<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

?>

<div class="mecanico-form">

    <?php $form = ActiveForm::begin(); ?>

        <div class="row">
   
    	<div class="col-md-4">
    		<?= $form->field($model, 'nombre')->textInput(['maxlength' => true])->label('NOMBRE <span class="obligatorio">*&nbsp;</span>');  ?>
    	</div>
    	<div class="col-md-4">
    		<?= $form->field($model, 'cedula')->textInput(['maxlength' => true])->label('CÉDULA <span class="obligatorio">*&nbsp;</span>');  ?>
    	</div>
    	<div class="col-md-4">
    		<?= $form->field($model, 'telefono')->textInput(['maxlength' => true])->label('TELÉFONO');  ?>
    	</div>
    </div>
    <div class="row">
    	<div class="col-md-12">
    		<?= $form->field($model, 'direccion')->textInput(['maxlength' => true])->label('DIRECCIÓN'); ?>
    	</div>
    </div>


 
    
<?php
      if(!$model->isNewRecord){
      ?>
          <div class="row" align="center">
              <div>
                  <?= Html::a('CANCELAR', ['index'], ['class' => 'btn btn-cancelar']) ?>
                  &nbsp;&nbsp;&nbsp;
                  <?= Html::submitButton('GUARDAR CAMBIOS', ['class' => 'btn btn-primario']) ?>
              </div>
          </div>
      <?php
      }else{
      ?>
          <div class="form-group" align="center">
              <?= Html::submitButton('GUARDAR', ['class' => 'btn btn-primario']) ?>
          </div>
      <?php
      }
  ?>

    <?php ActiveForm::end(); ?>

</div>



 
 