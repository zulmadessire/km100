<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MecanicosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'LISTADO DE MECÁNICOS';
?>
<div class="mecanico-index">
    <div class="row">
        <div class="col-md-1 col-xs-3">
            <h4 class="titulo"><i class="fa fa-user-circle fa-4x" aria-hidden="true"></i></h4>
        </div>
        <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
            <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                <?= Html::a('Registrar Mecánico',['mecanico/create'], ['class' => 'btn btn-small-gris']) ?>
                
            </div>
        </div>
        </br>
        </br>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => ['class' => 'table table-responsive'],
                'summary' => '',
                'options' =>[
                    'class' => 'grid',
                ],
                'columns' => [
                    [
                        'attribute' => 'nombre',
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'cedula',
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'telefono',
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'direccion',
                        'enableSorting' => false,
                    ],                 
                    [  
                        'class' => 'yii\grid\ActionColumn',
                        //'contentOptions' => ['style' => 'width:260px;'],
                        'header'=>'Acciones',
                        'template' => '{menu}',
                        'buttons' => [

                            //view button
                            'menu' => function ($url, $model) {

                                 return  '<div class="dropdown">
                                          <button class="btn btn-default dropdown-toggle btn-sm" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i>
                                          <span class="caret"></span></button>
                                          <ul class="dropdown-menu">
                                            
                                            <li>'.Html::a('<span><i class="fa fa-pencil fa-sm" aria-hidden="true"></i></span> Modificar Datos',[ '/mecanico/update', 'id' => $model->id ]).' </li>
                                            
                                          </ul>
                                        </div>';


                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>

</div>
    