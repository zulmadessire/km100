<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Empresa;
use app\models\Pieza;
use app\models\Marca;
use app\models\TipoEmpresa;
use app\models\ProveedorPieza;
use app\models\TipoPieza;
use app\models\MarcaInsumo;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProveedorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'LISTADO DE CONCESIONARIOS';

//$this->params['breadcrumbs'][] = $this->title;
?>

<div class="concesionario-index">

     
    <div class="row">
        <div class="col-md-1 col-xs-3">
            <h4 class="titulo"><i class="fa fa-building fa-4x" aria-hidden="true"></i></h4>
        </div>
        <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
            <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
        </div>
    </div>
 

    <div class="row">

        <div class="col-md-12 col-xs-12">
            <div class="pull-right">
                <?= Html::a('Registrar Concesionario',['concesionario/create'], ['class' => 'btn btn-small-gris']) ?>
            </div>
        </div>
        </br>

        </br>
        <div class="col-md-12 col-xs-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => ['class' => 'table table-responsive'],
                'summary' => '',
                'options' =>[
                    'class' => 'grid',
                ],
                'columns' => [
                    [
                        'attribute' => 'nombre',
                        'label' => 'Nombre',
                        'format' => 'text',
                        'filter' => true,
                        'enableSorting' => false,
                        'value' => function( $model ){
                            return $model->nombre;
                        }
                        //'headerOptions' => ['width' => '300',],
                    ],
                    [
                        'label' => 'Sucursales',
                        'format' => 'raw',
                        'filter' => false,
                        'enableSorting' => false,
                        'value' => function ($model, $key, $index, $column) {
                            $piezas = "";
                           
                            foreach ($model->sucursalConcesionarios as $value) {
                         
                                $piezas = $model->nombre ."-".$value->nombre."<br>".$piezas ;
                          
                            }
                            return $piezas;
                        },
                    ],
                    [  
                        'class' => 'yii\grid\ActionColumn',
                        //'contentOptions' => ['style' => 'width:260px;'],
                        'header'=>'Acciones',
                        'template' => '{menu}',
                        'buttons' => [

                            //view button
                            'menu' => function ($url, $model) {

                                 return  '<div class="dropdown">
                                          <button class="btn btn-default dropdown-toggle btn-sm" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i>
                                          <span class="caret"></span></button>
                                          <ul class="dropdown-menu">
                                            <li>'.Html::a('<span><i class="fa fa-eye fa-sm" aria-hidden="true"></i></span> Ver Detalles',[ '/concesionario/view', 'id' => $model->id ]).' </li>
                                            <li>'.Html::a('<span><i class="fa fa-pencil fa-sm" aria-hidden="true"></i></span> Modificar Datos',[ '/concesionario/update', 'id' => $model->id ]).' </li>
                                            
                                          </ul>
                                        </div>';


                            },
                        ],

                    ],
                ],
            ]); ?>

        </div>
    </div>
</div>


 
    