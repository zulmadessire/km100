<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Concesionario */

$this->title = 'REGISTRO DE CONCESIONARIO';

?>
<div class="concesionario-create">

  
    
    <div class="row">
        <div class="col-md-1 col-xs-3">
            <h4 class="titulo"><i class="fa fa-building fa-4x" aria-hidden="true"></i></h4>
        </div>
        <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
            <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
        </div>
    </div>
    <br>

    <?= $this->render('_form', [
        'model' => $model,
        'model_partes' => $model_partes,
        'model_sucursal' => $model_sucursal,
        'model_detalle_garantia' => $model_detalle_garantia,
        'model_partes_garantia' => $model_partes_garantia,
        'marcas' => $marcas,
        'partes' => $partes,
    ]) ?>

</div>
 