 

 <?php

use yii\helpers\Html;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\helpers\ArrayHelper;
use app\models\Marca;
use app\models\Partes;
use app\models\Modelo;
use app\models\PartesGarantia;
use yii\helpers\Url;

?>

<?php DynamicFormWidget::begin([
    'widgetContainer' => 'dynamicform_inner',
    'widgetBody' => '.container-rooms',
    'widgetItem' => '.room-item',
    'limit' => 4,
    'min' => 1,
    'insertButton' => '.add-room',
    'deleteButton' => '.remove-room',
    'model' => $modelsRoom[0],
    'formId' => 'dynamic-form',
    'formFields' => [
        'description'
    ],
]); ?>
<table class="table table-bordered">
    <thead>
        <tr>
            <th>MARCAS Y MODELOS</th>
            <th class="text-center">
                <button type="button" class="add-room btn btn-success btn-xs"><span class="glyphicon glyphicon-plus"></span></button>
            </th>
        </tr>
    </thead>
    <tbody class="container-rooms">
    <?php foreach ($modelsRoom as $indexRoom => $modelRoom): ?>
   
        <tr class="room-item">
            <td class="vcenter">



                        <?php
                    // necessary for update action.
                $modelosList = [];
                    if (! $modelRoom->isNewRecord) {
                        echo Html::activeHiddenInput($modelRoom, "[{$indexHouse}][{$indexRoom}]id");
                        $modelRoom->marca = $modelRoom->modeloIdModelo->id_marca;
                        $modelosList = ArrayHelper::map(Modelo::find()->orderBy('nombre')->where(['id_marca'=>$modelRoom->marca])->all(), 'id_modelo', 'nombre');
                     
                    }
                ?>


                  <div class="col-md-6">

  


<?php 
$marcas = ArrayHelper::map(Marca::find()->orderBy('nombre')->all(), 'id_marca', 'nombre');



    echo  $form->field($modelRoom,"[{$indexHouse}][{$indexRoom}]marca")->dropDownList(
                $marcas,
                [
                    'prompt'=>'Seleccione Marca',
                    'class' => 'form-control marca','id' => 'marca',
                    'onchange'=>'
                            var object = $(this);
                                $.get( "'.Url::toRoute('proveedorpieza/listarmodelos').'", { id: $(this).val() } )

                                    .done(function( data ) {
                                       

                                     object.parent().parent().parent().find("select.modelos").html( null );
                                     object.parent().parent().parent().find("select.modelos").html( data );
                                    }
                                );
                            ',

                    ]
                )->label(false); ?>

 
</div>

 <div class="col-md-6">
            
         
                <?= $form->field($modelRoom,"[{$indexHouse}][{$indexRoom}]modelo_id_modelo")->dropDownList(
                $modelosList,
                [
                    'prompt'=>'Seleccione Modelo',
                    'class' => 'form-control modelos','id' => 'field_modelo_id']
                )->label(false);
            ?>


</div>
 
         <div class="col-md-4">
       <!-- <label class="control-label" style="align:right;"> Descripción</label>  -->
         </div>
         <div class="col-md-4">
        <label class="control-label" style="align:right;"> Tiempo</label> 
         </div>
         <div class="col-md-4">
         <label class="control-label" style="align:right;"> Kilometraje</label> 
         </div>
        <br>

           <?php
                    // necessary for update action.
                    if (! $modelRoom->isNewRecord) {
                        echo Html::activeHiddenInput($modelRoom, "[{$indexHouse}][{$indexRoom}]id");
                                $modelsRoom2 = PartesGarantia::find()
                                ->where([ 'partes_garantia.detalle_garantia_id' => $modelRoom->id])
                                ->all();

                        $i=0;
//print_r($modelRoom->partesGarantias);
//die();
                       foreach ($modelRoom->partesGarantias as $modelRoom2) {
                        ?>

                        
                        <div class="col-md-4">
                          <?php echo $modelRoom2->partes->nombre ;

                          ?>
                           <div class="col-md-4">
                        <?= $form->field($modelRoom2, "[{$indexHouse}][{$indexRoom}]id[$i]")->hiddenInput(['value'=>$modelRoom2->id])->label(false);?>

                           </div>

                           </div>
                         <div class="col-md-4">
                        <?= $form->field($modelRoom2, "[{$indexHouse}][{$indexRoom}]tiempo[$i]")->label(false)->textInput(['maxlength' => true, 'placeholder'=>'Tiempo', 'value'=>$modelRoom2->tiempo]);?>
                          </div>
                          <div class="col-md-4">

                        <?= $form->field($modelRoom2, "[{$indexHouse}][{$indexRoom}]kilometraje[$i]")->label(false)->textInput(['maxlength' => true, 'placeholder'=>'Tiempo','value'=>$modelRoom2->kilometraje]);?>
                        </div>
                        <?php
                        $i++;
                     
                       }
                    
                     
                    }else{
                ?>

<?php 

$partes = ArrayHelper::map(Partes::find()->all(), 'id', 'nombre');
 
 $piezas[] = 0;
 $i=0;
 foreach ($modelsRoom2 as $indexRoom2 => $modelRoom2){
 
   foreach ($partes as $key => $value) {?>
 

        <div class="col-md-4">
            <label class="control-label"><?php echo $value; ?></label> 
 
        </div>
         <div class="col-md-4">
         <?= $form->field($modelRoom2, "[{$indexHouse}][{$indexRoom2}]tiempo[$i]")->label(false)->textInput(['maxlength' => true, 'placeholder'=>'Tiempo']) ?>
         </div>
         <div class="col-md-4">
         <?= $form->field($modelRoom2, "[{$indexHouse}][{$indexRoom2}]kilometraje[$i]")->label(false)->textInput(['maxlength' => true,'placeholder'=>'Kilometraje']) ?>
         </div>
       
        <?php 
 $i++;
    }
} 
}

 ?>



            </td>
            <td class="text-center vcenter" style="width: 90px;">
                <button type="button" class="remove-room btn btn-danger btn-xs"><span class="glyphicon glyphicon-minus"></span></button>
            </td>
        </tr>
     <?php endforeach; ?>
    </tbody>
</table>
<?php DynamicFormWidget::end(); ?>