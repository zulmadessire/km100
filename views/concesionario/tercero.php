 

 <?php

use yii\helpers\Html;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\helpers\ArrayHelper;
use app\models\Marca;
use app\models\Partes;
use app\models\Modelo;
use app\models\PartesGarantia;
use yii\helpers\Url;

?>

<?php DynamicFormWidget::begin([
    'widgetContainer' => 'dynamicform_inner',
    'widgetBody' => '.container-rooms',
    'widgetItem' => '.room-item',
    'limit' => 4,
    'min' => 1,
    'insertButton' => '.add-room',
    'deleteButton' => '.remove-room',
    'model' => $modelsRoom2[0], 
    'formId' => 'dynamic-form',
    'formFields' => [
        'description'
    ],
]); ?>
<table class="table table-bordered">
    <thead>
        <tr>
            <th>MARCAS Y MODELOS</th>
            <th class="text-center">
                <button type="button" class="add-room btn btn-success btn-xs"><span class="glyphicon glyphicon-plus"></span></button>
            </th>
        </tr>
    </thead>
    <tbody class="container-rooms">
    <?php foreach ($modelsRoom2 as $indexRoom2 => $modelRoom2): ?>
   
        <tr class="room-item">
            <td class="vcenter">
                <?php
                    // necessary for update action.
                    if (! $modelRoom2->isNewRecord) {
                        echo Html::activeHiddenInput($modelRoom2, "[{$indexHouse}][{$indexRoom}]id");
                    }
                ?>


 
         <div class="col-md-4">
       <!-- <label class="control-label" style="align:right;"> Descripción</label>  -->
         </div>
         <div class="col-md-4">
        <label class="control-label" style="align:right;"> Tiempo</label> 
         </div>
         <div class="col-md-4">
         <label class="control-label" style="align:right;"> Kilometraje</label> 
         </div>
        <br>
<?php 

$partes = ArrayHelper::map(Partes::find()->orderBy('nombre')->all(), 'id', 'nombre');
 
 $piezas[] = 0;
 $i=0;
 
   foreach ($partes as $key => $value) {
 ?>
 

        <div class="col-md-4">
            <label class="control-label"><?php echo $value; ?></label> 
 
        </div>
         <div class="col-md-4">
         <?= $form->field($modelRoom2, "[{$indexRoom}][{$indexRoom2}]tiempo[]")->label(false)->textInput(['maxlength' => true, 'placeholder'=>'Tiempo']) ?>
         </div>
         <div class="col-md-4">
         <?= $form->field($modelRoom2, "[{$indexRoom}][{$indexRoom2}]kilometraje[]")->label(false)->textInput(['maxlength' => true,'placeholder'=>'Kilometraje']) ?>
         </div>
       
        <?php 
 $i++;
    }

 ?>



            </td>
            <td class="text-center vcenter" style="width: 90px;">
                <button type="button" class="remove-room btn btn-danger btn-xs"><span class="glyphicon glyphicon-minus"></span></button>
            </td>
        </tr>
     <?php endforeach; ?>
    </tbody>
</table>
<?php DynamicFormWidget::end(); ?>