<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Model;
use kolyunya\yii2\widgets\MapInputWidget;

$this->title = 'MODIFICAR CONCESIONARIO';
$periodos = [ 0 => 'Días', 1 => 'Meses', 2 => 'Años' ];
?>

<div class="concesionario-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

    <div class="row">
        <div class="col-xs-12 col-xs-12">
            <div class="pull-right" style="margin-top: 30px;">
                <?= Html::a('Listado de Concesionarios',['index'], ['class' => 'btn btn-small-gris']) ?> 
            </div>
        </div>
        </br>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DEL CONCESIONARIO</p>
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'nombre')->textInput(['maxlength' => true])->label('NOMBRE <span class="obligatorio">*&nbsp;</span>');  ?>
        </div> 
    </div>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">SUCURSALES</p>
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-xs-12">
            <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_sucursales',
                'widgetBody' => '.container-sucursales',
                'widgetItem' => '.sucursal',
                'limit' => 50,
                'min' => 1,
                'insertButton' => '.add-sucursal',
                'deleteButton' => '.remove-sucursal',
                'model' => $model_sucursal[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'nombre',
                    'email',
                    'telefono',
                    'nombre_asesor',
                    'apellido_asesor',
                    'email_asesor',
                    'telefono_asesor',
                    'direccion',
                    'coord_google_maps',
                ],
            ]); ?>

     
                <table class="table table-hover">
                    <tbody class="container-sucursales">
                        <?php foreach ($model_sucursal as $i => $sucursal): ?>
                            <tr class="sucursal panel panel-default">
                                <td>
                                    <div class="col-xs-12 col-md-4">
                                        <?= $form->field($sucursal, "[{$i}]nombre")->textInput(['placeholder'=>'Nombre',])->label('NOMBRE SUCURSAL'); ?>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <?= $form->field($sucursal, "[{$i}]email")->textInput(['placeholder'=>'Correo',])->label('CORREO'); ?>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <?= $form->field($sucursal, "[{$i}]telefono")->textInput(['placeholder'=>'Teléfono',])->label('TELÉFONO'); ?>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-xs-12 col-md-4">
                                        <?= $form->field($sucursal, "[{$i}]nombre_asesor")->textInput(['placeholder'=>'Nombre asesor',])->label('NOMBRE ASESOR'); ?>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <?= $form->field($sucursal, "[{$i}]apellido_asesor")->textInput(['placeholder'=>'Apellido asesor',])->label('APELLIDO ASESOR'); ?>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <?= $form->field($sucursal, "[{$i}]email_asesor")->textInput(['placeholder'=>'Correo asesor',])->label('CORREO ASESOR'); ?>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-xs-12 col-md-4">
                                        <?= $form->field($sucursal, "[{$i}]telefono_asesor")->textInput(['placeholder'=>'Teléfono asesor',])->label('TELÉFONO ASESOR'); ?>
                                    </div>
                                    <div class="col-xs-12 col-md-7">
                                        <?= $form->field($sucursal, "[{$i}]direccion")->textInput(['placeholder'=>'Dirección',])->label('DIRECCIÓN'); ?>
                                    </div>
                                    <!-- <div class="col-xs-12 col-md-4">
                                        <?php /*
                                            echo $form->field($sucursal, '[{$i}]coord_google_maps')->widget(
                                                'kolyunya\yii2\widgets\MapInputWidget',
                                                [
                                                    'key' => 'AIzaSyA6MKTxmH6B2J6s9xydrJds1DxvlCxwARw',
                                                    'latitude' => 19.0000000,
                                                    'longitude' => -70.6667000,
                                                    'zoom' => 7,
                                                    'width' => '100%',
                                                    'height' => '200px',
                                                    'pattern' => '%longitude%,%latitude%',
                                                    'mapType' => 'roadmap',
                                                    'animateMarker' => true,
                                                    'enableSearchBar' => true,

                                                ]
                                            ); */
                                        ?>
                                    </div> -->
                                    <div class="col-xs-12 col-md-1 text-center">
                                            <button type="button" class="remove-sucursal btn btn-cancelar btn-xs" style="margin-top:28px;">
                                            <i class="glyphicon glyphicon-minus"></i></button>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        
                    </tbody>
                </table>
                <br>
                <button type="button" class="add-sucursal btn btn-accion  btn-xs pull-right">Agregar Sucursal</button>
                <?php DynamicFormWidget::end(); ?>
            </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DETALLE DE GARANTÍAS</p>
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-xs-12">
            
            <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_garantias',
                'widgetBody' => '.container-garantias',
                'widgetItem' => '.garantia',
                'limit' => 10,
                'min' => 1,
                'insertButton' => '.add-garantia',
                'deleteButton' => '.remove-garantia',
                'model' => $model_detalle_garantia[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'description',
                ],
            ]); ?>
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th style="width: 700px;">VEHÍCULOS CON GARANTÍA</th>
                        <th class="text-center" style="width: 60px;">
                            
                            <button type="button" class="pull-right add-garantia btn btn-gris btn-xs letranegra" style="float: right;">Añadir Detalle Garantiía</button>
                        </th>
                    </tr>
                </thead>
                <tbody class="container-garantias">
                <?php foreach ($model_detalle_garantia as $i => $garantia): ?>
                    <tr class="garantia">
                        <?php
                            // necessary for update action.
                            if (! $garantia->isNewRecord) {
                                echo Html::activeHiddenInput($garantia, "[{$i}]id");

                                $respaldo = 0;
                            }else{
                              $respaldo = $i;
                            }
                        ?>
                        <td>
                            <div class="col-md-6">
                                <?php

                                    echo  $form->field($garantia,"[{$i}]id_marca")->dropDownList(
                                    $marcas,
                                    [
                                        'prompt'=>'Seleccione la marca',
                                        'class' => 'form-control marca','id' => 'marca',

                                    ]
                                    )->label('MARCA');
                                ?>
                            </div>
                            <div class="clearfix"></div>
                            <br>
                            <div class="col-md-offset-4 col-md-3">
                                <label class="control-label" style="align:right;"> Kilometraje</label> 
                            </div>
                            <div class="col-md-3">
                                <label class="control-label" style="align:right;"> Tiempo</label> 
                            </div>
                            <div class="col-md-2">
                                <label class="control-label" style="align:right;"> Periodo</label> 
                            </div>
                            <?php foreach ($model_partes_garantia as $k => $parte_garantia) { 
                                /*echo "<pre>";
                                print_r($parte_garantia);
                                echo "</pre>";*/
                               // if(!$parte_garantia->isNewRecord && isset($parte_garantia[$k]->detalle_garantia_id) &&   $parte_garantia[$k]->detalle_garantia_id == $garantia->id){
                                         
                             

                                if( isset($parte_garantia[$k]) && isset($parte_garantia[$k]->detalle_garantia_id) &&  
                                 $parte_garantia[$k]->detalle_garantia_id == $garantia->id){
                                 ?>
                                    <?php foreach ($partes as $j => $parte) { 
                                      //  var_dump($parte_garantia[0]->tiempo); die();
                                      
                                        ?>
                                        <div class="col-md-4" style="text-transform: uppercase;">
                                            <?= $parte->nombre ?>
                                        </div>
                                        <div class="col-md-3">
                                            <?= $form->field($parte_garantia[$j], "[{$k}]kilometraje[{$parte->id}]")->textInput(['placeholder'=>'Kilometraje', 'value' => $parte_garantia[$j]->kilometraje])->label(false); ?>
                                        </div>

                                        <div class="col-md-3">
                                            <?= $form->field($parte_garantia[$j], "[{$k}]tiempo[{$parte->id}]")->textInput(['placeholder'=>'Tiempo', 'value' => $parte_garantia[$j]->tiempo])->label(false); ?>

                                           
                                            

                                            <?= $form->field($parte_garantia[$j], "[{$k}]id[{$parte->id}]")->hiddenInput(['placeholder'=>'Tiempo', 'value' => $parte_garantia[$j]->id])->label(false); ?>
                                        </div>

                                        <div class="col-md-2">
                                            <?php
                                                echo  $form->field($parte_garantia[$j],"[{$k}]periodo[{$parte->id}]")->dropDownList(
                                                $periodos,
                                                [
                                                    'prompt'=>'Seleccione',
                                                    'class' => 'form-control','id' => 'periodo',
                                                    'options' => [$parte_garantia[$j]->periodo => ['selected' => true ]],

                                                ]
                                                )->label(false);
                                            ?>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                        </td>
                        <td class="text-center vcenter" style="width: 90px;">
                            <button type="button" class="pull-right remove-garantia btn btn-cancelar btn-xs letranegra" style="float: right;"><i class="glyphicon glyphicon-minus"></i></button>
                        </td>
                    </tr>
                 <?php endforeach; ?>
                </tbody>
            </table>
            <?php DynamicFormWidget::end(); ?>
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>




    <?php
        if(!$model->isNewRecord){
        ?>
            <div class="row" align="center">
                <div>
                    <?= Html::a('CANCELAR', ['view', 'id' => $model->id], ['class' => 'btn btn-cancelar']) ?>
                    &nbsp;&nbsp;&nbsp;
                    <?= Html::submitButton('GUARDAR CAMBIOS', ['class' => 'btn btn-primario']) ?>
                </div>
            </div>
        <?php
        }else{
        ?>
            <div class="form-group" align="center">
                <?= Html::submitButton('GUARDAR', ['class' => 'btn btn-primario']) ?>
            </div>
        <?php
        }
    ?>



    <?php ActiveForm::end(); ?>


</div>