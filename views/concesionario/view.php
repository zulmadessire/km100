<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use app\models\SucursalConcesionario;
use app\models\DetalleGarantia;
use app\models\TipoEmpresa;
use kartik\form\ActiveForm;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\Concesionario */
$this->title = 'CONCESIONARIO '.strtoupper($model->nombre);
?>
<div class="concesionario-view">


    <div class="row">
        <div class="col-md-1 col-xs-3">
            <h4 class="titulo"><i class="fa fa-building fa-4x" aria-hidden="true"></i></h4>
        </div>
        <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
            <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
        </div>
    </div>
    
    <div class="row" style="border-bottom: 1px solid #222d32;">
        <div class="col-xs-12 col-md-6">
            <h4 class="titulo-form">DATOS DEL CONCESIONARIO</h4>
        </div>
        <div class="col-xs-12 col-md-6">
            <div align="right">
                <?php
                echo Html::a('Listado de Concesionarios', ['index'], ['class' => 'btn btn-small-gris']);
                ?>&nbsp;&nbsp;&nbsp;<?php
                echo Html::a('Modificar Concesionario', ['update', 'id' => $model->id], ['class' => 'btn btn-small-gris']);
                ?>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-4">
            <h4><b>Nombre Concesionario: </b> <?= $model->nombre ?> </h4>
        </div>
    </div>
    <br>
    <div class="row" style="border-bottom: 1px solid #222d32;">
        <div class="col-xs-12 col-md-6">
            <h4 class="titulo-form">SUCURSALES</h4>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <?php foreach ($model->sucursalConcesionarios as $key => $sucursal): ?>
                <table class ="tablacaso" border="1">
                    <tr><th style="width: 30%;">Nombre Sucursal: </th>               <td><?= $sucursal->nombre ?></td></tr>
                    <tr><th>Dirección Sucursal: </th>                                <td><?= $sucursal->direccion ?></td></tr>
                    <tr><th>Teléfono Sucursal: </th>                                 <td><?= $sucursal->telefono ?></td></tr>
                    <tr><th>Correo Sucursal: </th>                                   <td><?= $sucursal->email ?></td></tr>
                    <tr><th>Nombre Asesor: </th>                                     <td><?= $sucursal->nombre_asesor .' '. $sucursal->apellido_asesor ?></td></tr>
                    <tr><th>Teléfono Asesor: </th>                                   <td><?= $sucursal->telefono_asesor ?></td></tr>
                    <tr><th>Correo Asesor: </th>                                     <td><?= $sucursal->email_asesor ?></td></tr>                         
                </table> 
                <br>
            <?php endforeach ?>
        </div>
    </div>
    <br>
    <div class="row" style="border-bottom: 1px solid #222d32;">
        <div class="col-xs-12 col-md-6">
            <h4 class="titulo-form">DETALLE DE GARANTÍAS</h4>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <?php foreach ($model->detalleGarantias as $key => $detalle_garantia): ?>
                <table class ="tablaproveedor" border="1">
                    <tr><td colspan="3" style="font-size: 14pt; font-weight:bold; text-align:center; height: 50px; text-transform: uppercase; background-color: #AAA;"> <?= $detalle_garantia->idMarca->nombre ?></td></tr>
                    <tr>
                        <td style="font-weight:bold; text-align:center; background:#DAE3F3; text-transform: uppercase;">Garantía</td>
                        <td style="font-weight:bold; text-align:center; background:#DAE3F3; text-transform: uppercase;">Tiempo</td>
                        <td style="font-weight:bold; text-align:center; background:#DAE3F3; text-transform: uppercase;">Kilometraje</td>
                    </tr>
                    
                    <?php foreach ($detalle_garantia->partesGarantias as $key => $parte_garantia): ?>
                        <tr>
                            <td style="text-transform: uppercase;"><?= $parte_garantia->partes->nombre ?></td>
                            <td style="text-transform: uppercase; text-align:center;">

                                <?php 
                                    if ( $parte_garantia->tiempo ) {
                                        $periodo = '';
                                        if ( $parte_garantia->periodo ==  0 ) {
                                            if ( $parte_garantia->tiempo > 1 ) {
                                                $periodo = 'días';
                                            } else{
                                                $periodo = 'día';
                                            }
                                            
                                        } elseif ( $parte_garantia->periodo == 1 ) {
                                            if ( $parte_garantia->tiempo > 1 ) {
                                                $periodo = 'meses';
                                            } else{
                                                $periodo = 'mes';
                                            }
                                            
                                        } elseif ( $parte_garantia->periodo == 2 ) {
                                            if ( $parte_garantia->tiempo > 1 ) {
                                                $periodo = 'años';
                                            } else{
                                                $periodo = 'año';
                                            }
                                        }
                                        echo $parte_garantia->tiempo. ' ' .$periodo;
                                    } else{
                                        echo '-';
                                    }
                                ?>
                                
                            </td>
                            <td style="text-transform: uppercase; text-align:center;"><?= number_format(floatval($parte_garantia->kilometraje), 0, '.', ',') ?></td>
                        </tr>
                    <?php endforeach ?>
                    <br>
                    <br>
                </table>
            <?php endforeach ?>
        </div>
    </div>
</div>
 
 
 


 

 