<?php 
use app\models\User;
use app\models\Proveedor;
use app\models\MarcaProveedor;
 ?>

<aside class="main-sidebar">
<br><br>
    <section class="sidebar">
        <?php 

                   $usuario  = User::findOne(['id'=>Yii::$app->user->identity->id]);
                    $prov  = Proveedor::findOne(['id'=>$usuario->id_proveedor]);
                    $marcaprov  = MarcaProveedor::findOne(['id_proveedor'=>$usuario->id_proveedor]);
                    if (!$prov) {
                         
                        $prov= new Proveedor();

                    } 
                      if (!$marcaprov) {
                         
                        $var = false;

                    } else{$var = true;}
                    
                    

         ?>
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'LOGIN', 'url' => ['/user/security/login'], 'visible' => Yii::$app->user->isGuest],

                    [
                        'label' => 'ADMÓN. DE CASOS',
                        'icon' => 'cogs',
                        'visible' => !Yii::$app->user->isGuest && !\Yii::$app->user->can('taller') && !\Yii::$app->user->can('proveedor'),
                        'items' => [
                            ['label' => 'REGISTRO DE CASOS', 'icon' => 'file-text-o', 'url' => ['/caso/create'], 'visible' => ( \Yii::$app->user->can('registro casos cliente') || \Yii::$app->user->can('registro casos estaciones asociadas') || \Yii::$app->user->can('Registro Casos Todos') )],
                            ['label' => 'CONSULTA DE CASOS', 'icon' => 'eye', 'url' => ['/caso/index'], 'visible' => ( \Yii::$app->user->can('casos cliente') || \Yii::$app->user->can('casos estaciones asociadas') || \Yii::$app->user->can('casos todos') || \Yii::$app->user->can('casos propios') )],
                            ['label' => 'REGISTRO DE SOLICITUD', 'icon' => 'file-text-o', 'url' => ['/servicio/create'], 'visible' => (\Yii::$app->user->can('admin') || \Yii::$app->user->can('Nivel 3') )],
                            ['label' => 'LISTADO DE SOLICITUDES', 'icon' => 'list-ul', 'url' => ['/servicio/index'], 'visible' => (\Yii::$app->user->can('admin') || \Yii::$app->user->can('Nivel 3') )],
                            ['label' => 'ENVÍOS A TALLER', 'icon' => 'list-ul', 'url' => ['/servicio/indexenvioestacion'], 'visible' => (\Yii::$app->user->can('admin') || \Yii::$app->user->can('Nivel 3') )],
                            ['label' => 'HISTÓRICO DE SOLICITUDES', 'icon' => 'list-ul', 'url' => ['/servicio/index-record'], 'visible' => (\Yii::$app->user->can('admin') || \Yii::$app->user->can('Nivel 3') )],
                            ['label' => 'REGISTRO DE COBROS', 'icon' => 'file-text-o', 'url' => ['/cobro-caso/create'], 'visible' => (\Yii::$app->user->can('admin') )],
                            ['label' => 'REPORTE DE COBROS', 'icon' => 'list-ul', 'url' => ['/cobro-caso/index'], 'visible' => (\Yii::$app->user->can('admin'))],
                        ],
                    ],
                    
                           [
                        'label' => 'PROVEEDOR',
                        'icon' => 'user-plus',
                        'visible' => !Yii::$app->user->isGuest && \Yii::$app->user->can('proveedor') && $var,
                        'items' => [
                            ['label' => 'VER DATOS', 'icon' => 'eye', 'url' => ['/proveedorpieza/view','id'=>$prov->id]],
                            ['label' => 'ACTUALIZAR DATOS', 'icon' => 'list-ul', 'url' => ['/proveedorpieza/update','id'=>$prov->id]],
                        ],
                    ],
                           [
                        'label' => 'LICITACIONES',
                        'icon' => 'search',

                        'visible' => !Yii::$app->user->isGuest && \Yii::$app->user->can('admin'),

                        'items' => [

                            ['label' => 'LISTADO DE LICITACIONES', 'icon' => 'eye', 'url' => ['/licitacion/index','id'=>$prov->id]],
                            ['label' => 'REGISTRO DE LICITACIONES', 'icon' => 'list-ul', 'url' => ['/licitacion/create','id'=>$prov->id]],
                        ],
                    ],
                
                       [
                        'label' => 'LICITACIONES',
                        'icon' => 'search',
                        'visible' => (!Yii::$app->user->isGuest && \Yii::$app->user->can('proveedor')),
                        'items' => [

                            ['label' => 'LISTADO DE LICITACIONES', 'icon' => 'eye', 'url' => ['/licitacion/indexlicproveedor']],
                         
                        ],
                    ],

                    [
                        'label' => 'VEHÍCULOS',
                        'visible'=> (!Yii::$app->user->isGuest && \Yii::$app->user->can('admin') || \Yii::$app->user->can('flota todos') || \Yii::$app->user->can('flota estaciones asociadas')),
                        'icon' => 'car',
                        'items' => [
                            ['label' => 'FLOTA','icon' => 'car','url' => ['/vehiculo/index'],],
                            ['label' => 'CARGAR VEHICULOS','icon' => 'upload','url' => ['/vehiculo/cargarvehiculos'], 'visible'=> \Yii::$app->user->can('admin') ],
                            ['label' => 'FLOTA EN MANTENIMIENTO','icon' => 'wrench','url' => ['/vehiculo/indexmantenimiento'], 'visible'=> \Yii::$app->user->can('admin')],
                            ['label' => 'VEHICULOS EN TRÁNSITO', 'icon' => 'car', 'url' => ['/vehiculo/transito'], 'visible'=> \Yii::$app->user->can('admin')],
                        ],
                    ],

                      [
                        'label' => 'AUDITORIAS',
                        'visible'=> (!Yii::$app->user->isGuest && \Yii::$app->user->can('admin') || \Yii::$app->user->can('garantia')),
                        'icon' => 'search',
                        'items' => [
                          
                            [
                            'label' => 'AUDIT. MENSUALES PENDIENTES',
                            'icon' => 'wrench',

                            'url' => ['/auditoriageneral/index'],
                            ],
                               [
                            'label' => 'AUDIT. CONTINUAS',
                            'icon' => 'wrench',

                            'url' => ['/auditoriageneral/indexcontinuo'],
                            ],
                             
                        ],
                    ],
                    [
                        'label' => 'AVALÚOS',
                        'icon' => 'car',
                        'visible' => !Yii::$app->user->isGuest && \Yii::$app->user->can('analista'),
                        'items' => [
                            ['label' => 'LISTADO DE SOLICITUDES', 'icon' => 'list-ul', 'url' => ['/analista-mantenimiento/listadochecklist']],
                        ],
                    ],
                    [
                        'label' => 'GESTIÓN DE SERVICIOS',
                        'icon' => 'wrench',
                        'visible' => !Yii::$app->user->isGuest && (\Yii::$app->user->can('analista')  || \Yii::$app->user->can('gerente-tecnico') || \Yii::$app->user->can('planner') || \Yii::$app->user->can('admin') || \Yii::$app->user->can('director-comercial')),
                        'items' => [
                            ['label' => 'LISTADO DE SOLICITUDES', 'icon' => 'wrench', 'url' => ['/servicio/gestionservicio'], ],
                            ['label' => 'ASIGNACIÓN', 'icon' => 'wrench', 'url' => ['/servicio/indexasignacion'], 'visible' => \Yii::$app->user->can('planner')],
                            ['label' => 'APROBACIÓN', 'icon' => 'wrench', 'url' => ['/servicio/indexanalista'], 'visible' => \Yii::$app->user->can('analista')],
                            ['label' => 'APROBACIÓN', 'icon' => 'wrench', 'url' => ['/servicio/indexgerente'], 'visible' => \Yii::$app->user->can('gerente-tecnico') || \Yii::$app->user->can('director-comercial')],
                            ['label' => 'LISTADO DE OT', 'icon' => 'list-ul', 'url' => ['/servicio/indexordentrabajo'],],
                        ],
                    ],
                     [
                        'label' => 'DESPACHOS',
                        'icon' => 'list-ul',
                        'visible' => !Yii::$app->user->isGuest && \Yii::$app->user->can('reparto'),
                        'items' => [
                            ['label' => 'LISTADO DE DESPACHOS', 'icon' => 'list-ul', 'url' => ['/solicitud-compra-pieza/listado'], 'visible' => \Yii::$app->user->can('reparto')],
                            ['label' => 'LISTADO DE DESPACHOS EST.', 'icon' => 'list-ul', 'url' => ['/solicitud-compra-estacion/listado'], 'visible' => \Yii::$app->user->can('reparto')],
                        ],
                    ],
                    [
                        'label' => 'CONFIGURACIÓN',
                        'icon' => 'gears',
                        'visible' => (!Yii::$app->user->isGuest && \Yii::$app->user->can('admin')),
                        'items' => [
                            ['label' => 'USUARIOS', 'icon' => 'users', 'url' => ['/user/admin/index'], 'visible' => \Yii::$app->user->can('admin')],
                            ['label' => 'ROLES', 'icon' => 'user-secret', 'url' => ['/rbac/role/index'], 'visible' => \Yii::$app->user->can('admin')],
                            ['label' => 'MANTENIMIENTO', 'icon' => 'wrench', 'url' => ['/tipo-mantenimiento/index']],
                            ['label' => 'PIEZAS', 'icon' => 'tint', 'url' => ['/pieza/index']],
                            ['label' => 'MECÁNICOS', 'icon' => 'user-circle', 'url' => ['/mecanico/index']],
                            ['label' => 'REPRESENTANTE DE REPARTO', 'img' => 'Yii::$app->request->baseUrl/img/driver_24.png', 'url' => ['/mensajero/index']],
                            ['label' => 'PROVEEDORES', 'icon' => 'user-plus', 'url' => ['/proveedor/index']],
                            // ['label' => 'EMPRESAS', 'icon' => 'home', 'url' => ['/empresa/index']],
                            ['label' => 'ESTACIONES', 'icon' => 'home', 'url' => ['/estacion/index']],
                            ['label' => 'CONCESIONARIOS', 'icon' => 'building', 'url' => ['/concesionario/index']],
                            ['label' => 'TALLERES', 'icon' => 'car', 'url' => ['/taller/index']],
                            ['label' => 'SERVICIOS TALLER', 'icon' => 'suitcase', 'url' => ['/pieza-servicio-taller/index'],],
                            ['label' => 'VARIABLES', 'icon' => 'wrench', 'url' => ['/variable/index']],
                            //['label' => 'ANALISTA DE MANTENIMIENTO', 'icon ' => 'building', 'url' => ['/analista-mantenimiento/index']],
                             
                        ],
                    ],
          
                    [
                        'label' => 'MANTENIMIENTO',
                        'icon' => 'wrench',
                        'visible' => !Yii::$app->user->isGuest && (\Yii::$app->user->can('taller')),
                        'items' => [
                            ['label' => 'LISTADO DE SOLICITUDES', 'icon' => 'list-ul', 'url' => ['/servicio/indextaller'], 'visible' => \Yii::$app->user->can('taller')],
                            ['label' => 'LISTADO DE OT', 'icon' => 'list-ul', 'url' => ['/servicio/indexordentrabajo'], 'visible' => \Yii::$app->user->can('admin')],
                        ],
                    ],
                     

                    [
                        'label' => 'GESTIÓN DE PIEZAS',
                        'icon' => 'cogs',
                        'visible' => !Yii::$app->user->isGuest && \Yii::$app->user->can('admin') || \Yii::$app->user->can('proveedor') || \Yii::$app->user->can('gerente-compras') || \Yii::$app->user->can('director-comercial') || \Yii::$app->user->can('analista-compras') || \Yii::$app->user->can('admin-logistica') || \Yii::$app->user->can('Nivel 3'),
                        'items' => [
                            ['label' => 'LISTADO DE SOLICITUDES', 'icon' => 'list-ul', 'url' => ['/solicitud-compra-pieza/index'], 'visible' => (\Yii::$app->user->can('admin') || \Yii::$app->user->can('analista-compras') || \Yii::$app->user->can('admin-logistica')    )],
                            //['label' => 'LISTADO DE PIEZAS RECIBIDAS', 'icon' => 'list-ul', 'url' => ['/solicitud-compra-pieza/recibidas'], 'visible' => \Yii::$app->user->can('admin')],
                            //['label' => 'LISTADO DE PIEZAS DEVUELTAS', 'icon' => 'list-ul', 'url' => ['/solicitud-compra-pieza/indexdevolucion'], 'visible' => \Yii::$app->user->can('admin') || \Yii::$app->user->can('admin-logistica')],
                            //['label' => 'LISTADO DE PIEZAS DEVUELTAS', 'icon' => 'list-ul', 'url' => ['/solicitud-compra-pieza/indexdevolucionproveedor'], 'visible' => \Yii::$app->user->can('proveedor')],
                            //['label' => 'SOLIC. DE COMPRA CERRADAS', 'icon' => 'list-ul', 'url' => ['/solicitud-compra-pieza/cerradas'], 'visible' => \Yii::$app->user->can('admin')],
                            ['label' => 'LISTADO DE SOLICITUDES EST.', 'icon' => 'list-ul', 'url' => ['/solicitud-compra-estacion/index'], 'visible' => (\Yii::$app->user->can('admin') || \Yii::$app->user->can('analista-compras') || \Yii::$app->user->can('admin-logistica')    )],
                            ['label' => 'LISTADO DE SOLIC. COTIZACIÓN', 'icon' => 'list-ul', 'url' => ['/solicitud-compra-pieza/indexsolicitudcotizacion'], 'visible' => \Yii::$app->user->can('proveedor')],
                            ['label' => 'LISTADO DE ORDEN COMPRA', 'icon' => 'list-ul', 'url' => ['/solicitud-compra-pieza/indexordencompraproveedor'], 'visible' => \Yii::$app->user->can('proveedor')],
                            ['label' => 'LIST. DE ORDEN COMPRA EST.', 'icon' => 'list-ul', 'url' => ['/solicitud-compra-estacion/indexordencompraproveedor'], 'visible' => \Yii::$app->user->can('proveedor')],
                            ['label' => 'LISTADO DE SOLIC. COTIZACIÓN', 'icon' => 'list-ul', 'url' => ['/solicitud-compra-pieza/indexsolicitudaprobacion'], 'visible' => (\Yii::$app->user->can('gerente-compras') || \Yii::$app->user->can('director-comercial')) ],
                            ['label' => 'LISTADO DE S. COTIZACIÓN EST.', 'icon' => 'list-ul', 'url' => ['/solicitud-compra-estacion/indexsolicitudaprobacion'], 'visible' => (\Yii::$app->user->can('gerente-compras') || \Yii::$app->user->can('director-comercial')) ],
                            ['label' => 'LIST. DE SOLIC. COTIZACIÓN EST.', 'icon' => 'list-ul', 'url' => ['/solicitud-compra-estacion/indexsolicitudcotizacion'], 'visible' => \Yii::$app->user->can('proveedor')],
                            ['label' => 'SOLICITUD DE COMPRA', 'icon' => 'list-ul', 'url' => ['/solicitud-compra-estacion/create'], 'visible' => \Yii::$app->user->can('admin') || \Yii::$app->user->can('Nivel 3')],
                            ['label' => 'LISTADO DE SOLICITUDES', 'icon' => 'list-ul', 'url' => ['/solicitud-compra-estacion/index'], 'visible' => (\Yii::$app->user->can('admin') || \Yii::$app->user->can('Nivel 3') )],
                            ['label' => 'LISTADO DE ORDEN COMPRA', 'icon' => 'list-ul', 'url' => ['/solicitud-compra-pieza/indexordencompra'], 'visible' => (\Yii::$app->user->can('analista-compras') || \Yii::$app->user->can('director-comercial') || \Yii::$app->user->can('gerente-compras'))],
                            ['label' => 'LIST. DE ORDEN COMPRA EST.', 'icon' => 'list-ul', 'url' => ['/solicitud-compra-estacion/indexordencompra'], 'visible' => (\Yii::$app->user->can('analista-compras') || \Yii::$app->user->can('director-comercial') || \Yii::$app->user->can('gerente-compras'))],
                            
                        ],
                    ],
                     [
                        'label' => 'INDICADORES',
                        'icon' => 'search',

                        'visible' => !Yii::$app->user->isGuest && \Yii::$app->user->can('admin'),

                        'items' => [

                            ['label' => 'FUERA DE SERVICIO EN ESTACIÓN', 'icon' => 'chart-bar', 'url' => ['/indicador/fueradeservicioest']],
                           
                        ],
                    ],

                    /*['label' => 'REGISTRO DE COBRO', 'icon' => 'users', 'url' => ['https://docs.google.com/forms/d/e/1FAIpQLSe9PKBdr8eSGTczBR6E4e4_CAbV-rBmWW-z5JEYOfTAIOW1aw/viewform#responses'], 'visible' => ( \Yii::$app->user->can('form - registro de cobro') ), 'template'=> '<a href="https://docs.google.com/forms/d/e/1FAIpQLSe9PKBdr8eSGTczBR6E4e4_CAbV-rBmWW-z5JEYOfTAIOW1aw/viewform#responses" target="_blank">{label}</a>'],
                    ['label' => 'SOLICITUD DE TALLER', 'icon' => 'users', 'url' => ['https://docs.google.com/forms/d/e/1FAIpQLSe4tmut-6hKFl0Y1V0KEVHgdOTdMDdaqO_0dH1VMGyDn-61kg/viewform'], 'visible' => ( \Yii::$app->user->can('form - soliciud taller') ), 'template'=> '<a href="https://docs.google.com/forms/d/e/1FAIpQLSe4tmut-6hKFl0Y1V0KEVHgdOTdMDdaqO_0dH1VMGyDn-61kg/viewform" target="_blank">{label}</a>'],
                    ['label' => 'REGISTRO MANTENIENDO PREVENTIVO', 'icon' => 'users', 'url' => ['https://docs.google.com/forms/d/e/1FAIpQLSfEIZJ1b8eQgMz7azY2MCyuq4d2Bp4ijHknyZrKQfJejTYMyA/viewform?usp=sf_link'], 'visible' => ( \Yii::$app->user->can('form - mantenimiento') ), 'template'=> '<a href="https://docs.google.com/forms/d/e/1FAIpQLSfEIZJ1b8eQgMz7azY2MCyuq4d2Bp4ijHknyZrKQfJejTYMyA/viewform?usp=sf_link" target="_blank">{label}</a>'],
                    ['label' => 'SOLICITUD DE COMPRA', 'icon' => 'users', 'url' => ['https://docs.google.com/forms/d/e/1FAIpQLScI0L_PrqrSjPo02ks9_X26wGYmhiqhKSS6xfv9OU26LOdg8w/viewform?usp=sf_link'], 'visible' => ( \Yii::$app->user->can('form - solicitud compra') ), 'template'=> '<a href="https://docs.google.com/forms/d/e/1FAIpQLScI0L_PrqrSjPo02ks9_X26wGYmhiqhKSS6xfv9OU26LOdg8w/viewform?usp=sf_link" target="_blank">{label}</a>'],
                    ['label' => 'NOTIFICACIÓN DE ESTACIONES', 'icon' => 'users', 'url' => ['https://docs.google.com/forms/d/e/1FAIpQLSfyLHxFpYwiGz8f_aGxjbs2vn_tgKm2ByG6J0dcaKMbg8NMFg/viewform?usp=sf_link'], 'visible' => ( \Yii::$app->user->can('form - notificacion') ), 'template'=> '<a href="https://docs.google.com/forms/d/e/1FAIpQLSfyLHxFpYwiGz8f_aGxjbs2vn_tgKm2ByG6J0dcaKMbg8NMFg/viewform?usp=sf_link" target="_blank">{label}</a>'],
                    */
                    ['label' => 'PERFIL', 'icon' => 'street-view', 'url' => ['/user/admin/update', 'id'=>Yii::$app->user->identity->id], 'visible' => !\Yii::$app->user->can('admin')],
                    /*[
                        'label' => 'Proveedores',
                        'icon' => 'handshake-o',
                        'visible' => !Yii::$app->user->isGuest,
                        'items' => [
                            ['label' => 'Registro de Proveedores', 'icon' => 'user-plus ', 'url' => ['/proveedorpieza/create'], 'visible' => (\Yii::$app->user->can('supervisor') || \Yii::$app->user->can('admin'))],
                            ['label' => 'Listado de Proveedores', 'icon' => 'eye', 'url' => ['/proveedor/index']],
                        ],
                    ],
                    [
                        'label' => 'Concesionarios',
                        'icon' => 'building',
                        'visible' => !Yii::$app->user->isGuest,
                        'items' => [
                            ['label' => 'Registro de Concesionarios', 'icon' => 'user-plus ', 'url' => ['/concesionario/create'], 'visible' => (\Yii::$app->user->can('supervisor') || \Yii::$app->user->can('admin'))],
                            ['label' => 'Listado de Concesionarios', 'icon' => 'eye', 'url' => ['/concesionario/index']],
                        ],
                    ],*/
                ],
            ]
        ) ?>

    </section>
</aside>
