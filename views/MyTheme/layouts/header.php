<?php
use yii\helpers\Html;
use app\models\Notificaciones;
/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">

    <nav class="navbar navbar-static-top" role="navigation" style="height: 60px;">
          
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only"></span>
                </a>

 

                <div style="margin-left: 5px; float: left;">
                    <?= Html::a('<img src="'.Yii::$app->request->baseUrl.'/img/logo.png" height="100%"/><span> Km100</span>', Yii::$app->homeUrl, ['class' => 'logo', 'style'=>'text-align:left;']) ?>
                </div>

 
      <div class="navbar-custom-menu">

        <ul class="nav navbar-nav">
<?php 
                $cantidad = Notificaciones::find()->where(['estado'=>0])->all();
                $cantidad_placas = Notificaciones::find()->where(['estado'=>0,'tipo'=>0])->all();
                $cantidad_seguros = Notificaciones::find()->where(['estado'=>0,'tipo'=>1])->all();
                $cantidad_marbetes = Notificaciones::find()->where(['estado'=>0,'tipo'=>2])->all();
                $cantidad_rm = Notificaciones::find()->where(['estado'=>0,'tipo'=>3])->all();
                $cantidad_acuerdo = Notificaciones::find()->where(['estado'=>0,'tipo'=>4])->all();
                $contador = 0;
                foreach ($cantidad as $key => $value) {
                    $contador = $contador + 1;
                }
                $contador_placas = 0;
                foreach ($cantidad_placas as $key => $value2) {
                    $contador_placas = $contador_placas + 1;
                }
                $contador_marbetes = 0;
                foreach ($cantidad_marbetes as $key => $value2) {
                    $contador_marbetes = $contador_marbetes + 1;
                }
                $contador_seguros = 0;
                foreach ($cantidad_seguros as $key => $value2) {
                    $contador_seguros = $contador_seguros + 1;
                }
                   $contador_rm = 0;
                foreach ($cantidad_rm as $key => $value2) {
                    $contador_rm = $contador_rm + 1;
                }
                    $contador_acuerdo = 0;
                foreach ($cantidad_acuerdo as $key => $value2) {
                    $contador_acuerdo = $contador_acuerdo + 1;
                }
             ?>
         


                
                <!-- Tasks: style can be found in dropdown.less -->
            <li class="dropdown tasks-menu">
             <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell-o" ></i>
                        <span class="label label-warning notifications-icon-count"><?php echo $contador; ?></span>
                    </a>
                    <ul class="dropdown-menu">
                              <li>
                            <ul class="menu">
                                <div id="notifications"></div>
                            </ul>
                        </li>
                        <li class="footer">
                            <?php 
                                if ($cantidad_placas) {?>
                                    <?= Html::a('<span>Tiene ('.$contador_placas.') placas nuevas por vencer</span>', ['/notificaciones/indexplacas']) ?>
                                <?php  }?>

                            <?php 
                                if (!$cantidad_placas) {?>
                                    <?= Html::a('<span>Ver placas próximas a vencer</span>', ['/notificaciones/indexplacas']) ?>
                            <?php  }?>

                             <?php 
                                if ($cantidad_seguros) {?>
                                    <?= Html::a('<span>Tiene ('.$contador_seguros.') seguros nuevos por vencer</span>', ['/notificaciones/indexseguros']) ?>
                                <?php  }?>

                            <?php 
                                if (!$cantidad_seguros) {?>
                                    <?= Html::a('<span>Ver seguros próximos a vencer</span>', ['/notificaciones/indexseguros']) ?>
                            <?php  }?>

                            <?php 
                                if ($cantidad_marbetes) {?>
                                    <?= Html::a('<span>Tiene ('.$contador_marbetes.') marbetes nuevos por vencer</span>', ['/notificaciones/indexmarbetes']) ?>
                                <?php  }?>

                            <?php 
                                if (!$cantidad_marbetes) {?>
                                    <?= Html::a('<span>Ver marbetes próximos a vencer</span>', ['/notificaciones/indexmarbetes']) ?>
                            <?php  }?>

                                  <?php 
                                if ($cantidad_rm) {?>
                                    <?= Html::a('<span>Tiene ('.$contador_rm.') Registro Mercantil por vencer</span>', ['/notificaciones/indexrm']) ?>
                                <?php  }?>

                            <?php 
                                if (!$cantidad_rm) {?>
                                    <?= Html::a('<span>Ver Registros Mercantil próximos a vencer</span>', ['/notificaciones/indexrm']) ?>
                            <?php  }?>

                                 <?php 
                                if ($cantidad_acuerdo) {?>
                                    <?= Html::a('<span>Tiene ('.$contador_acuerdo.') acuerdos comerciales por vencer</span>', ['/notificaciones/indexacuerdo']) ?>
                                <?php  }?>

                            <?php 
                                if (!$cantidad_acuerdo) {?>
                                    <?= Html::a('<span>Ver acuerdos comerciales próximos a vencer</span>', ['/notificaciones/indexacuerdo']) ?>
                            <?php  }?>
                             
                          

                         

                        </li>
                     
                    </ul>
                </li>
                <!-- User Account: style can be found in dropdown.less -->

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                    

                       
                        
                        

                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">

                              
                            <p>
                            

                               <!--  <small>Member since Nov. 2012</small> -->
                            </p>
                        </li>

                        <!-- Menu Body -->
                        <li class="user-body">
                           
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <?= Html::a(
                                    'Editar perfil',    
                                    ['/usuario/perfil','id'=>Yii::$app->user->id,'a'=>0],
                                    ['data-method' => 'post', 'class' => 'btn btn-perfil']
                                ) ?>
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    'Sign out',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-perfil']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>

                <!-- User Account: style can be found in dropdown.less -->
                <li id="ima_avatar">



                <?php if(!Yii::$app->user->isGuest){ ?>
                 
                        <?= Html::a(
                            'Cerrar Sesión ('.Yii::$app->user->identity->username.')',
                            ['/site/logout'],
                            ['data-method' => 'post', 'class' => 'btn btn-default']
                        ) ?>
             
                <?php } ?>
            
                </li>
            </ul>
        </div>
    </nav>
</header>
