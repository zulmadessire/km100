<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
$objeto_error =  (array) $exception;
?>
<section class="content">
    
     <?php if( $objeto_error['statusCode'] == 403 ): ?>

        <div id="w0-error" class="alert-danger alert fade in">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

        <i class="icon fa fa-ban"></i> No tiene permisos para poder acceder a esta acción. Por favor comuniquese con el administrador.

        </div>

        <div class="row" align="center">
            <?= Html::a('<img src="'.Yii::$app->request->baseUrl.'/img/logo.png" width="150"/>', Yii::$app->homeUrl) ?>
        </div>

    <?php else: ?>

        <div class="error-page">

            <h2 class="headline text-info"><i class="fa fa-warning text-yellow"></i></h2>

            <div class="error-content">
                <h3><?= $name ?></h3>
                <p>
                    <?= nl2br(Html::encode($message)) ?>
                </p>

                <p>
                    The above error occurred while the Web server was processing your request.
                    Please contact us if you think this is a server error. Thank you.
                    Meanwhile, you may <a href='<?= Yii::$app->homeUrl ?>'>return to dashboard</a> or try using the search
                    form.
                </p>

                <form class='search-form'>
                    <div class='input-group'>
                        <input type="text" name="search" class='form-control' placeholder="Search"/>

                        <div class="input-group-btn">
                            <button type="submit" name="submit" class="btn btn-primary"><i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    <?php endif;  ?>

</section>
