<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Empresa;
use app\models\Cliente;
use yii\data\Pagination;
use yii\widgets\LinkPager;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel app\models\EstacionesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Listado de estaciones';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estacion-index">

        <div class="row">
    <div class="col-md-1 col-xs-3">
      <h4 class="titulo"><i class="fa fa-home fa-4x" aria-hidden="true"></i></h4>
    </div>
    <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
      <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
    </div>
  </div>
  </br>
  <div class="row">
    <div class="col-md-12">
      <?php
          echo Html::a('Registrar Estación', ['create'], ['class' => 'btn btn-small-gris', 'style' => 'float:right;']);
      ?>
    </div>
  </div>
  </br>
  </br>
    <div class="row">
        <div class="col-md-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'summary' => '',
                 'options' =>[
                                'class' => 'grid',
                            ],
                'columns' => [
                    [
                        'attribute' => 'codigo',
                        'enableSorting' => false,
                        'value' => function( $model ){
                            if ( $model->codigo_generado ) {
                                return $model->codigo_generado;
                            } else{
                                return '-';
                            }
                        },
                    ],
                         [
                    
                        'label' => 'Cliente',
                        'value' => function ($model) {
                                    
                            return $model->idEmpresa->cliente->nombre_comercial;
                              
                        },
                        'format' => 'text',
                          'filter' => Select2::widget([
                                  'name' => 'EstacionesSearch[id_cliente]',
                                  'data' => ArrayHelper::map(Cliente::find()->orderBy('nombre_comercial')->all(), 'id', 'nombre_comercial'),
                                  'hideSearch' => true,
                                  'options' => ['placeholder' => 'Cliente',],
                                  'pluginOptions' => [
                                      'allowClear' => true
                                  ],
                              ]),
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'id_empresa',
                        'label' => 'Empresa',
                        'value' => function ($model) {
                                    
                            return  $model->idEmpresa->nombre;
                              
                        },
                        'format' => 'text',
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'id_empresa',
                            'data' =>  ArrayHelper::map(Empresa::find()->orderBy('nombre')->all(), 'id_empresa', 'nombre'),
                            'options' => ['placeholder' => 'Seleccione empresa...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]),
                        'enableSorting' => false,
                    ],

                    
                    [
                        'attribute' => 'nombre',
                        'label' => 'Estación',
                        'enableSorting' => false,
                    ],
                    
                    /*[
                        'attribute' => 'coord_google_maps',
                        'label' => 'Coordenadas',
                        'enableSorting' => false,
                    ],*/
                    [  
                        'class' => 'yii\grid\ActionColumn',
                        //'contentOptions' => ['style' => 'width:260px;'],
                        'header'=>'Acciones',
                        'template' => '{menu}',
                        'buttons' => [

                            //view button
                            'menu' => function ($url, $model) {

                                 return  '<div class="dropdown">
                                          <button class="btn btn-default dropdown-toggle btn-sm" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i>
                                          <span class="caret"></span></button>
                                          <ul class="dropdown-menu">
                                            
                                            <li>'.Html::a('<span><i class="fa fa-pencil fa-sm" aria-hidden="true"></i></span> Modificar Datos',[ 'estacion/update/', 'id' => $model->id_estacion ]).' </li>
                                            <li>'.Html::a('<span><i class="fa fa-eye fa-sm" aria-hidden="true"></i></span> Ver Estación',[ 'estacion/view/', 'id' => $model->id_estacion ]).' </li>
                                            
                                          </ul>
                                        </div>';


                            },
                        ],

                   ],
                ],
            ]); ?>
        </div>
    </div>
</div>
