<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\db\Query;
use yii\web\view;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\file\FileInput;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\widgets\InputWidget;
use app\models\Estacion;
use app\models\Empresa;
use kolyunya\yii2\widgets\MapInputWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Estacion */

$this->title = 'INFORMACIÓN DE LA ESTACIÓN';
?>
<div class="estacion-view">
 <?php $form = ActiveForm::begin(['id' => 'dynamic_lista-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>
    
    <div class="row">
        <div class="col-md-1 col-xs-3">
            <h4 class="titulo"><i class="fa fa-home fa-4x" aria-hidden="true"></i></h4>
        </div>
        <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
            <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                <?= Html::a('Listado de Estaciones',['estacion/index'], ['class' => 'btn btn-small-gris']) ?>
            </div>
        </div>
        </br>
        </br>
    </div>

     
      <div class="row" style="border-bottom: 1px solid #222d32;">
            <div class="col-xs-12 col-md-6">
                <h4>DATOS DE LA ESTACIÓN</h4>
            </div>
             <br>
    


</div>

<br>
 <table class ="tablacaso" border="1">
              <tr><th style="width: 30%;">Nombre: </th>           <td><?= $model->nombre ?></td></tr>
              <tr><th>Cliente: </th>                      <td><?= ($model->idEmpresa->cliente->nombre_comercial) ?></td></tr>
              <tr><th>Código: </th>                                  <td><?= $model->codigo_generado ?></td></tr>
              <tr><th>Código Wizard: </th>                                  <td><?php if($model->codigo){echo $model->codigo; }else{echo 'No posee';} ?></td></tr>

              <!-- <tr><th>Coordinador: </th>                             <td><?= $model->coordinador ?></td></tr> -->
              <tr><th>Dirección: </th>                            <td><?= $model->direccion ?></td></tr>
              <tr><th>Coordenadas Google Maps: </th>              <td>

<?= $model->coord_google_maps ?>
 <br><br>
   <?= $form->field($model, 'coord_google_maps')->widget(MapInputWidget::ClassName(),[
                                                'latitude'=>19.0000000,
                                                'longitude'=>-70.6667000,
                                                'width'=>'100%', 
                                                'height'=>'200px', 
                                                'zoom'=> 7, 
                                                'animateMarker'=>true, 
                                                  'enableSearchBar' => false,
                                                'key'=>'AIzaSyA6MKTxmH6B2J6s9xydrJds1DxvlCxwARw'])->label(false)?>
       

              </td></tr>
              
           
               
      </table> 
 <?php ActiveForm::end(); ?>