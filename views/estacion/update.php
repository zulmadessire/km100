<?php

use yii\helpers\Html;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\widgets\InputWidget;
use app\models\Marca;
use app\models\Modelo;
use kolyunya\yii2\widgets\MapInputWidget;
use app\models\TipoServicio;


/* @var $this yii\web\View */
/* @var $model app\models\Estacion */

$this->title = 'ACTUALIZAR AGENCIA';
 
 
?>
<div class="estacion-update">
 <div class="row">
      <div class="col-md-1 col-xs-3">
        <h4 class="titulo"><i class="fa fa-home fa-4x" aria-hidden="true"></i></h4>
      </div>
      <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
        <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
      </div>
    </div>


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
