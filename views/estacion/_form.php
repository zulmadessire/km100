<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\db\Query;
use yii\web\view;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\file\FileInput;
use kartik\select2\Select2;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\widgets\InputWidget;
use app\models\Estacion;
use app\models\Empresa;
use app\models\Cliente;
use kartik\depdrop\DepDrop;
use kolyunya\yii2\widgets\MapInputWidget;
/* @var $this yii\web\View */
/* @var $model app\models\Estacion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="estacion-form">
    <?php $form = ActiveForm::begin(['id' => 'dynamic_lista-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row" style="border-bottom: 1px solid #222d32;">
        <div class="col-xs-12 col-md-6">
            <h4>DATOS DE LA ESTACIÓN</h4>
        </div>
        <div class="col-xs-12 col-md-6">
          <div align="right">
                <?php
                echo Html::a('Listado de Estaciones', ['index'], ['class' => 'btn btn-small-gris']);
                ?>
          </div>
        </div>
    </div>
    <br>

     <div class="row">

        <div class="col-xs-12 col-md-2">
            <?=
                $form->field($model, 'asoc_taller')->widget(Select2::classname(), [
                    'data' => ['0'=>'No', '1'=>'Si'],
                    'options' => ['placeholder' => 'Selecciona',],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('¿TIENE TALLER ASOCIADO?');
            ?>
        </div>
           <div class="col-xs-12 col-md-2">
            <label style="font-weight: normal;">CLIENTE</label>
                 <?=  Select2::widget([
                                    'name' => 'clienteSearch[id_cliente]',
                                     'data' => ArrayHelper::map(Cliente::find()->orderBy('nombre_comercial')->all(), 'id', 'nombre_comercial'),
                                    'options' => ['placeholder' => 'Selecciona el cliente', 'id' => 'id_cliente']
                                ]); ?>
 
        </div>

        <div class="col-xs-12 col-md-2">
             <?=
                $form->field($model, 'id_empresa')->widget(DepDrop::classname(), [
                    'type'=>DepDrop::TYPE_SELECT2,
                    'options'=>['id'=>'id_empresa', 'class' => 'form-control'],
                    'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                    'pluginOptions'=>[
                        'depends'=>['id_cliente'],
                        'placeholder'=>'Selecciona la empresa',
                        'url'=>Url::to(['/estacion/getempresa'])
                    ]
                ])->label('EMPRESA');
            ?>
        </div>
<!--         <div class="col-xs-12 col-md-3">
            <?php // $form->field($model, 'codigo')->textInput(['maxlength' => true])->label('CÓDIGO DE WIZARD') ?>
        </div> -->
             <div class="col-xs-12 col-md-2">
            <?= $form->field($model, 'nombre')->textInput(['maxlength' => true])->label('NOMBRE DE ESTACIÓN'); ?>
        </div>
        <div class="col-xs-12 col-md-3">
            <?=
                $form->field($model, 'audita')->widget(Select2::classname(), [
                    'data' => ['0'=>'No audita', '1'=>'Mensual', '2'=>'Continua'],
                    'options' => ['placeholder' => 'Selecciona',],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('¿frecuencia de auditoria?');
            ?>
        </div>
   
     </div>

     
  <div class="row">
        <div class="col-md-6">
    <?= $form->field($model, 'direccion')->textInput(['maxlength' => true]) ?>
        </div>
         <div class="col-md-6">
 
   <?= $form->field($model, 'coord_google_maps')->widget(MapInputWidget::ClassName(),[
                                                'latitude'=>19.0000000,
                                                'longitude'=>-70.6667000,
                                                'width'=>'100%', 
                                                'height'=>'200px', 
                                                'zoom'=> 7, 
                                                'animateMarker'=>true, 
                                                'pattern' => '%longitude%,%latitude%',
                                                'enableSearchBar' => true,
                                                'key'=>'AIzaSyA6MKTxmH6B2J6s9xydrJds1DxvlCxwARw'])?>
        </div>
     </div>


<br>
<br>
<br>
<br>
    <div class="form-group" style="text-align:center;">
        <?= Html::submitButton($model->isNewRecord ? 'Registrar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-submit' : 'btn btn-submit']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
 
<script type="text/javascript">
// setTimeout(function(){ $("#estacion-coord_google_maps").prop("disabled", false); }, 500);

                    <?php
 $this->registerJs(
       '          
           setTimeout(function(){ $("#estacion-coord_google_maps").prop("disabled", false); }, 1000);

       '
   );
?>
</script>
