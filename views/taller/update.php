<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Taller */

$this->title = 'MODIFICAR TALLER';
?>
<div class="taller-update">

      <div class="row">
        <div class="col-md-1 col-xs-3">
          <h4 class="titulo"><i class="fa fa-car fa-4x" aria-hidden="true"></i></h4>
        </div>
        <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
          <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
        </div>
      </div>
      </br>
    <?= $this->render('_form', [
        'model' => $model,
        'modelmodelo' => $modelmodelo,
        //'modelservicio' => $modelservicio,
        'acuerdo' => $acuerdo,
        'mercantil' => $mercantil
    ]) ?>

</div>
