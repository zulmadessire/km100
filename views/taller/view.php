<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Modelo;
use app\models\Marca;
use app\models\ServicioTaller;
use app\models\VehiculoTaller;
use app\models\TipoServicio;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use kolyunya\yii2\widgets\MapInputWidget;
/* @var $this yii\web\View */
/* @var $model app\models\Taller */

$this->title = 'INFORMACIÓN DE TALLER ';//.upper($model->nombre);
?>
<div class="taller-view">
 <?php $form = ActiveForm::begin(['id' => 'dynamic_lista-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>
      <div class="row">
        <div class="col-md-1 col-xs-3">
          <h4 class="titulo"><i class="fa fa-car fa-4x" aria-hidden="true"></i></h4>
        </div>
        <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
          <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
        </div>
      </div>
      </br>
      <div class="row" style="border-bottom: 1px solid #222d32;">
            <div class="col-xs-12 col-md-3">
                <h4>DATOS DEL TALLER</h4>
            </div>
            <div class="col-xs-12 col-md-9">
            <ul class="nav nav-pills pull-right">
              <li role="presentation">
              <?php
                    echo Html::a('Listado de talleres', ['index'], ['class' => 'btn btn-small-gris']);
                    ?>&nbsp;&nbsp;&nbsp;
              </li>
              <li role="presentation">
              <?php
              echo Html::a('Modificar taller', ['update', 'id' => $model->id_taller], ['class' => 'btn btn-small-gris']);
                    ?>&nbsp;&nbsp;&nbsp;
              </li>
              <li role="presentation">
              <?php
                    //echo Html::a('Asociar servicios', ['asociar', 'id' => $model->id_taller], ['class' => 'btn btn-small-gris']);
                    ?>
              </li>
            </ul>
            </div>
      </div>
      <br>
    <p>
      <table class ="tablacaso" border="1">
              <tr><th style="width: 30%;">Nombre: </th>           <td><?= $model->nombre ?></td></tr>
              <tr><th>Tipo de Persona: </th>                      <td><?= ($model->tipo_persona == 1) ? 'Jurídica' : 'Natural' ?></td></tr>
              <tr><th>RNC: </th>                                  <td><?= $model->rnc ?></td></tr>
              <tr><th>Teléfono: </th>                             <td><?= $model->telefono ?></td></tr>
              <tr><th>Email del taller: </th>                     <td><?= $model->email_taller ?></td></tr>
              <tr><th>Dirección: </th>                            <td><?= $model->direccion ?></td></tr>
              <tr><th>Coordenadas Google Maps: </th>              <td><?= $model->coord_google_maps?>
                                                             <?php echo $form->field($model, 'coord_google_maps')->widget(
    'kolyunya\yii2\widgets\MapInputWidget',
    [

   
        'key' => 'AIzaSyA6MKTxmH6B2J6s9xydrJds1DxvlCxwARw',
        'latitude' => 19.0000000,
        'longitude' => -70.6667000,
        'zoom' => 7,
        'width' => '100%',
        'height' => '200px',
        'pattern' => '[%longitude%-%latitude%]',
        'mapType' => 'roadmap',
        'animateMarker' => true,
        'enableSearchBar' => false,

    ]
)->label(false); ?>
              </td></tr>
              <tr><th>Tipo de Pago: </th>                         <td><?php if($model->tipo_pago == 0){ echo "Pronto pago";
                                                                            }else if($model->tipo_pago == 1){ echo "Crédito";
                                                                            }else{ echo "Contado"; }  ?></td></tr>
              <?php if($model->tipo_pago == 0){ ?>
                <tr><th>Descuento: </th>                          <td><?= $model->descuento ?></td></tr>
              <?php }else if($model->tipo_pago == 1){ ?>
                <tr><th>Límite de Crédito: </th>                  <td><?= $model->limite_credito ?></td></tr>
              <?php } ?>
              <tr><th>Nombre del representante: </th>             <td><?= $model->nombre_representante ?></td></tr>
              <tr><th>Número de identificación del representante: </th><td><?= $model->identif_representante ?></td></tr>
              <tr><th>Correo Electrónico del representante: </th> <td><?= $model->email_representante ?></td></tr>
              <tr><th>Celular del representante: </th>            <td><?= $model->celular_representante ?></td></tr>
              <tr><th>Espacios: </th>                             <td><?= $model->espacios ?></td></tr>
              <tr><th>Nivel de Servicio: </th>                    <td><?= $model->nivel_servicio ?></td></tr>
              <tr><th>Acuerdo Comercial: </th>                    <td><?= ($model->acuerdo_comercial) ? Html::a('Ver acuerdo comercial', ['descargar', 'dir' => $model->acuerdo_comercial], ['target'=>'_blank']) : '<span style="color:#999;">No aplica<span>' ?> - Fecha de vencimiento: <strong><?= $model->vencimiento_acuerdo ?></strong></td></tr>
              <tr><th>Registro Mercantil: </th>                    <td><?= ($model->registro_mercantil) ? Html::a('Ver registro mercantil', ['descargar', 'dir' => $model->registro_mercantil], ['target'=>'_blank']) : '<span style="color:#999;">No aplica<span>' ?> - Fecha de vencimiento: <strong><?= $model->vencimiento_rm ?></strong> </td></tr>
              <tr><th>Marcas -> Modelos Asociados</th>
                <td>
                <?php
                  $rma = VehiculoTaller::find()->where(['id_taller' => $model->id_taller])->All();
                  foreach ($rma as $key => $r) {
                    $modelo = Modelo::find()->where(['id_modelo' => $r->id_modelo])->One();
                    $marca = Marca::find()->where(['id_marca' => $modelo->id_marca])->One();
                    echo ''.$marca->nombre.' -> '.$modelo->nombre.'<br>';
                  }
                 ?>
               </td>
              </tr>
              <tr><th>Servicios Asociados <small class="label bg-green">Decimal</small></th>
                <td>
                <?php /*
                  $rma = ServicioTaller::find()->where(['id_taller' => $model->id_taller])->All();
                  $labor = [0 => 'Reparación', 1 => 'Salvamento-leve', 2 => 'Salvamento-fuerte'];
                  foreach ($rma as $key => $r) {
                    $tipo_s = TipoServicio::find()->where(['id_tipo_servicio' => $r->id_tipo_servicio])->One();
                    echo ''.$tipo_s->nombre.'   ------ '.$labor[$r->labor].' ------   <small class="label bg-green">'.$r->decimal.'</small> <br>';
                  }*/
                 ?>
               </td>
              </tr>
      </table><br>
  </div>
  <?php ActiveForm::end(); ?>
</div>
