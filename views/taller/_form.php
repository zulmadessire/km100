<?php
 
use yii\helpers\Html;
use yii\db\Query;
use yii\web\view;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\widgets\InputWidget;
use app\models\Marca;
use app\models\Modelo;
use kolyunya\yii2\widgets\MapInputWidget;
use app\models\TipoServicio;
use kartik\date\DatePicker;


/* @var $this yii\web\View */
/* @var $model app\models\Taller */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="taller-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic_lista-form']); ?>

    <!-- --DATOS DEL TALLER-- -->

    <div class="row" style="border-bottom: 1px solid #222d32;">
        <div class="col-xs-12 col-md-6">
            <h4>DATOS DEL TALLER</h4>
        </div>
        <div class="col-xs-12 col-md-6">
          <div align="right">
                <?php
                echo Html::a('Listado de talleres', ['index'], ['class' => 'btn btn-small-gris']);
                ?>
          </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12 col-md-3">
            <?php
                $tipos_persona = [0 => 'Natural', 1 => 'Jurídica'];
                echo $form->field($model, "tipo_persona")->dropDownList(
                    $tipos_persona,
                    [
                        'prompt'=>'Selecciona el tipo de persona',
                        'class' => 'form-control',
                    ]
                    );
            ?>
        </div>
        <div class="col-xs-12 col-md-3">
            <?= $form->field($model, 'rnc')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-12 col-md-3">
            <?php
                if(!$model->isNewRecord) {
            ?>
            <?= $form->field($model, 'nombre')->textInput(['maxlength' => true, 'class' => 'form-control']) ?>
            <?php
                }else {
            ?>
            <?= $form->field($model, 'nombre')->textInput(['maxlength' => true, 'class' => 'form-control nameuser']) ?>
                    
            <?php 
                }
            ?>
        </div>
        <div class="col-xs-12 col-md-3">
            <?= $form->field($model, 'email_taller')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
    <div class="col-xs-12 col-md-3">
    <?php
        $tipotaller = [0 => 'Quick Line', 1 => 'Normal Line', 2 => 'Taller km100'];
        echo $form->field($model, "tipo_taller")->dropDownList(
            $tipotaller,
            [
                'prompt'=>'Selecciona',
                'class' => 'form-control'
            ]
        );
    ?>
    </div>
        <div class="col-xs-12 col-md-3">
            <?php
                if(!$model->isNewRecord) {
            ?>
            <?= $form->field($model, 'user')->textInput(['maxlength' => true]) ?>

            <?php
                }else {
            ?>
            <?= $form->field($model, 'user', [
                'inputTemplate' => '<div class="input-group"><span class="input-group-addon">rsa.</span>{input}</div>',
            ])->textInput(['id' => 'field-taller-user', 'class' => 'username form-control']) ?>
            <?php
                }
            ?>
        </div>
        <div class="col-xs-12 col-md-3">
            <?= $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-12 col-md-3">
            <?= $form->field($model, 'tlf1')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-4">
            <?= $form->field($model, 'tlf2')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-12 col-md-4">
            <?= $form->field($model, 'direccion')->textInput(['maxlength' => true]) ?>
        </div>
                  <div class="col-md-4">
<!--  
   <?= $form->field($model, 'coord_google_maps')->widget(MapInputWidget::ClassName(),[
                                                'latitude'=>19.0000000,
                                                'longitude'=>-70.6667000,
                                                'width'=>'100%', 
                                                'height'=>'200px', 
                                                'zoom'=> 7, 
                                                'animateMarker'=>true, 
                                                'enableSearchBar' => true,
                                                'key'=>'AIzaSyA6MKTxmH6B2J6s9xydrJds1DxvlCxwARw'])?> -->


    <?php echo $form->field($model, 'coord_google_maps')->widget(
    'kolyunya\yii2\widgets\MapInputWidget',
    [

   
        'key' => 'AIzaSyA6MKTxmH6B2J6s9xydrJds1DxvlCxwARw',
        'latitude' => 19.0000000,
        'longitude' => -70.6667000,
        'zoom' => 7,
        'width' => '100%',
        'height' => '200px',
        'pattern' => '%longitude%,%latitude%',
        'mapType' => 'roadmap',
        'animateMarker' => true,
        'enableSearchBar' => true,

    ]
); 
    
?>
        </div>
     </div>
     <br>
     <br>
     <div class="row">
         <div class="col-xs-12 col-md-4">
            <?= $form->field($model, 'garantia_global')->textInput(['maxlength' => true]) ?>
         </div>
         <div class="col-xs-12 col-md-4">
                <?= $form->field($model, 'espacios')->textInput(['maxlength' => true]) ?>
        </div>
     </div>
     <div class="row">
        <div class="col-xs-12 col-md-4">
            <?php
                $tipos_servicios = [0 => 'Desabolladura y Pintura', 1 => 'Mecánica', 2 => 'Desabolladura y Pintura / Mecánica'];
                echo $form->field($model, "tipo_servicio_taller")->dropDownList(
                    $tipos_servicios,
                    [
                        'prompt'=>'Selecciona',
                        'class' => 'form-control',
                        'onchange' => ' if(($(this).val() == 0)){
                                          document.getElementById("desabolladura").style.display="block";
                                          document.getElementById("mecanica").style.display="none";
                                          document.getElementById("precio-mec").value="";
                                        }else if(($(this).val() == 1)){
                                          document.getElementById("desabolladura").style.display="none";
                                          document.getElementById("mecanica").style.display="block";
                                          document.getElementById("precio-des").value="";
                                        }else if(($(this).val() == 2)){
                                          document.getElementById("desabolladura").style.display="block";
                                          document.getElementById("mecanica").style.display="block";
                                        }',
                    ]
                    );
            ?>
        </div>
        <div class="col-xs-12 col-md-4" id="desabolladura" style="display: <?= (!$model->isNewRecord && ($model->tipo_servicio_taller==0 || $model->tipo_servicio_taller ==2))?'block;':'none;' ?>)">
            <?= $form->field($model, 'precio_pieza',[
                'inputTemplate' => '<div class="input-group"><span class="input-group-addon">RD$</span>{input}</div>',
            ])->textInput(['maxlength' => true, 'class' => 'form-control positive-integer', 'id' => 'precio-des']) ?>
        </div>
        <div class="col-xs-12 col-md-4" id="mecanica" style="display:<?= (!$model->isNewRecord && ($model->tipo_servicio_taller==1 || $model->tipo_servicio_taller ==2))?'block;':'none;' ?>)">
            <?= $form->field($model, 'precio_pieza_mecanica',[
                'inputTemplate' => '<div class="input-group"><span class="input-group-addon">RD$</span>{input}</div>',
            ])->textInput(['maxlength' => true, 'class' => 'form-control positive-integer', 'id' => 'precio-mec']) ?>
         </div>
     </div>
    <div class="row">
        
        <div class="col-xs-12 col-md-4">
            <?php
                $tipos_pago = [0 => 'Pronto pago', 1 => 'Crédito', 2 => 'Contado'];
                echo $form->field($model, "tipo_pago")->dropDownList(
                    $tipos_pago,
                    [
                        'prompt'=>'Selecciona el tipo de pago',
                        'class' => 'form-control',
                        'onchange' => ' if(($(this).val() == 0)){
                                          document.getElementById("sec_descuento").style.display="block";
                                          document.getElementById("sec_credito").style.display="none";
                                          document.getElementById("dias").style.display="none";
                                          document.getElementById("field_limite_credito").value="";
                                        }else if(($(this).val() == 1)){
                                          document.getElementById("sec_descuento").style.display="none";
                                          document.getElementById("sec_credito").style.display="block";
                                          document.getElementById("dias").style.display="block";
                                          document.getElementById("field_descuento").value="";
                                        }else{
                                          document.getElementById("sec_descuento").style.display="none";
                                          document.getElementById("sec_credito").style.display="none";
                                          document.getElementById("dias").style.display="none";
                                          document.getElementById("field_descuento").value="";
                                          document.getElementById("field_limite_credito").value="";
                                        }',
                        'id' => 'field_tipo_pago'
                    ]
                    );
            ?>

        </div>
        <div class="col-xs-12 col-md-4">
          <?php if(!$model->isNewRecord && $model->tipo_pago==0){ ?>
          <div id="sec_descuento">
          <?php }else{ ?>
          <div id="sec_descuento" style="display: none;">
          <?php } ?>
            <?= $form->field($model, 'descuento', [
                'inputTemplate' => '<div class="input-group"><span class="input-group-addon">%</span>{input}</div>',
            ])->textInput(['id' => 'field_descuento']) ?>
          </div>

          <?php if(!$model->isNewRecord && $model->tipo_pago==1){ ?>
          <div id="sec_credito">
          <?php }else{ ?>
          <div id="sec_credito" style="display: none;">
          <?php } ?>
            <?= $form->field($model, 'limite_credito', [
                'inputTemplate' => '<div class="input-group"><span class="input-group-addon">RD$</span>{input}</div>',
            ])->textInput(['id' => 'field_limite_credito', 'class' => 'form-control positive-integer']) ?>
          </div>

        </div>
        <div class="col-xs-12 col-md-4" id="dias" style="display: none;">

            <?= $form->field($model, 'dias', [
                'inputTemplate' => '<div class="input-group"><span class="input-group-addon">Días</span>{input}</div>',
            ])->textInput(['id' => 'field_limite_credito']) ?>

        </div>
    </div>

    <!-- --DATOS DE REPRESENTANTE LEGAL-- -->
    <br>
    <div class="row" style="border-bottom: 1px solid #222d32;">
        <h4>DATOS DE REPRESENTANTE LEGAL</h4>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12 col-md-4">
            <?= $form->field($model, 'nombre_representante')->textInput() ?>
        </div>
        <div class="col-xs-12 col-md-4">
            <?= $form->field($model, 'identif_representante')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-12 col-md-4">
            <?= $form->field($model, 'email_representante')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-4">
            <?= $form->field($model, 'celular_representante')->textInput() ?>
        </div>
    </div>




    <!-- --ASOCIACIÓN DE VEHÍCULOS-- -->
    <br>
    <div class="row" style="border-bottom: 1px solid #222d32;">
        <h4>ASOCIACIÓN DE VEHÍCULOS</h4>
    </div>
    <br>
<?php DynamicFormWidget::begin([
        'widgetContainer' => 'dynamicform_wrapper_resultados',
        'widgetBody' => '.container-items',
        'widgetItem' => '.item',
        'limit' => 50,
        'min' => 1,
        'insertButton' => '.add-item',
        'deleteButton' => '.remove-item',
        'model' => $modelmodelo[0],
        'formId' => 'dynamic_lista-form',
        'formFields' => [
            'nombre',
            'id_modelo',
            'id_marca',
        ],
  ]); ?>
  <table class="table table-hover">
      <tbody class="container-items">
      <?php foreach ($modelmodelo as $i => $modelmodel): ?>
          <tr class="item panel panel-default">
              <td>
                  <div class="col-xs-12 col-md-4"><?php
                      $marcas = ArrayHelper::map(Marca::find()->orderBy('nombre')->all(), 'id_marca', 'nombre');
                      echo $form->field($modelmodel, "[{$i}]id_marca")->dropDownList(
                              $marcas,
                              [
                                  'prompt'=>'Selecciona la marca',
                                  'class' => 'form-control',
                                  'onchange'=>'
                                      var ident = $(this).attr("id");
                                      var nom = ident.split("-");
                                      var nmodelo = "modelo-"+nom[1]+"-id_modelo";
                                      console.log("id"+nmodelo);
                                      $.get( "'.Url::toRoute('caso/listarmodelos').'", { id: $(this).val() } )
                                          .done(function( data ) {
                                              $( "#"+nmodelo ).html( null );
                                              $( "#"+nmodelo ).html( data );
                                          }
                                      );
                                  ',
                              ] )->label('MARCA'); ?>
                      </div>
                  <div class="col-xs-12 col-md-7">
                    <?php
                      $modelos=array(); //echo "trae: {$i} --- ";
                      if(!$modelmodel->isNewRecord){
                          $modelos = ArrayHelper::map(Modelo::find()->where(['id_marca'=>$modelmodel->id_marca])->orderBy('nombre')->all(), 'id_modelo', 'nombre');
                      }
                      echo $form->field($modelmodel, "[{$i}]id_modelo")->dropDownList(
                          $modelos,
                          [
                              'prompt'=>'Selecciona el modelo',
                              'class' => 'form-control']
                          )->label('MODELO');
                    ?>
                  </div>

                  <div class="col-xs-12 col-md-1 text-center">
                      <button type="button" class="remove-item btn btn-cancelar btn-xs" style="margin-top:28px;">
                          <i class="glyphicon glyphicon-minus"></i></button>
                  </div>
              </td>
          </tr>
      <?php endforeach; ?>
  <?php DynamicFormWidget::end(); ?>
      </tbody>
  </table>
<br>
  <button type="button" class="add-item btn btn-accion  btn-xs pull-right">Agregar marca</button>
  <br>
  <br>
    <!-- --DOCUMENTOS-- -->
    <br>
    <div class="row" style="border-bottom: 1px solid #222d32;">
        <h4>DOCUMENTOS</h4>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12 col-md-8">
            <?php
            echo $form->field($acuerdo, 'imageFiles[]')->fileInput(['multiple' => true, 'accept' => 'image/*', 'class' =>"form-control btn btn-default btn-md"]);
            ?>
        </div>
        <div class="col-xs-12 col-md-4">
            <?=
                $form->field($model, 'vencimiento_acuerdo')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'Selecciona la fecha'],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd',
                        'orientation' => 'bottom left',
                    ]
                ]);
            ?>
        </div>
    </div>
    <br>
        <!-- Datos registro mercantil -->     
    <div class="row">  
        <div class="col-xs-12 col-md-8">
            <?php
               echo $form->field($mercantil, 'imageFiles[]')->fileInput(['multiple' => true, 'accept' => 'image/*', 'class' =>"form-control btn btn-default btn-md"]);
            ?>
        </div>                  
        <div class="col-xs-12 col-md-4">
            <?=
                $form->field($model, 'vencimiento_rm')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'Selecciona la fecha'],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd',
                        'orientation' => 'bottom left',
                    ]
                ]);
            ?>
        </div>
    </div>

    <br>
    <br>
    <br>
    <br>
    <?php
        if(!$model->isNewRecord){
        ?>
            <div class="row" align="center">
                <div>
                    <?= Html::a('CANCELAR', ['view', 'id' => $model->id_taller], ['class' => 'btn btn-cancelar']) ?>
                    &nbsp;&nbsp;&nbsp;
                    <?= Html::submitButton('GUARDAR CAMBIOS', ['class' => 'btn btn-primario']) ?>
                </div>
            </div>
        <?php
        }else{
        ?>
            <div class="form-group" align="center">
                <?= Html::submitButton('GUARDAR', ['class' => 'btn btn-primario']) ?>
            </div>
        <?php
        }
    ?>

    <?php ActiveForm::end(); ?>

</div>
<?php
  $this->registerJs(
        '
            $(".positive-integer").numeric(".");   

            $( ".nameuser" ).keyup(function() {
                var name = $(this).val();
                var userName = name.split(" ").join(".").toLowerCase();
                $(".username").val(userName);                    
            });   
            
            
            setTimeout(function(){ $("#taller-coord_google_maps").prop("disabled", false); }, 1000);

        '
    );
 ?>
