<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Marca;
use app\models\Modelo;
use yii\helpers\ArrayHelper;
use app\models\ServicioTaller;
use app\models\TipoServicio;
use app\models\VehiculoTaller;
use kartik\form\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TallerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'LISTADO DE TALLERES';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="taller-index">

  <div class="row">
    <div class="col-md-1 col-xs-3">
      <h4 class="titulo"><i class="fa fa-car fa-4x" aria-hidden="true"></i></h4>
    </div>
    <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
      <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
    </div>
  </div>
  </br>
    <div class="row">
        <div class="col-md-12">
            <?php
              echo Html::a('Registrar Taller', ['create'], ['class' => 'btn btn-small-gris', 'style' => 'float:right;']);
            ?>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => ['class' => 'table table-responsive'],
                'summary' => '',
                'options' =>[
                                'class' => 'grid',
                            ],
                'columns' => [
            
                    [
                        'attribute' => 'nombre',
                        'label' => 'Nombre',
                        'value' => 'nombre',
                        'format' => 'text',
                        'filter' => true,
                        'enableSorting' => false,
                    ],
                    [
                     
                        'label' => 'Marcas de vehículos',
                        'format' => 'html',
                        'enableSorting' => false,
                        'value' => function ($model) {
                                    $modelos = '';
                                    $v_taller = VehiculoTaller::find()->where(['id_taller' => $model->id_taller])->All();
                                    foreach ($v_taller as $taller) {
                                      $modelo = Modelo::find()->where(['id_modelo' => $taller->id_modelo])->One();
                                      $marca = Marca::find()->where(['id_marca' => $modelo->id_marca])->One();
                                      $modelos.='('.$marca->nombre.') '.$modelo->nombre.'<br>';
                                    }
                                    return $modelos;
                        },

                        'filter' => Select2::widget([
                            'name' => 'TallerSearch[id_marca]',
                            // 'data' => ArrayHelper::map(Marca::find()->joinWith('Modelo')->orderBy('nombre')->all(), 'id_marca', 'nombre'),
                            'data' => ArrayHelper::map(Marca::find()->orderBy('nombre')->all(), 'id_marca', 'nombre'),
                            'hideSearch' => true,
                            'options' => ['placeholder' => 'Marcas',],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]),
                    ],
                    /*[

                        'label' => 'Servicios',
                        'format' => 'html',
                        'value' => function ($model) {
                                    $servis = '';
                                    $s_taller = ServicioTaller::find()->where(['id_taller' => $model->id_taller])->All();
                                    foreach ($s_taller as $taller) {
                                      $tipo_serv = TipoServicio::find()->where(['id_tipo_servicio' => $taller->id_tipo_servicio])->One();
                                      $servis.= $tipo_serv->nombre.'<br>';
                                    }
                                    return $servis;
                        },
                        'filter' => Select2::widget([
                            'name' => 'TallerSearch[id_tipo_servicio]',
                            // 'data' => ArrayHelper::map(Marca::find()->joinWith('Modelo')->orderBy('nombre')->all(), 'id_marca', 'nombre'),
                            'data' => ArrayHelper::map(TipoServicio::find()->orderBy('nombre')->all(), 'id_tipo_servicio', 'nombre'),
                            'hideSearch' => true,
                            'options' => ['placeholder' => 'Servicios',],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                             ]),
                    ],*/
                    [
                        'attribute' => 'espacios',
                        'label' => 'Espacios',
                        'value' => 'espacios',
         
                        'filter' => true,
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'nivel_servicio',
                        'label' => 'Nivel de Servicio',
                        'filter' => true,
                        'enableSorting' => false,
                        'value' => function ($model) {
                          return "-";
                        },
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header'=>'',
                        'template' => '{menu}',
                        'buttons' => [
                            'menu' => function ($url, $model) {
                                 return  '<div class="dropdown">
                                          <button class="btn btn-default dropdown-toggle btn-sm" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i>
                                          <span class="caret"></span></button>
                                          <ul class="dropdown-menu">
                                            <li>'.Html::a('<span><i class="fa fa-eye fa-sm" aria-hidden="true"></i></span> Ver detalle',[ '/taller/view', 'id' => $model->id_taller ]).' </li>
                                            <li>'.Html::a('<span><i class="fa fa-pencil fa-sm" aria-hidden="true"></i></span> Modificar datos',[ '/taller/update', 'id' => $model->id_taller]).' </li>
                                          </ul>
                                        </div>';


                            },
                        ],
                   ],
                ],
            ]); ?>
        </div>
    </div>
  </div>
 
</div>




