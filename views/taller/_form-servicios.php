<?php

use yii\helpers\Html;
use yii\db\Query;
use yii\web\view;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;

use app\models\Marca;
use app\models\Modelo;
use app\models\TipoServicio;

$this->title = 'ASOCIACIÓN DE SERVICIOS';
?>

<div class="taller-form">
  <?php $form = ActiveForm::begin(['id' => 'dynamic_lista-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>

      <div class="row">
        <div class="col-md-1 col-xs-3">
          <h4 class="titulo"><i class="fa fa-car fa-4x" aria-hidden="true"></i></h4>
        </div>
        <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
          <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
        </div>
      </div>
      </br>
      <!-- --ASOCIACIÓN DE SERVICIOS-- -->
      <br>
      <div class="row" style="border-bottom: 1px solid #222d32;">
            <div class="col-xs-12 col-md-6">
                <h4>DATOS DE SERVICIOS</h4>
            </div>
            <div class="col-xs-12 col-md-6">
              <div align="right">
                    <?php
                    echo Html::a('Listado de talleres', ['index'], ['class' => 'btn btn-small-gris']);
                    ?>&nbsp;&nbsp;&nbsp;<?php
                    echo Html::a('Ver taller', ['update', 'id' => $model->id_taller], ['class' => 'btn btn-small-gris']);
                    ?>
              </div>
            </div>
      </div>
      <br>

    <?php
        echo $form->field($model, "id_taller")->hiddenInput()->label(false);

        DynamicFormWidget::begin([
          'widgetContainer' => 'dynamicform_wrapper_resultados',
          'widgetBody' => '.container-items2',
          'widgetItem' => '.item2',
          'limit' => 50,
          'min' => 1,
          'insertButton' => '.add-item2',
          'deleteButton' => '.remove-item2',
          'model' => $modelservicio[0],
          'formId' => 'dynamic_lista-form',
          'formFields' => [
              'id_tipo_servicio',
              'garantia',
              'linea',
          ],
    ]); ?>
    <table class="table table-hover">
        <tbody class="container-items2">
        <?php foreach ($modelservicio as $j => $modelserv): ?>
            <tr class="item2 panel panel-default">
                <td>
                    <div class="col-xs-12 col-md-3"><?php
                        $tip_serv = ArrayHelper::map(TipoServicio::find()->orderBy('nombre')->all(), 'id_tipo_servicio', 'nombre');
                        echo $form->field($modelserv, "[{$j}]id_tipo_servicio")->dropDownList(
                                $tip_serv,
                                [
                                    'prompt'=>'Selecciona el tipo de servicio',
                                    'class' => 'form-control',
                                ] ); ?>
                        </div>
                         <div class="col-xs-12 col-md-3">
                   
                         <?php
                          $lineas = [0 => 'Reparación', 1 => 'Salvamento-leve', 2 => 'Salvamento-fuerte'];
                          echo $form->field($modelserv, "[{$j}]labor")->dropDownList(
                              $lineas,
                              [
                                  'prompt'=>'Selecciona una labor',
                                  'class' => 'form-control']
                              );
                      ?>
                    </div>
                    <div class="col-xs-12 col-md-2">
                    <?= $form->field($modelserv, "[{$j}]decimal")->textInput(['maxlength' => true, 'class' => 'form-control positive-integer']) ?>

                    </div>
                    <div class="col-xs-12 col-md-4">
                      
                    </div>

                    <div class="col-xs-12 col-md-1 text-center">
                        <button type="button" class="remove-item2 btn btn-cancelar btn-xs" style="margin-top:28px;">
                            <i class="glyphicon glyphicon-minus"></i></button>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
    <?php DynamicFormWidget::end(); ?>
        </tbody>
    </table>

    <button type="button" class="add-item2 btn btn-accion  btn-xs pull-right">Agregar servicio</button>
    <br>
    <br>
    <div class="row" align="center">
        <div>
            <?= Html::a('CANCELAR', ['view', 'id' => $model->id_taller], ['class' => 'btn btn-cancelar']) ?>
            &nbsp;&nbsp;&nbsp;
            <?= Html::submitButton('GUARDAR', ['class' => 'btn btn-primario']) ?>
        </div>
    </div>
</div>

    <?php ActiveForm::end(); ?>

    <?php
  $this->registerJs(
        ' $(".positive-integer").numeric(".");   
        '
    );
 ?>