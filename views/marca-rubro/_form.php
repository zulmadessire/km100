<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model app\models\MarcaRubro */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="marca-rubro-form">

      <?php $form = ActiveForm::begin(['id' => 'dynamic_lista-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>

        <!-- --DATOS DE LA MARCA-- -->
        <br>
        <div class="row" style="border-bottom: 1px solid #222d32;">
            <h4>DATOS DE LA MARCA</h4>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-12 col-md-5">
              <?= $form->field($model, 'nombre')->textInput(['maxlength' => true])->label('Marca') ?>
            </div>
            <div class="col-xs-12 col-md-7">
              <?php echo '<label class="control-label">Modelos</label>'; ?>
              <?php DynamicFormWidget::begin([
                      'widgetContainer' => 'dynamicform_wrapper_resultados',
                      'widgetBody' => '.container-items',
                      'widgetItem' => '.item',
                      'limit' => 50,
                      'min' => 1,
                      'insertButton' => '.add-item',
                      'deleteButton' => '.remove-item',
                      'model' => $modelrubro[0],
                      'formId' => 'dynamic_lista-form',
                      'formFields' => [
                          'nombre',
                          'id_modelo',
                          'id_marca',
                      ],
                ]); ?>
                <table class="table table-hover">
                    <tbody class="container-items" style="padding: 0px;">
                    <?php foreach ($modelrubro as $i => $modelrub): ?>
                        <tr class="item panel panel-default">
                            <td style="padding: 0px;">
                                <div class="col-xs-12 col-md-10">
                                  <?= $form->field($modelrub, "[{$i}]nombre")->textInput(['maxlength' => true])->label(false) ?>
                                </div>
                                <div class="col-xs-12 col-md-2 text-center" style="margin-top: -25px;">
                                    <button type="button" class="remove-item btn btn-cancelar btn-xs" style="margin-top:28px;">
                                        <i class="glyphicon glyphicon-minus"></i></button>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php DynamicFormWidget::end(); ?>
                    </tbody>
                </table>

                <button type="button" class="add-item btn btn-accion  btn-xs pull-right">Agregar modelo</button>
                <br>

            </div>
        </div>
        
        <div class="row" align="center">
            <div>
                <?= Html::a('CANCELAR', ['/rubro/create'], ['class' => 'btn btn-cancelar']) ?>
                &nbsp;&nbsp;&nbsp;
                <?= Html::submitButton('REGISTRAR', ['class' => 'btn btn-primario']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>

</div>
