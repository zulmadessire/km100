<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MarcaRubro */

$this->title = 'Update Marca Rubro: ' . $model->id_marca_rubro;
$this->params['breadcrumbs'][] = ['label' => 'Marca Rubros', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_marca_rubro, 'url' => ['view', 'id' => $model->id_marca_rubro]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="marca-rubro-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
