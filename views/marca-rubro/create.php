<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MarcaRubro */

$this->title = 'REGISTRAR NUEVA MARCA DE RUBRO';
?>
<div class="marca-rubro-create">

        <h3><strong><span class="fa fa-tint"></span> <?= Html::encode($this->title) ?></strong></h3>
        <br>
    <?= $this->render('_form', [
        'model' => $model,
        'modelrubro' => $modelrubro
    ]) ?>

</div>
