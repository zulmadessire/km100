<?php
 
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Estacion;
use kartik\select2\Select2;
use yii\widgets\Pjax;
use miloschuman\highcharts\Highcharts;

$this->title = 'Indicador - Fuera de servicio en estación'; ?>
<h2>
    <?= Html::encode($this->title) ?>
</h2>

<?php $form = ActiveForm::begin(); ?>
 
<div class="row" style="display: flex; align-items: center;">
       <div class="col-md-3">
        <?= $form->field($modelIndicador, 'estacion')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(Estacion::find()->all(), 'id_estacion', 'nombre'),
                        //'value' => ($model->id_estacion)?$model->id_estacion:'',
                        'options' => ['placeholder' => 'Selecciona la estacion', 'id' => 'id_estacion'],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ])->label('ESTACIÓN'); ?>
    </div> 
    <div class="col-md-3">
        <?= $form->field($modelIndicador, 'mes')->dropDownList([
            '01' => 'Enero',
            '02' => 'Febrero',
            '03' => 'Marzo',
            '04' => 'Abril',
            '05' => 'Mayo',
            '06' => 'Junio',
            '07' => 'Julio',
            '08' => 'Agosto',
            '09' => 'Septiembre',
            '10' => 'Octubre',
            '11' => 'Noviembre',
            '12' => 'Diciembre',
        ], [
            'prompt' => 'Mes', 
            'class' => 'input-lg form-control',
        ]); ?>
    </div>
 
 
    <div class="col-md-3">
        <?= $form->field($modelIndicador, 'ano')->dropDownList([
            '2017' => '2017',
            '2018' => '2018'
        ], [
            'prompt' => 'Año', 
            'class' => 'input-lg form-control'
        ])->label('AÑO'); ?>
    </div>
    <div class="col-md-2 col-md-offset-1">
        <?= Html::submitButton('Consultar', ['class' =>'btn btn-kraken btn-lg']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<br>

<?php $estacion = Estacion::find( )->where(['id_estacion' => $modelIndicador->estacion])->one(); ?>
 
<?php 
if (sizeof($estaciones) > 0) {
    echo Highcharts::widget(['options' => [
        'chart' => ['type' => 'column'],
        'title' => ['text' => ' '],
        'subtitle' => ['text' => ' '],
        'credits' => ['enabled' => false],

        'xAxis' => [
            'categories' => $estaciones,
            'title' => [
                'text' => null
            ]
        ],
        'yAxis' => [
            'min' => 0,
            'title' => [
                'text' => 'Fuera de servicio',
                'align' => 'high'
            ],
            'labels' => [
                'overflow' => 'justify'
            ]
        ],
        'tooltip' => [
            'valueSuffix' => ' Vehículos'
        ],
         'plotOptions' => [
            'column' => [
                'pointPadding' => 0.1,
                'borderWidth' => 0
            ]
        ],
        'legend' => [
            'layout' => 'vertical',
            'align' => 'right',
            'verticalAlign' => 'top',
            'x' => -40,
            'y' => 80,
            'floating' => true,
            'borderWidth' => 1,
            'backgroundColor' => (('Highcharts.theme && Highcharts.theme.legendBackgroundColor') || '#FFFFFF'),
            'shadow' => true
        ],

        'series' => [
            [
                'name' => 'Estación:' . ucfirst($estacion['nombre']),
                'data' => $total,
                'color' => '#C3FF68'

            ],
            [
            'name' => 'Nro. OC abiertas en el mes seleccionado',
            'data' => $total4,
            'color' => '#4F81BD'

            ],
        [
            'name' => 'Nro. OC abiertas en el año seleccionado',
            'data' => $total5,
            'color' => '#FF0000'

        ]

        ]    
    ]]);
} else {
    echo '
        <br><br>
        <div class="row" style="display: flex; justify-content: center;">
            <div class="col-md-4">
                <h3>No hay resultados para mostrar.</h3>
            </div>
        </div>
    ';
}
?>



<?php 


 ?>