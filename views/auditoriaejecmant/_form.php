<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Auditoriaejecmant */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="auditoriaejecmant-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tecnico')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mva_vehiculo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_tipo_mnto')->textInput() ?>

    <?= $form->field($model, 'estado')->textInput() ?>

    <?= $form->field($model, 'fecha')->textInput() ?>

    <?= $form->field($model, 'id_estacion')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
