<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Auditoriaejecmant */

$this->title = 'Update Auditoriaejecmant: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Auditoriaejecmants', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="auditoriaejecmant-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
