<?php

use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel app\models\AuditoriasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'REGISTRO DE AUDITORIA - EJECUCIÓN DE MTTO ';
?>
<div class="auditoria-index">
      <div class="row">
    <div class="col-md-1 col-xs-3">
      <h4 class="titulo"><i class="fa fa-list-ul fa-4x" aria-hidden="true"></i></h4>
    </div>
    <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
      <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
    </div>
  </div>
 <span>Total de estaciones sin mantenimiento: <?= $dataProvider->getTotalCount() ?></span>

 </br>
  <div class="row">
      <?php
          echo Html::a('Historico de Auditorias', ['create'], ['class' => 'btn btn-accion', 'style' => 'float:right;']);
      ?>
  </div>

 


  <br>
 
    <?= GridView::widget([
           'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
         'tableOptions' => ['class' => 'table table-responsive'],
               'summary' => '',
                    'options' =>[
                        'class' => 'grid',
                    ],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],




             [
                            'attribute' => 'empresa_id_empresa',
                            'label' => 'Empresa',
                            'format' => 'text',
                            'filter' => true,
                            'enableSorting' => false,
                            'value' => function( $model ){
                                return $model->idEmpresa->nombre;
                            }
                            //'headerOptions' => ['width' => '300',],
            ],
      
          [
                            'label' => 'Agencia',
                            'format' => 'raw',
                            'filter' => false,
                            'enableSorting' => false,
                           'value' => function ($model, $key, $index, $column) {
                              
                               return $model->nombre;
                                },
            ],

          [
                            'label' => 'Fuera de Servicio',
                            'format' => 'raw',
                            'filter' => false,
                            'enableSorting' => false,
                           'value' => function ($model, $key, $index, $column) {

                            $acumulador = 0;

                                  foreach ($model->servicios as $value) {
                                    
                                      foreach ($value->solicitudServicioTallers as $value2) {
                                        
                                          foreach ($value2->cotizacionTallers as $value3) {
                                            
                                              foreach ($value3->ordenTrabajos as $value4) {
                                                
                                                  foreach ($value4->solicitudCompraPiezas as $value5) {
                                                      
                                                      foreach ($value5->solicitudCompraProveedors as $value6) {
                                                        
                                                          foreach ($value6->cotizacionProveedors as $value7) {
                                                            
                                                               foreach ($value7->ordenCompraPiezas as $value7) {
                                                                
                                                                  $acumulador = $acumulador+1;
                                                               }
                                                          }
                                                      }
                                                  }
                                              }
                                          }
                                      }
                                  }

                              
                               return $acumulador;
                                },
            ],
              [
                            'label' => 'Mttos. Pendientes',
                            'format' => 'raw',
                            'filter' => false,
                            'enableSorting' => false,
                           'value' => function ($model, $key, $index, $column) {
                              $contador=0;
                                foreach ($model->servicios as $value) {
                                  foreach ($value->idVehiculo->proyeccionMantenimientos as $value2) {
                                     if($value2->realizado==1){
                                        $contador++;
                                     }
                                  }
                                }
                              
                               return $contador;
                                },
            ],

                     [  
                            'class' => 'yii\grid\ActionColumn',
                            //'contentOptions' => ['style' => 'width:260px;'],
                            'header'=>'Acciones',
                            'template' => '{menu}',
                            'buttons' => [

                                //view button
                                'menu' => function ($url, $model) {

                                     return  '<div class="dropdown">
                                              <button class="btn btn-default dropdown-toggle btn-sm" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i>
                                              <span class="caret"></span></button>
                                              <ul class="dropdown-menu">
                                               <li>'.Html::a('<span><i class="fa fa-file-text fa-sm" aria-hidden="true"></i></span> Registrar Auditoria',[ '/auditoriaejecmant/auditoriaejec', 'id' => $model->id_estacion]).' </li>
                                                <li>'.Html::a('<span><i class="fa fa-history fa-sm" aria-hidden="true"></i></span> Historico de Auditorias',[ '/taller/view', 'id' => $model->id_estacion ]).' </li>
                                              </ul>
                                            </div>';


                                },
                            ],

                       ],

            
        ],
    ]); ?>
</div>
