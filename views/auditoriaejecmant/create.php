<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Auditoriaejecmant */

$this->title = 'Create Auditoriaejecmant';
$this->params['breadcrumbs'][] = ['label' => 'Auditoriaejecmants', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auditoriaejecmant-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
