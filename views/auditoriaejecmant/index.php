<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AuditoriaejecmantSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Auditoriaejecmants';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auditoriaejecmant-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Auditoriaejecmant', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'tecnico',
            'mva_vehiculo',
            'id_tipo_mnto',
            'estado',
            // 'fecha',
            // 'id_estacion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
