<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\TipoMantenimiento;
/*use yii\widgets\ActiveForm;*/
use yii\db\Query;
use kartik\form\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Vehiculo */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Registro de auditoría - Ejecución de Mantenimientos';
//$this->params['breadcrumbs'][] = $this->title;

 
?>
<div class="Audiorias">

  <div class="row">
    <div class="col-md-1 col-xs-3">
      <h4 class="titulo"><i class="fa fa-list-ul fa-4x" aria-hidden="true"></i></h4>
    </div>
    <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
      <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
    </div>
  </div>
 
    <br>
    
    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DEL CLIENTE</p>
             <div style = "margin-top: 1px !important;
    border-top: 1px solid #222d32 !important;"><br><br></div>
        </div>
    </div>


    <div class="row">
       <?php $form = ActiveForm::begin(['id' => 'dynamic_lista-form']); ?>
         <div class="col-md-4 col-xs-12">
            <?= $form->field($model_est, 'empresa')->textInput(['disabled' => true, 'value' => $model_est->idEmpresa->nombre])->label('Empresa') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'nombre')->textInput(['value' => strtoupper($model_est->nombre), 'disabled' => true])->label('Estación') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'fecha')->textInput(['value' => date('Y-m-d'),'maxlength' => true, 'disabled' => true]) ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'tecnico')->textInput(['maxlength' => true, 'disabled' => false])->label('TÉCNICO'); ?>
        </div>
         <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'mva_vehiculo')->textInput(['maxlength' => true, 'disabled' => false])->label('MVA EL VEHÍCULO'); ?>
        </div>
         <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'id_tipo_mnto')->dropDownList(
                ArrayHelper::map(TipoMantenimiento::find()->all(), 'id_tipo_mnto' , 'nombre'),
                    ['prompt' => 'Seleccione el tipo de mantenimiento', 'class' => 'form-control'
                     
                        
                    ]
            )->label('TIPO DE MANTENIMIENTO');   ?>
        </div>
            <div class="row">
        <div class="col-xs-12">
            <br>
            <p class="titulo-form " >ASPECTOS A EVALUAR</p>
            <div style = "margin-top: 1px !important;
    border-top: 1px solid #222d32 !important;"><br><br></div>
        
        </div>
    </div>

      <div class="col-md-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
     
                'tableOptions' => ['class' => 'grid-blue table table-responsive table-bordered'],
                'summary' => '',
       
                'options' =>[
                    'class' => '',
                ],
                'columns' => [
                     [
                        'label' => 'Descripción',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:left;'],
                         'value' => function( $model ){
                               


                            return $model->descripcion;
                        },
                    ],
                        [
                        'class' => 'yii\grid\CheckboxColumn',
                        'name' => 'si',
                        'checkboxOptions'=>['style'=>'display: block;margin-right: auto;margin-left: auto;'],
                        'header' => Html::checkBox('selection_all', false, [
                            'class' => 'select-on-check-all hidden',
                            
                            'label' => 'Si',
                        ]),
                    ], 
                       [
                        'class' => 'yii\grid\CheckboxColumn',
                        'name' => 'no',
                        'checkboxOptions'=>['style'=>'display: block;margin-right: auto;margin-left: auto;'],
                        'header' => Html::checkBox('selection_all', false, [
                            'class' => 'select-on-check-all hidden',

                            'label' => 'No',
                        ]),
                    ], 
              
                ],
                
            ]); ?>
        </div>
         <div class="row">
        <div class="col-xs-12">
            <br>
            <p class="titulo-form " >RETROALIMENTACIÓN DE LA AUDITORIA</p>
            <div style = "margin-top: 1px !important;
    border-top: 1px solid #222d32 !important;"><br><br></div>
        
        </div>
    </div>
        <div class="col-md-12 col-xs-12">
        <?= $form->field($model, 'acuerdo')->textarea(['rows' => '6'])->label(false); ?>
        </div>
         <div class="row">
        <div class="col-xs-12">
            <br>
            <p class="titulo-form " >ACUERDOS Y COMPROMISOS DE LA ESTACIÓN</p>
            <div style = "margin-top: 1px !important;
    border-top: 1px solid #222d32 !important;"><br><br></div>
        
        </div>
    </div>
        <div class="col-md-12 col-xs-12">
        <?= $form->field($model, 'compromiso')->textarea(['rows' => '6'])->label(false); ?>
        </div>
        <div class="col-md-12 col-xs-12">
             <br><br><br><br>
        <div class="form-group text-center">  
<?= Html::submitButton($model->isNewRecord ? 'GUARDAR' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-submit' : 'btn btn-primary']) ?>
        </div>
    </div>
        <?php ActiveForm::end(); ?>
    </div>
