<?php

use yii\helpers\Html;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\InputWidget;
use kartik\form\ActiveForm;
use kartik\date\DatePicker;
use kartik\file\FileInput;
use app\models\Empresa;
use app\models\Pieza;
use app\models\Estacion;
use app\models\Marca;
use app\models\Modelo;
use app\models\Capacitaciones;
use app\models\Caso;
use app\models\Tipoaccidente;
use app\models\LicitacionSolicitud;
use app\models\Ubicacionunidad;
use app\models\Cliente;
use kartik\select2\Select2;
use yii\web\View;
use kartik\depdrop\DepDrop;
use yii\web\JsExpression;
use wbraganca\dynamicform\DynamicFormWidget;
/* @var $this yii\web\View */
/* @var $model app\models\Caso */
/* @var $form yii\widgets\ActiveForm */

$rubro = $model_licitacion->idRubro->nombre;
$dia = explode("*", $model_licitacion['dias']);
$dia_concat='';
foreach ($dia as $key => $value) {
  if ($value==1) {
    $dia_concat = 'Lunes';
  }
  if ($value==2) {
    $dia_concat = $dia_concat.', Martes';
  }
  if ($value==3) {
    $dia_concat = $dia_concat.', Miércoles';
  }
  if ($value==4) {
    $dia_concat = $dia_concat.', Jueves';
  }
  if ($value==5) {
    $dia_concat = $dia_concat.', Viernes';
  }
  if ($value==6) {
    $dia_concat = $dia_concat.', Sábado';
  }
  if ($value==7) {
    $dia_concat = $dia_concat.', Domingo';
  }
}
 
$alineacion = LicitacionSolicitud::find()->where([ 'id_licitacion' => $model_licitacion->id,'id_solicitud' => 5 ])->all();
$aceite = LicitacionSolicitud::find()->where([ 'id_licitacion' => $model_licitacion->id,'id_solicitud' => 6 ])->all();
$via = LicitacionSolicitud::find()->where([ 'id_licitacion' => $model_licitacion->id,'id_solicitud' => 7 ])->all();
$delivery = LicitacionSolicitud::find()->where([ 'id_licitacion' => $model_licitacion->id,'id_solicitud' => 8 ])->all();


$this->title = 'Registro de licitación';
?>

<?php $form = ActiveForm::begin(['id' => 'dynamic_lista-form', 'options' => ['enctype' => 'multipart/form-data'], ]); ?>
<div class="caso-form">



  <div class="row">
    <div class="col-md-1 col-xs-3">
      <h4 class="titulo"><i class="fa fa-clipboard fa-4x" aria-hidden="true"></i></h4>
    </div>
    <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
      <h4 class="titulo"><span> <?= Html::encode( $this->title .' de '. $rubro  ) ?></span></h4>
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-xs-12">
      <!--<div class="arrow-before"></div><div class="arrow-after"></div>-->
      <div class="notificacion">
        <div class="col-md-2 col-xs-4 text-center bell">
          <h5><i class="fa fa-bell fa-5x" aria-hidden="true"></i></h5>
        </div>
        <div class="col-md-10 col-xs-8 text-left info">
          <?php if ($model_licitacion['garantia']): ?>

          <p><b>Garantía: </b> <?= $model_licitacion['garantia']; ?></p>

        <?php endif ?>

        <?php if ($model_licitacion['tiempo_entrega_s_d']): ?>

        <p><b>Tiempo de entrega Sto.Domingo: </b><?= $model_licitacion['tiempo_entrega_s_d'].' Horas.' ?></p>

      <?php endif ?>

      <?php if ($model_licitacion['tiempo_entrega_i']): ?>

      <p><b>Tiempo de entrega interior: </b><?= $model_licitacion['tiempo_entrega_i'].' Horas.' ?></p>

    <?php endif ?>

    <?php if ($model_licitacion['dias']): ?>

    <p><b>Días de entrega: </b> <?= $dia_concat; ?></span></p>

  <?php endif ?>
  <?php if ($model_licitacion['fecha_inicio']): ?>

  <p><b>Fecha de Licitación: </b> <?= $model_licitacion['fecha_inicio']; ?></span></p>

<?php endif ?>

</div>
</div>
</div>
</div>

<br><br>
<div class="row" style="border-bottom: 1px solid #222d32;">
  <h4>ASOCIACIÓN DE PRECIOS</h4>
</div>
<br>
<!-- _______________________Dinamico____________________________ -->

<div class="clearfix"></div></br> <br><br>

<?php DynamicFormWidget::begin([
  'widgetContainer' => 'dynamicform_wrapper_resultados',
  'widgetBody' => '.container-items',
  'widgetItem' => '.item',
  'limit' => 50,
  'min' => 1,
  'insertButton' => '.add-item',
  'deleteButton' => '.remove-item',
  'model' => $modelsprecios[0],
  'formId' => 'dynamic_lista-form',
  'formFields' => [
  'tipo_capacitacion',
  'numero_cap',

  ],
  ]); ?>
  <table class="table table-hover">
    <tbody class="container-items">
      <?php foreach ($modelsprecios as $i => $modelmodel): ?>
      <tr class="item panel panel-default">
        <td>
          <div class="col-xs-12 col-md-2">
            <?php
            echo $form->field($modelmodel, "[{$i}]modelo")->textInput([])->label('MODELO');
            ?>
          </div>
          <div class="col-xs-12 col-md-2">
            <?php
            echo $form->field($modelmodel, "[{$i}]marca")->textInput([])->label('MARCA');
            ?>
          </div>
          <div class="col-xs-12 col-md-2">
            <?php
            echo $form->field($modelmodel, "[{$i}]precio_mercado")->textInput([])->label('PRECIO MERCADO');
            ?>
          </div>
          <div class="col-xs-12 col-md-2">
            <?php
            echo $form->field($modelmodel, "[{$i}]porc_descuento")->textInput([])->label('% DE DESCUENTO');
            ?>
          </div>
          <div class="col-xs-12 col-md-2">
            <?php
            echo $form->field($modelmodel, "[{$i}]precio_f")->textInput([])->label('PRECIO CARROZZA');
            ?>
          </div>



          <div class="col-xs-12 col-md-1 text-center">
            <button type="button" class="remove-item btn btn-cancelar btn-xs" style="margin-top:28px;">
              <i class="glyphicon glyphicon-minus"></i></button>
            </div>
          </td>
        </tr>
      <?php endforeach; ?>
      <?php DynamicFormWidget::end(); ?>
    </tbody>
  </table>

  <button type="button" id="agg" class="add-item btn btn-accion  btn-xs pull-right" >Agregar</button>



  <!-- _____________________Fin Dinamico________________________ -->




  


  <br><br>
  <div class="row" style="border-bottom: 1px solid #222d32;">
    <h4>SERVICIOS SOLICITADOS</h4>
  </div>
  <br>
  <div class="row">


   <?php if ($model_licitacion['garantia']): ?>

   <div class="col-md-4 col-xs-12 " >
    <?php 
     $piezas = ['6 meses'=>'6 meses', '12 meses'=>'12 meses'];
    echo $form->field($modelprecioprov, 'garantia')->dropDownList(
      $piezas,
      [
      'prompt'=>'Seleccione Garantía',
      'class' => 'form-control',
      ])->label('GARANTÍA'); ?>
    </div>


  <?php endif ?>

  <?php if ($model_licitacion['tiempo_entrega_s_d']): ?>
  <div class="col-md-4 col-xs-12 " >
    <?php echo $form->field($modelprecioprov, 'tiempo_santo_domingo', [
      'addon' => ['prepend' => ['content'=>'Horas']],
      ])->textInput(['disabled'=> false])->label('TIEMPO DE ENTREGA STO. DOMINGO');  ?>
  </div>

    <?php endif ?>

    <?php if ($model_licitacion['tiempo_entrega_i']): ?>

        <?php echo $form->field($modelprecioprov, 'tiempo_interior', [
                    'addon' => ['prepend' => ['content'=>'Horas']]
                ])->textInput(['disabled'=> false])->label('TIEMPO DE ENTREGA INTERIOR DEL PAÍS');  ?>
      
    <?php endif ?>


    <?php if ($model_licitacion['dias']): ?>

<br>
 

        <div class="col-xs-12 col-md-12  checkbox">
    <h5>DÍAS DE ENTREGA EN SANTO DOMINGO: </h5>
                    <label class="col-md-1 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="check1" value="1"  name="sem[]"> Lun.
                    </label>
                    <label class="col-md-1 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="check2" value="2"  name="sem[]"> Mar.
                    </label>
                    <label class="col-md-1 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="check3" value="3"  name="sem[]"> Mie.
                    </label>
                    <label class="col-md-1 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="check4" value="4"  name="sem[]"> Jue.
                    </label>
                    <label class="col-md-1 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="check5" value="5"  name="sem[]"> Vie.
                    </label>
                    <label class="col-md-1 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="check6" value="6"  name="sem[]"> Sab.
                    </label>
                      <label class="col-md-1 col-xs-12 checkbox-inline" >
                        <input type="checkbox" id="check7"  value="7"  name="sem[]"> Dom.
                    </label>
                </div>
  <?php endif ?>
  </div>
<br><br>
  <?php if ($alineacion): ?>
<div class="row">

        <div class="col-md-6 col-xs-12 " >
        
       <?php 
                $opciones = ['1'=>'Si', '2'=>'No'];
                echo $form->field($modelprecioprov, 'alineacion')->dropDownList(
                        $opciones,
                        [
                            'prompt'=>'Seleccione...',
                            'class' => 'form-control',
                            'disabled'=>false,
                        ] )->label('¿SERVICIO DE ALINEACIÓN?'); ;
            ?>
    </div>
    <div class="col-md-6 col-xs-12 " >
        <?= $form->field($modelprecioprov, 'obs_alineacion')->textInput(['maxlength' => true])->label('OBSERVACIONES ADICIONALES') ?>
    
    </div>
</div>
      
    <?php endif ?>
    <?php if ($aceite): ?>
<div class="row">

        <div class="col-md-6 col-xs-12 " >
        
       <?php 
                $opciones = ['1'=>'Si', '2'=>'No'];
                echo $form->field($modelprecioprov, 'capacitacion')->dropDownList(
                        $opciones,
                        [
                            'prompt'=>'Seleccione...',
                            'class' => 'form-control',
                            'disabled'=>false,
                        ] )->label('¿CAPACITACIÓN DE CAMBIO DE ACEITE?'); ;
            ?>
    </div>
    <div class="col-md-6 col-xs-12 " >
        <?= $form->field($modelprecioprov, 'obs_capacitacion')->textInput(['maxlength' => true])->label('OBSERVACIONES ADICIONALES') ?>
    
    </div>
</div>
      
    <?php endif ?>
    <?php if ($via): ?>
<div class="row">

        <div class="col-md-6 col-xs-12 " >
        
       <?php 
                $opciones = ['1'=>'Si', '2'=>'No'];
                echo $form->field($modelprecioprov, 'entrega_via')->dropDownList(
                        $opciones,
                        [
                            'prompt'=>'Seleccione...',
                            'class' => 'form-control',
                            'disabled'=>false,
                        ] )->label('¿SERVICIO DE ENTREGA EN LA VÍA?'); ;
            ?>
    </div>
    <div class="col-md-6 col-xs-12 " >
        <?= $form->field($modelprecioprov, 'obs_entrega_via')->textInput(['maxlength' => true])->label('OBSERVACIONES ADICIONALES') ?>
    
    </div>
</div>
      
    <?php endif ?>
    <?php if ($delivery): ?>
<div class="row">

        <div class="col-md-6 col-xs-12 " >
        
       <?php 
                $opciones = ['1'=>'Si', '2'=>'No'];
                echo $form->field($modelprecioprov, 'delivery')->dropDownList(
                        $opciones,
                        [
                            'prompt'=>'Seleccione...',
                            'class' => 'form-control',
                            'disabled'=>false,
                        ] )->label('¿SERVICIO DE DELIVERY?'); ;
            ?>
    </div>
    <div class="col-md-6 col-xs-12 " >
        <?= $form->field($modelprecioprov, 'obs_delivery')->textInput(['maxlength' => true])->label('OBSERVACIONES ADICIONALES') ?>
    
    </div>
</div>
      
    <?php endif ?>

<br><br><br>

    <div class="form-group" align="center">
    <?= Html::submitButton('GUARDAR', ['class' => 'btn btn-primario']) ?>
  </div>
</div>
<?php ActiveForm::end(); ?>
<style>

.notificacion {
 /*width: 80px;*/
 text-align: center;
 font-family: verdana;
 box-shadow: 3px 3px 3px #888888;
 border: 1px solid #dedede;
 height: 150px;
}

.notificacion > .info > p {
  margin: 0 0 3px;
}

.notificacion > .info {
  color:black; padding-top: 15px;
}

.notificacion > .bell {
  background-color: #fee599; height: 100%; border-right: 1px solid #dedede;
}

.notificacion > .bell > h5 {
  margin-top: 25%; font-weight: bold;
}
</style>
