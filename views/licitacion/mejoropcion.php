<?php
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use app\models\SolicitudCompraProveedor;

$this->title = 'EVALUACIÓN DE OFERTAS';
/*echo "<pre>";
print_r($cotizaciones_piezas);
echo "</pre>";*/
$id = Yii::$app->request->get('id');
?>

<div class="cotizaciones-recibidas">
	<?php $form = ActiveForm::begin(['id' => 'cotizaciones-recibidas-form']); ?>

	<div class="row">
		<div class="col-md-1 col-xs-3">
			<h4 class="titulo"><img src="<?= Yii::$app->request->baseUrl ?>/img/hand_money_64.png" alt=""></h4>
		</div>
		<div class="col-md-10 col-xs-9" style="padding-top: 30px;">
			<h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
		</div>
	</div>
	</br>
	<div class="row">
		<div class="col-md-4 col-xs-12">
			<div class="form-group">
                <label class="control-label" for="id_estado">FILTRAR POR RUBRO</label>
                <?=  
                    Select2::widget([
                        'name' => 'id_pieza',
                        'data' => $piezas,
                        'options' => ['placeholder' => 'Selecciona el Rubro', 'id' => 'id-pieza'],
                        'pluginOptions' => [ 'allowClear'=>true ],
                    ]); 
                ?>
            </div>
		</div>
		      <div class="col-md-8 col-xs-12">
            <div class="pull-right" style="margin-top: 30px;">
                <?= Html::a('Listado de Licitaciones',[ 'index' ], ['class' => 'btn btn-small-gris']) ?> 
				 
			</div>
        </div>
    </div>
	</br>
	<?php
		$piezaPrecio = [];
		$piezaID = [];
		$piezaProveedor = [];	
		$uno =1;
		$dos = '6 meses';
	?>
	<?php foreach ($licitaciones_recibidas as $key => $licitacion): ?>
			<div id="prov-<?= $licitacion->id_prov_licitacion ?>" class="col-md-6 col-xs-12 pieza-content"  data-pieza="<?= $licitacion->id ?>">
				<div class="panel panel-default">

					<div class="panel-heading text-center pieza-<?= $licitacion->idProvLicitacion->idLicitacion->id_rubro ?> pp-<?= $licitacion->id_prov_licitacion."-".$licitacion->idProvLicitacion->idLicitacion->id_rubro ?>"  data-id="<?= $licitacion->idProvLicitacion->id_licitacion ?>" data-estado="<?= $licitacion->idProvLicitacion->idLicitacion->id_estado_lic ?>" data-idpieza="<?= $licitacion->idProvLicitacion->idLicitacion->id_rubro ?>">
				  	 
				  		<b> <?= 'Proveedor: '.$licitacion->idProvLicitacion->idProvPieza->proveedor->nombre.' Pieza Licitada: ' .$licitacion->idProvLicitacion->idProvPieza->pieza->nombre ?> </b>
						<input type="checkbox" id="check-<?= $licitacion->idProvLicitacion->id_licitacion?>" name="selected[]" value="<?= $licitacion->idProvLicitacion->id_licitacion ?>" hidden="true">
				  		<span class="pull-right" id="icon-<?= $licitacion->idProvLicitacion->id_licitacion?>" hidden="true"><i class="fa fa-check-circle fa-lg" aria-hidden="true" style="color: #4b8723; background-color: #c5e0b4;"></i></span>
				  	</div>


				 
		    		<table class="table table-bordered">
		    			<tbody>
		    				<tr>
		    					<td colspan="3"><b>Ahorro: </b> <?= $uno ?> </td>
		    				</tr>
		    				<tr>
		    					<td><b>% Delivery: </b> <?php if($licitacion->delivery){echo 'Si';}else{echo 'No';}'<span class="sin-respuesta">SIN RESPUESTA</span>' ?></td>
		    					<td><b>Capacitación: </b> <?php if($licitacion->capacitacion){echo 'Si';}else{echo 'No';}'<span class="sin-respuesta">SIN RESPUESTA</span>' ?> </td>
		    					<td><b>Entrega en la vía: </b> <?php if($licitacion->entrega_via){echo 'Si';}else{echo 'No';}'<span class="sin-respuesta">SIN RESPUESTA</span>' ?> </td>
		    				</tr>
		    				<tr>
		    					<td><b>Tiempo de entrega en Santo Domingo: </b> <?php if($licitacion->tiempo_santo_domingo){echo $licitacion->tiempo_santo_domingo;}else{echo 'No Tiene';} ?> </td>
		    					<td><b>Tiempo de entrega en el interior: </b> <?php if($licitacion->tiempo_interior){echo $licitacion->tiempo_interior;}else{echo 'No Tiene';} ?> </td>
		    					 
		    				</tr>
		    				<tr>
		    					<td><b>Garantia: </b> <?= $dos ?> </td>
		    					<td colspan="2"><div class="col-xs-5"><b>Cumplimiento: </b></div> <div class="col-xs-4"><?= Html::textInput('comprar[]', $uno,['class' => 'form-control pieza-'.$dos.'', 'type' => 'number', 'disabled' => true ]) ?></div></td>
		    				</tr>
		    			</tbody>
			    	</table>

					<?php							
					  
					?>
				</div>
			</div>
		<?php endforeach ?>		
		<div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="form-group text-center">
                <?= Html::submitButton('Guardar', ['class' => 'btn btn-submit', 'id' => 'guardar-submit']) ?>
            </div>
        </div>
    </div>

    
<style>
	.panel{
		border-radius: 0px;
	}

	.panel .panel-heading{
		font-size: 12pt;
	}

	.sin-respuesta{
		color: #c51414;
		font-weight: bold;
	}

	.panel-heading{
	    padding: 5px 5px;
	}

	.panel-heading.selected {
		background-color: #c5e0b4 !important;
	}

	.panel-heading.sugerido {
		background-color: #33adff;
	}

	.modal-dialog {
        margin: 80px auto;
    }

</style>

<script>

	

	$(function() {  
		var proveedor = <?php echo json_encode($piezaProveedor) ?>;
		var piezas =  <?php echo json_encode($piezaID) ?>;
		for(var i=0 ; i< proveedor.length ; i++) {
			$(".cotizaciones-recibidas").find('div.pp-'+proveedor[i]+'-'+piezas[i]).addClass('sugerido');				
		}
	});

	$('.panel-heading').on('click', function(event) {
		event.preventDefault();
		/* Act on the event */
		var id = $(this).data('id');
		var estado = $(this).data('estado');
		var idpieza = $(this).data('idpieza');
		var parent = $(this).parent().parent().attr('id');
		if (estado != 0) {
			if ( $('#check-'+id).is(':checked') ) {
				$('#check-'+id).prop({
					checked: false,
				});
				$(this).removeClass('selected');
				$('#icon-'+id).hide();			

				$('.pieza-'+idpieza+'').parent().parent().show();
				$('#'+parent+' input.pieza-'+idpieza+'').prop('disabled', true);
			} else{
				$('#check-'+id).prop({
					checked: true,
				});
				$(this).addClass('selected');
				$('#icon-'+id).show();

				$('.pieza-'+idpieza+'').parent().parent().hide();
				$('#'+parent+' .pieza-'+idpieza+'').parent().parent().show();
				$('#'+parent+' input.pieza-'+idpieza+'').prop('disabled', false);

			}
		}
	});

	$('#guardar-submit').on('click', function(event) {
		event.preventDefault();
		/* Act on the event */
		var total = $("input[name='selected[]']:checked").length;
		var countpiezas = $('#count-piezas').val();
		if (total < countpiezas) {
			alert('Debes Selecionar todas las piezas');
		} else{
			$(this).submit();
		}
	});

	// $('#guardar-submit').on('click', function(event) {
	// 	event.preventDefault();
	// 	/* Act on the event */
	// 	var total = $("input[name='selected[]']:checked").length;

	// 	if (total < 3) {
	// 		$('#observacionModal').modal('show');
	// 	} else{
	// 		$(this).submit();
	// 	}
	// });

	$('#id-pieza').on('change', function(event) {
		event.preventDefault();
		/* Act on the event */
		var id_pieza = $(this).val();
		$('.pieza-content').hide();
		if ( id_pieza == '') {
			$('.pieza-content').show();
		} else{
			$('div[data-pieza='+id_pieza+']').show();
		}
	});


	$('#solicitarCotizacionModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var id = button.data('id');
        $('#submit-solicitar').data('id', id);
        $.ajax({
            url: "../getproveedores",
            type: 'post',
            data: {
                id: id,
            },
            success: function (response) {
				console.log(response);
                $('#response-proveedor').empty();
                for (var i = 0; i < response.length; i++) {
                    $('#response-proveedor').append(
                        '<div class="col-md-6 col-xs-12">'+
                            '<label class="checkbox-inline">'+
                                '<input type="checkbox" value="'+response[i]['id']+'" name="proveedor[]"> '+response[i]['nombre']+
                            '</label>'+
                        '</div>'
                    );
                }
            }
        });
    });

    $('#solicitar').on('submit', function(event) {
        event.preventDefault();
        event.stopImmediatePropagation();

        $('#submit-solicitar').attr({
            disabled: 'true'
        });

        $('#solicitarCotizacionModal').modal('hide');
        var id = $('#submit-solicitar').data('id');

        var form = $(this);
        var data = form.serializeArray();      
        data.push({name: 'id', value: id});

        $.post( form.attr("action"), data ).done(function(response){
            location.reload();
        });
    });
</script>

