<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LicitacionesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="licitacion-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'fecha_inicio') ?>

    <?= $form->field($model, 'fecha_culminacion') ?>

    <?= $form->field($model, 'objetivo') ?>

    <?= $form->field($model, 'garantia') ?>

    <?php // echo $form->field($model, 'tiempo_entrega_s_d') ?>

    <?php // echo $form->field($model, 'tiempo_entrega_i') ?>

    <?php // echo $form->field($model, 'dias') ?>

    <?php // echo $form->field($model, 'id_solicitudes') ?>

    <?php // echo $form->field($model, 'id_rubro') ?>

    <?php // echo $form->field($model, 'id_estado_lic') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
