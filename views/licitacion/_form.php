<?php

use yii\helpers\Html;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\InputWidget;
use kartik\form\ActiveForm;
use kartik\date\DatePicker;
use kartik\file\FileInput;
use app\models\Empresa;
use app\models\Pieza;
use app\models\Estacion;
use app\models\Marca;
use app\models\Modelo;
use app\models\Capacitaciones;
use app\models\Caso;
use app\models\Tipoaccidente;
use app\models\Ubicacionunidad;
use app\models\Cliente;
use kartik\select2\Select2;
use yii\web\View;
use kartik\depdrop\DepDrop;
use yii\web\JsExpression;
use wbraganca\dynamicform\DynamicFormWidget;
/* @var $this yii\web\View */
/* @var $model app\models\Caso */
/* @var $form yii\widgets\ActiveForm */
?>

    <?php $form = ActiveForm::begin(['id' => 'dynamic_lista-form', 'options' => ['enctype' => 'multipart/form-data'], ]); ?>
<div class="caso-form">

 
    <br>
        <div class="row">
        <div class="col-md-4 col-xs-12">
            <?php 
                   
                echo $form->field($model, 'id_rubro')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Pieza::find()->where(['tipo_pieza_id'=>1])->orderBy('nombre')->all(), 'id_pieza', 'nombre'),
                    'options' => ['placeholder' => 'Selecciona la prioridad',],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('PIEZA'); 

           ?>
    </div>
     <div class="col-md-4 col-xs-12">
<?=$form->field($model, 'id_rubro')->widget(Select2::className(), [
        'data' => ArrayHelper::map(Pieza::find()->where(['tipo_pieza_id'=>1])->orderBy('nombre')->all(), 'id_pieza', 'nombre'),
        'options' => [
            'id'       => 'id_rubro',
            'multiple' => true,
        ],
    ])->label('Categoria de pieza')?>
    </div>
     <div class="col-md-4 col-xs-12">
         <?php 
           echo $form->field($model, 'fecha_inicio')->widget(DatePicker::classname(), [
            'options' => ['placeholder' => 'Ingrese Fecha Inicio'],
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd',
            ]
        ])->label('FECHA DE INICIO');
          ?>

       </div>

        <div class="col-md-4 col-xs-12">
         
                      <?php echo $form->field($model, 'objetivo', [
                    'addon' => ['prepend' => ['content'=>'%']]
                ])->textInput(['disabled'=> false])->label('OBJETIVO DE AHORRO');  ?>

       </div>
</div>
      <br>
 <label class="t2">SOLICITUDES DE SERVICIOS</label>
        <span type="button" class="glyphicon glyphicon-plus" id="imagen_cambiar8" onclick="cambiar8()"></span>
        <br>
        <div class="col-md-12 " id="ocultar8" style="display:none;">


                 <div class="col-xs-12 col-md-12  checkbox">
                    <label class="col-md-3 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox1" onclick="garantia(this);" value="1" name="pie[]"> Garantía
                    </label>
                    <label class="col-md-3 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox2" onclick="sol_ser(this);" value="2" name="pie[]"> Tiempo de entrega Sto. Domingo
                    </label>
                    <label class="col-md-3 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox3" onclick="interior(this);" value="3" name="pie[]"> Tiempo de entrega Interior del país
                    </label>
 
                </div>
                <br>
                  <div class="col-xs-12 col-md-12 checkbox">
                    <label class="col-md-3 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox4" onclick="dias(this);" value="4" name="pie[]"> Días de entrega
                    </label>
                    <label class="col-md-3 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox5" value="5" name="pie[]"> Alineación y balanceo
                    </label>
                    <label class="col-md-3 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox6" onclick="capacitacion(this);" value="6" name="pie[]"> Capacitaciones
                    </label>

                   
                </div>
                            <br>
                  <div class="col-xs-12 col-md-12 checkbox">
                    <label class="col-md-3 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox7" value="7" name="pie[]"> Recoger
                    </label>
                    <label class="col-md-3 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox8" value="8" name="pie[]"> Delivery
                    </label>
                    <label class="col-md-3 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox9" onclick="todas(this);" value="9" name="pie[]"> Todas
                    </label>
                   
                </div>
                           
 
                   
                </div>


        </div>
        <div class="clearfix"></div></br>
 
    <!-- -DEFINICION DE PARAMETROS DEL SERVICIO -->
      <div class="row " style="border-top: 10px" >
        <h4>DEFINICION DE PARAMETROS DEL SERVICIO</h4>
        <hr >
    </div>
 
    <br>
    <div class="row">
   <div class="col-md-4 col-xs-12 " >
            <?php 
                 $piezas = ['6 meses'=>'6 meses', '12 meses'=>'12 meses'];
                 echo $form->field($model, 'garantia')->dropDownList(
                                    $piezas,
                                    [
                                        'prompt'=>'Seleccione Garantía',
                                        'class' => 'form-control',
                                        'disabled'=>true, 
                                    ])->label('GARANTÍA'); ?>
    </div>


        <div class="col-md-4 col-xs-12">
         
                      <?php echo $form->field($model, 'tiempo_entrega_s_d', [
                    'addon' => ['prepend' => ['content'=>'Horas']],
                ])->textInput(['disabled'=> true])->label('TIEMPO DE ENTREGA STO. DOMINGO');  ?>

       </div>

        <div class="col-md-4 col-xs-12">
         
                      <?php echo $form->field($model, 'tiempo_entrega_i', [
                    'addon' => ['prepend' => ['content'=>'Horas']]
                ])->textInput(['disabled'=> true])->label('TIEMPO DE ENTREGA INTERIOR DEL PAÍS');  ?>

       </div>
    </div>
           <div class="clearfix"></div></br>
        <div class="row">
        <h4>DÍAS DE ENTREGA EN EL INTERIOR DEL PAÍS</h4>
        <hr>
        <div class="clearfix"></div></br>
    </div>

        <div class="col-xs-12 col-md-12  checkbox">
                    <label class="col-md-1 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="check1" value="1" disabled name="sem[]"> Lun.
                    </label>
                    <label class="col-md-1 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="check2" value="2" disabled name="sem[]"> Mar.
                    </label>
                    <label class="col-md-1 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="check3" value="3" disabled name="sem[]"> Mie.
                    </label>
                    <label class="col-md-1 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="check4" value="4" disabled name="sem[]"> Jue.
                    </label>
                    <label class="col-md-1 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="check5" value="5" disabled name="sem[]"> Vie.
                    </label>
                    <label class="col-md-1 col-xs-12 checkbox-inline">
                        <input type="checkbox" id="check6" value="6" disabled name="sem[]"> Sab.
                    </label>
                      <label class="col-md-1 col-xs-12 checkbox-inline" >
                        <input type="checkbox" id="check7"  value="7" disabled name="sem[]"> Dom.
                    </label>
                </div>


 
  <!-- _______________________Dinamico____________________________ -->
 
           <div class="clearfix"></div></br> <br><br>

 


  <?php DynamicFormWidget::begin([
          'widgetContainer' => 'dynamicform_wrapper_resultados',
        'widgetBody' => '.container-items',
        'widgetItem' => '.item',
        'limit' => 50,
        'min' => 1,
        'insertButton' => '.add-item',
        'deleteButton' => '.remove-item',
        'model' => $modelscap[0],
        'formId' => 'dynamic_lista-form',
        'formFields' => [
            'tipo_capacitacion',
            'numero_cap',
             
        ],
  ]); ?>
  <table class="table table-hover">
      <tbody class="container-items">
      <?php foreach ($modelscap as $i => $modelmodel): ?>
          <tr class="item panel panel-default">
              <td>
                  <div class="col-xs-12 col-md-4">
                    <?php
                        echo $form->field($modelmodel, "[{$i}]tipo_capacitacion")->textInput([])->label('TIPO DE CAPACITACIÓN');
                    ?>
                    </div>
                       <div class="col-xs-12 col-md-4">
                    <?php
                        echo $form->field($modelmodel, "[{$i}]numero")->textInput([])->label('NÚMERO DE CAPACITACIONES');
                    ?>
                                </div>
      

                  <div class="col-xs-12 col-md-1 text-center">
                      <button type="button" class="remove-item btn btn-cancelar btn-xs" style="margin-top:28px;">
                          <i class="glyphicon glyphicon-minus"></i></button>
                  </div>
              </td>
          </tr>
      <?php endforeach; ?>
  <?php DynamicFormWidget::end(); ?>
      </tbody>
  </table>

  <button type="button" id="agg" class="add-item btn btn-accion  btn-xs pull-right" disabled>Agregar</button>

 

    <!-- _____________________Fin Dinamico________________________ -->



      <div class="form-group" align="center">
              <?= Html::submitButton('GUARDAR', ['class' => 'btn btn-primario']) ?>
          </div>
    <?php ActiveForm::end(); ?>
  
    </div>
 
<script type="text/javascript">

   function garantia(checkbox) {
      if (checkbox.checked) {
   $('#licitacion-garantia').removeAttr("disabled")
 
 }else{
  $("#licitacion-garantia").prop('disabled', true);

 }


    }

       function dias(checkbox) {
      if (checkbox.checked) {
        $('#check1').removeAttr('disabled');
        $('#check2').removeAttr('disabled');
        $('#check3').removeAttr('disabled');
        $('#check4').removeAttr('disabled');
        $('#check5').removeAttr('disabled');
        $('#check6').removeAttr('disabled');
        $('#check7').removeAttr('disabled');

 }else{
       
        document.getElementById("check1").disabled= true;
        document.getElementById("check2").disabled= true;
        document.getElementById("check3").disabled= true;
        document.getElementById("check4").disabled= true;
        document.getElementById("check5").disabled= true;
        document.getElementById("check6").disabled= true;
        document.getElementById("check7").disabled= true;
 }


    }
   function interior(checkbox) {
      if (checkbox.checked) {
   $('#licitacion-tiempo_entrega_i').removeAttr("disabled");
 
 }else{
  $("#licitacion-tiempo_entrega_i").prop('disabled', true);

 }


    }

      function capacitacion(checkbox) {
      if (checkbox.checked) {
   // $('#capacitaciones-0-tipo_capacitacion').removeAttr("disabled");
   // $('#capacitaciones-0-numero').removeAttr("disabled");
   $('#agg').removeAttr("disabled");

 
 }else{
  // $("#capacitaciones-0-tipo_capacitacion").prop('disabled', true);
  // $("#capacitaciones-0-numero").prop('disabled', true);
  $("#agg").prop('disabled', true);


 }


    }

      function btnagg(checkbox) {
 alert("entroooooo");
   $('#capacitaciones-0-tipo_capacitacion').removeAttr("disabled");
   $('#capacitaciones-0-numero').removeAttr("disabled");
  

 


    }
       function sol_ser(checkbox) {
      if (checkbox.checked) {
   $('#licitacion-tiempo_entrega_s_d').removeAttr("disabled")
 
 }else{
  $("#licitacion-tiempo_entrega_s_d").prop('disabled', true);

 }


    }
     function cambiar8()
        {
            var elemento = document.querySelector('#imagen_cambiar8');
            // class
            if (elemento.className == "glyphicon glyphicon-minus") {
                $("#imagen_cambiar8").removeClass("glyphicon glyphicon-minus");
                $("#imagen_cambiar8").addClass("glyphicon glyphicon-plus");
                $("#ocultar8").hide();
            } else {
                $("#imagen_cambiar8").removeClass("glyphicon glyphicon-plus");
                $("#imagen_cambiar8").addClass("glyphicon glyphicon-minus");
                 $("#ocultar8").show();
           
            }
 
        }
     function todas(checkbox) {
      if (checkbox.checked) {
   $("#inlineCheckbox1").prop('checked', true);
   $("#inlineCheckbox2").prop('checked', true);
   $("#inlineCheckbox3").prop('checked', true);
   $("#inlineCheckbox4").prop('checked', true);
   $("#inlineCheckbox5").prop('checked', true);
   $("#inlineCheckbox6").prop('checked', true);
   $("#inlineCheckbox7").prop('checked', true);
   $("#inlineCheckbox8").prop('checked', true);
   $('#check1').removeAttr('disabled');
   $('#check2').removeAttr('disabled');
   $('#check3').removeAttr('disabled');
   $('#check4').removeAttr('disabled');
   $('#check5').removeAttr('disabled');
   $('#check6').removeAttr('disabled');
   $('#check7').removeAttr('disabled');
   $('#licitacion-garantia').removeAttr("disabled")
   $('#licitacion-tiempo_entrega_i').removeAttr("disabled");
   $('#capacitaciones-0-tipo_capacitacion').removeAttr("disabled");
   $('#capacitaciones-0-numero').removeAttr("disabled");
   $('#agg').removeAttr("disabled");
   $('#licitacion-tiempo_entrega_s_d').removeAttr("disabled")

 }else{
  $("#inlineCheckbox1").prop('checked', false);
   $("#inlineCheckbox2").prop('checked', false);
   $("#inlineCheckbox3").prop('checked', false);
   $("#inlineCheckbox4").prop('checked', false);
   $("#inlineCheckbox5").prop('checked', false);
   $("#inlineCheckbox6").prop('checked', false);
   $("#inlineCheckbox7").prop('checked', false);
   $("#inlineCheckbox8").prop('checked', false);
   $("#licitacion-garantia").prop('disabled', true);
  document.getElementById("check1").disabled= true;
  document.getElementById("check2").disabled= true;
  document.getElementById("check3").disabled= true;
  document.getElementById("check4").disabled= true;
  document.getElementById("check5").disabled= true;
  document.getElementById("check6").disabled= true;
  document.getElementById("check7").disabled= true;
  $("#licitacion-tiempo_entrega_i").prop('disabled', true);
  $("#capacitaciones-0-tipo_capacitacion").prop('disabled', true);
  $("#capacitaciones-0-numero").prop('disabled', true);
  $("#agg").prop('disabled', true);
  $("#licitacion-tiempo_entrega_s_d").prop('disabled', true);
 }


    }

    
</script>
 