 <?php

use yii\helpers\Html;
use app\models\LicitacionSearch;
use app\models\Pieza;
use app\models\User;
use app\models\ProveedorPiezaLic;
use app\models\EstadoLicitacion;
use yii\helpers\ArrayHelper;
use yii\db\Query;
use kartik\grid\GridView;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Vehiculo */

$this->title = 'Evaluación de ofertas';
if ($precprov['dias']) {
	$dia = explode("*", $precprov['dias']);
$dia_concat='';
foreach ($dia as $key => $value) {
  if ($value==1) {
    $dia_concat = 'Lunes';
  }
  if ($value==2) {
    $dia_concat = $dia_concat.', Martes';
  }
  if ($value==3) {
    $dia_concat = $dia_concat.', Miércoles';
  }
  if ($value==4) {
    $dia_concat = $dia_concat.', Jueves';
  }
  if ($value==5) {
    $dia_concat = $dia_concat.', Viernes';
  }
  if ($value==6) {
    $dia_concat = $dia_concat.', Sábado';
  }
  if ($value==7) {
    $dia_concat = $dia_concat.', Domingo';
  }
}
}else{
	$dia_concat='';
}

?>
<div class="ficha-vehiculo">
	
	<div class="row">
		<div class="col-md-1 col-xs-3">
			<h4 class="titulo"><i class="fa fa-clipboard fa-4x" aria-hidden="true"></i></h4>
		</div>
		<div class="col-md-10 col-xs-9" style="padding-top: 30px;">
			<h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
		</div>
	</div>

	 <br><br><br>
	<?= GridView::widget([
       'dataProvider' => $dataProvider,
                // 'filterModel'   => $searchModel,                
                'tableOptions' => ['class' => 'table table-responsive',],
                'summary' => '',
                'options' =>[
                    'class' => 'grid',
                ],
        
        'columns' => [
          [
                              'attribute' => 'modelo',
                              'label' => 'Modelo',
                              'format' => 'text',
                              'filter' =>true,
                              'enableSorting' => false,
                           
                              
              ],
               [
                              'attribute' => 'marca',
                              'label' => 'Marca',
                              'format' => 'text',
                              'filter' =>true,
                              'enableSorting' => false,
                              
                              
              ],
               [
                               
                              'label' => 'Precio de Mercado',
                              'format' => 'text',
                              'filter' =>true,
                              'enableSorting' => false,
                              'value' => function( $model ){
                                 return $model->precio_mercado;

                              }
                              
              ],
               [
                             
                              'label' => '% de Descuento',
                              'format' => 'text',
                              'filter' =>true,
                              'enableSorting' => false,
                              'value' => function( $model ){
                                 return $model->porc_descuento.' %';

                              }
                  
                              
              ],
               [
                              'attribute' => 'precio_f',
                              'label' => 'Precio a Forteza',
                              'format' => 'text',
                              'filter' =>true,
                              'enableSorting' => false,
          
                              
              ],

    
        ],
    ]); ?>
    <br><br>
<div class="row">
	<?php  if(\Yii::$app->user->can('admin')){  ?>
	<div class="col-md-3 col-xs-12">
		<label>Ahorro Total:</label> <?php if($precprov->garantia){echo ' '.$precprov->garantia; }else{echo 'No posee';} ?> 
	</div>
	<?php } ?>
	<div class="col-md-3 col-xs-12">
		<label>Garantía:</label> <?php if($precprov->garantia){echo ' '.$precprov->garantia; }else{echo 'No posee';} ?> 
	</div>
	<div class="col-md-3 col-xs-12">
			<label>Tiempo Entrega Sto. Domingo:</label> <?php if($precprov->tiempo_santo_domingo){echo ' '.$precprov->tiempo_santo_domingo.'Horas.'; }else{echo ' No posee';} ?> 
	</div>
	<div class="col-md-3 col-xs-12">
			<label>Tiempo Entrega Interior del País:</label> <?php if($precprov->tiempo_interior){echo ' '.$precprov->tiempo_interior.'Horas.'; }else{echo ' No posee';} ?> 
	</div>
	 <div class="col-md-3 col-xs-12">
			<label>Días de entrega Interior del País:</label> <?php if($dia_concat){echo ' '.$dia_concat.'.'; }else{echo ' No posee';} ?> 
	</div>
		
 

</div>
<div class="row">
 <div class="col-md-6 col-xs-12">
			<label>Alineación y Balanceo:</label> <?php
			 if($precprov->alineacion){
			 	if ($precprov->obs_alineacion==1) {
			 		 
			 	echo ' Si';
			 	}else echo ' No';
		 
			 }else{echo ' No posee';} ?> 
	</div>
	 <div class="col-md-6 col-xs-12">
			<label>Observaciones Adicionales:</label> <?php if($precprov->obs_alineacion){echo ' '.$precprov->obs_alineacion.'.'; }else{echo ' No posee';} ?> 
	</div>
</div>
<div class="row">
 <div class="col-md-6 col-xs-12">
			<label>Delivery:</label> <?php 
		 
			if($precprov->delivery){
			 	if ($precprov->delivery==1) {
			 		 
			 	echo ' Si';
			 	}else echo ' No';
		 
			 }
			else{echo ' No posee';} ?> 
	</div>
	 <div class="col-md-6 col-xs-12">
			<label>Observaciones Adicionales:</label> <?php if($precprov->obs_delivery){echo ' '.$precprov->obs_delivery.'.'; }else{echo ' No posee';} ?> 
	</div>
</div>
    <div class="row">
 <div class="col-md-6 col-xs-12">
			<label>Capacitaciones:</label>
			 <?php  
			 		if($precprov->capacitacion){
			 	if ($precprov->capacitacion==1) {
			 		 
			 	echo ' Si';
			 	}else echo ' No';
		 
			 }
			 else{echo ' No posee';} ?> 
	</div>
	 <div class="col-md-6 col-xs-12">
			<label>Observaciones Adicionales:</label> <?php if($precprov->obs_capacitacion){echo ' '.$precprov->obs_capacitacion.'.'; }else{echo ' No posee';} ?> 
	</div>
</div>
 <div class="row">
 <div class="col-md-6 col-xs-12">
			<label>Entrega en la vía:</label> <?php 
			 
if($precprov->entrega_via){
			 	if ($precprov->entrega_via==1) {
			 		 
			 	echo ' Si';
			 	}else echo ' No';
		 
			 }
			else{echo ' No posee';} ?> 
	</div>
	 <div class="col-md-6 col-xs-12">
			<label>Observaciones Adicionales:</label> <?php if($precprov->obs_entrega_via){echo ' '.$precprov->obs_entrega_via.'.'; }else{echo ' No posee';} ?> 
	</div>
</div>
    
       
 