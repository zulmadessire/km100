<?php

 
use app\models\Pieza;
use app\models\EstadoLicitacion;

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\db\Query;
use kartik\grid\GridView;
use app\models\PipelineServicio;
use app\models\Empresa;
use app\models\SolicitudServicioTaller;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use app\models\AnalistaMantenimiento;
use app\models\CotizacionTaller;
use app\models\CotizacionTallerAnalista;
use app\models\CotizacionTallerActividadSearch;
use app\models\Modelo;
use app\models\EstadoServicio;
use kartik\date\DatePicker;
use app\models\Cliente;



/* @var $this yii\web\View */
/* @var $searchModel app\models\LicitacionesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Listado de Licitaciones';
 
?>
<div>
       <div class="row">
      <div class="col-md-1 col-xs-3">
        <h4 class="titulo"><i class="fa fa-tint fa-4x" aria-hidden="true"></i></h4>
      </div>
      <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
        <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
      </div>
    </div>
    </br>
    <div class="row">
        <div class="col-md-12">
            <?php
                echo Html::a('Registrar Licitación', ['create'], ['class' => 'btn btn-small-gris', 'style' => 'float:right;']);
            ?>
        </div>
    </div>
    <br><br><br>
    <?= GridView::widget([
       'dataProvider' => $dataProvider,
                'filterModel'   => $searchModel,                
                'tableOptions' => ['class' => 'table table-responsive',],
                'summary' => '',
                'options' =>[
                    'class' => 'grid',
                ],
        
          'resizableColumns' => false,
                'columns' => [
                    [
                        'class'=>'kartik\grid\ExpandRowColumn',
                        'width'=>'50px',
                        'allowBatchToggle' => false,
                        'expandIcon' => '<i class="fa fa-caret-right" aria-hidden="true" style="color:#d9d9d9;"></i>',
                        'collapseIcon' => '<i class="fa fa-caret-down" aria-hidden="true" style="color:#d9d9d9;"></i>',
                        'value'=>function ($model, $key, $index, $column) {
                         
 
                                return GridView::ROW_COLLAPSED;
                        
                        },
                        'detail'=>function ($model, $key, $index, $column) {

                          if ($model->id_estado_lic ==1) {
                                    return Yii::$app->controller->renderPartial('_expand-lic',['estado' => 1,'model'=>$model->id]);
                              
                          }if ($model->id_estado_lic ==2) {

                                    return Yii::$app->controller->renderPartial('_expand-lic',['estado' => 2,'model'=>$model->id]);
                           
                          }if ($model->id_estado_lic ==3) {

                                    return Yii::$app->controller->renderPartial('_expand-lic',['estado' => 3,'model'=>$model->id]);
                           
                          }
                        },
                        'headerOptions'=>['class'=>'kartik-sheet-style'],
                        'expandOneOnly'=>true,
                        'enableRowClick' => true,
                    ],
             
                         [
                              'attribute' => 'id_rubro',
                              'label' => 'Rubro',
                              'format' => 'text',
                              'filter' => ArrayHelper::map(Pieza::find()->where(['tipo_pieza_id'=>1])->orderBy('nombre')->all(), 'id_pieza', 'nombre'),
                              'enableSorting' => false,
                              'value' => function( $model ){
                                 return $model->idRubro->nombre;

                              }
                              
              ],
               [
                        'label' => 'Fecha de Inicio',
                        'attribute' => 'fecha_inicio',
                        'format' => 'text',                  
                        'filter' =>DatePicker::widget([
                          'name' => 'LicitacionesSearch[fecha_inicio]', 
                          'pluginOptions' => [
                            'format' => 'yyyy-mm-dd',
                          ]
                        ]),
                        'value' => function( $model ){
                            if ( $model->fecha_inicio ) {
                                 return $model->fecha_inicio;
                            } else{
                                return '-';
                            }
                        }
                    ],
                      [
                        'label' => 'Fecha de Culminación',
                        'attribute' => 'fecha_culminacion',
                        'format' => 'text',                        
                        'filter' =>DatePicker::widget([
                          'name' => 'LicitacionesSearch[fecha_culminacion]', 
                          'pluginOptions' => [
                            'format' => 'yyyy-mm-dd',
                          ]
                        ]),
                        'value' => function( $model ){
                            if ( $model->fecha_culminacion ) {
                                 return $model->fecha_culminacion;
                            } else{
                                return '-';
                            }
                        }
                    ],

                     [
                        'label' => 'Estatus',
                        'attribute' => 'id_estado_lic',
                        'format' => 'raw',                        
                        'filter' =>true,
                        'value' => function( $model ){

                            if ( $model->id_estado_lic) {


                                 $edo = $model->idEstadoLic->nombre;
                                 $color = $model->idEstadoLic->color;
                            } 
                            if( !$model->id_estado_lic) {
                                return '-';
                            }
                             return '<span class="label label-oval" style="background-color:'.$color.'; ">'.$edo.'</span>';
                        },
                        'filterType'=> GridView::FILTER_SELECT2,
                        'filter'=>ArrayHelper::map(EstadoLicitacion::find()->orderBy('nombre')->all(), 'id', 'nombre'),
                        'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                        'name' => 'ServicioSearch[id_servicio]',
                        ],
                    ],
         
           
        
            

    
        ],
    ]); ?>
</div>
