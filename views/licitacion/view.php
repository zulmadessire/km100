<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Licitacion */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Licitacions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="licitacion-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'fecha_inicio',
            'fecha_culminacion',
            'objetivo',
            'garantia',
            'tiempo_entrega_s_d',
            'tiempo_entrega_i',
            'dias',
 
            'id_rubro',
            'id_estado_lic',
        ],
    ]) ?>

</div>
