 <?php

use yii\helpers\Html;
use app\models\LicitacionSearch;
use app\models\Pieza;
use app\models\User;
use app\models\ProveedorPiezaLic;
use app\models\EstadoLicitacion;
use yii\helpers\ArrayHelper;
use yii\db\Query;
use kartik\grid\GridView;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Vehiculo */

$this->title = 'Listado de licitaciones';

?>
<div class="ficha-vehiculo">
	  <?php $form = ActiveForm::begin([
            'action' => ['indexlicproveedor'],
            'method' => 'get',
        ]); ?>
	<div class="row">
		<div class="col-md-1 col-xs-3">
			<h4 class="titulo"><i class="fa fa-clipboard fa-4x" aria-hidden="true"></i></h4>
		</div>
		<div class="col-md-10 col-xs-9" style="padding-top: 30px;">
			<h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
		</div>
	</div>
  <br>
<div class="col-md-3 col-xs-12">
            <div class="form-group">
                <label class="control-label" for="id_empresa">RUBRO</label>
                <?=  Select2::widget([
                        'name' =>'LicitacionesSearch[id_rubro]',
                        'data' => ArrayHelper::map(Pieza::find()->where(['tipo_pieza_id'=>1])->orderBy('nombre')->all(), 'id_pieza', 'nombre'),
                        'options' => ['placeholder' => 'Selecciona el rubro', 'id' => 'id_empresa']
                    ]); 
                ?>
            </div>
        </div>
            <div class="col-md-4 col-xs-12">
            <div class="form-group">
            
              <label class="control-label">FECHA DE INICIO</label> ;
           <?= DatePicker::widget([
                          'name' => 'LicitacionesSearch[fecha_inicio]', 
                          'pluginOptions' => [
                            'format' => 'yyyy-mm-dd',
                          ]
                        ]);
 
         ?>
        
            </div>
        </div>
         <br>
         <div class="col-md-2 col-xs-12" style="margin-top:3px">
            <div class="form-group text-center">
                <?= Html::submitButton('BUSCAR', ['class' => 'btn2']) ?>
            </div>
        </div>
	 <br><br><br>
    <?= GridView::widget([
       'dataProvider' => $dataProvider,
                // 'filterModel'   => $searchModel,                
                'tableOptions' => ['class' => 'table table-responsive',],
                'summary' => '',
                'options' =>[
                    'class' => 'grid',
                ],
        
        'columns' => [
          [
                              'attribute' => 'idRubro.nombre',
                              'label' => 'Rubro',
                              'format' => 'raw',
                              'enableSorting' => false,
                             
                              
              ],

              [
                        'label' => 'Fecha de la Licitación',
                        'attribute' => 'fecha_inicio',
                        'format' => 'text',                  
                        'value' => function( $model ){
                            if ( $model->fecha_inicio ) {
                                 return $model->fecha_inicio;
                            } else{
                                return '-';
                            }
                        }
                    ],
                     [
                        'label' => 'Cantidad comprada',
                        'format' => 'text',                  
                        'filter' =>true,
                        'value' => function( $model ){
                             $id_proveedor = User::findOne(Yii::$app->user->id);

                              $query = new query();
                                $query->select('SUM(cpp.cantidad_recibida) pieza')
                                    ->from('cotizacion_proveedor_pieza cpp')
                                    ->join('INNER JOIN','cotizacion_proveedor cp','cpp.id_cotizacion_proveedor = cp.id')
                                    ->join('INNER JOIN','solicitud_compra_proveedor scp','cp.id_solicitud_compra_proveedor = scp.id')
                                    ->join('INNER JOIN','cotizacion_taller_pieza ctp','cpp.id_cotizacion_taller_pieza = ctp.id')
                                    ->where('scp.id_proveedor = '. $id_proveedor['id_proveedor'] )
                                    ->andWhere('ctp.id_pieza='.$model->id_rubro)
                                    ->andWhere('cpp.aprobacion = 2');

                                $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                                $pieza = Yii::$app->db->createCommand($sql)->queryOne();
                                return $pieza['pieza'];
                        }
                    ],
                      [
                        'label' => 'Estatus',
                        'attribute' => 'id_estado_lic',
                        'format' => 'raw',                        
                        'filter' =>true,
                        'value' => function( $model ){
                           $id_proveedor = User::findOne(Yii::$app->user->id);

                            $query = new query();
                            $query->select('ppl.estado')
                                ->from('proveedor_pieza_lic ppl')
                                ->where('ppl.id_licitacion = '. $model->id )
                                ->andWhere('ppl.id_prov_pieza='.$id_proveedor['id_proveedor']);
                            $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
                            $estado = Yii::$app->db->createCommand($sql)->queryOne();

                            if ( $estado['estado']) {
                                if ($estado['estado']==1) {
                                  $edo = 'Pendiente';
                                 $color = '#70AD47';
                             return '<span class="label label-oval" style="background-color:'.$color.'; ">'.$edo.'</span>';
                                }
                                 if ($estado['estado']==2) {
                                  $edo = 'Enviada';
                                 $color = '#8FAADC';
                             return '<span class="label label-oval" style="background-color:'.$color.'; ">'.$edo.'</span>';
                                }

                                 
                            } 
                            if( !$estado['estado']) {
                                return '-';
                            }
                        },
                        'filterType'=> GridView::FILTER_SELECT2,
                        'filter'=>ArrayHelper::map(EstadoLicitacion::find()->orderBy('nombre')->all(), 'id', 'nombre'),
                        'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                        'name' => 'ServicioSearch[id_servicio]',
                        ],
                    ],             
                [  

                                    'class' => 'yii\grid\ActionColumn',
                                    //'contentOptions' => ['style' => 'width:260px;'],
                                    'header'=>'Acciones',
                                    'template' => '{menu}',
                                    'buttons' => [

                                        //view button
                                        'menu' => function ($url, $model) {
                                 $id_proveedor = User::findOne(Yii::$app->user->id);
                                 $prov = $id_proveedor['id_proveedor'];

 
      $ppl = ProveedorPiezaLic::find()->where(['id_licitacion'=> $model->id])->andWhere(['id_prov_pieza'=> $prov])->one();
 
                                      
                                                return  '<div class="dropdown">
                                          <button class="btn btn-default dropdown-toggle btn-sm" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i>
                                          <span class="caret"></span></button>
                                          <ul class="dropdown-menu">'
                                          
                                          .(( $ppl->estado==1)?
                                                '<li>'.Html::a('<span><i class="fa fa-check-square-o fa-sm" aria-hidden="true"></i></span>&nbsp;&nbsp;Registrar Licitación',[ '/licitacion/registroproveedor', 'id' => $model->id ]).' </li>'
                                            : '').(( $ppl->estado==2)?
                                                '<li>'.Html::a('<span><i class="fa fa-check-square-o fa-sm" aria-hidden="true"></i></span>&nbsp;&nbsp;Ver Licitacion',[ '/licitacion/vistaproveedor', 'id' => $model->id, 'prov'=>$prov]).' </li>'
                                            : '').'</ul>
                                        </div>';

                                        },
                                    ],

                               ],
            

    
        ],
    ]); ?>
        <?php ActiveForm::end(); ?>

</div>