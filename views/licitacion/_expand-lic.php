<?php
use yii\helpers\Html;
use yii\db\Query;
use app\models\CotizacionTallerActividad;
use kartik\grid\GridView;

$porcentaje_seguimiento_fecha = ((1 * 100) / 3);
 
?>

<div class="expand-licitacion-index">
    <div class="row">
        
                <div class="col-xs-12 col-md-offset-2 col-md-8">
            <div style="border-left: 1px solid #9e9e9e; height: 80px; position: relative;">
                <!-- <div style="height: 50px; top: 33px; position: relative;"> -->
                    <div style="border-right: 1px solid #9e9e9e; border-top: 1px solid #9e9e9e; border-bottom: 1px solid #9e9e9e; height: 40px; top: 20px; left: 0;">
                    </div>
                        <?php if ( $estado == 1 ): ?>
                    <div style="border-right: 0px solid #9e9e9e; width: 50%; float: left; height: 100%; position: absolute; left: 0; top: 0; " >  
                        <div style="border-top: 0px solid #9e9e9e; border-bottom: 1px solid #9e9e9e; background-color: #548235; height: 40px; width: 70%; ">
                        </div>
                            <div style="border-right: 1px solid #9e9e9e; float: left; height: 100px; position: absolute; top: 0; left: 70%;">
                     </div>
                                        <i class="fa fa-check" aria-hidden="true" style="padding-left: 10%"></i>
                                         Lic. enviada  
                        <?php endif ?>
                    <?php if ( $estado == 2 ): ?>
               
                     <div style="border-right: 0px solid #9e9e9e; width: 50%; float: left; height: 100%; position: absolute; left: 0; top: 0; ">
                        <div style="border-top: 1px solid #9e9e9e; border-bottom: 1px solid #9e9e9e; background-color: #548235; height: 40px; width: 130%; ">
                        </div>
                           <div style="border-right: 1px solid #9e9e9e; float: left; height: 100px; position: absolute; top: 0; left: 130%;">
                            </div>

                       <i class="fa fa-check" aria-hidden="true" style="padding-left: 65%"></i>
                 
                       Lic. Por evaluar</div>
                       <br>
                               <div class="row">
                <div class="col-xs-12 col-md-12">
                    <div class="pull-right">
                        <?= Html::a('Ver propuestas',['mejoropcion', 'id' =>$model], ['class' => 'btn btn-small-gris', ]) ?> 
                    </div>
                </div>
            </div>

                        <?php endif ?>  
                        <?php if ( $estado == 3 ): ?>
                     <div style="border-right: 0px solid #9e9e9e; width: 50%; float: left; height: 100%; position: absolute; left: 0; top: 0; ">
                        <div style="border-top: 1px solid #9e9e9e; border-bottom: 1px solid #9e9e9e; background-color: #548235; height: 40px; width: 200%; ">
                        </div>
                           <div style="border-right: 1px solid #9e9e9e; float: left; height: 100px; position: absolute; top: 0; left: 200%;">
                            </div>
                                  <i class="fa fa-check" aria-hidden="true" style="padding-left: 65%"></i>
                              </i>Lic. Cerrada
                        <?php endif ?>

                    </div>
               <br><br><br>
         
                    
        <!-- </div> -->

    </div>
 </div>