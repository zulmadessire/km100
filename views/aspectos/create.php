<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Aspectos */

$this->title = 'Create Aspectos';
$this->params['breadcrumbs'][] = ['label' => 'Aspectos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aspectos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
