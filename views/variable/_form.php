<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Variable */
/* @var $form yii\widgets\ActiveForm */
?>
    <div class="row">
    <div class="col-xs-12 col-md-6">
      <div class="row">
        <div class="col-md-2 col-xs-3">
            <h4 class="titulo"><i class="fa fa-file-text fa-4x" aria-hidden="true"></i></h4>
        </div>
        <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
            <h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-md-6">
    <br>
    <br>
        <ul class="nav nav-pills pull-right">
            <li role="presentation">
                    
                    <?= Html::a('Listado de variables', ['index'], ['class' => 'btn btn-small-gris']) ?>

            </li>
        </ul>
    </div>
</div>
<div class="variable-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'label')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'value')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'key')->textInput(['maxlength' => true, 'disabled' => $model->isNewRecord ? false : true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Registrar Varible' : 'Actualizar Variable', ['class' => $model->isNewRecord ? 'btn btn-primario' : 'btn btn-primario']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
