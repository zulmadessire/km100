<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Variable */

$this->title = 'Crear Variable';
// $this->params['breadcrumbs'][] = ['label' => 'Variables', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="variable-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
