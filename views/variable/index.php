<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\VariableSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Variables';
//$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="row">
        <div class="col-xs-12 col-md-6">
          <div class="row">
            <div class="col-md-2 col-xs-3">
                <h4 class="titulo"><i class="fa fa-list-ol fa-4x" aria-hidden="true"></i></h4>
        	</div>
        	<div class="col-md-10 col-xs-9" style="padding-top: 30px;">
        		<h4 class="titulo"><span> <?= Html::encode( $this->title) ?></span></h4>
        	</div>
          </div>
        </div>
        <div class="col-xs-12 col-md-6">
        <br>
        <br>
            <ul class="nav nav-pills pull-right">
                <li role="presentation">
                        
                        <?php // Html::a('Registro de Variable', ['create'], ['class' => 'btn btn-small-gris']) ?>

                </li>
            </ul>
        </div>
    </div>
<div class="variable-index">
    <br>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table'],
        'options' =>[
            'class' => 'grid table-responsive',
        ],        
        'layout' => "{items}\n<div align='center'>{pager}</div>",
        'summary' => '',
        
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'label',
            'descripcion',
            'value',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
