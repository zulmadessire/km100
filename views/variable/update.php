<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Variable */

 $this->title = 'Actualizar Variable ' 
// $this->params['breadcrumbs'][] = ['label' => 'Variables', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
// $this->params['breadcrumbs'][] = 'Update';
?>
<div class="variable-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
