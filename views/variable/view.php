<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Variable */

$this->title = 'INFORMACIÓN DE VARIABLES';
?>
<div class="variable-view">

<div class="row">
<div class="col-xs-12 col-md-6">
  <div class="row">
    <div class="col-md-2 col-xs-3">
        <h4 class="titulo"><i class="fa fa fa-wrench fa-4x" aria-hidden="true"></i></h4>
    </div>
    <div class="col-md-10 col-xs-9" style="padding-top: 30px;">
        <h4 class="titulo">INFORMACIÓN DE VARIABLES</h4>
    </div>
  </div>
</div>
<div class="col-xs-12 col-md-6">
<br>
<br>
    <ul class="nav nav-pills pull-right">
        <li role="presentation">
                
                <?=  Html::a('Listado de variables', ['index'], ['class' => 'btn btn-small-gris']) ?>

        </li>
    </ul>
</div>
</div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'label',
            'descripcion',
            'value',
            'key'
        ],
    ]) ?>

</div>
