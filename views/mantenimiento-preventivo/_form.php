<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\models\Modelo;
use app\models\Marca;
use app\models\Estacion;
use app\models\Empresa;
use app\models\GarantiaVehiculo;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use kartik\form\ActiveForm;
use kartik\file\FileInput;
use kartik\grid\GridView;
use yii\db\Query;
 use yii\web\UploadedFile;
/* @var $this yii\web\View */
/* @var $model app\models\MantenimientoPreventivo */
/* @var $form yii\widgets\ActiveForm */

if ($model_vehiculo) {
    $modelo = Modelo::findOne($model_vehiculo->id_modelo);
    $marca = Marca::findOne($modelo->id_marca)->nombre;
    //$garantia = GarantiaVehiculo::findOne(['id_vehiculo'=>$model_vehiculo->id])->anular;
    $garantia = 1;
}
?>

<div class="mantenimiento-preventivo-form">

    
    <?php $form = ActiveForm::begin(); ?>
    
    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DATOS DEL VEHÍCULO</p>
            <hr>
        </div>
    </div>

    <div class="row">

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_modelo')->textInput(['disabled' => true, 'value' => $marca ])->label('Marca') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'id_modelo')->textInput(['value' => strtoupper($model_vehiculo->idModelo->nombre), 'disabled' => true])->label('Modelo') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'mva')->textInput(['maxlength' => true, 'disabled' => true])->label('Ficha o MVA') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'chasis')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'placa')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model_vehiculo, 'km')->textInput(['maxlength' => true, 'disabled' => true, 'value' => number_format(floatval($model_vehiculo->km), 0, ',', '.'). ' Km', ])->label('KM REGISTRADO') ?>
        </div>
    </div>
    <br>
    <br>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DETALLES DEL MANTENIMIENTO</p>
            <hr>
        </div>
    </div>
    <br>

    <div class="row">
        <?php 

            if (number_format(floatval($model_vehiculo->km), 0, ',', '.')>50000) {
                # code...
           
         ?>
    
         <?php 
            if ($garantia == 1) {?>
                    <div class="col-md-4 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="id_estado">CLIENTE</label>
                            <?=  Select2::widget([
                                    'name' => 'MantenimientoPreventivo[id_empresa]',
                                    'data' => ArrayHelper::map(Empresa::find()->select(['nombre'])->distinct()->orderBy('nombre')->all(), 'nombre', 'nombre'),
                                    'options' => ['placeholder' => 'Selecciona el cliente', 'id' => 'id_empresa']
                                ]); 
                            ?>
                        </div>
                    </div>
           <?php  }  ?>

                  <?php 
            if ($garantia == 0) {?>
                    <div class="col-md-4 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="id_estado">CLIENTE</label>
                            <?=  Select2::widget([
                                    'name' => 'MantenimientoPreventivo[id_empresa]',
                                    'data' => ['Concesionario'=>'Concesionario','Estaciones'=> ArrayHelper::map(Empresa::find()->select(['nombre'])->distinct()->orderBy('nombre')->all(), 'nombre', 'nombre')],
                                    'options' => ['placeholder' => 'Selecciona el cliente', 'id' => 'id_empresa']
                                ]); 
                            ?>
                        </div>
                    </div>
           <?php  }  ?>
 

        <div class="col-md-4 col-xs-12">
            <?=
                $form->field($model, 'id_estacion')->widget(DepDrop::classname(), [
                    'type'=>DepDrop::TYPE_SELECT2,
                    'options'=>['id'=>'id_estacion', 'class' => 'form-control'],
                    'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                    'pluginOptions'=>[
                        'depends'=>['id_empresa'],
                        'placeholder'=>'Selecciona la estación',
                        'url'=>Url::to(['/vehiculo/getestacionestaller'])
                    ]
                ])->label('ESTACIÓN');
            ?>
        </div>

        <?php 
         }

         ?>
         <?php 

            if (number_format(floatval($model_vehiculo->km), 0, ',', '.')<50000) {
                # code...
           
         ?>
                       
           <?php   if ($garantia == 1) {?>
        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <label class="control-label" for="id_estado">CLIENTE</label>
                <?=  Select2::widget([
                        'name' => 'MantenimientoPreventivo[id_empresa]',
                        'data' => ArrayHelper::map(Empresa::find()->select(['nombre'])->distinct()->orderBy('nombre')->all(), 'nombre', 'nombre'),
                        'options' => ['placeholder' => 'Selecciona el cliente', 'id' => 'id_empresa']
                    ]); 
                ?>
            </div>
        </div>
        <?php  }  ?>
          <?php   if ($garantia == 0) {?>
        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <label class="control-label" for="id_estado">CLIENTE</label>
                 <?=  Select2::widget([
                        'name' => 'MantenimientoPreventivo[id_empresa]',
                        'data' =>  ['Concesionario'=>'Concesionario','Estaciones'=>ArrayHelper::map(Empresa::find()->select(['nombre'])->distinct()->orderBy('nombre')->all(), 'nombre', 'nombre')],
                        'options' => [
                                    'placeholder' => 'Selecciona el cliente', 
                                    'id' => 'id_empresa',
                                    'onchange' => 'if(($(this).val() == "Concesionario")){
                                          document.getElementById("estaciones").style.display="none";
                                          document.getElementById("concesionarios").style.display="block";
                                            }else{
                                               document.getElementById("estaciones").style.display="block"; 
                                               document.getElementById("concesionarios").style.display="none";
                                            }',
                                    ],
                        
                    ]); 
                ?>
            </div>
        </div>
        <?php  }  ?>
            <div class="col-md-4 col-xs-12" id="estaciones">
                <?=
                    $form->field($model, 'id_estacion')->widget(DepDrop::classname(), [
                        'type'=>DepDrop::TYPE_SELECT2,
                        'options'=>['id'=>'id_estacion', 'class' => 'form-control'],
                        'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                        'pluginOptions'=>[
                            'depends'=>['id_empresa'],
                            'placeholder'=>'Selecciona la estación',
                            'url'=>Url::to(['/vehiculo/getestacionestaller'])
                        ]
                    ])->label('ESTACIÓN');
                ?>
            </div>
                <div class="col-md-4 col-xs-12" id="concesionarios" style="display:none">
                    <label  class="control-label">SUCURSALES</label>
                   <?php 
              $query = new Query();
            $query->select([
                'concat(concesionario.nombre, concat(" - ",sucursal_concesionario.nombre)) as suc'])
                ->from('vehiculo')
                ->join('INNER JOIN', 'concesionario', 'concesionario.id = vehiculo.id_concesionario')
                ->join('INNER JOIN', 'sucursal_concesionario', 'concesionario.id = sucursal_concesionario.concesionario_id')
                ->where(['vehiculo.id' => $model_vehiculo->id])
                ->orderBy(['sucursal_concesionario.nombre' => SORT_ASC]);
            $rows = $query->all();


                $vector=[];
                $tip_clientes = $rows;
                foreach ($tip_clientes as $key => $value) {
            $vector[] = $value['suc'];
                  
                }
                 echo Select2::widget([
                        'name' => 'suc',
                        'data' =>  $vector,
                        'options' => [
                                    'placeholder' => 'Selecciona el cliente', 
                                   
                                    ],
                        
                    ]);
                ?>
       
            </div>
        <?php 
            }
        ?>


        <?php   if ($garantia == 0) {?>
        
            <div class="col-md-4 col-xs-12">
      
             
                <?php
                  echo $form->field($acuerdo, 'imageFiles[]')->fileInput(['multiple' => true, 'accept' => 'image/*', 'class' =>"form-control btn btn-default btn-md",'disabled' => false]);
                ?>
          
           </div>
        <?php  }  ?>
        <?php   if ($garantia == 1) {?>
            <div class="col-md-4 col-xs-12">
                <?php
                    echo $form->field($acuerdo, 'imageFiles[]')->fileInput(['multiple' => true, 'accept' => 'image/*', 'class' =>"form-control btn btn-default btn-md",'disabled' => true]);
                ?>
            </div>
        <?php  }  ?>
    

        <div class="clearfix"></div>

        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <?= Html::label('KM PROYECTADO', 'MantenimientoPreventivo[km_proyectado]', ['class' => 'control-label']) ?>
                <?= Html::textInput('MantenimientoPreventivo[km_proyectado]', $ultimo_mantenimiento->km_proyectado,['class' => 'form-control', 'disabled' => true, 'id' =>'km1']) ?>
            </div>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'km')->textInput(['maxlength' => true, 'id'=>'km2'])->label('KM ACTUAL DEL VEHÍCULO') ?>
        </div>

        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <?= $form->field($model, 'diferencia')->textInput(['maxlength' => true, 'id'=>'diferencia','disabled' => true]) ?>
            </div>
        </div>

        <div class="clearfix"></div>


        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <?= Html::label('TIPO MTTO.', 'MantenimientoPreventivo[tipo]', ['class' => 'control-label']) ?>
                <?= Html::textInput('MantenimientoPreventivo[tipo]', ($ultimo_mantenimiento)?$ultimo_mantenimiento->idProximoTipoMantenimiento->nombre: 'GENERAL',['class' => 'form-control', 'disabled' => true,]) ?>
            </div>
        </div>

        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'responsable')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <br>
    <br>

    <div class="row">
        <div class="col-xs-12">
            <p class="titulo-form">DETALLES DE ACTIVIDADES Y CONSUMOS</p>
            <hr>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'id' => 'mantenimiento-actividades',
                'tableOptions' => ['class' => 'grid-blue table table-responsive table-bordered'],
                'summary' => '',
                'pjax' => true,
                'options' =>[
                    'class' => '',
                ],
                'columns' => [
                    [
                        'label' => 'Actividad Realizada',
                        'attribute' => 'nombre',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                        'contentOptions'=>['style'=>'text-align:left;'],
                    ],
                    [
                        'class' => 'yii\grid\CheckboxColumn',
                        'checkboxOptions'=>['style'=>'display: block;margin-right: auto;margin-left: auto;'],//center checkboxes
                        'header' => Html::checkBox('selection_all', false, [
                            'class' => 'select-on-check-all hidden',//pull right the checkbox
                            'label' => 'Realizado',//pull left the label
                        ]),
                    ],
                    [
                        'label' => 'Pieza consumida',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                        'value' => function( $model ){
                            $piezas = \app\models\ActividadPieza::find()->where(['id_actividad_mnto' => $model->id_actividad_mnto ])->all();
                            
                            foreach ($piezas as $key => $pieza) {
                                $vector_piezas[] = $pieza->idPieza->nombre;
                            }

                            $respuesta_piezas = implode('<br>', $vector_piezas);

                            return $respuesta_piezas;
                        },
                    ],
                    [
                        'label' => 'Tamaño',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                        'value' => function( $model ){
                            $piezas = \app\models\ActividadPieza::find()->where(['id_actividad_mnto' => $model->id_actividad_mnto ])->all();
                            
                            foreach ($piezas as $key => $pieza) {
                                $vector_piezas[] = ($pieza->idPieza->tamano)?$pieza->idPieza->tamano:'-';
                            }

                            $respuesta_piezas = implode('<br>', $vector_piezas);

                            return $respuesta_piezas;
                        },
                    ],
                    [
                        'label' => 'Unidad de medición',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                        'value' => function( $model ){
                            $piezas = \app\models\ActividadPieza::find()->where(['id_actividad_mnto' => $model->id_actividad_mnto ])->all();
                            
                            foreach ($piezas as $key => $pieza) {
                                $vector_piezas[] = ($pieza->idPieza->unidad)?$pieza->idPieza->unidad:'-';
                            }

                            $respuesta_piezas = implode('<br>', $vector_piezas);

                            return $respuesta_piezas;
                        },
                    ],
                    [
                        'label' => 'Cantidad',
                        'format' => 'raw',
                        'filter' => false,
                        'enableSorting' => false,
                        'value' => function( $model ){
                            $piezas = \app\models\ActividadPieza::find()->where(['id_actividad_mnto' => $model->id_actividad_mnto ])->all();
                            
                            foreach ($piezas as $key => $pieza) {
                                $vector_piezas[] = Html::input('text', 'cantidad[]', '0', ['class'=> 'form-control cantidad', 'style' => 'font-size: 10pt; text-align: center;', 'disabled' => true, 'data' => ['id-actividad-pieza' => $pieza->id_activ_pieza], ]).
                                Html::hiddenInput('cantidad_id[]', $pieza->id_activ_pieza, ['class' => 'cantidad_id', 'disabled' => true ]);
                            }

                            $respuesta_piezas = implode('<br>', $vector_piezas);

                            return $respuesta_piezas;
                        },
                        'contentOptions'=>['style'=>'width:80px;'],
                        'headerOptions' => ['width' => '80',],
                    ],
                    [
                        'label' => 'Costo',
                        'format' => 'text',
                        'filter' => false,
                        'enableSorting' => false,
                        'value' => function( $model ){
                            return '-';
                        },
                    ],
                ],
            ]); ?>
        </div>
    </div>
    </br>
    </br>
    </br>
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <?= $form->field($model, 'observaciones')->textarea(['rows' => 3]) ?>
        </div>
    </div>
    <div class="row">
           <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <label class="control-label" for="id_estado">¿QUEDÓ ALGO PENDIENTE POR REALIZAR? </label>
                 <?=  Select2::widget([
                        'name' => 'MantenimientoPreventivo[obs_add]',
                        'data' =>  ['0'=>'No','1'=>'Si'],
                        'options' => [
                                    'placeholder' => 'Selecciona', 
 
                                    'onchange' => 'if(($(this).val() == "0")){
                                          document.getElementById("obs_add").style.display="none";
                                         
                                            }else{
                                               document.getElementById("obs_add").style.display="block"; 
                                             
                                            }',
                                    ],
                        
                    ]); 
                ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-12" id="obs_add" style="display:none">
            <?= $form->field($model, 'observacion_adicional')->textarea(['rows' => 3]) ?>
        </div>
    </div>
    </br>
    </br>



    <?php // $form->field($model, 'fecha_proyectada')->textInput() ?>

    <?php // $form->field($model, 'fecha_realizacion')->textInput() ?>

    <?php // $form->field($model, 'km')->textInput(['maxlength' => true]) ?>

    <?php // $form->field($model, 'id_vehiculo')->textInput() ?>

    <?php // $form->field($model, 'id_estacion')->textInput() ?>

    <div class="col-md-12 col-xs-12">
        <div class="form-group text-center">        <?= Html::submitButton($model->isNewRecord ? 'REGISTRAR' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-submit' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
 
 
 
<script type="text/javascript">

 
      $(document).ready(function () {
          $("#km2").keyup(function () {
              var value = document.getElementById("km1").value; 
              var value2 = document.getElementById("km2").value; 
              var resta  = value2 - value;

              $("#diferencia").val(resta);
          });
      });


    $('input[name="selection[]"]').on('change', function(event) {
        event.preventDefault();
        /* Act on the event */
        if ( this.checked ) {
            $('tr[data-key="'+$(this).val()+'"]').addClass('active');
            $('tr[data-key="'+$(this).val()+'"]>td>input.cantidad').removeAttr("disabled");
            $('tr[data-key="'+$(this).val()+'"]>td>input.cantidad_id').removeAttr("disabled");
        } else{
            $('tr[data-key="'+$(this).val()+'"]').removeClass('active');
            $('tr[data-key="'+$(this).val()+'"]>td>input.cantidad').prop({
                disabled: 'true',
            });
            $('tr[data-key="'+$(this).val()+'"]>td>input.cantidad_id').prop({
                disabled: 'true',
            });
        }
    });
</script>
