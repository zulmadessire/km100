<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MantenimientoPreventivo */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Mantenimiento Preventivos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mantenimiento-preventivo-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'imagen_soporte',
            'responsable',
            'fecha_proyectada',
            'fecha_realizacion',
            'observaciones:ntext',
            'km',
            'id_vehiculo',
            'id_estacion',
        ],
    ]) ?>

</div>
