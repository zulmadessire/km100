<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MantenimientoPreventivo */

$this->title = 'Registro de Mantenimiento';
?>
<div class="mantenimiento-preventivo-create">
	
	<div class="row">
		<div class="col-md-1 col-xs-3">
			<h5 class="titulo"><i class="fa fa-file-text fa-4x" aria-hidden="true"></i></h5>
		</div>
		<div class="col-md-10 col-xs-9" style="padding-top: 30px;">
			<h4 class="titulo"><span> <?= Html::encode( $this->title ) ?></span></h4>
		</div>
	</div>
	</br>
	<div class="row">
		<div class="col-md-12 col-xs-12">
            <div class="pull-right">
                <?= Html::a('Ficha Vehicular',['/vehiculo/ficha', 'id' => $model_vehiculo->id, 'type' => 3, ], ['class' => 'btn btn-small-gris']) ?>
            </div>
		</div>
	</div>

    <?= $this->render('_form', [
        'model' => $model,
        'model_vehiculo' => $model_vehiculo,
        'ultimo_mantenimiento' => $ultimo_mantenimiento,
        'dataProvider' => $dataProvider,
          'acuerdo' => $acuerdo
    ]) ?>

</div>
