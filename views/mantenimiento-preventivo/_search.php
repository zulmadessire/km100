<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MantenimientoPreventivoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mantenimiento-preventivo-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'imagen_soporte') ?>

    <?= $form->field($model, 'responsable') ?>

    <?= $form->field($model, 'fecha_proyectada') ?>

    <?= $form->field($model, 'fecha_realizacion') ?>

    <?php // echo $form->field($model, 'observaciones') ?>

    <?php // echo $form->field($model, 'km') ?>

    <?php // echo $form->field($model, 'id_vehiculo') ?>

    <?php // echo $form->field($model, 'id_estacion') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
