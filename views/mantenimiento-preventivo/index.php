<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MantenimientoPreventivoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mantenimiento Preventivos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mantenimiento-preventivo-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Mantenimiento Preventivo', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'imagen_soporte',
            'responsable',
            'fecha_proyectada',
            'fecha_realizacion',
            // 'observaciones:ntext',
            // 'km',
            // 'id_vehiculo',
            // 'id_estacion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
